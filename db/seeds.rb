after :cli_valoration_fields, :insurance_default,:diseases, :fields_prescription, :location_cities, :specialties, :time_zones, :vaccines do

end

PharmacyItem.delete_all
if MedicamentCategory.all.count == 0
  MedicamentCategory.create(name:"Sin categoría")
end
if MeasureUnitItem.all.count == 0
  MeasureUnitItem.create(name:"Pieza")
end


Feature.create(name:"General", cost:"6000", key:"gen")
Feature.create(name:"Pediatría", cost:"6300", key:"ped")
Feature.create(name:"Ginecología", cost:"6800", key:"gin")
Feature.create(name:"Urología", cost:"6300", key:"uro")

admin_type =  TypeUser.create(id:TypeUser::ADMIN, name: "ADMIN")
doctor_type = TypeUser.create(id:TypeUser::DOCTOR, name: "DOCTOR")
TypeUser.create(id:TypeUser::MEDICAL_ASSISTANT, name: "MEDICAL_ASSISTANT")
TypeUser.create(id:TypeUser::PATIENT, name: "PATIENT")
TypeUser.create(id:TypeUser::SHARED_ASSISTANT, name: "SHARED_ASSISTANT")
Gender.create(name:"Hombre")
Gender.create(name:"Mujer")
person_admin = Person.create(name:"Administrador", last_name:"Sistema", city_id:1, gender_id:1)


User.create(user_name: "admin", password: "m3ds1@dm1n", email:"franciscolopez@stj2.com.mx", type_user: admin_type, person:person_admin)


ActivityType.create(name: "Cita", color:"bgm-cyan")
ActivityType.create(name: "Cirugía", color:"bgm-red")
ActivityType.create(name: "Vacaciones", color:"bgm-green")
ActivityType.create(name: "Otro", color:"bgm-orange")

ActivityStatus.create(id:1, name: "AGENDADO", activity_type_id: 1)
ActivityStatus.create(id:2, name: "EN ESPERA", activity_type_id: 1)
ActivityStatus.create(id:3, name: "EN CONSULTA", activity_type_id: 1)
ActivityStatus.create(id:4, name: "ATENDIDO", activity_type_id: 1)
ActivityStatus.create(id:5, name: "CANCELADO", activity_type_id: 1)


ActivityStatus.create(id:6, name: "AGENDADO", activity_type_id: 2)
ActivityStatus.create(id:7, name: "TERMINADO", activity_type_id: 2)

ActivityStatus.create(id:8, name: "AGENDADO", activity_type_id: 3)
ActivityStatus.create(id:9, name: "TERMINADO", activity_type_id: 3)

ActivityStatus.create(id:10, name: "AGENDADO", activity_type_id: 4)
ActivityStatus.create(id:11,name: "TERMINADO", activity_type_id: 4)

ActivityStatus.create(id:12, name: "POR CONFIRMAR", activity_type_id: 1)

TemplatePrescription.delete_all
TemplatePrescription.create(id:TemplatePrescription::TEMPLATE_HORIZONTAL_A, name:"Plantilla A (Horizontal, Media Carta)")
TemplatePrescription.create(id:TemplatePrescription::TEMPLATE_VERTICAL_B, name:"Plantilla B (Vertical, Media Carta)")
TemplatePrescription.create(id:TemplatePrescription::TEMPLATE_VERTICAL_SIZE_CARTA_C, name:"Plantilla C (Vertical, Tamaño Carta)")

TransactionType.create(name: 'Cobro de Consulta')
TransactionType.create(name: 'Transferencia')
TransactionType.create(name: 'Otros Movimientos')
TransactionType.create(name: 'Cobros de estudios')
TransactionType.create(name: 'Admisión hospitalaria')

Permission.create(id:1, name:'read', label:'Ver')
Permission.create(id:2, name:'create', label:'Agregar')
Permission.create(id:3, name:'update', label:'Editar')
Permission.create(id:4, name:'delete', label:'Eliminar')
Permission.create(id:5, name:'daily_cash', label:'Corte de Caja')


BirthType.create(id:1,name:'Inducido')
BirthType.create(id:2,name:'Conducido')
BirthType.create(id:3,name:'Espontáneo')
BirthType.create(id:4,name:'Eutócico')
BirthType.create(id:5,name:'Distócico')
BirthType.create(id:6,name:'Cesárea')

DeliveryType.create(id:1,name:'Conducido')
DeliveryType.create(id:2,name:'Espontáneo')
DeliveryType.create(id:3,name:'Manual')

EpisiotomyType.create(id:1,name:'Media')
EpisiotomyType.create(id:2,name:'Media Lateral')
EpisiotomyType.create(id:3,name:'Prolongación')

TearType.create(id:1,name:'Perine')
TearType.create(id:2,name:'Vagina')
TearType.create(id:3,name:'Cuello Uterino')

DurationType.create(id:1,name:'Normal')
DurationType.create(id:2,name:'Rápido')
DurationType.create(id:3,name:'Prolongado')

AbortionType.create(id:1,name:'Natural')
AbortionType.create(id:2,name:'Inducido')
AbortionType.create(id:3,name:'Otras Causas')

Bank.create(name:'BANAMEX', is_local:false)
Bank.create(name:'BBVA BANCOMER', is_local:false)
Bank.create(name:'SANTANDER', is_local:false)
Bank.create(name:'BANJERCITO', is_local:false)
Bank.create(name:'HSBC', is_local:false)
Bank.create(name:'INBURSA', is_local:false)
Bank.create(name:'SCOTIABANK', is_local:false)
Bank.create(name:'BANREGIO', is_local:false)
Bank.create(name:'BANORTE', is_local:false)
Bank.create(name:'AMERICAN EXPRESS', is_local:false)
Bank.create(name:'VE POR MAS', is_local:false)
Bank.create(name:'AZTECA', is_local:false)
Bank.create(name:'CAJA', is_local:true)

ClinicalOrderType.create!(id:ClinicalOrderType::STUDY_CLINICAL, name: "Estudio Clínico")
ClinicalOrderType.create!(id:ClinicalOrderType::STUDY_GABINETE,name: "Estudio de Gabinete")

Currency.create(country:"México",name:"Peso mexicano",symbol:"$",iso_code:"MXN",fractional_unit:"Centavo",base_number:100)
Currency.create(country:"Estados Unidos",name:"Dólar estadounidense",symbol:"$",iso_code:"USD",fractional_unit:"Cents",base_number:100)
Currency.create(country:"Belice",name:"Dólar beliceño",symbol:"$",iso_code:"BZD",fractional_unit:"Centavo",base_number:100)
Currency.create(country:"Colombia",name:"Peso colombiano",symbol:"$",iso_code:"COP",fractional_unit:"Centavo",base_number:100)
Currency.create(country:"Costa Rica",name:"Colón costarricense",symbol:"C",iso_code:"CRC",fractional_unit:"Centavo",base_number:100)
Currency.create(country:"Guatemala",name:"Quetzal guatemalteco",symbol:"Q",iso_code:"GTQ",fractional_unit:"Centavo",base_number:100)
Currency.create(country:"Nicaragua",name:"Córdoba nicaragüense",symbol:"C$",iso_code:"NIO",fractional_unit:"Centavo",base_number:100)
Currency.create(country:"Panamá",name:"Balboa panameño",symbol:"B/.",iso_code:"PAB",fractional_unit:"Centésimo",base_number:100)

DistributionType.create(name: "Directa")
DistributionType.create(name: "Distribuidor")
DistributionType.create(name: "Comisionista")
DistributionType.create(name: "Cortesía")


ExpedientPermission.create(id: ExpedientPermission::VIEW_PATIENT_DATA, name: "VER DATOS DE PACIENTE")
ExpedientPermission.create(id: ExpedientPermission::EDIT_PATIENT_DATA, name: "MODIFICAR DATOS DE PACIENTE")
ExpedientPermission.create(id: ExpedientPermission::VIEW_REVIEWS, name: "VER VALORACIONES")
ExpedientPermission.create(id: ExpedientPermission::VIEW_STUDIES, name: "VER ESTUDIOS")
ExpedientPermission.create(id: ExpedientPermission::EDIT_STUDIES, name: "MODIFICAR ESTUDIOS")
ExpedientPermission.create(id: ExpedientPermission::VIEW_MEDICAL_CONSULTATIONS, name: "VER CONSULTAS")
ExpedientPermission.create(id: ExpedientPermission::EDIT_MEDICAL_CONSULTATIONS, name: "MODIFICAR CONSULTAS")
ExpedientPermission.create(id: ExpedientPermission::VIEW_PREGNANCY, name: "VER EMBARAZO")
ExpedientPermission.create(id: ExpedientPermission::EDIT_PREGNANCY, name: "MODIFICAR EMBARAZO")
ExpedientPermission.create(id: ExpedientPermission::VIEW_OBSERVATIONS, name: "VER OBSERVACIONES")
ExpedientPermission.create(id: ExpedientPermission::EDIT_OBSERVATIONS, name: "MODIFICAR OBSERVACIONES")

ExpedientPermission.create(id: ExpedientPermission::EDIT_GENERAL_DATA_MEDICAL_BACKGROUND, name: "MODIFICAR DATOS GENERALES (ANTECEDENTES MÉDICOS)")
ExpedientPermission.create(id: ExpedientPermission::EDIT_VACCINES_MEDICAL_BACKGROUND, name: "MODIFICAR VACUNAS (ANTECEDENTES MÉDICOS)")
ExpedientPermission.create(id: ExpedientPermission::EDIT_PEDIATRICS_MEDICAL_BACKGROUND, name: "MODIFICAR PEDRIATRÍA (ANTECEDENTES MÉDICOS)")
ExpedientPermission.create(id: ExpedientPermission::EDIT_GYNECOLOGY_MEDICAL_BACKGROUND, name: "MODIFICAR GINECOLOGÍA (ANTECEDENTES MÉDICOS)")

HospitalService.create(name:"Cobro de farmacia",is_user:0,user_id:1 )
HospitalService.create(name:"Honorarios medicos",is_user:0,user_id:1 )
HospitalService.create(name:"Abono a cuenta",is_user:0,user_id:1 )


Hospital.create(name:'Particular',description:'Es un consultorio particular',city_id:1)

MaritalStatus.create(name:'Soltero(a)',male:'Soltero',female:'Soltera')
MaritalStatus.create(name:'Casado(a)',male:'Casado',female:'Casada')
MaritalStatus.create(name:'Viudo(a)',male:'Viudo',female:'Viuda')
MaritalStatus.create(name:'Divorciado(a)',male:'Divorciado',female:'Divorciada')
MaritalStatus.create(name:'Unión libre',male:'Unión libre',female:'Unión libre')
MaritalStatus.create(name:'No especificado',male:'No especificado',female:'No especificado')

MeasureUnit.create(name:"No aplica")
MeasureUnit.create(name:"Servicio")
MeasureUnit.create(name:"Pieza")

MovementCategory.create(id:1,name:'Honorarios',m_type:true)
MovementCategory.create(id:2,name:'Caja',m_type:true)
MovementCategory.create(id:3,name:'Otros ingresos',m_type:true)
MovementCategory.create(id:4,name:'Insumos de oficina',m_type:false)
MovementCategory.create(id:5,name:'Pago de servicios',m_type:false)
MovementCategory.create(id:6,name:'Gastos de operación',m_type:false)
MovementCategory.create(id:7,name:'Otros egresos',m_type:false)


MovementType.create(name:'Honorarios por cirugía',movement_category_id:1)
MovementType.create(name:'Recolección de muestra',movement_category_id:1)
MovementType.create(name:'Aplicación de vacuna',movement_category_id:1)
MovementType.create(name:'Fondo de caja',movement_category_id:2)
MovementType.create(name:'Ajuste de saldo',movement_category_id:2)
MovementType.create(name:'Otros ingresos',movement_category_id:3)
MovementType.create(name:'Papelería',movement_category_id:4)
MovementType.create(name:'Mobiliario y equipo',movement_category_id:4)
MovementType.create(name:'Consumibles de cómputo',movement_category_id:4)
MovementType.create(name:'Recolección de basura',movement_category_id:5)
MovementType.create(name:'Servicio de luz eléctrica',movement_category_id:5)
MovementType.create(name:'Servicio de agua potable',movement_category_id:5)
MovementType.create(name:'Servicio de televisión',movement_category_id:5)
MovementType.create(name:'Servicio de telefonía e Internet',movement_category_id:5)
MovementType.create(name:'Sueldos',movement_category_id:6)
MovementType.create(name:'Mantenimiento del Local',movement_category_id:6)
MovementType.create(name:'Mantenimiento de equipos',movement_category_id:6)
MovementType.create(name:'Licencias de operación',movement_category_id:6)
MovementType.create(name:'Publicidad',movement_category_id:6)
MovementType.create(name:'Otros egresos',movement_category_id:7)
MovementType.create(name:'Retiro de utilidad',movement_category_id:7)

OperationRole.create(name:"Médico cirujano")
OperationRole.create(name:"Primer ayudante")
OperationRole.create(name:"Segundo ayudante")
OperationRole.create(name:"Instrumentador")
OperationRole.create(name:"Médico anestesista")
OperationRole.create(name:"Auxiliar de anestesia")
OperationRole.create(name:"Enfermera circulante")

PaymentMethod.create(name: "Efectivo",affect_local: 1, code:"01")
PaymentMethod.create(name: "Cheque",affect_local: 1, code:"02")
PaymentMethod.create(name: "Transferencia",affect_local: 0, code:"03")
PaymentMethod.create(name: "Depósito",affect_local: 0, code:"")
PaymentMethod.create(name: "Tarjeta de crédito",affect_local: 0, code:"04")
PaymentMethod.create(name: "Monederos electronicos",affect_local: 0, code:"05")
PaymentMethod.create(name: "Dinero electronico",affect_local: 0, code:"06")
PaymentMethod.create(name: "Vales de despensa",affect_local: 0, code:"08")
PaymentMethod.create(name: "Tarje de débito",affect_local: 0, code:"28")
PaymentMethod.create(name: "Efectivo de servicio",affect_local: 0, code:"29")

PaymentWay.create(name:"PAGO EN UNA SOLA EXIBICIÓN")
PaymentWay.create(name:"PARCIALIDADES")

FinkokConfig.create(test_username:"jesusuc@stj2.com.mx",test_password:"Jesusuc2016.",prod_username:"",prod_password:"",test_wsdl:"http://demo-facturacion.finkok.com/servicios/soap",prod_wsdl:"https://facturacion.finkok.com/servicios/soap",environment:"pruebas")

efi = EmisorFiscalInformation.create(rfc:"")
ClinicInfo.create(name:"", address:"", telephone: "", fax:"", city_id:1, user_id:1, charge_reports:0)


TypePercentile.create(id: TypePercentile::SIZE_LENGTH, name: "Talla")
TypePercentile.create(id: TypePercentile::HEAD_CIRCUMFERENCE, name: "Circunferencia de la cabeza")
TypePercentile.create(id: TypePercentile::WEIGHT, name: "Peso")
TypePercentile.create(id: TypePercentile::IMC, name: "Indice de masa corporal")


CategoryImc.create(id:CategoryImc::NORMAL, name:"Normal")
CategoryImc.create(id:CategoryImc::OVERWEIGHT, name:"Sobrepeso")
CategoryImc.create(id:CategoryImc::OBESITY_GRADE_ONE, name:"Obesidad grado 1")
CategoryImc.create(id:CategoryImc::OBESITY_GRADE_TWO, name:"Obesidad grado 2")
CategoryImc.create(id:CategoryImc::OBESITY_GRADE_THREE, name:"Obesidad grado 3")

RankImcAdult.create(id:RankImcAdult::MAX, name:"Máximo")
RankImcAdult.create(id:RankImcAdult::MIN, name:"Minimo")

# de 1 a 11 meses niñas
ImcChildrenMax19Year.create(age_month: 0,danger_malnutrition: 2.8, normal:3.2,overweight:3.7,obesity:4.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 1,danger_malnutrition: 3.6, normal:4.2,overweight:4.8,obesity:5.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 2,danger_malnutrition: 4.5, normal:5.1,overweight:5.8,obesity:6.6, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 3,danger_malnutrition: 5.2, normal:5.8,overweight:6.6,obesity:7.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 4,danger_malnutrition: 5.7, normal:6.4,overweight:7.3,obesity:8.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 5,danger_malnutrition: 6.1, normal:6.9,overweight:7.8,obesity:8.8, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 6,danger_malnutrition: 6.5, normal:7.3,overweight:8.2,obesity:9.3, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 7,danger_malnutrition: 6.8, normal:7.6,overweight:8.6,obesity:9.8, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 8,danger_malnutrition: 7.0, normal:7.9,overweight:9.0,obesity:10.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 9,danger_malnutrition: 7.3, normal:8.2,overweight:9.3,obesity:10.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 10,danger_malnutrition: 7.5, normal:8.5,overweight:9.6,obesity:10.9, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 11,danger_malnutrition: 7.7, normal:8.7,overweight:9.9,obesity:11.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)

# de 1 a 11 meses niños
ImcChildrenMax19Year.create(age_month: 0,danger_malnutrition: 2.9, normal:3.3,overweight:3.9,obesity:4.4, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 1,danger_malnutrition: 3.9, normal:4.5,overweight:5.1,obesity:5.8, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 2,danger_malnutrition: 4.9, normal:5.6,overweight:6.3,obesity:7.1, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 3,danger_malnutrition: 5.7, normal:6.4,overweight:7.2,obesity:8.0, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 4,danger_malnutrition: 6.2, normal:7.0,overweight:7.8,obesity:8.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 5,danger_malnutrition: 6.7, normal:7.5,overweight:8.4,obesity:9.3, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 6,danger_malnutrition: 7.1, normal:7.9,overweight:8.8,obesity:9.8, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 7,danger_malnutrition: 7.4, normal:8.3,overweight:9.2,obesity:10.3, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 8,danger_malnutrition: 7.7, normal:8.6,overweight:9.6,obesity:10.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 9,danger_malnutrition: 8.0, normal:8.9,overweight:9.9,obesity:11.0, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 10,danger_malnutrition: 8.2, normal:9.2,overweight:10.2,obesity:11.4, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 11,danger_malnutrition: 8.4, normal:9.4,overweight:10.5,obesity:11.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)

# de 1 a 4 años y 6 meses niñas
ImcChildrenMax19Year.create(age_month: 12,danger_malnutrition: 7.9, normal:8.9,overweight:10.1,obesity:11.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 18,danger_malnutrition: 9.1, normal:10.2,overweight:11.6,obesity:13.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 24,danger_malnutrition: 10.2, normal:11.5,overweight:13.0,obesity:14.8, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 30,danger_malnutrition: 11.2, normal:12.7,overweight:14.4,obesity:16.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 36,danger_malnutrition: 12.2, normal:13.9,overweight:15.8,obesity:18.1, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 42,danger_malnutrition: 13.1, normal:15.0,overweight:17.2,obesity:19.8, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 48,danger_malnutrition: 14.0, normal:16.1,overweight:18.5,obesity:21.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 54,danger_malnutrition: 14.9, normal:17.2,overweight:19.9,obesity:23.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)

# de 1 a 4 años y 6 meses niños
ImcChildrenMax19Year.create(age_month: 12,danger_malnutrition: 8.6, normal:9.6,overweight:10.8,obesity:12.0, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 18,danger_malnutrition: 9.8, normal:10.9,overweight:12.2,obesity:13.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 24,danger_malnutrition: 10.8, normal:12.2,overweight:13.6,obesity:15.3, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 30,danger_malnutrition: 11.8, normal:13.3,overweight:15.0,obesity:16.9, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 36,danger_malnutrition: 12.7, normal:14.3,overweight:16.2,obesity:18.3, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 42,danger_malnutrition: 13.6, normal:15.3,overweight:17.4,obesity:19.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 48,danger_malnutrition: 14.4, normal:16.3,overweight:18.6,obesity:21.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 54,danger_malnutrition: 15.2, normal:17.3,overweight:19.8,obesity:22.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)

# de 5 a 9 años y 6 meses niñas
# Kg
ImcChildrenMax19Year.create(age_month: 60,danger_malnutrition: 15.2, normal:18.2,overweight:21.2,obesity:24.9, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::WOMEN)
# I.M.C
ImcChildrenMax19Year.create(age_month: 66,danger_malnutrition: 12.7, normal:15.2,overweight:16.9,obesity:19.0, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 72,danger_malnutrition: 12.7, normal:15.3,overweight:17.0,obesity:19.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 78,danger_malnutrition: 12.7, normal:15.3,overweight:17.1,obesity:19.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 84,danger_malnutrition: 12.7, normal:15.4,overweight:17.3,obesity:19.8, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 90,danger_malnutrition: 12.8, normal:15.5,overweight:17.5,obesity:20.1, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 96,danger_malnutrition: 12.9, normal:15.7,overweight:17.7,obesity:20.6, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 102,danger_malnutrition: 13.0, normal:15.9,overweight:18.0,obesity:21.0, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 108,danger_malnutrition: 13.1, normal:16.1,overweight:18.3,obesity:21.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 114,danger_malnutrition: 13.3, normal:16.3,overweight:18.7,obesity:22.0, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)

# de 5 a 9 años y 6 meses niños
# Kg
ImcChildrenMax19Year.create(age_month: 60,danger_malnutrition: 16.0, normal:18.3,overweight:21.0,obesity:24.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_KG, gender_id: Gender::MAN)
# I.M.C
ImcChildrenMax19Year.create(age_month: 66,danger_malnutrition: 13.0, normal:15.3,overweight:16.7,obesity:18.4, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 72,danger_malnutrition: 13.0, normal:15.3,overweight:16.8,obesity:18.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 78,danger_malnutrition: 13.1, normal:15.4,overweight:16.9,obesity:18.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 84,danger_malnutrition: 13.1, normal:15.5,overweight:17.0,obesity:19.0, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 90,danger_malnutrition: 13.2, normal:15.6,overweight:17.2,obesity:19.3, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 96,danger_malnutrition: 13.3, normal:15.7,overweight:17.4,obesity:19.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 102,danger_malnutrition: 13.4, normal:15.9,overweight:17.7,obesity:20.1, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 108,danger_malnutrition: 13.5, normal:16.0,overweight:17.9,obesity:20.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 114,danger_malnutrition: 13.6, normal:16.2,overweight:18.2,obesity:20.9, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)

# de 10 a 19 años niñas
ImcChildrenMax19Year.create(age_month: 120,danger_malnutrition: 13.5, normal:16.6,overweight:19.0,obesity:22.6, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 132,danger_malnutrition: 13.9, normal:17.2,overweight:19.9,obesity:23.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 144,danger_malnutrition: 14.4, normal:18.0,overweight:20.8,obesity:25.0, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 156,danger_malnutrition: 14.9, normal:18.8,overweight:21.8,obesity:26.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 168,danger_malnutrition: 15.4, normal:19.6,overweight:22.7,obesity:27.3, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 180,danger_malnutrition: 15.9, normal:20.2,overweight:23.5,obesity:28.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 192,danger_malnutrition: 16.2, normal:20.7,overweight:24.1,obesity:28.9, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 204,danger_malnutrition: 16.4, normal:21.0,overweight:24.5,obesity:29.3, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 216,danger_malnutrition: 16.4, normal:21.3,overweight:24.8,obesity:29.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)
ImcChildrenMax19Year.create(age_month: 228,danger_malnutrition: 16.5, normal:21.4,overweight:25.0,obesity:29.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::WOMEN)

# de 10 a 19 años niños
ImcChildrenMax19Year.create(age_month: 120,danger_malnutrition: 13.7, normal:16.4,overweight:18.5,obesity:21.4, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 132,danger_malnutrition: 14.1, normal:16.9,overweight:19.2,obesity:22.5, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 144,danger_malnutrition: 14.5, normal:17.5,overweight:19.9,obesity:23.6, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 156,danger_malnutrition: 14.9, normal:18.2,overweight:20.8,obesity:24.8, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 168,danger_malnutrition: 15.5, normal:19.0,overweight:21.8,obesity:25.9, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 180,danger_malnutrition: 16.0, normal:19.8,overweight:22.7,obesity:27.0, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 192,danger_malnutrition: 16.5, normal:20.5,overweight:23.5,obesity:27.9, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 204,danger_malnutrition: 16.9, normal:21.1,overweight:24.3,obesity:28.6, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 216,danger_malnutrition: 17.3, normal:21.7,overweight:24.9,obesity:29.2, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)
ImcChildrenMax19Year.create(age_month: 228,danger_malnutrition: 17.6, normal:22.2,overweight:25.4,obesity:29.7, unit_weight: ImcChildrenMax19Year::UNIT_WEIGHT_IMC, gender_id: Gender::MAN)

# ==================== I.M.C para adultos de 20 años en adelante

ImcAdult.create(id:1, imc: 18.5, category_imc_id: CategoryImc::NORMAL, rank_imc_adult_id: RankImcAdult::MIN, association_id: 2 )
ImcAdult.create(id:2, imc: 24.9, category_imc_id: CategoryImc::NORMAL, rank_imc_adult_id: RankImcAdult::MAX, association_id: 1 )

ImcAdult.create(id:3, imc: 25, category_imc_id: CategoryImc::OVERWEIGHT, rank_imc_adult_id: RankImcAdult::MIN, association_id: 4 )
ImcAdult.create(id:4, imc: 29.9, category_imc_id: CategoryImc::OVERWEIGHT, rank_imc_adult_id: RankImcAdult::MAX, association_id: 3 )

ImcAdult.create(id:5, imc: 30, category_imc_id: CategoryImc::OBESITY_GRADE_ONE, rank_imc_adult_id: RankImcAdult::MIN, association_id: 6 )
ImcAdult.create(id:6, imc: 34.9, category_imc_id: CategoryImc::OBESITY_GRADE_ONE, rank_imc_adult_id: RankImcAdult::MAX, association_id: 5 )

ImcAdult.create(id:7, imc: 35, category_imc_id: CategoryImc::OBESITY_GRADE_TWO, rank_imc_adult_id: RankImcAdult::MIN, association_id: 8 )
ImcAdult.create(id:8, imc: 39.9, category_imc_id: CategoryImc::OBESITY_GRADE_TWO, rank_imc_adult_id: RankImcAdult::MAX, association_id: 7 )

ImcAdult.create(id:9, imc: 40, category_imc_id: CategoryImc::OBESITY_GRADE_THREE, rank_imc_adult_id: RankImcAdult::MIN, association_id: nil )




