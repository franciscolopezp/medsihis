pisa = MedicamentLaboratory.create(name:"PISA")

med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOL. CS INY PLAS 250 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:26.00256.round(2),is_consigment:0,sale_cost:16.2516.round(2))
act = ActiveSubstance.create(name:'SOL. CS INY PLAS 250 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOL. CS INY FLEXOVAL 500 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:32.79552.round(2),is_consigment:0,sale_cost:20.4972.round(2))
act = ActiveSubstance.create(name:'SOL. CS INY FLEXOVAL 500 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOL. CD INY FLEXOVL 1000 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:39.58848.round(2),is_consigment:0,sale_cost:24.7428.round(2))
act = ActiveSubstance.create(name:'SOL. CD INY FLEXOVL 1000 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOL. DX-CS INY FLEXOVAL 500 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:32.79552.round(2),is_consigment:0,sale_cost:20.4972.round(2))
act = ActiveSubstance.create(name:'SOL. DX-CS INY FLEXOVAL 500 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOL. DX-CS INY FLEXOVAL 1000 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:39.58848.round(2),is_consigment:0,sale_cost:24.7428.round(2))
act = ActiveSubstance.create(name:'SOL. DX-CS INY FLEXOVAL 1000 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOL.HT INY. FLEXOVAL 500 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:32.79552.round(2),is_consigment:0,sale_cost:20.4972.round(2))
act = ActiveSubstance.create(name:'SOL.HT INY. FLEXOVAL 500 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOLUCION HT INY FLEXOVAL 1000 ML ',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:39.58848.round(2),is_consigment:0,sale_cost:24.7428.round(2))
act = ActiveSubstance.create(name:'SOLUCION HT INY FLEXOVAL 1000 ML ')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOL. DX-5 INY FLEXOVAL 500 ML ',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:32.79552.round(2),is_consigment:0,sale_cost:20.4972.round(2))
act = ActiveSubstance.create(name:'SOL. DX-5 INY FLEXOVAL 500 ML ')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOL. DX-5 INY FLEXOVAL 1000 ML ',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:39.58848.round(2),is_consigment:0,sale_cost:24.7428.round(2))
act = ActiveSubstance.create(name:'SOL. DX-5 INY FLEXOVAL 1000 ML ')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'PISACAINA 1% INY FCO AMP 50 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:55.55008.round(2),is_consigment:0,sale_cost:34.7188.round(2))
act = ActiveSubstance.create(name:'PISACAINA 1% INY FCO AMP 50 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'PISACAINA 2% INY FCO AMP 50 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:51.18848.round(2),is_consigment:0,sale_cost:31.9928.round(2))
act = ActiveSubstance.create(name:'PISACAINA 2% INY FCO AMP 50 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'PISACAINA 2% C/EPINEFRINA 50 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:51.4112.round(2),is_consigment:0,sale_cost:32.132.round(2))
act = ActiveSubstance.create(name:'PISACAINA 2% C/EPINEFRINA 50 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'PISACAINA 2% 10 ML AMP PLA C/10',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:93.48672.round(2),is_consigment:0,sale_cost:58.4292.round(2))
act = ActiveSubstance.create(name:'PISACAINA 2% 10 ML AMP PLA C/10')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'ONEMER 30 MG/ML 1 ML C/3AMP 1 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:133.3536.round(2),is_consigment:0,sale_cost:83.346.round(2))
act = ActiveSubstance.create(name:'ONEMER 30 MG/ML 1 ML C/3AMP 1 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'CAPIN BH S.I. 20 MG/MLC/3AMP 1 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:84.18816.round(2),is_consigment:0,sale_cost:52.6176.round(2))
act = ActiveSubstance.create(name:'CAPIN BH S.I. 20 MG/MLC/3AMP 1 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOLUCION CS INY 100 ML MINIOVAL',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:22.38336.round(2),is_consigment:0,sale_cost:13.9896.round(2))
act = ActiveSubstance.create(name:'SOLUCION CS INY 100 ML MINIOVAL')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'INHEPAR 1000 UL INY F AMP 10 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:92.00192.round(2),is_consigment:0,sale_cost:57.5012.round(2))
act = ActiveSubstance.create(name:'INHEPAR 1000 UL INY F AMP 10 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'INHEPAR 5000 UL INY F AMP 10 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:256.83328.round(2),is_consigment:0,sale_cost:160.5208.round(2))
act = ActiveSubstance.create(name:'INHEPAR 5000 UL INY F AMP 10 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'PRAMOTIL 10 MG INY C/6AMP 2 ML',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:45.86176.round(2),is_consigment:0,sale_cost:28.6636.round(2))
act = ActiveSubstance.create(name:'PRAMOTIL 10 MG INY C/6AMP 2 ML')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'HENEXAL SOL. INY 2 ml c/5 AMPL',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:83.52.round(2),is_consigment:0,sale_cost:52.2.round(2))
act = ActiveSubstance.create(name:'HENEXAL SOL. INY 2 ml c/5 AMPL')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'RANULIN 50 MG 2 MLS.I. AMP C/5',presentation:'PASTILLAS',contraindication:'ninguna conocida',cost:45.41632.round(2),is_consigment:0,sale_cost:28.3852.round(2))
act = ActiveSubstance.create(name:'RANULIN 50 MG 2 MLS.I. AMP C/5')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'PENTREN 40 MG P/LIOF.SI 1X1 PI',presentation:'PASTILLAS',contraindication:'ninguna conocida',cost:222.72.round(2),is_consigment:0,sale_cost:139.2.round(2))
act = ActiveSubstance.create(name:'PENTREN 40 MG P/LIOF.SI 1X1 PI')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'ONEMER 10 MG C/10 Tab ',presentation:'PASTILLAS',contraindication:'ninguna conocida',cost:154.27072.round(2),is_consigment:0,sale_cost:96.4192.round(2))
act = ActiveSubstance.create(name:'ONEMER 10 MG C/10 Tab ')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'ANTIVON 8mg/4ml c/1amp',presentation:'PASTILLAS',contraindication:'ninguna conocida',cost:371.2.round(2),is_consigment:0,sale_cost:232.round(2))
act = ActiveSubstance.create(name:'ANTIVON 8mg/4ml c/1amp')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'DICLOPISA 75mg/3ml SOL.INY.3mLC/2',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:170.69632.round(2),is_consigment:0,sale_cost:106.6852.round(2))
act = ActiveSubstance.create(name:'DICLOPISA 75mg/3ml SOL.INY.3mLC/2')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SOLUCION CS 0.9% Irrig bolsa 3000 ml',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:95.62112.round(2),is_consigment:0,sale_cost:59.7632.round(2))
act = ActiveSubstance.create(name:'SOLUCION CS 0.9% Irrig bolsa 3000 ml')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'VITAFUSIN INY FCO AMP C/5ml ADU',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:151.24544.round(2),is_consigment:0,sale_cost:94.5284.round(2))
act = ActiveSubstance.create(name:'VITAFUSIN INY FCO AMP C/5ml ADU')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'ONEMER SL 30 mg C/6 tab subling',presentation:'PASTILLAS',contraindication:'ninguna conocida',cost:154.27072.round(2),is_consigment:0,sale_cost:96.4192.round(2))
act = ActiveSubstance.create(name:'ONEMER SL 30 mg C/6 tab subling')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'CEFAXONA 1g IV fco amp Y S KIT',presentation:'PASTILLAS',contraindication:'ninguna conocida',cost:426.88.round(2),is_consigment:0,sale_cost:266.8.round(2))
act = ActiveSubstance.create(name:'CEFAXONA 1g IV fco amp Y S KIT')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'ANTIVON 4 mg/2ml SOL INY C/1 AMP',presentation:'PASTILLAS',contraindication:'ninguna conocida',cost:278.4.round(2),is_consigment:0,sale_cost:174.round(2))
act = ActiveSubstance.create(name:'ANTIVON 4 mg/2ml SOL INY C/1 AMP')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'VINALTRO 0.25MG/5ML C/1 FCO AMP',presentation:'PASTILLAS',contraindication:'ninguna conocida',cost:3811.59296.round(2),is_consigment:0,sale_cost:2382.2456.round(2))
act = ActiveSubstance.create(name:'VINALTRO 0.25MG/5ML C/1 FCO AMP')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'DECOREX 8MG/2ML CJA AMPO 2ML SI',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:28.04416.round(2),is_consigment:0,sale_cost:17.5276.round(2))
act = ActiveSubstance.create(name:'DECOREX 8MG/2ML CJA AMPO 2ML SI')
act.medicaments << med
med = Medicament.create(medicament_laboratory_id:pisa.id,medicament_category_id:1,comercial_name:'SALPIFAR 500MG/50ML CJA C/1FCO 50 ML SI',presentation:'MILILITROS',contraindication:'ninguna conocida',cost:146.624.round(2),is_consigment:0,sale_cost:91.64.round(2))
act = ActiveSubstance.create(name:'SALPIFAR 500MG/50ML CJA C/1FCO 50 ML SI')
act.medicaments << med





