State.connection.execute("ALTER TABLE states MODIFY created_at DATETIME NULL")
State.connection.execute("ALTER TABLE states MODIFY updated_at DATETIME NULL")
State.connection.execute('ALTER TABLE states AUTO_INCREMENT = 1')

stateInserts = []

stateInserts.push "('1','Aguascalientes','158')"
stateInserts.push "('2','Baja California','158')"
stateInserts.push "('3','Baja California Sur','158')"
stateInserts.push "('4','Campeche','158')"
stateInserts.push "('5','Chiapas','158')"
stateInserts.push "('6','Chihuahua','158')"
stateInserts.push "('7','Coahuila','158')"
stateInserts.push "('8','Colima','158')"
stateInserts.push "('9','Distrito Federal','158')"
stateInserts.push "('10','Durango','158')"
stateInserts.push "('11','Guanajuato','158')"
stateInserts.push "('12','Guerrero','158')"
stateInserts.push "('13','Hidalgo','158')"
stateInserts.push "('14','Jalisco','158')"
stateInserts.push "('15','Estado de México','158')"
stateInserts.push "('16','Michoacán','158')"
stateInserts.push "('17','Morelos','158')"
stateInserts.push "('18','Nayarit','158')"
stateInserts.push "('19','Nuevo León','158')"
stateInserts.push "('20','Oaxaca','158')"
stateInserts.push "('21','Puebla','158')"
stateInserts.push "('22','Querétaro','158')"
stateInserts.push "('23','Quintana Roo','158')"
stateInserts.push "('24','San Luis Potosí','158')"
stateInserts.push "('25','Sinaloa','158')"
stateInserts.push "('26','Sonora','158')"
stateInserts.push "('27','Tabasco','158')"
stateInserts.push "('28','Tamaulipas','158')"
stateInserts.push "('29','Tlaxcala','158')"
stateInserts.push "('30','Veracruz','158')"
stateInserts.push "('31','Yucatán','158')"
stateInserts.push "('32','Zacatecas','158')"
stateInserts.push "('33','No Proporcionado','1')"
stateInserts.push "('34','California','72')"
stateInserts.push "('35','Alabama','72')"
stateInserts.push "('36','Alaska','72')"
stateInserts.push "('37','Arkansas','72')"
stateInserts.push "('38','Arizona','72')"
stateInserts.push "('39','Colorado','72')"
stateInserts.push "('40','Connecticut','72')"
stateInserts.push "('41','Dakota del Sur','72')"
stateInserts.push "('42','Delaware','72')"
stateInserts.push "('43','Florida','72')"
stateInserts.push "('44','Georgia','72')"
stateInserts.push "('45','Hawaii','72')"
stateInserts.push "('46','Idaho','72')"
stateInserts.push "('47','Illinois','72')"
stateInserts.push "('48','Indiana','72')"
stateInserts.push "('49','Iowa','72')"
stateInserts.push "('50','Kansas','72')"
stateInserts.push "('51','Kentucky','72')"
stateInserts.push "('52','Louisiana','72')"
stateInserts.push "('53','Maine','72')"
stateInserts.push "('54','Maryland','72')"
stateInserts.push "('55','Massachussetts','72')"
stateInserts.push "('56','Michigan','72')"
stateInserts.push "('57','Minnesota','72')"
stateInserts.push "('58','Mississippi','72')"
stateInserts.push "('59','Missouri','72')"
stateInserts.push "('60','Montana','72')"
stateInserts.push "('61','Nebraska','72')"
stateInserts.push "('62','Nevada','72')"
stateInserts.push "('63','New Hampshire','72')"
stateInserts.push "('64','New Jersey','72')"
stateInserts.push "('65','New Mexico','72')"
stateInserts.push "('66','New York','72')"
stateInserts.push "('67','North Carolina','72')"
stateInserts.push "('68','North Dakota','72')"
stateInserts.push "('69','Ohio','72')"
stateInserts.push "('70','Oklahoma','72')"
stateInserts.push "('71','Oregon','72')"
stateInserts.push "('72','Pennsylvania','72')"
stateInserts.push "('73','Rhode Island','72')"
stateInserts.push "('74','South Carolina','72')"
stateInserts.push "('75','Tennessee','72')"
stateInserts.push "('76','Texas','72')"
stateInserts.push "('77','Utah','72')"
stateInserts.push "('78','Vermont','72')"
stateInserts.push "('79','Virginia','72')"
stateInserts.push "('80','Washington','72')"
stateInserts.push "('81','West Virginia','72')"
stateInserts.push "('82','Wisconsin','72')"
stateInserts.push "('83','Wyoming','72')"


sqlState = "INSERT INTO states (`id`, `name`, `country_id`) VALUES #{stateInserts.join(", ")}"
State.connection.execute(sqlState)


State.connection.execute("UPDATE states SET created_at='"+Time.now.to_formatted_s(:db)+"'")
State.connection.execute("UPDATE states SET updated_at='"+Time.now.to_formatted_s(:db)+"'")
State.connection.execute("ALTER TABLE states MODIFY created_at DATETIME NOT NULL")
State.connection.execute("ALTER TABLE states MODIFY updated_at DATETIME NOT NULL")