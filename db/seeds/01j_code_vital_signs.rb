codes = [
    ["weight","PESO","Kg"],
    ["height","ALTURA","Cm"],
    ["temperature","TEMPERATURA","°C"],
    ["blood_pressure","TA","mmHg"],
    ["glucose","GLUCOSA","mg/dl"],
    ["imc","IMC","kg/m²"],
    ["s_weight","PESO_SUGERIDO","Kg"],
    ["head_circumference","PERIMETRO_ENCEFALICO",""],
    ["heart_rate","LPM",""],
    ["breathing_frequency","rpm",""],
    ["eco_rctg","ECO_RCTG",""],
    ["fu","FU",""],
    ["pres","PRES",""],
    ["ff","FF",""],
    ["vdrl","VDRL",""],
    ["ego","EGO",""],
    ["other","OTROS",""],
    ["oxygen_saturation","SATURACION_OXIGENO",""],
]

codes.each do |c|
  field = ValorationField.where('f_key = ?',c[0]).first

  if field.present?
    field.update(code:c[1], sufix: c[2])
  end
end