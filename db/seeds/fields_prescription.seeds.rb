campo = FieldPrescriptionType.create(name:"Campo")
etiqueta = FieldPrescriptionType.create(name:"Etiqueta")
forma = FieldPrescriptionType.create(name:"Forma")
imagen = FieldPrescriptionType.create(name:"Imagen")

FieldsPrescription.create(id:FieldsPrescription::DOCTOR_NAME , name: "Nombre del médico",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::OFFICE_NAME_DOCTOR , name: "Nombre del consultorio",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::ADDRESS_OFFICE_DOCTOR , name: "Dirección del consultorio",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::PHONE_OFFICE , name: "Télefono del consultorio",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::CELL_DOCTOR , name: "Celular del médico",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::MEDICAL_SPECIALTIES , name: "Especialidades médicas",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::ACADEMIC_STUDIES , name: "Estudios académicos",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::PROFESSIONAL_IDENTITY_CARD , name: "Cédula profesional",field_prescription_type_id:campo.id)

FieldsPrescription.create(id:FieldsPrescription::PATIENT_NAME , name: "Nombre de paciente",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::AGE_PATIENT , name: "Edad de paciente",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::DATE_CONSULTATION , name: "Fecha de consulta",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::DIAGNOSTIC_CONSULTATION , name: "Diagnostico de la consulta",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::MEDICAL_TREATMENT , name: "Tratamiendo de la consulta",field_prescription_type_id:campo.id)

FieldsPrescription.create(id:FieldsPrescription::BINDING_CODE_EXPEDIENT , name: "Código de vinculación",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::QR_CODE_EXPEDIENT , name: "Código Qr",field_prescription_type_id:campo.id)

FieldsPrescription.create(id:FieldsPrescription::FOLIO_CONSULTATION , name: "Folio de consulta",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::WEIGHT_PATIENT , name: "Peso de paciente",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::HEIGHT_PATIENT , name: "Talla de paciente",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::LOGO_OFFICE , name: "Logo del consultorio",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::EXTRA_DATA , name: "Datos extras",field_prescription_type_id:campo.id)
FieldsPrescription.create(id:FieldsPrescription::CRANIAL_PERIMETER , name: "Perímetro craneal",field_prescription_type_id:campo.id)

FieldsPrescription.create(id:FieldsPrescription::LABEL , name: "Etiqueta",field_prescription_type_id:etiqueta.id)
FieldsPrescription.create(id:FieldsPrescription::LINE , name: "Linea",field_prescription_type_id:forma.id)

FieldsPrescription.create(id:FieldsPrescription::SQUARE , name: "Cuadrado",field_prescription_type_id:forma.id)
FieldsPrescription.create(id:FieldsPrescription::RECTANGLE , name: "Rectangulo",field_prescription_type_id:forma.id)
FieldsPrescription.create(id:FieldsPrescription::CIRCLE , name: "Circulo",field_prescription_type_id:forma.id)
FieldsPrescription.create(id:FieldsPrescription::IMAGE , name: "Imagen",field_prescription_type_id:imagen.id)


FieldsPrescription.create(name:"Etiqueta",field_prescription_type_id:2)
FieldsPrescription.create(name:"Línea",field_prescription_type_id:3)
FieldsPrescription.create(name:"Cuadrado",field_prescription_type_id:3)
FieldsPrescription.create(name:"Rectangulo",field_prescription_type_id:3)
FieldsPrescription.create(name:"Circulo",field_prescription_type_id:3)
FieldsPrescription.create(name:"Imagen",field_prescription_type_id:4)

