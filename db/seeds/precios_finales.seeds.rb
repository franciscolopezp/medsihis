act = ActiveSubstance.create(name:"No especificado")
med = Medicament.create(code:'4000383',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'HESTAR 8G/100ML FLEXOVAL 500ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:533.7216.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4003208',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'INSULEX R(INSU.HUMAN.)FCOC/10ML.X100UI/ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:475.6.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4006739',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'ROPICONEST S.I. 2MG/ML 5 AMP PLAS20ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:709.8412.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4006740',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'ROPICONEST S.I. 7.5MG/ML 5 AMP PLAS20ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:1214.1904.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4002463',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'QUIRODREN 1/8 (3MM)',presentation:'No especificado',contraindication:'ninguna conocida',cost:224.5488.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4002465',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'QUIRODREN 1/4 (6MM)',presentation:'No especificado',contraindication:'ninguna conocida',cost:212.5768.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4003039',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'AGUA INYEC. SOL.INY.500ML IRRIGASTERIL',presentation:'No especificado',contraindication:'ninguna conocida',cost:46.2644.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4007670',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'SOL.CS 0.9% 500ML PLA',presentation:'No especificado',contraindication:'ninguna conocida',cost:42.6892.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000176',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'INDAMID 300MG/2ML C/1AMP',presentation:'No especificado',contraindication:'ninguna conocida',cost:97.3012.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000398',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'EPICLODINAO. 15MG/ML AMP. 1MLC/5',presentation:'No especificado',contraindication:'ninguna conocida',cost:224.5488.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4014254',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'BOLENTAX 4OMG SIENV/2JGA 0.4ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:724.224.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4031367',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'SALPIFAR 1G/100ML CJA/1FCO 100 ML SI',presentation:'No especificado',contraindication:'ninguna conocida',cost:394.338.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4039037',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'SALPIFAR 500MG/50ML CJA C/1FCO 50 ML SI',presentation:'No especificado',contraindication:'ninguna conocida',cost:311.3704.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4006629',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'PISAPEM 500 MG SOL. INY. C /DILUY',presentation:'No especificado',contraindication:'ninguna conocida',cost:66.1084.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4006631',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'PISAPEM 1G SOL. INY. C/DILUY',presentation:'No especificado',contraindication:'ninguna conocida',cost:129.2812.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4006647',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'CIPROBAC 500MG C/14 TABLETAS',presentation:'No especificado',contraindication:'ninguna conocida',cost:1056.0452.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4027521',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'PIXIRIV 160MG/100ML FCO C/250 ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:140.0724.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4008516',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'FLUCOXAN S.l. 2MG/ML, 50ML C/1 MINIOVAL',presentation:'No especificado',contraindication:'ninguna conocida',cost:728.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4021823',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'OMEPRAZOL 40 MG POLVO LIO SI GENEPISA',presentation:'No especificado',contraindication:'ninguna conocida',cost:252.7732.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000397',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'INHEPAR 100 Ul INY.F.AMP.10ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:81.2948.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000400',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'INHEPAR 5000 Ul INY.F.AMP.10 ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:226.9432.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000203',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'PENCLOX 500 MMG INY.FCO.AMP Y S',presentation:'No especificado',contraindication:'ninguna conocida',cost:142.5488.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000954',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'FLUONING 500 MG SOL.INY.FLEXO 100 ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:1126.9424.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000954',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'FLUONING 759 MG SOL. INY.FLEXO 159 ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:1502.486.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000173',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'AMK 100 MG INY. 1 AMP. 2 ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:153.996.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000174',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'AMK 500 MG INY. 1 AMP. 2 ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:422.4968.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4027988',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'PINADRINA 1MG/ML FCO AMP C/50',presentation:'No especificado',contraindication:'ninguna conocida',cost:11.644.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000181',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'CIPROBAC 200MG/100MlSI.MINIOVAL',presentation:'No especificado',contraindication:'ninguna conocida',cost:746.3312.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4038241',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'CLORHEXI-DERM 2% ENV C/40ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:157.44.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4038247',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'CLORHEXI-CLEAN 0.12% ENV C/60ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:47.56.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4038248',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'CLORHEXI-RUB 0.5% ENV C/500ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:360.8.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000077',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'SOL. CS INY. PLA 250 ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:22.9764.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000324',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'ONEMER 30 MG/ML 1ML C/3 AMP',presentation:'No especificado',contraindication:'ninguna conocida',cost:117.834.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000345',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'CAPIN BH S.l. 20 MG/MLC/3AMP1ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:74.3904.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000475',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'PENTREN 40 MG P/LIOF.Sl 1X1 Pl',presentation:'No especificado',contraindication:'ninguna conocida',cost:196.8.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000954',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'IMATION 1G SOL.INY. FCO C/DILUY 10ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:742.428.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000954',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'SUPACID 40 MG SI CJA C/1 FCO AMP Y DIL 7G',presentation:'No especificado',contraindication:'ninguna conocida',cost:467.5804.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4012969',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'SUPACID 40 MG SI CJA C/1 FCO AMP Y DIL 14G',presentation:'No especificado',contraindication:'ninguna conocida',cost:926.7804.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000468',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'RANULIN 50MG/2MLS.I. AMP.C/5',presentation:'No especificado',contraindication:'ninguna conocida',cost:40.1308.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000465',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'HENEXAL SOL.INY 2 ML C/5AMPL',presentation:'No especificado',contraindication:'ninguna conocida',cost:73.8.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000394',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'SOLUCION CS INY 100 ML MINIOVAL',presentation:'No especificado',contraindication:'ninguna conocida',cost:19.7784.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000464',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'PRAMOTIL 10 MG INY. C/6AMP. 2 ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:40.5244.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4027557',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'VINALTRO 0.25MG/5ML/1FCO AMP',presentation:'No especificado',contraindication:'ninguna conocida',cost:3368.0024.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4031367',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'SALPIFAR 1G/100ML CJA/1FCO 100 ML SI',presentation:'No especificado',contraindication:'ninguna conocida',cost:160.72.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4012818',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'ANTIVON 4MG/2ML SOL. INY C/1 AMP',presentation:'No especificado',contraindication:'ninguna conocida',cost:246.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4011553',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'CEAXONA 1G Lv FCO. AMP. Y S. KIT',presentation:'No especificado',contraindication:'ninguna conocida',cost:377.2.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4009649',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'ONEMER SL 30MG C/6 TAB SUBLING',presentation:'No especificado',contraindication:'ninguna conocida',cost:136.3168.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000954',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'CEFTRIAXONA 1G IV PVO/INY. GE',presentation:'No especificado',contraindication:'ninguna conocida',cost:1068.296.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4000190',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'VANAURUS 500MG FCO. 10ML Pl',presentation:'No especificado',contraindication:'ninguna conocida',cost:599.0264.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4006626',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'IMATION 500 MG SOL.INY. FCOC.DILUY. 5ML',presentation:'No especificado',contraindication:'ninguna conocida',cost:387.8928.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4002924',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'ONEMER 10MG C/10TAB',presentation:'No especificado',contraindication:'ninguna conocida',cost:136.3168.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4003338',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'ANTIVON 8MG/4ML C/1AMP',presentation:'No especificado',contraindication:'ninguna conocida',cost:328.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4003700',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'DICLOPISA 75MG/3ML SOL.INY.3MLC/2',presentation:'No especificado',contraindication:'ninguna conocida',cost:150.8308.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4007579',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'VITAFUSIN INY FCO AMP C/5ML ADU',presentation:'No especificado',contraindication:'ninguna conocida',cost:133.6436.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4032901',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'DECOREX 8MG/2ML CJA 2ML SI',presentation:'No especificado',contraindication:'ninguna conocida',cost:24.7804.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end
med = Medicament.create(code:'4039037',medicament_laboratory_id:1,medicament_category_id:1,comercial_name:'SALPIFAR 500MG CJA C/1FCO 50 ML SI',presentation:'No especificado',contraindication:'ninguna conocida',cost:129.56.round(2),is_consigment:0,sale_cost:0)
act.medicaments << med
new_lot = Lot.new(name:med.comercial_name,expiration_date:Date.today,medicament_id:med.id,user_id:1)
if new_lot.save
  MedicamentInventory.new(warehouse_id:1,lot_id:new_lot.id,quantity:100,user_id:1).save
end

item = PharmacyItem.create(code:'4006800',description:'TAPON GIRATEK P/SITIO DE INYECCION',measure_unit_item_id:1,medicament_category_id:1,cost:12.546.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000687',description:'GUANTES QX 7 LATEX  NAT ESTER UNISEAL',measure_unit_item_id:1,medicament_category_id:1,cost:10.824.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000686',description:'GUANTES QX 6.5 LATEX NAT ESTER UNISEAL',measure_unit_item_id:1,medicament_category_id:1,cost:10.824.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000688',description:'GUANTES QX 7.5 LATEX NAT ESTER UNISEAL',measure_unit_item_id:1,medicament_category_id:1,cost:10.824.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000689',description:'GUANTES QX 8 LATEX NAT ESTER UNISEAL',measure_unit_item_id:1,medicament_category_id:1,cost:10.824.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000847',description:'IRRIGATEK 2 VIAS P/BOLSA',measure_unit_item_id:1,medicament_category_id:1,cost:160.1132.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000662',description:'ESP D/GASA 7.5X5CM 12C T20X12',measure_unit_item_id:1,medicament_category_id:1,cost:10.824.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000255',description:'AGUA ESTERIL PLASTICO 500 ML IRRIGADUAL',measure_unit_item_id:1,medicament_category_id:1,cost:36.1456.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000258',description:'AGUA ESTERIL PLASTICO 1000ML IRRIGADUAL',measure_unit_item_id:1,medicament_category_id:1,cost:41.3116.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000261',description:'AGUA ESTERILIZADA P/IRRIG. 3LT',measure_unit_item_id:1,medicament_category_id:1,cost:89.216.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000669',description:'ESP. D/GASA10X10CM 12 CR T20X12 ESTE C/10',measure_unit_item_id:1,medicament_category_id:1,cost:24.928.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000670',description:'ESP D/GASA 10X10CM T20X12 ESTERIL C/5PZ',measure_unit_item_id:1,medicament_category_id:1,cost:13.94.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000662',description:'ESP D/GASA 7.5X5CM 12C T20X12 ESTE C/5PZ',measure_unit_item_id:1,medicament_category_id:1,cost:10.824.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000686',description:'GUANTES QX 6.5 LATEX NAT ESTER UNISEAL',measure_unit_item_id:1,medicament_category_id:1,cost:10.824.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000687',description:'GUANTES QX 7 LATEX NAT ESTER UNISEAL',measure_unit_item_id:1,medicament_category_id:1,cost:10.824.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000688',description:'GUANTES QX 7.5 LATEX NAT ESTER UNISEAL',measure_unit_item_id:1,medicament_category_id:1,cost:10.824.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000689',description:'GUANTES QX8 LATEX NAT ESTER UNISEAL',measure_unit_item_id:1,medicament_category_id:1,cost:10.824.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000488',description:'FLEBOTEK CON AGUJA EQ. VENOCLISIS ',measure_unit_item_id:1,medicament_category_id:1,cost:34.44.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000717',description:'KIT-QX PAQUETE QUIRURGICO UNIVERSAL',measure_unit_item_id:1,medicament_category_id:1,cost:795.4.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000718',description:'KIT-QX PAQUETE QUIRURGICO BASICO',measure_unit_item_id:1,medicament_category_id:1,cost:634.68.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000722',description:'KIT-QX PAQUETE QUIRURGICO CABEZA Y CUE',measure_unit_item_id:1,medicament_category_id:1,cost:636.32.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000698',description:'BATA DESECHABLE NO REFORZADA ESTERIL',measure_unit_item_id:1,medicament_category_id:1,cost:162.36.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4003304',description:'AGUJA PORT-A-SITE 20G X 19 MM',measure_unit_item_id:1,medicament_category_id:1,cost:116.44.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4003305',description:'AGUJA PORT-A-SITE 22G X 19 MM',measure_unit_item_id:1,medicament_category_id:1,cost:116.44.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4003307',description:'AGUJA OIRT-A-SITE 20G X 25MM',measure_unit_item_id:1,medicament_category_id:1,cost:116.44.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4013735',description:'VEINCAT 14 G CATETER PERIFERICO',measure_unit_item_id:1,medicament_category_id:1,cost:13.94.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4013736',description:'VEINCAT 16 G CATETER PERIFERICO',measure_unit_item_id:1,medicament_category_id:1,cost:13.94.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4013737',description:'VEINCAT 18 G CATETER PERIFERICO',measure_unit_item_id:1,medicament_category_id:1,cost:13.94.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4013738',description:'VEINCAT 20 G CATETER PERIFRICO',measure_unit_item_id:1,medicament_category_id:1,cost:13.94.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4013739',description:'VEINCAT 22 G CATETER PERIFERICO',measure_unit_item_id:1,medicament_category_id:1,cost:13.94.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4013740',description:'VEINCAT 24 G CATETER PERIFERICO',measure_unit_item_id:1,medicament_category_id:1,cost:13.94.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000686',description:'GUANTES QUIRUR 6.5 LATEX NATURAL ESTER',measure_unit_item_id:1,medicament_category_id:1,cost:24.436.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000688',description:'GUANTES QUIRUR 7.5 LATEX NATURAL ESTER',measure_unit_item_id:1,medicament_category_id:1,cost:24.436.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000689',description:'GUANTES QUIRUR 8BLATEX NATURAL ESTER',measure_unit_item_id:1,medicament_category_id:1,cost:24.436.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000698',description:'BATA DESECHABLE NO REFORZADA ESTERIL',measure_unit_item_id:1,medicament_category_id:1,cost:365.2444.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4039484',description:'EMULATING INDICATOR 7-A BOLSA C/250PZA',measure_unit_item_id:1,medicament_category_id:1,cost:1206.876.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4039489',description:'GREEN TAPE 18 MM STEAM BOLSA C/1 PZA',measure_unit_item_id:1,medicament_category_id:1,cost:182.86.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4039490',description:'TAPE 24MM EO BOLSA C/A PZA',measure_unit_item_id:1,medicament_category_id:1,cost:259.94.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4039491',description:'TAPE 18MM EO BOLSA C/1 PZA',measure_unit_item_id:1,medicament_category_id:1,cost:195.16.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4008950',description:'BATA DESECHABLE REFORZADA ESTERIL T/XL',measure_unit_item_id:1,medicament_category_id:1,cost:154.078.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4006265',description:'CAMPO QUIRURG DESECHAB C/APZA ESTERIL',measure_unit_item_id:1,medicament_category_id:1,cost:75.4072.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4006697',description:'SOLUCION CS 0.9%, IRRIG BOLSA 3000ML',measure_unit_item_id:1,medicament_category_id:1,cost:84.4928.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4001877',description:'KIT-QX PAQ QUIRURP/ASTROSCOPIA',measure_unit_item_id:1,medicament_category_id:1,cost:1680.3276.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4001878',description:'KIT-QX PAQ QUIRURGP/ORTOPEDIA',measure_unit_item_id:1,medicament_category_id:1,cost:993.3972.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4001969',description:'FLEBOTEK QUIRURGICO NB CON CLAVE',measure_unit_item_id:1,medicament_category_id:1,cost:113.898.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4002759',description:'FLEBOTEK CON CLAVE PARA BOMBA',measure_unit_item_id:1,medicament_category_id:1,cost:213.2.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4002760',description:'FLEBOTEK 0-150 CON CLAVE PARA BOMBA',measure_unit_item_id:1,medicament_category_id:1,cost:254.2.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000717',description:'KIT-QX PAQUETE QUIRURGICO UNIVERSAL',measure_unit_item_id:1,medicament_category_id:1,cost:1800.72.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000313',description:'PISACAINA 1% INY.FCO.AMP.50 ML',measure_unit_item_id:1,medicament_category_id:1,cost:48.1012.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000316',description:'PISACAINA 2% INY.FCO.AMP.50 ML',measure_unit_item_id:1,medicament_category_id:1,cost:45.2312.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000317',description:'PISACAINA 2% C/EPINEFRINA 50 ML',measure_unit_item_id:1,medicament_category_id:1,cost:45.428.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000319',description:'PISACAINA 2% 10ML AMP.PLA.C/10',measure_unit_item_id:1,medicament_category_id:1,cost:82.6068.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000078',description:'SOL. CS INY FLEXOVAL 500 ML',measure_unit_item_id:1,medicament_category_id:1,cost:28.9788.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000079',description:'SOL. CS FLEXOVAL 1000ML',measure_unit_item_id:1,medicament_category_id:1,cost:34.9812.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000082',description:'SOL. DX-CS FLEXOVAL 500 ML',measure_unit_item_id:1,medicament_category_id:1,cost:28.9788.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000083',description:'SOL. DX-CS INY. FLEXOVAL 1000ML',measure_unit_item_id:1,medicament_category_id:1,cost:34.9812.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000088',description:'SOL.HT INY. FLEXOVAL 500 ML',measure_unit_item_id:1,medicament_category_id:1,cost:28.9788.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000089',description:'SOLUCION HT INY. FLEXOVAL 100ML',measure_unit_item_id:1,medicament_category_id:1,cost:34.9812.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000013',description:'SOL.DX5 INY FLEXOVAL 500 ML',measure_unit_item_id:1,medicament_category_id:1,cost:28.9788.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000104',description:'SOL. DX-5 FLEXOVAL 100ML',measure_unit_item_id:1,medicament_category_id:1,cost:34.9812.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000741',description:'CONECTOR CLC 2000 CON FLUSH',measure_unit_item_id:1,medicament_category_id:1,cost:202.7532.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000730',description:'ENTEROBAG B BOLSA DE 500 ML P/BOMBA D/INF',measure_unit_item_id:1,medicament_category_id:1,cost:114.8.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000767',description:'PORT-A-CATCH ll 7.8FR POLIUR. INTR.8.5FR',measure_unit_item_id:1,medicament_category_id:1,cost:8737.92.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000768',description:'PORT-A-CATH ll 8.4FR SILICON INTR.9FR',measure_unit_item_id:1,medicament_category_id:1,cost:8737.92.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000769',description:'PORT-A-CATCH 5.8FR B PERF INTR. 6FR',measure_unit_item_id:1,medicament_category_id:1,cost:8737.92.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4003549',description:'GIRATEK LLAVE DE TRES VIAS',measure_unit_item_id:1,medicament_category_id:1,cost:17.2528.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4003552',description:'GIRATEK C/EXTENSION 50 CM',measure_unit_item_id:1,medicament_category_id:1,cost:30.2088.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000178',description:'IKATIN 80 MG INY.1 AMP. 2ML',measure_unit_item_id:1,medicament_category_id:1,cost:136.6284.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4014227',description:'MASCARILLA QUIRURG ALT FILT CJA C/50 PZS',measure_unit_item_id:1,medicament_category_id:1,cost:114.1768.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4023633',description:'MICROCLAVE CLEAR C/EXT 13 CM MICRO 2 VIAS',measure_unit_item_id:1,medicament_category_id:1,cost:138.1536.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000909',description:'INFUSOR 125ML 5ML/H HOMEPUMPC',measure_unit_item_id:1,medicament_category_id:1,cost:683.1748.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000480',description:'FLEBOTEK MICROGOTERO EQ. VENOCLISIS',measure_unit_item_id:1,medicament_category_id:1,cost:35.8996.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000484',description:'FLEBOTEK QUIRURGICO NB EQ. VENOCLISIS',measure_unit_item_id:1,medicament_category_id:1,cost:31.16.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000954',description:'ARZOMEBA 500/500MGPVO.SI',measure_unit_item_id:1,medicament_category_id:1,cost:1283.7428.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000954',description:'CIPROBAC400MG/200MlSI.PLAS',measure_unit_item_id:1,medicament_category_id:1,cost:1171.0748.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000663',description:'CP/VIENT PRELAV 45X70CM T20X24 E C/5 PZ',measure_unit_item_id:1,medicament_category_id:1,cost:149.24.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000680',description:'ESP D/GAS 10X10CM 12 BISNEC/200PZAS',measure_unit_item_id:1,medicament_category_id:1,cost:165.968.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000791',description:'UREOTEK 2 LTB BSA RECUPERACION DE ORINA',measure_unit_item_id:1,medicament_category_id:1,cost:78.474.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000908',description:'INFUSOR 100ML /H HOMEPUMP C',measure_unit_item_id:1,medicament_category_id:1,cost:502.66.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000924',description:'FRYHAND 500ML UNA PIEZA',measure_unit_item_id:1,medicament_category_id:1,cost:176.054.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000927',description:'ENDOZIME AW PLUS GALON 4000 ML',measure_unit_item_id:1,medicament_category_id:1,cost:176.054.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000928',description:'SURGISTAIN GALON 4,000 ML',measure_unit_item_id:1,medicament_category_id:1,cost:4018.738.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4000929',description:'PREMIXSLIP GALON 4,000 ML',measure_unit_item_id:1,medicament_category_id:1,cost:2285.6516.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4031209',description:'MICROCLAVE CLEAR ',measure_unit_item_id:1,medicament_category_id:1,cost:87.9204.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4039476',description:'GETINGE BOWIE-DICK MINI NO TURBO C/30 PZA',measure_unit_item_id:1,medicament_category_id:1,cost:2337.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4003309',description:'FLEBOTEK OPACO PARA BOMBA',measure_unit_item_id:1,medicament_category_id:1,cost:213.2.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4036587',description:'MICROCLAVE CLEAR C/EXT17CM MIC ANT 3 VIAS',measure_unit_item_id:1,medicament_category_id:1,cost:122.9508.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200600925-PAQ',description:'2239 RED DOT (MICROPORE-ADULTO ELECTRODOS ADULTO C/50 3M',measure_unit_item_id:1,medicament_category_id:1,cost:458.0684.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200718560-CJA',description:'9200 AVAGARD CHG C/8 500 ML',measure_unit_item_id:1,medicament_category_id:1,cost:13712.614.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'MH900100222-CJA',description:'9222 AVAGARD D ANTISEPTICO INSTANTNEO PMANOS 500 ML',measure_unit_item_id:1,medicament_category_id:1,cost:5279.9144.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200745340',description:'6650 IOBAN ANTIMICROBIAL INCISE STERILE C/10-3M',measure_unit_item_id:1,medicament_category_id:1,cost:3131.006.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200739772-C2',description:'8630 DURAPREP APLICCADOR 26ML C/20-3M',measure_unit_item_id:1,medicament_category_id:1,cost:7216.8528.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'700200534736-CJA',description:'15331 MICROPORE SKINTONE 1-C/12 3M',measure_unit_item_id:1,medicament_category_id:1,cost:483.8656.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'700200534744-CJA',description:'15332 MICROPORE SKINTONE 2-C/16 3M',measure_unit_item_id:1,medicament_category_id:1,cost:483.8656.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200729203-CJA',description:'3584 APOSITOS TEGADERM +PAD 6CM X 10CM-C/50 3M',measure_unit_item_id:1,medicament_category_id:1,cost:2435.4.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'3586-CJA50',description:'TEGADERM + PAD 3586 9X10CM C/50-3M',measure_unit_item_id:1,medicament_category_id:1,cost:1918.0948.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200784448',description:'1680 TEGADERM IVADVANCED 3.8CM X45CMC/100 PZAS',measure_unit_item_id:1,medicament_category_id:1,cost:2371.0792.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'MH900100453 CJA',description:'1633MX TEGADERM-APOSITO 7X8.5CM C/50 3M',measure_unit_item_id:1,medicament_category_id:1,cost:1959.8.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'JH200148320 CJA',description:'J82003 SCOTCHCAST PREMIUM C/10-3M',measure_unit_item_id:1,medicament_category_id:1,cost:1902.1048.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'JH200148338-CJA',description:'J82004 SCOTCHCAST PREMIUM C/10 3M',measure_unit_item_id:1,medicament_category_id:1,cost:2301.0512.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'JH200148346-CJA',description:'J82005 SCOTCHCAST PREMIUM C/10-3M',measure_unit_item_id:1,medicament_category_id:1,cost:2873.3456.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200473018-ROI',description:'MS03 STOCKINETETM C/1 ROLLO 7.5CM X 22.8 M-3M',measure_unit_item_id:1,medicament_category_id:1,cost:1160.3492.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200473026-CJA ',description:'MS04 STOCKINETETM C/1 ROLLO 10 CM X 22.8M -3M',measure_unit_item_id:1,medicament_category_id:1,cost:1436.476.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200712472-CJA',description:'CMW03 CAST PADDINGTM 7.5CM X 3.65M C/20 3M',measure_unit_item_id:1,medicament_category_id:1,cost:1540.1568.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70-2007-7498-7',description:'2455 ESTETOSCOPIO LIGHTWEIGHT LITTMANN',measure_unit_item_id:1,medicament_category_id:1,cost:2905.6864.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200681503',description:'2208 CLASSIC II VERDE-3M',measure_unit_item_id:1,medicament_category_id:1,cost:4051.702.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200749276-CJA',description:'1624W TEGADERM FILM TRANSPARENT C/100 3M',measure_unit_item_id:1,medicament_category_id:1,cost:1536.024.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001412',description:'SONDA NELATON LATEX LONG. 40 CM CAL 12 FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:26.1416.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001414',description:'SONDA NELATON LATEX LONG.40 CM CAL 14 FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:26.1416.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001416',description:'SONDA NELATON LATEX LONG.40 CM CAL 16 FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:26.1416.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001418',description:'SONDA NELATON LATEX LONG.40 CM CAL 18 FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:26.1416.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'CTPEST',description:'VASO COPRO TAPA ROSCA ESTERIL DE 100ML-SYM',measure_unit_item_id:1,medicament_category_id:1,cost:5.6416.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'35.1',description:'LUBRI-6 135 GR-ALTAMIRA',measure_unit_item_id:1,medicament_category_id:1,cost:55.3008.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4980-250 ',description:'MATRAZ ERLENMEYER GRAD 250 ML PYREX',measure_unit_item_id:1,medicament_category_id:1,cost:218.12.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4980-500',description:'MATRAZ ERLENMEYER GRADUADO 500 ML.-PYREX',measure_unit_item_id:1,medicament_category_id:1,cost:273.88.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4980-1000',description:'MATRAZ ERLENMEYER GRAD.CAP. 1000 ML',measure_unit_item_id:1,medicament_category_id:1,cost:459.2.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'FLORENCE-01',description:'CUBREBOCAS PARA USO EN AREA HOSPITALARIA C/150 FLORENCE',measure_unit_item_id:1,medicament_category_id:1,cost:94.4476.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'10417',description:'ESPONJAS DE GASA LE ROY 5X5 TEJIDO 20X12 PAQUETE C/200PZA',measure_unit_item_id:1,medicament_category_id:1,cost:62.156.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'10438',description:'ESPONJA DE GASA LEROY C/TRAMA OPACA 10X10 TEJIDO 20X12 PAQUETE C/200 PZA',measure_unit_item_id:1,medicament_category_id:1,cost:196.8.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'K-729',description:'CATETER P/SUMINISTRO DE OXIGENO NASAL',measure_unit_item_id:1,medicament_category_id:1,cost:20.1228.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'K-759',description:'CATETER P/SUCCION DE FLEMAS C/VALVULA FR.12 LONG 55.0CM TROKAR',measure_unit_item_id:1,medicament_category_id:1,cost:18.614.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'K-760',description:'CATETER P/SUCCION DE FLEMAS C/VALVULA 14FR LONG 55 CM-KORTEX',measure_unit_item_id:1,medicament_category_id:1,cost:18.614.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'K-762',description:'CATETER PARA SUCCION DE FLEMAS CON VALVULA 18 FR LONG.55.0CM -TROKAR',measure_unit_item_id:1,medicament_category_id:1,cost:13.5792.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'LMT003',description:'BANCO GIRATORIO CROMADO SIN RODAJA TUBO 1 CAZAG',measure_unit_item_id:1,medicament_category_id:1,cost:1160.0212.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'LMT023',description:'ESCALERILLA DE DOS PELDAÑOS TUBO REDONDO C/HULE ASTRIADO CAZAGUA',measure_unit_item_id:1,medicament_category_id:1,cost:1544.88.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'LRO-120-200R',description:'BOLSA DE POLIETILENO ROJA DE 90X120CM PARA RECOLECCION DE RESIDUOS BIOLOGICOS-A1',measure_unit_item_id:1,medicament_category_id:1,cost:24.026.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'300182',description:'CONTENEDOR DE PUNZOCORTANTES 22.7 LTS',measure_unit_item_id:1,medicament_category_id:1,cost:284.1628.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'CAZG-CH',description:'LAMPARA DE CHICOTE BASE PESADA CHICA-CAZAGUA',measure_unit_item_id:1,medicament_category_id:1,cost:1625.8468.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'6001600',description:'TERMOMETRO ORAL-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:25.4364.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'LMT027-CH',description:'MESA MAYO BASE CROMADA CON CHAROLA CAZAGUA',measure_unit_item_id:1,medicament_category_id:1,cost:1729.872.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200702697',description:'1243A STERI-GAGE TM PARA VAPOR C/1000-3M',measure_unit_item_id:1,medicament_category_id:1,cost:11433.6864.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'SS0732-CJA',description:'SEDA 2-0 7 X75CM, SIN AGUJA C/12 ATRAMAT',measure_unit_item_id:1,medicament_category_id:1,cost:653.376.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200739764-CJA',description:'8635 DURAPREP APLICADOR 6ML-C/50 3M',measure_unit_item_id:1,medicament_category_id:1,cost:12104.9548.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1292-CJA',description:'1292 RAPID ATTEST TM PARA VAPOR C/50-3M',measure_unit_item_id:1,medicament_category_id:1,cost:5549.562052.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'4985-250',description:'MATRAZ ERLENMEYER GRAD ROSCADO 4985-250',measure_unit_item_id:1,medicament_category_id:1,cost:623.2.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'ALG500',description:'TORUNDAS DE ALGODÓN DE 500GR.QUIRMEX',measure_unit_item_id:1,medicament_category_id:1,cost:106.6.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'406358',description:'ALGODÓN ABSORBENTE 300GR-QUIRMEX',measure_unit_item_id:1,medicament_category_id:1,cost:52.316.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'434000-CJA',description:'GUANTE EXPLORACION AMBID. LATEX N/E CH C/100-PROTE',measure_unit_item_id:1,medicament_category_id:1,cost:225.5.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'434100-CJA',description:'GUANTE EXPLORACION AMBID. LATEX MD NO EST C/100-PROTEC',measure_unit_item_id:1,medicament_category_id:1,cost:225.5.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'434200-CJA',description:'GUANTE EXPLORACION AMBID. LATEX GD N/EST C/100-PROTEC',measure_unit_item_id:1,medicament_category_id:1,cost:225.5.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7H1504',description:'1500CC SUCTION LINER-BEMIS',measure_unit_item_id:1,medicament_category_id:1,cost:119.064.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7H3004',description:'3000CC SUCTION LINER-BEMIS',measure_unit_item_id:1,medicament_category_id:1,cost:128.576.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'371073-CJA',description:'EZ-SCRUB CEPILLO/ESPONJA C/CHG 4% C/30-BD',measure_unit_item_id:1,medicament_category_id:1,cost:618.28.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'322010',description:'DERMODINE ESPUMA 3.5 LTS-DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:347.106.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'323000',description:'DERMODINE 3.5 LTS-DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:404.7028.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1911000',description:'ALCOHOL DESNATURALIZ. 1000ML-PROTEC',measure_unit_item_id:1,medicament_category_id:1,cost:67.076.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'837570-CJA',description:'GORRO PAC. ENFERMERA TNT 30GR AZUL C/100-PROTEC',measure_unit_item_id:1,medicament_category_id:1,cost:318.2092.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'GCTAI',description:'GORRO DE CIRUJANO DESECHABLES TELA TNT AZUL C/100 PZAS.',measure_unit_item_id:1,medicament_category_id:1,cost:118.08.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'CZPROM22',description:'LEBRILLO DE ACERO INOXIDABLE(LAVAMANOS)DE 5 LTS',measure_unit_item_id:1,medicament_category_id:1,cost:680.6.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'ESTQX-5LTS',description:'ESTERICIDE QX DE 5 LTS',measure_unit_item_id:1,medicament_category_id:1,cost:872.30944.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'319060',description:'JBON PRE-QUIR.(BENZALCONIO)0 .5% 3.85L (ROSA)DE GASA',measure_unit_item_id:1,medicament_category_id:1,cost:87.9696.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'511000',description:'TUK CINTA TESTIGO P/AUNTO-CLAVE 18M MX50M-DEGASA ',measure_unit_item_id:1,medicament_category_id:1,cost:97.8096.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'TORCAZ250',description:'TORUNDERO 225-250 ML-CAZAGUA',measure_unit_item_id:1,medicament_category_id:1,cost:205.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'32',description:'KRIT GERMICIDA CONCENTRADO CON ANTICORROSIVO PARA DESINFECTAR EL INSTRUMENTAL MED ALTS-ALTAMIRANO',measure_unit_item_id:1,medicament_category_id:1,cost:402.4232.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'BIS-DLP-11-CJA',description:'HOJAS PARA BISTURI DE A. INOX. MARCA DLP, N. 11 C/100',measure_unit_item_id:1,medicament_category_id:1,cost:307.5.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'BIS-DLP-21-CJA',description:'HOJAS PARA BISTURI DE ACERO INOXIDABLE N 21.DLP',measure_unit_item_id:1,medicament_category_id:1,cost:307.5.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'LR90200-RJ-',description:'BOLSA PARA RESIDUOS TOXICOS ROJA 70X90 -A1',measure_unit_item_id:1,medicament_category_id:1,cost:14.022.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'10790-PAQ',description:'VENDA N.10 IDEAL-LEROY C/12-LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:153.504.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1501700',description:'TAPETE BIOSANITARIO PROTEC 45CM X 114CM C/30 C/4',measure_unit_item_id:1,medicament_category_id:1,cost:1163.7932.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'AFEN1825-CJA',description:'PUNZOCAT, CATETER INTRAVENOSO 18G X 25 MM C/50-VIZCARRA',measure_unit_item_id:1,medicament_category_id:1,cost:463.8904.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'AFER2032-CJA',description:'PUNZOCAT CAL.20G X 32MM ROSA C/50 -VIZCARRA',measure_unit_item_id:1,medicament_category_id:1,cost:463.8904.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'AFER2225-CJA',description:'PUNZOCAT CAL.22 X 25MM AZUL C/50 PZAS-VICARRA',measure_unit_item_id:1,medicament_category_id:1,cost:463.8904.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200722463-CJA',description:'15382TELA ADHESIVA DURAPORE 5X10 C/6 3M',measure_unit_item_id:1,medicament_category_id:1,cost:485.6368.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200722471-CJA',description:'15381TELA ADHESIVA DURAPORE 2.5CM (1) C/12 3M',measure_unit_item_id:1,medicament_category_id:1,cost:485.6368.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'SG3698-CJA',description:'PGA 1 70 CM AGUJA SG36 AHUSADA PREMIUM 36 MM 1/2 CIRC. CX12SOB',measure_unit_item_id:1,medicament_category_id:1,cost:718.2052.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'G3792-CJA',description:'PGA 2-0 70CM A/ G37 AHUS. GRUESA 37MM 1/2 CIRC. C/12-ATRAMAT',measure_unit_item_id:1,medicament_category_id:1,cost:661.658.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'R2693-75-CJA',description:'PGA 3-0 75CM AGUJA R26 AHUSADA REGULAR 26 MM 1/2 CIRC C/12 ATRAMAT',measure_unit_item_id:1,medicament_category_id:1,cost:661.658.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'G3718-75-CJA',description:'CATGUT CROMICO 1 75 CM AGUJA G37 AHUSADA 37MM 1/2 CIRC. C/12-ATRAMAT',measure_unit_item_id:1,medicament_category_id:1,cost:626.316.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'SR2612-75',description:'ATRAMAT CHROMIC GUT SURGICAL SUT. USO 2-0 EP 3,5 75CM NEEDLE SR-26PREM. REGULAR TAPE POINT 26 C/12MM 1/2 CIRCLE',measure_unit_item_id:1,medicament_category_id:1,cost:726.684.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'CE2442-75N-CJA',description:'NYLON NEGRO 2/0 CON AGUJA 24 MM C/12-ATRAMAT ',measure_unit_item_id:1,medicament_category_id:1,cost:490.032.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'R2643-75N-CJA',description:'NYLON 3-0 75CM , AGUJA R26 AHUSADA REGULAR 26MM 1/2CIR C/12-ATRAMAT',measure_unit_item_id:1,medicament_category_id:1,cost:490.032.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'CEI244-N-CJA',description:'NYLON NEGRO 4-0 45CM, CE12 A/REV./CORT. 12MM C/12-ATRAMAT',measure_unit_item_id:1,medicament_category_id:1,cost:626.2996.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'DH999980245-CJA',description:'1633 TEGADERM IV CARTON 7X8.5CM C/50-3M',measure_unit_item_id:1,medicament_category_id:1,cost:1959.8.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200729203-CJA',description:'3584 APOSITOS TEGADERM+PAD 6CMX10 CM-C/50 3M',measure_unit_item_id:1,medicament_category_id:1,cost:2435.4.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'3586-CJA50',description:'TEGADERM + PAD 3586 9X10CM C/50-3M',measure_unit_item_id:1,medicament_category_id:1,cost:1918.0948.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'302358-CJA',description:'AGUJA HIP. EST.DESCH 27G X 13 MM C/100 BD',measure_unit_item_id:1,medicament_category_id:1,cost:115.9152.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'301730-CJA',description:'AGUJA HIP. EST.DESCH 20G X 32 MM C/100 BD',measure_unit_item_id:1,medicament_category_id:1,cost:115.9152.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'300081-CJA',description:'AGUJA HIP. EST.DESCH 22G X 32 MM C/100-BD',measure_unit_item_id:1,medicament_category_id:1,cost:115.9152.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'CTPEST',description:'VASO COPRO TAPA ROSCA ESTERIL DE 100ML-SYM',measure_unit_item_id:1,medicament_category_id:1,cost:5.6416.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001412',description:'SONDA NELATON LATEX LONG.40 CM CAL 12 FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:26.1416.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001414',description:'SONDA NELATON LATEX LONG.40 CM CAL 14 FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:26.1416.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001416',description:'SONDA NELATON LATEX LONG.40 CM CAL 16 FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:26.1416.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001418',description:'SONDA NELATON LATEX LONG.40 CM CAL 18 FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:26.1416.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001014',description:'SONDA FOLEY LTX 2V GLBS 14FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:35.342.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001016',description:'SONDA FOLEY LTX 2V GLBS 5 16FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:35.342.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001018',description:'SONDA FOLEY LTX 2V GLB5 18FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:35.342.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13001020',description:'SONDA FOLEY LTX 2V GLB5 20FR-JAYOR',measure_unit_item_id:1,medicament_category_id:1,cost:35.342.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'R2632-CJA',description:'SEDA 2-0 75CM,AGUJA R26 AHUSADA REG. 26 MM 1/2 CIRC C/12 ATRAMAT',measure_unit_item_id:1,medicament_category_id:1,cost:495.936.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200534736-CJA',description:'MICROPORE SKINTONE 1-C/12 3M',measure_unit_item_id:1,medicament_category_id:1,cost:483.8656.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'AA20402',description:'CEPILLO DE LECHUGUILLA-EDIGAR',measure_unit_item_id:1,medicament_category_id:1,cost:31.8324.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7136180',description:'GLUCOMETRO FS OPTIUM NEO METER',measure_unit_item_id:1,medicament_category_id:1,cost:706.84.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'9964955-CJA',description:'TIRAS OPTIUM C/50 MCA MEDISENSE',measure_unit_item_id:1,medicament_category_id:1,cost:706.84.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'9964821',description:'TIRAS OPTIUM C25 PZS-ABBOTT',measure_unit_item_id:1,medicament_category_id:1,cost:373.2476.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7004303',description:'LANCETAS THIN C/100-ABBOTT',measure_unit_item_id:1,medicament_category_id:1,cost:247.4104.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'BIS-DLP-15-CJA',description:'HOJAS PARA BISTURI DE A.INOX. MARCA DLP, N. 15 C-/100',measure_unit_item_id:1,medicament_category_id:1,cost:307.5.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'BIS-DLP-20-CJA',description:'HOJAS PARA BISTURI DE A.INOX. MARCA DLP, N. 20 C-/100',measure_unit_item_id:1,medicament_category_id:1,cost:307.5.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'31145926-CJA',description:'150 MARCADOR DE PIEL C/25-KENDALL',measure_unit_item_id:1,medicament_category_id:1,cost:1271.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'300279',description:'CONTENEDOR PUNZOCORTANTE 1.4LTS-BD',measure_unit_item_id:1,medicament_category_id:1,cost:108.8468.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1321140-CJA',description:'2D285 GUANTE ORTOPEDICO 8 C/40PZAS',measure_unit_item_id:1,medicament_category_id:1,cost:1772.9384.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7877',description:'GORRO PACIENTE/ENFERMERA BLANCO FLORENCE C/100PZAS',measure_unit_item_id:1,medicament_category_id:1,cost:84.8208.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'AA21800',description:'ORINAL DE PLASTICO DE 1 LT-EDIGAR',measure_unit_item_id:1,medicament_category_id:1,cost:38.1628.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'AA22000',description:'RIÑONERA DE PLASTICO EDIGAR',measure_unit_item_id:1,medicament_category_id:1,cost:33.21.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200745340',description:'6650 IOBAN ANTIMICROBIAL INCISE DRAPE STERILE C/10-3M',measure_unit_item_id:1,medicament_category_id:1,cost:3131.006.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'302485-CJA',description:'JERINGA 1ML P/INSULINA C/AGUJA 27X13 C/100CBD',measure_unit_item_id:1,medicament_category_id:1,cost:309.6156.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'302545-CJA',description:'JERINGA 3ML SIN AGUJA C/100-BD',measure_unit_item_id:1,medicament_category_id:1,cost:183.7784.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'302553-CJA',description:'JERINGA 5ML SIN AGUJA C/100-BD',measure_unit_item_id:1,medicament_category_id:1,cost:236.0944.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'302561-CJA',description:'JERINGA 10ML SIN AGUJA C/100-BD',measure_unit_item_id:1,medicament_category_id:1,cost:321.6368.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'302562-CJA',description:'JERINGA 20ML SIN AGUJA C/50-BD',measure_unit_item_id:1,medicament_category_id:1,cost:262.9576.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200712480-CJA',description:'CMW03 CAST PADDINGTM 7.5CM X 3.65M C/20 3M',measure_unit_item_id:1,medicament_category_id:1,cost:1053.2736.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1401330-CJA',description:'HUATA QUIRURGICA EN VENDA 75CMX5MT C/24-DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:163.2784.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1401340-CJA',description:'HATA QUIRURGICA EN VENDA 10C MX5MT C/24-DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:222.6628.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'10791-PAQ',description:'VENDA ELASTICA BAJA COMPRESION IDEAL 15CMX 5M C/12-LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:226.976.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'10794-CJA',description:'VENDA ELASTICA 30CM IDEAL C/12-LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:396.224.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'10788-PAQ',description:'VENDA ELASTICA 5CM X5 MTS MCA.C/12 IDEAL',measure_unit_item_id:1,medicament_category_id:1,cost:89.216.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200473018-ROI',description:'MS03 STOCKINETETM C/1 ROLLO 7.5CM X 22.8 M-3M',measure_unit_item_id:1,medicament_category_id:1,cost:1160.3492.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200473026-CJA ',description:'MS04 STOCKINETETM C/1 ROLLO 10 CM X 22.8M -3M',measure_unit_item_id:1,medicament_category_id:1,cost:1436.476.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'00302PAQ',description:'BOTAS P/QUIROFANO DESECHABLE C/25 PRS. FLORECE',measure_unit_item_id:1,medicament_category_id:1,cost:121.36.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200718560',description:'9200 AVAGARD CHG C/8 500 ML',measure_unit_item_id:1,medicament_category_id:1,cost:13712.614.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200739772-C2',description:'8630 DURAPREP APLICCADOR 26ML C/20-3M',measure_unit_item_id:1,medicament_category_id:1,cost:7216.8528.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13017-CJA',description:'VENDA ENYESADA 10CM C/12 LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:325.868.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13018-CJA',description:'VENDA ENYESADA 15X2.75CM C/12 LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:359.7996.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13016-CJA',description:'VENDA ENYESADA INSTI 5CM X 2.75M. C/12-LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:200.7524.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'2060-HM-CJA',description:'TUBO DE HULE LATEX COLOR AMBAR LUZ:6.50MM',measure_unit_item_id:1,medicament_category_id:1,cost:1312.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1321100-CJA',description:'2D7 GUANTE ORTOPEDICO 7 C/40 DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:1772.9384.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1321120-CJA',description:'2D7284 GUANTE ORTOPEDICO 7 1/2 C/40 DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:1772.9384.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'10790-PAQ',description:'VENDA N.10 IDEAL-LEROY C/12-LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:179.58.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'10791-PAQ',description:'VENDA ELASTICA BAJA COMPRESION IDEAL 15CMX 5M C/12-LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:258.382.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'SS1038-CJA',description:'SEDA S/AGUJA CAL.1 10 HEBRAS 75 CM C/12 ATRAMAT',measure_unit_item_id:1,medicament_category_id:1,cost:844.272.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'AA02600',description:'JERINGA ASEPTICA 90 ML. CON CAJA-EDIGAR',measure_unit_item_id:1,medicament_category_id:1,cost:179.5472.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7503003406044PZ',description:'CINTA UMBILICAL-QUIRMEX',measure_unit_item_id:1,medicament_category_id:1,cost:4.5428.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'70200749276-CJA',description:'1624 TEGADERM FILM TRANSPARENT C/100 3M',measure_unit_item_id:1,medicament_category_id:1,cost:1667.6832.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'101000-PAQ',description:'APOSITO GASA NO.1 20X8 CM C/10-DEGASA NO ESTERIL',measure_unit_item_id:1,medicament_category_id:1,cost:69.4212.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'10403',description:'VENDA DE GASA 5CM MARCA: LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:25.4364.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'608940052',description:'TOALLA CONFEM PROTEC DESECHABLES PAQ C/10',measure_unit_item_id:1,medicament_category_id:1,cost:57.4.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'K-351',description:'CONECTOR DELGADO MOD.SIMS 1 VIA-MEDICA EXPRESS',measure_unit_item_id:1,medicament_category_id:1,cost:7.0684.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'K-360',description:'CONECTOR DE DOS VIASEN Y DE PLASTICO DESECHABLE',measure_unit_item_id:1,medicament_category_id:1,cost:12.5296.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'131-RS-PZA',description:'APLICADOR PLASTICO CON ALGODÓN NO ESTERIL',measure_unit_item_id:1,medicament_category_id:1,cost:1.0496.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'POP25',description:'ABATELENGUAS DE MADERA C/25 PZS',measure_unit_item_id:1,medicament_category_id:1,cost:8.4788.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1321080',description:'2D7282 GUANTE ORTOPEDICO 6 1/2 C/40 PRS',measure_unit_item_id:1,medicament_category_id:1,cost:1772.9384.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1321100-CJA',description:'2D7283 GUANTE ORTOPEDICO 7 C/40 DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:1772.9384.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1321120-CJA',description:'2D7284 GUANTE ORTOPEDICO 7 1/2 C/40 DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:1772.9384.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'LATUPRESHO14-S',description:'TUBO PENROSE ESTERIL DE LATEX 1/4 HOLY',measure_unit_item_id:1,medicament_category_id:1,cost:34.44.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'HOLPEN12',description:'TUPO PENROSE 1/2 ESTERIL HOLY',measure_unit_item_id:1,medicament_category_id:1,cost:44.28.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'HOLPEN34',description:'TUBO PENROSE 3/4 ESTERIL',measure_unit_item_id:1,medicament_category_id:1,cost:55.76.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'80-1408',description:'CODMAN.COMPRESA QUIRURGICA 2.54CM X 7.62CM-JOHN',measure_unit_item_id:1,medicament_category_id:1,cost:746.4952.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'044-7-CJA',description:'GUANTE P/EXAMEN DESECHABLE ESTERIL MEDIANO-UNISEAL',measure_unit_item_id:1,medicament_category_id:1,cost:226.2052.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'GESTGDECO-CJA',description:'GUANTE DEEXPLORACION DESECHALE GRANDE',measure_unit_item_id:1,medicament_category_id:1,cost:226.2052.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'2019-0014',description:'CATETER GASTROSTOMIA 14 FR MCA FORTUNE MEDICAL',measure_unit_item_id:1,medicament_category_id:1,cost:1558.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'2019-0016',description:'CATETER GASTROSTOMIA 16 FR MARCA FORTUNE MEDICA',measure_unit_item_id:1,medicament_category_id:1,cost:1558.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'0101P-C-10PAQ',description:'PAÑAL PREDOBLADO ADULTO C/10/ELASTICO COTIDIAN',measure_unit_item_id:1,medicament_category_id:1,cost:93.316.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'HUD1885',description:'MICRONEBULIZADOR MICRO-MIST C/MASCARARILLA ADULTO-HUDSON',measure_unit_item_id:1,medicament_category_id:1,cost:51.086.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1886',description:'MICRO NEBULIZADOR PEDIATRICO-HUDSON',measure_unit_item_id:1,medicament_category_id:1,cost:54.8744.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'31145926-CJA',description:'150 MARCADOR DE PIEL CAJA C/25-KENDALL',measure_unit_item_id:1,medicament_category_id:1,cost:1271.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13016-CJA',description:'VENDA ENYESADA INSTI 6 CM X 2.75 M C/12-LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:424.35.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13017-CJA',description:'VENDA ENYESADA 10 CM C/12 LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:576.87.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'13018-CJA',description:'VENDA ENYESADA 15X2.75CM C/12 LEROY',measure_unit_item_id:1,medicament_category_id:1,cost:787.2.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1401340-CJA',description:'HUATA QUIRURGICA EN VENDA 10C MX5MT C/24-DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:222.6628.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1401330-CJA',description:'HUATAQUIRURGICA EN VENDA 7C MX5MT C/24-DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:163.2784.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1401350-CJA',description:'HUATA QUIRURGICA EN VENDA 15CM MX5MT C/24-DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:322.342.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1321140-CJA',description:'2D7285 GUANTE ORTOPEDICO 8 C/40PZS',measure_unit_item_id:1,medicament_category_id:1,cost:1772.9384.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7136180',description:'GLUCOMERO FS OPTIUM NEO METER',measure_unit_item_id:1,medicament_category_id:1,cost:706.84.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'9964821',description:'TIRAS OPTIUM C25 PZS ABBOTT',measure_unit_item_id:1,medicament_category_id:1,cost:373.2476.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'9964955-CJA',description:'TIRAS OPTIUM C/50 MCA. MEDISENSE',measure_unit_item_id:1,medicament_category_id:1,cost:611.064.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7004303',description:'LANCETAS THIN C/100-ABBOTT',measure_unit_item_id:1,medicament_category_id:1,cost:247.4104.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'FER2',description:'FERULA PARA RODILLA UNIVERSAL-MEDIPAR',measure_unit_item_id:1,medicament_category_id:1,cost:822.1156.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'FER8',description:'FERULA P/RODILLA TRIPANEL UNIVERSAL LARGO',measure_unit_item_id:1,medicament_category_id:1,cost:1097.16.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'FEM2I-MD',description:'FERULA PARA MUÑECA Y ANTEBRAZO IZQUIERDA MD-MEDIPAR',measure_unit_item_id:1,medicament_category_id:1,cost:382.0216.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'FEM2D-MD',description:'FERULA PARA MUÑECA Y ANTEBRAZO DERECHA MD-MEDIPAR',measure_unit_item_id:1,medicament_category_id:1,cost:382.0216.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'T00INHU',description:'INMOVILIZADOR DE HOMBRO UNIVERSAL/ADULTO-MEDIDPAR',measure_unit_item_id:1,medicament_category_id:1,cost:286.0652.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'CAB-MD',description:'CABESTRILLO-MD-MEDIPAR',measure_unit_item_id:1,medicament_category_id:1,cost:103.9104.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'302358-CJA',description:'AGUJA HIP.EST.DESECH. 27GX13 MM C/100-BD',measure_unit_item_id:1,medicament_category_id:1,cost:115.9152.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'301060',description:'DERMO-QRIT 500 ML AL 12% CLOR BENZALCONIO-DEGAS',measure_unit_item_id:1,medicament_category_id:1,cost:33.9316.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7877',description:'GORRO PACIENTE/ENFERMERA BLANCO FLORENCE C/100P',measure_unit_item_id:1,medicament_category_id:1,cost:84.8208.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'00303PAQ',description:'BOTAS P/QUIROFANO DESECHABLE C/25 PRS. FLORENCE',measure_unit_item_id:1,medicament_category_id:1,cost:121.36.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'FLORENCE-01',description:'CUBREBOCAS PARA USO EN AREA HOSPITALARIA C/150 FLORENCE',measure_unit_item_id:1,medicament_category_id:1,cost:94.4148.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'AA20402',description:'CEPILLO DE LECHUGUILLA-EDIGAR',measure_unit_item_id:1,medicament_category_id:1,cost:31.8324.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'1910200',description:'ALCOHOL DESNATURALIZ. 200 ML-DEGASA',measure_unit_item_id:1,medicament_category_id:1,cost:16.25896.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'31145926-CAJ',description:'MARCADOR QUIRURGICO PARA PIEL COLECTIVO CON 4 CAJ',measure_unit_item_id:1,medicament_category_id:1,cost:5084.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'2060-HM-CJA',description:'TUBO DE HULE LATEX COLOR AMBAR LUZ 6.50MM PARED 3.70MM',measure_unit_item_id:1,medicament_category_id:1,cost:1312.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7E-A',description:'ASPIRADOR DE FLEMAS PORTATIL ELECTRICO-HERGOM',measure_unit_item_id:1,medicament_category_id:1,cost:5552.0724.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'7A-200',description:'FRASCO ACRILICO P/ASPIRADOR PORTATIL 1000ML HERGO ',measure_unit_item_id:1,medicament_category_id:1,cost:410.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'A6701',description:'BOLSA LINER PARA RESIDUOS DE 1500 ML-BEMIS',measure_unit_item_id:1,medicament_category_id:1,cost:113.16.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save
item = PharmacyItem.create(code:'326716-CJA',description:'JERINGAHIP 1ML 27GA 13 MM-BD C/100',measure_unit_item_id:1,medicament_category_id:1,cost:365.4576.round(2),is_consigment:0,sale_cost:0)
PharmacyItemInventory.new(quantity:100,warehouse_id:1,  pharmacy_item_id:item.id,user_id:1).save





