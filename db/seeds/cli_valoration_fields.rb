fields = [
    {
        :f_key => 'weight',
        :name => 'Peso',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'number'
    },{
        :f_key => 'height',
        :name => 'Altura',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'number'
    },{
        :f_key => 'temperature',
        :name => 'Temperatura',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'number'
    },{
        :f_key => 'blood_pressure',
        :name => 'TA',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'glucose',
        :name => 'Glucosa',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'number'
    },{
        :f_key => 'imc',
        :name => 'IMC',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'number'
    },{
        :f_key => 's_weight',
        :name => 'Peso sugerido',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'number'
    },{
        :f_key => 'head_circumference',
        :name => 'Perímetro encefálico',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'number'
    },{
        :f_key => 'heart_rate',
        :name => 'Lpm',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'breathing_frequency',
        :name => 'Rpm',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'eco_rctg',
        :name => 'ECO/RCGT',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'fu',
        :name => 'FU',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'pres',
        :name => 'PRES',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'ff',
        :name => 'FF',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'vdrl',
        :name => 'VDRL',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'ego',
        :name => 'EGO',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'other',
        :name => 'Otros',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'oxygen_saturation',
        :name => 'Saturación de oxígeno',
        :f_type => ValorationField::VITAL_SIGN,
        :f_html_type => 'text'
    },{
        :f_key => 'breasts',
        :name => 'Senos',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'vulva',
        :name => 'Vulva',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'abdomen',
        :name => 'Abdomen',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'cervix',
        :name => 'Cervix',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'ovaries',
        :name => 'Ovarios',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'annexed',
        :name => 'Anexos',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'edemas',
        :name => 'Edemas',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'uterine_height',
        :name => 'Altura uterina',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'head',
        :name => 'Cabeza',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'lungs',
        :name => 'Pulmones',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'heart',
        :name => 'Corazón',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'extremities',
        :name => 'Extremidades',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'other',
        :name => 'Otros',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'long_text'
    },{
        :f_key => 'vagina',
        :name => 'Vagina',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'pelvis',
        :name => 'Pelvis',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'rectum',
        :name => 'Recto-Vaginal',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'lymph_nodes',
        :name => 'Ganglios linfáticos',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'abdomen',
        :name => 'Abdomen',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'bladder',
        :name => 'Vejiga',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'lumbar_region',
        :name => 'Región lumbar',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'scrotum_testicles',
        :name => 'Escroto y testículos',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'hernias',
        :name => 'Hernias',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'epididymis_vessels',
        :name => 'Epidídimo y vasos',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'penis',
        :name => 'Pene',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'injuries',
        :name => 'Lesiones',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'prostate',
        :name => 'Próstata',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'seminal_vesicles',
        :name => 'Vesículas seminales',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'text'
    },{
        :f_key => 'notes',
        :name => 'Notas',
        :f_type => ValorationField::EXPLORATION,
        :f_html_type => 'long_text'
    }
]



fields.each do |data|
  ValorationField.create(data)
end

v = ValorationType.create(name: "Signos Vitales", valoration_type: ValorationField::VITAL_SIGN )
v = ValorationType.create(name: "Exploración ginecológica completa", valoration_type: ValorationField::EXPLORATION )
v = ValorationType.create(name: "Exploración ginecológica pélvica", valoration_type: ValorationField::EXPLORATION )
v = ValorationType.create(name: "Exploración ginecológica física", valoration_type: ValorationField::EXPLORATION )
v = ValorationType.create(name: "Valoración Urológica", valoration_type: ValorationField::EXPLORATION )


v = ValorationType.find(1)
fields = ValorationField.where('id >= 1 and id <= 18')
v.update(valoration_fields: fields)

v = ValorationType.find(2)
fields = ValorationField.where('id >= 19 and id <= 34')
v.update(valoration_fields: fields)


v = ValorationType.find(3)
fields = ValorationField.where('id IN (20,22,23,24,25,26,32,33,34)')
v.update(valoration_fields: fields)

v = ValorationType.find(4)
fields = ValorationField.where('id IN (19,21,27,28,29,30,31)')
v.update(valoration_fields: fields)

v = ValorationType.find(5)
fields = ValorationField.where('id >= 35 and id <= 46')
v.update(valoration_fields: fields)