VaccineBook.delete_all
Vaccine.delete_all
ApplicationAge.delete_all

ninios = VaccineBook.create(name: "Niños",gender: "ambos", start_age:0,end_age:9)
adolescentes = VaccineBook.create(name: "Adolescentes",gender: "ambos", start_age:10,end_age:19)
mujer = VaccineBook.create(name: "Mujer",gender: "Mujer", start_age:20,end_age:59)
hombre = VaccineBook.create(name: "Hombre",gender: "Hombre", start_age:20,end_age:59)
adulto_mayor = VaccineBook.create(name: "Adulto mayor",gender: "ambos", start_age:60,end_age:100)

tuberculosis = Vaccine.create(name:"BCG", prevent_disease:"Tuberculosis", vaccine_book_id:ninios.id)
ApplicationAge.create(age:"Al nacer" , age_type:0, dose:"Única" , comments:"", vaccine_id: tuberculosis.id)

hepatits_b = Vaccine.create(name:"Hepatitis B", prevent_disease:"Hepatitis B", vaccine_book_id:ninios.id)
ApplicationAge.create(age:"Al nacer" , age_type:0, dose:"Primera" , comments:"", vaccine_id: hepatits_b.id)
ApplicationAge.create(age:"2 meses" , age_type:1, dose:"Segunda" , comments:"", vaccine_id: hepatits_b.id)
ApplicationAge.create(age:"6 meses" , age_type:2, dose:"Tercera" , comments:"", vaccine_id: hepatits_b.id)

pentavalente = Vaccine.create(name:"Pentavalenta acelular DPaT + VPI + Hib", prevent_disease:"Disteria, tos ferina, tetanos, poliomielitis e infecciones por H. inflenzae b", vaccine_book_id:ninios.id)
ApplicationAge.create(age:"2 meses" , age_type:0, dose:"Primera" , comments:"", vaccine_id: pentavalente.id)
ApplicationAge.create(age:"4 meses" , age_type:1, dose:"Segunda" , comments:"", vaccine_id: pentavalente.id)
ApplicationAge.create(age:"6 meses" , age_type:2, dose:"Tercera" , comments:"", vaccine_id: pentavalente.id)
ApplicationAge.create(age:"18 meses" , age_type:3, dose:"Cuarta" , comments:"", vaccine_id: pentavalente.id)

dpt = Vaccine.create(name:"DPT", prevent_disease:"Disteria, tos ferina y tetanos", vaccine_book_id:ninios.id)
ApplicationAge.create(age:"4 años" , age_type:0, dose:"Refuerzo" , comments:"", vaccine_id: dpt.id)

rotavirus = Vaccine.create(name:"Rotavirus", prevent_disease:"Diarrea por rotavirus", vaccine_book_id:ninios.id)
ApplicationAge.create(age:"2 meses" , age_type:0, dose:"Primera" , comments:"", vaccine_id: rotavirus.id)
ApplicationAge.create(age:"4 meses" , age_type:1, dose:"Segunda" , comments:"", vaccine_id: rotavirus.id)
ApplicationAge.create(age:"6 meses" , age_type:2, dose:"Tercera" , comments:"", vaccine_id: rotavirus.id)

neumococica = Vaccine.create(name:"Neumocócica", prevent_disease:"Infecciones por neumococo", vaccine_book_id:ninios.id)
ApplicationAge.create(age:"2 meses" , age_type:0, dose:"Primera" , comments:"", vaccine_id: neumococica.id)
ApplicationAge.create(age:"4 meses" , age_type:1, dose:"Segunda" , comments:"", vaccine_id: neumococica.id)
ApplicationAge.create(age:"12 meses" , age_type:2, dose:"Tercera" , comments:"", vaccine_id: neumococica.id)

influenza = Vaccine.create(name:"Influenza", prevent_disease:"Influenza", vaccine_book_id:ninios.id)
ApplicationAge.create(age:"6 meses" , age_type:0, dose:"Primera" , comments:"", vaccine_id: influenza.id)
ApplicationAge.create(age:"7 meses" , age_type:1, dose:"Segunda" , comments:"", vaccine_id: influenza.id)
ApplicationAge.create(age:"Anual hasta los 59 meses" , age_type:2, dose:"Revacunación" , comments:"", vaccine_id: influenza.id)

srp = Vaccine.create(name:"SRP", prevent_disease:"Saranpion, rubéola y parotiditis", vaccine_book_id:ninios.id)
ApplicationAge.create(age:"1 año" , age_type:0, dose:"Primera" , comments:"", vaccine_id: srp.id)
ApplicationAge.create(age:"6 años" , age_type:1, dose:"Refuerzo" , comments:"", vaccine_id: srp.id)

sabin = Vaccine.create(name:"Sabin", prevent_disease:"Poliomielitis", vaccine_book_id:ninios.id)
ApplicationAge.create(age:"Adicionales" , age_type:0, dose:"Adicionales" , comments:"", vaccine_id: sabin.id)

sr = Vaccine.create(name:"S R", prevent_disease:"Saranpion y rubéola", vaccine_book_id:ninios.id)
ApplicationAge.create(age:"Adicionales" , age_type:0, dose:"Adicionales" , comments:"", vaccine_id: sr.id)

hepatitisb = Vaccine.create(name:"Hepatitis b (los que no se han vacunado)", prevent_disease:"Hepatitis B", vaccine_book_id:adolescentes.id)
ApplicationAge.create(age:"A partir de los 11 años" , age_type:0, dose:"Primera" , comments:"", vaccine_id: hepatitisb.id)
ApplicationAge.create(age:"4 semanas posteriores a la primera" , age_type:1, dose:"Segunda" , comments:"", vaccine_id: hepatitisb.id)

td = Vaccine.create(name:"Td", prevent_disease:"Tétanos y Difteria", vaccine_book_id:adolescentes.id)
ApplicationAge.create(age:"11 años" , age_type:0, dose:"Refuerzo (con esquema completo)" , comments:"", vaccine_id: td.id)
ApplicationAge.create(age:"Dosis inicial" , age_type:1, dose:"Primera (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td.id)
ApplicationAge.create(age:"1 mes despues de la primera dosis" , age_type:2, dose:"Segunda (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td.id)
ApplicationAge.create(age:"12 meses posteriores a la primera dosis" , age_type:3, dose:"Tercera (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td.id)

tdpa = Vaccine.create(name:"Tdpa", prevent_disease:"Tétanos, difteria y tos ferina", vaccine_book_id:adolescentes.id)
ApplicationAge.create(age:"A partir de la semana 20 del embarazo" , age_type:0, dose:"Única" , comments:"", vaccine_id: tdpa.id)

influencia_estacional = Vaccine.create(name:"Influencia estacional", prevent_disease:"Influencia", vaccine_book_id:adolescentes.id)
ApplicationAge.create(age:"Cualquier trimestre del embarazo" , age_type:0, dose:"Única" , comments:"", vaccine_id: influencia_estacional.id)

sr_ado = Vaccine.create(name:"SR (los que no han sido vacunados o tienen esquema incompleto)", prevent_disease:"Saranpión y rubéola", vaccine_book_id:adolescentes.id)
ApplicationAge.create(age:"En el primer contacto" , age_type:0, dose:"Primera (sin antecedente vacunal)" , comments:"", vaccine_id: sr_ado.id)
ApplicationAge.create(age:"4 semanas despues de la primera" , age_type:1, dose:"Segunda (sin antecedente vacunal)" , comments:"", vaccine_id: sr_ado.id)
ApplicationAge.create(age:"En el primer contacto" , age_type:2, dose:"Dosis única (con esquema incompleto)" , comments:"", vaccine_id: sr_ado.id)

vph = Vaccine.create(name:"VPH", prevent_disease:"INFECCIÓN POR EL VIRUS DEL PAPILOMA HUMANO Y CÁNCER CERVICO-UTERINO", vaccine_book_id:adolescentes.id)
ApplicationAge.create(age:"Mujeres en el 5to grado de primeria y de 11 años de edad no escolarizada" , age_type:0, dose:"Primera" , comments:"", vaccine_id: vph.id)
ApplicationAge.create(age:"6 meses despues de la primera dosis" , age_type:1, dose:"Segunda" , comments:"", vaccine_id: vph.id)
ApplicationAge.create(age:"60 meses despues de la primera dosis" , age_type:2, dose:"Tercera" , comments:"", vaccine_id: vph.id)

sr_mujer = Vaccine.create(name:"SR (los que no han sido vacunados o tienen esquema incompleto hasta los 39 años de edad)", prevent_disease:"Saranpión y rubéola", vaccine_book_id:mujer.id)
ApplicationAge.create(age:"En el primer contacto" , age_type:0, dose:"Primera (sin antecedente vacunal)" , comments:"", vaccine_id: sr_mujer.id)
ApplicationAge.create(age:"4 semanas despues de la primera" , age_type:1, dose:"Segunda (sin antecedente vacunal)" , comments:"", vaccine_id: sr_mujer.id)
ApplicationAge.create(age:"En el primer contacto" , age_type:2, dose:"Dosis única (con esquema incompleto)" , comments:"", vaccine_id: sr_mujer.id)

td_mujer = Vaccine.create(name:"Td", prevent_disease:"Tétanos y Difteria", vaccine_book_id:mujer.id)
ApplicationAge.create(age:"Cada 10 años" , age_type:0, dose:"Refuerzo (con esquema completo)" , comments:"", vaccine_id: td_mujer.id)
ApplicationAge.create(age:"Dosis inicial" , age_type:1, dose:"Primera (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td_mujer.id)
ApplicationAge.create(age:"1 mes despues de la primera dosis" , age_type:2, dose:"Segunda (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td_mujer.id)
ApplicationAge.create(age:"12 meses posteriores a la primera dosis" , age_type:3, dose:"Tercera (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td_mujer.id)

tdpa_mujer = Vaccine.create(name:"Tdpa", prevent_disease:"Tétanos, difteria y tos ferina", vaccine_book_id:mujer.id)
ApplicationAge.create(age:"A partir de la semana 20 del embarazo" , age_type:0, dose:"Única" , comments:"", vaccine_id: tdpa_mujer.id)

influencia_estacional_mujer = Vaccine.create(name:"Influencia estacional", prevent_disease:"Influencia", vaccine_book_id:mujer.id)
ApplicationAge.create(age:"Personas con factores de riesgo" , age_type:0, dose:"Anual" , comments:"", vaccine_id: influencia_estacional_mujer.id)
ApplicationAge.create(age:"Cualquier trimestre del embarazo" , age_type:0, dose:"Única (Embarazadas)" , comments:"", vaccine_id: influencia_estacional_mujer.id)

sr_hombre = Vaccine.create(name:"SR (los que no han sido vacunados o tienen esquema incompleto hasta los 39 años de edad)", prevent_disease:"Saranpión y rubéola", vaccine_book_id:hombre.id)
ApplicationAge.create(age:"En el primer contacto" , age_type:0, dose:"Primera (sin antecedente vacunal)" , comments:"", vaccine_id: sr_hombre.id)
ApplicationAge.create(age:"4 semanas despues de la primera" , age_type:1, dose:"Segunda (sin antecedente vacunal)" , comments:"", vaccine_id: sr_hombre.id)
ApplicationAge.create(age:"En el primer contacto" , age_type:2, dose:"Dosis única (con esquema incompleto)" , comments:"", vaccine_id: sr_hombre.id)

td_hombre = Vaccine.create(name:"Td", prevent_disease:"Tétanos y Difteria", vaccine_book_id:hombre.id)
ApplicationAge.create(age:"Cada 10 años" , age_type:0, dose:"Refuerzo (con esquema completo)" , comments:"", vaccine_id: td_hombre.id)
ApplicationAge.create(age:"Dosis inicial" , age_type:1, dose:"Primera (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td_hombre.id)
ApplicationAge.create(age:"1 mes despues de la primera dosis" , age_type:2, dose:"Segunda (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td_hombre.id)
ApplicationAge.create(age:"12 meses posteriores a la primera dosis" , age_type:3, dose:"Tercera (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td_hombre.id)


influencia_estacional_hombre = Vaccine.create(name:"Influencia estacional", prevent_disease:"Influencia", vaccine_book_id:hombre.id)
ApplicationAge.create(age:"Personas con factores de riesgo" , age_type:0, dose:"Anual" , comments:"", vaccine_id: influencia_estacional_hombre.id)

neumo_poli = Vaccine.create(name:"Neumocócica polisacárida", prevent_disease:"Neumonía por neumococos", vaccine_book_id:adulto_mayor.id)
ApplicationAge.create(age:"A partir de los 65 años" , age_type:0, dose:"Única" , comments:"", vaccine_id: neumo_poli.id)
ApplicationAge.create(age:"60 a 64 años de edad" , age_type:1, dose:"Dosis inicial (personas con factores de riesgo)" , comments:"", vaccine_id: neumo_poli.id)
ApplicationAge.create(age:"Cinco años despues de la dosis inicial" , age_type:2, dose:"Revacunación única (personas con factores de riesgo)" , comments:"", vaccine_id: neumo_poli.id)

td_adulto = Vaccine.create(name:"Td", prevent_disease:"Tétanos y Difteria", vaccine_book_id:adulto_mayor.id)
ApplicationAge.create(age:"Cada 10 años" , age_type:0, dose:"Refuerzo (con esquema completo)" , comments:"", vaccine_id: td_adulto.id)
ApplicationAge.create(age:"Dosis inicial" , age_type:1, dose:"Primera (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td_adulto.id)
ApplicationAge.create(age:"1 mes despues de la primera dosis" , age_type:2, dose:"Segunda (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td_adulto.id)
ApplicationAge.create(age:"12 meses posteriores a la primera dosis" , age_type:3, dose:"Tercera (con esquema incompleto o no documentado)" , comments:"", vaccine_id: td_adulto.id)


influencia_estacional_adulto = Vaccine.create(name:"Influencia estacional", prevent_disease:"Influencia", vaccine_book_id:adulto_mayor.id)
ApplicationAge.create(age:"Personas con factores de riesgo" , age_type:0, dose:"Anual" , comments:"", vaccine_id: influencia_estacional_adulto.id)




