# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170731220553) do

  create_table "abortion_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "abortions", force: :cascade do |t|
    t.datetime "date"
    t.string   "hospital",         limit: 255
    t.string   "observations",     limit: 255
    t.integer  "abortion_type_id", limit: 4
    t.integer  "pregnancy_id",     limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "user_id",          limit: 4
  end

  add_index "abortions", ["abortion_type_id"], name: "index_abortions_on_abortion_type_id", using: :btree
  add_index "abortions", ["pregnancy_id"], name: "index_abortions_on_pregnancy_id", using: :btree
  add_index "abortions", ["user_id"], name: "index_abortions_on_user_id", using: :btree

  create_table "accounts", force: :cascade do |t|
    t.integer  "bank_id",        limit: 4
    t.string   "account_number", limit: 255
    t.string   "clabe",          limit: 255
    t.float    "balance",        limit: 24
    t.string   "details",        limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "currency_id",    limit: 4
    t.boolean  "is_global"
  end

  add_index "accounts", ["bank_id"], name: "index_accounts_on_bank_id", using: :btree
  add_index "accounts", ["currency_id"], name: "index_accounts_on_currency_id", using: :btree

  create_table "accounts_offices", id: false, force: :cascade do |t|
    t.integer "account_id", limit: 4
    t.integer "office_id",  limit: 4
  end

  add_index "accounts_offices", ["account_id"], name: "index_accounts_offices_on_account_id", using: :btree
  add_index "accounts_offices", ["office_id"], name: "index_accounts_offices_on_office_id", using: :btree

  create_table "accounts_transactions", id: false, force: :cascade do |t|
    t.integer "account_id",     limit: 4
    t.integer "transaction_id", limit: 4
  end

  add_index "accounts_transactions", ["account_id"], name: "index_accounts_transactions_on_account_id", using: :btree
  add_index "accounts_transactions", ["transaction_id"], name: "index_accounts_transactions_on_transaction_id", using: :btree

  create_table "accounts_users", id: false, force: :cascade do |t|
    t.integer "account_id", limit: 4
    t.integer "user_id",    limit: 4
  end

  add_index "accounts_users", ["account_id"], name: "index_accounts_users_on_account_id", using: :btree
  add_index "accounts_users", ["user_id"], name: "index_accounts_users_on_user_id", using: :btree

  create_table "active_substances", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "active_substances_medicaments", id: false, force: :cascade do |t|
    t.integer "active_substance_id", limit: 4
    t.integer "medicament_id",       limit: 4
  end

  add_index "active_substances_medicaments", ["active_substance_id"], name: "index_active_substances_medicaments_on_active_substance_id", using: :btree
  add_index "active_substances_medicaments", ["medicament_id"], name: "index_active_substances_medicaments_on_medicament_id", using: :btree

  create_table "activities", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.string   "details",            limit: 255
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "doctor_id",          limit: 4
    t.boolean  "all_day"
    t.integer  "activity_type_id",   limit: 4
    t.integer  "activity_status_id", limit: 4
    t.string   "owner_type",         limit: 255
    t.integer  "owner_id",           limit: 4
  end

  add_index "activities", ["activity_status_id"], name: "index_activities_on_activity_status_id", using: :btree
  add_index "activities", ["activity_type_id"], name: "index_activities_on_activity_type_id", using: :btree
  add_index "activities", ["doctor_id"], name: "index_activities_on_doctor_id", using: :btree

  create_table "activities_offices", id: false, force: :cascade do |t|
    t.integer "office_id",   limit: 4
    t.integer "activity_id", limit: 4
  end

  add_index "activities_offices", ["activity_id"], name: "index_activities_offices_on_activity_id", using: :btree
  add_index "activities_offices", ["office_id"], name: "index_activities_offices_on_office_id", using: :btree

  create_table "activities_patients", id: false, force: :cascade do |t|
    t.integer "activity_id", limit: 4
    t.integer "patient_id",  limit: 4
  end

  add_index "activities_patients", ["activity_id"], name: "index_activities_patients_on_activity_id", using: :btree
  add_index "activities_patients", ["patient_id"], name: "index_activities_patients_on_patient_id", using: :btree

  create_table "activities_personalsupports", id: false, force: :cascade do |t|
    t.integer "activity_id",        limit: 4
    t.integer "personalsupport_id", limit: 4
  end

  add_index "activities_personalsupports", ["activity_id"], name: "index_activities_personalsupports_on_activity_id", using: :btree
  add_index "activities_personalsupports", ["personalsupport_id"], name: "index_activities_personalsupports_on_personalsupport_id", using: :btree

  create_table "activities_user_patients", id: false, force: :cascade do |t|
    t.integer "activity_id",     limit: 4
    t.integer "user_patient_id", limit: 4
  end

  add_index "activities_user_patients", ["activity_id"], name: "index_activities_user_patients_on_activity_id", using: :btree
  add_index "activities_user_patients", ["user_patient_id"], name: "index_activities_user_patients_on_user_patient_id", using: :btree

  create_table "activity_statuses", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "activity_type_id", limit: 4
  end

  add_index "activity_statuses", ["activity_type_id"], name: "index_activity_statuses_on_activity_type_id", using: :btree

  create_table "activity_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "color",      limit: 255
  end

  create_table "address_books", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "telephone",   limit: 255
    t.string   "cellphone",   limit: 255
    t.string   "email",       limit: 255
    t.string   "address",     limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "doctor_id",   limit: 4
    t.integer  "hospital_id", limit: 4
    t.integer  "office_id",   limit: 4
  end

  add_index "address_books", ["doctor_id"], name: "index_address_books_on_doctor_id", using: :btree
  add_index "address_books", ["hospital_id"], name: "index_address_books_on_hospital_id", using: :btree
  add_index "address_books", ["office_id"], name: "index_address_books_on_office_id", using: :btree

  create_table "admission_invoice_concepts", force: :cascade do |t|
    t.integer  "quantity",             limit: 4
    t.text     "description",          limit: 65535
    t.decimal  "unit_price",                         precision: 11, scale: 2
    t.decimal  "cost",                               precision: 11, scale: 2
    t.integer  "admission_invoice_id", limit: 4
    t.integer  "measure_unit_id",      limit: 4
    t.text     "unit",                 limit: 65535
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
  end

  add_index "admission_invoice_concepts", ["admission_invoice_id"], name: "index_admission_invoice_concepts_on_admission_invoice_id", using: :btree
  add_index "admission_invoice_concepts", ["measure_unit_id"], name: "index_admission_invoice_concepts_on_measure_unit_id", using: :btree

  create_table "admission_invoice_emisors", force: :cascade do |t|
    t.string   "business_name",        limit: 255
    t.string   "rfc",                  limit: 255
    t.string   "address",              limit: 255
    t.string   "suburb",               limit: 255
    t.string   "no_ext",               limit: 255
    t.string   "no_int",               limit: 255
    t.string   "zip",                  limit: 255
    t.string   "locality",             limit: 255
    t.string   "city",                 limit: 255
    t.string   "state",                limit: 255
    t.string   "country",              limit: 255
    t.string   "tax_regime",           limit: 255
    t.integer  "admission_invoice_id", limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "admission_invoice_emisors", ["admission_invoice_id"], name: "index_admission_invoice_emisors_on_admission_invoice_id", using: :btree

  create_table "admission_invoice_receptors", force: :cascade do |t|
    t.string   "business_name",        limit: 255
    t.string   "rfc",                  limit: 255
    t.string   "address",              limit: 255
    t.string   "suburb",               limit: 255
    t.string   "no_ext",               limit: 255
    t.string   "no_int",               limit: 255
    t.string   "zip",                  limit: 255
    t.string   "locality",             limit: 255
    t.string   "city",                 limit: 255
    t.string   "state",                limit: 255
    t.string   "country",              limit: 255
    t.integer  "admission_invoice_id", limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "admission_invoice_receptors", ["admission_invoice_id"], name: "index_admission_invoice_receptors_on_admission_invoice_id", using: :btree

  create_table "admission_invoices", force: :cascade do |t|
    t.string   "xml_base",                     limit: 255
    t.text     "original_string",              limit: 65535
    t.string   "serie",                        limit: 255
    t.integer  "folio",                        limit: 4
    t.string   "certification_date",           limit: 255
    t.string   "payment_condition",            limit: 255
    t.string   "payment_methos_name",          limit: 255
    t.string   "account_number",               limit: 255
    t.decimal  "subtotal",                                   precision: 10, scale: 2
    t.decimal  "iva",                                        precision: 10, scale: 2
    t.decimal  "isr",                                        precision: 10, scale: 2
    t.string   "UUID",                         limit: 255
    t.text     "xml",                          limit: 65535
    t.text     "sat_seal",                     limit: 65535
    t.string   "no_cert_sat",                  limit: 255
    t.text     "transmitter_seal",             limit: 65535
    t.string   "qr_code",                      limit: 255
    t.boolean  "cancel"
    t.integer  "invoice_type",                 limit: 4
    t.integer  "user_id",                      limit: 4
    t.integer  "hospital_admission_id",        limit: 4
    t.integer  "payment_way_id",               limit: 4
    t.text     "original_string_text",         limit: 65535
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
    t.integer  "emisor_fiscal_information_id", limit: 4
  end

  add_index "admission_invoices", ["emisor_fiscal_information_id"], name: "index_admission_invoices_on_emisor_fiscal_information_id", using: :btree
  add_index "admission_invoices", ["hospital_admission_id"], name: "index_admission_invoices_on_hospital_admission_id", using: :btree
  add_index "admission_invoices", ["payment_way_id"], name: "index_admission_invoices_on_payment_way_id", using: :btree
  add_index "admission_invoices", ["user_id"], name: "index_admission_invoices_on_user_id", using: :btree

  create_table "admission_transactions", force: :cascade do |t|
    t.string   "folio",                 limit: 255
    t.float    "amount",                limit: 24
    t.integer  "user_id",               limit: 4
    t.integer  "hospital_admission_id", limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.boolean  "is_invoiced"
    t.integer  "clinic_invoice_id",     limit: 4
    t.boolean  "is_cancel"
  end

  add_index "admission_transactions", ["clinic_invoice_id"], name: "index_admission_transactions_on_clinic_invoice_id", using: :btree
  add_index "admission_transactions", ["hospital_admission_id"], name: "index_admission_transactions_on_hospital_admission_id", using: :btree
  add_index "admission_transactions", ["user_id"], name: "index_admission_transactions_on_user_id", using: :btree

  create_table "allergies", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "doctor_id",  limit: 4
  end

  add_index "allergies", ["doctor_id"], name: "index_allergies_on_doctor_id", using: :btree

  create_table "allergies_patients", id: false, force: :cascade do |t|
    t.integer "allergy_id", limit: 4
    t.integer "patient_id", limit: 4
  end

  create_table "annotation_fields", force: :cascade do |t|
    t.integer  "medical_annotation_id",       limit: 4
    t.integer  "medical_annotation_field_id", limit: 4
    t.string   "data",                        limit: 255
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "annotation_fields", ["medical_annotation_field_id"], name: "index_annotation_fields_on_medical_annotation_field_id", using: :btree
  add_index "annotation_fields", ["medical_annotation_id"], name: "index_annotation_fields_on_medical_annotation_id", using: :btree

  create_table "application_ages", force: :cascade do |t|
    t.string   "age",        limit: 255
    t.integer  "age_type",   limit: 4
    t.string   "dose",       limit: 255
    t.string   "comments",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "vaccine_id", limit: 4
  end

  add_index "application_ages", ["vaccine_id"], name: "index_application_ages_on_vaccine_id", using: :btree

  create_table "archives", force: :cascade do |t|
    t.string   "file_name",                  limit: 255
    t.string   "original_name",              limit: 255
    t.integer  "clinical_analysis_order_id", limit: 4
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "archives", ["clinical_analysis_order_id"], name: "index_archives_on_clinical_analysis_order_id", using: :btree

  create_table "asset_activities", force: :cascade do |t|
    t.integer  "fixed_asset_item_id",     limit: 4
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "title",                   limit: 255
    t.string   "details",                 limit: 255
    t.integer  "user_id",                 limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "fixed_asset_activity_id", limit: 4
  end

  add_index "asset_activities", ["fixed_asset_activity_id"], name: "index_asset_activities_on_fixed_asset_activity_id", using: :btree
  add_index "asset_activities", ["fixed_asset_item_id"], name: "index_asset_activities_on_fixed_asset_item_id", using: :btree
  add_index "asset_activities", ["user_id"], name: "index_asset_activities_on_user_id", using: :btree

  create_table "assistants", force: :cascade do |t|
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "user_id",    limit: 4
    t.boolean  "shared"
  end

  add_index "assistants", ["user_id"], name: "index_assistants_on_user_id", using: :btree

  create_table "assistants_doctors", id: false, force: :cascade do |t|
    t.integer "assistant_id", limit: 4
    t.integer "doctor_id",    limit: 4
  end

  create_table "assistants_expedient_permissions", id: false, force: :cascade do |t|
    t.integer "assistant_id",            limit: 4
    t.integer "expedient_permission_id", limit: 4
  end

  add_index "assistants_expedient_permissions", ["assistant_id"], name: "index_assistants_expedient_permissions_on_assistant_id", using: :btree
  add_index "assistants_expedient_permissions", ["expedient_permission_id"], name: "expedient_permission", using: :btree

  create_table "assortment_pharmacy_items", force: :cascade do |t|
    t.integer  "quantity",                 limit: 4
    t.decimal  "cost",                               precision: 10, scale: 2
    t.integer  "pharmacy_item_id",         limit: 4
    t.integer  "warehouse_id",             limit: 4
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.integer  "medicament_assortment_id", limit: 4
  end

  add_index "assortment_pharmacy_items", ["medicament_assortment_id"], name: "index_assortment_pharmacy_items_on_medicament_assortment_id", using: :btree
  add_index "assortment_pharmacy_items", ["pharmacy_item_id"], name: "index_assortment_pharmacy_items_on_pharmacy_item_id", using: :btree
  add_index "assortment_pharmacy_items", ["warehouse_id"], name: "index_assortment_pharmacy_items_on_warehouse_id", using: :btree

  create_table "attention_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "authors", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "last_name",     limit: 255
    t.string   "email",         limit: 255
    t.string   "profile_image", limit: 255
    t.string   "twitter",       limit: 255
    t.string   "company_title", limit: 255
    t.string   "job_title",     limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "banks", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.boolean  "is_local"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "birth_infos", force: :cascade do |t|
    t.datetime "date"
    t.integer  "birth_type_id",          limit: 4
    t.string   "observations",           limit: 255
    t.string   "rpm",                    limit: 255
    t.string   "hospital",               limit: 255
    t.integer  "delivery_type_id",       limit: 4
    t.integer  "episiotomy_type_id",     limit: 4
    t.integer  "tear_type_id",           limit: 4
    t.integer  "duration_type_id",       limit: 4
    t.string   "aditional_observations", limit: 255
    t.integer  "pregnancy_id",           limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "user_id",                limit: 4
  end

  add_index "birth_infos", ["birth_type_id"], name: "index_birth_infos_on_birth_type_id", using: :btree
  add_index "birth_infos", ["delivery_type_id"], name: "index_birth_infos_on_delivery_type_id", using: :btree
  add_index "birth_infos", ["duration_type_id"], name: "index_birth_infos_on_duration_type_id", using: :btree
  add_index "birth_infos", ["episiotomy_type_id"], name: "index_birth_infos_on_episiotomy_type_id", using: :btree
  add_index "birth_infos", ["pregnancy_id"], name: "index_birth_infos_on_pregnancy_id", using: :btree
  add_index "birth_infos", ["tear_type_id"], name: "index_birth_infos_on_tear_type_id", using: :btree
  add_index "birth_infos", ["user_id"], name: "index_birth_infos_on_user_id", using: :btree

  create_table "birth_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "blood_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "ca_groups", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "laboratory_id", limit: 4
  end

  add_index "ca_groups", ["laboratory_id"], name: "index_ca_groups_on_laboratory_id", using: :btree

  create_table "cabinet_report_pictures", force: :cascade do |t|
    t.text     "picture_root",      limit: 65535
    t.text     "annotations",       limit: 65535
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "cabinet_report_id", limit: 4
  end

  add_index "cabinet_report_pictures", ["cabinet_report_id"], name: "index_cabinet_report_pictures_on_cabinet_report_id", using: :btree

  create_table "cabinet_reports", force: :cascade do |t|
    t.text     "conclusions",                limit: 65535
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "clinical_analysis_order_id", limit: 4
    t.boolean  "is_charged"
    t.float    "price",                      limit: 24
    t.boolean  "invoiced"
    t.integer  "invoice_id",                 limit: 4
    t.integer  "user_id",                    limit: 4
  end

  add_index "cabinet_reports", ["clinical_analysis_order_id"], name: "index_cabinet_reports_on_clinical_analysis_order_id", using: :btree
  add_index "cabinet_reports", ["invoice_id"], name: "index_cabinet_reports_on_invoice_id", using: :btree
  add_index "cabinet_reports", ["user_id"], name: "index_cabinet_reports_on_user_id", using: :btree

  create_table "cardiovascular_aps", force: :cascade do |t|
    t.string   "description",                  limit: 255
    t.string   "chest_pain",                   limit: 255
    t.string   "edema",                        limit: 255
    t.string   "dyspnoea",                     limit: 255
    t.string   "paroxysmal_nocturnal_dyspnea", limit: 255
    t.string   "orthopnea",                    limit: 255
    t.string   "palpitations",                 limit: 255
    t.string   "syncope_presyncope",           limit: 255
    t.string   "peripheral_vascular",          limit: 255
    t.integer  "medical_history_id",           limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "cardiovascular_aps", ["medical_history_id"], name: "index_cardiovascular_aps_on_medical_history_id", using: :btree

  create_table "category_imcs", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "certificates", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.text     "container_html",      limit: 65535
    t.integer  "office_id",           limit: 4
    t.integer  "patient_id",          limit: 4
    t.integer  "type_certificate_id", limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "certificates", ["office_id"], name: "index_certificates_on_office_id", using: :btree
  add_index "certificates", ["patient_id"], name: "index_certificates_on_patient_id", using: :btree
  add_index "certificates", ["type_certificate_id"], name: "index_certificates_on_type_certificate_id", using: :btree

  create_table "charge_report_studies", force: :cascade do |t|
    t.integer  "cabinet_report_id", limit: 4
    t.string   "details",           limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "charge_report_studies", ["cabinet_report_id"], name: "index_charge_report_studies_on_cabinet_report_id", using: :btree

  create_table "charges", force: :cascade do |t|
    t.integer  "medical_consultation_id", limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "details",                 limit: 255
  end

  add_index "charges", ["medical_consultation_id"], name: "index_charges_on_medical_consultation_id", using: :btree

  create_table "child_non_pathological_infos", force: :cascade do |t|
    t.integer  "medical_history_id",   limit: 4
    t.string   "house_type",           limit: 255
    t.integer  "number_people",        limit: 4
    t.string   "child_academic_grade", limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "child_non_pathological_infos", ["medical_history_id"], name: "index_child_non_pathological_infos_on_medical_history_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "state_id",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "clinic_infos", force: :cascade do |t|
    t.string   "name",                          limit: 255
    t.string   "address",                       limit: 255
    t.string   "telephone",                     limit: 255
    t.string   "fax",                           limit: 255
    t.string   "logo",                          limit: 255
    t.integer  "city_id",                       limit: 4
    t.integer  "user_id",                       limit: 4
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.boolean  "charge_reports"
    t.string   "contact_email",                 limit: 255
    t.boolean  "set_custom_ticket_description"
    t.string   "ticket_description",            limit: 255
    t.string   "ticket_series",                 limit: 255
    t.integer  "ticket_next_folio",             limit: 4
  end

  add_index "clinic_infos", ["city_id"], name: "index_clinic_infos_on_city_id", using: :btree
  add_index "clinic_infos", ["user_id"], name: "index_clinic_infos_on_user_id", using: :btree

  create_table "clinic_invoice_concepts", force: :cascade do |t|
    t.integer  "quantity",          limit: 4
    t.text     "description",       limit: 65535
    t.decimal  "unit_price",                      precision: 11, scale: 2
    t.decimal  "cost",                            precision: 11, scale: 2
    t.integer  "clinic_invoice_id", limit: 4
    t.integer  "measure_unit_id",   limit: 4
    t.text     "unit",              limit: 65535
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
  end

  add_index "clinic_invoice_concepts", ["clinic_invoice_id"], name: "index_clinic_invoice_concepts_on_clinic_invoice_id", using: :btree
  add_index "clinic_invoice_concepts", ["measure_unit_id"], name: "index_clinic_invoice_concepts_on_measure_unit_id", using: :btree

  create_table "clinic_invoice_emisors", force: :cascade do |t|
    t.string   "business_name",     limit: 255
    t.string   "rfc",               limit: 255
    t.string   "address",           limit: 255
    t.string   "suburb",            limit: 255
    t.string   "no_ext",            limit: 255
    t.string   "no_int",            limit: 255
    t.string   "zip",               limit: 255
    t.string   "locality",          limit: 255
    t.string   "city",              limit: 255
    t.string   "state",             limit: 255
    t.string   "country",           limit: 255
    t.string   "tax_regime",        limit: 255
    t.integer  "clinic_invoice_id", limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "clinic_invoice_emisors", ["clinic_invoice_id"], name: "index_clinic_invoice_emisors_on_clinic_invoice_id", using: :btree

  create_table "clinic_invoice_receptors", force: :cascade do |t|
    t.string   "business_name",     limit: 255
    t.string   "rfc",               limit: 255
    t.string   "address",           limit: 255
    t.string   "suburb",            limit: 255
    t.string   "no_ext",            limit: 255
    t.string   "no_int",            limit: 255
    t.string   "zip",               limit: 255
    t.string   "locality",          limit: 255
    t.string   "city",              limit: 255
    t.string   "state",             limit: 255
    t.string   "country",           limit: 255
    t.integer  "clinic_invoice_id", limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "clinic_invoice_receptors", ["clinic_invoice_id"], name: "index_clinic_invoice_receptors_on_clinic_invoice_id", using: :btree

  create_table "clinic_invoices", force: :cascade do |t|
    t.string   "xml_base",                      limit: 255
    t.text     "original_string",               limit: 65535
    t.string   "serie",                         limit: 255
    t.integer  "folio",                         limit: 4
    t.string   "certification_date",            limit: 255
    t.string   "payment_condition",             limit: 255
    t.string   "payment_methos_name",           limit: 255
    t.string   "account_number",                limit: 255
    t.decimal  "subtotal",                                    precision: 10, scale: 2
    t.decimal  "iva",                                         precision: 10, scale: 2
    t.decimal  "isr",                                         precision: 10, scale: 2
    t.string   "UUID",                          limit: 255
    t.text     "xml",                           limit: 65535
    t.integer  "patient_fiscal_information_id", limit: 4
    t.text     "sat_seal",                      limit: 65535
    t.string   "no_cert_sat",                   limit: 255
    t.text     "transmitter_seal",              limit: 65535
    t.string   "qr_code",                       limit: 255
    t.boolean  "cancel"
    t.integer  "invoice_type",                  limit: 4
    t.integer  "user_id",                       limit: 4
    t.integer  "payment_way_id",                limit: 4
    t.text     "original_string_text",          limit: 65535
    t.datetime "created_at",                                                           null: false
    t.datetime "updated_at",                                                           null: false
  end

  add_index "clinic_invoices", ["patient_fiscal_information_id"], name: "index_clinic_invoices_on_patient_fiscal_information_id", using: :btree
  add_index "clinic_invoices", ["payment_way_id"], name: "index_clinic_invoices_on_payment_way_id", using: :btree
  add_index "clinic_invoices", ["user_id"], name: "index_clinic_invoices_on_user_id", using: :btree

  create_table "clinical_analysis_orders", force: :cascade do |t|
    t.integer  "folio",                  limit: 4
    t.date     "date_order"
    t.text     "annotations",            limit: 65535
    t.integer  "medical_expedient_id",   limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "clinical_order_type_id", limit: 4
    t.text     "result",                 limit: 65535
    t.integer  "office_id",              limit: 4
    t.integer  "laboratory_id",          limit: 4
    t.integer  "user_id",                limit: 4
  end

  add_index "clinical_analysis_orders", ["clinical_order_type_id"], name: "index_clinical_analysis_orders_on_clinical_order_type_id", using: :btree
  add_index "clinical_analysis_orders", ["laboratory_id"], name: "index_clinical_analysis_orders_on_laboratory_id", using: :btree
  add_index "clinical_analysis_orders", ["medical_expedient_id"], name: "index_clinical_analysis_orders_on_medical_expedient_id", using: :btree
  add_index "clinical_analysis_orders", ["office_id"], name: "index_clinical_analysis_orders_on_office_id", using: :btree
  add_index "clinical_analysis_orders", ["user_id"], name: "index_clinical_analysis_orders_on_user_id", using: :btree

  create_table "clinical_analysis_orders_studies", id: false, force: :cascade do |t|
    t.integer "clinical_analysis_order_id", limit: 4
    t.integer "clinical_study_id",          limit: 4
  end

  add_index "clinical_analysis_orders_studies", ["clinical_analysis_order_id"], name: "clinical_analysis_order", using: :btree
  add_index "clinical_analysis_orders_studies", ["clinical_study_id"], name: "index_clinical_analysis_orders_studies_on_clinical_study_id", using: :btree

  create_table "clinical_order_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "clinical_studies", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "laboratory_id",          limit: 4
    t.integer  "clinical_study_type_id", limit: 4
    t.float    "price",                  limit: 24
  end

  add_index "clinical_studies", ["clinical_study_type_id"], name: "index_clinical_studies_on_clinical_study_type_id", using: :btree
  add_index "clinical_studies", ["laboratory_id"], name: "index_clinical_studies_on_laboratory_id", using: :btree

  create_table "clinical_studies_indications", id: false, force: :cascade do |t|
    t.integer "clinical_study_id", limit: 4
    t.integer "indication_id",     limit: 4
  end

  create_table "clinical_studies_study_packages", id: false, force: :cascade do |t|
    t.integer "study_package_id",  limit: 4
    t.integer "clinical_study_id", limit: 4
  end

  add_index "clinical_studies_study_packages", ["clinical_study_id"], name: "index_clinical_studies_study_packages_on_clinical_study_id", using: :btree
  add_index "clinical_studies_study_packages", ["study_package_id"], name: "index_clinical_studies_study_packages_on_study_package_id", using: :btree

  create_table "clinical_study_types", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "clinical_order_type_id", limit: 4
    t.integer  "laboratory_id",          limit: 4
  end

  add_index "clinical_study_types", ["clinical_order_type_id"], name: "index_clinical_study_types_on_clinical_order_type_id", using: :btree
  add_index "clinical_study_types", ["laboratory_id"], name: "index_clinical_study_types_on_laboratory_id", using: :btree

  create_table "consultation_costs", force: :cascade do |t|
    t.float    "cost",                 limit: 24
    t.integer  "consultation_type_id", limit: 4
    t.integer  "office_id",            limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "consultation_costs", ["consultation_type_id"], name: "index_consultation_costs_on_consultation_type_id", using: :btree
  add_index "consultation_costs", ["office_id"], name: "index_consultation_costs_on_office_id", using: :btree

  create_table "consultation_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.boolean  "is_local"
  end

  create_table "consultation_types_users", id: false, force: :cascade do |t|
    t.integer "consultation_type_id", limit: 4
    t.integer "user_id",              limit: 4
  end

  add_index "consultation_types_users", ["consultation_type_id"], name: "index_consultation_types_users_on_consultation_type_id", using: :btree
  add_index "consultation_types_users", ["user_id"], name: "index_consultation_types_users_on_user_id", using: :btree

  create_table "consultations_valorations", id: false, force: :cascade do |t|
    t.integer "medical_consultation_id", limit: 4
    t.integer "patient_valoration_id",   limit: 4
  end

  add_index "consultations_valorations", ["medical_consultation_id"], name: "index_consultations_valorations_on_medical_consultation_id", using: :btree
  add_index "consultations_valorations", ["patient_valoration_id"], name: "index_consultations_valorations_on_patient_valoration_id", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "iso",        limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "currencies", force: :cascade do |t|
    t.string   "country",         limit: 255
    t.string   "name",            limit: 255
    t.string   "symbol",          limit: 255
    t.string   "iso_code",        limit: 255
    t.string   "fractional_unit", limit: 255
    t.integer  "base_number",     limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "currency_exchanges", force: :cascade do |t|
    t.integer  "currency_id",    limit: 4
    t.float    "value",          limit: 24
    t.boolean  "is_default"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "user_id",        limit: 4
    t.integer  "clinic_info_id", limit: 4
  end

  add_index "currency_exchanges", ["clinic_info_id"], name: "index_currency_exchanges_on_clinic_info_id", using: :btree
  add_index "currency_exchanges", ["user_id"], name: "index_currency_exchanges_on_user_id", using: :btree

  create_table "custom_template_prescription_fields", force: :cascade do |t|
    t.string   "font",                            limit: 255
    t.boolean  "is_bold"
    t.integer  "size",                            limit: 4
    t.float    "position_top_cm",                 limit: 24
    t.float    "position_left_cm",                limit: 24
    t.boolean  "show_field"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "custom_template_prescription_id", limit: 4
    t.integer  "fields_prescription_id",          limit: 4
    t.string   "section",                         limit: 255
    t.float    "width_box",                       limit: 24
    t.string   "extra",                           limit: 255
    t.string   "label",                           limit: 255
  end

  add_index "custom_template_prescription_fields", ["custom_template_prescription_id"], name: "index_custom_prescription_fields", using: :btree
  add_index "custom_template_prescription_fields", ["fields_prescription_id"], name: "index_custom_fields", using: :btree

  create_table "custom_template_prescriptions", force: :cascade do |t|
    t.float    "header_cm",   limit: 24
    t.float    "footer_cm",   limit: 24
    t.integer  "doctor_id",   limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "orientation", limit: 255
    t.string   "page_size",   limit: 255
  end

  add_index "custom_template_prescriptions", ["doctor_id"], name: "index_custom_template_prescriptions_on_doctor_id", using: :btree

  create_table "custom_template_report_fields", force: :cascade do |t|
    t.string   "font",                      limit: 255
    t.boolean  "is_bold"
    t.integer  "size",                      limit: 4
    t.float    "position_top_cm",           limit: 24
    t.float    "position_left_cm",          limit: 24
    t.boolean  "show_field"
    t.string   "section",                   limit: 255
    t.float    "width_box",                 limit: 24
    t.string   "extra",                     limit: 255
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "custom_template_report_id", limit: 4
    t.integer  "fields_prescription_id",    limit: 4
    t.string   "label",                     limit: 255
  end

  add_index "custom_template_report_fields", ["custom_template_report_id"], name: "index_custom_report_fields", using: :btree
  add_index "custom_template_report_fields", ["fields_prescription_id"], name: "index_custom_fields", using: :btree

  create_table "custom_template_reports", force: :cascade do |t|
    t.float    "header_cm",   limit: 24
    t.float    "footer_cm",   limit: 24
    t.string   "orientation", limit: 255
    t.string   "page_size",   limit: 255
    t.integer  "doctor_id",   limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "custom_template_reports", ["doctor_id"], name: "index_custom_template_reports_on_doctor_id", using: :btree

  create_table "daily_cashes", force: :cascade do |t|
    t.integer  "account_id",    limit: 4
    t.datetime "start_date"
    t.datetime "end_date"
    t.float    "start_balance", limit: 24
    t.float    "end_balance",   limit: 24
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "status",        limit: 4
    t.integer  "user_id",       limit: 4
    t.integer  "close_user_id", limit: 4
  end

  add_index "daily_cashes", ["account_id"], name: "index_daily_cashes_on_account_id", using: :btree
  add_index "daily_cashes", ["user_id"], name: "index_daily_cashes_on_user_id", using: :btree

  create_table "daily_cashes_transactions", id: false, force: :cascade do |t|
    t.integer "daily_cash_id",  limit: 4
    t.integer "transaction_id", limit: 4
  end

  add_index "daily_cashes_transactions", ["daily_cash_id"], name: "index_daily_cashes_transactions_on_daily_cash_id", using: :btree
  add_index "daily_cashes_transactions", ["transaction_id"], name: "index_daily_cashes_transactions_on_transaction_id", using: :btree

  create_table "dc_balances", force: :cascade do |t|
    t.integer  "daily_cash_id", limit: 4
    t.integer  "currency_id",   limit: 4
    t.float    "start",         limit: 24
    t.float    "end",           limit: 24
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "dc_balances", ["currency_id"], name: "index_dc_balances_on_currency_id", using: :btree
  add_index "dc_balances", ["daily_cash_id"], name: "index_dc_balances_on_daily_cash_id", using: :btree

  create_table "default_ginecology_backgrounds", force: :cascade do |t|
    t.boolean  "ginecopatia"
    t.text     "ginecopatia_description",          limit: 65535
    t.boolean  "mastopatia"
    t.text     "mastopatia_description",           limit: 65535
    t.boolean  "gemelaridad"
    t.text     "gemelaridad_description",          limit: 65535
    t.boolean  "congenital_anomalies"
    t.text     "congenital_anomalies_description", limit: 65535
    t.boolean  "infertility"
    t.text     "infertility_description",          limit: 65535
    t.boolean  "others"
    t.text     "others_diseases",                  limit: 65535
    t.text     "annotations",                      limit: 65535
    t.string   "menstruation_start_age",           limit: 255
    t.boolean  "married"
    t.string   "spouse_name",                      limit: 255
    t.integer  "gestations",                       limit: 4
    t.integer  "natural_births",                   limit: 4
    t.string   "cesareans",                        limit: 255
    t.integer  "abortions",                        limit: 4
    t.string   "abortion_causes",                  limit: 255
    t.string   "menstruation_type",                limit: 255
    t.boolean  "sexually_active"
    t.string   "contraceptives_method",            limit: 255
    t.string   "menopause",                        limit: 255
    t.text     "obstetrical_annotations",          limit: 65535
    t.string   "spouse_information",               limit: 255
    t.string   "cycles",                           limit: 255
    t.string   "ivs",                              limit: 255
    t.integer  "sexual_partners",                  limit: 4
    t.string   "std",                              limit: 255
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  create_table "default_no_pathologicals", force: :cascade do |t|
    t.boolean  "smoking"
    t.text     "smoking_description",    limit: 65535
    t.boolean  "alcoholism"
    t.text     "alcoholism_description", limit: 65535
    t.boolean  "play_sport"
    t.text     "play_sport_description", limit: 65535
    t.text     "annotations",            limit: 65535
    t.string   "religion",               limit: 255
    t.string   "scolarship",             limit: 255
    t.boolean  "drugs"
    t.text     "drugs_description",      limit: 65535
    t.integer  "marital_status_id",      limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "default_no_pathologicals", ["marital_status_id"], name: "index_default_no_pathologicals_on_marital_status_id", using: :btree

  create_table "default_pathologicals", force: :cascade do |t|
    t.boolean  "diabetes"
    t.text     "diabetes_description",                  limit: 65535
    t.boolean  "overweight"
    t.text     "overweight_description",                limit: 65535
    t.boolean  "hypertension"
    t.text     "hypertension_description",              limit: 65535
    t.boolean  "asthma"
    t.text     "asthma_description",                    limit: 65535
    t.boolean  "cancer"
    t.text     "cancer_description",                    limit: 65535
    t.text     "annotations",                           limit: 65535
    t.boolean  "other_diseases"
    t.boolean  "psychiatric_diaseases"
    t.string   "psychiatric_diaseases_description",     limit: 255
    t.boolean  "neurological_diseases"
    t.string   "neurological_diseases_description",     limit: 255
    t.boolean  "cardiovascular_diseases"
    t.string   "cardiovascular_diseases_description",   limit: 255
    t.boolean  "bronchopulmonary_diseases"
    t.string   "bronchopulmonary_diseases_description", limit: 255
    t.boolean  "thyroid_diseases"
    t.string   "thyroid_diseases_description",          limit: 255
    t.boolean  "kidney_diseases"
    t.string   "kidney_diseases_description",           limit: 255
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.text     "surgeries",                             limit: 65535
    t.text     "transfusions",                          limit: 65535
    t.boolean  "allergy"
    t.text     "allergy_description",                   limit: 65535
    t.text     "recent_diseases",                       limit: 65535
    t.text     "other_allergies",                       limit: 65535
    t.text     "other_medicaments",                     limit: 65535
  end

  create_table "default_perinatal_backgrounds", force: :cascade do |t|
    t.string   "hospital",        limit: 255
    t.float    "weight",          limit: 24
    t.float    "height",          limit: 24
    t.text     "pregnancy_risks", limit: 65535
    t.text     "other_info",      limit: 65535
    t.string   "scholarship",     limit: 255
    t.string   "house_type",      limit: 255
    t.string   "hounse_inmates",  limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "default_urological_backgrounds", force: :cascade do |t|
    t.boolean  "testicular_pain"
    t.boolean  "impotence"
    t.boolean  "erection_dificulty"
    t.boolean  "premature_ejaculation"
    t.boolean  "vaginal_discharge"
    t.boolean  "anormal_hair_growth"
    t.boolean  "back_pain"
    t.boolean  "kidneys_pain"
    t.boolean  "pelvic_pain"
    t.boolean  "low_abdomen_pain"
    t.boolean  "kidney_bk"
    t.boolean  "kidney_stones"
    t.boolean  "venereal_diseases"
    t.boolean  "self_examination"
    t.boolean  "prostate"
    t.date     "last_prostate_examination"
    t.date     "fum"
    t.integer  "gestations",                limit: 4
    t.date     "last_pap_date"
    t.string   "contraceptive_method",      limit: 255
    t.boolean  "sexually_active"
    t.date     "sex_life_start"
    t.integer  "sexual_partners",           limit: 4
    t.string   "relationship_type",         limit: 255
    t.boolean  "satisfying_sex"
    t.boolean  "intercourse_pain"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "delivery_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "digestive_aps", force: :cascade do |t|
    t.string   "description",        limit: 255
    t.integer  "medical_history_id", limit: 4
    t.string   "anorexia",           limit: 255
    t.string   "polidipsia",         limit: 255
    t.string   "nauseas",            limit: 255
    t.string   "vomitos",            limit: 255
    t.string   "dispepsia",          limit: 255
    t.string   "disfagia",           limit: 255
    t.string   "odinofagia",         limit: 255
    t.string   "rectorragia",        limit: 255
    t.string   "melenas",            limit: 255
    t.string   "abdominalgia",       limit: 255
    t.string   "pirosis",            limit: 255
    t.string   "hematemesis",        limit: 255
    t.string   "acolia",             limit: 255
    t.string   "meteorismo",         limit: 255
    t.string   "tenesmo",            limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "digestive_aps", ["medical_history_id"], name: "index_digestive_aps_on_medical_history_id", using: :btree

  create_table "disease_categories", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "diseases", force: :cascade do |t|
    t.string   "code",                limit: 255
    t.text     "name",                limit: 65535
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "disease_category_id", limit: 4
    t.integer  "doctor_id",           limit: 4
  end

  add_index "diseases", ["disease_category_id"], name: "index_diseases_on_disease_category_id", using: :btree
  add_index "diseases", ["doctor_id"], name: "index_diseases_on_doctor_id", using: :btree

  create_table "diseases_family_diseases", id: false, force: :cascade do |t|
    t.integer "disease_id",        limit: 4
    t.integer "family_disease_id", limit: 4
  end

  add_index "diseases_family_diseases", ["disease_id"], name: "disease_id", using: :btree
  add_index "diseases_family_diseases", ["family_disease_id"], name: "family_disease_id", using: :btree

  create_table "diseases_medical_consultations", id: false, force: :cascade do |t|
    t.integer "medical_consultation_id", limit: 4
    t.integer "disease_id",              limit: 4
  end

  add_index "diseases_medical_consultations", ["disease_id"], name: "index_diseases_medical_consultations_on_disease_id", using: :btree
  add_index "diseases_medical_consultations", ["medical_consultation_id"], name: "index_diseases_medical_consultations_on_medical_consultation_id", using: :btree

  create_table "distribution_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "doctors", force: :cascade do |t|
    t.string   "identity_card",                  limit: 255
    t.string   "curp",                           limit: 255
    t.string   "digital_sign",                   limit: 255
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "fiscal_information_id",          limit: 4
    t.integer  "user_id",                        limit: 4
    t.integer  "laboratory_id",                  limit: 4
    t.text     "picture",                        limit: 65535
    t.string   "pres_speciality",                limit: 255
    t.string   "pres_studies",                   limit: 255
    t.string   "pres_professional_license",      limit: 255
    t.string   "pres_another_info",              limit: 255
    t.boolean  "accept_term_conditions"
    t.boolean  "pres_show_another_info"
    t.boolean  "pres_show_professional_license"
    t.boolean  "pres_show_speciality"
    t.boolean  "pres_show_studies"
    t.boolean  "pres_show_height"
    t.boolean  "pres_show_weight"
    t.boolean  "pres_show_folio"
    t.boolean  "pres_show_logo"
    t.integer  "time_zone_id",                   limit: 4
    t.boolean  "is_active_custom_prescription"
    t.boolean  "pres_show_head_circumference"
    t.boolean  "charge_reports"
    t.boolean  "iva_report"
    t.boolean  "iva_report_included"
    t.boolean  "is_active_custom_report"
    t.boolean  "searcher_visible"
  end

  add_index "doctors", ["fiscal_information_id"], name: "index_doctors_on_fiscal_information_id", using: :btree
  add_index "doctors", ["laboratory_id"], name: "index_doctors_on_laboratory_id", using: :btree
  add_index "doctors", ["time_zone_id"], name: "index_doctors_on_time_zone_id", using: :btree
  add_index "doctors", ["user_id"], name: "index_doctors_on_user_id", using: :btree

  create_table "doctors_laboratories", id: false, force: :cascade do |t|
    t.integer "doctor_id",     limit: 4
    t.integer "laboratory_id", limit: 4
  end

  add_index "doctors_laboratories", ["doctor_id"], name: "index_doctors_laboratories_on_doctor_id", using: :btree
  add_index "doctors_laboratories", ["laboratory_id"], name: "index_doctors_laboratories_on_laboratory_id", using: :btree

  create_table "doctors_user_patients", id: false, force: :cascade do |t|
    t.integer "doctor_id",       limit: 4, null: false
    t.integer "user_patient_id", limit: 4, null: false
  end

  add_index "doctors_user_patients", ["doctor_id"], name: "fk_rails_170f55cdac", using: :btree
  add_index "doctors_user_patients", ["user_patient_id"], name: "fk_rails_ee4f427707", using: :btree

  create_table "doctors_users", id: false, force: :cascade do |t|
    t.integer "doctor_id", limit: 4
    t.integer "user_id",   limit: 4
  end

  add_index "doctors_users", ["doctor_id"], name: "index_doctors_users_on_doctor_id", using: :btree
  add_index "doctors_users", ["user_id"], name: "index_doctors_users_on_user_id", using: :btree

  create_table "duration_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "email_settings", force: :cascade do |t|
    t.string   "host",       limit: 255
    t.string   "username",   limit: 255
    t.string   "password",   limit: 255
    t.integer  "port",       limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "doctor_id",  limit: 4
  end

  add_index "email_settings", ["doctor_id"], name: "index_email_settings_on_doctor_id", using: :btree

  create_table "emisor_fiscal_informations", force: :cascade do |t|
    t.string   "bussiness_name",       limit: 255
    t.string   "fiscal_address",       limit: 255
    t.string   "ext_number",           limit: 255
    t.string   "certificate_stamp",    limit: 255
    t.string   "certificate_key",      limit: 255
    t.string   "certificate_password", limit: 255
    t.string   "tax_regime",           limit: 255
    t.integer  "city_id",              limit: 4
    t.string   "rfc",                  limit: 255
    t.string   "zip",                  limit: 255
    t.string   "int_number",           limit: 255
    t.string   "locality",             limit: 255
    t.string   "suburb",               limit: 255
    t.string   "serie",                limit: 255
    t.integer  "folio",                limit: 4
    t.string   "iva",                  limit: 255
    t.string   "isr",                  limit: 255
    t.string   "certificate_number",   limit: 255
    t.text     "certificate_base64",   limit: 65535
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "emisor_fiscal_informations", ["city_id"], name: "index_emisor_fiscal_informations_on_city_id", using: :btree

  create_table "episiotomy_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "expedient_permissions", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "expedient_requests", force: :cascade do |t|
    t.integer  "medical_expedient_id", limit: 4
    t.integer  "status",               limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "health_history_id",    limit: 4
    t.boolean  "is_default"
  end

  add_index "expedient_requests", ["health_history_id"], name: "index_expedient_requests_on_health_history_id", using: :btree
  add_index "expedient_requests", ["medical_expedient_id"], name: "index_expedient_requests_on_medical_expedient_id", using: :btree

  create_table "exploration_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "explorations", force: :cascade do |t|
    t.string   "e_type",                  limit: 255
    t.string   "breasts",                 limit: 255
    t.string   "vulva",                   limit: 255
    t.string   "abdomen",                 limit: 255
    t.string   "cervix",                  limit: 255
    t.string   "ovaries",                 limit: 255
    t.string   "annexed",                 limit: 255
    t.string   "edemas",                  limit: 255
    t.string   "uterine_height",          limit: 255
    t.integer  "medical_consultation_id", limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "head",                    limit: 255
    t.string   "lungs",                   limit: 255
    t.string   "heart",                   limit: 255
    t.string   "extremities",             limit: 255
    t.string   "other",                   limit: 255
    t.string   "vagina",                  limit: 255
    t.string   "pelvis",                  limit: 255
    t.string   "rectum",                  limit: 255
    t.integer  "exploration_type_id",     limit: 4
  end

  add_index "explorations", ["exploration_type_id"], name: "index_explorations_on_exploration_type_id", using: :btree
  add_index "explorations", ["medical_consultation_id"], name: "index_explorations_on_medical_consultation_id", using: :btree

  create_table "family_diseases", force: :cascade do |t|
    t.boolean  "diabetes"
    t.text     "diabetes_description",                  limit: 65535
    t.boolean  "overweight"
    t.text     "overweight_description",                limit: 65535
    t.boolean  "hypertension"
    t.text     "hypertension_description",              limit: 65535
    t.boolean  "asthma"
    t.text     "asthma_description",                    limit: 65535
    t.boolean  "cancer"
    t.text     "cancer_description",                    limit: 65535
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.text     "annotations",                           limit: 65535
    t.boolean  "other_diseases"
    t.boolean  "psychiatric_diseases"
    t.string   "psychiatric_diseases_description",      limit: 255
    t.boolean  "neurological_diseases"
    t.string   "neurological_diseases_description",     limit: 255
    t.boolean  "cardiovascular_diseases"
    t.string   "cardiovascular_diseases_description",   limit: 255
    t.boolean  "bronchopulmonary_diseases"
    t.string   "bronchopulmonary_diseases_description", limit: 255
    t.boolean  "thyroid_diseases"
    t.string   "thyroid_diseases_description",          limit: 255
    t.boolean  "kidney_diseases"
    t.string   "kidney_diseases_description",           limit: 255
  end

  create_table "features", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.decimal  "cost",                   precision: 10
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "key",        limit: 255
  end

  create_table "field_prescription_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "fields_prescriptions", force: :cascade do |t|
    t.string   "name",                       limit: 255
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "field_prescription_type_id", limit: 4
  end

  add_index "fields_prescriptions", ["field_prescription_type_id"], name: "index_fields_prescriptions_on_field_prescription_type_id", using: :btree

  create_table "finkok_configs", force: :cascade do |t|
    t.string   "test_username", limit: 255
    t.string   "test_password", limit: 255
    t.string   "prod_username", limit: 255
    t.string   "prod_password", limit: 255
    t.string   "test_wsdl",     limit: 255
    t.string   "prod_wsdl",     limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "environment",   limit: 255
  end

  create_table "fiscal_informations", force: :cascade do |t|
    t.string   "bussiness_name",       limit: 255
    t.string   "fiscal_address",       limit: 255
    t.string   "ext_number",           limit: 255
    t.string   "certificate_stamp",    limit: 255
    t.string   "certificate_key",      limit: 255
    t.text     "certificate_password", limit: 65535
    t.string   "tax_regime",           limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "city_id",              limit: 4
    t.string   "rfc",                  limit: 255
    t.string   "zip",                  limit: 255
    t.string   "int_number",           limit: 255
    t.string   "locality",             limit: 255
    t.string   "suburb",               limit: 255
    t.string   "serie",                limit: 255
    t.integer  "folio",                limit: 4
    t.float    "iva",                  limit: 24
    t.float    "isr",                  limit: 24
    t.string   "certificate_number",   limit: 255
    t.text     "certificate_base64",   limit: 65535
  end

  add_index "fiscal_informations", ["city_id"], name: "index_fiscal_informations_on_city_id", using: :btree

  create_table "fixed_asset_activities", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.integer  "fixed_asset_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "color",          limit: 255
  end

  add_index "fixed_asset_activities", ["fixed_asset_id"], name: "index_fixed_asset_activities_on_fixed_asset_id", using: :btree

  create_table "fixed_asset_categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "fixed_asset_items", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.string   "description",    limit: 255
    t.string   "identifier",     limit: 255
    t.boolean  "state"
    t.boolean  "schedule"
    t.integer  "fixed_asset_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "fixed_asset_items", ["fixed_asset_id"], name: "index_fixed_asset_items_on_fixed_asset_id", using: :btree

  create_table "fixed_assets", force: :cascade do |t|
    t.string   "name",                    limit: 255
    t.integer  "fixed_asset_category_id", limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "color",                   limit: 255
  end

  add_index "fixed_assets", ["fixed_asset_category_id"], name: "index_fixed_assets_on_fixed_asset_category_id", using: :btree

  create_table "genders", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "general_accounts", force: :cascade do |t|
    t.string   "account_number", limit: 255
    t.string   "clabe",          limit: 255
    t.float    "balance",        limit: 24
    t.string   "details",        limit: 255
    t.integer  "bank_id",        limit: 4
    t.integer  "currency_id",    limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "general_accounts", ["bank_id"], name: "index_general_accounts_on_bank_id", using: :btree
  add_index "general_accounts", ["currency_id"], name: "index_general_accounts_on_currency_id", using: :btree

  create_table "general_consultation_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.float    "cost",       limit: 24
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "general_consultation_types_users", id: false, force: :cascade do |t|
    t.integer "general_consultation_type_id", limit: 4
    t.integer "user_id",                      limit: 4
  end

  create_table "general_fiscal_infos", force: :cascade do |t|
    t.text     "business_name", limit: 65535
    t.string   "rfc",           limit: 255
    t.string   "address",       limit: 255
    t.string   "locality",      limit: 255
    t.string   "ext_number",    limit: 255
    t.string   "int_number",    limit: 255
    t.string   "zip",           limit: 255
    t.string   "suburb",        limit: 255
    t.integer  "city_id",       limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "person_type",   limit: 255
    t.integer  "user_id",       limit: 4
  end

  add_index "general_fiscal_infos", ["city_id"], name: "index_general_fiscal_infos_on_city_id", using: :btree
  add_index "general_fiscal_infos", ["user_id"], name: "index_general_fiscal_infos_on_user_id", using: :btree

  create_table "genital_aps", force: :cascade do |t|
    t.string   "description",        limit: 255
    t.integer  "medical_history_id", limit: 4
    t.text     "menarquia",          limit: 65535
    t.text     "menstrual_rhythm",   limit: 65535
    t.text     "hypermenorrea",      limit: 65535
    t.text     "amenorrea",          limit: 65535
    t.text     "metrorragia",        limit: 65535
    t.text     "leucorrea",          limit: 65535
    t.text     "dismenorrea",        limit: 65535
    t.text     "dispareunia",        limit: 65535
    t.text     "impotence",          limit: 65535
    t.text     "libido",             limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "genital_aps", ["medical_history_id"], name: "index_genital_aps_on_medical_history_id", using: :btree

  create_table "gynecologist_obstetrical_backgrounds", force: :cascade do |t|
    t.string   "menstruation_start_age", limit: 255
    t.boolean  "married"
    t.string   "spouse_name",            limit: 255
    t.integer  "gestations",             limit: 4
    t.integer  "natural_birth",          limit: 4
    t.string   "caesarean_surgery",      limit: 255
    t.integer  "abortion_number",        limit: 4
    t.string   "abortion_causes",        limit: 255
    t.string   "menstruation_type",      limit: 255
    t.boolean  "sexually_active"
    t.string   "contraceptives_method",  limit: 255
    t.string   "menopause",              limit: 255
    t.string   "anotations",             limit: 255
    t.integer  "medical_history_id",     limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "information_spouse",     limit: 255
    t.datetime "menses_last_date"
    t.datetime "papanicolau_last_date"
    t.string   "cycles",                 limit: 255
    t.string   "ivs",                    limit: 255
    t.date     "fup"
    t.integer  "sexual_partners",        limit: 4
    t.string   "relationship_type",      limit: 255
    t.string   "std",                    limit: 255
  end

  add_index "gynecologist_obstetrical_backgrounds", ["medical_history_id"], name: "index_gynecologist_obstetrical_backgrounds_on_medical_history_id", using: :btree

  create_table "health_histories", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.integer  "user_patient_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "health_histories", ["user_patient_id"], name: "index_health_histories_on_user_patient_id", using: :btree

  create_table "hematological_sis", force: :cascade do |t|
    t.string   "description",        limit: 255
    t.text     "astenia",            limit: 65535
    t.text     "palidez",            limit: 65535
    t.text     "hemorrhages",        limit: 65535
    t.text     "adenopathies",       limit: 65535
    t.integer  "medical_history_id", limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "hematological_sis", ["medical_history_id"], name: "index_hematological_sis_on_medical_history_id", using: :btree

  create_table "hospital_admission_discounts", force: :cascade do |t|
    t.integer  "hospital_admission_id", limit: 4
    t.float    "amount",                limit: 24
    t.integer  "user_id",               limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "hospital_admission_discounts", ["hospital_admission_id"], name: "index_hospital_admission_discounts_on_hospital_admission_id", using: :btree
  add_index "hospital_admission_discounts", ["user_id"], name: "index_hospital_admission_discounts_on_user_id", using: :btree

  create_table "hospital_admissions", force: :cascade do |t|
    t.integer  "user_id",           limit: 4
    t.integer  "doctor_id",         limit: 4
    t.integer  "patient_id",        limit: 4
    t.integer  "triage_id",         limit: 4
    t.integer  "attention_type_id", limit: 4
    t.datetime "admission_date"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.float    "total",             limit: 24
    t.float    "payment",           limit: 24
    t.float    "debt",              limit: 24
    t.boolean  "is_closed"
  end

  add_index "hospital_admissions", ["attention_type_id"], name: "index_hospital_admissions_on_attention_type_id", using: :btree
  add_index "hospital_admissions", ["doctor_id"], name: "index_hospital_admissions_on_doctor_id", using: :btree
  add_index "hospital_admissions", ["patient_id"], name: "index_hospital_admissions_on_patient_id", using: :btree
  add_index "hospital_admissions", ["triage_id"], name: "index_hospital_admissions_on_triage_id", using: :btree
  add_index "hospital_admissions", ["user_id"], name: "index_hospital_admissions_on_user_id", using: :btree

  create_table "hospital_service_categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "hospital_services", force: :cascade do |t|
    t.text     "name",                         limit: 65535
    t.boolean  "is_user"
    t.integer  "user_id",                      limit: 4
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.float    "cost",                         limit: 24
    t.boolean  "is_fixed_cost"
    t.string   "code",                         limit: 255
    t.integer  "hospital_service_category_id", limit: 4
  end

  add_index "hospital_services", ["hospital_service_category_id"], name: "index_hospital_services_on_hospital_service_category_id", using: :btree
  add_index "hospital_services", ["user_id"], name: "index_hospital_services_on_user_id", using: :btree

  create_table "hospitalizations_annotations", id: false, force: :cascade do |t|
    t.integer "hospital_admission_id", limit: 4
    t.integer "medical_annotation_id", limit: 4
  end

  add_index "hospitalizations_annotations", ["hospital_admission_id"], name: "index_hospitalizations_annotations_on_hospital_admission_id", using: :btree
  add_index "hospitalizations_annotations", ["medical_annotation_id"], name: "index_hospitalizations_annotations_on_medical_annotation_id", using: :btree

  create_table "hospitalizations_valorations", id: false, force: :cascade do |t|
    t.integer "hospital_admission_id", limit: 4
    t.integer "patient_valoration_id", limit: 4
  end

  add_index "hospitalizations_valorations", ["hospital_admission_id"], name: "index_hospitalizations_valorations_on_hospital_admission_id", using: :btree
  add_index "hospitalizations_valorations", ["patient_valoration_id"], name: "index_hospitalizations_valorations_on_patient_valoration_id", using: :btree

  create_table "hospitals", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.integer  "city_id",     limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "hospitals", ["city_id"], name: "index_hospitals_on_city_id", using: :btree

  create_table "identity_cards", force: :cascade do |t|
    t.string   "identity",     limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "doctor_id",    limit: 4
    t.integer  "specialty_id", limit: 4
    t.string   "institute",    limit: 255
  end

  add_index "identity_cards", ["doctor_id"], name: "index_identity_cards_on_doctor_id", using: :btree
  add_index "identity_cards", ["specialty_id"], name: "index_identity_cards_on_specialty_id", using: :btree

  create_table "imc_adults", force: :cascade do |t|
    t.float    "imc",               limit: 24
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "category_imc_id",   limit: 4
    t.integer  "rank_imc_adult_id", limit: 4
    t.integer  "association_id",    limit: 4
  end

  add_index "imc_adults", ["association_id"], name: "index_imc_adults_on_association_id", using: :btree
  add_index "imc_adults", ["category_imc_id"], name: "index_imc_adults_on_category_imc_id", using: :btree
  add_index "imc_adults", ["rank_imc_adult_id"], name: "index_imc_adults_on_rank_imc_adult_id", using: :btree

  create_table "imc_children_max19_years", force: :cascade do |t|
    t.integer  "age_month",           limit: 4
    t.float    "danger_malnutrition", limit: 24
    t.float    "normal",              limit: 24
    t.float    "overweight",          limit: 24
    t.float    "obesity",             limit: 24
    t.string   "unit_weight",         limit: 255
    t.integer  "gender_id",           limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "imc_children_max19_years", ["gender_id"], name: "index_imc_children_max19_years_on_gender_id", using: :btree

  create_table "index_comments", force: :cascade do |t|
    t.integer  "p_type",     limit: 4
    t.string   "prefix",     limit: 255
    t.string   "first_name", limit: 255
    t.string   "last_name",  limit: 255
    t.date     "birth_date"
    t.string   "job_title",  limit: 255
    t.string   "university", limit: 255
    t.string   "gender",     limit: 255
    t.text     "comments",   limit: 65535
    t.string   "photo",      limit: 255
    t.boolean  "active"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "indications", force: :cascade do |t|
    t.text     "description", limit: 65535
    t.integer  "priority",    limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "ca_group_id", limit: 4
  end

  add_index "indications", ["ca_group_id"], name: "index_indications_on_ca_group_id", using: :btree

  create_table "infection_ots", force: :cascade do |t|
    t.string   "description",                 limit: 255
    t.text     "contacts",                    limit: 65535
    t.text     "stings",                      limit: 65535
    t.text     "environment",                 limit: 65535
    t.text     "foods",                       limit: 65535
    t.text     "travels",                     limit: 65535
    t.text     "internal_surgery_prosthesis", limit: 65535
    t.text     "intravenous_drugs",           limit: 65535
    t.text     "tattoos",                     limit: 65535
    t.text     "transfusions",                limit: 65535
    t.text     "sexual_activity",             limit: 65535
    t.integer  "medical_history_id",          limit: 4
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "infection_ots", ["medical_history_id"], name: "index_infection_ots_on_medical_history_id", using: :btree

  create_table "inherited_family_backgrounds", force: :cascade do |t|
    t.boolean  "gynecological_diseases"
    t.string   "gynecological_diseases_description", limit: 255
    t.boolean  "mammary_dysplasia"
    t.string   "mammary_dysplasia_description",      limit: 255
    t.boolean  "twinhood"
    t.string   "twinhood_description",               limit: 255
    t.boolean  "congenital_anomalies"
    t.string   "congenital_anomalies_description",   limit: 255
    t.boolean  "infertility"
    t.string   "infertility_description",            limit: 255
    t.boolean  "others"
    t.string   "others_description",                 limit: 255
    t.string   "anotations",                         limit: 255
    t.integer  "medical_history_id",                 limit: 4
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  add_index "inherited_family_backgrounds", ["medical_history_id"], name: "index_inherited_family_backgrounds_on_medical_history_id", using: :btree

  create_table "insurances", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "address",    limit: 255
    t.string   "telephone",  limit: 255
    t.string   "contact",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.boolean  "is_active"
  end

  create_table "insurence_prices", force: :cascade do |t|
    t.decimal  "price",                         precision: 10, scale: 2
    t.integer  "hospital_service_id", limit: 4
    t.integer  "insurance_id",        limit: 4
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
  end

  add_index "insurence_prices", ["hospital_service_id"], name: "index_insurence_prices_on_hospital_service_id", using: :btree
  add_index "insurence_prices", ["insurance_id"], name: "index_insurence_prices_on_insurance_id", using: :btree

  create_table "inventories", force: :cascade do |t|
    t.datetime "inventory_date"
    t.integer  "warehouse_id",           limit: 4
    t.integer  "medicament_category_id", limit: 4
    t.integer  "created_user_id",        limit: 4
    t.integer  "updated_user_id",        limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "is_closed"
  end

  add_index "inventories", ["medicament_category_id"], name: "index_inventories_on_medicament_category_id", using: :btree
  add_index "inventories", ["warehouse_id"], name: "index_inventories_on_warehouse_id", using: :btree

  create_table "inventory_pharmacy_stocks", force: :cascade do |t|
    t.integer  "quantity",         limit: 4
    t.integer  "pharmacy_item_id", limit: 4
    t.integer  "inventory_id",     limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "inventory_pharmacy_stocks", ["inventory_id"], name: "index_inventory_pharmacy_stocks_on_inventory_id", using: :btree
  add_index "inventory_pharmacy_stocks", ["pharmacy_item_id"], name: "index_inventory_pharmacy_stocks_on_pharmacy_item_id", using: :btree

  create_table "inventory_stocks", force: :cascade do |t|
    t.integer  "inventory_id", limit: 4
    t.integer  "quantity",     limit: 4
    t.integer  "lot_id",       limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "inventory_stocks", ["inventory_id"], name: "index_inventory_stocks_on_inventory_id", using: :btree
  add_index "inventory_stocks", ["lot_id"], name: "index_inventory_stocks_on_lot_id", using: :btree

  create_table "invoice_concepts", force: :cascade do |t|
    t.integer  "quantity",        limit: 4
    t.text     "description",     limit: 65535
    t.decimal  "unit_price",                    precision: 11, scale: 2
    t.decimal  "cost",                          precision: 11, scale: 2
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.integer  "invoice_id",      limit: 4
    t.integer  "measure_unit_id", limit: 4
    t.string   "unit",            limit: 255
  end

  add_index "invoice_concepts", ["invoice_id"], name: "index_invoice_concepts_on_invoice_id", using: :btree
  add_index "invoice_concepts", ["measure_unit_id"], name: "index_invoice_concepts_on_measure_unit_id", using: :btree

  create_table "invoice_emisors", force: :cascade do |t|
    t.string   "business_name", limit: 255
    t.string   "rfc",           limit: 255
    t.string   "address",       limit: 255
    t.string   "suburb",        limit: 255
    t.string   "no_ext",        limit: 255
    t.string   "no_int",        limit: 255
    t.string   "zip",           limit: 255
    t.string   "locality",      limit: 255
    t.string   "city",          limit: 255
    t.string   "state",         limit: 255
    t.string   "country",       limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "invoice_id",    limit: 4
    t.string   "tax_regime",    limit: 255
  end

  add_index "invoice_emisors", ["invoice_id"], name: "index_invoice_emisors_on_invoice_id", using: :btree

  create_table "invoices", force: :cascade do |t|
    t.string   "xml_base",                      limit: 255
    t.text     "original_string",               limit: 65535
    t.datetime "created_at",                                                           null: false
    t.datetime "updated_at",                                                           null: false
    t.string   "serie",                         limit: 255
    t.integer  "folio",                         limit: 4
    t.string   "certification_date",            limit: 255
    t.string   "payment_condition",             limit: 255
    t.string   "payment_methods_name",          limit: 255
    t.string   "account_number",                limit: 255
    t.decimal  "subtotal",                                    precision: 11, scale: 2
    t.decimal  "iva",                                         precision: 11, scale: 2
    t.decimal  "isr",                                         precision: 11, scale: 2
    t.string   "UUID",                          limit: 255
    t.text     "xml",                           limit: 65535
    t.integer  "patient_fiscal_information_id", limit: 4
    t.text     "sat_seal",                      limit: 65535
    t.string   "no_cert_sat",                   limit: 255
    t.text     "transmitter_seal",              limit: 65535
    t.string   "qr_code",                       limit: 255
    t.integer  "payment_method_id",             limit: 4
    t.boolean  "cancel"
    t.integer  "invoice_type",                  limit: 4
    t.integer  "doctor_id",                     limit: 4
    t.integer  "payment_way_id",                limit: 4
    t.text     "original_string_text",          limit: 65535
  end

  add_index "invoices", ["doctor_id"], name: "index_invoices_on_doctor_id", using: :btree
  add_index "invoices", ["patient_fiscal_information_id"], name: "index_invoices_on_patient_fiscal_information_id", using: :btree
  add_index "invoices", ["payment_method_id"], name: "index_invoices_on_payment_method_id", using: :btree
  add_index "invoices", ["payment_way_id"], name: "index_invoices_on_payment_way_id", using: :btree

  create_table "laboratories", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "address",                limit: 255
    t.string   "telephone",              limit: 255
    t.string   "email",                  limit: 255
    t.string   "logo",                   limit: 255
    t.string   "observations",           limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "city_id",                limit: 4
    t.string   "contact",                limit: 255
    t.integer  "clinical_order_type_id", limit: 4
    t.boolean  "is_custom"
  end

  add_index "laboratories", ["city_id"], name: "index_laboratories_on_city_id", using: :btree
  add_index "laboratories", ["clinical_order_type_id"], name: "index_laboratories_on_clinical_order_type_id", using: :btree

  create_table "license_features", force: :cascade do |t|
    t.decimal  "cost",                 precision: 10
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "license_id", limit: 4
    t.integer  "feature_id", limit: 4
  end

  add_index "license_features", ["feature_id"], name: "index_license_features_on_feature_id", using: :btree
  add_index "license_features", ["license_id"], name: "index_license_features_on_license_id", using: :btree

  create_table "licenses", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.string   "status",               limit: 255
    t.boolean  "current"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "doctor_id",            limit: 4
    t.integer  "distribution_type_id", limit: 4
    t.text     "detail",               limit: 65535
    t.string   "code",                 limit: 255
  end

  add_index "licenses", ["distribution_type_id"], name: "index_licenses_on_distribution_type_id", using: :btree
  add_index "licenses", ["doctor_id"], name: "index_licenses_on_doctor_id", using: :btree

  create_table "locomotor_sis", force: :cascade do |t|
    t.integer  "medical_history_id",  limit: 4
    t.text     "mialgias",            limit: 65535
    t.text     "artralgias",          limit: 65535
    t.text     "articular_rigideces", limit: 65535
    t.text     "inflammations",       limit: 65535
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "locomotor_sis", ["medical_history_id"], name: "index_locomotor_sis_on_medical_history_id", using: :btree

  create_table "lots", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.date     "expiration_date"
    t.integer  "medicament_id",   limit: 4
    t.integer  "user_id",         limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "lots", ["medicament_id"], name: "index_lots_on_medicament_id", using: :btree
  add_index "lots", ["user_id"], name: "index_lots_on_user_id", using: :btree

  create_table "marital_statuses", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "male",       limit: 255
    t.string   "female",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "measure_unit_items", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "measure_units", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "medical_annotation_fields", force: :cascade do |t|
    t.text     "config",                     limit: 65535
    t.integer  "medical_annotation_type_id", limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "medical_annotation_fields", ["medical_annotation_type_id"], name: "index_medical_annotation_fields_on_medical_annotation_type_id", using: :btree

  create_table "medical_annotation_files", force: :cascade do |t|
    t.integer  "medical_annotation_id", limit: 4
    t.integer  "user_id",               limit: 4
    t.string   "document",              limit: 255
    t.string   "notes",                 limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "medical_annotation_files", ["medical_annotation_id"], name: "index_medical_annotation_files_on_medical_annotation_id", using: :btree
  add_index "medical_annotation_files", ["user_id"], name: "index_medical_annotation_files_on_user_id", using: :btree

  create_table "medical_annotation_images", force: :cascade do |t|
    t.integer  "medical_annotation_type_id", limit: 4
    t.string   "f_key",                      limit: 255
    t.string   "image",                      limit: 255
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "medical_annotation_images", ["medical_annotation_type_id"], name: "index_medical_annotation_images_on_medical_annotation_type_id", using: :btree

  create_table "medical_annotation_types", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.text     "template",       limit: 65535
    t.boolean  "require_upload"
    t.boolean  "add_valoration"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "template_file",  limit: 255
  end

  create_table "medical_annotations", force: :cascade do |t|
    t.integer  "medical_annotation_type_id", limit: 4
    t.integer  "patient_id",                 limit: 4
    t.integer  "user_id",                    limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "medical_annotations", ["medical_annotation_type_id"], name: "index_medical_annotations_on_medical_annotation_type_id", using: :btree
  add_index "medical_annotations", ["patient_id"], name: "index_medical_annotations_on_patient_id", using: :btree
  add_index "medical_annotations", ["user_id"], name: "index_medical_annotations_on_user_id", using: :btree

  create_table "medical_annotations_valorations", id: false, force: :cascade do |t|
    t.integer "medical_annotation_id", limit: 4
    t.integer "patient_valoration_id", limit: 4
  end

  add_index "medical_annotations_valorations", ["medical_annotation_id"], name: "index_medical_annotations_valorations_on_medical_annotation_id", using: :btree
  add_index "medical_annotations_valorations", ["patient_valoration_id"], name: "index_medical_annotations_valorations_on_patient_valoration_id", using: :btree

  create_table "medical_consultations", force: :cascade do |t|
    t.text     "current_condition",    limit: 65535
    t.datetime "date_consultation"
    t.text     "diagnostic",           limit: 65535
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "medical_expedient_id", limit: 4
    t.integer  "office_id",            limit: 4
    t.integer  "vital_sign_id",        limit: 4
    t.float    "price",                limit: 24
    t.boolean  "print_diagnostic"
    t.integer  "consultation_type_id", limit: 4
    t.text     "notes",                limit: 65535
    t.integer  "folio",                limit: 4
    t.boolean  "is_charged"
    t.boolean  "invoiced"
    t.integer  "invoice_id",           limit: 4
    t.integer  "user_id",              limit: 4
  end

  add_index "medical_consultations", ["consultation_type_id"], name: "index_medical_consultations_on_consultation_type_id", using: :btree
  add_index "medical_consultations", ["invoice_id"], name: "index_medical_consultations_on_invoice_id", using: :btree
  add_index "medical_consultations", ["medical_expedient_id"], name: "index_medical_consultations_on_medical_expedient_id", using: :btree
  add_index "medical_consultations", ["office_id"], name: "index_medical_consultations_on_office_id", using: :btree
  add_index "medical_consultations", ["user_id"], name: "index_medical_consultations_on_user_id", using: :btree
  add_index "medical_consultations", ["vital_sign_id"], name: "index_medical_consultations_on_vital_sign_id", using: :btree

  create_table "medical_consultations_pregnancies", id: false, force: :cascade do |t|
    t.integer "medical_consultation_id", limit: 4
    t.integer "pregnancy_id",            limit: 4
  end

  add_index "medical_consultations_pregnancies", ["medical_consultation_id"], name: "mcp_consultation", using: :btree
  add_index "medical_consultations_pregnancies", ["pregnancy_id"], name: "mcp_pregnancy", using: :btree

  create_table "medical_expedients", force: :cascade do |t|
    t.string   "code",                 limit: 255
    t.integer  "patient_id",           limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "folio",                limit: 4
    t.integer  "active",               limit: 4
    t.text     "general_observations", limit: 65535
    t.integer  "user_id",              limit: 4
  end

  add_index "medical_expedients", ["patient_id"], name: "index_medical_expedients_on_patient_id", using: :btree
  add_index "medical_expedients", ["user_id"], name: "index_medical_expedients_on_user_id", using: :btree

  create_table "medical_histories", force: :cascade do |t|
    t.string   "recent_diseases",                limit: 255
    t.string   "other_medicines",                limit: 255
    t.string   "transfusions",                   limit: 255
    t.string   "surgeries",                      limit: 255
    t.boolean  "allergy_medicament"
    t.string   "allergy_medicament_description", limit: 255
    t.string   "other_allergies",                limit: 255
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "medical_expedient_id",           limit: 4
    t.integer  "family_disease_id",              limit: 4
  end

  add_index "medical_histories", ["family_disease_id"], name: "index_medical_histories_on_family_disease_id", using: :btree
  add_index "medical_histories", ["medical_expedient_id"], name: "index_medical_histories_on_medical_expedient_id", using: :btree

  create_table "medical_histories_medicaments", force: :cascade do |t|
    t.integer "medical_history_id", limit: 4
    t.integer "medicament_id",      limit: 4
  end

  add_index "medical_histories_medicaments", ["medical_history_id"], name: "index_medical_histories_medicaments_on_medical_history_id", using: :btree
  add_index "medical_histories_medicaments", ["medicament_id"], name: "index_medical_histories_medicaments_on_medicament_id", using: :btree

  create_table "medical_indications", force: :cascade do |t|
    t.text     "dose",            limit: 65535
    t.integer  "medicament_id",   limit: 4
    t.integer  "prescription_id", limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "medical_indications", ["medicament_id"], name: "index_medical_indications_on_medicament_id", using: :btree
  add_index "medical_indications", ["prescription_id"], name: "index_medical_indications_on_prescription_id", using: :btree

  create_table "medicament_assortment_items", force: :cascade do |t|
    t.integer  "quantity",                 limit: 4
    t.float    "cost",                     limit: 24
    t.integer  "lot_id",                   limit: 4
    t.integer  "medicament_id",            limit: 4
    t.integer  "medicament_assortment_id", limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "medicament_assortment_items", ["lot_id"], name: "index_medicament_assortment_items_on_lot_id", using: :btree
  add_index "medicament_assortment_items", ["medicament_assortment_id"], name: "index_medicament_assortment_items_on_medicament_assortment_id", using: :btree
  add_index "medicament_assortment_items", ["medicament_id"], name: "index_medicament_assortment_items_on_medicament_id", using: :btree

  create_table "medicament_assortments", force: :cascade do |t|
    t.integer  "folio",                 limit: 4
    t.float    "cost",                  limit: 24
    t.integer  "user_id",               limit: 4
    t.integer  "warehouse_id",          limit: 4
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.integer  "hospital_admission_id", limit: 4
    t.boolean  "is_paid"
    t.float    "residue",               limit: 24
    t.decimal  "discount",                         precision: 10, scale: 2
  end

  add_index "medicament_assortments", ["hospital_admission_id"], name: "index_medicament_assortments_on_hospital_admission_id", using: :btree
  add_index "medicament_assortments", ["user_id"], name: "index_medicament_assortments_on_user_id", using: :btree
  add_index "medicament_assortments", ["warehouse_id"], name: "index_medicament_assortments_on_warehouse_id", using: :btree

  create_table "medicament_categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "medicament_inventories", force: :cascade do |t|
    t.integer  "quantity",     limit: 4
    t.integer  "lot_id",       limit: 4
    t.integer  "warehouse_id", limit: 4
    t.integer  "user_id",      limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "medicament_inventories", ["lot_id"], name: "index_medicament_inventories_on_lot_id", using: :btree
  add_index "medicament_inventories", ["user_id"], name: "index_medicament_inventories_on_user_id", using: :btree
  add_index "medicament_inventories", ["warehouse_id"], name: "index_medicament_inventories_on_warehouse_id", using: :btree

  create_table "medicament_laboratories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "medicament_logs", force: :cascade do |t|
    t.integer  "quantity",       limit: 4
    t.boolean  "income"
    t.integer  "user_id",        limit: 4
    t.integer  "warehouse_id",   limit: 4
    t.integer  "lot_id",         limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "traceable_id",   limit: 4
    t.string   "traceable_type", limit: 255
  end

  add_index "medicament_logs", ["lot_id"], name: "index_medicament_logs_on_lot_id", using: :btree
  add_index "medicament_logs", ["traceable_type", "traceable_id"], name: "index_medicament_logs_on_traceable_type_and_traceable_id", using: :btree
  add_index "medicament_logs", ["user_id"], name: "index_medicament_logs_on_user_id", using: :btree
  add_index "medicament_logs", ["warehouse_id"], name: "index_medicament_logs_on_warehouse_id", using: :btree

  create_table "medicament_movements", force: :cascade do |t|
    t.integer  "quantity",          limit: 4
    t.integer  "user_id",           limit: 4
    t.integer  "lot_id",            limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "from_warehouse_id", limit: 4
    t.integer  "to_warehouse_id",   limit: 4
  end

  add_index "medicament_movements", ["lot_id"], name: "index_medicament_movements_on_lot_id", using: :btree
  add_index "medicament_movements", ["user_id"], name: "index_medicament_movements_on_user_id", using: :btree

  create_table "medicament_purchase_farmacy_items", force: :cascade do |t|
    t.integer  "quantity",               limit: 4
    t.decimal  "cost",                             precision: 10, scale: 2
    t.integer  "medicament_purchase_id", limit: 4
    t.integer  "pharmacy_item_id",       limit: 4
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
  end

  add_index "medicament_purchase_farmacy_items", ["medicament_purchase_id"], name: "purchase_pharmacy_item_id", using: :btree
  add_index "medicament_purchase_farmacy_items", ["pharmacy_item_id"], name: "index_medicament_purchase_farmacy_items_on_pharmacy_item_id", using: :btree

  create_table "medicament_purchase_items", force: :cascade do |t|
    t.integer  "quantity",               limit: 4
    t.float    "cost",                   limit: 24
    t.integer  "lot_id",                 limit: 4
    t.integer  "medicament_id",          limit: 4
    t.integer  "medicament_purchase_id", limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "medicament_purchase_items", ["lot_id"], name: "index_medicament_purchase_items_on_lot_id", using: :btree
  add_index "medicament_purchase_items", ["medicament_id"], name: "index_medicament_purchase_items_on_medicament_id", using: :btree
  add_index "medicament_purchase_items", ["medicament_purchase_id"], name: "index_medicament_purchase_items_on_medicament_purchase_id", using: :btree

  create_table "medicament_purchases", force: :cascade do |t|
    t.float    "cost",           limit: 24
    t.integer  "folio",          limit: 4
    t.string   "invoice_number", limit: 255
    t.integer  "supplier_id",    limit: 4
    t.integer  "user_id",        limit: 4
    t.integer  "warehouse_id",   limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "medicament_purchases", ["supplier_id"], name: "index_medicament_purchases_on_supplier_id", using: :btree
  add_index "medicament_purchases", ["user_id"], name: "index_medicament_purchases_on_user_id", using: :btree
  add_index "medicament_purchases", ["warehouse_id"], name: "index_medicament_purchases_on_warehouse_id", using: :btree

  create_table "medicament_wastes", force: :cascade do |t|
    t.integer  "lot_id",        limit: 4
    t.integer  "warehouse_id",  limit: 4
    t.integer  "user_id",       limit: 4
    t.text     "cause",         limit: 65535
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "quantity",      limit: 4
    t.integer  "waste_type_id", limit: 4
  end

  add_index "medicament_wastes", ["lot_id"], name: "index_medicament_wastes_on_lot_id", using: :btree
  add_index "medicament_wastes", ["user_id"], name: "index_medicament_wastes_on_user_id", using: :btree
  add_index "medicament_wastes", ["warehouse_id"], name: "index_medicament_wastes_on_warehouse_id", using: :btree
  add_index "medicament_wastes", ["waste_type_id"], name: "index_medicament_wastes_on_waste_type_id", using: :btree

  create_table "medicaments", force: :cascade do |t|
    t.integer  "medicament_category_id",   limit: 4
    t.text     "comercial_name",           limit: 65535
    t.text     "presentation",             limit: 65535
    t.text     "active_substance",         limit: 65535
    t.text     "contraindication",         limit: 65535
    t.string   "brand",                    limit: 255
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.integer  "medicament_laboratory_id", limit: 4
    t.float    "cost",                     limit: 24
    t.boolean  "is_consigment"
    t.decimal  "sale_cost",                              precision: 10, scale: 2
    t.text     "tags",                     limit: 65535
    t.string   "code",                     limit: 255
  end

  add_index "medicaments", ["medicament_category_id"], name: "index_medicaments_on_medicament_category_id", using: :btree
  add_index "medicaments", ["medicament_laboratory_id"], name: "index_medicaments_on_medicament_laboratory_id", using: :btree

  create_table "medicaments_pathological_medicines", id: false, force: :cascade do |t|
    t.integer "pathological_medicine_id", limit: 4
    t.integer "medicament_id",            limit: 4
  end

  add_index "medicaments_pathological_medicines", ["medicament_id"], name: "medicament_id", using: :btree
  add_index "medicaments_pathological_medicines", ["pathological_medicine_id"], name: "pathological_medicine_id", using: :btree

  create_table "metabolic_sis", force: :cascade do |t|
    t.string   "description",        limit: 255
    t.text     "weight",             limit: 65535
    t.text     "size_body_shape",    limit: 65535
    t.text     "asthenia",           limit: 65535
    t.text     "weakness",           limit: 65535
    t.text     "appetite",           limit: 65535
    t.text     "thirst",             limit: 65535
    t.text     "hot_cold",           limit: 65535
    t.text     "sex",                limit: 65535
    t.text     "galactorrea",        limit: 65535
    t.text     "age_puberty",        limit: 65535
    t.integer  "medical_history_id", limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "metabolic_sis", ["medical_history_id"], name: "index_metabolic_sis_on_medical_history_id", using: :btree

  create_table "movement_categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.boolean  "m_type"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "movement_types", force: :cascade do |t|
    t.string   "name",                 limit: 255
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "movement_category_id", limit: 4
  end

  add_index "movement_types", ["movement_category_id"], name: "index_movement_types_on_movement_category_id", using: :btree

  create_table "neonate_infos", force: :cascade do |t|
    t.string   "child_name",       limit: 255
    t.integer  "gender_id",        limit: 4
    t.datetime "birthdate"
    t.float    "weight",           limit: 24
    t.float    "height",           limit: 24
    t.integer  "apgar_appearance", limit: 4
    t.integer  "apgar_pulse",      limit: 4
    t.integer  "apgar_gesture",    limit: 4
    t.integer  "apgar_activity",   limit: 4
    t.integer  "apgar_breathing",  limit: 4
    t.string   "abnormalities",    limit: 255
    t.string   "observations",     limit: 255
    t.string   "attended",         limit: 255
    t.string   "pediatrician",     limit: 255
    t.integer  "birth_info_id",    limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "blood_type_id",    limit: 4
  end

  add_index "neonate_infos", ["birth_info_id"], name: "index_neonate_infos_on_birth_info_id", using: :btree
  add_index "neonate_infos", ["blood_type_id"], name: "index_neonate_infos_on_blood_type_id", using: :btree
  add_index "neonate_infos", ["gender_id"], name: "index_neonate_infos_on_gender_id", using: :btree

  create_table "nervous_sis", force: :cascade do |t|
    t.string   "description",        limit: 255
    t.text     "headache",           limit: 65535
    t.text     "convulsions",        limit: 65535
    t.text     "transitory_deficit", limit: 65535
    t.text     "confusion",          limit: 65535
    t.text     "obnubilation",       limit: 65535
    t.text     "march",              limit: 65535
    t.text     "balance",            limit: 65535
    t.text     "language",           limit: 65535
    t.text     "sleep_vigil",        limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "medical_history_id", limit: 4
  end

  add_index "nervous_sis", ["medical_history_id"], name: "index_nervous_sis_on_medical_history_id", using: :btree

  create_table "news_items", force: :cascade do |t|
    t.string   "banner",     limit: 255
    t.string   "title",      limit: 255
    t.string   "summary",    limit: 255
    t.text     "content",    limit: 65535
    t.integer  "status",     limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "author_id",  limit: 4
  end

  add_index "news_items", ["author_id"], name: "index_news_items_on_author_id", using: :btree

  create_table "no_pathologicals", force: :cascade do |t|
    t.boolean  "smoking"
    t.text     "smoking_description",    limit: 65535
    t.boolean  "alcoholism"
    t.text     "alcoholism_description", limit: 65535
    t.boolean  "play_sport"
    t.text     "play_sport_description", limit: 65535
    t.text     "annotations",            limit: 65535
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "medical_history_id",     limit: 4
    t.integer  "marital_status_id",      limit: 4
    t.string   "religion",               limit: 255
    t.string   "scholarship",            limit: 255
    t.boolean  "drugs"
    t.string   "drugs_description",      limit: 255
  end

  add_index "no_pathologicals", ["marital_status_id"], name: "index_no_pathologicals_on_marital_status_id", using: :btree
  add_index "no_pathologicals", ["medical_history_id"], name: "index_no_pathologicals_on_medical_history_id", using: :btree

  create_table "office_permissions", force: :cascade do |t|
    t.integer  "assistant_id",  limit: 4
    t.integer  "office_id",     limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "permission_id", limit: 4
  end

  add_index "office_permissions", ["assistant_id"], name: "index_office_permissions_on_assistant_id", using: :btree
  add_index "office_permissions", ["office_id"], name: "index_office_permissions_on_office_id", using: :btree
  add_index "office_permissions", ["permission_id"], name: "index_office_permissions_on_permission_id", using: :btree

  create_table "office_times", force: :cascade do |t|
    t.time     "start"
    t.time     "end"
    t.integer  "day",        limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "office_id",  limit: 4
  end

  add_index "office_times", ["office_id"], name: "index_office_times_on_office_id", using: :btree

  create_table "offices", force: :cascade do |t|
    t.string   "name",                     limit: 255
    t.string   "address",                  limit: 255
    t.string   "telephone",                limit: 255
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "consult_duration",         limit: 255
    t.integer  "template_prescription_id", limit: 4
    t.string   "logo",                     limit: 255
    t.integer  "city_id",                  limit: 4
    t.integer  "hospital_id",              limit: 4
    t.string   "lat",                      limit: 255
    t.string   "lng",                      limit: 255
    t.boolean  "active"
    t.integer  "folio",                    limit: 4
  end

  add_index "offices", ["city_id"], name: "index_offices_on_city_id", using: :btree
  add_index "offices", ["hospital_id"], name: "index_offices_on_hospital_id", using: :btree
  add_index "offices", ["template_prescription_id"], name: "index_offices_on_template_prescription_id", using: :btree

  create_table "offices_users", id: false, force: :cascade do |t|
    t.integer "office_id", limit: 4
    t.integer "user_id",   limit: 4
  end

  add_index "offices_users", ["office_id"], name: "index_offices_users_on_office_id", using: :btree
  add_index "offices_users", ["user_id"], name: "index_offices_users_on_user_id", using: :btree

  create_table "open_invoice_receptors", force: :cascade do |t|
    t.string   "business_name", limit: 255
    t.string   "rfc",           limit: 255
    t.string   "address",       limit: 255
    t.string   "suburb",        limit: 255
    t.string   "no_ext",        limit: 255
    t.string   "no_int",        limit: 255
    t.string   "zip",           limit: 255
    t.string   "locality",      limit: 255
    t.string   "city",          limit: 255
    t.string   "state",         limit: 255
    t.string   "country",       limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "invoice_id",    limit: 4
  end

  add_index "open_invoice_receptors", ["invoice_id"], name: "index_open_invoice_receptors_on_invoice_id", using: :btree

  create_table "operation_roles", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "ophthalmological_sis", force: :cascade do |t|
    t.text     "agudeza",            limit: 65535
    t.text     "diplopia",           limit: 65535
    t.text     "eye_pain",           limit: 65535
    t.text     "photofobia",         limit: 65535
    t.text     "amaurosis",          limit: 65535
    t.text     "fotopsias",          limit: 65535
    t.text     "miodesopsias",       limit: 65535
    t.text     "picor",              limit: 65535
    t.text     "pitanas",            limit: 65535
    t.integer  "medical_history_id", limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "ophthalmological_sis", ["medical_history_id"], name: "index_ophthalmological_sis_on_medical_history_id", using: :btree

  create_table "other_charge_items", force: :cascade do |t|
    t.decimal  "service_amount",                precision: 10, scale: 2
    t.decimal  "service_discount",              precision: 10, scale: 2
    t.decimal  "total",                         precision: 10, scale: 2
    t.integer  "other_charge_id",     limit: 4
    t.integer  "hospital_service_id", limit: 4
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
  end

  add_index "other_charge_items", ["hospital_service_id"], name: "index_other_charge_items_on_hospital_service_id", using: :btree
  add_index "other_charge_items", ["other_charge_id"], name: "index_other_charge_items_on_other_charge_id", using: :btree

  create_table "other_charge_transactions", force: :cascade do |t|
    t.string   "folio",           limit: 255
    t.decimal  "amount",                      precision: 10, scale: 2
    t.integer  "user_id",         limit: 4
    t.integer  "other_charge_id", limit: 4
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.boolean  "is_cancel"
  end

  add_index "other_charge_transactions", ["other_charge_id"], name: "index_other_charge_transactions_on_other_charge_id", using: :btree
  add_index "other_charge_transactions", ["user_id"], name: "index_other_charge_transactions_on_user_id", using: :btree

  create_table "other_charges", force: :cascade do |t|
    t.string   "patient_name",   limit: 255
    t.decimal  "total_amount",               precision: 10, scale: 2
    t.decimal  "total_discount",             precision: 10, scale: 2
    t.integer  "user_id",        limit: 4
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.boolean  "is_closed"
  end

  add_index "other_charges", ["user_id"], name: "index_other_charges_on_user_id", using: :btree

  create_table "other_movements", force: :cascade do |t|
    t.integer  "movement_type_id", limit: 4
    t.string   "details",          limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "other_movements", ["movement_type_id"], name: "index_other_movements_on_movement_type_id", using: :btree

  create_table "otolaryngology_sis", force: :cascade do |t|
    t.text     "otalgia",            limit: 65535
    t.integer  "medical_history_id", limit: 4
    t.text     "otorrea",            limit: 65535
    t.text     "otorragia",          limit: 65535
    t.text     "hypoacusia",         limit: 65535
    t.text     "epistaxis",          limit: 65535
    t.text     "rinorrea",           limit: 65535
    t.text     "odinofagia",         limit: 65535
    t.text     "phonation",          limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "otolaryngology_sis", ["medical_history_id"], name: "index_otolaryngology_sis_on_medical_history_id", using: :btree

  create_table "package_service_costs", force: :cascade do |t|
    t.decimal  "cost",                          precision: 10, scale: 2
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.integer  "service_package_id",  limit: 4
    t.integer  "hospital_service_id", limit: 4
  end

  add_index "package_service_costs", ["hospital_service_id"], name: "index_package_service_costs_on_hospital_service_id", using: :btree
  add_index "package_service_costs", ["service_package_id"], name: "index_package_service_costs_on_service_package_id", using: :btree

  create_table "parent_infos", force: :cascade do |t|
    t.integer  "child_non_pathological_info_id", limit: 4
    t.string   "first_name",                     limit: 255
    t.string   "last_name",                      limit: 255
    t.string   "gender",                         limit: 255
    t.date     "birth_date"
    t.string   "academic_grade",                 limit: 255
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "blood_type_id",                  limit: 4
  end

  add_index "parent_infos", ["blood_type_id"], name: "index_parent_infos_on_blood_type_id", using: :btree
  add_index "parent_infos", ["child_non_pathological_info_id"], name: "index_parent_infos_on_child_non_pathological_info_id", using: :btree

  create_table "pathological_allergies", force: :cascade do |t|
    t.boolean  "allergy_medications"
    t.text     "allergy_medications_description", limit: 65535
    t.text     "other_allergies",                 limit: 65535
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  create_table "pathological_conditions", force: :cascade do |t|
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "pathological_medicines", force: :cascade do |t|
    t.text     "other_medicines", limit: 65535
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "pathologicals", force: :cascade do |t|
    t.integer  "section_id",                  limit: 4
    t.string   "section_type",                limit: 255
    t.integer  "subcategory_pathological_id", limit: 4
    t.integer  "order",                       limit: 4
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "medical_history_id",          limit: 4
  end

  add_index "pathologicals", ["medical_history_id"], name: "index_pathologicals_on_medical_history_id", using: :btree
  add_index "pathologicals", ["section_type", "section_id"], name: "index_pathologicals_on_section_type_and_section_id", using: :btree
  add_index "pathologicals", ["subcategory_pathological_id"], name: "index_pathologicals_on_subcategory_pathological_id", using: :btree

  create_table "patient_fiscal_informations", force: :cascade do |t|
    t.string   "person_type",   limit: 255
    t.string   "business_name", limit: 255
    t.string   "rfc",           limit: 255
    t.string   "address",       limit: 255
    t.string   "locality",      limit: 255
    t.string   "ext_number",    limit: 255
    t.string   "int_number",    limit: 255
    t.string   "zip",           limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "patient_id",    limit: 4
    t.integer  "city_id",       limit: 4
    t.string   "suburb",        limit: 255
    t.string   "email",         limit: 255
    t.string   "email2",        limit: 255
  end

  add_index "patient_fiscal_informations", ["city_id"], name: "index_patient_fiscal_informations_on_city_id", using: :btree
  add_index "patient_fiscal_informations", ["patient_id"], name: "index_patient_fiscal_informations_on_patient_id", using: :btree

  create_table "patient_valoration_fields", force: :cascade do |t|
    t.integer  "patient_valoration_id", limit: 4
    t.integer  "valoration_field_id",   limit: 4
    t.string   "data",                  limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "patient_valoration_fields", ["patient_valoration_id"], name: "index_patient_valoration_fields_on_patient_valoration_id", using: :btree
  add_index "patient_valoration_fields", ["valoration_field_id"], name: "index_patient_valoration_fields_on_valoration_field_id", using: :btree

  create_table "patient_valorations", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.integer  "valoration_type_id", limit: 4
    t.integer  "patient_id",         limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "patient_valorations", ["patient_id"], name: "index_patient_valorations_on_patient_id", using: :btree
  add_index "patient_valorations", ["user_id"], name: "index_patient_valorations_on_user_id", using: :btree
  add_index "patient_valorations", ["valoration_type_id"], name: "index_patient_valorations_on_valoration_type_id", using: :btree

  create_table "patients", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.string   "last_name",          limit: 255
    t.date     "birth_day"
    t.float    "weight",             limit: 24
    t.float    "height",             limit: 24
    t.string   "phone",              limit: 255
    t.string   "cell",               limit: 255
    t.string   "email",              limit: 255
    t.integer  "city_id",            limit: 4
    t.string   "address",            limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "picture",            limit: 255
    t.string   "birthplace",         limit: 255
    t.integer  "license_type",       limit: 4
    t.date     "license_start_date"
    t.integer  "active",             limit: 4
    t.integer  "gender_id",          limit: 4
    t.string   "occupation",         limit: 255
    t.integer  "systolic_pressure",  limit: 4
    t.integer  "diastolic_pressure", limit: 4
    t.integer  "user_id",            limit: 4
    t.integer  "blood_type_id",      limit: 4
    t.integer  "insurance_id",       limit: 4
  end

  add_index "patients", ["blood_type_id"], name: "index_patients_on_blood_type_id", using: :btree
  add_index "patients", ["city_id"], name: "index_patients_on_city_id", using: :btree
  add_index "patients", ["gender_id"], name: "index_patients_on_gender_id", using: :btree
  add_index "patients", ["insurance_id"], name: "index_patients_on_insurance_id", using: :btree
  add_index "patients", ["user_id"], name: "index_patients_on_user_id", using: :btree

  create_table "patients_vaccine_books", id: false, force: :cascade do |t|
    t.integer "patient_id",      limit: 4
    t.integer "vaccine_book_id", limit: 4
  end

  create_table "payment_methods", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.boolean  "affect_local"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "code",         limit: 255
  end

  create_table "payment_ways", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "last_name",  limit: 255
    t.date     "birth_day"
    t.string   "telephone",  limit: 255
    t.string   "cellphone",  limit: 255
    t.string   "address",    limit: 255
    t.integer  "city_id",    limit: 4
    t.integer  "gender_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "people", ["city_id"], name: "index_people_on_city_id", using: :btree
  add_index "people", ["gender_id"], name: "index_people_on_gender_id", using: :btree

  create_table "percentile_cdcs", force: :cascade do |t|
    t.integer  "gender_id",          limit: 4
    t.float    "months",             limit: 24
    t.float    "l",                  limit: 24
    t.float    "m",                  limit: 24
    t.float    "s",                  limit: 24
    t.float    "p3",                 limit: 24
    t.float    "p5",                 limit: 24
    t.float    "p10",                limit: 24
    t.float    "p25",                limit: 24
    t.float    "p50",                limit: 24
    t.float    "p75",                limit: 24
    t.float    "p85",                limit: 24
    t.float    "p90",                limit: 24
    t.float    "p95",                limit: 24
    t.float    "p97",                limit: 24
    t.integer  "type_percentile_id", limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "percentile_cdcs", ["gender_id"], name: "index_percentile_cdcs_on_gender_id", using: :btree
  add_index "percentile_cdcs", ["type_percentile_id"], name: "index_percentile_cdcs_on_type_percentile_id", using: :btree

  create_table "percentile_children", force: :cascade do |t|
    t.integer  "age_day",            limit: 4
    t.float    "p01",                limit: 24
    t.float    "p1",                 limit: 24
    t.float    "p3",                 limit: 24
    t.float    "p5",                 limit: 24
    t.float    "p10",                limit: 24
    t.float    "p15",                limit: 24
    t.float    "p25",                limit: 24
    t.float    "p50",                limit: 24
    t.float    "p75",                limit: 24
    t.float    "p85",                limit: 24
    t.float    "p90",                limit: 24
    t.float    "p95",                limit: 24
    t.float    "p97",                limit: 24
    t.float    "p99",                limit: 24
    t.float    "p999",               limit: 24
    t.integer  "gender_id",          limit: 4
    t.integer  "type_percentile_id", limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "percentile_children", ["gender_id"], name: "index_percentile_children_on_gender_id", using: :btree
  add_index "percentile_children", ["type_percentile_id"], name: "index_percentile_children_on_type_percentile_id", using: :btree

  create_table "perinatal_informations", force: :cascade do |t|
    t.integer  "medical_history_id", limit: 4
    t.string   "hospital",           limit: 255
    t.integer  "city_id",            limit: 4
    t.float    "weight",             limit: 24
    t.float    "height",             limit: 24
    t.string   "pregnat_risks",      limit: 255
    t.string   "other_info",         limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "perinatal_informations", ["city_id"], name: "index_perinatal_informations_on_city_id", using: :btree
  add_index "perinatal_informations", ["medical_history_id"], name: "index_perinatal_informations_on_medical_history_id", using: :btree

  create_table "permissions", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "label",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "personalsupports", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "lastname",          limit: 255
    t.string   "email",             limit: 255
    t.string   "contact_number",    limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "operation_role_id", limit: 4
    t.integer  "doctor_id",         limit: 4
  end

  add_index "personalsupports", ["doctor_id"], name: "index_personalsupports_on_doctor_id", using: :btree
  add_index "personalsupports", ["operation_role_id"], name: "index_personalsupports_on_operation_role_id", using: :btree

  create_table "pharmacy_item_inventories", force: :cascade do |t|
    t.integer  "quantity",         limit: 4
    t.integer  "user_id",          limit: 4
    t.integer  "warehouse_id",     limit: 4
    t.integer  "pharmacy_item_id", limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "pharmacy_item_inventories", ["pharmacy_item_id"], name: "index_pharmacy_item_inventories_on_pharmacy_item_id", using: :btree
  add_index "pharmacy_item_inventories", ["user_id"], name: "index_pharmacy_item_inventories_on_user_id", using: :btree
  add_index "pharmacy_item_inventories", ["warehouse_id"], name: "index_pharmacy_item_inventories_on_warehouse_id", using: :btree

  create_table "pharmacy_item_logs", force: :cascade do |t|
    t.integer  "quantity",         limit: 4
    t.boolean  "income"
    t.integer  "traceable_id",     limit: 4
    t.string   "traceable_type",   limit: 255
    t.integer  "user_id",          limit: 4
    t.integer  "pharmacy_item_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "warehouse_id",     limit: 4
  end

  add_index "pharmacy_item_logs", ["pharmacy_item_id"], name: "index_pharmacy_item_logs_on_pharmacy_item_id", using: :btree
  add_index "pharmacy_item_logs", ["traceable_type", "traceable_id"], name: "index_pharmacy_on_chargeable", using: :btree
  add_index "pharmacy_item_logs", ["user_id"], name: "index_pharmacy_item_logs_on_user_id", using: :btree
  add_index "pharmacy_item_logs", ["warehouse_id"], name: "index_pharmacy_item_logs_on_warehouse_id", using: :btree

  create_table "pharmacy_item_movements", force: :cascade do |t|
    t.integer  "quantity",          limit: 4
    t.integer  "pharmacy_item_id",  limit: 4
    t.integer  "user_id",           limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "from_warehouse_id", limit: 4
    t.integer  "to_warehouse_id",   limit: 4
  end

  add_index "pharmacy_item_movements", ["pharmacy_item_id"], name: "index_pharmacy_item_movements_on_pharmacy_item_id", using: :btree
  add_index "pharmacy_item_movements", ["user_id"], name: "index_pharmacy_item_movements_on_user_id", using: :btree

  create_table "pharmacy_item_wastes", force: :cascade do |t|
    t.integer  "quantity",         limit: 4
    t.text     "cause",            limit: 65535
    t.integer  "user_id",          limit: 4
    t.integer  "pharmacy_item_id", limit: 4
    t.integer  "warehouse_id",     limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "waste_type_id",    limit: 4
  end

  add_index "pharmacy_item_wastes", ["pharmacy_item_id"], name: "index_pharmacy_item_wastes_on_pharmacy_item_id", using: :btree
  add_index "pharmacy_item_wastes", ["user_id"], name: "index_pharmacy_item_wastes_on_user_id", using: :btree
  add_index "pharmacy_item_wastes", ["warehouse_id"], name: "index_pharmacy_item_wastes_on_warehouse_id", using: :btree
  add_index "pharmacy_item_wastes", ["waste_type_id"], name: "index_pharmacy_item_wastes_on_waste_type_id", using: :btree

  create_table "pharmacy_items", force: :cascade do |t|
    t.string   "code",                   limit: 255
    t.string   "description",            limit: 255
    t.float    "cost",                   limit: 24
    t.integer  "measure_unit_item_id",   limit: 4
    t.datetime "created_at",                                                    null: false
    t.datetime "updated_at",                                                    null: false
    t.integer  "medicament_category_id", limit: 4
    t.boolean  "is_consigment"
    t.decimal  "sale_cost",                            precision: 10, scale: 2
    t.text     "tags",                   limit: 65535
  end

  add_index "pharmacy_items", ["measure_unit_item_id"], name: "index_pharmacy_items_on_measure_unit_item_id", using: :btree
  add_index "pharmacy_items", ["medicament_category_id"], name: "index_pharmacy_items_on_medicament_category_id", using: :btree

  create_table "pregnancies", force: :cascade do |t|
    t.integer  "medical_expedient_id",   limit: 4
    t.boolean  "active"
    t.date     "last_menstruation_date"
    t.date     "probable_birth_date"
    t.float    "initial_weight",         limit: 24
    t.string   "baby_name",              limit: 255
    t.boolean  "pregnancy_end_success"
    t.date     "pregnancy_end_date"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "user_id",                limit: 4
  end

  add_index "pregnancies", ["medical_expedient_id"], name: "index_pregnancies_on_medical_expedient_id", using: :btree
  add_index "pregnancies", ["user_id"], name: "index_pregnancies_on_user_id", using: :btree

  create_table "prescriptions", force: :cascade do |t|
    t.text     "recommendations",         limit: 65535
    t.integer  "medical_consultation_id", limit: 4
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "prescriptions", ["medical_consultation_id"], name: "index_prescriptions_on_medical_consultation_id", using: :btree

  create_table "psychosomatic_sis", force: :cascade do |t|
    t.string   "description",        limit: 255
    t.integer  "medical_history_id", limit: 4
    t.text     "personalidad",       limit: 65535
    t.text     "ansiedad",           limit: 65535
    t.text     "animo",              limit: 65535
    t.text     "amnesia",            limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "psychosomatic_sis", ["medical_history_id"], name: "index_psychosomatic_sis_on_medical_history_id", using: :btree

  create_table "rank_imc_adults", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "respiratory_aps", force: :cascade do |t|
    t.string   "description",        limit: 255
    t.integer  "medical_history_id", limit: 4
    t.string   "cough",              limit: 255
    t.string   "chest_pain",         limit: 255
    t.string   "dyspnoea",           limit: 255
    t.string   "hemoptisis",         limit: 255
    t.string   "epistaxis",          limit: 255
    t.string   "cyanosis",           limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "respiratory_aps", ["medical_history_id"], name: "index_respiratory_aps_on_medical_history_id", using: :btree

  create_table "sense_organs", force: :cascade do |t|
    t.integer  "medical_history_id", limit: 4
    t.text     "hair",               limit: 65535
    t.text     "skin",               limit: 65535
    t.text     "head",               limit: 65535
    t.text     "ears",               limit: 65535
    t.text     "nose",               limit: 65535
    t.text     "mouth",              limit: 65535
    t.text     "throat",             limit: 65535
    t.text     "neck",               limit: 65535
    t.text     "breasts",            limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "sense_organs", ["medical_history_id"], name: "index_sense_organs_on_medical_history_id", using: :btree

  create_table "service_charges", force: :cascade do |t|
    t.float    "cost",                  limit: 24
    t.text     "observations",          limit: 65535
    t.integer  "user_id",               limit: 4
    t.integer  "hospital_service_id",   limit: 4
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.integer  "hospital_admission_id", limit: 4
    t.boolean  "is_paid"
    t.float    "residue",               limit: 24
    t.decimal  "discount",                            precision: 10, scale: 2
  end

  add_index "service_charges", ["hospital_admission_id"], name: "index_service_charges_on_hospital_admission_id", using: :btree
  add_index "service_charges", ["hospital_service_id"], name: "index_service_charges_on_hospital_service_id", using: :btree
  add_index "service_charges", ["user_id"], name: "index_service_charges_on_user_id", using: :btree

  create_table "service_packages", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "specialties", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "states", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "country_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "states", ["country_id"], name: "index_states_on_country_id", using: :btree

  create_table "study_packages", force: :cascade do |t|
    t.integer  "laboratory_id", limit: 4
    t.string   "name",          limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "study_packages", ["laboratory_id"], name: "index_study_packages_on_laboratory_id", using: :btree

  create_table "study_report_templates", force: :cascade do |t|
    t.integer  "clinical_study_id", limit: 4
    t.text     "template",          limit: 65535
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "study_report_templates", ["clinical_study_id"], name: "index_study_report_templates_on_clinical_study_id", using: :btree

  create_table "subcategory_pathologicals", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "suppliers", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "telephone",  limit: 255
    t.string   "address",    limit: 255
    t.string   "contact",    limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "surgeries", force: :cascade do |t|
    t.text     "description",      limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "hospitalizations", limit: 255
    t.string   "transfusions",     limit: 255
  end

  create_table "tear_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "template_prescriptions", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "time_zones", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "rails_name",   limit: 255
    t.string   "country_name", limit: 255
    t.string   "utc_value",    limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "tooth_pathologies", force: :cascade do |t|
    t.text     "description",        limit: 65535
    t.integer  "tooth_section_id",   limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "medical_history_id", limit: 4
    t.boolean  "is_original"
  end

  add_index "tooth_pathologies", ["medical_history_id"], name: "index_tooth_pathologies_on_medical_history_id", using: :btree
  add_index "tooth_pathologies", ["tooth_section_id"], name: "index_tooth_pathologies_on_tooth_section_id", using: :btree

  create_table "tooth_sections", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "tooth_id",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "tooth_sections", ["tooth_id"], name: "index_tooth_sections_on_tooth_id", using: :btree

  create_table "tooth_treatments", force: :cascade do |t|
    t.text     "description",        limit: 65535
    t.integer  "tooth_section_id",   limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "medical_history_id", limit: 4
    t.boolean  "is_original"
  end

  add_index "tooth_treatments", ["medical_history_id"], name: "index_tooth_treatments_on_medical_history_id", using: :btree
  add_index "tooth_treatments", ["tooth_section_id"], name: "index_tooth_treatments_on_tooth_section_id", using: :btree

  create_table "tooths", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.boolean  "is_superior"
    t.boolean  "is_adult"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "transaction_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.integer  "t_type",              limit: 4
    t.float    "amount",              limit: 24
    t.string   "description",         limit: 255
    t.integer  "transaction_type_id", limit: 4
    t.integer  "payment_method_id",   limit: 4
    t.datetime "date"
    t.float    "init_balance",        limit: 24
    t.float    "end_balance",         limit: 24
    t.integer  "chargeable_id",       limit: 4
    t.string   "chargeable_type",     limit: 255
    t.integer  "user_id",             limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "invoiced"
    t.integer  "folio",               limit: 4
    t.string   "serie",               limit: 255
    t.integer  "invoice_id",          limit: 4
    t.integer  "doctor_id",           limit: 4
    t.integer  "currency_id",         limit: 4
    t.float    "currency_exchange",   limit: 24
  end

  add_index "transactions", ["chargeable_type", "chargeable_id"], name: "index_transactions_on_chargeable_type_and_chargeable_id", using: :btree
  add_index "transactions", ["currency_id"], name: "index_transactions_on_currency_id", using: :btree
  add_index "transactions", ["doctor_id"], name: "index_transactions_on_doctor_id", using: :btree
  add_index "transactions", ["invoice_id"], name: "index_transactions_on_invoice_id", using: :btree
  add_index "transactions", ["payment_method_id"], name: "index_transactions_on_payment_method_id", using: :btree
  add_index "transactions", ["transaction_type_id"], name: "index_transactions_on_transaction_type_id", using: :btree
  add_index "transactions", ["user_id"], name: "index_transactions_on_user_id", using: :btree

  create_table "transfers", force: :cascade do |t|
    t.integer  "from_account_id", limit: 4
    t.integer  "to_account_id",   limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "triages", force: :cascade do |t|
    t.string   "level",          limit: 255
    t.string   "attention_time", limit: 255
    t.string   "description",    limit: 255
    t.string   "color",          limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "type_certificates", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.text     "container_html", limit: 65535
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "doctor_id",      limit: 4
  end

  add_index "type_certificates", ["doctor_id"], name: "index_type_certificates_on_doctor_id", using: :btree

  create_table "type_percentiles", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "type_users", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "ultrasounds", force: :cascade do |t|
    t.integer  "pregnancy_id", limit: 4
    t.date     "date"
    t.integer  "weeks",        limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "ultrasounds", ["pregnancy_id"], name: "index_ultrasounds_on_pregnancy_id", using: :btree

  create_table "urinary_aps", force: :cascade do |t|
    t.string   "description",        limit: 255
    t.integer  "medical_history_id", limit: 4
    t.text     "disuria",            limit: 65535
    t.text     "polaquiuria",        limit: 65535
    t.text     "incontinencia",      limit: 65535
    t.text     "poliuria",           limit: 65535
    t.text     "nicturia",           limit: 65535
    t.text     "hematuria",          limit: 65535
    t.text     "prostatismo",        limit: 65535
    t.text     "tenesmo",            limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "urinary_aps", ["medical_history_id"], name: "index_urinary_aps_on_medical_history_id", using: :btree

  create_table "uro_explorations", force: :cascade do |t|
    t.integer  "medical_consultation_id", limit: 4
    t.boolean  "lymph_nodes"
    t.boolean  "abdomen"
    t.boolean  "bladder"
    t.boolean  "lumbar_region"
    t.boolean  "scrotum_testicles"
    t.boolean  "hernias"
    t.boolean  "epididymis_vessels"
    t.boolean  "penis"
    t.boolean  "injuries"
    t.boolean  "prostate"
    t.boolean  "seminal_vesicles"
    t.text     "notes",                   limit: 65535
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "uro_explorations", ["medical_consultation_id"], name: "index_uro_explorations_on_medical_consultation_id", using: :btree

  create_table "urological_backgrounds", force: :cascade do |t|
    t.integer  "medical_history_id",        limit: 4
    t.boolean  "testicular_pain"
    t.boolean  "impotence"
    t.boolean  "erection_dificulty"
    t.boolean  "premature_ejaculation"
    t.boolean  "vaginal_discharge"
    t.boolean  "anormal_hair_growth"
    t.boolean  "back_pain"
    t.boolean  "kidneys_pain"
    t.boolean  "pelvic_pain"
    t.boolean  "low_abdomen_pain"
    t.boolean  "kidney_bk"
    t.boolean  "kidney_stones"
    t.boolean  "venereal_diseases"
    t.boolean  "self_examination"
    t.boolean  "prostate"
    t.date     "last_prostate_examination"
    t.date     "fum"
    t.integer  "gestations",                limit: 4
    t.date     "last_pap_date"
    t.string   "contraceptive_method",      limit: 255
    t.boolean  "sexually_active"
    t.date     "sex_life_start"
    t.integer  "sexual_partners",           limit: 4
    t.string   "relationship_type",         limit: 255
    t.boolean  "satisfying_sex"
    t.boolean  "intercourse_pain"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "urological_backgrounds", ["medical_history_id"], name: "index_urological_backgrounds_on_medical_history_id", using: :btree

  create_table "user_patients", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "last_name",              limit: 255
    t.date     "birth_date"
    t.string   "phone",                  limit: 255
    t.integer  "user_id",                limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "city_id",                limit: 4
    t.boolean  "accept_term_conditions"
    t.boolean  "is_active"
    t.boolean  "premium_account"
    t.integer  "gender_id",              limit: 4
  end

  add_index "user_patients", ["city_id"], name: "index_user_patients_on_city_id", using: :btree
  add_index "user_patients", ["gender_id"], name: "index_user_patients_on_gender_id", using: :btree
  add_index "user_patients", ["user_id"], name: "index_user_patients_on_user_id", using: :btree

  create_table "user_tokens", force: :cascade do |t|
    t.text     "token",      limit: 65535
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "user_tokens", ["user_id"], name: "index_user_tokens_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "user_name",          limit: 255
    t.string   "password_digest",    limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "type_user_id",       limit: 4
    t.string   "email",              limit: 255
    t.integer  "person_id",          limit: 4
    t.boolean  "can_cancel_payment"
    t.boolean  "can_close_charge"
  end

  add_index "users", ["person_id"], name: "index_users_on_person_id", using: :btree
  add_index "users", ["type_user_id"], name: "index_users_on_type_user_id", using: :btree

  create_table "users_warehouses", id: false, force: :cascade do |t|
    t.integer "warehouse_id", limit: 4
    t.integer "user_id",      limit: 4
  end

  add_index "users_warehouses", ["user_id"], name: "index_users_warehouses_on_user_id", using: :btree
  add_index "users_warehouses", ["warehouse_id"], name: "index_users_warehouses_on_warehouse_id", using: :btree

  create_table "vaccinations", force: :cascade do |t|
    t.date     "vaccinate_date"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "patient_id",         limit: 4
    t.integer  "application_age_id", limit: 4
  end

  add_index "vaccinations", ["application_age_id"], name: "index_vaccinations_on_application_age_id", using: :btree
  add_index "vaccinations", ["patient_id"], name: "index_vaccinations_on_patient_id", using: :btree

  create_table "vaccine_books", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "gender",     limit: 255
    t.integer  "start_age",  limit: 4
    t.integer  "end_age",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "vaccines", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "prevent_disease", limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "vaccine_book_id", limit: 4
  end

  add_index "vaccines", ["vaccine_book_id"], name: "index_vaccines_on_vaccine_book_id", using: :btree

  create_table "valoration_fields", force: :cascade do |t|
    t.string   "f_key",       limit: 255
    t.string   "name",        limit: 255
    t.string   "code",        limit: 255
    t.integer  "f_type",      limit: 4
    t.string   "f_html_type", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "sufix",       limit: 255
  end

  create_table "valoration_types", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.integer  "valoration_type", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "valoration_types_fields", id: false, force: :cascade do |t|
    t.integer "valoration_type_id",  limit: 4
    t.integer "valoration_field_id", limit: 4
  end

  add_index "valoration_types_fields", ["valoration_field_id"], name: "index_valoration_types_fields_on_valoration_field_id", using: :btree
  add_index "valoration_types_fields", ["valoration_type_id"], name: "index_valoration_types_fields_on_valoration_type_id", using: :btree

  create_table "vital_signs", force: :cascade do |t|
    t.float    "weight",                     limit: 24
    t.float    "height",                     limit: 24
    t.float    "temperature",                limit: 24
    t.string   "blood_pressure",             limit: 255
    t.float    "glucose",                    limit: 24
    t.integer  "imc",                        limit: 4
    t.string   "suggested_weight",           limit: 255
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "imc_adult_id",               limit: 4
    t.integer  "imc_children_max19_year_id", limit: 4
    t.float    "head_circumference",         limit: 24
    t.float    "heart_rate",                 limit: 24
    t.float    "breathing_frequency",        limit: 24
    t.string   "eco_rctg",                   limit: 255
    t.string   "fu",                         limit: 255
    t.string   "pres",                       limit: 255
    t.string   "ff",                         limit: 255
    t.string   "vdrl",                       limit: 255
    t.string   "ego",                        limit: 255
    t.text     "other",                      limit: 65535
    t.integer  "systolic_pressure",          limit: 4
    t.integer  "diastolic_pressure",         limit: 4
    t.float    "oxygen_saturation",          limit: 24
  end

  add_index "vital_signs", ["imc_adult_id"], name: "index_vital_signs_on_imc_adult_id", using: :btree
  add_index "vital_signs", ["imc_children_max19_year_id"], name: "index_vital_signs_on_imc_children_max19_year_id", using: :btree

  create_table "warehouses", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "waste_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_foreign_key "abortions", "abortion_types"
  add_foreign_key "abortions", "pregnancies"
  add_foreign_key "abortions", "users"
  add_foreign_key "accounts", "banks"
  add_foreign_key "accounts", "currencies"
  add_foreign_key "accounts_offices", "accounts"
  add_foreign_key "accounts_offices", "offices"
  add_foreign_key "accounts_transactions", "accounts"
  add_foreign_key "accounts_transactions", "transactions"
  add_foreign_key "accounts_users", "accounts"
  add_foreign_key "accounts_users", "users"
  add_foreign_key "active_substances_medicaments", "active_substances"
  add_foreign_key "active_substances_medicaments", "medicaments"
  add_foreign_key "activities", "activity_statuses"
  add_foreign_key "activities", "activity_types"
  add_foreign_key "activities", "doctors"
  add_foreign_key "activities_offices", "activities"
  add_foreign_key "activities_offices", "offices"
  add_foreign_key "activities_patients", "activities"
  add_foreign_key "activities_patients", "patients"
  add_foreign_key "activities_personalsupports", "activities"
  add_foreign_key "activities_personalsupports", "personalsupports"
  add_foreign_key "activities_user_patients", "activities"
  add_foreign_key "activities_user_patients", "user_patients"
  add_foreign_key "activity_statuses", "activity_types"
  add_foreign_key "address_books", "doctors"
  add_foreign_key "address_books", "hospitals"
  add_foreign_key "address_books", "offices"
  add_foreign_key "admission_invoice_concepts", "admission_invoices"
  add_foreign_key "admission_invoice_concepts", "measure_units"
  add_foreign_key "admission_invoice_emisors", "admission_invoices"
  add_foreign_key "admission_invoice_receptors", "admission_invoices"
  add_foreign_key "admission_invoices", "emisor_fiscal_informations"
  add_foreign_key "admission_invoices", "hospital_admissions"
  add_foreign_key "admission_invoices", "payment_ways"
  add_foreign_key "admission_invoices", "users"
  add_foreign_key "admission_transactions", "clinic_invoices"
  add_foreign_key "admission_transactions", "hospital_admissions"
  add_foreign_key "admission_transactions", "users"
  add_foreign_key "allergies", "doctors"
  add_foreign_key "annotation_fields", "medical_annotation_fields"
  add_foreign_key "annotation_fields", "medical_annotations"
  add_foreign_key "archives", "clinical_analysis_orders"
  add_foreign_key "asset_activities", "fixed_asset_activities"
  add_foreign_key "asset_activities", "fixed_asset_items"
  add_foreign_key "asset_activities", "users"
  add_foreign_key "assistants", "users"
  add_foreign_key "assistants_expedient_permissions", "assistants"
  add_foreign_key "assistants_expedient_permissions", "expedient_permissions"
  add_foreign_key "assortment_pharmacy_items", "medicament_assortments"
  add_foreign_key "assortment_pharmacy_items", "pharmacy_items"
  add_foreign_key "assortment_pharmacy_items", "warehouses"
  add_foreign_key "birth_infos", "birth_types"
  add_foreign_key "birth_infos", "delivery_types"
  add_foreign_key "birth_infos", "duration_types"
  add_foreign_key "birth_infos", "episiotomy_types"
  add_foreign_key "birth_infos", "pregnancies"
  add_foreign_key "birth_infos", "tear_types"
  add_foreign_key "birth_infos", "users"
  add_foreign_key "ca_groups", "laboratories"
  add_foreign_key "cabinet_report_pictures", "cabinet_reports"
  add_foreign_key "cabinet_reports", "clinical_analysis_orders"
  add_foreign_key "cabinet_reports", "invoices"
  add_foreign_key "cabinet_reports", "users"
  add_foreign_key "cardiovascular_aps", "medical_histories"
  add_foreign_key "certificates", "offices"
  add_foreign_key "certificates", "patients"
  add_foreign_key "certificates", "type_certificates"
  add_foreign_key "charge_report_studies", "cabinet_reports"
  add_foreign_key "charges", "medical_consultations"
  add_foreign_key "child_non_pathological_infos", "medical_histories"
  add_foreign_key "cities", "states"
  add_foreign_key "clinic_infos", "cities"
  add_foreign_key "clinic_infos", "users"
  add_foreign_key "clinic_invoice_concepts", "clinic_invoices"
  add_foreign_key "clinic_invoice_concepts", "measure_units"
  add_foreign_key "clinic_invoice_emisors", "clinic_invoices"
  add_foreign_key "clinic_invoice_receptors", "clinic_invoices"
  add_foreign_key "clinic_invoices", "patient_fiscal_informations"
  add_foreign_key "clinic_invoices", "payment_ways"
  add_foreign_key "clinic_invoices", "users"
  add_foreign_key "clinical_analysis_orders", "clinical_order_types"
  add_foreign_key "clinical_analysis_orders", "laboratories"
  add_foreign_key "clinical_analysis_orders", "medical_expedients"
  add_foreign_key "clinical_analysis_orders", "offices"
  add_foreign_key "clinical_analysis_orders", "users"
  add_foreign_key "clinical_analysis_orders_studies", "clinical_analysis_orders"
  add_foreign_key "clinical_analysis_orders_studies", "clinical_studies"
  add_foreign_key "clinical_studies", "clinical_study_types"
  add_foreign_key "clinical_studies", "laboratories"
  add_foreign_key "clinical_studies_study_packages", "clinical_studies"
  add_foreign_key "clinical_studies_study_packages", "study_packages"
  add_foreign_key "clinical_study_types", "clinical_order_types"
  add_foreign_key "clinical_study_types", "laboratories"
  add_foreign_key "consultation_costs", "consultation_types"
  add_foreign_key "consultation_costs", "offices"
  add_foreign_key "consultation_types_users", "consultation_types"
  add_foreign_key "consultation_types_users", "users"
  add_foreign_key "consultations_valorations", "medical_consultations"
  add_foreign_key "consultations_valorations", "patient_valorations"
  add_foreign_key "currency_exchanges", "clinic_infos"
  add_foreign_key "currency_exchanges", "users"
  add_foreign_key "custom_template_prescription_fields", "custom_template_prescriptions"
  add_foreign_key "custom_template_prescription_fields", "fields_prescriptions"
  add_foreign_key "custom_template_prescriptions", "doctors"
  add_foreign_key "custom_template_report_fields", "custom_template_reports"
  add_foreign_key "custom_template_report_fields", "fields_prescriptions"
  add_foreign_key "custom_template_reports", "doctors"
  add_foreign_key "daily_cashes", "accounts"
  add_foreign_key "daily_cashes", "users"
  add_foreign_key "daily_cashes_transactions", "daily_cashes"
  add_foreign_key "daily_cashes_transactions", "transactions"
  add_foreign_key "dc_balances", "currencies"
  add_foreign_key "dc_balances", "daily_cashes"
  add_foreign_key "default_no_pathologicals", "marital_statuses"
  add_foreign_key "digestive_aps", "medical_histories"
  add_foreign_key "diseases", "disease_categories"
  add_foreign_key "diseases", "doctors"
  add_foreign_key "diseases_family_diseases", "diseases"
  add_foreign_key "diseases_family_diseases", "family_diseases"
  add_foreign_key "diseases_medical_consultations", "diseases"
  add_foreign_key "diseases_medical_consultations", "medical_consultations"
  add_foreign_key "doctors", "fiscal_informations"
  add_foreign_key "doctors", "laboratories"
  add_foreign_key "doctors", "time_zones"
  add_foreign_key "doctors", "users"
  add_foreign_key "doctors_laboratories", "doctors"
  add_foreign_key "doctors_laboratories", "laboratories"
  add_foreign_key "doctors_user_patients", "doctors"
  add_foreign_key "doctors_user_patients", "user_patients"
  add_foreign_key "doctors_users", "doctors"
  add_foreign_key "doctors_users", "users"
  add_foreign_key "email_settings", "doctors"
  add_foreign_key "emisor_fiscal_informations", "cities"
  add_foreign_key "expedient_requests", "health_histories"
  add_foreign_key "expedient_requests", "medical_expedients"
  add_foreign_key "explorations", "exploration_types"
  add_foreign_key "explorations", "medical_consultations"
  add_foreign_key "fields_prescriptions", "field_prescription_types"
  add_foreign_key "fiscal_informations", "cities"
  add_foreign_key "fixed_asset_activities", "fixed_assets"
  add_foreign_key "fixed_asset_items", "fixed_assets"
  add_foreign_key "fixed_assets", "fixed_asset_categories"
  add_foreign_key "general_accounts", "banks"
  add_foreign_key "general_accounts", "currencies"
  add_foreign_key "general_fiscal_infos", "users"
  add_foreign_key "genital_aps", "medical_histories"
  add_foreign_key "gynecologist_obstetrical_backgrounds", "medical_histories"
  add_foreign_key "health_histories", "user_patients"
  add_foreign_key "hematological_sis", "medical_histories"
  add_foreign_key "hospital_admission_discounts", "hospital_admissions"
  add_foreign_key "hospital_admission_discounts", "users"
  add_foreign_key "hospital_admissions", "attention_types"
  add_foreign_key "hospital_admissions", "doctors"
  add_foreign_key "hospital_admissions", "patients"
  add_foreign_key "hospital_admissions", "triages"
  add_foreign_key "hospital_admissions", "users"
  add_foreign_key "hospital_services", "hospital_service_categories"
  add_foreign_key "hospital_services", "users"
  add_foreign_key "hospitalizations_annotations", "hospital_admissions"
  add_foreign_key "hospitalizations_annotations", "medical_annotations"
  add_foreign_key "hospitalizations_valorations", "hospital_admissions"
  add_foreign_key "hospitalizations_valorations", "patient_valorations"
  add_foreign_key "hospitals", "cities"
  add_foreign_key "identity_cards", "doctors"
  add_foreign_key "identity_cards", "specialties"
  add_foreign_key "imc_adults", "category_imcs"
  add_foreign_key "imc_adults", "rank_imc_adults"
  add_foreign_key "imc_children_max19_years", "genders"
  add_foreign_key "indications", "ca_groups"
  add_foreign_key "infection_ots", "medical_histories"
  add_foreign_key "inherited_family_backgrounds", "medical_histories"
  add_foreign_key "insurence_prices", "hospital_services"
  add_foreign_key "insurence_prices", "insurances"
  add_foreign_key "inventories", "medicament_categories"
  add_foreign_key "inventories", "warehouses"
  add_foreign_key "inventory_pharmacy_stocks", "inventories"
  add_foreign_key "inventory_pharmacy_stocks", "pharmacy_items"
  add_foreign_key "inventory_stocks", "inventories"
  add_foreign_key "inventory_stocks", "lots"
  add_foreign_key "invoice_concepts", "invoices"
  add_foreign_key "laboratories", "cities"
  add_foreign_key "laboratories", "clinical_order_types"
  add_foreign_key "license_features", "features"
  add_foreign_key "license_features", "licenses"
  add_foreign_key "licenses", "distribution_types"
  add_foreign_key "licenses", "doctors"
  add_foreign_key "locomotor_sis", "medical_histories"
  add_foreign_key "lots", "medicaments"
  add_foreign_key "lots", "users"
  add_foreign_key "medical_annotation_fields", "medical_annotation_types"
  add_foreign_key "medical_annotation_files", "medical_annotations"
  add_foreign_key "medical_annotation_files", "users"
  add_foreign_key "medical_annotation_images", "medical_annotation_types"
  add_foreign_key "medical_annotations", "medical_annotation_types"
  add_foreign_key "medical_annotations", "patients"
  add_foreign_key "medical_annotations", "users"
  add_foreign_key "medical_annotations_valorations", "medical_annotations"
  add_foreign_key "medical_annotations_valorations", "patient_valorations"
  add_foreign_key "medical_consultations", "consultation_types"
  add_foreign_key "medical_consultations", "invoices"
  add_foreign_key "medical_consultations", "medical_expedients"
  add_foreign_key "medical_consultations", "offices"
  add_foreign_key "medical_consultations", "users"
  add_foreign_key "medical_consultations", "vital_signs"
  add_foreign_key "medical_consultations_pregnancies", "medical_consultations"
  add_foreign_key "medical_consultations_pregnancies", "pregnancies"
  add_foreign_key "medical_expedients", "patients"
  add_foreign_key "medical_expedients", "users"
  add_foreign_key "medical_histories", "family_diseases"
  add_foreign_key "medical_histories", "medical_expedients"
  add_foreign_key "medical_histories_medicaments", "medical_histories"
  add_foreign_key "medical_histories_medicaments", "medicaments"
  add_foreign_key "medical_indications", "medicaments"
  add_foreign_key "medical_indications", "prescriptions"
  add_foreign_key "medicament_assortment_items", "lots"
  add_foreign_key "medicament_assortment_items", "medicament_assortments"
  add_foreign_key "medicament_assortment_items", "medicaments"
  add_foreign_key "medicament_assortments", "hospital_admissions"
  add_foreign_key "medicament_assortments", "users"
  add_foreign_key "medicament_assortments", "warehouses"
  add_foreign_key "medicament_inventories", "lots"
  add_foreign_key "medicament_inventories", "users"
  add_foreign_key "medicament_inventories", "warehouses"
  add_foreign_key "medicament_logs", "lots"
  add_foreign_key "medicament_logs", "users"
  add_foreign_key "medicament_logs", "warehouses"
  add_foreign_key "medicament_movements", "lots"
  add_foreign_key "medicament_movements", "users"
  add_foreign_key "medicament_purchase_farmacy_items", "medicament_purchases"
  add_foreign_key "medicament_purchase_farmacy_items", "pharmacy_items"
  add_foreign_key "medicament_purchase_items", "lots"
  add_foreign_key "medicament_purchase_items", "medicament_purchases"
  add_foreign_key "medicament_purchase_items", "medicaments"
  add_foreign_key "medicament_purchases", "suppliers"
  add_foreign_key "medicament_purchases", "users"
  add_foreign_key "medicament_purchases", "warehouses"
  add_foreign_key "medicament_wastes", "lots"
  add_foreign_key "medicament_wastes", "users"
  add_foreign_key "medicament_wastes", "warehouses"
  add_foreign_key "medicament_wastes", "waste_types"
  add_foreign_key "medicaments", "medicament_categories"
  add_foreign_key "medicaments", "medicament_laboratories"
  add_foreign_key "medicaments_pathological_medicines", "medicaments"
  add_foreign_key "medicaments_pathological_medicines", "pathological_medicines"
  add_foreign_key "metabolic_sis", "medical_histories"
  add_foreign_key "movement_types", "movement_categories"
  add_foreign_key "neonate_infos", "birth_infos"
  add_foreign_key "neonate_infos", "blood_types"
  add_foreign_key "neonate_infos", "genders"
  add_foreign_key "nervous_sis", "medical_histories"
  add_foreign_key "news_items", "authors"
  add_foreign_key "no_pathologicals", "marital_statuses"
  add_foreign_key "no_pathologicals", "medical_histories"
  add_foreign_key "office_permissions", "assistants"
  add_foreign_key "office_permissions", "offices"
  add_foreign_key "office_permissions", "permissions"
  add_foreign_key "office_times", "offices"
  add_foreign_key "offices", "cities"
  add_foreign_key "offices", "hospitals"
  add_foreign_key "offices", "template_prescriptions"
  add_foreign_key "offices_users", "offices"
  add_foreign_key "offices_users", "users"
  add_foreign_key "ophthalmological_sis", "medical_histories"
  add_foreign_key "other_charge_items", "hospital_services"
  add_foreign_key "other_charge_items", "other_charges"
  add_foreign_key "other_charge_transactions", "other_charges"
  add_foreign_key "other_charge_transactions", "users"
  add_foreign_key "other_charges", "users"
  add_foreign_key "other_movements", "movement_types"
  add_foreign_key "otolaryngology_sis", "medical_histories"
  add_foreign_key "package_service_costs", "hospital_services"
  add_foreign_key "package_service_costs", "service_packages"
  add_foreign_key "parent_infos", "blood_types"
  add_foreign_key "parent_infos", "child_non_pathological_infos"
  add_foreign_key "pathologicals", "medical_histories"
  add_foreign_key "pathologicals", "subcategory_pathologicals"
  add_foreign_key "patient_valoration_fields", "patient_valorations"
  add_foreign_key "patient_valoration_fields", "valoration_fields"
  add_foreign_key "patient_valorations", "patients"
  add_foreign_key "patient_valorations", "users"
  add_foreign_key "patient_valorations", "valoration_types"
  add_foreign_key "patients", "blood_types"
  add_foreign_key "patients", "cities"
  add_foreign_key "patients", "genders"
  add_foreign_key "patients", "insurances"
  add_foreign_key "patients", "users"
  add_foreign_key "people", "cities"
  add_foreign_key "people", "genders"
  add_foreign_key "percentile_cdcs", "genders"
  add_foreign_key "percentile_cdcs", "type_percentiles"
  add_foreign_key "percentile_children", "genders"
  add_foreign_key "percentile_children", "type_percentiles"
  add_foreign_key "perinatal_informations", "cities"
  add_foreign_key "perinatal_informations", "medical_histories"
  add_foreign_key "personalsupports", "doctors"
  add_foreign_key "personalsupports", "operation_roles"
  add_foreign_key "pharmacy_item_inventories", "pharmacy_items"
  add_foreign_key "pharmacy_item_inventories", "users"
  add_foreign_key "pharmacy_item_inventories", "warehouses"
  add_foreign_key "pharmacy_item_logs", "pharmacy_items"
  add_foreign_key "pharmacy_item_logs", "users"
  add_foreign_key "pharmacy_item_logs", "warehouses"
  add_foreign_key "pharmacy_item_movements", "pharmacy_items"
  add_foreign_key "pharmacy_item_movements", "users"
  add_foreign_key "pharmacy_item_wastes", "pharmacy_items"
  add_foreign_key "pharmacy_item_wastes", "users"
  add_foreign_key "pharmacy_item_wastes", "warehouses"
  add_foreign_key "pharmacy_item_wastes", "waste_types"
  add_foreign_key "pharmacy_items", "measure_unit_items"
  add_foreign_key "pharmacy_items", "medicament_categories"
  add_foreign_key "pregnancies", "medical_expedients"
  add_foreign_key "pregnancies", "users"
  add_foreign_key "prescriptions", "medical_consultations"
  add_foreign_key "psychosomatic_sis", "medical_histories"
  add_foreign_key "respiratory_aps", "medical_histories"
  add_foreign_key "sense_organs", "medical_histories"
  add_foreign_key "service_charges", "hospital_admissions"
  add_foreign_key "service_charges", "hospital_services"
  add_foreign_key "service_charges", "users"
  add_foreign_key "states", "countries"
  add_foreign_key "study_packages", "laboratories"
  add_foreign_key "study_report_templates", "clinical_studies"
  add_foreign_key "tooth_pathologies", "medical_histories"
  add_foreign_key "tooth_pathologies", "tooth_sections"
  add_foreign_key "tooth_sections", "tooths"
  add_foreign_key "tooth_treatments", "medical_histories"
  add_foreign_key "tooth_treatments", "tooth_sections"
  add_foreign_key "transactions", "currencies"
  add_foreign_key "transactions", "doctors"
  add_foreign_key "transactions", "payment_methods"
  add_foreign_key "transactions", "transaction_types"
  add_foreign_key "transactions", "users"
  add_foreign_key "type_certificates", "doctors"
  add_foreign_key "ultrasounds", "pregnancies"
  add_foreign_key "urinary_aps", "medical_histories"
  add_foreign_key "uro_explorations", "medical_consultations"
  add_foreign_key "urological_backgrounds", "medical_histories"
  add_foreign_key "user_patients", "cities"
  add_foreign_key "user_patients", "genders"
  add_foreign_key "user_patients", "users"
  add_foreign_key "user_tokens", "users"
  add_foreign_key "users", "people"
  add_foreign_key "users", "type_users"
  add_foreign_key "users_warehouses", "users"
  add_foreign_key "users_warehouses", "warehouses"
  add_foreign_key "vaccinations", "application_ages"
  add_foreign_key "vaccinations", "patients"
  add_foreign_key "valoration_types_fields", "valoration_fields"
  add_foreign_key "valoration_types_fields", "valoration_types"
  add_foreign_key "vital_signs", "imc_adults"
  add_foreign_key "vital_signs", "imc_children_max19_years"
end
