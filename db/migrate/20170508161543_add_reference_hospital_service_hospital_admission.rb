class AddReferenceHospitalServiceHospitalAdmission < ActiveRecord::Migration
  def change
    add_reference :service_charges, :hospital_admission, index: true, foreign_key: true
  end
end
