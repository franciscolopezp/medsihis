class CreateFixedAssetActivities < ActiveRecord::Migration
  def change
    create_table :fixed_asset_activities do |t|
      t.string :name
      t.references :fixed_asset, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
