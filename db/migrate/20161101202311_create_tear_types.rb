class CreateTearTypes < ActiveRecord::Migration
  def change
    create_table :tear_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
