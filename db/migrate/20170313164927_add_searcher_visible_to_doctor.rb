class AddSearcherVisibleToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :searcher_visible, :boolean
  end
end
