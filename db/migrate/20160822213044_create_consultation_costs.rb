class CreateConsultationCosts < ActiveRecord::Migration
  def change
    create_table :consultation_costs do |t|
      t.float :cost
      t.references :consultation_type, index: true, foreign_key: true
      t.references :office, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
