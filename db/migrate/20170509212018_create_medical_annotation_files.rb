class CreateMedicalAnnotationFiles < ActiveRecord::Migration
  def change
    create_table :medical_annotation_files do |t|
      t.references :medical_annotation, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :document
      t.string :notes

      t.timestamps null: false
    end
  end
end
