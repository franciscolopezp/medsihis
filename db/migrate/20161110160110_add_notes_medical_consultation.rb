class AddNotesMedicalConsultation < ActiveRecord::Migration
  def change
    add_column :medical_consultations, :notes, :text
  end
end
