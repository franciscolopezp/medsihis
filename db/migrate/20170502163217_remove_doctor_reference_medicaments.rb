class RemoveDoctorReferenceMedicaments < ActiveRecord::Migration
  def change
    remove_reference(:medicaments, :doctor, index: true, foreign_key: true)
    add_reference :medicaments, :medicament_laboratory, index: true, foreign_key: true
  end
end
