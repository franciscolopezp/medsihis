class Removeaddreference < ActiveRecord::Migration
  def change
    add_reference :clinic_infos, :emisor_fiscal_information, index: true, foreign_key: true
    remove_reference(:emisor_fiscal_informations, :clinic_info, index: true, foreign_key: true)
  end
end
