class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :country
      t.string :name
      t.string :symbol
      t.string :iso_code
      t.string :fractional_unit
      t.integer :base_number

      t.timestamps null: false
    end
  end
end
