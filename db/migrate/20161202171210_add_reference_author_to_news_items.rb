class AddReferenceAuthorToNewsItems < ActiveRecord::Migration
  def change
    add_reference :news_items, :author, index: true, foreign_key: true
  end
end
