class AddHospitalServiceCategory < ActiveRecord::Migration
  def change
    add_reference :hospital_services, :hospital_service_category, index: true, foreign_key: true
  end
end
