class ChangeColumnBloodPressure < ActiveRecord::Migration
  def change
    change_column :vital_signs, :blood_pressure, :string
  end
end
