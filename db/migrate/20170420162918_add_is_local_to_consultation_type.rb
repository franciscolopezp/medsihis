class AddIsLocalToConsultationType < ActiveRecord::Migration
  def change
    add_column :consultation_types, :is_local, :boolean
  end
end
