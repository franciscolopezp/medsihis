class AddVitalSignToMedicalConsultations < ActiveRecord::Migration
  def change
    add_reference :medical_consultations, :vital_sign, index: true, foreign_key: true
  end
end
