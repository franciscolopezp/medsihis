class CreateApplicationAges < ActiveRecord::Migration
  def change
    create_table :application_ages do |t|
      t.string :age
      t.integer :age_type
      t.string :dose
      t.string :comments

      t.timestamps null: false
    end
  end
end
