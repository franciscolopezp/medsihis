class AddActiveToPatient < ActiveRecord::Migration
  def change
    add_column :patients, :active, :integer
    add_column :medical_expedients, :active, :integer
  end
end
