class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :t_type
      t.decimal :amount
      t.string :description
      t.references :transaction_type, index: true, foreign_key: true
      t.references :payment_method, index: true, foreign_key: true
      t.datetime :date
      t.decimal :init_balance
      t.decimal :end_balance
      t.references :chargeable, polymorphic: true, index: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
