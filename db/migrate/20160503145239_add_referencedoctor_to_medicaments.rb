class AddReferencedoctorToMedicaments < ActiveRecord::Migration
  def change
    add_reference :medicaments, :doctor, index: true, foreign_key: true
  end
end
