class Addreferenceconceptmeasue < ActiveRecord::Migration
  def change
    add_reference :invoice_concepts, :measure_unit, index: true
  end
end
