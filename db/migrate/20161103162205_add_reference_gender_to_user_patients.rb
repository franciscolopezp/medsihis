class AddReferenceGenderToUserPatients < ActiveRecord::Migration
  def change
    add_reference :user_patients, :gender, index: true, foreign_key: true
  end
end
