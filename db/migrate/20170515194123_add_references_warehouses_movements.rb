class AddReferencesWarehousesMovements < ActiveRecord::Migration
  def change
    remove_column :medicament_movements, :from_warehouse
    remove_column :medicament_movements, :to_warehouse
    add_column :medicament_movements, :from_warehouse, :integer
    add_column :medicament_movements, :to_warehouse, :integer
  end
end
