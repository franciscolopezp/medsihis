class CreateLaboratories < ActiveRecord::Migration
  def change
    create_table :laboratories do |t|
      t.string :name
      t.string :address
      t.string :telephone
      t.string :email
      t.string :logo
      t.string :observations

      t.timestamps null: false
    end
  end
end
