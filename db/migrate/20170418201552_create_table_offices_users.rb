class CreateTableOfficesUsers < ActiveRecord::Migration
  def change
    create_table :offices_users, id:false do |t|
      t.belongs_to :office, index: true
      t.belongs_to :user, index: true
    end
    add_foreign_key :offices_users, :offices
    add_foreign_key :offices_users, :users
  end
end
