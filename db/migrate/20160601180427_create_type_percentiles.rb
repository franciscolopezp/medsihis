class CreateTypePercentiles < ActiveRecord::Migration
  def change
    create_table :type_percentiles do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
