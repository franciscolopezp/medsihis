class RemoveAgendaPermissionFromOfficePermission < ActiveRecord::Migration
  def change
    remove_reference :office_permissions, :agenda_permission, index: true, foreign_key: true
    add_reference :office_permissions, :permission, index: true, foreign_key: true
  end
end
