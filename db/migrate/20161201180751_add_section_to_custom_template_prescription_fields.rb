class AddSectionToCustomTemplatePrescriptionFields < ActiveRecord::Migration
  def change
    add_column :custom_template_prescription_fields, :section, :string
  end
end
