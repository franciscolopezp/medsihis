class AddReferenceClinicalOrderTypeToClinicalAnalysisOrders < ActiveRecord::Migration
  def change
    add_reference :clinical_analysis_orders, :clinical_order_type, index: true, foreign_key: true
  end
end
