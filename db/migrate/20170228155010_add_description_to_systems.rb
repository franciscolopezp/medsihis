class AddDescriptionToSystems < ActiveRecord::Migration
  def change
    add_column :genital_aps, :description, :string, after: :id
    add_column :urinary_aps, :description, :string, after: :id
    add_column :digestive_aps, :description, :string, after: :id
    add_column :respiratory_aps, :description, :string, after: :id
    add_column :cardiovascular_aps, :description, :string, after: :id
    add_column :hematological_sis, :description, :string, after: :id
    add_column :metabolic_sis, :description, :string, after: :id
    add_column :nervous_sis, :description, :string, after: :id
    add_column :psychosomatic_sis, :description, :string, after: :id
    add_column :infection_ots, :description, :string, after: :id
  end
end
