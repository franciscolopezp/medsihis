class AddReferenceLaboratoryToCaGroup < ActiveRecord::Migration
  def change
    add_reference :ca_groups, :laboratory, index: true, foreign_key: true
    add_reference :clinical_study_types, :laboratory, index: true, foreign_key: true
  end
end
