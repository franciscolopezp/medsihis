class AddReferencePersonUser < ActiveRecord::Migration
  def change
    remove_reference(:doctors, :person, index: true, foreign_key: true)
    remove_reference(:assistants, :person, index: true, foreign_key: true)
    add_reference :users, :person, index: true, foreign_key: true
  end
end
