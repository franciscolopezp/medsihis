class CreateLicenseFeatures < ActiveRecord::Migration
  def change
    create_table :license_features do |t|
      t.decimal :cost

      t.timestamps null: false
    end
  end
end
