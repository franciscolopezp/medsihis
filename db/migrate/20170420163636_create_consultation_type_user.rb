class CreateConsultationTypeUser < ActiveRecord::Migration
  def change
    create_table :consultation_types_users, id:false do |t|
      t.belongs_to :consultation_type, index: true
      t.belongs_to :user, index: true
    end
    add_foreign_key :consultation_types_users, :consultation_types
    add_foreign_key :consultation_types_users, :users
    end
end
