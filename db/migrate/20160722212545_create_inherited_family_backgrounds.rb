class CreateInheritedFamilyBackgrounds < ActiveRecord::Migration
  def change
    create_table :inherited_family_backgrounds do |t|
      t.boolean :gynecological_diseases
      t.string :gynecological_diseases_description
      t. boolean :mammary_dysplasia
      t.string :mammary_dysplasia_description
      t.boolean :twinhood
      t.string :twinhood_description
      t.boolean :congenital_anomalies
      t.string :congenital_anomalies_description
      t.boolean :infertility
      t.string :infertility_description
      t.boolean :others
      t.string :others_description
      t.string :anotations
      t.references :medical_history, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
