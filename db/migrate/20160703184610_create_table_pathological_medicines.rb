class CreateTablePathologicalMedicines < ActiveRecord::Migration
  def change
    create_table :medicaments_pathological_medicines, id:false do |t|
    t.belongs_to :pathological_medicine, index: {:name => 'pathological_medicine_id'}
    t.belongs_to :medicament, index: {:name => 'medicament_id'}
    end
  add_foreign_key :medicaments_pathological_medicines, :pathological_medicines
  add_foreign_key :medicaments_pathological_medicines, :medicaments
  end
end
