class CreateUserPatients < ActiveRecord::Migration
  def change
    create_table :user_patients do |t|
      t.string :name
      t.string :last_name
      t.date :birth_date
      t.string :email
      t.string :phone
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
