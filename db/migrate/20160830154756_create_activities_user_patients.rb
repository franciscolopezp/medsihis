class CreateActivitiesUserPatients < ActiveRecord::Migration
  def change
    create_table :activities_user_patients,id:false do |t|
      t.references :activity, index: true, foreign_key: true
      t.references :user_patient, index: true, foreign_key: true
    end
  end
end
