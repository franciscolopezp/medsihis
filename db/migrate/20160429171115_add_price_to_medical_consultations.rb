class AddPriceToMedicalConsultations < ActiveRecord::Migration
  def change
    add_column :medical_consultations, :price, :float
  end
end
