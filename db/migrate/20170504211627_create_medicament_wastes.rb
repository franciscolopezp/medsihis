class CreateMedicamentWastes < ActiveRecord::Migration
  def change
    create_table :medicament_wastes do |t|
      t.references :lot, index: true, foreign_key: true
      t.references :warehouse, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.text :cause
      t.timestamps null: false
    end
  end
end
