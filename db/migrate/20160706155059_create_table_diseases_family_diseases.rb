class CreateTableDiseasesFamilyDiseases < ActiveRecord::Migration
  def change
    create_table :diseases_family_diseases, id:false do |t|
      t.belongs_to :disease, index: {:name => 'disease_id'}
      t.belongs_to :family_disease, index: {:name => 'family_disease_id'}
    end
    add_foreign_key :diseases_family_diseases, :diseases
    add_foreign_key :diseases_family_diseases, :family_diseases
  end
end
