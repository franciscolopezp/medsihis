class CreateAttentionTypes < ActiveRecord::Migration
  def change
    create_table :attention_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
