class CreatePatientValorationFields < ActiveRecord::Migration
  def change
    create_table :patient_valoration_fields do |t|
      t.references :patient_valoration, index: true, foreign_key: true
      t.references :valoration_field, index: true, foreign_key: true
      t.string :data

      t.timestamps null: false
    end
  end
end
