class AddCancelToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :cancel, :boolean
  end
end
