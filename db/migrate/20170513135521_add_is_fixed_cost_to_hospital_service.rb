class AddIsFixedCostToHospitalService < ActiveRecord::Migration
  def change
    add_column :hospital_services, :is_fixed_cost, :boolean
  end
end
