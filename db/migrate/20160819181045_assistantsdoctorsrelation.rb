class Assistantsdoctorsrelation < ActiveRecord::Migration
  def change
      create_table :assistants_doctors, :id => false do |t|
        t.integer :assistant_id
        t.integer :doctor_id
      end
  end
end
