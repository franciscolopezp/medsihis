class AddHeightBoxAndWidthBoxToCustomTemplatePrescriptionFields < ActiveRecord::Migration
  def change
    add_column :custom_template_prescription_fields, :height_box, :float
    add_column :custom_template_prescription_fields, :width_box, :float
  end
end
