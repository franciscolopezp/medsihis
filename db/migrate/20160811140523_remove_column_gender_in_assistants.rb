class RemoveColumnGenderInAssistants < ActiveRecord::Migration
  def change
    remove_column :assistants, :gender, :string
  end
end
