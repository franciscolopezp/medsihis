class CreateEpisiotomyTypes < ActiveRecord::Migration
  def change
    create_table :episiotomy_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
