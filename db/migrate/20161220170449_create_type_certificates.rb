class CreateTypeCertificates < ActiveRecord::Migration
  def change
    create_table :type_certificates do |t|
      t.string :name
      t.text :container_html

      t.timestamps null: false
    end
  end
end
