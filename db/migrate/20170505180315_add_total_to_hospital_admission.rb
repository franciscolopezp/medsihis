class AddTotalToHospitalAdmission < ActiveRecord::Migration
  def change
    add_column :hospital_admissions, :total, :float
    add_column :hospital_admissions, :payment, :float
    add_column :hospital_admissions, :debt, :float
  end
end
