class CreateImcAdults < ActiveRecord::Migration
  def change
    create_table :imc_adults do |t|
      t.float :imc

      t.timestamps null: false
    end
  end
end
