class AddReferenceFeatureToLicenseFeature < ActiveRecord::Migration
  def change
    add_reference :license_features, :feature, index: true, foreign_key: true
  end
end
