class AddLabelToCustomTemplatePrescriptionField < ActiveRecord::Migration
  def change
    add_column :custom_template_prescription_fields, :label, :string
  end
end
