class AddReferenceInsureToPatient < ActiveRecord::Migration
  def change
    add_reference :patients, :insurance, index: true, foreign_key: true
  end
end
