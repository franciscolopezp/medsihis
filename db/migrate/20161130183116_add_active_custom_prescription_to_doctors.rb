class AddActiveCustomPrescriptionToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :is_active_custom_prescription, :boolean
  end
end
