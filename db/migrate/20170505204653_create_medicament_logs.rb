class CreateMedicamentLogs < ActiveRecord::Migration
  def change
    create_table :medicament_logs do |t|
      t.integer :quantity
      t.boolean :income
      t.references :user, index: true, foreign_key: true
      t.references :warehouse, index: true, foreign_key: true
      t.references :lot, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
