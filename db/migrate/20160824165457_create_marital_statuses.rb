class CreateMaritalStatuses < ActiveRecord::Migration
  def change
    create_table :marital_statuses do |t|
      t.string :name
      t.string :male
      t.string :female

      t.timestamps null: false
    end
  end
end
