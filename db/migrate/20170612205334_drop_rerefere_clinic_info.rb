class DropRerefereClinicInfo < ActiveRecord::Migration
  def change
    remove_reference(:clinic_infos, :emisor_fiscal_information, index: true, foreign_key: true)
  end
end
