class AddColumnsToMedicalHistory < ActiveRecord::Migration
  def change
    add_column :medical_histories, :other_allergies, :string, :after => :id
    add_column :medical_histories, :allergy_medicament_description, :string, :after => :id
    add_column :medical_histories, :allergy_medicament, :boolean, :after => :id

    add_column :medical_histories, :surgeries, :string, :after => :id
    add_column :medical_histories, :transfusions, :string, :after => :id

    add_column :medical_histories, :other_medicines, :string, :after => :id

    add_column :medical_histories, :recent_diseases, :string, :after => :id
  end
end
