class AddColumnsToGynecologistObstetricalBackground < ActiveRecord::Migration
  def change
    add_column :gynecologist_obstetrical_backgrounds, :ivs, :string
    add_column :gynecologist_obstetrical_backgrounds, :fup, :date
  end
end
