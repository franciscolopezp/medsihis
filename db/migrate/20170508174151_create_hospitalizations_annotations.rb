class CreateHospitalizationsAnnotations < ActiveRecord::Migration
  def change
    create_table :hospitalizations_annotations, :id => false do |t|
      t.references :hospital_admission, index: true, foreign_key: true
      t.references :medical_annotation, index: true, foreign_key: true
    end
  end
end
