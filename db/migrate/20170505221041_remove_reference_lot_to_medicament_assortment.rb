class RemoveReferenceLotToMedicamentAssortment < ActiveRecord::Migration
  def change
    remove_reference(:medicament_assortments, :lot, index: true, foreign_key: true)
  end
end
