class CreateGeneralConsultationTypes < ActiveRecord::Migration
  def change
    create_table :general_consultation_types do |t|
      t.string :name
      t.float :cost

      t.timestamps null: false
    end
  end
end
