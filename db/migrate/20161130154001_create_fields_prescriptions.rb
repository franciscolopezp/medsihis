class CreateFieldsPrescriptions < ActiveRecord::Migration
  def change
    create_table :fields_prescriptions do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
