class CreatePersonalsupports < ActiveRecord::Migration
  def change
    create_table :personalsupports do |t|
      t.string :name
      t.string :lastname
      t.string :email
      t.string :contact_number

      t.timestamps null: false
    end
  end
end
