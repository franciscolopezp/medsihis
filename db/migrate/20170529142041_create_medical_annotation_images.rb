class CreateMedicalAnnotationImages < ActiveRecord::Migration
  def change
    create_table :medical_annotation_images do |t|
      t.references :medical_annotation_type, index: true, foreign_key: true
      t.string :f_key
      t.string :image

      t.timestamps null: false
    end
  end
end
