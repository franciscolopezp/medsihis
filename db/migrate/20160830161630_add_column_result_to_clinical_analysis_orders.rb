class AddColumnResultToClinicalAnalysisOrders < ActiveRecord::Migration
  def change
    add_column :clinical_analysis_orders, :result, :text
  end
end
