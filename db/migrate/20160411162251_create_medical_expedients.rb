class CreateMedicalExpedients < ActiveRecord::Migration
  def change
    create_table :medical_expedients do |t|
      t.string :code
      t.references :patient, index: true, foreign_key: true
      t.references :doctor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
