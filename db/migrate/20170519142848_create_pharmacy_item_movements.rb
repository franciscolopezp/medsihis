class CreatePharmacyItemMovements < ActiveRecord::Migration
  def change
    create_table :pharmacy_item_movements do |t|
      t.integer :quantity
      t.references :pharmacy_item, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
