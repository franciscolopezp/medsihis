class ChangeCommentsToText < ActiveRecord::Migration
  def change
    change_column :index_comments, :comments, :text
  end
end
