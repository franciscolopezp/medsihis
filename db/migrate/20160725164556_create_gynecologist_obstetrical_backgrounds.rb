class CreateGynecologistObstetricalBackgrounds < ActiveRecord::Migration
  def change
    create_table :gynecologist_obstetrical_backgrounds do |t|
      t.string :menstruation_start_age
      t.boolean :married
      t.string :spouse_name
      t.integer :children_number
      t.integer :natural_birth
      t.string :caesarean_surgery
      t.integer :abortion_number
      t.string :abortion_causes
      t.string :menstruation_type
      t.boolean :sexually_active
      t.string :contraceptives_method
      t.string :menopause
      t.string :anotations
      t.references :medical_history, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
