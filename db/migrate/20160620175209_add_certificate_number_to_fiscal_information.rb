class AddCertificateNumberToFiscalInformation < ActiveRecord::Migration
  def change
    add_column :fiscal_informations, :certificate_number, :string
  end
end
