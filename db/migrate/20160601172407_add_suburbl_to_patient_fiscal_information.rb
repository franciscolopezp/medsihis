class AddSuburblToPatientFiscalInformation < ActiveRecord::Migration
  def change
    add_column :patient_fiscal_informations, :suburb, :string
  end
end
