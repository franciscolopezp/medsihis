class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :name
      t.string :last_name
      t.string :gender
      t.date :birth_day
      t.float :weight
      t.float :height
      t.string :phone
      t.string :cell
      t.string :email
      t.references :city, index: true, foreign_key: true
      t.string :address
      t.string :blood_type

      t.timestamps null: false
    end
  end
end
