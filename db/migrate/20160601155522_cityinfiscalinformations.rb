class Cityinfiscalinformations < ActiveRecord::Migration
  def change
    add_reference :patient_fiscal_informations, :city, index: true
  end
end
