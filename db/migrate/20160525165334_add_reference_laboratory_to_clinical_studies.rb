class AddReferenceLaboratoryToClinicalStudies < ActiveRecord::Migration
  def change
    add_reference :clinical_studies, :laboratory, index: true, foreign_key: true
  end
end
