class AddPresShowHeadCircumferenceToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :pres_show_head_circumference, :boolean
  end
end
