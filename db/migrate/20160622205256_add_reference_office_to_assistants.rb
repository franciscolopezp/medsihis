class AddReferenceOfficeToAssistants < ActiveRecord::Migration
  def change
    add_reference :assistants, :office, index: true, foreign_key: true
  end
end
