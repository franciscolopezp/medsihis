class RemoveDoctorReferenceAccounts < ActiveRecord::Migration
  def change
    remove_reference(:accounts, :doctor, index: true, foreign_key: true)
    add_reference :accounts, :user, index: true, foreign_key: true
  end
end
