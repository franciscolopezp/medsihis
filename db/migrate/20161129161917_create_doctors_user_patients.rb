class CreateDoctorsUserPatients < ActiveRecord::Migration
  def change
    create_table :doctors_user_patients, :id => false do |t|
      t.references :doctor, :null => false
      t.references :user_patient, :null => false
    end
    add_foreign_key :doctors_user_patients, :doctors
    add_foreign_key :doctors_user_patients, :user_patients
  end
end
