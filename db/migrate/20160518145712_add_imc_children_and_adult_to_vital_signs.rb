class AddImcChildrenAndAdultToVitalSigns < ActiveRecord::Migration
  def change
    add_reference :vital_signs, :imc_adult, index: true, foreign_key: true
    add_reference :vital_signs, :imc_children_max19_year, index: true, foreign_key: true
  end
end
