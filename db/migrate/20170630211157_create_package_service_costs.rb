class CreatePackageServiceCosts < ActiveRecord::Migration
  def change
    create_table :package_service_costs do |t|
      t.decimal :cost, precision: 10, scale: 2

      t.timestamps null: false
    end
  end
end
