class CreateFinkokConfigs < ActiveRecord::Migration
  def change
    create_table :finkok_configs do |t|
      t.string :test_username
      t.string :test_password
      t.string :prod_username
      t.string :prod_password
      t.string :test_wsdl
      t.string :prod_wsdl

      t.timestamps null: false
    end
  end
end
