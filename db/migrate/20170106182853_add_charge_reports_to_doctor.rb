class AddChargeReportsToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :charge_reports, :boolean
  end
end
