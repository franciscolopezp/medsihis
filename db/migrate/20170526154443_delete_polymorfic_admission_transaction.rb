class DeletePolymorficAdmissionTransaction < ActiveRecord::Migration
  def change
    remove_column :admission_transactions, :chargeable_type
    remove_column :admission_transactions, :chargeable_id
  end
end
