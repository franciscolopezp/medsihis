class CreateNervousSis < ActiveRecord::Migration
  def change
    create_table :nervous_sis do |t|
      t.text :headache
      t.text :convulsions
      t.text :transitory_deficit
      t.text :confusion
      t.text :obnubilation
      t.text :march
      t.text :balance
      t.text :language
      t.text :sleep_vigil

      t.timestamps null: false
    end
  end
end
