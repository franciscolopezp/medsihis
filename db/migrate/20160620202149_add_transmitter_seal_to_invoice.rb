class AddTransmitterSealToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :transmitter_seal, :text
  end
end
