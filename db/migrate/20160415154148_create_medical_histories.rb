class CreateMedicalHistories < ActiveRecord::Migration
  def change
    create_table :medical_histories do |t|
      t.string :nonpathological
      t.string :pathological
      t.boolean :overweight
      t.boolean :hypertension
      t.boolean :diabetes
      t.boolean :relative

      t.timestamps null: false
    end
  end
end
