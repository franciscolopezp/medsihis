class AddOwnerToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :owner_type, :string
    add_column :activities, :owner_id, :integer
  end
end
