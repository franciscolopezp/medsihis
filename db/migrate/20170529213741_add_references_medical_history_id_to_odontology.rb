class AddReferencesMedicalHistoryIdToOdontology < ActiveRecord::Migration
  def change
    add_reference :tooth_treatments, :medical_history, index: true, foreign_key: true
    add_reference :tooth_pathologies, :medical_history, index: true, foreign_key: true
    add_column :tooth_pathologies, :is_original, :boolean
    add_column :tooth_treatments, :is_original, :boolean
  end
end
