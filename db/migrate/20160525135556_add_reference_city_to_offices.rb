class AddReferenceCityToOffices < ActiveRecord::Migration
  def change
    add_reference :offices, :city, index: true, foreign_key: true
  end
end
