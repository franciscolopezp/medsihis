class AddReferenceGenderToDoctors < ActiveRecord::Migration
  def change
    add_reference :doctors, :gender, index: true, foreign_key: true
  end
end
