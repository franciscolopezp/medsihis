class RenameAgendaPermissionToPermission < ActiveRecord::Migration
  def change
    rename_table :agenda_permissions, :permissions
  end
end
