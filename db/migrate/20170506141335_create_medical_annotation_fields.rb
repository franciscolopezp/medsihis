class CreateMedicalAnnotationFields < ActiveRecord::Migration
  def change
    create_table :medical_annotation_fields do |t|
      t.references :medical_annotation_type, index: true, foreign_key: true
      t.string :f_key
      t.string :label
      t.string :f_html_type

      t.timestamps null: false
    end
  end
end
