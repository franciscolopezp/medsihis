class CreateAbortionTypes < ActiveRecord::Migration
  def change
    create_table :abortion_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
