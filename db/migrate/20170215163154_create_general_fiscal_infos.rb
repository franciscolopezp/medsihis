class CreateGeneralFiscalInfos < ActiveRecord::Migration
  def change
    create_table :general_fiscal_infos do |t|
      t.text :business_name
      t.string :rfc
      t.string :address
      t.string :locality
      t.string :ext_number
      t.string :int_number
      t.string :zip
      t.string :suburb
      t.references :doctor, index: true, foreign_key: true
      t.references :city, index: true, foreing_key: true
      t.timestamps null: false
    end
  end
end
