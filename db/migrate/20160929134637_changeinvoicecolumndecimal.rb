class Changeinvoicecolumndecimal < ActiveRecord::Migration
  def change
    change_column :invoices, :iva, :decimal, :precision => 11, :scale => 2
    change_column :invoices, :subtotal, :decimal, :precision => 11, :scale => 2
  end
end
