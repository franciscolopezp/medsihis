class RemoveConsultCostFromDoctors < ActiveRecord::Migration
  def change
    remove_column :doctors, :consult_cost, :decimal
    remove_column :offices, :consult_cost, :string
  end
end
