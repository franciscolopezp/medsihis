class CreateToothTreatments < ActiveRecord::Migration
  def change
    create_table :tooth_treatments do |t|
      t.text :description
      t.references :tooth_section, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
