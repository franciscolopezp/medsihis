class CreateMedicalAnnotationTypes < ActiveRecord::Migration
  def change
    create_table :medical_annotation_types do |t|
      t.string :name
      t.text :template

      t.timestamps null: false
    end
  end
end
