class Addvaluestoinvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :certification_date, :string
    add_column :invoices, :payment_condition, :string
    add_column :invoices, :payment_method, :string
    add_column :invoices, :account_number, :string
    add_column :invoices, :subtotal, :float
    add_column :invoices, :iva, :float
    add_column :invoices, :isr, :float
    add_column :invoices, :UUID, :string
    add_column :invoices, :xml, :text
  end
end
