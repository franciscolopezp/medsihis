class CreateAdmissionTransactions < ActiveRecord::Migration
  def change
    create_table :admission_transactions do |t|
      t.float :amount
      t.references :chargeable, polymorphic: true, index: {:name => "index_charges_on_chargeable"}
      t.references :user, index: true, foreign_key: true
      t.references :hospital_admission, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
