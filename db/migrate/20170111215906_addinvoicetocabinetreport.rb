class Addinvoicetocabinetreport < ActiveRecord::Migration
  def change
    add_column :cabinet_reports, :invoiced, :boolean
  end
end
