class Addreferencestoaddressbook < ActiveRecord::Migration
  def change
    add_reference :address_books, :doctor, index: true, foreign_key: true
    add_reference :address_books, :hospital, index: true, foreign_key: true
    add_reference :address_books, :office, index: true, foreign_key: true
  end
end
