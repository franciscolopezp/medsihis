class CreateBirthInfos < ActiveRecord::Migration
  def change
    create_table :birth_infos do |t|
      t.datetime :date
      t.references :birth_type, index: true, foreign_key: true
      t.string :observations
      t.string :rpm
      t.string :hospital
      t.references :delivery_type, index: true, foreign_key: true
      t.references :episiotomy_type, index: true, foreign_key: true
      t.references :tear_type, index: true, foreign_key: true
      t.references :duration_type, index: true, foreign_key: true
      t.string :aditional_observations
      t.references :pregnancy, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
