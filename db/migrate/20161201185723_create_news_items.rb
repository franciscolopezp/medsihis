class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.string :banner
      t.string :title
      t.string :summary
      t.text :content
      t.integer :status

      t.timestamps null: false
    end
  end
end
