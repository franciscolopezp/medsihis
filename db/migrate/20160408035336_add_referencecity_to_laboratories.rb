class AddReferencecityToLaboratories < ActiveRecord::Migration
  def change
    add_reference :laboratories, :city, index: true, foreign_key: true
  end
end
