class CreateMedicalAnnotations < ActiveRecord::Migration
  def change
    create_table :medical_annotations do |t|
      t.references :medical_annotation_type, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
