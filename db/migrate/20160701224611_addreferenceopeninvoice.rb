class Addreferenceopeninvoice < ActiveRecord::Migration
  def change
    add_reference :open_invoice_receptors, :invoice, index: true
  end
end
