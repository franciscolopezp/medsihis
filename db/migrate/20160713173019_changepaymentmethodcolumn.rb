class Changepaymentmethodcolumn < ActiveRecord::Migration
  def change
    rename_column :invoices, :payment_method, :payment_methods_name
  end
end
