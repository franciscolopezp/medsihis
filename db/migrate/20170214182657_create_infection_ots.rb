class CreateInfectionOts < ActiveRecord::Migration
  def change
    create_table :infection_ots do |t|
      t.text :contacts
      t.text :stings
      t.text :environment
      t.text :foods
      t.text :travels
      t.text :internal_surgery_prosthesis
      t.text :intravenous_drugs
      t.text :tattoos
      t.text :transfusions
      t.text :sexual_activity
      t.references :medical_history, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
