class AddLogoAndBirthplaceToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :picture, :string
    add_column :patients, :birthplace, :string
  end
end
