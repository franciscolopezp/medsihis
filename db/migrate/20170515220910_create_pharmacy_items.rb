class CreatePharmacyItems < ActiveRecord::Migration
  def change
    create_table :pharmacy_items do |t|
      t.string :name
      t.string :description
      t.float :cost
      t.references :measure_unit_item, index:true, foreign_key:true
      t.timestamps null: false
    end
  end
end
