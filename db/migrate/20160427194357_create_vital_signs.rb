class CreateVitalSigns < ActiveRecord::Migration
  def change
    create_table :vital_signs do |t|
      t.float :weight
      t.float :height
      t.float :temperature
      t.float :blood_pressure
      t.float :glucose
      t.integer :imc
      t.float :suggested_weight

      t.timestamps null: false
    end
  end
end
