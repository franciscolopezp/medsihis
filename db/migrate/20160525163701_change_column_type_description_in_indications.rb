class ChangeColumnTypeDescriptionInIndications < ActiveRecord::Migration
  def change
    change_column :indications, :description, :text
  end
end
