class ChangeColumnNameDataOrderInClinicalAnalysisOrders < ActiveRecord::Migration
  def change
    rename_column :clinical_analysis_orders, :data_order, :date_order
  end
end
