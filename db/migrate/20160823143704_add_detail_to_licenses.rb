class AddDetailToLicenses < ActiveRecord::Migration
  def change
    add_column :licenses, :detail, :text
  end
end
