class CreateAssistantsExpedientPermissions < ActiveRecord::Migration
  def change
    create_table :assistants_expedient_permissions, :id => false do |t|
      t.references :assistant, index: true
      t.references :expedient_permission, index: {:name => "expedient_permission"}
    end
    add_foreign_key :assistants_expedient_permissions, :assistants
    add_foreign_key :assistants_expedient_permissions, :expedient_permissions
  end
end
