class CreatePathologicalAllergies < ActiveRecord::Migration
  def change
    create_table :pathological_allergies do |t|
      t.boolean :allergy_any_medications
      t.text :allergy_medications_description
      t.text :other_allergies

      t.timestamps null: false
    end
  end
end
