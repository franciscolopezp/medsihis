class CreateHospitalAdmissionDiscounts < ActiveRecord::Migration
  def change
    create_table :hospital_admission_discounts do |t|
      t.references :hospital_admission, index: true, foreign_key: true
      t.float :amount
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
