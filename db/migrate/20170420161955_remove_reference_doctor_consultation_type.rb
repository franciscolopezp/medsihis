class RemoveReferenceDoctorConsultationType < ActiveRecord::Migration
  def change
    remove_reference(:consultation_types, :doctor, index: true, foreign_key: true)
  end
end
