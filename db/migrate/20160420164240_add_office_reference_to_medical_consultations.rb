class AddOfficeReferenceToMedicalConsultations < ActiveRecord::Migration
  def change
    add_reference :medical_consultations, :office, index: true, foreign_key: true
  end
end
