class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.string :last_name
      t.date :birth_day
      t.string :telephone
      t.string :cellphone
      t.string :address
      t.references :city, index: true, foreign_key: true
      t.references :gender, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
