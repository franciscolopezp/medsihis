class Patientswithfiscalinformations < ActiveRecord::Migration
  def change
    add_reference :patient_fiscal_informations, :patient, index: true
  end
end
