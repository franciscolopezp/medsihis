class CreateFixedAssets < ActiveRecord::Migration
  def change
    create_table :fixed_assets do |t|
      t.string :name
      t.references :fixed_asset_category, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
