class CreateAssistants < ActiveRecord::Migration
  def change
    create_table :assistants do |t|
      t.string :name
      t.string :last_name
      t.string :gender
      t.date :birth_day
      t.string :telephone
      t.string :cellphone
      t.string :email
      t.string :address

      t.timestamps null: false
    end
  end
end
