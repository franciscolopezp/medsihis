class AddReferenceClinicalOrderTypeToLaboratories < ActiveRecord::Migration
  def change
    add_reference :laboratories, :clinical_order_type, index: true, foreign_key: true
  end
end
