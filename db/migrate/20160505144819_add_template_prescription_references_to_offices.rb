class AddTemplatePrescriptionReferencesToOffices < ActiveRecord::Migration
  def change
    add_reference :offices, :template_prescription, index: true, foreign_key: true
  end
end
