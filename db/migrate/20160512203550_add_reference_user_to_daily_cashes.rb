class AddReferenceUserToDailyCashes < ActiveRecord::Migration
  def change
    add_reference :daily_cashes, :user, index: true, foreign_key: true
  end
end
