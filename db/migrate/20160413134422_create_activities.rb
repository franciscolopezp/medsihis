class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name
      t.string :details
      t.datetime :start_date
      t.datetime :end_date
      t.integer :type

      t.timestamps null: false
    end
  end
end
