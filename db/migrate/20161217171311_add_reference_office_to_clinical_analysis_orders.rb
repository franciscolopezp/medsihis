class AddReferenceOfficeToClinicalAnalysisOrders < ActiveRecord::Migration
  def change
    add_reference :clinical_analysis_orders, :office, index: true, foreign_key: true
  end
end
