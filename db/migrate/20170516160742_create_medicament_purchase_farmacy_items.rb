class CreateMedicamentPurchaseFarmacyItems < ActiveRecord::Migration
  def change
    create_table :medicament_purchase_farmacy_items do |t|
      t.integer :quantity
      t.decimal :cost, precision: 10, scale: 2
      t.references :medicament_purchase, index: {:name => "purchase_pharmacy_item_id"}, foreign_key: true
      t.references :pharmacy_item, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
