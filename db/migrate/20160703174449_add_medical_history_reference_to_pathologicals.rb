class AddMedicalHistoryReferenceToPathologicals < ActiveRecord::Migration
  def change
    add_reference :pathologicals, :medical_history, index: true, foreign_key: true
  end
end
