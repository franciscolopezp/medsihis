class CreateMedicamentAssortmentItems < ActiveRecord::Migration
  def change
    create_table :medicament_assortment_items do |t|
      t.integer :quantity
      t.float :cost
      t.references :lot, index: true, foreign_key: true
      t.references :medicament, index: true, foreign_key: true
      t.references :medicament_assortment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
