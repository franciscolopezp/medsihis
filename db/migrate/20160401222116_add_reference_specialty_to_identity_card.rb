class AddReferenceSpecialtyToIdentityCard < ActiveRecord::Migration
  def change
    add_reference :identity_cards, :specialty, index: true, foreign_key: true
  end
end
