class AddDefaultToExpedientRequest < ActiveRecord::Migration
  def change
    add_column :expedient_requests, :is_default, :boolean
  end
end
