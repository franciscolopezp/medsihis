class AddCertificateBase64ToFiscalInformation < ActiveRecord::Migration
  def change
    add_column :fiscal_informations, :certificate_base64, :text
  end
end
