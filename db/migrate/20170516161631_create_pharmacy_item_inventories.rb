class CreatePharmacyItemInventories < ActiveRecord::Migration
  def change
    create_table :pharmacy_item_inventories do |t|
      t.integer :quantity
      t.references :user, index: true, foreign_key: true
      t.references :warehouse, index: true, foreign_key: true
      t.references :pharmacy_item, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
