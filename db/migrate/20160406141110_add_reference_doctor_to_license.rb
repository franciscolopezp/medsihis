class AddReferenceDoctorToLicense < ActiveRecord::Migration
  def change
    add_reference :licenses, :doctor, index: true, foreign_key: true
  end
end
