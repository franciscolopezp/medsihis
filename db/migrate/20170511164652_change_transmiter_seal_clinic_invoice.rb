class ChangeTransmiterSealClinicInvoice < ActiveRecord::Migration
  def change
    change_column :clinic_invoices, :transmitter_seal, :text
  end
end
