class ChangeColumnTypeFooterCmInCustomTemplatePrescriptions < ActiveRecord::Migration
  def change
    change_column :custom_template_prescriptions, :footer_cm, :float
  end
end
