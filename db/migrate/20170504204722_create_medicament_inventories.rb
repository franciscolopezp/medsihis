class CreateMedicamentInventories < ActiveRecord::Migration
  def change
    create_table :medicament_inventories do |t|
      t.integer :quantity
      t.references :lot, index: true, foreign_key: true
      t.references :warehouse, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
