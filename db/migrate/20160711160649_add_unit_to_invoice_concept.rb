class AddUnitToInvoiceConcept < ActiveRecord::Migration
  def change
    add_column :invoice_concepts, :unit, :string
  end
end
