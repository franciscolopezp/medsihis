class AddReferencePolymorphicMedicamentLog < ActiveRecord::Migration
  def change
    add_reference :medicament_logs, :traceable, polymorphic: true, index: true
  end
end
