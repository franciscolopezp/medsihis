class AddIsClosedToHospitalAdmission < ActiveRecord::Migration
  def change
    add_column :hospital_admissions, :is_closed, :boolean
  end
end
