class CreateDoctorsUsers < ActiveRecord::Migration
  def change
    create_table :doctors_users, id:false do |t|
      t.belongs_to :doctor, index: true
      t.belongs_to :user, index: true
    end
    add_foreign_key :doctors_users, :doctors
    add_foreign_key :doctors_users, :users
  end
end
