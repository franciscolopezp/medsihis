class RemaneColumnMedicamentAssortment < ActiveRecord::Migration
  def change
    rename_column :medicament_assortments, :quantity, :folio
  end
end
