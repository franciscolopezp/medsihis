class CreateCustomTemplateReportFields < ActiveRecord::Migration
  def change
    create_table :custom_template_report_fields do |t|
      t.string :font
      t.boolean :is_bold
      t.integer :size
      t.float :position_top_cm
      t.float :position_left_cm
      t.boolean :show_field
      t.string :section
      t.float :width_box
      t.string :extra

      t.timestamps null: false
    end
  end
end
