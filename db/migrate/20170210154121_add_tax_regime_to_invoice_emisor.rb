class AddTaxRegimeToInvoiceEmisor < ActiveRecord::Migration
  def change
    add_column :invoice_emisors, :tax_regime, :string
  end
end
