class AddPersonTypeToGeneralFiscalInfo < ActiveRecord::Migration
  def change
    add_column :general_fiscal_infos, :person_type, :string
  end
end
