class CreateDefaultGinecologyBackgrounds < ActiveRecord::Migration
  def change
    create_table :default_ginecology_backgrounds do |t|
      t.boolean :ginecopatia
      t.text :ginecopatia_description
      t.boolean :mastopatia
      t.text :mastopatia_description
      t.boolean :gemelaridad
      t.text :gemelaridad_description
      t.boolean :congenital_anomalies
      t.text :congenital_anomalies_description
      t.boolean :infertility
      t.text :infertility_description
      t.boolean :others
      t.text :others_diseases
      t.text :annotations
      t.string :menstruation_start_age
      t.boolean :married
      t.string :spouse_name
      t.integer :gestations
      t.integer :natural_births
      t.string :cesareans
      t.integer :abortions
      t.string :abortion_causes
      t.string :menstruation_type
      t.boolean :sexually_active
      t.string :contraceptives_method
      t.string :menopause
      t.text :obstetrical_annotations
      t.string :spouse_information
      t.string :cycles
      t.string :ivs
      t.integer :sexual_partners
      t.string :std
      t.timestamps null: false
    end
  end
end
