class Addreferenceemisor < ActiveRecord::Migration
  def change
    add_reference :admission_invoices, :emisor_fiscal_information, index: true, foreign_key: true
  end
end
