class AddReferenceBloodType < ActiveRecord::Migration
  def change
    add_reference :patients, :blood_type, index: true, foreign_key: true
  end
end
