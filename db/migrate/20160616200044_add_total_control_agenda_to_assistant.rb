class AddTotalControlAgendaToAssistant < ActiveRecord::Migration
  def change
    add_column :assistants, :total_control_agenda, :boolean
  end
end
