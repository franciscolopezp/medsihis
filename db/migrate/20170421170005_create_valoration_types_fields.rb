class CreateValorationTypesFields < ActiveRecord::Migration
  def change
    create_table :valoration_types_fields, :id => false do |t|
      t.references :valoration_type, index: true, foreign_key: true
      t.references :valoration_field, index: true, foreign_key: true
    end
  end
end
