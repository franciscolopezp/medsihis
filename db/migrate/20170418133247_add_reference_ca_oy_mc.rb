class AddReferenceCaOyMc < ActiveRecord::Migration
  def change
    add_reference :clinical_analysis_orders, :user, index: true, foreign_key: true
    add_reference :medical_consultations, :user, index: true, foreign_key: true
  end
end
