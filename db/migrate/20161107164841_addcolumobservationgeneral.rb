class Addcolumobservationgeneral < ActiveRecord::Migration
  def change
    add_column :medical_expedients, :general_observations, :text
  end
end
