class AccountsTransactions < ActiveRecord::Migration
  def change
    create_table :accounts_transactions, id:false do |t|
      t.belongs_to :account, index: true
      t.belongs_to :transaction, index: true
    end
    add_foreign_key :accounts_transactions, :accounts
    add_foreign_key :accounts_transactions, :transactions
  end
end
