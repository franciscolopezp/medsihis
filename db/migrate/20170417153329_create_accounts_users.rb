class CreateAccountsUsers < ActiveRecord::Migration
  def change
    create_table :accounts_users, id:false do |t|
      t.belongs_to :account, index: true
      t.belongs_to :user, index: true
    end
    add_foreign_key :accounts_users, :accounts
    add_foreign_key :accounts_users, :users
  end
end
