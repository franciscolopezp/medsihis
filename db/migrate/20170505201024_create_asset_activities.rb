class CreateAssetActivities < ActiveRecord::Migration
  def change
    create_table :asset_activities do |t|
      t.references :fixed_asset_item, index: true, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date
      t.string :title
      t.string :details
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
