class AddreferenceHospitalAdmissionToMedicamentAssortment < ActiveRecord::Migration
  def change
    remove_reference(:medicament_assortments, :admission_transaction, index: true, foreign_key: true)
    add_reference :medicament_assortments, :hospital_admission, index: true, foreign_key: true
  end
end
