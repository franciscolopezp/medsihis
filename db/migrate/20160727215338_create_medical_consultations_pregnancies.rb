class CreateMedicalConsultationsPregnancies < ActiveRecord::Migration
  def change
    create_table :medical_consultations_pregnancies, id: false do |t|
      t.belongs_to :medical_consultation, index: {:name => "mcp_consultation"}
      t.belongs_to :pregnancy, index: {:name => "mcp_pregnancy"}
    end

    add_foreign_key :medical_consultations_pregnancies, :medical_consultations
    add_foreign_key :medical_consultations_pregnancies, :pregnancies
  end
end
