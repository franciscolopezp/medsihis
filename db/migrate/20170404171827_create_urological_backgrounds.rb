class CreateUrologicalBackgrounds < ActiveRecord::Migration
  def change
    create_table :urological_backgrounds do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.boolean :testicular_pain
      t.boolean :impotence
      t.boolean :erection_dificulty
      t.boolean :premature_ejaculation
      t.boolean :vaginal_discharge
      t.boolean :anormal_hair_growth
      t.boolean :back_pain
      t.boolean :kidneys_pain
      t.boolean :pelvic_pain
      t.boolean :low_abdomen_pain
      t.boolean :kidney_bk
      t.boolean :kidney_stones
      t.boolean :venereal_diseases
      t.boolean :self_examination
      t.boolean :prostate
      t.date :last_prostate_examination
      t.date :fum
      t.integer :gestations
      t.date :last_pap_date
      t.string :contraceptive_method
      t.boolean :sexually_active
      t.date :sex_life_start
      t.integer :sexual_partners
      t.string :relationship_type
      t.boolean :satisfying_sex
      t.boolean :intercourse_pain

      t.timestamps null: false
    end
  end
end
