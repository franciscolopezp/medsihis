class AddCodeToPaymentMethod < ActiveRecord::Migration
  def change
    add_column :payment_methods, :code, :string
  end
end
