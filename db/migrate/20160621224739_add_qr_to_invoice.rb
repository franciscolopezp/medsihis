class AddQrToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :qr_code, :string
  end
end
