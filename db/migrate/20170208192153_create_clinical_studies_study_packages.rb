class CreateClinicalStudiesStudyPackages < ActiveRecord::Migration
  def change
    create_table :clinical_studies_study_packages, :id => false do |t|
      t.references :study_package, index: true, foreign_key: true
      t.references :clinical_study, index: true, foreign_key: true
    end
  end
end
