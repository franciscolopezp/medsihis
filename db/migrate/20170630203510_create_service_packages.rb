class CreateServicePackages < ActiveRecord::Migration
  def change
    create_table :service_packages do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
