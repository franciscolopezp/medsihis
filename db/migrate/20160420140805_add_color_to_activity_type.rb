class AddColorToActivityType < ActiveRecord::Migration
  def change
    add_column :activity_types, :color, :string
  end
end
