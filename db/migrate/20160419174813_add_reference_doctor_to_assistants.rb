class AddReferenceDoctorToAssistants < ActiveRecord::Migration
  def change
    add_reference :assistants, :doctor, index: true, foreign_key: true
    add_reference :assistants, :office, index: true, foreign_key: true
    add_reference :assistants, :user, index: true, foreign_key: true
  end
end
