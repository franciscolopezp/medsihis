class CreateTriages < ActiveRecord::Migration
  def change
    create_table :triages do |t|
      t.string :level
      t.string :attention_time
      t.string :description
      t.string :color

      t.timestamps null: false
    end
  end
end
