class ChangeColumnNameHospitalService < ActiveRecord::Migration
  def change
    change_column :hospital_services, :name, :text
  end
end
