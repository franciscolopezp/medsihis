class AddMedicamentTagsPharmacyTags < ActiveRecord::Migration
  def change
    add_column :medicaments, :tags, :text
    add_column :pharmacy_items, :tags, :text
  end
end
