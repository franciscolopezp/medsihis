class AddSaleCostMedPhar < ActiveRecord::Migration
  def change
    add_column :medicaments, :sale_cost, :decimal, precision: 10, scale: 2
    add_column :pharmacy_items, :sale_cost, :decimal, precision: 10, scale: 2
  end
end
