class ChangeContraindicationInMedicaments < ActiveRecord::Migration
  def change
    change_column :medicaments, :contraindication, :text
    change_column :medicaments, :presentation, :text
    change_column :medicaments, :comercial_name, :text
    change_column :medicaments, :active_substance, :text
  end
end
