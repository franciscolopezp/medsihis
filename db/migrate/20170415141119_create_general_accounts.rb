class CreateGeneralAccounts < ActiveRecord::Migration
  def change
    create_table :general_accounts do |t|
      t.string :account_number
      t.string :clabe
      t.float :balance
      t.string :details
      t.references :bank, index: true, foreign_key: true
      t.references :currency, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
