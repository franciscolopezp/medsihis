class CreateParentInfos < ActiveRecord::Migration
  def change
    create_table :parent_infos do |t|
      t.references :child_non_pathological_info, index: true, foreign_key: true
      t.string :first_name
      t.string :last_name
      t.string :gender
      t.date :birth_date
      t.string :blood_type
      t.string :academic_grade

      t.timestamps null: false
    end
  end
end
