class AddMedicamentCategoryToMedicament < ActiveRecord::Migration
  def change
    add_reference :medicaments, :medicament_category, index: true, foreign_key: true, :after => :id
  end
end
