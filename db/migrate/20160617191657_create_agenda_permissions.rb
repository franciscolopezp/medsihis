class CreateAgendaPermissions < ActiveRecord::Migration
  def change
    create_table :agenda_permissions do |t|
      t.string :name
      t.string :label

      t.timestamps null: false
    end
  end
end
