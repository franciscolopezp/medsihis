class CreateOtolaryngologySis < ActiveRecord::Migration
  def change
    create_table :otolaryngology_sis do |t|
      t.text :otalgia
      t.references :medical_history, index: true, foreign_key: true
      t.text :otorrea
      t.text :otorragia
      t.text :hypoacusia
      t.text :epistaxis
      t.text :rinorrea
      t.text :odinofagia
      t.text :phonation

      t.timestamps null: false
    end
  end
end
