class AddReferenceLaboratoryToDoctors < ActiveRecord::Migration
  def change
    add_reference :doctors, :laboratory, index: true, foreign_key: true
  end
end
