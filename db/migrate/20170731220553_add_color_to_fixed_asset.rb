class AddColorToFixedAsset < ActiveRecord::Migration
  def change
    add_column :fixed_assets, :color, :string
  end
end
