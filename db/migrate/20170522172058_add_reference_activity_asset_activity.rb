class AddReferenceActivityAssetActivity < ActiveRecord::Migration
  def change
    add_reference :asset_activities, :fixed_asset_activity, index: true, foreign_key: true
    remove_reference(:fixed_asset_items, :fixed_asset_activity, index: true, foreign_key: true)
  end
end
