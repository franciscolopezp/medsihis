class AddColorToFixedAssetActivity < ActiveRecord::Migration
  def change
    add_column :fixed_asset_activities, :color, :string
  end
end
