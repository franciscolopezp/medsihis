class AddIsCancelToAdmissionTransaction < ActiveRecord::Migration
  def change
    add_column :admission_transactions, :is_cancel, :boolean
  end
end
