class CreateFiscalInformations < ActiveRecord::Migration
  def change
    create_table :fiscal_informations do |t|
      t.string :bussiness_name
      t.string :fiscal_address
      t.string :ext_number
      t.string :certificate_stamp
      t.string :certificate_key
      t.string :certificate_password
      t.string :tax_regime

      t.timestamps null: false
    end
  end
end
