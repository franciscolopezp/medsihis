class CreateMedicalIndications < ActiveRecord::Migration
  def change
    create_table :medical_indications do |t|
      t.text :dose
      t.references :medicament, index: true, foreign_key: true
      t.references :prescription, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
