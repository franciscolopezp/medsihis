class ClinicalStudiesIndications < ActiveRecord::Migration
  def change
    create_table :clinical_studies_indications, :id => false do |t|
      t.integer :clinical_study_id
      t.integer :indication_id
      end
  end
end
