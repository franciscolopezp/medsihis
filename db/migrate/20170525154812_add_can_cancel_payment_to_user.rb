class AddCanCancelPaymentToUser < ActiveRecord::Migration
  def change
    add_column :users, :can_cancel_payment, :boolean
  end
end
