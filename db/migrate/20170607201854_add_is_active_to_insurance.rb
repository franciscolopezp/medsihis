class AddIsActiveToInsurance < ActiveRecord::Migration
  def change
    add_column :insurances, :is_active, :boolean
  end
end
