class AddFolioToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions, :folio, :integer
  end
end
