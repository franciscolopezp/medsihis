class AddLabelToCustomTemplateReportField < ActiveRecord::Migration
  def change
    add_column :custom_template_report_fields, :label, :string
  end
end
