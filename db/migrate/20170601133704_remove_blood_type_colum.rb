class RemoveBloodTypeColum < ActiveRecord::Migration
  def change
    remove_column :patients, :blood_type
    remove_column :parent_infos, :blood_type
    add_reference :parent_infos, :blood_type, index: true, foreign_key: true
  end
end
