class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.references :medical_consultation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
