class CreateOtherMovements < ActiveRecord::Migration
  def change
    create_table :other_movements do |t|
      t.references :movement_type, index: true, foreign_key: true
      t.string :details

      t.timestamps null: false
    end
  end
end
