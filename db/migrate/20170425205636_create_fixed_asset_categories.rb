class CreateFixedAssetCategories < ActiveRecord::Migration
  def change
    create_table :fixed_asset_categories do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
