class Addivadoctorinvoicecabinetreport < ActiveRecord::Migration
  def change
    add_column :doctors, :iva_report, :boolean
    add_column :doctors, :iva_report_included, :boolean
    add_reference :cabinet_reports, :invoice, index: true, foreign_key: true
  end
end
