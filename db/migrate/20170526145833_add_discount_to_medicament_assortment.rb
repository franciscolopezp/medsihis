class AddDiscountToMedicamentAssortment < ActiveRecord::Migration
  def change
    add_column :medicament_assortments, :discount, :decimal, precision: 10, scale: 2
  end
end
