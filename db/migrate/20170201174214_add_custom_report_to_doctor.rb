class AddCustomReportToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :is_active_custom_report, :boolean
  end
end
