class CreateSenseOrgans < ActiveRecord::Migration
  def change
    create_table :sense_organs do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.text :hair
      t.text :skin
      t.text :head
      t.text :ears
      t.text :nose
      t.text :mouth
      t.text :throat
      t.text :neck
      t.text :breasts

      t.timestamps null: false
    end
  end
end
