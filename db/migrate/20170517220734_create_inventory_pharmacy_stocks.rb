class CreateInventoryPharmacyStocks < ActiveRecord::Migration
  def change
    create_table :inventory_pharmacy_stocks do |t|
      t.integer :quantity
      t.references :pharmacy_item, index: true, foreign_key: true
      t.references :inventory, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
