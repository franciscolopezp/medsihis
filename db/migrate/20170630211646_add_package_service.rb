class AddPackageService < ActiveRecord::Migration
  def change
    add_reference :package_service_costs, :service_package, index: true, foreign_key: true
    add_reference :package_service_costs, :hospital_service, index: true, foreign_key: true
  end
end
