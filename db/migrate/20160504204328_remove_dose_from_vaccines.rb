class RemoveDoseFromVaccines < ActiveRecord::Migration
  def change
    remove_column :vaccines, :dose, :string
    remove_column :vaccines, :age_or_frecuency, :string
  end
end
