class CreateCabinetReports < ActiveRecord::Migration
  def change
    create_table :cabinet_reports do |t|
      t.text :conclusions

      t.timestamps null: false
    end
  end
end
