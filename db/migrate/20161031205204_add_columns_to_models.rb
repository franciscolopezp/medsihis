class AddColumnsToModels < ActiveRecord::Migration
  def change
    rename_column :no_pathologicals, :occupation, :scholarship

    add_column :no_pathologicals, :drugs, :boolean
    add_column :no_pathologicals, :drugs_description, :string

    add_column :surgeries, :hospitalizations, :string
    add_column :surgeries, :transfusions, :string

    add_column :inherited_family_backgrounds, :psychiatric_diseases, :boolean
    add_column :inherited_family_backgrounds, :psychiatric_diseases_description, :string
    add_column :inherited_family_backgrounds, :neurological_diseases, :boolean
    add_column :inherited_family_backgrounds, :neurological_diseases_description, :string
    add_column :inherited_family_backgrounds, :cardiovascular_diseases, :boolean
    add_column :inherited_family_backgrounds, :cardiovascular_diseases_description, :string
    add_column :inherited_family_backgrounds, :bronchopulmonary_diseases, :boolean
    add_column :inherited_family_backgrounds, :bronchopulmonary_diseases_description, :string
    add_column :inherited_family_backgrounds, :thyroid_diseases, :boolean
    add_column :inherited_family_backgrounds, :thyroid_diseases_description, :string
    add_column :inherited_family_backgrounds, :kidney_diseases, :boolean
    add_column :inherited_family_backgrounds, :kidney_diseases_description, :string


    add_column :gynecologist_obstetrical_backgrounds, :sexual_partners, :integer
    add_column :gynecologist_obstetrical_backgrounds, :relationship_type, :string
    add_column :gynecologist_obstetrical_backgrounds, :std, :string
  end
end
