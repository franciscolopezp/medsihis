class CreateIndexComments < ActiveRecord::Migration
  def change
    create_table :index_comments do |t|
      t.integer :p_type
      t.string :prefix
      t.string :first_name
      t.string :last_name
      t.date :birth_date
      t.string :job_title
      t.string :university
      t.string :gender
      t.string :comments
      t.string :photo
      t.boolean :active

      t.timestamps null: false
    end
  end
end
