class ChangeFormatSuggestedWeightInVitalSigns < ActiveRecord::Migration
  def change
    change_column :vital_signs, :suggested_weight, :string
  end
end
