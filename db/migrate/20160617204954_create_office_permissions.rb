class CreateOfficePermissions < ActiveRecord::Migration
  def change
    create_table :office_permissions do |t|
      t.references :assistant, index: true, foreign_key: true
      t.references :office, index: true, foreign_key: true
      t.references :agenda_permission, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
