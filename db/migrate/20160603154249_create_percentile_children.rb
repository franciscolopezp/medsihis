class CreatePercentileChildren < ActiveRecord::Migration
  def change
    create_table :percentile_children do |t|
      t.integer :age_day
      t.float :p01
      t.float :p1
      t.float :p3
      t.float :p5
      t.float :p10
      t.float :p15
      t.float :p25
      t.float :p50
      t.float :p75
      t.float :p85
      t.float :p90
      t.float :p95
      t.float :p97
      t.float :p99
      t.float :p999
      t.references :gender, index: true, foreign_key: true
      t.references :type_percentile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
