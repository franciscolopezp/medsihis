class CreatePerinatalInformations < ActiveRecord::Migration
  def change
    create_table :perinatal_informations do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.string :hospital
      t.references :city, index: true, foreign_key: true
      t.date :birthdate
      t.decimal :weight
      t.decimal :height
      t.string :pregnat_risks
      t.string :other_info

      t.timestamps null: false
    end
  end
end
