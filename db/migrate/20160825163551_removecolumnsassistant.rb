class Removecolumnsassistant < ActiveRecord::Migration
  def change
    remove_reference :assistants, :office, index: true, foreign_key: true
    remove_reference :assistants, :doctor, index: true, foreign_key: true
  end
end
