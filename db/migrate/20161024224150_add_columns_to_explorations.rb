class AddColumnsToExplorations < ActiveRecord::Migration
  def change
    add_column :explorations, :head, :string
    add_column :explorations, :lungs, :string
    add_column :explorations, :heart, :string
    add_column :explorations, :extremities, :string
    add_column :explorations, :other, :string
    add_column :explorations, :vagina, :string
    add_column :explorations, :pelvis, :string
    add_column :explorations, :rectum, :string
  end
end
