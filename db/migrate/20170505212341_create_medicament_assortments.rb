class CreateMedicamentAssortments < ActiveRecord::Migration
  def change
    create_table :medicament_assortments do |t|
      t.integer :quantity
      t.float :cost
      t.references :user, index: true, foreign_key: true
      t.references :warehouse, index: true, foreign_key: true
      t.references :lot, index: true, foreign_key: true
      t.references :admission_transaction, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
