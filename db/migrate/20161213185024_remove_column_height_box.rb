class RemoveColumnHeightBox < ActiveRecord::Migration
  def change
    remove_column :custom_template_prescription_fields, :height_box, :float
  end
end
