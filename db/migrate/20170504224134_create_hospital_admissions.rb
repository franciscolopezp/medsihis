class CreateHospitalAdmissions < ActiveRecord::Migration
  def change
    create_table :hospital_admissions do |t|
      t.references :user, index: true, foreign_key: true
      t.references :doctor, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true
      t.references :triage, index: true, foreign_key: true
      t.references :attention_type, index: true, foreign_key: true
      t.datetime :admission_date

      t.timestamps null: false
    end
  end
end
