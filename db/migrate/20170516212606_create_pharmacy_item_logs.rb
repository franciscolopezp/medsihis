class CreatePharmacyItemLogs < ActiveRecord::Migration
  def change
    create_table :pharmacy_item_logs do |t|
      t.integer :quantity
      t.boolean :income
      t.references :traceable, polymorphic: true, index: {:name => "index_pharmacy_on_chargeable"}
      t.references :user, index: true, foreign_key: true
      t.references :pharmacy_item, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
