class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.datetime :inventory_date
      t.references :warehouse, index: true, foreign_key: true
      t.references :medicament_category, index: true, foreign_key: true
      t.integer :created_user_id
      t.integer :updated_user_id

      t.timestamps null: false
    end
  end
end
