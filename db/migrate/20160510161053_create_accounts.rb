class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.references :doctor, index: true, foreign_key: true
      t.references :bank, index: true, foreign_key: true
      t.string :account_number
      t.string :clabe
      t.decimal :balance
      t.string :details

      t.timestamps null: false
    end
  end
end
