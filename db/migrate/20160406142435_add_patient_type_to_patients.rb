class AddPatientTypeToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :license_type, :integer
    add_column :patients, :license_start_date, :date
  end
end
