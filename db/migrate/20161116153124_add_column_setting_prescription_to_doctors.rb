class AddColumnSettingPrescriptionToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :pres_show_another_info, :boolean
    add_column :doctors, :pres_show_professional_license, :boolean
    add_column :doctors, :pres_show_speciality, :boolean
    add_column :doctors, :pres_show_studies, :boolean
    add_column :doctors, :pres_show_height, :boolean
    add_column :doctors, :pres_show_weight, :boolean
    add_column :doctors, :pres_show_folio, :boolean
    add_column :doctors, :pres_show_logo, :boolean
  end
end
