class AddAllDayToActivity < ActiveRecord::Migration
  def change
    add_column :activities, :all_day, :boolean
  end
end
