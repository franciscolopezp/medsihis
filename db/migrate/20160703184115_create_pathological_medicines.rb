class CreatePathologicalMedicines < ActiveRecord::Migration
  def change
    create_table :pathological_medicines do |t|
      t.text :other_medicines

      t.timestamps null: false
    end
  end
end
