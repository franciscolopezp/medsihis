class CreateTemplatePrescriptions < ActiveRecord::Migration
  def change
    create_table :template_prescriptions do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
