class AddColumnsToPatient < ActiveRecord::Migration
  def change
    add_column :patients, :systolic_pressure, :integer
    add_column :patients, :diastolic_pressure, :integer
  end
end
