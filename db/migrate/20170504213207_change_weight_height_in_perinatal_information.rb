class ChangeWeightHeightInPerinatalInformation < ActiveRecord::Migration
  def change
    change_column :perinatal_informations, :weight, :float
    change_column :perinatal_informations, :height, :float
  end
end
