class Referencestemplatereportfields < ActiveRecord::Migration
  def change
    add_reference :custom_template_report_fields, :custom_template_report, index: {:name => "index_custom_report_fields"}, foreign_key: true
    add_reference :custom_template_report_fields, :fields_prescription, index: {:name => "index_custom_fields"}, foreign_key: true
  end
end
