class AddEmailToPatientFiscalInformation < ActiveRecord::Migration
  def change
    add_column :patient_fiscal_informations, :email, :string
  end
end
