class CreateOtherChargeItems < ActiveRecord::Migration
  def change
    create_table :other_charge_items do |t|
      t.decimal :service_amount, precision: 10, scale: 2
      t.decimal :service_discount, precision: 10, scale: 2
      t.decimal :total, precision: 10, scale: 2
      t.references :other_charge, index: true, foreign_key: true
      t.references :hospital_service, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
