class AddHeadCircumferenceToVitalSigns < ActiveRecord::Migration
  def change
    add_column :vital_signs, :head_circumference, :float
  end
end
