class CreateJoinTableClinicalAnalysisOrderClinicalStudy < ActiveRecord::Migration
  def change
    create_table :clinical_analysis_orders_studies,:id => false do |t|
      t.references :clinical_analysis_order, index: {:name => "clinical_analysis_order"}, foreign_key: true
      t.references :clinical_study, index: true, foreign_key: true
    end
  end
end
