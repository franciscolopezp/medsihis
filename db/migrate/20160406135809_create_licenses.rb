class CreateLicenses < ActiveRecord::Migration
  def change
    create_table :licenses do |t|
      t.date :start_date
      t.date :end_date
      t.string :status
      t.boolean :current

      t.timestamps null: false
    end
  end
end
