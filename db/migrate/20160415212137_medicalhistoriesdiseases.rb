class Medicalhistoriesdiseases < ActiveRecord::Migration
  def change
    create_table :diseases_medical_histories, :id => false do |t|
      t.integer :medical_history_id
      t.integer :disease_id
    end
  end
end
