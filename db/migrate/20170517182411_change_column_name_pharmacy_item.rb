class ChangeColumnNamePharmacyItem < ActiveRecord::Migration
  def change
    rename_column :pharmacy_items, :name, :code
    add_reference :pharmacy_items, :medicament_category, index: true, foreign_key: true
  end
end
