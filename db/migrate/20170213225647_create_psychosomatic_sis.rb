class CreatePsychosomaticSis < ActiveRecord::Migration
  def change
    create_table :psychosomatic_sis do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.text :personalidad
      t.text :ansiedad
      t.text :animo
      t.text :amnesia

      t.timestamps null: false
    end
  end
end
