class AddReferenceGenderToAssistants < ActiveRecord::Migration
  def change
    add_reference :assistants, :gender, index: true, foreign_key: true
  end
end
