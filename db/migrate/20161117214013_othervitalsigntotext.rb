class Othervitalsigntotext < ActiveRecord::Migration
  def change
    change_column :vital_signs, :other, :text
  end
end
