class CreateDailyCashes < ActiveRecord::Migration
  def change
    create_table :daily_cashes do |t|
      t.references :account, index: true, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date
      t.decimal :start_balance
      t.decimal :end_balance

      t.timestamps null: false
    end
  end
end
