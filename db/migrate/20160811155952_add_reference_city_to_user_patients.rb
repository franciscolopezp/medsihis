class AddReferenceCityToUserPatients < ActiveRecord::Migration
  def change
    add_reference :user_patients, :city, index: true, foreign_key: true
  end
end
