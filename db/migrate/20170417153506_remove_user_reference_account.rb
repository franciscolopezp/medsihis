class RemoveUserReferenceAccount < ActiveRecord::Migration
  def change
    remove_reference(:accounts, :user, index: true, foreign_key: true)
  end
end
