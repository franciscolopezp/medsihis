class Addrefecnceinvoices < ActiveRecord::Migration
  def change
    add_reference :invoices, :payment_way, index: true
  end
end
