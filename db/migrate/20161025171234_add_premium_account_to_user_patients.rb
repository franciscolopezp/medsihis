class AddPremiumAccountToUserPatients < ActiveRecord::Migration
  def change
    add_column :user_patients, :premium_account, :boolean
  end
end
