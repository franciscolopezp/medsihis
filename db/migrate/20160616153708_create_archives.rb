class CreateArchives < ActiveRecord::Migration
  def change
    create_table :archives do |t|
      t.string :file_name
      t.string :original_name
      t.references :clinical_analysis_order, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
