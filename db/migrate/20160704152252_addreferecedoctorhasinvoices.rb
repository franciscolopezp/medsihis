class Addreferecedoctorhasinvoices < ActiveRecord::Migration
  def change
    add_reference :invoices, :doctor, index: true
  end
end
