class CreateNeonateInfos < ActiveRecord::Migration
  def change
    create_table :neonate_infos do |t|
      t.string :child_name
      t.references :gender, index: true, foreign_key: true
      t.datetime :birthdate
      t.float :weight
      t.float :height
      t.integer :apgar_appearance
      t.integer :apgar_pulse
      t.integer :apgar_gesture
      t.integer :apgar_activity
      t.integer :apgar_breathing
      t.string :blood_type
      t.string :abnormalities
      t.string :observations
      t.string :attended
      t.string :pediatrician
      t.references :birth_info, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
