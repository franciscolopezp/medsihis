class CreateCustomTemplateReports < ActiveRecord::Migration
  def change
    create_table :custom_template_reports do |t|
      t.float :header_cm
      t.float :footer_cm
      t.string :orientation
      t.string :page_size
      t.references :doctor, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
