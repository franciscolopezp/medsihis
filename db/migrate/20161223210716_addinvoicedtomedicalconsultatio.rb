class Addinvoicedtomedicalconsultatio < ActiveRecord::Migration
  def change
    add_column :medical_consultations, :invoiced, :boolean
    add_reference :medical_consultations, :invoice, index: true, foreign_key: true
  end
end
