class CreateUrinaryAps < ActiveRecord::Migration
  def change
    create_table :urinary_aps do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.text :disuria
      t.text :polaquiuria
      t.text :incontinencia
      t.text :poliuria
      t.text :nicturia
      t.text :hematuria
      t.text :prostatismo
      t.text :tenesmo

      t.timestamps null: false
    end
  end
end
