class AddColumnsToNoPathologicals < ActiveRecord::Migration
  def change
    add_reference :no_pathologicals, :marital_status, index: true, foreign_key: true
    add_column :no_pathologicals, :religion, :string
    add_column :no_pathologicals, :occupation, :string
  end
end
