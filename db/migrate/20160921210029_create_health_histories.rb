class CreateHealthHistories < ActiveRecord::Migration
  def change
    create_table :health_histories do |t|
      t.string :name
      t.references :user_patient, index: true, foreign_key: true

      t.timestamps null: false
    end

    remove_reference(:expedient_requests, :user_patient, index: true, foreign_key: true)

    add_reference :expedient_requests, :health_history, index: true, foreign_key: true

  end
end
