class AddInvoicedToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions, :invoiced, :boolean
    add_column :transactions, :charged, :boolean
  end
end
