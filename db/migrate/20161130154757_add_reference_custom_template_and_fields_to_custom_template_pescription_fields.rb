class AddReferenceCustomTemplateAndFieldsToCustomTemplatePescriptionFields < ActiveRecord::Migration
  def change
    add_reference :custom_template_prescription_fields, :custom_template_prescription, index: {:name => "index_custom_prescription_fields"}, foreign_key: true
    add_reference :custom_template_prescription_fields, :fields_prescription, index: {:name => "index_custom_fields"}, foreign_key: true
  end
end
