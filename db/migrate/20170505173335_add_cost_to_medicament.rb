class AddCostToMedicament < ActiveRecord::Migration
  def change
    add_column :medicaments, :cost, :float
  end
end
