class CreateAbortions < ActiveRecord::Migration
  def change
    create_table :abortions do |t|
      t.datetime :date
      t.string :hospital
      t.string :observations
      t.references :abortion_type, index: true, foreign_key: true
      t.references :pregnancy, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
