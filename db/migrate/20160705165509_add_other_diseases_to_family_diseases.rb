class AddOtherDiseasesToFamilyDiseases < ActiveRecord::Migration
  def change
    add_column :family_diseases, :other_diseases, :text
  end
end
