class CreateAddressBooks < ActiveRecord::Migration
  def change
    create_table :address_books do |t|
      t.string :name
      t.string :telephone
      t.string :cellphone
      t.string :email
      t.string :address

      t.timestamps null: false
    end
  end
end
