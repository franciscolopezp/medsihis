class AddReferenceMovementCategoryToMovementType < ActiveRecord::Migration
  def change
    add_reference :movement_types, :movement_category, index: true, foreign_key: true
    remove_column :movement_types, :category
  end
end
