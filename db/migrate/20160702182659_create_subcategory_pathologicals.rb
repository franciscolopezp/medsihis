class CreateSubcategoryPathologicals < ActiveRecord::Migration
  def change
    create_table :subcategory_pathologicals do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
