class RemoveAllColumnFromMedicalHistories < ActiveRecord::Migration
  def change
    remove_column :medical_histories, :nonpathological, :string
    remove_column :medical_histories, :pathological, :string
    remove_column :medical_histories, :hypertension, :boolean
    remove_column :medical_histories, :other_diseases, :string
    remove_column :medical_histories, :other_medicaments, :string
    remove_column :medical_histories, :overweight, :boolean
    remove_column :medical_histories, :diabetes, :boolean
    remove_column :medical_histories, :relative, :boolean
    remove_column :medical_histories, :surgeries, :string
  end
end
