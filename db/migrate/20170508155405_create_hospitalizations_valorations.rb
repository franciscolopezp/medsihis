class CreateHospitalizationsValorations < ActiveRecord::Migration
  def change
    create_table :hospitalizations_valorations, :id => false do |t|
      t.references :hospital_admission, index: true, foreign_key: true
      t.references :patient_valoration, index: true, foreign_key: true
    end
  end
end
