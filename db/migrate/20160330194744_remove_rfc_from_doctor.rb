class RemoveRfcFromDoctor < ActiveRecord::Migration
  def change
    remove_column :doctors, :rfc, :string
  end
end
