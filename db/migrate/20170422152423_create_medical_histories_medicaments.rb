class CreateMedicalHistoriesMedicaments < ActiveRecord::Migration
  def change
    create_table :medical_histories_medicaments do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.references :medicament, index: true, foreign_key: true
    end
  end
end
