class RemoveColumnIsoFromStates < ActiveRecord::Migration
  def change
    remove_column :states, :iso, :string
  end
end
