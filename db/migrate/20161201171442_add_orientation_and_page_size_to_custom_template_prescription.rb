class AddOrientationAndPageSizeToCustomTemplatePrescription < ActiveRecord::Migration
  def change
    add_column :custom_template_prescriptions, :orientation, :string
    add_column :custom_template_prescriptions, :page_size, :string
  end
end
