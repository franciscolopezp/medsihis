class AddColumnIsCustomToLaboratory < ActiveRecord::Migration
  def change
    add_column :laboratories, :is_custom, :boolean
    add_column :clinical_studies, :price, :float
  end
end
