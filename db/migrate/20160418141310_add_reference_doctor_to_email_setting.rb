class AddReferenceDoctorToEmailSetting < ActiveRecord::Migration
  def change
    add_reference :email_settings, :doctor, index: true, foreign_key: true
  end
end
