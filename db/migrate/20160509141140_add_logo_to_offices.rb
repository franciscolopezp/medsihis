class AddLogoToOffices < ActiveRecord::Migration
  def change
    add_column :offices, :logo, :string
  end
end
