class AddActiveToOffices < ActiveRecord::Migration
  def change
    add_column :offices, :active, :boolean
  end
end
