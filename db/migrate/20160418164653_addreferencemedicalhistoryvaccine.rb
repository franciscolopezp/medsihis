class Addreferencemedicalhistoryvaccine < ActiveRecord::Migration
  def change
    add_reference :vaccinations, :medical_history, index: true, foreign_key: true
  end
end
