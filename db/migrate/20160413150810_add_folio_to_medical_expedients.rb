class AddFolioToMedicalExpedients < ActiveRecord::Migration
  def change
    add_column :medical_expedients, :folio, :string
  end
end
