class AddReferencesDoctorToPersonalsupports < ActiveRecord::Migration
  def change
    add_reference :personalsupports, :doctor, index: true, foreign_key: true
  end
end
