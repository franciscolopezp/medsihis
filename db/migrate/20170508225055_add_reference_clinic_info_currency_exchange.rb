class AddReferenceClinicInfoCurrencyExchange < ActiveRecord::Migration
  def change
    add_reference :currency_exchanges, :clinic_info, index: true, foreign_key: true
  end
end
