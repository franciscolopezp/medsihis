class ChangeColumnOtherDiseasesAndAddNewColumnToFamilyDiseases < ActiveRecord::Migration
  def change
    rename_column :family_diseases, :other_diseases, :annotations
    add_column :family_diseases, :other_diseases, :boolean
  end
end
