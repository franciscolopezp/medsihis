class AddReferenceAssormentToPharmacy < ActiveRecord::Migration
  def change
    add_reference :assortment_pharmacy_items, :medicament_assortment, index: true, foreign_key: true
  end
end
