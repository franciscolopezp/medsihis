class CreateTableGeneralConsultationTypeUsers < ActiveRecord::Migration
  def change
    create_table :general_consultation_types_users, id:false do |t|
      t.belongs_to :general_consultation_type
      t.belongs_to :user
    end

  end
end
