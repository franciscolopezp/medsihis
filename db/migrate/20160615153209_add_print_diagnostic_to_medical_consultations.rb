class AddPrintDiagnosticToMedicalConsultations < ActiveRecord::Migration
  def change
    add_column :medical_consultations, :print_diagnostic, :boolean
  end
end
