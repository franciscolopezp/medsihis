class ChangeColumnTypeDataCertificatePasswordFromFiscalInformation < ActiveRecord::Migration
  def change
    change_column :fiscal_informations, :certificate_password, :text
  end
end
