class AddSerieToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :serie, :string
    add_column :invoices, :folio, :integer
  end
end
