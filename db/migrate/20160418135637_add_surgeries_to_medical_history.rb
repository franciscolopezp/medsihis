class AddSurgeriesToMedicalHistory < ActiveRecord::Migration
  def change
    add_column :medical_histories, :surgeries, :string
  end
end
