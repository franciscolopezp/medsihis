class MedicalConsultationHasDisease < ActiveRecord::Migration
  def change
    create_table :diseases_medical_consultations, id:false do |t|
      t.belongs_to :medical_consultation, index: true
      t.belongs_to :disease, index: true
    end
    add_foreign_key :diseases_medical_consultations, :medical_consultations
    add_foreign_key :diseases_medical_consultations, :diseases
  end
end
