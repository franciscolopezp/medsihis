class CreateClinicalAnalysisOrders < ActiveRecord::Migration
  def change
    create_table :clinical_analysis_orders do |t|
      t.integer :folio
      t.string :name_analysis
      t.date :data_order
      t.text :annotations
      t.references :medical_expedient, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
