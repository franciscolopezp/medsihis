class AddClinicalStudyTypeToClinicalStudies < ActiveRecord::Migration
  def change
    add_reference :clinical_studies, :clinical_study_type, index: true, foreign_key: true
  end
end
