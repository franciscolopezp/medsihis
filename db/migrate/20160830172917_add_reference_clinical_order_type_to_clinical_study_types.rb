class AddReferenceClinicalOrderTypeToClinicalStudyTypes < ActiveRecord::Migration
  def change
    add_reference :clinical_study_types, :clinical_order_type, index: true, foreign_key: true
  end
end
