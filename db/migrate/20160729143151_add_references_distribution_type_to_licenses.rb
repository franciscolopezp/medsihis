class AddReferencesDistributionTypeToLicenses < ActiveRecord::Migration
  def change
    add_reference :licenses, :distribution_type, index: true, foreign_key: true
  end
end
