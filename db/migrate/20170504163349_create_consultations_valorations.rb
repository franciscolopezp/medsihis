class CreateConsultationsValorations < ActiveRecord::Migration
  def change
    create_table :consultations_valorations, :id => false do |t|
      t.references :medical_consultation, index: true, foreign_key: true
      t.references :patient_valoration, index: true, foreign_key: true
    end
  end
end
