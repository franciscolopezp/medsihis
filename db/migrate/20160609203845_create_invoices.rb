class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :xml_base
      t.text :original_string

      t.timestamps null: false
    end
  end
end
