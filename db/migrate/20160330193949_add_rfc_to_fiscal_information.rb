class AddRfcToFiscalInformation < ActiveRecord::Migration
  def change
    add_column :fiscal_informations, :rfc, :string
  end
end
