class AddReferenceCityToDoctors < ActiveRecord::Migration
  def change
    add_reference :doctors, :city, index: true, foreign_key: true
  end
end
