class CreateUroExplorations < ActiveRecord::Migration
  def change
    create_table :uro_explorations do |t|
      t.references :medical_consultation, index: true, foreign_key: true
      t.boolean :lymph_nodes
      t.boolean :abdomen
      t.boolean :bladder
      t.boolean :lumbar_region
      t.boolean :scrotum_testicles
      t.boolean :hernias
      t.boolean :epididymis_vessels
      t.boolean :penis
      t.boolean :injuries
      t.boolean :prostate
      t.boolean :seminal_vesicles
      t.text :notes

      t.timestamps null: false
    end
  end
end
