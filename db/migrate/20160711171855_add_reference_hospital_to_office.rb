class AddReferenceHospitalToOffice < ActiveRecord::Migration
  def change
    add_reference :offices, :hospital, index: true, foreign_key: true
    add_column :offices, :lat, :string
    add_column :offices, :lng, :string
  end
end
