class CreateCardiovascularAps < ActiveRecord::Migration
  def change
    create_table :cardiovascular_aps do |t|
      t.string :chest_pain
      t.string :edema
      t.string :dyspnoea
      t.string :paroxysmal_nocturnal_dyspnea
      t.string :orthopnea
      t.string :palpitations
      t.string :syncope_presyncope
      t.string :peripheral_vascular
      t.references :medical_history, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
