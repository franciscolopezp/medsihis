class AddMedicalHistoryToNoPthologicals < ActiveRecord::Migration
  def change
    add_reference :no_pathologicals, :medical_history, index: true, foreign_key: true
  end
end
