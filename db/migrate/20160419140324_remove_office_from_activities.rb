class RemoveOfficeFromActivities < ActiveRecord::Migration
  def change
    remove_reference :activities, :office, index: true, foreign_key: true
  end
end
