class AllergiesPatients < ActiveRecord::Migration
  def change
    create_table :allergies_patients, :id => false do |t|
      t.integer :allergy_id
      t.integer :patient_id
    end
  end
end
