class CreateRankImcAdults < ActiveRecord::Migration
  def change
    create_table :rank_imc_adults do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
