class AddReferenceWasteType < ActiveRecord::Migration
  def change
    add_reference :medicament_wastes, :waste_type, index: true, foreign_key: true
    add_reference :pharmacy_item_wastes, :waste_type, index: true, foreign_key: true
  end
end
