class AddExtraToCustomRemplatePrescriptionFields < ActiveRecord::Migration
  def change
    add_column :custom_template_prescription_fields, :extra, :string
  end
end
