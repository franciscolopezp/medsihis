class AddColumnsBloodPressureToVitalSigns < ActiveRecord::Migration
  def change
    add_column :vital_signs, :systolic_pressure, :integer
    add_column :vital_signs, :diastolic_pressure, :integer
  end
end
