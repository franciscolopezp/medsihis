class CreateOfficeTimes < ActiveRecord::Migration
  def change
    create_table :office_times do |t|
      t.string :start
      t.string :end
      t.integer :day

      t.timestamps null: false
    end
  end
end
