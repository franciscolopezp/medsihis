class RemoveRefereceAddReferenceOffice < ActiveRecord::Migration
  def change
    add_reference :offices, :user, index: true, foreign_key: true
    remove_reference(:offices, :doctor, index: true, foreign_key: true)
  end
end
