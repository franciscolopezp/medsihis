class Addreferenceemisorfiscalinfotoclinic < ActiveRecord::Migration
  def change
    add_reference :emisor_fiscal_informations, :clinic_info, index: true, foreign_key: true
  end
end
