class ChangeColumnNamesFamilyDiseases < ActiveRecord::Migration
  def change
    rename_column :family_diseases, :has_asthma, :asthma
    rename_column :family_diseases, :has_cancer, :cancer
    rename_column :family_diseases, :has_diabetes, :diabetes
    rename_column :family_diseases, :has_hypertension, :hypertension
    rename_column :family_diseases, :has_overweight, :overweight
  end
end
