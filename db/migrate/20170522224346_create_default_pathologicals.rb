class CreateDefaultPathologicals < ActiveRecord::Migration
  def change
    create_table :default_pathologicals do |t|
      t.boolean :diabetes
      t.text :diabetes_description
      t.boolean :overweight
      t.text :overweight_description
      t.boolean :hypertension
      t.text :hypertension_description
      t.boolean :asthma
      t.text :asthma_description
      t.boolean :cancer
      t.text :cancer_description
      t.text :annotations
      t.boolean :other_diseases
      t.boolean :psychiatric_diaseases
      t.string :psychiatric_diaseases_description
      t.boolean :neurological_diseases
      t.string :neurological_diseases_description
      t.boolean :cardiovascular_diseases
      t.string :cardiovascular_diseases_description
      t.boolean :bronchopulmonary_diseases
      t.string :bronchopulmonary_diseases_description
      t.boolean :thyroid_diseases
      t.string :thyroid_diseases_description
      t.boolean :kidney_diseases
      t.string :kidney_diseases_description

      t.timestamps null: false
    end
  end
end
