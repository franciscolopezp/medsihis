class ChagentransmiterSeal < ActiveRecord::Migration
  def change
    change_column :admission_invoices, :transmitter_seal, :text
  end
end
