class AddColumnsToVitalSigns < ActiveRecord::Migration
  def change
    add_column :vital_signs, :eco_rctg, :string
    add_column :vital_signs, :fu, :string
    add_column :vital_signs, :pres, :string
    add_column :vital_signs, :ff, :string
    add_column :vital_signs, :vdrl, :string
    add_column :vital_signs, :ego, :string
    add_column :vital_signs, :other, :string
  end
end
