class Changefoliotypetoint < ActiveRecord::Migration
  def change
    change_column :fiscal_informations, :folio, :integer
  end
end
