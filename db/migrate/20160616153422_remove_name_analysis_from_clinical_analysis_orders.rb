class RemoveNameAnalysisFromClinicalAnalysisOrders < ActiveRecord::Migration
  def change
    remove_column :clinical_analysis_orders, :name_analysis, :string
  end
end
