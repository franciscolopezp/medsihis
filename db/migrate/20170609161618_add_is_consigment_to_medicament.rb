class AddIsConsigmentToMedicament < ActiveRecord::Migration
  def change
    add_column :medicaments, :is_consigment, :boolean
    add_column :pharmacy_items, :is_consigment, :boolean
  end
end
