class Vaccinationnewreferences < ActiveRecord::Migration
  def change
    add_reference :vaccinations, :patient, index: true, foreign_key: true
    add_reference :vaccinations, :application_age, index: true, foreign_key: true
  end
end
