class CreateSurgeries < ActiveRecord::Migration
  def change
    create_table :surgeries do |t|
      t.text :description

      t.timestamps null: false
    end
  end
end
