class CreatePathologicalConditions < ActiveRecord::Migration
  def change
    create_table :pathological_conditions do |t|
      t.text :description

      t.timestamps null: false
    end
  end
end
