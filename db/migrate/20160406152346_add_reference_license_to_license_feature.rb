class AddReferenceLicenseToLicenseFeature < ActiveRecord::Migration
  def change
    add_reference :license_features, :license, index: true, foreign_key: true
  end
end
