class CreateCertificates < ActiveRecord::Migration
  def change
    create_table :certificates do |t|
      t.string :name
      t.text :container_html
      t.references :office, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true
      t.references :type_certificate, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
