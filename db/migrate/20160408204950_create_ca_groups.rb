class CreateCaGroups < ActiveRecord::Migration
  def change
    create_table :ca_groups do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
