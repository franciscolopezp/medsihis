class RemoveColumnBooleanAndChangeTypeColumn < ActiveRecord::Migration
  def change
    remove_column :custom_template_prescription_fields, :boolean
    change_column :custom_template_prescription_fields, :is_bold, :boolean
  end
end
