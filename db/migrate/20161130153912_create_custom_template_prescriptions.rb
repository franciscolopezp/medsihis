class CreateCustomTemplatePrescriptions < ActiveRecord::Migration
  def change
    create_table :custom_template_prescriptions do |t|
      t.float :header_cm
      t.integer :footer_cm
      t.references :doctor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
