class AddMedicamentsToMedicalHistory < ActiveRecord::Migration
  def change
    add_column :medical_histories, :other_medicaments, :string
  end
end
