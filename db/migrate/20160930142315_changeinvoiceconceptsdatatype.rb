class Changeinvoiceconceptsdatatype < ActiveRecord::Migration
  def change
    change_column :invoice_concepts, :unit_price, :decimal, :precision => 11, :scale => 2
    change_column :invoice_concepts, :cost, :decimal, :precision => 11, :scale => 2
    change_column :invoices, :isr, :decimal, :precision => 11, :scale => 2
  end
end
