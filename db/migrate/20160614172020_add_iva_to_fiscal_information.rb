class AddIvaToFiscalInformation < ActiveRecord::Migration
  def change
    add_column :fiscal_informations, :iva, :float
    add_column :fiscal_informations, :isr, :float
  end
end
