class AddAcceptTermConditionsToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :accept_term_conditions, :boolean
  end
end
