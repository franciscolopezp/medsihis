class AddReferenceCityToFiscalInformation < ActiveRecord::Migration
  def change
    add_reference :fiscal_informations, :city, index: true, foreign_key: true
  end
end
