class CreateClinicInfos < ActiveRecord::Migration
  def change
    create_table :clinic_infos do |t|
      t.string :name
      t.string :address
      t.string :telephone
      t.string :fax
      t.string :logo
      t.references :city, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
