class AddRequireUploadToMedicalAnnotation < ActiveRecord::Migration
  def change
    add_column :medical_annotation_types, :add_valoration, :boolean, :after => :template
    add_column :medical_annotation_types, :require_upload, :boolean, :after => :template
  end
end
