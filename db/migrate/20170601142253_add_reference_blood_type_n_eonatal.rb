class AddReferenceBloodTypeNEonatal < ActiveRecord::Migration
  def change
    remove_column :neonate_infos, :blood_type
    add_reference :neonate_infos, :blood_type, index: true, foreign_key: true
  end
end
