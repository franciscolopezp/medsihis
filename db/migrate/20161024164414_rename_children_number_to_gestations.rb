class RenameChildrenNumberToGestations < ActiveRecord::Migration
  def change
    rename_column :gynecologist_obstetrical_backgrounds, :children_number, :gestations
  end
end
