class CreateCategoryImcs < ActiveRecord::Migration
  def change
    create_table :category_imcs do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
