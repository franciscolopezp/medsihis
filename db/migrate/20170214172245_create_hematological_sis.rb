class CreateHematologicalSis < ActiveRecord::Migration
  def change
    create_table :hematological_sis do |t|
      t.text :astenia
      t.text :palidez
      t.text :hemorrhages
      t.text :adenopathies
      t.references :medical_history, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
