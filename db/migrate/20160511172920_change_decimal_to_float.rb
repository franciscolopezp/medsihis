class ChangeDecimalToFloat < ActiveRecord::Migration
  def change
    change_column :accounts, :balance, :float
    change_column :transactions, :amount, :float
    change_column :transactions, :init_balance, :float
    change_column :transactions, :end_balance, :float
    change_column :daily_cashes, :start_balance, :float
    change_column :daily_cashes, :end_balance, :float
  end
end
