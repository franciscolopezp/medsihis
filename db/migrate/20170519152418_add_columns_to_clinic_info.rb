class AddColumnsToClinicInfo < ActiveRecord::Migration
  def change
    add_column :clinic_infos, :set_custom_ticket_description, :boolean
    add_column :clinic_infos, :ticket_description, :string
  end
end
