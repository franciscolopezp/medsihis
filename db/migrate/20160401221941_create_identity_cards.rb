class CreateIdentityCards < ActiveRecord::Migration
  def change
    create_table :identity_cards do |t|
      t.string :identity

      t.timestamps null: false
    end
  end
end
