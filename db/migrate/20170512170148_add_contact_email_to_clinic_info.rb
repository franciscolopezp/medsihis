class AddContactEmailToClinicInfo < ActiveRecord::Migration
  def change
    add_column :clinic_infos, :contact_email, :string
  end
end
