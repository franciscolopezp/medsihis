class CreatePatientValorations < ActiveRecord::Migration
  def change
    create_table :patient_valorations do |t|
      t.references :valoration_type, index: true, foreign_key: true
      t.references :patient, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
