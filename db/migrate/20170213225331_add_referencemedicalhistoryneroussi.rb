class AddReferencemedicalhistoryneroussi < ActiveRecord::Migration
  def change
    add_reference :nervous_sis, :medical_history, index: true, foreign_key: true
  end
end
