class Addmigrationinvoicetransac < ActiveRecord::Migration
  def change
    add_reference :transactions, :invoice, index: true
  end
end
