class CreateVaccines < ActiveRecord::Migration
  def change
    create_table :vaccines do |t|
      t.string :name
      t.string :prevent_disease
      t.string :dose
      t.string :age_or_frecuency

      t.timestamps null: false
    end
  end
end
