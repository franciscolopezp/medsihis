class RemoveColumnGenderInDoctors < ActiveRecord::Migration
  def change
    remove_column :doctors, :gender, :string
  end
end
