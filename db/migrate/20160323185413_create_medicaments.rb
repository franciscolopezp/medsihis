class CreateMedicaments < ActiveRecord::Migration
  def change
    create_table :medicaments do |t|
      t.string :comercial_name
      t.string :presentation
      t.string :active_substance
      t.string :contraindication
      t.string :brand

      t.timestamps null: false
    end
  end
end
