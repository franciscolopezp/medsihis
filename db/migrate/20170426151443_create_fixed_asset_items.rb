class CreateFixedAssetItems < ActiveRecord::Migration
  def change
    create_table :fixed_asset_items do |t|
      t.string :name
      t.string :description
      t.string :identifier
      t.boolean :state
      t.boolean :schedule
      t.references :fixed_asset, index: true, foreign_key: true
      t.references :fixed_asset_activity, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
