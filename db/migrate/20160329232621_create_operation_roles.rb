class CreateOperationRoles < ActiveRecord::Migration
  def change
    create_table :operation_roles do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
