class AddCanCloseChargeToUser < ActiveRecord::Migration
  def change
    add_column :users, :can_close_charge, :boolean
  end
end
