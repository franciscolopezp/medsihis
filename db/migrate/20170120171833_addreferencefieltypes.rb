class Addreferencefieltypes < ActiveRecord::Migration
  def change
    add_reference :fields_prescriptions, :field_prescription_type, index: true, foreign_key: true
  end
end
