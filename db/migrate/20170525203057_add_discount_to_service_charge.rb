class AddDiscountToServiceCharge < ActiveRecord::Migration
  def change
    add_column :service_charges, :discount, :decimal, precision: 10, scale: 2
  end
end
