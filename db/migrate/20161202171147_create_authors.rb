class CreateAuthors < ActiveRecord::Migration
  def change
    create_table :authors do |t|
      t.string :name
      t.string :last_name
      t.string :email
      t.string :profile_image
      t.string :twitter
      t.string :company_title
      t.string :job_title

      t.timestamps null: false
    end
  end
end
