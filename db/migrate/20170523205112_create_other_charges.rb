class CreateOtherCharges < ActiveRecord::Migration
  def change
    create_table :other_charges do |t|
      t.string :patient_name
      t.decimal :total_amount, precision: 10, scale: 2
      t.decimal :total_discount, precision: 10, scale: 2
      t.references :user, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
