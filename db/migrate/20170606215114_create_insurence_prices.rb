class CreateInsurencePrices < ActiveRecord::Migration
  def change
    create_table :insurence_prices do |t|
      t.decimal :price, precision: 10, scale: 2
      t.references :hospital_service, index: true, foreign_key: true
      t.references :insurance, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
