class ChangeColumnIsAllergicMedicationsInPathologicalAllergies < ActiveRecord::Migration
  def change
    rename_column :pathological_allergies, :allergy_any_medications, :allergy_medications
  end
end
