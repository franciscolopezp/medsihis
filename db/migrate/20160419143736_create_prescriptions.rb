class CreatePrescriptions < ActiveRecord::Migration
  def change
    create_table :prescriptions do |t|
      t.text :recommendations
      t.references :medical_consultation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
