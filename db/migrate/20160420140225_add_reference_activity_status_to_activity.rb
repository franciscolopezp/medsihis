class AddReferenceActivityStatusToActivity < ActiveRecord::Migration
  def change
    add_reference :activities, :activity_status, index: true, foreign_key: true
  end
end
