class RemoveColumnEmailInPatients < ActiveRecord::Migration
  def change
    remove_column :user_patients, :email, :string
  end
end
