class CreateDcBalances < ActiveRecord::Migration
  def change
    create_table :dc_balances do |t|
      t.references :daily_cash, index: true, foreign_key: true
      t.references :currency, index: true, foreign_key: true
      t.float :start
      t.float :end

      t.timestamps null: false
    end
  end
end
