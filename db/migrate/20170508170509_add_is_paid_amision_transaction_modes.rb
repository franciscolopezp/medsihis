class AddIsPaidAmisionTransactionModes < ActiveRecord::Migration
  def change
    add_column :medicament_assortments, :is_paid, :boolean
    add_column :service_charges, :is_paid, :boolean
  end
end
