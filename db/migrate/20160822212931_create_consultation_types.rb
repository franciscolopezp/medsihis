class CreateConsultationTypes < ActiveRecord::Migration
  def change
    create_table :consultation_types do |t|
      t.string :name
      t.references :doctor, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
