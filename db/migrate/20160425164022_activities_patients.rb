class ActivitiesPatients < ActiveRecord::Migration
  def change
    create_table :activities_patients, id:false do |t|
      t.belongs_to :activity, index: true
      t.belongs_to :patient, index: true
    end
    add_foreign_key :activities_patients, :activities
    add_foreign_key :activities_patients, :patients
  end
end
