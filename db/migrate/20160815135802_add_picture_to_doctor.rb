class AddPictureToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :picture, :text
  end
end
