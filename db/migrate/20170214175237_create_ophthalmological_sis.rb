class CreateOphthalmologicalSis < ActiveRecord::Migration
  def change
    create_table :ophthalmological_sis do |t|
      t.text :agudeza
      t.text :diplopia
      t.text :eye_pain
      t.text :photofobia
      t.text :amaurosis
      t.text :fotopsias
      t.text :miodesopsias
      t.text :picor
      t.text :pitanas
      t.references :medical_history, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
