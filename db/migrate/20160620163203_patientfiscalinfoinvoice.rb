class Patientfiscalinfoinvoice < ActiveRecord::Migration
  def change
    add_reference :invoices, :patient_fiscal_information, index: true
  end
end
