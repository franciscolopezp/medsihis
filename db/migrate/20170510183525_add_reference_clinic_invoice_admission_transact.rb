class AddReferenceClinicInvoiceAdmissionTransact < ActiveRecord::Migration
  def change
    add_reference :admission_transactions, :clinic_invoice, index: true, foreign_key: true
  end
end
