class CreatePathologicals < ActiveRecord::Migration
  def change
    create_table :pathologicals do |t|
      t.references :section, polymorphic: true, index: true
      t.references :subcategory_pathological, index: true, foreign_key: true
      t.integer :order

      t.timestamps null: false
    end
  end
end
