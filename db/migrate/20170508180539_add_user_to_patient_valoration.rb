class AddUserToPatientValoration < ActiveRecord::Migration
  def change
    add_reference :patient_valorations,  :user, :after => :id, index: true, foreign_key: true
  end
end
