class AddIsChargedToCabinetReport < ActiveRecord::Migration
  def change
    add_column :cabinet_reports, :is_charged, :boolean
    add_column :cabinet_reports, :price, :float
  end
end
