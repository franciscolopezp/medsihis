class CreateVaccinations < ActiveRecord::Migration
  def change
    create_table :vaccinations do |t|
      t.date :vaccinate_date

      t.timestamps null: false
    end
  end
end
