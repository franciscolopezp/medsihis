class AddReferenceLaboratoryToClinicalAnalysisOrder < ActiveRecord::Migration
  def change
    add_reference :clinical_analysis_orders, :laboratory, index: true, foreign_key: true
  end
end
