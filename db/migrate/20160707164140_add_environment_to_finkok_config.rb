class AddEnvironmentToFinkokConfig < ActiveRecord::Migration
  def change
    add_column :finkok_configs, :environment, :string
  end
end
