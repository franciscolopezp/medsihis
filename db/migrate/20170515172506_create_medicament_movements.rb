class CreateMedicamentMovements < ActiveRecord::Migration
  def change
    create_table :medicament_movements do |t|
      t.string :from_warehouse
      t.string :to_warehouse
      t.integer :quantity
      t.references :user, index:true, foreign_key:true
      t.references :lot, index:true, foreign_key:true
      t.timestamps null: false
    end
  end
end
