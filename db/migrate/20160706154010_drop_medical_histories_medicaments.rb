class DropMedicalHistoriesMedicaments < ActiveRecord::Migration
  def change
    drop_table :medical_histories_medicaments
  end
end
