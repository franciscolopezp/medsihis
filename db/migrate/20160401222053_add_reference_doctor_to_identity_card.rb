class AddReferenceDoctorToIdentityCard < ActiveRecord::Migration
  def change
    add_reference :identity_cards, :doctor, index: true, foreign_key: true
  end
end
