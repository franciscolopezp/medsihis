class ChangeMedicalAnnotationField < ActiveRecord::Migration
  def change
    remove_column :medical_annotation_fields, :f_key
    remove_column :medical_annotation_fields, :f_html_type
    remove_column :medical_annotation_fields, :label
    add_column :medical_annotation_fields, :config, :text, :after => :id
  end
end
