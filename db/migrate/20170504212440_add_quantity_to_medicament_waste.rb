class AddQuantityToMedicamentWaste < ActiveRecord::Migration
  def change
    add_column :medicament_wastes, :quantity, :integer
  end
end
