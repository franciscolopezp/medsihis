class DailyCashesTransactions < ActiveRecord::Migration
  def change
    create_table :daily_cashes_transactions, id:false do |t|
      t.belongs_to :daily_cash, index: true
      t.belongs_to :transaction, index: true
    end
    add_foreign_key :daily_cashes_transactions, :daily_cashes
    add_foreign_key :daily_cashes_transactions, :transactions
  end
end
