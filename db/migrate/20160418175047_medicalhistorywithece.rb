class Medicalhistorywithece < ActiveRecord::Migration
  def change
    add_reference :medical_histories, :medical_expedient, index: true, foreign_key: true
  end
end
