class AddReferencevaccineToVaccineBooks < ActiveRecord::Migration
  def change
    add_reference :vaccines, :vaccine_book, index: true
  end
end
