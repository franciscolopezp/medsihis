class CreateRespiratoryAps < ActiveRecord::Migration
  def change
    create_table :respiratory_aps do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.string :cough
      t.string :chest_pain
      t.string :dyspnoea
      t.string :hemoptisis
      t.string :epistaxis
      t.string :cyanosis

      t.timestamps null: false
    end
  end
end
