class AddReferenceoperationroleToPersonalsupports < ActiveRecord::Migration
  def change
    add_reference :personalsupports, :operation_role, index: true, foreign_key: true
  end
end
