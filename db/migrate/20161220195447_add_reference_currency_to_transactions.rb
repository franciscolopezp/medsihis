class AddReferenceCurrencyToTransactions < ActiveRecord::Migration
  def change
    add_reference :transactions, :currency, index: true, foreign_key: true
    add_column :transactions, :currency_exchange, :float
    add_column :medical_consultations, :is_charged, :boolean
  end
end
