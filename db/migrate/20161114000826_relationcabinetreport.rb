class Relationcabinetreport < ActiveRecord::Migration
  def change
    add_reference :cabinet_reports, :clinical_analysis_order, index: true, foreign_key: true
    add_reference :cabinet_report_pictures, :cabinet_report, index: true, foreign_key: true
  end
end
