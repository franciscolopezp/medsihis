class CreateTableUserWarehoses < ActiveRecord::Migration
  def change
    create_table :users_warehouses, id:false do |t|
      t.belongs_to :warehouse, index: true
      t.belongs_to :user, index: true
    end
    add_foreign_key :users_warehouses, :warehouses
    add_foreign_key :users_warehouses, :users
  end
end
