class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :name
      t.string :last_name
      t.string :identity_card
      t.string :curp
      t.string :digital_sign
      t.decimal :consult_cost
      t.string :rfc
      t.string :gender
      t.date :birth_date
      t.string :telephone
      t.string :cellphone
      t.string :address
      t.string :other_location

      t.timestamps null: false
    end
  end
end
