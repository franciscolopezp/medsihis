class CreateMedicamentPurchaseItems < ActiveRecord::Migration
  def change
    create_table :medicament_purchase_items do |t|
      t.integer :quantity
      t.float :cost
      t.references :lot, index: true, foreign_key: true
      t.references :medicament, index: true, foreign_key: true
      t.references :medicament_purchase, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
