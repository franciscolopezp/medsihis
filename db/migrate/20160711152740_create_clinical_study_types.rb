class CreateClinicalStudyTypes < ActiveRecord::Migration
  def change
    create_table :clinical_study_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
