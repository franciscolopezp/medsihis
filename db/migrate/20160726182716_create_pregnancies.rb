class CreatePregnancies < ActiveRecord::Migration
  def change
    create_table :pregnancies do |t|
      t.references :medical_expedient, index: true, foreign_key: true
      t.boolean :active
      t.date :last_menstruation_date
      t.date :probable_birth_date
      t.float :initial_weight
      t.string :baby_name
      t.boolean :pregnancy_end_success
      t.date :pregnancy_end_date


      t.timestamps null: false
    end
  end
end
