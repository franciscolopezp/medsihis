class CreateDefaultNoPathologicals < ActiveRecord::Migration
  def change
    create_table :default_no_pathologicals do |t|
      t.boolean :smoking
      t.text :smoking_description
      t.boolean :alcoholism
      t.text :alcoholism_description
      t.boolean :play_sport
      t.text :play_sport_description
      t.text :annotations
      t.string :religion
      t.string :scolarship
      t.boolean :drugs
      t.text :drugs_description
      t.references :marital_status, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
