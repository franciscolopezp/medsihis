class AddAssociationIdToImcAdults < ActiveRecord::Migration
  def change
    add_reference :imc_adults, :association, index: true
  end
end
