class AddIsCancelToOtherChargeTransaction < ActiveRecord::Migration
  def change
    add_column :other_charge_transactions, :is_cancel, :boolean
  end
end
