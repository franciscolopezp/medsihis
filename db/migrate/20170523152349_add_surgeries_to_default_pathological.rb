class AddSurgeriesToDefaultPathological < ActiveRecord::Migration
  def change
    add_column :default_pathologicals, :surgeries, :text
    add_column :default_pathologicals, :transfusions, :text
    add_column :default_pathologicals, :allergy, :boolean
    add_column :default_pathologicals, :allergy_description, :text
    add_column :default_pathologicals, :recent_diseases, :text
    add_column :default_pathologicals, :other_allergies, :text
    add_column :default_pathologicals, :other_medicaments, :text
  end
end
