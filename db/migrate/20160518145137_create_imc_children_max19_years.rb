class CreateImcChildrenMax19Years < ActiveRecord::Migration
  def change
    create_table :imc_children_max19_years do |t|
      t.integer :age_month
      t.float :danger_malnutrition
      t.float :normal
      t.float :overweight
      t.float :obesity
      t.string :unit_weight
      t.references :gender, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
