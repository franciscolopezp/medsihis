class AddReferenceOfficeToOfficeTimes < ActiveRecord::Migration
  def change
    add_reference :office_times, :office, index: true, foreign_key: true
  end
end
