class AddIsClosedToInventory < ActiveRecord::Migration
  def change
    add_column :inventories, :is_closed, :boolean
  end
end
