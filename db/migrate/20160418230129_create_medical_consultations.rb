class CreateMedicalConsultations < ActiveRecord::Migration
  def change
    create_table :medical_consultations do |t|
      t.text :current_condition
      t.datetime :date_consultation
      t.text :diagnostic

      t.timestamps null: false
    end
  end
end
