class CreateStudyReportTemplates < ActiveRecord::Migration
  def change
    create_table :study_report_templates do |t|
      t.references :clinical_study, index: true, foreign_key: true
      t.text :template

      t.timestamps null: false
    end
  end
end
