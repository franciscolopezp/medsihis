class ChangeFolioToIntExpedients < ActiveRecord::Migration
  def change
    change_column :medical_expedients, :folio, :integer
  end
end
