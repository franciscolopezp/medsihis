class Revrefvaccinations < ActiveRecord::Migration
  def change
    remove_reference(:vaccinations, :vaccine, index: true, foreign_key: true)
    remove_reference(:vaccinations, :medical_history, index: true, foreign_key: true)
  end
end
