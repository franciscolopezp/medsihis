class Medicalhistoriesmedicaments < ActiveRecord::Migration
  def change
    create_table :medical_histories_medicaments, :id => false do |t|
      t.integer :medical_history_id
      t.integer :medicament_id
    end
  end
end
