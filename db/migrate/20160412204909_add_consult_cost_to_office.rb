class AddConsultCostToOffice < ActiveRecord::Migration
  def change
    add_column :offices, :consult_cost, :string
    add_column :offices, :consult_duration, :string
  end
end
