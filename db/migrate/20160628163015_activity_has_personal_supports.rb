class ActivityHasPersonalSupports < ActiveRecord::Migration
  def change
    create_table :activities_personalsupports, id:false do |t|
      t.belongs_to :activity, index: true
      t.belongs_to :personalsupport, index: true
    end
    add_foreign_key :activities_personalsupports, :activities
    add_foreign_key :activities_personalsupports, :personalsupports
  end
end
