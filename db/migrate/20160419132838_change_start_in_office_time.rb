class ChangeStartInOfficeTime < ActiveRecord::Migration
  def change
    change_column :office_times, :start, :time
    change_column :office_times, :end, :time
  end
end
