class PatientesVaccinesbooks < ActiveRecord::Migration
  def change
    create_table :patients_vaccine_books, :id => false do |t|
      t.integer :patient_id
      t.integer :vaccine_book_id
    end
  end
end
