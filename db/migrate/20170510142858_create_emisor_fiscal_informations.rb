class CreateEmisorFiscalInformations < ActiveRecord::Migration
  def change
    create_table :emisor_fiscal_informations do |t|
      t.string :bussiness_name
      t.string :fiscal_address
      t.string :ext_number
      t.string :certificate_stamp
      t.string :certificate_key
      t.string :certificate_password
      t.string :tax_regime
      t.references :city, index: true, foreign_key: true
      t.string :rfc
      t.string :zip
      t.string :int_number
      t.string :locality
      t.string :suburb
      t.string :serie
      t.integer :folio
      t.string :iva
      t.string :isr
      t.string :certificate_number
      t.text :certificate_base64
      t.timestamps null: false
    end
  end
end
