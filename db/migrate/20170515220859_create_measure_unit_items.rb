class CreateMeasureUnitItems < ActiveRecord::Migration
  def change
    create_table :measure_unit_items do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
