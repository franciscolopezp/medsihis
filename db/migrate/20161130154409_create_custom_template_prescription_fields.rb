class CreateCustomTemplatePrescriptionFields < ActiveRecord::Migration
  def change
    create_table :custom_template_prescription_fields do |t|
      t.string :font
      t.string :is_bold
      t.string :boolean
      t.integer :size
      t.float :position_top_cm
      t.float :position_left_cm
      t.boolean :show_field

      t.timestamps null: false
    end
  end
end
