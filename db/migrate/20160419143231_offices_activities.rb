class OfficesActivities < ActiveRecord::Migration
  def change
    create_table :activities_offices, id:false do |t|
      t.belongs_to :office, index: true
      t.belongs_to :activity, index: true
    end
    add_foreign_key :activities_offices, :offices
    add_foreign_key :activities_offices, :activities
  end
end
