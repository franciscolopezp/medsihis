class AddReferenceConsultationTypeToMedicalConsultations < ActiveRecord::Migration
  def change
    add_reference :medical_consultations, :consultation_type, index: true, foreign_key: true
  end
end
