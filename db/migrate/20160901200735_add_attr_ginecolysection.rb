class AddAttrGinecolysection < ActiveRecord::Migration
  def change
    add_column :gynecologist_obstetrical_backgrounds, :information_spouse, :string
    add_column :gynecologist_obstetrical_backgrounds, :menses_last_date, :datetime
    add_column :gynecologist_obstetrical_backgrounds, :papanicolau_last_date, :datetime
    add_column :gynecologist_obstetrical_backgrounds, :cycles, :string
  end
end
