class AddReferencedoctorToDiseases < ActiveRecord::Migration
  def change
    add_reference :diseases, :doctor, index: true, foreign_key: true
  end
end
