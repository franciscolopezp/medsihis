class AddReferenceDoctorToOffices < ActiveRecord::Migration
  def change
    add_reference :offices, :doctor, index: true, foreign_key: true
  end
end
