class AddReferenceUserToCabinetReport < ActiveRecord::Migration
  def change
    add_reference :cabinet_reports, :user, index: true, foreign_key: true
  end
end
