class AddReferenceDoctorToActivity < ActiveRecord::Migration
  def change
    add_reference :activities, :doctor, index: true, foreign_key: true
    add_reference :activities, :office, index: true, foreign_key: true
  end
end
