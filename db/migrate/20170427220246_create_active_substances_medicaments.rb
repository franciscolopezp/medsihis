class CreateActiveSubstancesMedicaments < ActiveRecord::Migration
  def change
    create_table :active_substances_medicaments, id:false do |t|
      t.belongs_to :active_substance, index: true
      t.belongs_to :medicament, index: true
    end
    add_foreign_key :active_substances_medicaments, :active_substances
    add_foreign_key :active_substances_medicaments, :medicaments
  end
end
