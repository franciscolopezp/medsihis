class CreateCurrencyExchanges < ActiveRecord::Migration
  def change
    create_table :currency_exchanges do |t|
      t.integer :doctor_id
      t.integer :currency_id
      t.float :value
      t.boolean :is_default

      t.timestamps null: false
    end
  end
end
