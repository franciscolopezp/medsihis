class AddCloseUserIdToDailyCash < ActiveRecord::Migration
  def change
    add_column :daily_cashes, :close_user_id, :integer
  end
end
