class CreateAssortmentPharmacyItems < ActiveRecord::Migration
  def change
    create_table :assortment_pharmacy_items do |t|
      t.integer :quantity
      t.decimal :cost, precision: 10, scale: 2
      t.references :pharmacy_item, index: true, foreign_key: true
      t.references :warehouse, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
