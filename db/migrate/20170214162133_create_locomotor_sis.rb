class CreateLocomotorSis < ActiveRecord::Migration
  def change
    create_table :locomotor_sis do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.text :mialgias
      t.text :artralgias
      t.text :articular_rigideces
      t.text :inflammations

      t.timestamps null: false
    end
  end
end
