class CreateSuppliers < ActiveRecord::Migration
  def change
    create_table :suppliers do |t|
      t.string :name
      t.string :telephone
      t.string :address
      t.string :contact

      t.timestamps null: false
    end
  end
end
