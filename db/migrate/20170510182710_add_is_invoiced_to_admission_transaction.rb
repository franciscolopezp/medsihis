class AddIsInvoicedToAdmissionTransaction < ActiveRecord::Migration
  def change
    add_column :admission_transactions, :is_invoiced, :boolean
  end
end
