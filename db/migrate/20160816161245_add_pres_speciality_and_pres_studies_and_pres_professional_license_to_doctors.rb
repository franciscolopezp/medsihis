class AddPresSpecialityAndPresStudiesAndPresProfessionalLicenseToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :pres_speciality, :string
    add_column :doctors, :pres_studies, :string
    add_column :doctors, :pres_professional_license, :string
  end
end
