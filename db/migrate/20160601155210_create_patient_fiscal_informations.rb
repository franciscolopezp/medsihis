class CreatePatientFiscalInformations < ActiveRecord::Migration
  def change
    create_table :patient_fiscal_informations do |t|
      t.string :person_type
      t.string :business_name
      t.string :rfc
      t.string :address
      t.string :locality
      t.string :ext_number
      t.string :int_number
      t.string :zip

      t.timestamps null: false
    end
  end
end
