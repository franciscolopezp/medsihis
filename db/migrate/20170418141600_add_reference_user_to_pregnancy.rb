class AddReferenceUserToPregnancy < ActiveRecord::Migration
  def change
    add_reference :pregnancies, :user, index: true, foreign_key: true
    add_reference :birth_infos, :user, index: true, foreign_key: true
    add_reference :abortions, :user, index: true, foreign_key: true
  end
end
