class CreateDigestiveAps < ActiveRecord::Migration
  def change
    create_table :digestive_aps do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.string :anorexia
      t.string :polidipsia
      t.string :nauseas
      t.string :vomitos
      t.string :dispepsia
      t.string :disfagia
      t.string :odinofagia
      t.string :rectorragia
      t.string :melenas
      t.string :abdominalgia
      t.string :pirosis
      t.string :hematemesis
      t.string :acolia
      t.string :meteorismo
      t.string :tenesmo

      t.timestamps null: false
    end
  end
end
