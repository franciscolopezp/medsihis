class AddReferenceFiscalInformationToDoctor < ActiveRecord::Migration
  def change
    add_reference :doctors, :fiscal_information, index: true, foreign_key: true
  end
end
