class AddInventoryToInventoryStock < ActiveRecord::Migration
  def change
    add_reference :inventory_stocks, :inventory, index: true, foreign_key: true, :after => :id
  end
end
