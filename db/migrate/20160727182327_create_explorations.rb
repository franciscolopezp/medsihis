class CreateExplorations < ActiveRecord::Migration
  def change
    create_table :explorations do |t|
      t.string :e_type
      t.string :breasts
      t.string :vulva
      t.string :abdomen
      t.string :cervix
      t.string :ovaries
      t.string :annexed
      t.string :edemas
      t.string :uterine_height
      t.references :medical_consultation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
