class CreateGenitalAps < ActiveRecord::Migration
  def change
    create_table :genital_aps do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.text :menarquia
      t.text :menstrual_rhythm
      t.text :hypermenorrea
      t.text :amenorrea
      t.text :metrorragia
      t.text :leucorrea
      t.text :dismenorrea
      t.text :dispareunia
      t.text :impotence
      t.text :libido

      t.timestamps null: false
    end
  end
end
