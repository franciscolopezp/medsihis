class CreateServiceCharges < ActiveRecord::Migration
  def change
    create_table :service_charges do |t|
      t.float :cost
      t.text :observations
      t.references :user, index: true, foreign_key: true
      t.references :hospital_service, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
