class CreateNoPathologicals < ActiveRecord::Migration
  def change
    create_table :no_pathologicals do |t|
      t.boolean :smoking
      t.text :smoking_description
      t.boolean :alcoholism
      t.text :alcoholism_description
      t.boolean :play_sport
      t.text :play_sport_description
      t.text :annotations

      t.timestamps null: false
    end
  end
end
