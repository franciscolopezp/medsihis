class CreateAccountsOffices < ActiveRecord::Migration
  def change
    create_table :accounts_offices, id:false do |t|
      t.belongs_to :account, index: true
      t.belongs_to :office, index: true
    end
    add_foreign_key :accounts_offices, :accounts
    add_foreign_key :accounts_offices, :offices
  end
end
