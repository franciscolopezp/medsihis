class AddFolioToAdmissionTransaction < ActiveRecord::Migration
  def change
    add_column :admission_transactions, :folio, :string, :after => :id
  end
end
