class AddCategoryImcAndRankImcAdultToImcAdults < ActiveRecord::Migration
  def change
    add_reference :imc_adults, :category_imc, index: true, foreign_key: true
    add_reference :imc_adults, :rank_imc_adult, index: true, foreign_key: true
  end
end
