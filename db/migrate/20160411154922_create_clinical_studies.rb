class CreateClinicalStudies < ActiveRecord::Migration
  def change
    create_table :clinical_studies do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
