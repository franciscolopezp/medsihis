class CreateTableDoctorsLaboratories < ActiveRecord::Migration
  def change
    create_table :doctors_laboratories, :id => false do |t|
      t.references :doctor, index: true, foreign_key: true
      t.references :laboratory, index: true, foreign_key: true
    end
  end
end
