class Oxygensaturation < ActiveRecord::Migration
  def change
    add_column :vital_signs, :oxygen_saturation, :float
  end
end
