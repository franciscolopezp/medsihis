class CreateActiveSubstances < ActiveRecord::Migration
  def change
    create_table :active_substances do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
