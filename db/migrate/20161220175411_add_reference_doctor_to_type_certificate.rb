class AddReferenceDoctorToTypeCertificate < ActiveRecord::Migration
  def change
    add_reference :type_certificates, :doctor, index: true, foreign_key: true
  end
end
