class CreateInvoiceConcepts < ActiveRecord::Migration
  def change
    create_table :invoice_concepts do |t|
      t.integer :quantity
      t.string :description
      t.float :unit_price
      t.float :cost

      t.timestamps null: false
    end
  end
end
