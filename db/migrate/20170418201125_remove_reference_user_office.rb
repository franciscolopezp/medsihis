class RemoveReferenceUserOffice < ActiveRecord::Migration
  def change
    remove_reference(:offices, :user, index: true, foreign_key: true)
  end
end
