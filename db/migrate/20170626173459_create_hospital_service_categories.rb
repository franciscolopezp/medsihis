class CreateHospitalServiceCategories < ActiveRecord::Migration
  def change
    create_table :hospital_service_categories do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
