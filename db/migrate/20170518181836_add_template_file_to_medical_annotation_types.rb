class AddTemplateFileToMedicalAnnotationTypes < ActiveRecord::Migration
  def change
    add_column :medical_annotation_types, :template_file, :string, :before => :template
  end
end
