class RemoveTotalControlAgendaFromAssistants < ActiveRecord::Migration
  def change
    remove_column :assistants, :total_control_agenda, :boolean
    remove_reference :assistants, :office, index: true, foreign_key: true
  end
end
