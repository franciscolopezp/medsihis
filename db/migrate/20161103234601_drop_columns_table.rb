class DropColumnsTable < ActiveRecord::Migration
  def change


    add_column :family_diseases, :psychiatric_diseases, :boolean
    add_column :family_diseases, :psychiatric_diseases_description, :string
    add_column :family_diseases, :neurological_diseases, :boolean
    add_column :family_diseases, :neurological_diseases_description, :string
    add_column :family_diseases, :cardiovascular_diseases, :boolean
    add_column :family_diseases, :cardiovascular_diseases_description, :string
    add_column :family_diseases, :bronchopulmonary_diseases, :boolean
    add_column :family_diseases, :bronchopulmonary_diseases_description, :string
    add_column :family_diseases, :thyroid_diseases, :boolean
    add_column :family_diseases, :thyroid_diseases_description, :string
    add_column :family_diseases, :kidney_diseases, :boolean
    add_column :family_diseases, :kidney_diseases_description, :string


    remove_column :inherited_family_backgrounds, :psychiatric_diseases
    remove_column :inherited_family_backgrounds, :psychiatric_diseases_description
    remove_column :inherited_family_backgrounds, :neurological_diseases
    remove_column :inherited_family_backgrounds, :neurological_diseases_description
    remove_column :inherited_family_backgrounds, :cardiovascular_diseases
    remove_column :inherited_family_backgrounds, :cardiovascular_diseases_description
    remove_column :inherited_family_backgrounds, :bronchopulmonary_diseases
    remove_column :inherited_family_backgrounds, :bronchopulmonary_diseases_description
    remove_column :inherited_family_backgrounds, :thyroid_diseases
    remove_column :inherited_family_backgrounds, :thyroid_diseases_description
    remove_column :inherited_family_backgrounds, :kidney_diseases
    remove_column :inherited_family_backgrounds, :kidney_diseases_description
  end
end
