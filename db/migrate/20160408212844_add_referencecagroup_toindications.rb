class AddReferencecagroupToindications < ActiveRecord::Migration
  def change
    add_reference :indications, :ca_group, index: true, foreign_key: true
  end
end
