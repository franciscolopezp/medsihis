class AddInstituteToIdentityCards < ActiveRecord::Migration
  def change
    add_column :identity_cards, :institute, :string
  end
end
