class CreateMetabolicSis < ActiveRecord::Migration
  def change
    create_table :metabolic_sis do |t|
      t.text :weight
      t.text :size_body_shape
      t.text :asthenia
      t.text :weakness
      t.text :appetite
      t.text :thirst
      t.text :hot_cold
      t.text :sex
      t.text :galactorrea
      t.text :age_puberty
      t.references :medical_history, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
