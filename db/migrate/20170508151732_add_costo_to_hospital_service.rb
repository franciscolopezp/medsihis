class AddCostoToHospitalService < ActiveRecord::Migration
  def change
    add_column :hospital_services, :cost, :float
  end
end
