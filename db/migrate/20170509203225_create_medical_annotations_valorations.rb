class CreateMedicalAnnotationsValorations < ActiveRecord::Migration
  def change
    create_table :medical_annotations_valorations, :id => false do |t|
      t.references :medical_annotation, index: true, foreign_key: true
      t.references :patient_valoration, index: true, foreign_key: true
    end
  end
end
