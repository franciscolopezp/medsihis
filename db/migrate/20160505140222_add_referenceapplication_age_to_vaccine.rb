class AddReferenceapplicationAgeToVaccine < ActiveRecord::Migration
  def change
    add_reference :application_ages, :vaccine, index: true
  end
end
