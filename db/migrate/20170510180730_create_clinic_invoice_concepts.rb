class CreateClinicInvoiceConcepts < ActiveRecord::Migration
  def change
    create_table :clinic_invoice_concepts do |t|
      t.integer :quantity
      t.text :description
      t.decimal :unit_price, precision: 11, scale: 2
      t.decimal :cost, precision: 11, scale: 2
      t.references :clinic_invoice, index:true, foreign_key:true
      t.references :measure_unit, index:true, foreign_key:true
      t.text :unit
      t.timestamps null: false
    end
  end
end
