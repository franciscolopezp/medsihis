class Addmorevitalsign < ActiveRecord::Migration
  def change
    add_column :vital_signs, :heart_rate, :float
    add_column :vital_signs, :breathing_frequency, :float
  end
end
