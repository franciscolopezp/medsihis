class AddReferenceTwiceModel < ActiveRecord::Migration
  def change
    remove_column :medicament_movements, :from_warehouse
    remove_column :medicament_movements, :to_warehouse
    add_reference :medicament_movements, :from_warehouse
    add_reference :medicament_movements, :to_warehouse
  end
end
