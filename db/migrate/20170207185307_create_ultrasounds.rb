class CreateUltrasounds < ActiveRecord::Migration
  def change
    create_table :ultrasounds do |t|
      t.references :pregnancy, index: true, foreign_key: true
      t.date :date
      t.integer :weeks

      t.timestamps null: false
    end
  end
end
