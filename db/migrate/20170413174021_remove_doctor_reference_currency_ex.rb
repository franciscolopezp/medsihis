class RemoveDoctorReferenceCurrencyEx < ActiveRecord::Migration
  def change
    remove_reference(:currency_exchanges, :doctor, index: true)
    add_reference :currency_exchanges, :user, index: true, foreign_key: true
  end
end
