class AddReferencedoctorToAllergies < ActiveRecord::Migration
  def change
    add_reference :allergies, :doctor, index: true, foreign_key: true
  end
end
