class CreateClinicInvoiceEmisors < ActiveRecord::Migration
  def change
    create_table :clinic_invoice_emisors do |t|
      t.string :business_name
      t.string :rfc
      t.string :address
      t.string :suburb
      t.string :no_ext
      t.string :no_int
      t.string :zip
      t.string :locality
      t.string :city
      t.string :state
      t.string :country
      t.string :tax_regime
      t.references :clinic_invoice, index:true, foreign_key:true
      t.timestamps null: false
    end
  end
end
