class CreateFamilyDiseases < ActiveRecord::Migration
  def change
    create_table :family_diseases do |t|
      t.boolean :has_diabetes
      t.text :diabetes_description
      t.boolean :has_overweight
      t.text :overweight_description
      t.boolean :has_hypertension
      t.text :hypertension_description
      t.boolean :has_asthma
      t.text :asthma_description
      t.boolean :has_cancer
      t.text :cancer_description

      t.timestamps null: false
    end
  end
end
