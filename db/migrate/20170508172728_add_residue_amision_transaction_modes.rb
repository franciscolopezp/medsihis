class AddResidueAmisionTransactionModes < ActiveRecord::Migration
  def change
    add_column :medicament_assortments, :residue, :float
    add_column :service_charges, :residue, :float
  end
end
