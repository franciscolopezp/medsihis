class CreateMovementCategories < ActiveRecord::Migration
  def change
    create_table :movement_categories do |t|
      t.string :name
      t.boolean :m_type

      t.timestamps null: false
    end
  end
end
