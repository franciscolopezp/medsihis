class AddIsClosedToOtherCharge < ActiveRecord::Migration
  def change
    add_column :other_charges, :is_closed, :boolean
  end
end
