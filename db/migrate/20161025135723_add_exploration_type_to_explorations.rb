class AddExplorationTypeToExplorations < ActiveRecord::Migration
  def change
    add_reference :explorations, :exploration_type, index: true, foreign_key: true
  end
end
