class AddCodeToHospitalService < ActiveRecord::Migration
  def change
    add_column :hospital_services, :code, :string
  end
end
