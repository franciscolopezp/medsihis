class AddColumnTermConditionAndIsActiveToUserPatients < ActiveRecord::Migration
  def change
    add_column :user_patients, :accept_term_conditions, :boolean
    add_column :user_patients, :is_active, :boolean
  end
end
