class CreateExpedientPermissions < ActiveRecord::Migration
  def change
    create_table :expedient_permissions do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
