class CreateValorationFields < ActiveRecord::Migration
  def change
    create_table :valoration_fields do |t|
      t.string :f_key
      t.string :name
      t.integer :f_type
      t.string :f_html_type

      t.timestamps null: false
    end
  end
end
