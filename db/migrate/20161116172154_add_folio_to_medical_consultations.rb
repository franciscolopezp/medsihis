class AddFolioToMedicalConsultations < ActiveRecord::Migration
  def change
    add_column :medical_consultations, :folio, :integer
  end
end
