class CreateDefaultPerinatalBackgrounds < ActiveRecord::Migration
  def change
    create_table :default_perinatal_backgrounds do |t|
      t.string :hospital
      t.float :weight
      t.float :height
      t.text :pregnancy_risks
      t.text :other_info
      t.string :scholarship
      t.string :house_type
      t.string :hounse_inmates
      t.timestamps null: false
    end
  end
end
