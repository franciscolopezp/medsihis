class RemoveDoctorReferenceDoctorPatienMediExp < ActiveRecord::Migration
  def change
    remove_reference(:patients, :doctor, index: true, foreign_key: true)
    remove_reference(:medical_expedients, :doctor, index: true, foreign_key: true)
    remove_reference(:general_fiscal_infos, :doctor, index: true, foreign_key: true)
    add_reference :patients, :user, index: true, foreign_key: true
    add_reference :medical_expedients, :user, index: true, foreign_key: true
    add_reference :general_fiscal_infos, :user, index: true, foreign_key: true
  end
end
