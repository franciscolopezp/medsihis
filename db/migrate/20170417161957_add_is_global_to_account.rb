class AddIsGlobalToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :is_global, :boolean
  end
end
