class AddSeriesToClinicInfo < ActiveRecord::Migration
  def change
    add_column :clinic_infos, :ticket_series, :string
    add_column :clinic_infos, :ticket_next_folio, :integer
  end
end
