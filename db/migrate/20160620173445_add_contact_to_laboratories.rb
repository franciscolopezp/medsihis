class AddContactToLaboratories < ActiveRecord::Migration
  def change
    add_column :laboratories, :contact, :string
  end
end
