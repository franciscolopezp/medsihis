class CreateTimeZones < ActiveRecord::Migration
  def change
    create_table :time_zones do |t|
      t.string :name
      t.string :rails_name
      t.string :country_name
      t.string :utc_value

      t.timestamps null: false
    end
  end
end
