class Addreferenceinvoicepaymentmethod < ActiveRecord::Migration
  def change
    add_reference :invoices, :payment_method, index: true
  end
end
