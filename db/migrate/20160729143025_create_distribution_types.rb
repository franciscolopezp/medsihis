class CreateDistributionTypes < ActiveRecord::Migration
  def change
    create_table :distribution_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
