class CreateChargeReportStudies < ActiveRecord::Migration
  def change
    create_table :charge_report_studies do |t|
      t.references :cabinet_report, index: true, foreign_key: true
      t.string :details

      t.timestamps null: false
    end
  end
end
