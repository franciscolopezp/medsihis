class CreateChildNonPathologicalInfos < ActiveRecord::Migration
  def change
    create_table :child_non_pathological_infos do |t|
      t.references :medical_history, index: true, foreign_key: true
      t.string :house_type
      t.integer :number_people
      t.string :child_academic_grade

      t.timestamps null: false
    end
  end
end
