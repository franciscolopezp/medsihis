class CreateVaccineBooks < ActiveRecord::Migration
  def change
    create_table :vaccine_books do |t|
      t.string :name
      t.string :gender
      t.integer :start_age
      t.integer :end_age

      t.timestamps null: false
    end
  end
end
