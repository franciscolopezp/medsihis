class AddTwiceWarehousePharMov < ActiveRecord::Migration
  def change
    add_reference :pharmacy_item_movements, :from_warehouse
    add_reference :pharmacy_item_movements, :to_warehouse
  end
end
