class CreateExpedientRequests < ActiveRecord::Migration
  def change
    create_table :expedient_requests do |t|
      t.references :user_patient, index: true, foreign_key: true
      t.references :medical_expedient, index: true, foreign_key: true
      t.integer :status

      t.timestamps null: false
    end
  end
end
