class CreateCabinetReportPictures < ActiveRecord::Migration
  def change
    create_table :cabinet_report_pictures do |t|
      t.text :picture_root
      t.text :annotations

      t.timestamps null: false
    end
  end
end
