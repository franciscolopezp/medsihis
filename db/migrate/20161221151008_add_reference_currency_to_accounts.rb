class AddReferenceCurrencyToAccounts < ActiveRecord::Migration
  def change
    add_reference :accounts, :currency, index: true, foreign_key: true
  end
end
