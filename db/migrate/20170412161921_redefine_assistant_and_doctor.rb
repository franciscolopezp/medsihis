class RedefineAssistantAndDoctor < ActiveRecord::Migration
  def change
    remove_reference(:doctors, :city, index: true, foreign_key: true)
    remove_reference(:doctors, :gender, index: true, foreign_key: true)
    remove_reference(:assistants, :city, index: true, foreign_key: true)
    remove_reference(:assistants, :gender, index: true, foreign_key: true)
    add_reference :doctors, :person, index: true, foreign_key: true
    add_reference :assistants, :person, index: true, foreign_key: true
    remove_column :doctors, :name
    remove_column :doctors, :last_name
    remove_column :doctors, :birth_date
    remove_column :doctors, :telephone
    remove_column :doctors, :cellphone
    remove_column :doctors, :address
    remove_column :doctors, :other_location

    remove_column :assistants, :name
    remove_column :assistants, :last_name
    remove_column :assistants, :birth_day
    remove_column :assistants, :telephone
    remove_column :assistants, :cellphone
    remove_column :assistants, :address

  end
end
