class RemoveBirthDateFromPerinatalInformations < ActiveRecord::Migration
  def change
    remove_column :perinatal_informations, :birthdate
  end
end
