class Addinvoicetransacrelation < ActiveRecord::Migration
  def change
    add_reference :invoices, :transaction, index: true
  end
end
