class CreateIndications < ActiveRecord::Migration
  def change
    create_table :indications do |t|
      t.string :description
      t.integer :priority

      t.timestamps null: false
    end
  end
end
