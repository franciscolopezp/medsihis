class CreateAnnotationFields < ActiveRecord::Migration
  def change
    create_table :annotation_fields do |t|
      t.references :medical_annotation, index: true, foreign_key: true
      t.references :medical_annotation_field, index: true, foreign_key: true
      t.string :data

      t.timestamps null: false
    end
  end
end
