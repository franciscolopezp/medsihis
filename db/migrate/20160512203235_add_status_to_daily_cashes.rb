class AddStatusToDailyCashes < ActiveRecord::Migration
  def change
    add_column :daily_cashes, :status, :integer
  end
end
