class CreateEmailSettings < ActiveRecord::Migration
  def change
    create_table :email_settings do |t|
      t.string :host
      t.string :username
      t.string :password
      t.integer :port

      t.timestamps null: false
    end
  end
end
