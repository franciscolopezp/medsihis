class CreateMedicamentPurchases < ActiveRecord::Migration
  def change
    create_table :medicament_purchases do |t|
      t.float :cost
      t.integer :folio
      t.string :invoice_number
      t.references :supplier, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.references :warehouse, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
