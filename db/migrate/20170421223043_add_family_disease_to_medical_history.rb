class AddFamilyDiseaseToMedicalHistory < ActiveRecord::Migration
  def change
    add_reference :medical_histories, :family_disease, index: true, foreign_key: true
  end
end
