class CreatePercentileCdcs < ActiveRecord::Migration
  def change
    create_table :percentile_cdcs do |t|
      t.references :gender, index: true, foreign_key: true
      t.float :months
      t.float :l
      t.float :m
      t.float :s
      t.float :p3
      t.float :p5
      t.float :p10
      t.float :p25
      t.float :p50
      t.float :p75
      t.float :p85
      t.float :p90
      t.float :p95
      t.float :p97
      t.references :type_percentile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
