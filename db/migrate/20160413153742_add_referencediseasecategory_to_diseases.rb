class AddReferencediseasecategoryToDiseases < ActiveRecord::Migration
  def change
    add_reference :diseases, :disease_category, index: true, foreign_key: true
  end
end
