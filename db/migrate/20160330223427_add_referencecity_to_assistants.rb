class AddReferencecityToAssistants < ActiveRecord::Migration
  def change
    add_reference :assistants, :city, index: true, foreign_key: true
  end
end
