class AddColumnMedicalExpedientToMedicalConsultations < ActiveRecord::Migration
  def change
    add_reference :medical_consultations, :medical_expedient, index: true, foreign_key: true
  end
end
