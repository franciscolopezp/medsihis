class AddSharedToAssistant < ActiveRecord::Migration
  def change
    add_column :assistants, :shared, :boolean
  end
end
