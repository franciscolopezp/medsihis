class AddChargeReportsToClincInfo < ActiveRecord::Migration
  def change
    add_column :clinic_infos, :charge_reports, :boolean
  end
end
