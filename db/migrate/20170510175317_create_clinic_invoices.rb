class CreateClinicInvoices < ActiveRecord::Migration
  def change
    create_table :clinic_invoices do |t|
      t.string :xml_base
      t.text :original_string
      t.string :serie
      t.integer :folio
      t.string :certification_date
      t.string :payment_condition
      t.string :payment_methos_name
      t.string :account_number
      t.decimal :subtotal, precision: 10, scale: 2
      t.decimal :iva, precision: 10, scale: 2
      t.decimal :isr, precision: 10, scale: 2
      t.string :UUID
      t.text :xml
      t.references :patient_fiscal_information, index: true, foreign_key: true
      t.text :sat_seal
      t.string :no_cert_sat
      t.string :transmitter_seal
      t.string :qr_code
      t.boolean :cancel
      t.integer :invoice_type
      t.references :user, index:true, foreign_key: true
      t.references :payment_way, index:true, foreign_key: true
      t.text :original_string_text
      t.timestamps null: false
    end
  end
end
