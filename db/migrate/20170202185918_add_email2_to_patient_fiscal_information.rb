class AddEmail2ToPatientFiscalInformation < ActiveRecord::Migration
  def change
    add_column :patient_fiscal_informations, :email2, :string
  end
end
