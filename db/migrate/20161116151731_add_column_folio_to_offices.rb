class AddColumnFolioToOffices < ActiveRecord::Migration
  def change
    add_column :offices, :folio, :integer
  end
end
