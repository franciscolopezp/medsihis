class CreateOtherChargeTransactions < ActiveRecord::Migration
  def change
    create_table :other_charge_transactions do |t|
      t.string :folio
      t.decimal :amount, precision: 10, scale: 2
      t.references :user, index: true, foreign_key: true
      t.references :other_charge, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
