class CreateInventoryStocks < ActiveRecord::Migration
  def change
    create_table :inventory_stocks do |t|
      t.integer :quantity
      t.references :lot, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
