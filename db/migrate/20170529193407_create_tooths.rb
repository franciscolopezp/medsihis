class CreateTooths < ActiveRecord::Migration
  def change
    create_table :tooths do |t|
      t.string :name
      t.boolean :is_superior
      t.boolean :is_adult

      t.timestamps null: false
    end
  end
end
