class AddZipToFiscalInformation < ActiveRecord::Migration
  def change
    add_column :fiscal_informations, :zip, :string
    add_column :fiscal_informations, :int_number, :string
    add_column :fiscal_informations, :locality, :string
    add_column :fiscal_informations, :suburb, :string
    add_column :fiscal_informations, :serie, :string
    add_column :fiscal_informations, :folio, :string
  end
end
