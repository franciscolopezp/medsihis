
var SELECTED_DATE = null;
var PENDING_STATUS = 12;
var global_data = {
    doctor_id: 0,
    office_id : 0,
    start_date : null,
    end_date: null,
    doctor_name : "",
    office_name : "",
    doctor_gender_id:0
};


$(document).ready(function(){
    var grid = $("#data-table-appointments").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $("#data-table-appointments").attr("data-source"),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                var html =  "<button class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Cancelar\"><i class=\"zmdi zmdi-delete\"></i></button>";
                return "";
            }
        }
    });

    var grid_doctors = $("#data-table-doctors").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function(){
            var city = $('#search_doctor_city_ts_autocomplete').val();
            var specialty = $("#search_doctor_specialty").val();
            return $('#data-table-doctors').data('source')+"?city_id="+city+"&specialty_id="+specialty;
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {

        }
    });
});

$(function() {
/*
    $("#search_doctor_city").on('change',function(){
        console.log("hello");
            setTimeout(function(){

            },500);
    });
*/
    $("body").on("blur",'#search_doctor_city',function(){
        $("#data-table-doctors").bootgrid("reload");
    });

    $("#search_doctor_specialty").change(function(){
        $("#data-table-doctors").bootgrid("reload");
    });

    //Generate the Calendar
    $("#select_date").fullCalendar({
        monthNames:monthNames,
        dayNames:['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
        dayNamesShort:['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
        header: {
            right: '',
            center: 'prev, title, next',
            left: ''
        },
        theme: true, //Do not remove this as it ruin the design
        selectable: true,
        selectHelper: true,
        editable: true,
        events: [],
        select: function(start, end, allDay) {
            var sd = start.toDate();
            sd.setTime(sd.getTime() + (1 * 24 * 60 * 60 * 1000));

            var today = new Date();
            if(sd < today){
                swal("Seleccione una fecha correcta","No se puede consultar disponibilidad de fechas anteriores","error");
                return;
            }

            SELECTED_DATE = sd;

            $("#info_selected_date").html("Fecha: "+dateToString(SELECTED_DATE,"dd/fm/yyyy"));

            loadDoctorSchedule();
        }
    });


    $('body').on('click', 'button.fc-prev-button', function() {
        checkAvailability();
    });

    $('body').on('click', 'button.fc-next-button', function() {
        checkAvailability();
    });

    $(".fc-other-month").html("");

});

function selectDoctor(doctor_id, office_id,doctor_name, office_name,doctor_gender_id){
    global_data.doctor_id = doctor_id;
    global_data.office_id = office_id;
    global_data.doctor_name = doctor_name;
    global_data.office_name = office_name;
    global_data.doctor_gender_id = doctor_gender_id;

    $('html, body').animate({
        scrollTop:($("#select_date").offset().top - 120)
    },1000);

    checkAvailability();
}

function checkAvailability(){

    var date = $("#select_date").fullCalendar('getDate').toDate();

    var month_int = date.getMonth() + 1;
    var year_int = date.getFullYear();


    var today = new Date();
    var today_month = today.getMonth() + 1;
    var today_year = today.getFullYear();


    var aux1 = new Date(year_int, month_int - 1, 1);
    var aux2 = new Date(today_year, today_month - 1, 1);

    if(aux1 < aux2){
        swal("Por favor seleccione una fecha válida","No se puede verificar disponibilidad de meses anteriores.","error");
        $("#select_date").fullCalendar( 'gotoDate', today );
        return;
    }

    if(global_data.doctor_id == 0){
        swal("Por favor seleccione un doctor");
        return;
    }

    if(global_data.office_id == 0){
        swal("Por favor seleccione un consultorio");
        return;
    }

    $.ajax({
        url:"/paciente/citas/check_availability",
        data:{
            doctor_id:global_data.doctor_id,
            office_id:global_data.office_id,
            year:year_int,
            month:month_int
        },
        dataType:"json",
        success:function(data){

            var days = $(".fc-day-number").not(".fc-other-month");

            $.each(days,function(idx,day){

                var bk_color = "";
                var color = "";
                if(data[$(day).html()] == "AD"){
                    bk_color = "green";
                    color = "white";
                }else if (data[$(day).html()] == "ND"){
                    bk_color = "red";
                    color = "white";
                }else if (data[$(day).html()] == "MD"){
                    bk_color = "orange";
                }


                $(day).css( "background", bk_color );
                $(day).css( "color", color );

            });

            $(".fc-other-month").html("");

        }
    });
}


function loadDoctorSchedule(){
    if(global_data.doctor_id == 0 || global_data.office_id == 0){
        swal("No se puede consultar el horario","Por favor seleccione correctamente el doctor y el consultorio.","error");
        return;
    }

    $("#schedule_calendar").fullCalendar( 'destroy' );

    $.ajax({
        url:"/paciente/citas/load_doctor_schedule",
        data:{
            doctor_id:global_data.doctor_id,
            office_id:global_data.office_id,
            date:SELECTED_DATE
        },
        dataType:"json",
        success:function(data){

            $("#schedule_calendar").fullCalendar({
                height: 650,
                allDaySlot:false,
                editable: false,
                monthNames:monthNames,
                dayNames:['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
                dayNamesShort:['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
                header: {
                    right: '',
                    center: 'prev, title, next',
                    left: ''
                },
                theme: true, //Do not remove this as it ruin the design
                selectable: true,
                selectHelper: true,
                events: [],
                defaultView:'agendaDay',
                minTime:data.start_time,
                maxTime:data.end_time,
                select: function(start, end, allDay) {
                   return;
                },
                eventClick: function(calEvent, jsEvent, view) {
                    var caldate = calEvent.start.toDate();
                    var now  = new Date();

                    if(!calEvent.available){
                        swal("No se puede generar una cita.","El horario no se encuentra disponible por favor seleccione uno diferente","error");
                        return;
                    }else if(caldate < now){
                        swal("No se puede generar una cita.","El horario seleccionado ya pasó.","error");
                        return;
                    }

                    var html = getActivityHtml(calEvent);
                    $("#appointment_info").html(html);

                    $("#appointment_details").val("");

                    $("#add_appointment_modal").modal("show");
                }
            });

            $("#schedule_calendar").fullCalendar( 'gotoDate', SELECTED_DATE );
            $('#calendar').fullCalendar('option', 'height', 700);

            $("#schedule_calendar .fc-toolbar").remove();

            $.each(data.activities,function(idx,activity){
                $("#schedule_calendar").fullCalendar('renderEvent',activity,true ); //Stick the event
            });

        }
    });
}

function saveAppointment(){
    var patient_name = $("#user_full_name").val();
    var params = {
        name:"Cita "+patient_name,
        details:$("#appointment_details").val(),
        start_date: global_data.start_date,
        end_date:global_data.end_date,
        all_day:false,
        activity_type_id:1,
        activity_status_id:PENDING_STATUS,
        personal_supports: [],
        doctor_id:global_data.doctor_id
    }

    var method = "POST";
    var url = "/paciente/citas/";

    $.ajax({
        url:url,
        dataType:"json",
        method:method,
        data:{
            activity:params,
            office_id:global_data.office_id,
            is_owner:false
        },
        success:function(data){
            if(typeof data == "string"){
                data = JSON.parse(data);
            }

            var prefix = "al Dr.";

            if(global_data.doctor_gender_id != 1){
                prefix = "a la Dra.";
            }

            if(!data.done){
                swal(data.title,data.message,data.type);
            }else{
                swal("Estimado(a) "+patient_name,"Hemos enviado la solicitud de cita "+prefix+" "+global_data.doctor_name+". En breve recibirás una notificación en tu bandeja de correo electrónico para confirmar que tu cita ha sido aceptada.","success");
            }
            loadDoctorSchedule();
        }
    });
}

function getActivityHtml(event){
    var start_date = stringToDate(event.s_date,"yyyy-mm-dd hh:mn");
    var end_date = stringToDate(event.e_date,"yyyy-mm-dd hh:mn");


    var html = "";
    html += "<strong>Fecha : </strong> "+dateToString(SELECTED_DATE,"dd/fm/yyyy")+"<br><br>";
    html += "<strong>Doctor : </strong> "+global_data.doctor_name;
    html += "<br><strong>Consultorio : </strong> "+global_data.office_name;
    html += "<br><strong>Horario : </strong>" + dateToString(start_date,"hh:mn pp")+" - "+dateToString(end_date,"hh:mn pp");

    global_data.start_date = start_date;
    global_data.end_date = end_date;

    return html;
}