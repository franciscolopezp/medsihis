$(function(){
    //$(".appointments_prescriptions table").bootgrid();

    $(".show_doctor_consultation_checkbox").click(function(){
        var expedient = $(this).val();

        if($(this).is(":checked")){
            $(".data_expedient_"+expedient).show();
        }else{
            $(".data_expedient_"+expedient).hide();
        }

        //$(".appointments_prescriptions table").bootgrid();
    });

});

function sendLinkRequest(force){
    var expedient_code = $("#link_code").val();
    var health_history  = $("#health_history_id").val();
    if(expedient_code == ""){
        swal("No se puede vincular el expediente.","El campo código está vacío.","error");
    }

    $.ajax({
        method:"POST",
        url:expedients_path,
        dataType:"json",
        data:{
            code:expedient_code,
            force:force,
            health_history_id:health_history
        },
        success:function(data){
            if(data.exist){
                swal({
                    title: "Vincular expediente",
                    text: "¿Confirma que desea vincular a su cuenta el expediente de "+data.patient_name+"?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn btn-primary",
                    confirmButtonText: "Confirmar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: true,
                    closeOnCancel: false
                }, function(isConfirm){
                    if (isConfirm) {
                        sendLinkRequest(true);
                    } else {
                        $("#link_expedient_modal").modal("hide");
                        $("#link_code").val("");
                        swal("Vinculación cancelada", "No se envió la solicitud de vinculación.", "error");
                    }
                });

            }else{
                if(data.done){
                    $("#link_expedient_modal").modal("hide");
                    $("#link_code").val("");
                }

                setTimeout(function(){
                    swal(data.title, data.message, data.type);
                },200);
            }
        }
    });
}

function openLinkExpedientModal(health_history){
    $("#link_code").val("");
    $("#health_history_id").val(health_history);
    $("#link_expedient_modal").modal("show");
}

function openNewHealthHistoryModal(){
    $("#health_history_name").val("");
    $("#health_history_id").val("");
    $("#health_history_action").val("add");
    $("#new_health_history_modal").modal("show");
}

function createHealthHistory(){

    if($("#health_history_name").val() == ""){
        swal("No se puede guardar el historial","Por favor ingrese un nombre válido.","error");
        return;
    }

    $("#new_health_history_modal").modal("show");
    $.ajax({
        url:create_health_history_path,
        dataType:"json",
        data:{
            id:$("#health_history_id").val(),
            name:$("#health_history_name").val(),
            operation:$("#health_history_action").val()
        },
        success:function(){
            location.reload();
        }
    });
}

function openChangeDefaultDataModal(health_history){
    $('input[name=default_doctor]').prop('checked', false);
    $("#default_data_health_history_id").val(health_history);
    $(".radio_health_history").hide();
    $(".health_history_"+health_history).show();
    $("#default_expedient_info").modal("show");
}

function setDefaultPatientData(){
    var selected_request = $('input[name=default_doctor]').filter(':checked').val();
    var medical_history = $("#default_data_health_history_id").val();

    if(selected_request == undefined){
        swal("No se puede establecer el médico.","Seleccione una de las opciones.","error");
        return;
    }

    $.ajax({
        url:default_data_patient_path,
        dataType:"json",
        data:{
            health_history_id:medical_history,
            request_id:selected_request
        },
        success:function(){
            location.reload();
        }
    });

}