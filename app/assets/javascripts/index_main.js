
//= require index_page/modernizr.custom.32033
//= require index_page/jquery-1.11.1.min
//= require index_page/bootstrap.min
//= require index_page/slick.min
//= require index_page/placeholdem.min
//= require index_page/jquery.themepunch.plugins.min
//= require index_page/jquery.themepunch.revolution.min
//= require index_page/waypoints.min
//= require index_page/scripts
//= require jquery-ui
//= require bootstrap-sweetalert/sweet-alert.min.js
//= require ts_autocomplete

var fields = ["doctor_name","doctor_last_name","doctor_phone","doctor_email","doctor_city","doctor_city_ts_autocomplete"];
var fields_patient = ["patient_name","patient_last_name","patient_birth_date","patient_email","patient_phone","patient_city","patient_gender", "patient_user", "patient_password", "patient_confirm_password"];


$(document).ready(function(){

    setTimeout(function(){
        $("#map_holder").hide();
    },200);

    setTimeout(function(){
        var specialty_id = $( "#specialties_list li").first().find("a").attr("specialty");
        if(specialty_id != undefined){
            loadDoctors(specialty_id);
        }
    },200);

    $("#city_search").ts_autocomplete({
        url:$("#url_autocomplete_cities").val(),
        in_modal:true,
        items:10,
        required:false,
        model:"una ciudad"
    });

    $(".search-city").ts_autocomplete({
        url:$("#url_autocomplete_cities").val(),
        in_modal:true,
        items:10,
        required:true,
        model:"una ciudad"
    });

    ///chat

    $("#main_chat_header").click(function(){
        $("#main_chat_form").slideToggle();
    });

});

$(function(){
    $("#search_doctor_btn").click(function(){
        var name = $("#name_search").val();
        if(name == "Búsqueda por nombre"){
            $("#name_search").val("");
        }
        $("#search_doctor_form").submit();
    });

    $("#search_all_btn").click(function(){
        $("#name_search").val("");
        $("#city_search").val("");
        $("#city_search_ts_autocomplete").val("");
        $("#search_doctor_form").submit();
    });



    $("#view_btn").click(function(){
        if($(this).attr("view") == "list"){
            showMap();
        }else{
            $("#doctors_holder").show();
            $("#map_holder").hide();
            $("#view_btn").attr("view","list");
            $("#view_btn").html("Ver Mapa");
        }
    });

    $('input[name="register_type"]').change(function(){
        $("#register_btn_user").prop("disabled",false);
        if($(this).val() == "doctor"){
            $("#form-patient").hide();
            $("#form-doctor").show();
        }else{
            $("#form-patient").show();
            $("#form-doctor").hide();
        }
    });

    $('#register_btn_user').click(function(){
        save_user();
    });

    $(".date_input").datepicker({ dateFormat: 'dd/mm/yy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun',
            'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Sábado', 'Domingo'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
        dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb']});


    /* chat */

    $("#start_chat_btn").click(function(){
        if($("#new_chat_name").val() == "Ingrese su nombre" || $("#new_chat_email").val() == "Ingrese su correo"){
            swal("No se puede inicar la conversación","Los campos no pueden estar vacíos.","error");
            return;
        }

        if(!validEmail($("#new_chat_email").val())){
            swal("No se puede inicar la conversación","El formato de correo electrónico es inconrrecto.","error");
            return;
        }

        window.open('/nuevo_chat?channel='+getChannel($("#new_chat_email").val())+'&email='+$("#new_chat_email").val()+'&name='+$("#new_chat_name").val()+'&role='+$("#new_chat_role").val(),
            'Nuevo chat',
            'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=400,height=580');
    });

    $(".features_item").click(function(){
        location.href = $(this).attr("href");
    });

});

function validEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function getChannel(str){
    str = str.replace(/@/g, "");
    str = str.replace(/\./g,"");
    str = str.replace(/ /g,"");
    return str;
}

function showMap(){
    $("#view_btn").attr("view","map");
    $("#view_btn").html("Ver Lista");

    $("#map_view").prop("checked",true);
    $("#doctors_holder").hide();
    $("#map_holder").show();
    refreshMap();
}

function openRegistrationModal(){
    $.each(fields,function(idx,id){
        $("#"+id).val("");
    });

    $("#doctor_comments").val("");

    $.each(fields_patient,function(idx,id){
        $("#"+id).val("");
    });

    $('input:radio[name="register_type"]')[0].checked = true;

    $('#form-patient').hide();
    $('#form-doctor').show();

    $("#register_doctor_btn").prop("disabled",false);

    $("#register_doctor_modal").modal('show');
}

function save_user(){

    if($('input[name="register_type"]:checked').val() == "doctor"){

        var validForm = true;
        $.each(fields,function(idx,id){
            if($("#"+id).val() == ""){
                validForm = false;
            }
        });

        if($("#doctor_city_ts_autocomplete").val() == ""){
            swal("No se puede continuar","Por favor seleccione una ciudad de la lista.","error");
            return;
        }

        if(validForm){
            $("#loading_image_email").show();
            $("#register_btn_user").prop("disabled",true);
            $.ajax({
                url:"/register_user",
                method:"POST",
                data:{
                    name:$("#doctor_name").val(),
                    last_name:$("#doctor_last_name").val(),
                    phone:$("#doctor_phone").val(),
                    email:$("#doctor_email").val(),
                    specialty:$("#doctor_specialty").val(),
                    city:$("#doctor_city_ts_autocomplete").val(),
                    comments:$("#doctor_comments").val()
                },
                dataType:"json",
                success:function(){
                    $("#loading_image_email").hide();
                    $("#register_btn_user").prop("disabled",false);
                    $("#register_doctor_modal").modal("hide");
                    swal("Solicitud de registro completada","En unas horas un miembro de nuestro Staff se pondrá en contacto con usted. ¡Gracias!","success");
                }
            });
        }else{
            swal("Formulario no válido","Por favor ingrese los datos correctamente.","error");
            $("#register_btn_user").prop("disabled",false);
        }
    }else{
        $("#form-patient").show();
        $("#form-doctor").hide();

        if($("#patient_city_ts_autocomplete").val() == ""){
            swal("No se puede continuar","Por favor seleccione una ciudad de la lista.","error");
            return;
        }

        $("#register_btn_user").prop("disabled",true);

        var result = validate_form_patient();

        if(result["valid"]){
            $("#loading_image_email").show();
            var params = {
                name:$("#patient_name").val(),
                last_name:$("#patient_last_name").val(),
                birth_date:$("#patient_birth_date").val(),
                phone:$("#patient_phone").val(),
                city_id:$("#patient_city_ts_autocomplete").val(),
                gender_id:$("#patient_gender").val(),
                user_attributes: { user_name:$("#patient_user").val(), password: $('#patient_password').val(),
                    email:$("#patient_email").val() }
            };

            $.ajax({
                url:"/register_patient",
                method:"POST",
                data:{
                    patient: params
                },
                dataType:"json",
                success:function(){
                    $("#loading_image_email").hide();
                    $("#register_btn_user").prop("disabled",false);
                    $("#register_doctor_modal").modal("hide");
                    swal("Solicitud de registro completada","¡Gracias! por registrase a MEDSI, ya puede acceder y disfrutar de todos los privilegios como nuestro usuario paciente","success");

                    setTimeout(function(){
                        location.href = "/login?user="+$("#patient_user").val();
                    },2000);

                }
            });
        }else{
            swal("Formulario no válido",result["message"],"error");
            $("#register_btn_user").prop("disabled",false);
        }
    }
}

var MESSAGE_ERROR_USER_PASSWORD = '';
function validate_form_patient(){
    var result = [];
    result["valid"] = true;
    // es formulario de medico
    var validForm = true;
    $.each(fields_patient,function(idx,id){
        if(id != "patient_phone"){
            if($("#"+id).val() == ""){
                validForm = false;
            }
        }
    });
    if(!validForm){
        result["valid"] = false;
        result["message"] = "Por favor ingrese los datos correctamente.";
    }else{
        if($('#patient_city_ts_autocomplete').val() == "undefined"){
            result["valid"] = false;
            result["message"] = "Seleccione una ciudad de la lista";
        }else{
            if(!isEmail($('#patient_email').val())){
                result["valid"] = false;
                result["message"] = "Correo no valido";
            }else{
                var pass = $('#patient_password').val();
                var pass_confirm = $('#patient_confirm_password').val();

                if(pass != pass_confirm) {
                    //validForm = false
                    // $('#patient_confirm_password').setCustomValidity("No coincide las contraseña");
                    result["valid"] = false;
                    result["message"] = "La contraseña no coincide";
                }else{
                    var check_user = check_user_exist($('#patient_user').val(),$('#patient_email').val());
                    if(check_user["valid"]){
                        result["valid"] = false;
                        result["message"] = check_user["message"];
                    }
                }
            }
        }
    }
    return result;
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function check_user_exist(user_name,email){
    var valid = [];
    valid["valid"] = false;
    $.ajax({
        url: '/check_user',
        dataType: "json",
        async:false,
        data: {user_name:user_name, email: email},
        success: function (data) {
            if(data.result){
                valid["valid"] =  true;
                valid["message"] =  data.message;
            }
        }
    });
    return valid;
}

function loadDoctors(specialty_id){
    clearMap();
    $("#specialties_list li").removeClass("active");
    $("#specialty_"+specialty_id).addClass("active");


    $("#doctors").html("<div style='text-align: center; font-size: 15px;'>Cargando ...</div>");

    var page_width = parseInt($('body').css('width').replace("px", ""));
    if(page_width < 973){
        goToByScroll("doctors_holder");
    }

    var name = $("#name_search").val();
    var city = $("#city_search_ts_autocomplete").val();
    if(name == "Búsqueda por nombre"){
        name = "";
    }

    $.ajax({
        url:"/search_results",
        data:{
            specialty_id:specialty_id,
            city_id: city,
            name:name
        },
        dataType:"json",
        success:function(data){
            var html = "";
            $.each(data,function(idx, item){
                var doctor = item.doctor;
                var offices = item.offices;
                var identity = item.identity_card;
                var city = item.city;

                var doctor_full_name = doctor.name+' '+doctor.last_name;
                var prefix = "";

                if(doctor.gender_id == 1){
                    prefix = "Dr."
                }else{
                    prefix = "Dra. "
                }

                doctor_full_name = prefix + ' ' + doctor_full_name;

                html += "<div class='col-md-3 col-xs-6'>";
                html += "<div class='doctor_info_holder'>"
                html += "<strong>"+doctor_full_name+"</strong><br>"
                html += "Cédula Pofesional: "+identity.identity+"<br>"+identity.institute+"<br>"
                html += city+"<br><br>";
                html += "<div class='open_info_btn_holder'><a onclick='openDoctorInfoModal("+doctor.id+")' class='btn btn-primary btn-xs waves-effect'>Ver detalles</a></div>";
                html += "</div>";
                html += "<hr>";
                html += "</div>";

                $.each(offices,function(idx,item){
                    var data = {
                        lat:parseFloat(item.lat),
                        lng:parseFloat(item.lng),
                        doctor_id:doctor.id,
                        doctor_full_name:doctor_full_name,
                        address:item.address,
                        tel:item.telephone,
                        name:item.name,
                        city:city,
                        hospital:item.hospital
                    };
                    addMarker(data);
                });


            });

            $("#doctors").html(html);
        }
    });

}


function clear_accent(text_string){
    var text = text_string.toLowerCase(); // a minusculas
    text = text.replace(/[áàäâå]/, 'a');
    text = text.replace(/[éèëê]/, 'e');
    text = text.replace(/[íìïî]/, 'i');
    text = text.replace(/[óòöô]/, 'o');
    text = text.replace(/[úùüû]/, 'u');
    text = text.replace(/[ýÿ]/, 'y');
    text = text.replace(/[ñ]/, 'n');
    text = text.replace(/[ç]/, 'c');
    text = text.replace(/['"]/, '');
    //text = text.replace(/[^a-zA-Z0-9-]/, '');
    /*text = text.replace(/\s+/, '-');
     text = text.replace(/' '/, '-');
     text = text.replace(/(_)$/, '');
     text = text.replace(/^(_)/, '');*/
    return text;
}



function openDoctorInfoModal(doctor_id){

    $("#show_in_map_btn").attr("onclick","showDoctorInMap("+doctor_id+")");

    $.ajax({
        url:"/get_doctor_info",
        dataType:"json",
        data:{id:doctor_id},
        success:function(data){
            var doctor = data.doctor;
            var html = "";

            var doctor_full_name = doctor.name+' '+doctor.last_name;
            var prefix = "";

            if(doctor.gender == "M"){
                prefix = "Dra."
            }else{
                prefix = "Dr. "
            }

            doctor_full_name = prefix + ' ' + doctor_full_name;

            $("#doctor_info_title").html(doctor_full_name);

            html += "<div class='row'>";
            var class_icard = 'col-md-12';
            if(data.identity_cards.length > 1){
                class_icard = 'col-md-6';
            }
            $.each(data.identity_cards,function (idx , identity){
                html += '<div class="'+class_icard+'" style="text-align: center;">';
                html += "<strong>"+identity.specialty+"</strong><br>";
                html += "Ced. Prof. : "+identity.identity+"<br>";
                html += identity.institute+"<br><br>";
                html += '</div>';
            });
            html += "</div>";
            html += "<div style='border-bottom: 1px #9C9C9C solid; '></div><br>";

            var class_o = 'col-md-12';
            html += '<div class="row">';
            $.each(data.offices,function(idx, office){
                var html_times = '<br>';

                var last_day = '';
                var first_time = true;
                $.each(office.times,function(idx,time){
                    if(last_day != time.day && !first_time){
                        html_times += "<br></div>";
                    }
                    if(last_day != time.day){
                        html_times += "<div class='col-md-4'>";
                        html_times += "<strong>"+time.day+"</strong><br>";
                    }
                    html_times += time.start+' hrs - '+time.end+" hrs<br>";
                    last_day = time.day;
                    first_time = false;
                });

                html_times += "<br></div>";

                html += '<div class="'+class_o+'" style="text-align: center;">';
                html += "<strong>"+office.name+" ("+office.hospital+")</strong>";
                html += "<div>";
                html += office.address+"<br>"+office.city;
                html += "</div>";
                html += "<div><i class='zmdi zmdi-phone-in-talk zmdi-hc-fw'></i> "+office.telephone+"</div>";
                html += html_times;
                html += "<hr>";
                html += "</div>";
            });
            html += '</div>';
            $("#doctor_info").html(html);

        }
    });

    $("#doctor_info_modal").modal("show");
}

function openPDFModal(){
    $("#medsi_pdf_info").modal("show");
    var height = $( window ).height();
    height -= 150;
    $("#pdf_hodler").css("height",height+"px");
}

function closePDFModal(){
    $("#medsi_pdf_info").modal("hide");
}