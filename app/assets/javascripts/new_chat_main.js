//= require index_page/jquery-1.11.1.min
//= require index_page/bootstrap.min
//= require private_pub

function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
$(document).on("keydown", disableF5);

$(document).ready(function(){
    var channel = "/messages/private/" + $("#channel").val();

    PrivatePub.subscribe("/messages/public", function(data) {
    });

    PrivatePub.subscribe(channel, function(data) {
        var message_class = "";
        if(data.sender == $("#me").val()){
            message_class = "me"
        }else{
            message_class = "you";
        }
        $("#user_chat_content").append('<div class="bubble '+message_class+'">' +
            '<strong>'+data.sender+':</strong><br>' +
            data.message +
            '</div>');

        $("#user_chat_content").scrollTop($("#user_chat_content")[0].scrollHeight);
    });

});

$(function () {
    $("#send_message_btn").click(function(){
        sendMessage();
    });

    $("#close_chat_btn").click(function () {
        $("#chat_message").val("******** Ha abandonado la conversación ********");
        sendMessage();
        window.close();
    });

    $("#chat_message").keyup(function(e){
        if(e.keyCode == 13){
            sendMessage();
        }
    });
    $(window).on('beforeunload', function(){
        return '¿Confirma que desea salir?';
    });
});

function sendMessage(){
    if($("#chat_message").val() == ""){
        return;
    }
    $.ajax({
        url:$("#send_message_url").val(),
        data:{
            sender:$("#me").val(),
            channel:$("#channel").val(),
            message:$("#chat_message").val()
        },
        dataType:"json",
        success:function (data) {
        }
    });
    $("#chat_message").val("");
    $("#chat_message").focus();
}