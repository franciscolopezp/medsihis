var map = null;
var markers = [];
var infoWindows = [];
var offices_array = [];
var init_pos = null;


function locatePosition(){
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 0, lng: 0},
        zoom: 13
    });

    infoWindow = new google.maps.InfoWindow({map: map});

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            init_pos = pos;

            infoWindow.setPosition(pos);
            infoWindow.setContent('<strong>Usted se encuentra aquí.</strong>');
            map.setCenter(pos);

        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
}

function addMarker(data){
    var marker = new google.maps.Marker({
        icon:"http://medsi.com.mx/30X39.png",
        map: map,
        //draggable: true,
        //animation: google.maps.Animation.DROP,
        position: {lat: data.lat, lng: data.lng}
    });

    var infoWindow = new google.maps.InfoWindow({map: map});
    //marker.addListener('click', toggleBounce);

    var html = '<strong>'+data.doctor_full_name+'</strong><br><label>'+data.name+' ('+data.hospital+')</label><br>';
    html += '<div>';
    html += data.address+'<br>'+data.city+'<br>Teléfono: '+data.tel;
    html += '</div>';
    html += '<div class="view_details_map_btn_holder">';
    html += "<a onclick='openDoctorInfoModal("+data.doctor_id+")' class='btn btn-primary btn-xs waves-effect'>Ver detalles</a>";
    html += '</div>';

    infoWindow.setContent(html);
    infoWindow.close();

    google.maps.event.addListener(marker, "click", function() {
        closeInfoWindows();
        infoWindow.open(map, marker);
    });

    markers.push(marker);
    infoWindows.push(infoWindow);
    offices_array.push({
        doctor_id:data.doctor_id
    });
}

function closeInfoWindows(){
    $.each(infoWindows,function(idx,item){
        item.close();
    });
}

function clearMap(){
    offices_array = [];
    $.each(infoWindows,function(idx,item){
        item.close();
        item = null;
    });
    infoWindows = [];

    $.each(markers,function(idx,item){
        item.setMap(null);
    });
    markers = [];
}

function refreshMap(){
    var center = map.getCenter();
    map.setCenter(center);
    google.maps.event.trigger(map, 'resize');
}

function showDoctorInMap(doc_id){
    showMap(); // index.js
    $.each(offices_array,function(idx,item){
        if(item.doctor_id == doc_id){
            infoWindows[idx].open(map,markers[idx]);
        }else{
            infoWindows[idx].close();
        }
    });
}
