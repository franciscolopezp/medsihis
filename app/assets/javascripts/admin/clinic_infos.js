
jQuery(document).ready(function() {
    $("#new_emisor").on("submit", function(e) {
        var data = new FormData(this);
        $.ajax({
            url: $(this).attr('action'),
            data: data,
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: false,
            success: function(response) {
                if(response.done){
                    swal("Exito!", "Datos guardados con exito", "sucess");
                    location.href = response.url
                }else{
                    swal("Error!", response.msg, "error");
                }
            },
            error: function(xhr, textStatus, errorThrown) {}
        });
        e.preventDefault(); //THIS IS VERY IMPORTANT
    });
    $('#change_pass').change(function(){
        if($(this).is(':checked')){
            $('#change_fiscal_datas').show();
            $('#admin_doctor_fiscal_information_certificate_password').val("");
            $('#actual_fiscal_datas').hide();
            $('#full_checkbox').hide();
        }else{
            $('#change_fiscal_datas').hide();
            $('#actual_fiscal_datas').show();
        }
    });
});
function clearNewCurrency(){
    $("#currency_exchange_value").val("");
    $("#current_exchange_edit_id").val("");
    $("#new_currency_id").val(1);
    $("#new_currency_id").selectpicker('refresh');
    $('#currency_default').prop('checked', false);
}
function editCurrencyExchange(ce_id,value,is_default,currency){
    clearNewCurrency();
    $("#currency_exchange_value").val(value);
    $("#current_exchange_edit_id").val(ce_id);
    if(is_default){
        $('#currency_default').prop('checked', true);
    }
    $("#new_currency_id").val(currency);
    $("#new_currency_id").selectpicker('refresh');
    $("#add_currency_modal").modal('show');
}
function deleteCurrency(cuex_id){
    var url = $("#url_delete_currency_exchange").val();
    var method = "GET";
    var data = {current_exchange_id:cuex_id};

    swal({
            title: "Eliminar tipo de cambio",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url:url,
                method:method,
                data:data,
                dataType:"json",
                success:function(response){
                    location.reload();
                }
            });

        });


}
function saveCurrencyExchange(){
    var url = $("#url_new_currency_exchange").val();
    var method = "GET";
    var is_default_currency = 0;

    if($("#currency_default").is(':checked')){
        is_default_currency = 1;
    }
    if($("#currency_exchange_value").val() == ""){
        swal("Por favor ingrese un valor a la moneda.");
        return;
    }
    var data = {value:$("#currency_exchange_value").val(),
        currency_id:$("#new_currency_id").val(),
        is_default:is_default_currency,
        edit_id:$("#current_exchange_edit_id").val()};

    $("btn_new_currency_exchange").prop( "disabled", true );
    $.ajax({
        url:url,
        method:method,
        data:data,
        dataType:"json",
        success:function(response){
            console.log(response);
            if(response.done){
                location.reload();
            }else{
                swal("Error!", "La moneda ya se encuentra agregada.", "error");
            }

        }
    });
}
function clear_modal(){
    $("#admin_doctor_fiscal_information_rfc").val("");
    $("#admin_doctor_fiscal_information_bussiness_name").val("");
    $("#admin_doctor_fiscal_information_fiscal_address").val("");
    $("#admin_doctor_fiscal_information_int_number").val("");
    $("#admin_doctor_fiscal_information_ext_number").val("");
    $("#admin_doctor_fiscal_information_suburb").val("");
    $("#admin_doctor_fiscal_information_locality").val("");
    $("#admin_doctor_fiscal_information_zip").val("");
    $("#admin_doctor_fiscal_information_city_id").val("");
    $("#admin_doctor_fiscal_information_serie").val("");
    $("#admin_doctor_fiscal_information_folio").val("");
    $("#admin_doctor_fiscal_information_tax_regime").val("");
    $("#admin_doctor_fiscal_information_iva").val("");
    $("#is_edit").val(0);
    $("#emisor_id").val(0);
    $("#admin_doctor_fiscal_information_city_id_ts_autocomplete").val("");
}
function enable_fields(){
    $('#admin_doctor_fiscal_information_rfc').prop('disabled', false);
    $('#admin_doctor_fiscal_information_bussiness_name').prop('disabled', false);
    $('#admin_doctor_fiscal_information_fiscal_address').prop('disabled', false);
    $('#admin_doctor_fiscal_information_int_number').prop('disabled', false);
    $('#admin_doctor_fiscal_information_ext_number').prop('disabled', false);
    $('#admin_doctor_fiscal_information_suburb').prop('disabled', false);
    $('#admin_doctor_fiscal_information_locality').prop('disabled', false);
    $('#admin_doctor_fiscal_information_zip').prop('disabled', false);
    $('#admin_doctor_fiscal_information_city_id').prop('disabled', false);
    $('#admin_doctor_fiscal_information_serie').prop('disabled', false);
    $('#admin_doctor_fiscal_information_folio').prop('disabled', false);
    $('#admin_doctor_fiscal_information_tax_regime').prop('disabled', false);
    $('#admin_doctor_fiscal_information_iva').prop('disabled', false);

}
function edit_emisor_info(id){
    $.ajax({
        url: $("#url_get_emisor_info").val(),
        dataType: "json",
        data: {
            id: id
        },
        success: function (data) {
            if (data.done) {
                console.log(data.data);
                $("#admin_doctor_fiscal_information_rfc").val(data.data.rfc);
                $("#admin_doctor_fiscal_information_bussiness_name").val(data.data.bussiness_name);
                $("#admin_doctor_fiscal_information_fiscal_address").val(data.data.fiscal_address);
                $("#admin_doctor_fiscal_information_int_number").val(data.data.int_number);
                $("#admin_doctor_fiscal_information_ext_number").val(data.data.ext_number);
                $("#admin_doctor_fiscal_information_suburb").val(data.data.suburb);
                $("#admin_doctor_fiscal_information_locality").val(data.data.locality);
                $("#admin_doctor_fiscal_information_zip").val(data.data.zip);
                $("#admin_doctor_fiscal_information_city_id").val(data.city);
                $("#admin_doctor_fiscal_information_city_id_ts_autocomplete").val(data.data.city_id);
                $("#admin_doctor_fiscal_information_serie").val(data.data.serie);
                $("#admin_doctor_fiscal_information_folio").val(data.data.folio);
                $("#admin_doctor_fiscal_information_tax_regime").val(data.data.tax_regime);
                $("#admin_doctor_fiscal_information_iva").val(data.data.iva);
                $("#is_edit").val(1);
                $("#emisor_id").val(id);
            } else {
                swal("Error", "Ocurrio un error al obtener la información fiscal", "error");
            }
        },
        error: function (data) {
            console.log("error controller");
        }
    });
}
function delete_emisor_info(id){
    swal({
            title: "¿Esta seguro que desea eliminar la información fiscal?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(isConfirm){
                $.ajax({
                    url: $("#url_delete_emisor_info").val(),
                    dataType: "json",
                    data: {
                        id: id
                    },
                    success: function (data) {
                        if (data.done) {
                            swal("Exito", "Información eliminada", "success");
                            location.reload();
                        } else {
                            swal("Error", "Ocurrio un error al eliminar la información", "error");
                        }
                    },
                    error: function (data) {
                        console.log("error controller");
                    }
                });
        });
}