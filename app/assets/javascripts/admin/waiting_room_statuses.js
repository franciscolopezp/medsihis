$(function () {
    $( "#waiting_room_statuses" ).sortable({
        handle: ".move_status_icon",
        stop:function () {
            var url = $("#waiting_room_statuses").attr("url");
            var ids = [];
            $.each($(".waiting_room_status_item"),function (idx, item) {
                ids.push($(item).attr("waiting_room_status_id"));
            });

            $.post(url,{ids:ids},function (data) {
                console.log(data);
            },"json");
        }
    });

    $("#new_waiting_room_status_btn").click(function () {
        $("#waiting_room_status_id").val("");
        $("#waiting_room_status_name").val("");
        $("#waiting_room_status_description").val("");
        $(".waiting_room_activity").prop("checked",false);
        $("#waiting_room_status_form").slideDown();
    });

    $("#cancel_waiting_room_btn").click(function () {
        $("#waiting_room_status_form").slideUp();
    });

    $(".edit_icon").click(function () {
        $("#waiting_room_status_form").slideDown();
        var url = $(this).attr("url");
        $.get(url,function (data) {
            $("#waiting_room_status_id").val(data.data.id);
            $("#waiting_room_status_name").val(data.data.name);
            $("#waiting_room_status_description").val(data.data.description);
            $(".waiting_room_activity").prop("checked",false);
            $.each(data.activities,function (idx, item) {
                $("#waiting_room_activity_"+item).prop("checked",true);
            });
        },"json");
    });

    $("#save_waiting_room_btn").click(function () {
        var url = $(this).attr("url");
        var method = "POST";
        var waiting_room = {
            name: $("#waiting_room_status_name").val(),
            description: $("#waiting_room_status_description").val()
        };

        if(waiting_room.name == "" || waiting_room.description == ""){
            swal("No se puede agregar el registro","Por favor complete los campos.","error");
            return;
        }

        if($("#waiting_room_status_id").val() != ""){
            url += "/"+ $("#waiting_room_status_id").val();
            method = "PUT";
        }

        var ids = $("input[name=waiting_room_activities]:checked").map(function(){
            return $(this).val();
        }).get();

        $.ajax({
            url: url,
            method: method,
            data:{
                activities:ids,
                waiting_room_status: waiting_room
            },
            dataType:"json",
            success:function (data) {
                if(data.done){
                    location.reload();
                }
            }
        });
    });
});