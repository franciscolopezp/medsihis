jQuery(document).ready(function() {
    var html = '<div class="col-md-offset-3 col-md-9">'+
        '<div class="preloader pl-xxl">'+
        '<svg class="pl-circular" viewBox="25 25 50 50">'+
        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
        '</svg>'+
        'Migrando...espere...'+
        '</div>'+
        '</div>';

    $('#loading_image_pacientes').append(html);
    $('#loading_image_antecedentes').append(html);
    $('#loading_image_consultas').append(html);
    $('#loading_image_embarazos').append(html);
    $('#loading_image_import_user').append(html)

    $('#loading_image_pacientes').hide();
    $('#loading_image_import_user').hide();
    $('#loading_image_antecedentes').hide();
    $('#loading_image_consultas').hide();
    $('#loading_image_embarazos').hide();
});
function showPregnantLoading(){
    $('#loading_image_embarazos').show();
}
function showConsultsLoading(){
    $('#loading_image_consultas').show();
}
function showUsersLoading(){
    $('#loading_image_import_user').show();
}
function backroundsLoading(){
    $('#loading_image_antecedentes').show();
}
function patientsLoading(){
    $('#loading_image_pacientes').show();
}

function import_patients(){
    console.log("importar");
    console.log($("#upload_file").val());
    $.ajax({
        type: "put",
        url: $("#url_migrate_patients").val(),
        data: {doctor_id:$("#doctor_for_migrate").val(),
                file_root:$("#cvs_root").val()},
        success: function (response) {
            if(response.result = "true") {
                swal("Exito!", "Se importaron " + response.total + " pacientes", "success");
            }else{
                swal("Error!", "Ocurrio un error al importar, se importaron " + response.total +" pacientes", "error");
            }
        },
        error : function (response) {
            console.log("Error al importar")
        }
    });
}