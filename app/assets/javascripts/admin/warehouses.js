var FIXED_ASSET_ITEM_ID = 0;
jQuery(document).ready(function() {
    $("#warehouses_div").hide();
    $("#user_name").ts_autocomplete({
        url:AUTOCOMPLETE_PERSONS_URL,
        items:10,
        callback:function(val, text){
            resetChecks();
            loadWarehouses(val);
        }
    });
    var table = $("#data-table-warehouses");
    var grid = $("#data-table-warehouses").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-warehouses').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }

    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el almacen  "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El almacen ha sido eliminado exitosamente.", "success");
                        $('#data-table-warehouses').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

});
function resetChecks() {
    $.each($(".s_parent"),function(idx, item){
        if($(item).is(":checked")){
            $(item).prop('checked', false);
        }
    });
}
function loadWarehouses(user){
    $.ajax({
        url: $("#url_get_user_warehouses").val(),
        dataType: "json",
        data: {user_id:user},
        success: function (data) {
            $("#warehouses_div").show();
            $.each(data, function (idx, info) {
                $("#"+info.name).prop('checked', true);
            });

        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function saveWarehouses(){
    var warehouses = [];
    $.each($(".s_parent"),function(idx, item){
        if($(item).is(":checked")){
            warehouses.push({
                warehouse_id:$(item).val()
            });
        }
    });
    if (warehouses.length == 0){
        swal("Error!", "Debe seleccionar un usuario y los almacenes a los cual tendra acceso.", "error");
        return;
    }
    $.ajax({
        url: $("#url_save_user_warehouses").val(),
        dataType: "json",
        data: {user_id:$("#user_name_ts_autocomplete").val(),warehouses:warehouses},
        success: function (data) {
            swal("Exito!", "Lo(s) almacen(es) han sido guardado.", "success");
        },
        error:function(data){
            console.log("error controller");
        }
    });
}