jQuery(document).ready(function() {
    var table = $("#data-table-medical-annotation-types");
    var grid = $("#data-table-medical-annotation-types").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $("#data-table-medical-annotation-types").data("source"),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El registro ha sido eliminado exitosamente.", "success");
                        location.reload();
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });
                });
        });
    });

    initHtmlEditor("#template",{
        height:600
    });

    if($("#medical_annotation_type_template")[0]){
        $("#template").summernote().code($("#medical_annotation_type_template").val());
    }

    setupAddFieldModal();
});

var new_fields = [];
$(function () {

    $("#field_html_type").change(setupAddFieldModal);

    $("#add_field_btn").click(function(){
        $("#field_label").val("");
        $("#field_code").val("");
        $("#add_field_modal").modal("show");
    });

    $("#add_linked_field_btn").click(function () {
        $("#add_linked_field_modal").modal("show");
    });

    $("#save_linked_fields_btn").click(function () {
        var url = $(this).attr("url");

        var linked_fields = [];
        $('input[name="linked_field_check"]:checked').each(function() {
            linked_fields.push(this.value);
        });

        $.post(url,{
            linked_fields: linked_fields
        },function (data) {

        },"json");
    });

    $("#add_new_field").click(function(){
        var html_type = $("#field_html_type").val();

        if($("#field_label").val() == "" || $("#field_code").val() == ""){
            swal("No es posible agregar!","Por favor llene correctamente la configuración de los campos.","error");
            return;
        }

        var field_conf = {
            label: $("#field_label").val(),
            code: $("#field_code").val(),
            html_type: html_type
        };
        switch (html_type){
            case "date":
                field_conf.date_format = $("#field_date_format").val();
                break;
            case "checkbox":
                field_conf.options = $("#field_checkbox_values").val();
                break;
        }

        $("#add_field_modal").modal("hide");

        new_fields.push(field_conf);

        $("#template_fields_holder").append("<div>" +
            "<strong>"+field_conf.label+"</strong>" +
            "<br>["+ field_conf.code +"]"+
            "</div>");
    });

    $("#add_image_btn").click(function(){
        $("#add_annotation_image").modal("show");
    });

    $("#save_medical_image_btn").click(function(){
        $("#new_medical_annotation_image").submit();
    });

    $("#show_linked_fields_btn").click(function () {
        $("#linkable_fields_holder").toggle();
    });

    $("#save_medical_annotation_type_btn").click(function(){
        var invalid_form = false;

        if($("#medical_annotation_type_name").val() == ""){
            invalid_form = true;
        }

        if(invalid_form){
            swal("No se puede guardar!","Por favor llene correctamente la configuración de los campos.","error");
            return;
        }

        var method = "POST";
        var url = $("#medical_annotation_types_url").val();

        if($("#medical_annotation_type_id").val() != ""){
            method = "PUT";
            url += "/"+$("#medical_annotation_type_id").val();
        }

        var formData = new FormData();
        if(new_fields.length > 0){
            formData.append('fields', JSON.stringify(new_fields));
        }

        formData.append('medical_annotation_type[template]', $("#template").summernote().code());
        formData.append('medical_annotation_type[name]', $("#medical_annotation_type_name").val());
        formData.append('medical_annotation_type[require_upload]', $("#require_upload").is(":checked"));
        formData.append('medical_annotation_type[add_valoration]',$("#add_valoration").is(":checked"));
        if($('#medical_annotation_type_template_file')[0].files[0]){
            formData.append('medical_annotation_type[template_file]', $('#medical_annotation_type_template_file')[0].files[0]);
        }
        $.ajax({
            url:url,
            method:method,
            data:formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success:function(data){
                if(data.done){
                    location.href = $("#medical_annotation_types_url").val();
                }
            }
        });

    });
    $(".delete_field").click(function(){
        var url = $(this).attr("url");
        var name = $(this).attr("name");
        var id = $(this).attr("field_id");

        deleteRecord(name,url,"",function () {
            $("#field_"+id).remove();
        });
    });
});

function setupAddFieldModal() {
    $(".date_config").hide();
    $(".checkbox_config").hide();
    $(".checkbox_config").hide();
    var html_type = $("#field_html_type").val();
    switch (html_type){
        case "date":
            $(".date_config").show();
            break;
        case "checkbox":
            $(".checkbox_config").show();
            break;
    }
}