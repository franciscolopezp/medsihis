jQuery(document).ready(function() {
    var table = $("#data-table-role");
    var grid = $("#data-table-role").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-role').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>" +
                    "<a href=" + row.url_permission + "  class=\"btn bgm-blue btn-icon waves-effect waves-circle waves-float btn-manage\" data-row-id=\"" + row.id + "\" title=\"Permisos\"><i class=\"zmdi zmdi-key zmdi-hc-fw\"></i></a>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar la rol "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El rol ha sido eliminado exitosamente.", "success");
                        $("#data-table-role").bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });
        $(".s_parent").change(function(){
            check_all_checks($(this));
        });


    if($("#manage_role_id")[0]){
        loadPermissions($("#manage_role_id").val());
    }
});
function check_all_checks(check){

   if(check.is(":checked")){
       $('.check_child_'+check.val()).prop('checked', true);
   }else{
       $('.check_child_'+check.val()).prop('checked', false);
   }
}
function savePermissions(id){
    var permissions = [];
    $.each($(".s_parent"),function(idx, item){
            var childs = [];
            $.each($("#actions_"+$(item).val()+" .action"),function(index, child){
                if($(child).is(":checked")){
                    childs.push({action_id:$(child).val()});
                }
            });

            permissions.push({
                section_id:$(item).val(),
                actions:childs
            });
    });
    $.ajax({
        url: $("#url_save_role_permissions").val(),
        dataType: "json",
        type:"post",
        data: {role_id:id,permissions:permissions},
        success: function (data) {
            location.reload();

        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function loadPermissions(id){
    $.ajax({
        url: $("#url_get_role_permission").val(),
        dataType: "json",
        data: {role_id:id},
        success: function (data) {
            $.each(data, function (idx, info) {
                $("#"+info.name).prop('checked', true);
            });

        },
        error:function(data){
            console.log("error controller");
        }
    });
}