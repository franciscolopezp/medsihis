var map;
var infoWindow;
var marker = null;
var init_lat = 0;
var init_lng = 0;

var lat_input;
var long_input;


function initMap() {
    lat_input = $("#clinic_info_latitude");
    long_input = $("#clinic_info_longitude");

    var current_lat = lat_input.val();
    var current_lng = long_input.val();

    if(current_lat != "" && current_lng != ""){
        initMapUI(parseFloat(current_lat),parseFloat(current_lng));
        init_lat = parseFloat(current_lat);
        init_lng = parseFloat(current_lng);
    }else{
        locatePosition();
    }

    addListenerToMap();

}

function locatePosition(){
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 0, lng: 0},
        zoom: 15
    });
    infoWindow = new google.maps.InfoWindow({map: map});

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('<strong>Se guardará como la ubicación.</strong>');
            map.setCenter(pos);

            lat_input.val(pos.lat);
            long_input.val(pos.lng);

            init_lat = pos.lat;
            init_lng = pos.lng;

        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}




function initMapUI(lat,lng){
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        zoom: 15
    });
    infoWindow = new google.maps.InfoWindow({map: map});

    var pos = {
        lat: lat,
        lng: lng
    };

    infoWindow.setPosition(pos);
    infoWindow.setContent('<strong>'+$("#clinic_info_name").val()+' se encuentra aquí.</strong>');
    map.setCenter(pos);
}

function addListenerToMap(){
    google.maps.event.addListener(map, "click", function(event) {
        removeMarker();

        marker = new google.maps.Marker({
            position: event.latLng,
            map: map
        });

        var click_message = '<strong>Se guardará como nueva ubicación de: '+$("#clinic_info_name").val()+'.</strong>';

        infoWindow.setContent(click_message);
        infoWindow.open(map, marker);

        google.maps.event.addListener(marker, "click", function() {
            infoWindow.setContent(click_message);
            infoWindow.open(map, marker);
        });

        var n_lat = marker.getPosition().lat();
        var n_lng = marker.getPosition().lng();

        lat_input.val(n_lat);
        long_input.val(n_lng);

    });
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
}

function removeMarker(){
    if(marker != null){
        marker.setMap(null);
    }

    infoWindow.close();
}