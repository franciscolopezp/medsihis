jQuery(document).ready(function() {

    $('form#new_assistant').find("input[type=text], textarea").val("");
    $("#new_assistant").validate();


    var grid = $("#data-table-assistant").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-assistant').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-edit\" title=\"Editar\"></i></a>" +
                    "<a href='#' onclick='show_assistant("+row.id+")' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var last_name = $tr_parent.children('td').eq(2).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar al asistente "+name+" "+last_name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Eliminar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El asistente ha sido eliminado exitosamente.", "success");
                        $('#data-table-assistant').bootgrid('reload');
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

    $('#change_pass').change(function(){
        if($(this).is(':checked')){
            $('#modificate_pass').show();
        }else{
            $('#modificate_pass').hide();
        }
    });

});


$(function(){
    $("#change_permissions").change(function(){
        if($(this).is(":checked")){
            $(".assistant_permission").prop("disabled",false);
        }else{
            $(".assistant_permission").prop("disabled",true);
        }
    });

    $("#load_offices").click(function(){

        var url = $('#url_get_doctor_offices').val();

        if($('#select_doctor_add_office_ts_autocomplete').val() != ""){
            var offices_option = "";
            $.ajax({
                url: url,
                dataType: "json",
                data: {doctor_id:$('#select_doctor_add_office_ts_autocomplete').val()},
                success: function (data) {
                    $.each(data.offices, function (idx, info) {
                        offices_option += "<option value = "+info.id+">"+info.name+"</option>";
                    });
                    $('#doctor_offices').html(offices_option);
                    $('#doctor_offices').selectpicker('refresh');

                },
                error:function(data){
                }
            });
        }else{
            $('#doctor_offices').html("");
            $('#doctor_offices').selectpicker('refresh');
        }

    });
});


function show_assistant(assistant_id){
    url = $('#url_show_assistant').val();
    $.ajax({
        url: url,
        dataType: "json",
        data: {assistant_id:assistant_id},
        success: function (data) {
            $.each(data.assist, function (idx, info) {
                $("#a_name").text(info.name);
                $("#a_last_name").text(info.last_name);
                $("#a_telephone").text(info.telephone);
                $("#a_cellphone").text(info.cellphone);
                $("#a_email").text(info.email);
                $("#a_birth_day").text(info.birth_day);
                $("#a_address").text(info.address);
                $("#a_city").text(info.city);
            });
            var tabla_per = "<table id='tablaper' class='table table-striped'><thead><tr>"+
                "<th>NOMBRE</th>"+
                "</tr></thead><tbody><tr>";
            if(data.docs.length > 0) {
                $.each(data.docs, function (idx, info) {
                    tabla_per += "<tr><td >" + info.name + " " + info.last_name + "</td ></tr>";
                });
            }else{
                tabla_per += "<tr><td >No existen doctores asignados al asistente</td ></tr>";
            }
            tabla_per += "</tr></tbody></table>";
            $('#table_permissions').html(tabla_per);

        },
        error:function(data){
        }
    });
    $('#show_assistant_modal').modal('show');
}

function checkuser_exist(){
    var  editValue = $('#edit_assistant').val();
    var  new_user_name = $('#assistant_user_attributes_user_name').val();
    var valid = true;
    $.ajax({
        url: $('#url_check_user').val(),
        dataType: "json",
        async:false,
        data: {user_name:new_user_name,edit_value:editValue},
        success: function (data) {
            if(data.result){
                swal("Error!", "El nombre de usuario ya existe.", "error");
                valid =  false;
            }

        }
    });
    return valid;

}

function addDoctorAssistant(){
    var params = {doctor_id:$('#select_doctor_add_office_ts_autocomplete').val(),
                    assistant_id:$('#assistant_id').val(),
                    office_id: $('#doctor_offices').val()
    };

    var url = $('#url_addDoctor_assistant').val()+"/";
    var method = "get";


    $.ajax({
        url:url,
        method:method,
        data:params,
        dataType:"json",
        success:function(data){
            if(data.respond){
                $("#addDoctorModal").modal("hide");
                location.reload();
            }else{
                swal("Error!", "El doctor ya se encuentra agregado.", "error");
            }

        }
    });
}
function deleteDoctor(doctor_id,doctor_name){
    var params = {doctor_id:doctor_id,
        assistant_id:$('#assistant_id').val()
    };

    var url = $('#url_deleteDoctor_assistant').val()+"/";
    var method = "get";

    swal({
            title: "¿Desea remover al doctor " + doctor_name + "?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Eliminar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url:url,
                method:method,
                data:params,
                dataType:"json",
                success:function(data){
                    location.reload();
                }
            });

        });

}