var DELETE_CLINICAL_STUDY_ID = 0;
var DELETE_INDICATION_ID = 0;

jQuery(document).ready(function() {

    var grid = $("#data-table-clinicalstudy").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function()
        {
           var laboratory = $('#lab_id').val();
            return $('#data-table-clinicalstudy').data('source')+"?laboratory="+laboratory;
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el estudio clínico "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El estudio clínico ha sido eliminado exitosamente.", "success");
                        location.reload();
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

    initHtmlEditor("#study_template",{
        height:600
    });

    $("#study_template").summernote().code($("#template_html").val());

});

$(function(){

    $("#add_template_btn").click(function(){
        $("#study_template_holder").show();
        $("#has_template").val(1);
    });

    $("#new_clinical_study").submit(function(event){
        $("#template_html").val($("#study_template").summernote().code());
        $("#new_clinical_study").submit();
        event.preventDefault();
    });

    $("form.edit_clinical_study").submit(function(event){
        $("#template_html").val($("#study_template").summernote().code());
        $("form.edit_clinical_study").submit();
        event.preventDefault();
    });

});