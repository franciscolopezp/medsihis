var LOT_ID = 0;
jQuery(document).ready(function() {
    var table = $("#data-table-lots");
    var grid = $("#data-table-lots").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-lots').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a onclick='editExpirationDate("+row.id+")' class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar la categoria de activos fijos "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "La categoria de activo fijo ha sido eliminado exitosamente.", "success");
                        $('#data-table-lots').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

});
function editExpirationDate(id){
    LOT_ID = id;
    $("#expiration_date").val("");
    $("#editExpirationDateModal").modal("show");

}
function saveExpirationDate(){
    var url = $("#url_edit_expiration_date").val();
    var method = "GET";

    var params = {
        lot_id:LOT_ID,
        expiration_date:$("#expiration_date").val()
    };
    if($("#expiration_date").val() != "") {
        $.ajax({
            url: url,
            method: method,
            dataType: "json",
            data: params,
            success: function (response) {
                $("#editExpirationDateModal").modal("hide");
                swal("success", "La fecha de caducidad ha sido actualizada", "success");
                $('#data-table-lots').bootgrid("reload");
            }
        });
    }else{
        swal("error","Debe ingresar una fecha", "error");
    }
}