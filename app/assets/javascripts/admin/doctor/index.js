jQuery(document).ready(function() {

    var grid = $("#data-table-doctor").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-doctor').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.id + "\/edit\ class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<a href=" + row.id + "\ class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\"></i></a>" +
                    "<a data-confirm=\"Eliminar?\" rel=\"nofollow\" data-method=\"delete\"  href=" + row.id + "\ class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\"" + row.id + "\" ><i class=\"zmdi zmdi-delete\"></i></a>";
            }
        }
    })



});
