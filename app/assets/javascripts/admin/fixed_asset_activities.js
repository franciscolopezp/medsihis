jQuery(document).ready(function() {

    var table = $("#data-table-fixed_asset_activities");
    var grid = $("#data-table-fixed_asset_activities").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-fixed_asset_activities').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar la actividad del activo fijo "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "La actividad del activo fijo ha sido eliminado exitosamente.", "success");
                        $('#data-table-fixed_asset_activities').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

        $('input.color').colorPicker({
            customBG: '#222',
            readOnly: true,
            init: function(elm, colors) { // colors is a different instance (not connected to colorPicker)
            }
        }).each(function(idx, elm) {
            // $(elm).css({'background-color': this.value})
        });

        $("#fixed_asset_activity_name").click(function(){
            console.log($("#colorcolor").val());
        });
});

function check_color(id){
    var response = false;
    if($("#fixed_asset_activity_color").val() != ""){
        $.ajax({
            url: $("#url_check_color_waste").val(),
            dataType: "json",
            async:false,
            data: {id:id,color:$("#fixed_asset_activity_color").val()},
            success: function (data) {
                if(data.done){
                    response = true;
                }else{
                    response = false;
                    swal("Color ya en uso.",data.msg,"error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else{
        swal("Debe seleccionar un color","error");
    }

    return response;
}