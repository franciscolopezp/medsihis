$(function () {
    $("#new_valoration_type_btn").click(function () {
        $("#type_id").val("");
        $("#type_name").val("");
        setupFields();
        $("#add_valoration_type_modal .modal-title").html("Agregar tipo de valoración");
        $("#add_valoration_type_modal").modal("show");
    });

    $("#field_type").change(setupFields);
    
    $("#save_valoration_type_btn").click(function () {
        var url = $(this).attr("url");
        var method = "POST";
        var data = {
            name: $("#type_name").val(),
            valoration_type: $("#field_type").val()
        };

        if(data.name == ""){
            swal("No es posible agregar","Por favor ingrese el nombre.","error");
            return;
        }

        var ids = $(".label_valoration_field_name input[name=valoration_fields]:checked").map(function(){
            return $(this).val();
        }).get();

        if($("#type_id").val() != ""){
            method = "PUT";
            url += "/"+$("#type_id").val();
        }

        $.ajax({
            method: method,
            url: url,
            dataType: "json",
            data:{
                fields:  ids,
                valoration_type: data
            },
            success:function (data) {
                if(data.done){
                    location.reload();
                }
            }
        });
    });

    $(".edit_valoration_icon").click(function () {
        $("#add_valoration_type_modal .modal-title").html("Editar tipo de valoración");
        var url = $(this).attr("url");
        $.get(url,function (data) {
            $("#type_id").val(data.data.id);
            $("#type_name").val(data.data.name);
            $("#field_type").val(data.data.valoration_type);
            $("#field_type").selectpicker("refresh");
            setupFields();

            $.each(data.fields,function (idx, item) {
               $("#valoration_field_"+item).prop("checked",true);
            });
            $("#add_valoration_type_modal").modal("show");
        },"json");
    });

    $(".delete_valoration_icon").click(function () {
        var url = $(this).attr("url");
        var holder = $(this).attr("holder");
        swal({
                title: "¿Confirma que desea eliminar el tipo de valoración?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    url:url,
                    method:"DELETE",
                    success:function (data) {
                        if(data.done){
                            $("#"+holder).remove();
                            swal("Registro eliminado","Se ha eliminado el registro exitosamente","success");
                        }else{
                            swal("No se puede eliminar el registro","No es posible eliminar el registro","error");
                        }
                    }
                });
            }
        );
    });
});

function setupFields() {
    $(".label_valoration_field_name").hide();
    $(".label_valoration_field_name input[type=checkbox]").prop("checked", false);

    $(".vf_"+$("#field_type").val()).show();

}