var DELETE_DISEASE_CATEGORY_ID = 0;
var DELETE_DISEASE_ID = 0;
jQuery(document).ready(function() {
    var table = $("#data-table-diseaseCategory");
    var grid = $("#data-table-diseaseCategory").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-diseaseCategory').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar la categoria "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "La categoria de enfermedad sido eliminado exitosamente.", "success");
                        location.reload();
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });
        });
    });
});

function setupAddDiseaseModal(category_disease_id){
    $("#add_Disease_title").html("Agregar Enfermedad");
    //$("#add_indication_btn").attr("onclick","saveIndication('add,'"+clinical_study_id+")");
}

function saveDisease(category_disease_id){
    var disease_name = $("#new_disease_input").val();
    var disease_code = $("#new_code_input").val();
    var url = $('#url_save_diseases').val();
    var method = "POST";
    var params = {
        disease: {
            code: disease_code,
            name: disease_name,
            disease_category_id:category_disease_id
        }
    };

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(){
            location.reload();
        }
    });
}

function setupDeleteDialogDisease(disease_id,disease_category_id){
    DELETE_DISEASE_CATEGORY_ID = disease_category_id;
    DELETE_DISEASE_ID = disease_id;
}

function deleteDesease(){
    var url = $('#url_disease_categories_diseases').val();
    var method = "GET";
    var params = {
        disease_id:DELETE_DISEASE_ID,
        disease_category_id:DELETE_DISEASE_CATEGORY_ID

    };

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(){
            location.reload();
        }
    });
}