 var SUBSTANCE_ID = 0;
jQuery(document).ready(function() {
    var table = $("#data-table-medicament");
    var grid = $("#data-table-medicament").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-medicament').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>"+
                    "<a href=" + row.url_substance + " class=\"btn bgm-blue btn-icon waves-effect waves-circle waves-float btn-manage\" data-row-id=\"" + row.id + "\" title=\"Sustancias activas\"><i class=\"zmdi zmdi-drink zmdi-hc-fw\"></i></a>" ;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el medicamento "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El medicamento ha sido eliminado exitosamente.", "success");
                        location.reload();
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

});

function addSusbtance(){
    $.ajax({
        url:$("#url_add_substance").val(),
        method:"GET",
        dataType:"json",
        data:{medicament_id:$("#medicament_id").val(), active_substance_id:$("#active_substance").val()},
        success:function(data){
            console.log(data);
            if(data){
                swal("Exito","Medicamento actualizado","success")
                location.reload();
            }else{
                swal("Error","El medicamento ya cuenta con la sustancia activa.","error")
            }

        }
    });
}
function setupdeletesusbtance(substance_id){
    SUBSTANCE_ID = substance_id;
}
 function deleteSusbtance(){
     $.ajax({
         url:$("#url_delete_substance").val(),
         method:"GET",
         dataType:"json",
         data:{medicament_id:$("#medicament_id").val(), active_substance_id:SUBSTANCE_ID},
         success:function(){
             swal("Exito","Medicamento actualizado","success")
             $("#deletesubstance").modal("hide");
             $("#substance_"+$("#active_substance").val()).remove();
         }
     });
 }
//$("#substance_"+$("#active_substance").val()).remove();