// Edit window
var DELETE_IDENTITY_CARD_ID = 0;
var DELETE_LICENSE_ID = 0;
var EDIT_LICENSE_ID = 0;
var EDIT_EMAIL_ID = 0;
var LICENSES = new Array();
jQuery(document).ready(function() {
    $('form#new_doctor').find("input[type=text], textarea").val("");
    $("#new_doctor").validate();

    if($('#all_features').length){
        LICENSES = jQuery.parseJSON($('#all_features').val());
    }
    var grid = $("#data-table-doctor").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-doctor').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Mostrar\"><i class=\"zmdi zmdi-eye\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>"+
                    "<button data-url-email=\"" + row.url_send_email + "\"  data-email=\""+row.email+"\"class=\"btn btn-success btn-icon waves-effect waves-circle waves-float btn-email\" data-row-id=\"" + row.id + "\" title=\"Restablecimiento de contraseña\"><i class=\"zmdi zmdi-email\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var last_name = $tr_parent.children('td').eq(2).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar al doctor "+name+" "+last_name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El doctor ha sido eliminado exitosamente.", "success");
                        location.reload();
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
        grid.find(".btn-email").on("click",function(e){
            var url = $(this).data("url-email");
            var email = $(this).data("email");
            var ajax = $.ajax({
                type: 'post',
                url: url
            });
            ajax.done(function(result, status){
                var message = "El correo se envio a "+email;
                swal("Enviado!", message, "success");
            });

        });
    });


    $('#change_files_fiscal').change(function(){
        if($(this).is(':checked')){
            $('#div_files_fiscal').show();
            $('#admin_doctor_fiscal_information_certificate_password').val("");
        }else{
            $('#div_files_fiscal').hide();
        }
    });

    var today = new moment().add(-1,'days');
    $('#admin_doctor_birth_date').data("DateTimePicker").maxDate(today);
});


function saveSpecialty(action){

    var url = $('#url_doctor_save_speciality').val();
    var method = "POST";

    var specialty = $("#new_specialty_select").val();
    var identity = $("#new_identity_input").val();
    var doctorId = $("#edit_doctor_id").val();
    var id = $("#add_specialty_id").val();
    var institute = $("#new_identity_institute").val();

    var params = {
        identity_card:{
            identity:identity,
            doctor_id:doctorId,
            specialty_id:specialty,
            institute:institute
        }
    };

    if(action != "add"){
        url += id;
        method = "PUT";
    }
    $("#addIdentityCardModal").modal("hide");

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(){
            location.reload();
        }
    });
}


function setupAddIdentityCardModal(){
    $("#add_specialty_title").html("Agregar Especialidad");
    $("#add_specialty_btn").attr("onclick","saveSpecialty('add')");
    $("#new_identity_input").val("");
    $("#new_identity_institute").val("");
}


function setupEditDialogIdentityCard(id,identity,specialty,institute){
    $("#add_specialty_title").html("Editar Especialidad");
    $("#add_specialty_btn").attr("onclick","saveSpecialty('update')");

    $("#new_identity_input").val(identity);
    $("#new_specialty_select").val(specialty);
    $("#new_identity_institute").val(institute);
    $("#add_specialty_id").val(id);
}

function setupDeleteDialogIdentityCard(id){
    DELETE_IDENTITY_CARD_ID = id;
}

function deleteSpecialty(){
    $("#deleteIdentityCardModal").modal("hide");
    var url = $('#url_doctor_save_speciality').val();
    $.ajax({
        url:url+"/"+DELETE_IDENTITY_CARD_ID,
        method:"DELETE",
        dataType:"json",
        success:function(){
            $("#doctor_identity_card_"+DELETE_IDENTITY_CARD_ID).remove();
        }
    });
}


function setupAddLicenseModal(){
    setPricesLicenses();
    $("#add_license_title").html("Agregar Licencia");
    $("#add_licence_start_date").val("");
    $("#add_licence_end_date").val("");
    $("#detail_license").val("");
    $(".feature").prop("checked",false);

    $("#new_license_current").prop("checked",false);
    $("#new_license_current").hide();
    $("#new_license_status").hide();
    $('#errors_licenses').empty();
    $("#add_license_btn").attr("onclick","saveLicense('add')");
}

function setPricesLicenses(){
    $.each(LICENSES,function(index,val){
        var id= "#price_license_"+val["id"];
        $(id).val(val["cost"]);
    });
}

function saveLicense(action){
    var features = [];
    $('.feature').each(function(idx,chk) {
        if ($(chk).is(":checked")) {
            var id_price = "#price_license_"+$(chk).attr('value');
            var license = {license:$(chk).attr('value'), price: $(id_price).val()}
            features.push(license);
        }
    });
    var url = $('#url_doctor_save_license').val()+"/";
    var method = "POST";
    var doctorId = $("#edit_doctor_id").val();

    var params = {
        start_date:$("#add_licence_start_date").val(),
        end_date:$("#add_licence_end_date").val(),
        distribution_type:$('#distribution_type').val(),
        features:JSON.stringify(features),
        doctor_id:doctorId,
        detail: $('#detail_license').val()
    };

    if(action != "add"){
        url += EDIT_LICENSE_ID;
        method = "PUT";
        params.current = $("#new_license_current").is(":checked");
        params.status = $("#new_license_status").val();

    }
    $('#errors_licenses').empty();
    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(response){
            if(response.status){
                $("#addLicenseModal").modal("hide");
                $('#errors_licenses').empty();
                location.reload();
            }else{
                var errors = response.errors;
                var i;
                var html = "<ul>";
                for(i=0;i<errors.length; i++){
                    var message = errors[i];
                    html = html + "<li>" +message+ "</li>";
                }
                html = html + "</ul>";
                $('#errors_licenses').html(html);
            }
        }
    });

}

function setupDeleteDialogLicense(id){
    swal({
        title: "¿Confirma que desea eliminar la licencia?",
        text: "Por favor confirme que desea eliminar la licencia del doctor",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
        confirmButtonClass:"btn btn-danger",
        closeOnConfirm: false,
        closeOnCancel: true
    }, function(isConfirm){
        if (isConfirm) {
            var url = $('#url_doctor_save_license').val();
            $.ajax({
                url:url+"/"+id,
                method:"DELETE",
                dataType:"json",
                success:function() {
                    $("#license_"+id).remove();
                }
            });
            swal("Eliminado!", "Se ha eliminado la licencia.", "success");
        } else {

        }
    });
}

function setupEditDialogLicense(id){
    setPricesLicenses();
    EDIT_LICENSE_ID = id;
    $('#errors_licenses').empty();
    $("#new_license_current").show();
    $("#new_license_status").show();
    $("#add_license_btn").attr("onclick","saveLicense('edit')");
    var url = $('#url_doctor_save_license').val();
    $.ajax({
        url:url+"/"+id,
        method:"GET",
        dataType:"json",
        success:function(data){
            $.each($( "#addLicenseModal input:checkbox" ), function(id,val){
                $(val).prop("checked",false);
            });
            var l = data.license;
            var lf = data.features;
            var c = false;
            $("#add_licence_start_date").val(l.start_date);
            $("#add_licence_end_date").val(l.end_date);
            $("#new_license_status").val(l.status).selectpicker('refresh');
            $("#distribution_type").val(l.distribution_type_id).selectpicker('refresh');
            $("#detail_license").val(l.detail);
            if(l.current){
                c = true;
            }

            $.each(lf,function(idx,val){
                $("#new_license_feature_"+val["licence"]).prop("checked",true);
                $("#price_license_"+val["licence"]).val(val["price"]);
            });
            $("#new_license_current").prop("checked",c);

        }
    });
}

function setupAddEmailSettingModal(){
    $("#add_email_setting_btn").html("GUARDAR");
    $("#add_email_setting_title").html("Agregar Configuración de Correo");
    $("#add_email_setting_btn").attr("onClick","saveEmailSetting('add')");
}

function setupEditDialogEmailSetting(id){
    EDIT_EMAIL_ID = id;
    $("#add_email_setting_btn").html("ACTUALIZAR");
    $("#add_email_setting_title").html("Editar Configuración de Correo");
    $("#add_email_setting_btn").attr("onClick","saveEmailSetting('update')");
    var url = $('#url_email_setting').val();
    $.ajax({
        url:url+"/"+id,
        method:"GET",
        dataType:"json",
        success:function(data){
            $("#email_host").val(data.host);
            $("#email_username").val(data.username);
            $("#email_password").val(data.password);
            $("#email_port").val(data.port);
        }
    });
}

function saveEmailSetting(action){
    var params = {
        email_setting:{
            host:$("#email_host").val(),
            username:$("#email_username").val(),
            password:$("#email_password").val(),
            port:$("#email_port").val(),
            doctor_id:$("#edit_doctor_id").val()
        }
    };

    var url = $('#url_email_setting').val()+"/";
    var method = "POST";

    if(action != "add"){
        url += EDIT_EMAIL_ID;
        method = "PUT";
    }

    $.ajax({
        url:url,
        method:method,
        data:params,
        dataType:"json",
        success:function(data){
            $("#addEmailSettingModal").modal("hide");
            location.reload();
        }
    });
}

function deleteEmailSetting(id){
    swal({
        title: "¿Confirma que desea eliminar la configuración?",
        text: "Por favor confirme que desea eliminar la configuración del correo electrónico",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
        confirmButtonClass:"btn btn-danger",
        closeOnConfirm: false,
        closeOnCancel: true
    }, function(isConfirm){
        if (isConfirm) {
            var url = $('#url_email_setting').val();
            $.ajax({
                url:url+"/"+id,
                method:"DELETE",
                dataType:"json",
                success:function(data){
                    $("#email_setting_"+id).remove();
                }
            });
            swal("Eliminado!", "Se ha eliminado la configuración del correo.", "success");
        } else {

        }
    });
}
function validateNointerior(){
    no_int = $('#admin_doctor_fiscal_information_int_number').val().trim();
    if(no_int==""){
        $('#admin_doctor_fiscal_information_int_number').val("-");
    }
    if($('#admin_doctor_fiscal_information_ext_number').val()==""){
        $('#admin_doctor_fiscal_information_ext_number').val("-");
    }
    fiscal_city = $('#admin_doctor_fiscal_information_city_id_ts_autocomplete').val();
    if(fiscal_city=="" || fiscal_city == null || fiscal_city == "undefined"){
        $('#admin_doctor_fiscal_information_city_id_ts_autocomplete').val($('#admin_doctor_city_id_ts_autocomplete').val());
    }
}


function checkuser_exist(){
    var  editValue = $('#edit_doctor').val();
    var  new_user_name = $('#admin_doctor_user_username').val();
    var  new_mail = $('#admin_doctor_user_email').val();
    var valid = true;

    $.ajax({
        url: $('#url_check_user').val(),
        dataType: "json",
        async:false,
        data: {user_name:new_user_name,edit_value:editValue,mail:new_mail},
        success: function (data) {
            if(data.result){
                swal("Error!", "El nombre de usuario o correo electrónico ya existe.", "error");
                valid =  false;
            }
        }
    });

    if(valid){
        if($('#change_files_fiscal').is(':checked')){
            // se pretende cambiar archivos fiscales
            if($('#certificate_stamp_file').val() == "" || $('#certificate_key_file').val() == "" || $('#admin_doctor_fiscal_information_certificate_password').val() == ""){
                swal("Error!", "Agregue los archivos fiscales .cer, .key, y la contraseña.", "error");
                valid = false;
            }
        }
    }
    validateNointerior();
    return valid;

}
