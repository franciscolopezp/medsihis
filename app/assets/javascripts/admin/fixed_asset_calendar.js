function initMainCalendar(calendar_id,options){
    var defaultView = selected_view_main_calendar;
    var fc_height = "auto";
    var minTime = "06:00:00";
    var maxTime = "23:00:00";
    var events = [];


    $(calendar_id).fullCalendar("destroy");

    if(options != undefined && options.minTime != undefined){
        minTime = options.minTime;
    }

    if(options != undefined && options.maxTime != undefined){
        maxTime = options.maxTime;
    }

    if(options != undefined && options.view != undefined){
        defaultView = options.view;
        if(options.view == "month"){
            fc_height = 700;
        }

    }

    if(parseInt($("body").css("width")) < 480){
        defaultView = 'agendaDay';
        fc_height = 600;
        $("#small_calendar").parent().remove();
    }

    if(options != undefined && options.activities != undefined){
        events = options.activities;
    }

    console.log(defaultView);

    $(calendar_id).fullCalendar({
        eventLimit: 5,
        eventLimitText:"más",
        height: fc_height,
        minTime:minTime,
        maxTime:maxTime,
        allDayText: 'Todo el día',
        defaultView:defaultView,
        monthNames:fcMonthNames,
        dayNames:fcDayNames,
        dayNamesShort:fcDayNamesShort,
        header: {
            right: '',
            center: 'title',
            left: 'prev,next'
        },
        selectable: true,
        selectHelper: true,
        editable: true,
        events: events,
        select: function(start, end, allDay) {
           /* ACTIVITY_UPDATE.id = null;
            ACTIVITY_ACTION = "add";
            resetFormAddActivity();
            $('#add_personal_support_div').hide();*/
            $('#addNew-event').modal('show');

            var ed = end.toDate();
            $("#activity_start_date").val(dateToString(ed,"dd/mm/yyyy"));
            $("#activity_end_date").val(dateToString(ed,"dd/mm/yyyy"));
        },
        eventRender: function(event, element) {
            var str_data = getPopoverHtml(event);
            element.popover({
                title: event.title,
                placement: 'top',
                trigger:"hover",
                content: str_data,
                html:true
            });
        },
        eventClick: function(calEvent, jsEvent, view) {
            var id = calEvent.id;
            swal({
                    title: "¿Confirma que desea eliminar la actividad?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: $("#asset_activities_url").val()+"/"+id
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "La actividad se eliminó exitosamente", "success");
                        $("#asset_activity_"+id).remove();
                        loadFixedActivities(options);
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                    });

                });
        },
        eventDrop:function(event,dayDelta, minuteDelta, allDay, revertFunct){
            updateActivityDate(event,dayDelta);
        },
        viewRender:function(view, element){


            /**if(view.type == "agendaDay" || view.type == "agendaWeek"){
                $(calendar_id).fullCalendar('option', 'height', 700);
            }**/
        }
    });

    if(options != undefined && options.date != undefined){
        $(calendar_id).fullCalendar( 'gotoDate', options.date );
    }
    $(".fc-day-grid-event").css('background-color', options.color);
    $(calendar_id).find(".fc-toolbar").find(".fc-center").find("h2").attr("onclick","openChangeDateModal()");
    $(calendar_id).find(".fc-toolbar").find(".fc-center").find("h2").css("cursor","pointer");

    $(calendar_id).find(".fc-toolbar").find(".fc-left").find("button").addClass("btn");
    $(calendar_id).find(".fc-toolbar").find(".fc-left").find("button").css("margin-left","1px");

    $(calendar_id).find(".fc-toolbar").find(".fc-right").append("<button onclick='changeMainCalendarView(\"\",\"month\")'>Mes</button>");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").append("<button onclick='changeMainCalendarView(\"\",\"agendaWeek\")'>Semana</button>");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").append("<button onclick='changeMainCalendarView(\"\",\"agendaDay\")'>Día</button>");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").find("button").addClass("btn");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").find("button").css("margin-left","1px");


    $(calendar_id).find(".fc-toolbar").css("background","rgb(0, 188, 212)");



    $(calendar_id).find(".fc-toolbar").find(".fc-left").find(".fc-prev-button").click(function(){
        var options = {
            view:$(calendar_id).fullCalendar("getView").type,
            date:$(calendar_id).fullCalendar("getDate").toDate()
        };

        loadFixedActivities(options);

    });

    $(calendar_id).find(".fc-toolbar").find(".fc-left").find(".fc-next-button").click(function(){
        var options = {
            view:$(calendar_id).fullCalendar("getView").type,
            date:$(calendar_id).fullCalendar("getDate").toDate()
        };


        loadFixedActivities(options);

    });
}
function changeMainCalendarView(date,view){
    var conf = {
        view:view,
        date:date
    };



    if(conf.date == ''){
        conf.date = $(calendar_id).fullCalendar( 'getDate' ).toDate();
    }

    if(conf.view == ''){
        //options.view = $(calendar_id).fullCalendar( 'getView' ).type;
    }

        loadFixedActivities(conf);


}
function loadActivities(calendar_id,options){
    var date = options.date;
    var month_int = date.getMonth();
    var year_int = date.getFullYear();

    var params = {
        year:year_int,
        month:month_int,
        is_agenda:true,
        fixed_asset_item_id:$("#fixed_asset_item_id").val()
    };
        $.ajax({
            url:$("#url_get_fixed_asset_activities").val(),
            method:"GET",
            dataType:"json",
            data:params,
            success:function(activities){
                initMainCalendar(calendar_id,{
                    date:date,
                    activities:activities,
                    view:options.view});
            }
        });


}
function getPopoverHtml(event){

    var date_start = stringToDate(event.s_date,"yyyy-mm-dd hh:mn");
    var date_end = stringToDate(event.e_date,"yyyy-mm-dd hh:mn");
    var ends_same_day = true;

    var str_sd = date_start.getDate()+""+date_start.getMonth()+""+date_start.getFullYear();
    var str_ed = date_end.getDate()+""+date_end.getMonth()+""+date_end.getFullYear();

    if(str_sd != str_ed){
        ends_same_day = false;
    }

    var str_data = "<div class='popover_evt'>";

    var start_date = dateToString(date_start,"dd/fm/yyyy");
    var start_hour = dateToString(date_start,"hh:mn pp");
    var end_date = dateToString(date_end,"dd/fm/yyyy");
    var end_hour = dateToString(date_end,"hh:mn pp");

    str_data += start_date;

    if(ends_same_day){
        str_data += "<br>";
    }else{
        str_data += " ";
    }

    str_data += start_hour;

    if(ends_same_day){
        str_data += " - ";
    }else{
        str_data += "<br>";
        str_data += end_date+" ";
    }

    str_data += end_hour;

    /*if(event.activity_type_id == A_TYPE.appointment && event.patient_name != "" ){
        str_data += "<br>Paciente: "+event.patient_name+"";
    }
*/
    str_data += "<br>Detalles: "+event.details

    return str_data;
}