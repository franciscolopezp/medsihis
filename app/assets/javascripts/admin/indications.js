$(function(){
    $(".add-indication-group").click(function(){
        var ig = $(this).attr("ig");
        $("#indication_group_id").val(ig);
        $("#indication_id").val("");
        $("#indication_description").val("");
        $("#indication_priority").val(0).selectpicker("refresh");

        $("#add_indication_title").html("Agregar indicación");
        $("#save-indication-btn").html("Agregar");
        $("#add_indication_modal").modal("show");
    });

    $(".ig_components i.edit").click(function(){
        $("#add_indication_title").html("Editar indicación");
        $("#save-indication-btn").html("Actualizar");
        $("#add_indication_modal").modal("show");
        var indication_id = $(this).attr("ind_id");
        $.ajax({
            url:$("#indications_url").val()+"/"+indication_id,
            dataType:"json",
            success:function (data) {
                $("#indication_description").val(data.description);
                $("#indication_group_id").val(data.ca_group_id);
                $("#indication_id").val(data.id);
                $("#indication_priority").val(data.priority).selectpicker("refresh");
            }
        });

    });

    $(".ig_components i.delete").click(function(){
        var indication_id = $(this).attr("ind_id");
        swal({
            title: "¿Confirma que desea eliminar la indicación?",
            text: "Al eliminarla también se borrará de las órdenes generadas previamente.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn btn-danger",
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url:$("#indications_url").val()+"/"+indication_id,
                    dataType:"json",
                    method:"DELETE",
                    success:function (data) {
                        location.reload();
                    }
                });
            }
        });
    });

    $("#save-indication-btn").click(function(){
        var data = {
            id:$("#indication_id").val(),
            description:$("#indication_description").val(),
            priority:$("#indication_priority").val(),
            ca_group_id:$("#indication_group_id").val()
        };

        var url = $("#indications_url").val();
        var method = "POST";
        if(data.id != ""){
            url += "/"+data.id;
            method = "PUT";
        }

        $.ajax({
            url:url,
            method:method,
            dataType:"json",
            data:{
                indication:data
            },
            success:function (data) {
                location.reload();
            }
        });

    });
});