

$(document).ready(function () {
    addConceptHtml();
    setIvaRate();
    var fiscals_options = "<option value='0'>Seleccione el receptor</option>";
    $.ajax({
        url:$("#url_get_fiscal_infos").val(),
        data:{},
        dataType: "json",
        success:function(data){
            $.each(data.fiscalInfos, function (idx, info) {
                fiscals_options += "<option type='"+info.type+"' value = "+info.id+">"+info.rfc + " | " +info.business_name+"</option>";
            });
            $('#receptor_id').html(fiscals_options);
            $('#receptor_id').selectpicker('refresh');
            $('#receptor_id').change(set_fiscal_info);
        }
    });

});
function  set_fiscal_info() {
    $("#receptor_type_id").val($("#receptor_id option:selected").attr("type"));
}
$(function () {
    $("#new_concept_btn").click(addConceptHtml);
    $(".inv_input").keypress(function (e) {
        if ( e.which == 13 ) {
            e.preventDefault();
            calculateSubTotal();
        }
    });

    $("#create_invoice_btn").click(function (e) {
        $(".inv_input").prop("disabled",false);
        $(".input_iv_concept").prop("disabled",false);
    });

    $("#emisor_id").change(setIvaRate);
});

function setIvaRate() {
    var emisor = $("#emisor_id").val();
    var iva_percent = parseFloat($("#emisor_"+emisor).attr("rate_iva"));
    var iva_rate = iva_percent / 100 ;
    $("#invoice_iva").attr("rate",iva_rate);
    $("#iva_rate").val(iva_rate);
}

function calculateSubTotal(){
    $("#invoice_iva").val(0);

    var sub_total = 0;
    $.each($(".item_total"),function (idx, item) {
        sub_total += parseFloat($(item).val());
    });
    $("#invoice_sub_total").val(sub_total);

    var rate_iva = parseFloat($("#invoice_iva").attr("rate"));
    $("#invoice_iva").val((rate_iva * sub_total).toFixed(2));

    calculateTotal();
}

function calculateTotal() {
    if($("#invoice_isr").val() == ""){
        $("#invoice_isr").val(0);
    }
    var iva = parseFloat($("#invoice_iva").val());
    var isr = parseFloat($("#invoice_isr").val());
    var sub_total = parseFloat($("#invoice_sub_total").val());
    var total = iva + isr + sub_total;
    $("#invoice_total").val(total.toFixed(2));
}

function measureHtml() {
    return "<select name='invoice_concepts[][measure]' class='form-control selectpicker'>" +
        "<option value='No aplica'>No aplica</option>" +
        "<option value='Pieza'>Pieza</option>" +
        "<option value='Servicio'>Servicio</option>" +
        "</select>";
}

function addConceptHtml(){
    $("#new_invoice_concepts tbody").append("<tr>" +
        "<td style='width: 100px;'><input type='number' name='invoice_concepts[][quantity]' class='input_iv_concept item_quantity w-100' autocomplete='off'></td>" +
        "<td><input type='text' name='invoice_concepts[][description]' class='input_iv_concept w-100' autocomplete='off'></td>" +
        "<td style='width: 200px;'>"+measureHtml()+"</td>" +
        "<td><input type='text' name='invoice_concepts[][unit_price]' class='input_iv_concept item_unit_price w-100' autocomplete='off'></td>" +
        "<td><input type='text' name='invoice_concepts[][total]' class='input_iv_concept item_total w-100' autocomplete='off' disabled></td>" +
        "<td><i class='zmdi zmdi-delete delete_concept_btn' style='cursor: pointer;'></i></td>" +
        "</tr>");

    $(".selectpicker").selectpicker();

    $(".input_iv_concept").unbind("keypress");
    $(".delete_concept_btn").unbind("click");
    $(".delete_concept_btn").click(function () {
        $(this).parent().parent().remove();
        calculateSubTotal();
    });

    $(".input_iv_concept").keypress(function (e) {
        if ( e.which == 13 ) {
            e.preventDefault();
        }
    });

    $(".item_unit_price").unbind("keypress");
    $(".item_unit_price").keypress(function (e) {
        if ( e.which == 13 ) {
            e.preventDefault();
            var quantity = parseFloat($(this).parent().parent().find(".item_quantity").val());
            var unit_price = parseFloat($(this).val().replace(/,/g, ""));

            $(this).parent().parent().find(".item_total").val(quantity * unit_price);
            calculateSubTotal();
        }
    });

    $(".item_quantity").unbind("keypress");
    $(".item_quantity").keypress(function (e) {
        if ( e.which == 13 ) {
            e.preventDefault();
            var unit_price = parseFloat($(this).parent().parent().find(".item_unit_price").val().replace(/,/g, ""));
            var quantity = parseFloat($(this).val());

            $(this).parent().parent().find(".item_total").val(quantity * unit_price);
            calculateSubTotal();
        }
    });
}
function validate_invoice(){
    var status = true;
        if($("#receptor_id").val()==0){
        swal("Error!", "Debe seleccionar un receptor de la lista", "error");
        status = false;
    }
    if($('#invoice_total').val()<= 0){
        swal("Error!", "Debe agregar conceptos y el total no debe ser 0", "error");
        status = false;
    }
    return status;
}