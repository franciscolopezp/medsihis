$(function () {
    $("#new_valoration_field_btn").click(function () {
        $("#add_valoration_field").modal("show");
        $("#field_id").val("");
        $("#field_name").val("");
        $("#field_label").val("");
        $("#field_unit").val("");
        $("#add_valoration_field .modal-title").html("Agregar campo de valoración");
        setupCollectionHolder();
    });

    $("#field_html_type").change(setupCollectionHolder);

    $("#save_valoration_field_btn").click(function () {
        var url = $(this).attr("url");
        var data = {
            f_type: $("#field_type").val(),
            name: $("#field_name").val(),
            label: $("#field_label").val(),
            unit: $("#field_unit").val(),
            code: "["+ $("#field_code").val()+"]",
            f_html_type: $("#field_html_type").val(),
            collection_type: $("#field_collection").val()
        };

        if(data.name == "" || data.label == ""){
            swal("No es posible agregar","Por favor ingrese el nombre del campo y su etiqueta.","error");
            return;
        }

        if(data.f_html_type != "collection"){
            data.collection_type = "";
        }

        var method = "POST";
        if($("#field_id").val() != ""){
            method = "PUT";
            url += "/"+$("#field_id").val();
        }

        $.ajax({
            method:method,
            url: url,
            dataType: "json",
            data:{valoration_field: data},
            success:function (data) {
                if(data.done){
                    location.reload();
                }
            }
        });


    });

    $(".edit_field_icon").click(function () {
        var url = $(this).attr("url");
        $.get(url,{},function (data) {
            $("#field_id").val(data.id);
            $("#field_type").val(data.f_type);
            $("#field_name").val(data.name);
            $("#field_label").val(data.label);
            $("#field_unit").val(data.unit);
            $("#field_collection").val(data.collection_type);
            if(data.code != null){
                $("#field_code").val(data.code.replace("[","").replace("]",""));
            }
            $("#field_html_type").val(data.f_html_type);
            $("#field_html_type").selectpicker("refresh");
            $("#field_type").selectpicker("refresh");
            $("#field_collection").selectpicker("refresh");
            setupCollectionHolder();
            $("#add_valoration_field .modal-title").html("Editar campo de valoración");
            $("#add_valoration_field").modal("show");
        },"json");
    });

    $(".delete_field_action").click(function () {
        var url = $(this).attr("url");
        var field = $(this).attr("field");
        swal({
                title: "¿Confirma que desea eliminar el campo?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Eliminar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    url:url,
                    method:"DELETE",
                    success:function (data) {
                        if(data.done){
                            $("#"+field).remove();
                            swal("Registro eliminado","Se ha eliminado el registro exitosamente","success");
                        }else{
                            swal("No se puede eliminar el registro","No es posible eliminar el registro","error");
                        }
                    }
                });
            }
        );
    });

    $(".add_field_to_valoration").click(function () {
        var field = $(this).attr("field");
        var valoration = $(this).attr("valoration");

        $.post($("#add_to_valoration_url").val(),{
            valoration_field_id: field,
            valoration_type_id: valoration
        },function (data) {
            if(data.done){
                location.reload();
            }else{
                swal("No es posible agregar el campo","El campo ya existe en la valoración.","error");
            }
        },"json");
    });
});

function setupCollectionHolder() {
    if($("#field_html_type").val() == "collection"){
        $("#collection_holder").show();
    }else {
        $("#collection_holder").hide();
    }
}