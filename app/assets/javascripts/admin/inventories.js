jQuery(document).ready(function() {

    var table = $("#data-table-inventories");
    var grid = $("#data-table-inventories").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-inventories').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                var cmds = "";
                if(!row.is_closed){
                    cmds += "<a href=\"" + row.print_url + "\" target=\"_blank\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-print\" data-row-id=\"" + row.id + "\" title=\"Imprimir\"><i class=\"zmdi zmdi-print\"></i></a>";
                    cmds += "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Ver\"><i class=\"zmdi zmdi-refresh-sync\"></i></a>";
                }
                return cmds;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el inventario ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El inventario ha sido eliminado exitosamente.", "success");
                        $('#data-table-medicament_categories').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

    $("#data-table-medicament-inventories-search").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function()
        {
            var consigment = 0;
            var warehouse_id_search = $('#warehouse_id').val();
            var medicament_catagory_id_search = $('#medicament_category_id').val();
            if($('#is_consigment').is(":checked")){
                consigment = 1;
            }
            if(warehouse_id_search == "" || warehouse_id_search == "undefined" || warehouse_id_search == "TODO"){
                warehouse_id_search = "all"
            }

            if(medicament_catagory_id_search == "" || medicament_catagory_id_search == "undefined"|| medicament_catagory_id_search == "TODO " ){
                medicament_catagory_id_search = "all"
            }

            return $('#data-table-medicament-inventories-search').data('source')+"?consigment="+consigment+"&warehouse_id_search="+warehouse_id_search+"&medicament_catagory_id_search="+medicament_catagory_id_search;
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        templates: {
            search: ""
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    });
});

function reload_inventory(){
    $('#data-table-medicament-inventories-search').bootgrid('reload');
}

function export_excel(){
    var consigment = 0;
    var warehouse_id_search = $('#warehouse_id').val();
    var medicament_catagory_id_search = $('#medicament_category_id').val();
    if($('#is_consigment').is(":checked")){
        consigment = 1;
    }
    if(warehouse_id_search == "" || warehouse_id_search == "undefined" || warehouse_id_search == "TODO"){
        warehouse_id_search = "all"
    }

    if(medicament_catagory_id_search == "" || medicament_catagory_id_search == "undefined"|| medicament_catagory_id_search == "TODO " ){
        medicament_catagory_id_search = "all"
    }
    window.location.href  = $("#export_excel_inventory").val()+"?consigment="+consigment+"&warehouse_id_search="+warehouse_id_search+"&medicament_catagory_id_search="+medicament_catagory_id_search;

}
function export_pdf(){
    var consigment = 0;
    var warehouse_id_search = $('#warehouse_id').val();
    var medicament_catagory_id_search = $('#medicament_category_id').val();
    if($('#is_consigment').is(":checked")){
        consigment = 1;
    }
    if(warehouse_id_search == "" || warehouse_id_search == "undefined" || warehouse_id_search == "TODO"){
        warehouse_id_search = "all"
    }

    if(medicament_catagory_id_search == "" || medicament_catagory_id_search == "undefined"|| medicament_catagory_id_search == "TODO " ){
        medicament_catagory_id_search = "all"
    }
    var url_pdf  = $("#url_print_inventories_report").val()+"?consigment="+consigment+"&warehouse_id_search="+warehouse_id_search+"&medicament_catagory_id_search="+medicament_catagory_id_search;
    window.open(url_pdf, '_blank');
}
function print_inventory_report(){
    url_pdf = $('#url_print_inventory_report').val()+"?warehouse_id_search="+warehouse_id_search+"&medicament_catagory_id_search="+medicament_catagory_id_search;
    window.open(url_pdf, '_blank');

}

function sync_inventory(){
    var stocks = [];
    inventory_id = $('#stock_table').find('#inventory_id').val();
    warehouse_id = $('#stock_table').find('#warehouse_id').val();
    $('#stock_table').find('tbody').find('tr').each(function (idx, item) {
        var row = $(this);
        stocks.push({
            stock_id:row.find("#stock_id").val(),
            lot_id: row.find('#lot_id').val(),
            quantity: row.find('#quantity').val(),
            quantity_old: row.find('#quantity_old').val(),
            is_medicament: row.find('#is_medicament').val(),
            pharmacy_item_id: row.find('#pharmacy_item_id').val()
        });
    });


    $.ajax({
        url: $("#url_update_medicament_inventories").val(),
        dataType: "json",
        type: "POST",
        data: {inventory_id:inventory_id, warehouse_id:warehouse_id, stocks:stocks},
        success: function (data) {
            swal({
                    title: "Exito!",
                    text: "Las existencias han sido guardadas.",
                    type: "success"
                },
                function(){
                    window.location.href = $('#url_inventories').val();;
                });
            //swal("Exito!", "Las existencias han sido guardadas.", "success");
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
