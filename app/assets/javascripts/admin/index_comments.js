function deleteComment(url,id){
    swal({
     title: "¿Confirma que desea eliminar el comentario?",
     text: "Esta operación no se puede deshacer",
     type: "warning",
     showCancelButton: true,
     confirmButtonText: "Si, Eliminar",
     cancelButtonText: "Cancelar",
     confirmButtonClass:"btn btn-danger",
     cancelButtonClass:"btn btn-link",
     closeOnConfirm: true,
     closeOnCancel: true
     }, function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url:url,
                method:"DELETE",
                success:function(){
                    location.reload();
                }
            });

        } else {

        }
    });

}