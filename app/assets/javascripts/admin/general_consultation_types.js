function resetChecks() {
    $.each($(".s_parent"),function(idx, item){
        if($(item).is(":checked")){
            $(item).prop('checked', false);
        }
    });
}
function saveConsultationTypes(){
    var consultation_types = [];
    $.each($(".s_parent"),function(idx, item){
        if($(item).is(":checked")){
            consultation_types.push({
                consultation_type_id:$(item).val()
            });
        }
    });
    if (consultation_types.length == 0){
        swal("Error!", "Debe seleccionar un usuario y los tipos de consulta para el usuario.", "error");
        return;
    }
    $.ajax({
        url: $("#url_save_user_consultation_types").val(),
        dataType: "json",
        data: {user_id:$("#user_name_ts_autocomplete").val(),consultation_types:consultation_types},
        success: function (data) {
            swal("Exito!", "Los tipos de consulta fueron agregadas.", "success");
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function loadConsultationTypes(user){
    $.ajax({
        url: $("#url_get_user_consultation_types").val(),
        dataType: "json",
        data: {user_id:user},
        success: function (data) {
            $("#consultation_types_div").show();
            $.each(data, function (idx, info) {
                $("#"+info.name).prop('checked', true);
            });

        },
        error:function(data){
            console.log("error controller");
        }
    });
}
jQuery(document).ready(function() {
    $("#consultation_types_div").hide();
    $("#user_name").ts_autocomplete({
        url:AUTOCOMPLETE_PERSONS_URL,
        items:10,
        callback:function(val, text){
            resetChecks();
            loadConsultationTypes(val);
        }
    });

    var grid = $("#data-table-general-consultation-types").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-general-consultation-types').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el tipo de consulta "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El tipo de consulta ha sido eliminada exitosamente.", "success");
                        $('#data-table-general_accounts').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });




});

