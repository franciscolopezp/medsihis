$(document).ready(function () {
   if($("#consultation_form_preview")[0]){
       loadPreviewFormConsultation();
   }

   initHtmlEditor("#consultation_template",{height:600});
   initHtmlEditor("#consultation_template_header",{height:150});
   initHtmlEditor("#consultation_template_footer",{height:150});
    $("#consultation_template").summernote().code($("#html_template").val());
    $("#consultation_template_header").summernote().code($("#html_template_header").val());
    $("#consultation_template_footer").summernote().code($("#html_template_footer").val());
});

$(function () {
    $( ".connectedSortable" ).sortable({
        placeholder: "consultation_field_item",
        connectWith: ".connectedSortable",
        cancel: ".disabled",
        stop:function () {
            var no_active = [];
            var active = [];
            $.each( $("#non_active_fields li"),function (idx, item) {
               no_active.push({id: $(item).attr("field")});
            });
            var order = 1;
            $.each($("#active_fields li"),function (idx, item) {
                active.push({id: $(item).attr("field"), order: order++});
            });

            $.post($("#url_save_configuration_fields").val(),{
                no_active: no_active,
                active:active
            },function (data) {
                loadPreviewFormConsultation();
            },"json");
        }
    }).disableSelection();

    $(".edit_field").click(function () {
        var url = $(this).attr("url");
        $.get(url,function (data) {
            $("#url_update").val(url);
            $("#add_consultation_field_modal").modal("show");
            $("#field_name").val(data.name);
            $("#field_code").val(data.code);
            $("#field_type").val(data.field_type).selectpicker("refresh");
            $("input[value="+data.field_class+"]").prop("checked",true);
        },"json");
    });

    $("#new_consultation_field").click(function () {
        $("#field_name").val("");
        $("#field_code").val("");
        $("#url_update").val("");
        $("#add_consultation_field_modal").modal("show");
    });



    $("#save_consultation_field").click(function () {
        var url = $(this).attr("url");
        var method = "POST";
        if($("#url_update").val() != ""){
            method = "PUT";
            url = $("#url_update").val();
        }


        $.ajax({
            method:method,
            url: url,
            dataType: "json",
            data:{
                consultation_field:{
                    name: $("#field_name").val(),
                    code: "[" +$("#field_code").val()+"]",
                    field_type: $("#field_type").val(),
                    field_class: $("input[name=field_class]:checked").val()
                }
            },
            success:function(data){
                if(data.done){
                    location.reload();
                }
            }
        });

    });

    $("#save_consultation_template_btn").click(function () {
        var url = $(this).attr("url");
        $.post(url,{
            consultation_template_header_height: $("#consultation_template_header_height").val(),
            consultation_template_footer_height: $("#consultation_template_footer_height").val(),
            html: $("#consultation_template").summernote().code(),
            header: $("#consultation_template_header").summernote().code(),
            footer: $("#consultation_template_footer").summernote().code()
        },function (data) {
            if(data.done){
                swal("Plantilla actualizada","La plantilla ha sido actualizada exitosamente.","success");
            }
        },"json");
    });
});

function loadPreviewFormConsultation(){
    $.get($("#url_form_medical_consultation").val(),function (data) {
        $("#consultation_form_preview").html(data);
        $(".date-picker").datetimepicker({
            format: "DD/MM/YYYY"
        });
    });
}