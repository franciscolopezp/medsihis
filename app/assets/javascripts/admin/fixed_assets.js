//= require admin/fixed_asset_calendar.js
var calendar_id = "#calendar_fixed_asset";
var FIXED_ASSET_ITEM_ID = 0;
var FIXED_ACTIVITY = 0;
jQuery(document).ready(function() {
    $("#old_ui").hide();
    var table = $("#data-table-fixed_assets");
    var grid = $("#data-table-fixed_assets").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-fixed_assets').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>" +
                    "<a href=" + row.url_manage + " class=\"btn bgm-blue btn-icon waves-effect waves-circle waves-float btn-manage\" data-row-id=\"" + row.id + "\" title=\"Elementos\"><i class=\"zmdi zmdi-view-dashboard zmdi-hc-fw\"></i></a>";
            }
        }

    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el activo fijo "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El activo fijo ha sido eliminado exitosamente.", "success");
                        $('#data-table-fixed_assets').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

    if ($("#url_get_fixed_asset_activities")[0]){
        loadFixedActivities({
            date: new Date(),
            view:"month"
        });
    }

    $('body').on('click', '.event-tag > span', function(){
        $('.event-tag > span').removeClass('selected');
        $(this).addClass('selected');
        FIXED_ACTIVITY = $(this).attr("activity-type");
    });
});


$(function () {
   $("#main_category_asset").change(function(){
       $.ajax({
           url:$("#dashboard_items_url").val(),
           data:{
               category_id: $("#main_category_asset").val()
           },
           success: function(data){
               $("#active_items_holder").html(data);
           }
       })
   });

   $("#add_asset_activity_btn").click(function(){
       addAssetActivity(false);
   });
    $("#addEventCalendar").click(function(){
        addAssetActivityCalendar(false);
    });
});
function loadFixedActivities(options){
    loadActivities(calendar_id,options);
}
function addAssetActivityCalendar(force) {
    var start_date = $("#activity_start_date").val();
    var end_date  = $("#activity_end_date").val();
    var start_time = $("#activity_start_time").val();
    var end_time  = $("#activity_end_time").val();

    var startDate = getDate(start_date,start_time);
    var endDate = getDate(end_date,end_time);

    if(FIXED_ACTIVITY == 0){
        swal("No es posible guardar el registro","Por favor seleccione el tipo de actividad.","error");
        return;
    }

    $.ajax({
        method:"POST",
        dataType:"json",
        url:$("#asset_activities_url").val(),
        data:{
            force:force,
            asset_activity:{
                fixed_asset_item_id: $("#fixed_asset_item_id").val(),
                title:$("#activity_name").val(),
                details:$("#activity_details").val(),
                start_date:startDate,
                end_date:endDate,
                activity:FIXED_ACTIVITY
            }
        },
        success:function(data){
            if(data.done){
                location.reload();
            }else{
                swal({
                        title: "Parece que hay una actividad para el activo en el mismo horario. ¿Confirma que desea agregar?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Aceptar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: true
                    },
                    function(){
                        addAssetActivityCalendar(true);
                    });
            }
        }
    });
}
function getDate(str1,str2){
    var dt1   = parseInt(str1.substring(0,2));
    var mon1  = parseInt(str1.substring(3,5));
    var yr1   = parseInt(str1.substring(6,10));

    var hourComponents = str2.split(" ");
    var hour = hourComponents[0];
    var format = hourComponents[1];
    var hComp = hour.split(":");

    var h = parseInt(hComp[0]);
    var m = parseInt(hComp[1]);

    if(format == "PM" && h != 12){
        h += 12;
    }else if(format == "AM" && h == 12){
        h = 0;
    }
    return new Date(yr1, mon1-1, dt1,h,m,0);
}
function addAssetActivity(force) {
    $.ajax({
        method:"POST",
        dataType:"json",
        url:$("#asset_activities_url").val(),
        data:{
            force:force,
            asset_activity:{
                fixed_asset_item_id: $("#fixed_asset_item_id").val(),
                title:$("#activity_title").val(),
                details:$("#activity_details").val(),
                start_date:$("#activity_start_date").val(),
                end_date:$("#activity_end_date").val()
            }
        },
        success:function(data){
            if(data.done){
                location.reload();
            }else{
                swal({
                        title: "Parece que hay una actividad para el activo en el mismo horario. ¿Confirma que desea agregar?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Aceptar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: true
                    },
                    function(){
                        addAssetActivity(true);
                    });
            }
        }
    });
}

function setFixedAssetItemEdit(asset_id,id,name,description ,identifier ,state ,fixed_asset_activity,schedule ){
    $("#add_fixed_asset_item_btn").attr("onclick","saveFixedAssetItem("+asset_id+",'update',"+id+")");
    if(state == "true"){$("#schedule").prop('checked', true);}else{$("#schedule").prop('checked', false);}
    if(schedule == "true"){$("#state").prop('checked', true);}else{$("#state").prop('checked', false);}
    $("#name").val(name);
    $("#identifier").val(identifier);
    $("#description").val(description);
}
function setFixedAssetItem(id){
    $("#add_fixed_asset_item_btn").attr("onclick","saveFixedAssetItem("+id+",'add',0)");
    $("#schedule").prop('checked', false);
    $("#state").prop('checked', false);
    $("#name").val("");
    $("#identifier").val("");
    $("#description").val("");

}
function saveFixedAssetItem(id,action,fi_id){

    var url = "/administrador/elementos_activos_fijos/";
    var method = "POST";
    var state = 0;
    var schedule = 0;
    if ($("#state").is(':checked')){
        state = 1;
    }
    if ($("#schedule").is(':checked')){
        schedule = 1;
    }
    var params = {
        fixed_asset_item:{
            name:$("#name").val(),
            identifier:$("#identifier").val(),
            description:$("#description").val(),
            state:state,
            schedule:schedule,
            fixed_asset_activity_id:$("#activity").val(),
            fixed_asset_id:id,
            grid:false
        }
    };

    if(action != "add"){
        url += fi_id;
        method = "PUT";
    }
    $("#addFixedAssetItemModal").modal("hide");

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(){
            location.reload();
        }
    });

}
function setupDeleteFixedAssetItem(id){
    FIXED_ASSET_ITEM_ID = id;
}
function deleteFixedAssetItem(){
    $("#delete_fixed_asset_item_modal").modal("hide");
    $.ajax({
        url:"/administrador/elementos_activos_fijos/"+FIXED_ASSET_ITEM_ID,
        method:"DELETE",
        dataType:"json",
        success:function(){
            $("#fixed_asset_item_"+FIXED_ASSET_ITEM_ID).remove();
            swal("Eliminado!", "El elemento ha sido eliminado exitosamente.", "success");
        }
    });
}

$(function(){
    $(".delete_asset_activity_btn").click(function() {
        var id = $(this).attr("asset-activity");
        swal({
                title: "¿Confirma que desea eliminar la actividad?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, eliminar!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            },
            function(){
                var ajax = $.ajax({
                    type: 'DELETE',
                    url: $("#asset_activities_url").val()+"/"+id
                });
                ajax.done(function(result, status){
                    swal("Eliminado!", "La actividad se eliminó exitosamente", "success");
                    $("#asset_activity_"+id).remove();
                });

                ajax.fail(function(error, status, msg){
                    swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                });

            });

    });
});