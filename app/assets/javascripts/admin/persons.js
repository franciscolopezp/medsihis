var row_counter = 1;
var doctors_data = [];
jQuery(document).ready(function() {

    $('form#new_person').find("input[type=text], textarea").val("");
    $("#new_person").validate();

    var table = $("#data-table-persons");
    var grid = $("#data-table-persons").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-persons').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-edit\" title=\"Editar\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var last_name = $tr_parent.children('td').eq(2).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar al usuario "+name+" - "+last_name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Eliminar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El usuario ha sido eliminado exitosamente.", "success");
                        $('#data-table-assistant').bootgrid('reload');
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

    $('#change_pass').change(function(){
        if($(this).is(':checked')){
            $('#modificate_pass').show();
        }else{
            $('#modificate_pass').hide();
        }
    });

    if($("#user_id").val()  != undefined){
        $.ajax({
            url: $("#url_get_doctors_agenda").val(),
            dataType: "json",
            data: {user_id:$("#user_id").val()},
            success: function (data) {
                if(data.done){
                    $.each(data.doctors, function (idx, info) {
                        $("#doctors_agendas").append("<tr id='doctor_agenda_"+row_counter+"'><td style='text-align: center'>"+
                            info.name+"</td>" +
                            "<td><a data-toggle='modal'  onclick='setDeleteDoctorAgenda("+row_counter+")'>" +
                            "<i class='zmdi zmdi-delete' style='font-size: 1.73em;' title='Eliminar'></i></a></td></tr>");

                        doctors_data.push({
                                id:row_counter,
                                doctor_id:info.id
                            }
                        );
                        console.log(doctors_data);
                        row_counter++;
                    });
                }else{
                    swal("Error!", "Ocurrio un error al guardar", "error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }
});
function clear_doctor_modal(){
    $('#agenda_doctor').val("");
    $("#agenda_doctor_ts_autocomplete").val("");
}
function add_doctor_table(){
    if ($('#agenda_doctor_ts_autocomplete').val() == "" ){
        swal("Error!", "Debe seleccionar un médico", "error");
        return;
    }
    $("#doctors_agendas").append("<tr id='doctor_agenda_"+row_counter+"'><td style='text-align: center'>"+
        $('#agenda_doctor').val()+"</td>" +
        "<td><a data-toggle='modal'  onclick='setDeleteDoctorAgenda("+row_counter+")'>" +
        "<i class='zmdi zmdi-delete' style='font-size: 1.73em;' title='Eliminar'></i></a></td></tr>");

    doctors_data.push({
            id:row_counter,
            doctor_id:$("#agenda_doctor_ts_autocomplete").val()
        }
    );
    console.log(doctors_data);
    row_counter++;

    $("#add_doctor_modal").modal("hide");
}
function setDeleteDoctorAgenda(row){
    swal({
            title: "¿Eliminar médico de la lista?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){
            $("#doctor_agenda_"+row).remove();
            doctors_data = $.grep(doctors_data,
                function(o,i) { return o.id == row; },
                true);
            swal("Eliminado!", "El médico ha sido eliminado.", "success");
            console.log(doctors_data);
        });
}
function save_doctors(){
    $.ajax({
        url: $("#url_save_doctors_agenda").val(),
        dataType: "json",
        data: {doctors:doctors_data,user_id:$("#user_id").val()},
        success: function (data) {
            if(data.done){
                swal("Exito", "Agenda(s) de médico(s) guardado con exito", "success");
            }else{
                swal("Error!", "Ocurrio un error al guardar", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function checkuser_exist(){
   /* var  editValue = $('#edit_assistant').val();
    var  new_user_name = $('#assistant_user_attributes_user_name').val();
    var valid = true;
    $.ajax({
        url: $('#url_check_user').val(),
        dataType: "json",
        async:false,
        data: {user_name:new_user_name,edit_value:editValue},
        success: function (data) {
            if(data.result){
                swal("Error!", "El nombre de usuario ya existe.", "error");
                valid =  false;
            }

        }
    });*/
    return true;

}

