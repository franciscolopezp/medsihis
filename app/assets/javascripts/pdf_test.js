$(function(){
    // rest of code here
    var array_ids = JSON.parse($("#elementsIds").val());
    $('#log_text').append(",-["+array_ids+"]");
    var height_before = 0;
    var top_before = 0;
    for(var i = 0; i < array_ids.length; i ++){
        var id = array_ids[i];
        var element_id = "info_"+id;
        var id_aux = "#"+element_id;
        $('#log_text').append(",Acumulado: "+height_before);
        if(i != 0){
            // segundo elemento del arreglo
            var height = parseInt(document.getElementById(element_id).clientHeight);
            var value_top = document.getElementById(element_id).style.top.split("px");
            var top = parseInt(value_top[0]);
            var name = $(id_aux).data('name');
            $('#log_text').append(","+name+"offsetH: "+height);
            $('#log_text').append(","+name+"top: "+top);
            if(parseInt(top) < parseInt(top_before)){
                // el alto anterior (height_before) es mayor al top actual, quiere decir que se debe bajar el siguiente
                // elemento, top = height_before
                var new_top = height_before.toString()+"px";
                $('#log_text').append(",3-"+new_top);
                document.getElementById(element_id).style.top = new_top;

                height_before += height; // solo se agrega la altura del siguiente elemento, el top no porque es el mismo de before
                $('#log_text').append(",4-"+height_before);
            }else{
                // el alto mas el top anterior, chocha con el siguiente elemento
                if(parseInt(top_before + height_before) > parseInt(top)){
                    var aux = top_before + height_before;
                    if(aux < 0){
                        aux = aux * -1;
                    }

                    var top_before = aux.toString()+"px";
                    document.getElementById(element_id).style.top = top_before;
                }
                // no chochan los elementos
                // se resetea el height
                height_before = parseInt(document.getElementById(element_id).clientHeight);
                // $('#log_text').append(",5-"+height_before);
                var value_top = document.getElementById(element_id).style.top.split("px");
                top_before = parseInt(value_top[0]);
                //  height_before += top;
                // $('#log_text').append(",6-"+height_before);


            }
        }else{
            height_before = parseInt(document.getElementById(element_id).clientHeight);
            var name = $(id_aux).data('name');
            $('#log_text').append(","+name+"offsetH: "+height_before);
            //  var id_aux = "#"+element_id;
            var h_aux = $( id_aux ).height();
            $('#log_text').append(","+name+" height "+h_aux);
            // height_before = h_aux;
            var value_top = document.getElementById(element_id).style.top.split("px");
            // height_before += parseInt(value_top[0]);
            $('#log_text').append(","+name+"top: "+value_top[0]);
            $('#log_text').append(","+name+" innerHeight: "+$( id_aux ).innerHeight());
            $('#log_text').append(","+name+" outerHeight: "+$( '#list-tra' ).height());
            top_before = parseInt(value_top[0]);
        }
    }
    $('#log_text').append(",TERMINO");
});