var socket_conf = {
    cal_id : "calendar_doctor"
};


var act_type = {
    appointment : 1
};

var act_status = {
    created:1,
    waiting:2,
    consult:3,
    attended:4,
    cancelled:5,
    confirm_pending:12
};

$(function(){
    if(typeof socket_params !== 'undefined'){
        PrivatePub.subscribe("/messages/public", function(data) {
        });

        PrivatePub.subscribe("/messages/private/" + socket_params.doctor_username, function(data) {
            socketResponse(data);
        });
    }
});

function socketResponse(data){
    var not_info = {
        title:"",
        body:""
    };
    switch (data.type){
        case "new_activity":// is called en new and edit
            data.activity.start = stringToDate(data.activity.s_date,"yyyy-mm-dd hh:mn");
            data.activity.end = stringToDate(data.activity.e_date,"yyyy-mm-dd hh:mn");
            removeActivity(data.activity.id);
            drawActivity(data.activity);
            if(data.activity.activity_type_id == act_type.appointment &&
                $("#in_waiting_room_page").val() == "yes" &&
                data.dragged != true){//draw activity if is in the wromm page
                drawActivityCard(data.activity);
            }

            if(data.activity.activity_status_id == act_status.confirm_pending){
                not_info.title = "Nueva actividad por confirmar";
                not_info.body = data.activity.title+"\nFecha: "+dateToString(data.activity.start,"dd/mm/yyyy hh:mn pp");
                drawPendingAppointment(data.activity);
            }else{
                $("#data_appointment_"+data.activity.id).remove();
                updateNotificationsLabel();
            }

            if(not_info.title == "" && data.is_new){
                not_info.title = "Nueva actividad en la agenda";
                not_info.body = data.activity.title+"\nFecha: "+dateToString(data.activity.start,"dd/mm/yyyy hh:mn pp");
            }else if(not_info.title == "" && !data.is_new){
                not_info.title = "Actividad actualizada en la agenda";
                not_info.body = data.activity.title+"\nFecha: "+dateToString(data.activity.start,"dd/mm/yyyy hh:mn pp");
            }

            break;
        case "delete_activity":
            not_info.title = "Se ha eliminado una actividad";
            not_info.body = data.activity.name;
            removeActivity(data.id);
            break;
        case 'new_wroom_status':
            console.log(data.activity);
            data.activity.start = stringToDate(data.activity.s_date,"yyyy-mm-dd hh:mn");
            data.activity.end = stringToDate(data.activity.e_date,"yyyy-mm-dd hh:mn");

            var start = data.activity.start;
            var today = new Date();

            if(data.activity.activity_status_id == act_status.created){
                not_info.title = "Cita agendada";
            }else if(data.activity.activity_status_id == act_status.waiting){
                not_info.title = "Nuevo paciente en sala de espera";
            }else if(data.activity.activity_status_id == act_status.consult){
                not_info.title = "Nuevo paciente en consulta";
            }else if(data.activity.activity_status_id == act_status.attended){
                not_info.title = "Cita atendida";
            }

            not_info.body = data.activity.patient_name+".\nHora de cita: "+dateToString(data.activity.start,"hh:mn pp");

            if(data.activity.activity_status_id == act_status.waiting){
                drawWaitingNotification(data.activity);
            }else{
                $("#waiting_patient_"+data.activity.id).remove();
            }


            if(data.prev_in_cosult != null && data.prev_in_cosult.activity_status_id == act_status.waiting){
                data.prev_in_cosult.start = stringToDate(data.prev_in_cosult.s_date,"yyyy-mm-dd hh:mn");
                drawWaitingNotification(data.prev_in_cosult);
            }
            updateWaitingCount();

            break;
        case "new_consult":
            not_info.title = "Se ha agregado una consulta";
            not_info.body = data.data.patient;
            break;
        case "charge_consult":
            not_info.title = "Se ha registrado un cobro";
            not_info.body = data.patient_name;
            break;
    }


    showNotification(not_info);

}


/* calendar page*/
function drawActivity(activity){
    $("#"+socket_conf.cal_id).fullCalendar( 'removeEvents', activity.id);

    if(activity.start == undefined){
        activity.start = stringToDate(activity.s_date,"yyyy-mm-dd hh:mn");
        activity.end = stringToDate(activity.e_date,"yyyy-mm-dd hh:mn");
    }

    if(activity.allDay){
        activity.end = new Date(activity.end.getTime() + (1 * 24 * 60 * 60 * 1000));
    }

    $("#"+socket_conf.cal_id).fullCalendar('renderEvent',activity,true ); //Stick the event
}

function removeActivity(id){
    $("#"+socket_conf.cal_id).fullCalendar('removeEvents', id);
}

/* waiting_room page */
function drawActivityCard(activity) {
    var column = "";
    var in_consult_class = "";
    var lbl_class = "";

    var card_header_html = "<strong>"+activity.patient_name+" - "+activity.title+"</strong>";
    card_header_html = "<a title='Generar nueva consulta' href='/doctor/ver_expediente/"+activity.expedient_id+"#abrir_nueva_consulta'>"+card_header_html+"</a>";

    if(activity.activity_status_id == act_status.created || activity.activity_status_id == act_status.confirm_pending){
        column = "pending";
        lbl_class = "label-warning";
    }else if(activity.activity_status_id == act_status.waiting){
        column = "waiting";
        lbl_class = "label-info";
    }else if(activity.activity_status_id == act_status.consult){
        column = "waiting";
        in_consult_class = "in_consult";
        lbl_class = "label-info";
    }else{
        column = "attended";
        lbl_class = "label-success";
    }

    $("#activity_"+activity.id).remove();

    var html = "";
    html += "<li class='draggable "+in_consult_class+"' id='activity_"+activity.id+"' activity='"+activity.id+"'>";
    html += "<div>";
    html += card_header_html+"<br>";
    html += activity.details+"<br>";
    html += dateToString(stringToDate(activity.s_date,"yyyy-mm-dd hh:mn"),"dd/fm/yyyy hh:mn pp")+"<br>";
    html += "<label id='status_"+activity.id+"' class='label "+lbl_class+"'>"+activity.activity_status_name+"</label>"+"<br>";
    html += "</div>";
    html += "</li>";

    if(activity.activity_status_id == act_status.consult){
        $("#"+column+" ul").prepend(html);
    }else{
        $("#"+column+" ul").append(html);
    }

    initUI();
}

function initUI(){
    $( ".sortable" ).sortable({
        revert: true,
        placeholder: "dashed-placeholder",
        connectWith: ".appointment_stack ul",
        start:function(event,ui){
            this.start_col = $(ui.item).parent().parent().attr("id");
            this.start_index = ui.item.index();
        },
        stop:function(event,ui){
            this.end_col = $(ui.item).parent().parent().attr("id");
            this.end_index = ui.item.index();
            var data = {
                start_index:this.start_index,
                end_index:this.end_index,
                start_col:this.start_col,
                end_col:this.end_col
            };
            updateStatusActivity(data,ui)
        }
    });

    $(".draggable").disableSelection();
}


function updateStatusActivity(data, ui){

    if(data.start_index == data.end_index && data.start_col == data.end_col){
        return;
    }

    var activity_id = $(ui.item).attr("activity");
    var new_col = data.end_col;
    var current_in_consult = null;

    if($("#waiting .in_consult")[0]){
        current_in_consult = $($("#waiting .in_consult")[0]).attr("activity");

    }

    var params = {
        activity_id : activity_id,
        dragged:true
    };

    var lbl_class = "";
    var lbl_name = "";
    $(ui.item).removeClass("in_consult");
    if(new_col == "pending"){
        params.activity_status_id = act_status.created;
        lbl_class = "label-warning";
        lbl_name = "AGENDADO";
    }else if(new_col == "waiting"){
        params.activity_status_id = act_status.waiting;
        lbl_class = "label-info";
        lbl_name = "EN ESPERA";

        if($(ui.item).is(':first-child')){
            params.activity_status_id = act_status.consult;
            lbl_name = "EN CONSULTA";
            $(ui.item).addClass("in_consult");
        }
    }else if(new_col == "attended"){
        params.activity_status_id = act_status.attended;

        lbl_class = "label-success";
        lbl_name = "ATENDIDO";
    }

    $("#status_"+activity_id).html(lbl_name);
    $("#status_"+activity_id).attr("class","label "+lbl_class);

    if(new_col == "waiting" && data.end_index == 0){
        // caso 1. Un elemento fue movido al principio de la lista
        console.log("new in consult");

        if(current_in_consult != null){
            params.old_in_consult = current_in_consult;
            $("#activity_"+current_in_consult).removeClass("in_consult");
            $("#status_"+current_in_consult).html("EN ESPERA");
            $("#status_"+current_in_consult).attr("class","label label-info");
        }

    }

    $.ajax({
        url:"/doctor/activities/update_status",
        data:params,
        dataType:"json"
    });


    /* caso 2: si el elemento que movieron era el que estaba en consulta y hay un nuevo elemento al principio de la lista de espera */
    if(data.start_col == "waiting" && data.start_index == 0){
        if($("#waiting li")[0]){
            var new_in_consult = $($("#waiting li")[0]).attr("activity");
            params = {
                dragged:true,
                activity_id:new_in_consult,
                activity_status_id:act_status.consult
            };

            $("#activity_"+new_in_consult).addClass("in_consult");
            $("#status_"+new_in_consult).html("EN CONSULTA");
            $("#status_"+new_in_consult).attr("class","label label-info");

            $.ajax({
                url:"/doctor/activities/update_status",
                data:params,
                dataType:"json"
            });
        }
    }
}







