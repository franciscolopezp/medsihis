// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require jquery
//= require jquery-ui
//= require jquery.session.js
//= require jquery_ujs
//= require jquery.mask.min
//= require twitter/bootstrap
//= require select2/select2.min
//= require flot/jquery.flot
//= require flot/jquery.flot.pie
//= require flot/jquery.flot.resize
//= require flot/jquery.flot.navigate
//= require flot/jquery.flot.tooltip.min
//= require flot.curvedlines/curvedLines.js
//= require color/jqColor.js
//= require color/jQueryColorPicker.min.js
//= require sparklines/jquery.sparkline.min.js
//= require jquery.easy-pie-chart/dist/jquery.easypiechart.min.js
//= require moment/moment.min
//= require moment/moment-timezone.min
//= require fullcalendar/fullcalendar.min.js
//= require simpleWheater/jquery.simpleWeather.min.js
//= require waves/waves.min.js
//= require bootstrap-growl/bootstrap-growl.min.js
//= require malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js
//= require demo_js/curved-line-chart.js
//= require demo_js/line-chart.js
//= require demo_js/charts.js
//= require demo_js/functions.js
//= require demo_js/demo.js
//= require bootstrap-datetimepicker/bootstrap-datetimepicker.min
//= require bootstrap-select/bootstrap-select
//= require fileinput/fileinput.min.js
//= require bootstrap-bootgrid/jquery.bootgrid.updated
//= require dataTables/jquery.dataTables
//= require bootstrap-sweetalert/sweet-alert.min.js
//= require ts_autocomplete
//= require ts_date_value
//= require demo_js/jquery.validate.js
//= require private_pub
//= require bootstrap-duallistbox
//= require raphael
//= require morris
//= require appointments_actions
//= require date-helper
//= require summernote/summernote

//= require dataTables/jquery.dataTables.min
//= require dataTables/dataTables.bootstrap.min
//= require dataTables/extensions/dataTables.buttons.min
//= require dataTables/extensions/buttons.html5.min
//= require dataTables/extensions/buttons.print.min
//= require dataTables/extensions/jszip.min
//= require dataTables/extensions/pdfmake.min
//= require dataTables/extensions/vfs_fonts
//= require dataTables/extensions/buttons.flash
//= require dataTables/extensions/buttons.colVis.min
//= require dataTables/extensions/dataTables.responsive.min

function openTermsModal(){
    $("#medsi_terms_conditions").modal("show");
}

function closeTermsModal(){
    $("#medsi_terms_conditions").modal("hide");
}

var fcMonthNames = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
    'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
var fcDayNames = ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'];
var fcDayNamesShort = ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'];

function clear_accent(text_string){
    var text = text_string.toLowerCase(); // a minusculas
    text = text.replace(/[áàäâå]/, 'a');
    text = text.replace(/[éèëê]/, 'e');
    text = text.replace(/[íìïî]/, 'i');
    text = text.replace(/[óòöô]/, 'o');
    text = text.replace(/[úùüû]/, 'u');
    text = text.replace(/[ýÿ]/, 'y');
    text = text.replace(/[ñ]/, 'n');
    text = text.replace(/[ç]/, 'c');
    text = text.replace(/['"]/, '');
    //text = text.replace(/[^a-zA-Z0-9-]/, '');
    /*text = text.replace(/\s+/, '-');
    text = text.replace(/' '/, '-');
    text = text.replace(/(_)$/, '');
    text = text.replace(/^(_)/, '');*/
    return text;
}

moment.locale('es', {
    months : "Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre".split("_"),
    monthsShort : "Ene._Fer._Mar_Abr._May_Jun_Jul._Ago_Sep._Oct._Nov._Dic.".split("_"),
    weekdays : "Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado".split("_"),
    weekdaysShort : "Dom._Lun._Mar._Mie._Jue._Vie._Sab.".split("_"),
    weekdaysMin : "Do_Lu_Ma_Mi_Ju_Vi_Sa".split("_"),});

    
var AUTOCOMPLETE_PATIENTS_URL = '';
var AUTOCOMPLETE_CITIES_URL = '';
var AUTOCOMPLETE_DISEASES_URL = '';
var AUTOCOMPLETE_MEDICAMENTS_URL = '';
var AUTOCOMPLETE_PHARMACY_ITEMS_URL = '';
var AUTOCOMPLETE_PERSONS_URL = '';
$(function(){

    AUTOCOMPLETE_PATIENTS_URL = $('#url_autocomplete_patients').val();
    AUTOCOMPLETE_CITIES_URL = $('#url_autocomplete_cities').val();
    AUTOCOMPLETE_DISEASES_URL = $('#url_autocomplete_medical_histories').val();
    AUTOCOMPLETE_MEDICAMENTS_URL = $('#url_autocomplete_medicaments').val();
    AUTOCOMPLETE_PERSONS_URL = $("#url_autocomplete_persons").val();
    AUTOCOMPLETE_DOCTORS_URL = $('#url_autocomplete_doctors').val();
    AUTOCOMPLETE_PHARMACY_ITEMS_URL = $("#url_autocomplete_pharmacy_items").val();

    $('input[type=text]').attr('autocomplete','off');
    $(".search-patient").ts_autocomplete({
        url:AUTOCOMPLETE_PATIENTS_URL,
        in_modal:true,
        items:10,
        required:true,
        model:"un paciente"
    });

    $(".search-city").ts_autocomplete({
        url:AUTOCOMPLETE_CITIES_URL,
        in_modal:true,
        items:10,
        required:true,
        model:"una ciudad"
    });

    $(".search-doctor").ts_autocomplete({
        url:AUTOCOMPLETE_DOCTORS_URL,
        in_modal:true,
        items:10,
        required:true,
        model:"un doctor"
    });

    $(".search-disease").ts_autocomplete({
        url:AUTOCOMPLETE_DISEASES_URL,
        in_modal:true,
        items:10,
        required:true,
        model:"una enfermedad"
    });
    $(".search-medicament").ts_autocomplete({
        url:AUTOCOMPLETE_MEDICAMENTS_URL,
        in_modal:true,
        items:10,
        required:true,
        model:"un medicamento"
    });
    $(".search-pharmacy-item").ts_autocomplete({
        url:AUTOCOMPLETE_PHARMACY_ITEMS_URL,
        in_modal:true,
        items:10,
        required:true,
        model:"un articulo de farmacia"
    });
/*    $(".search-person").ts_autocomplete({
        url:AUTOCOMPLETE_PERSONS_URL,
        in_modal:true,
        items:10,
        required:true,
        model:"una persona"
    });*/

    $('.date-picker-bottom').datetimepicker({
        format: 'DD/MM/YYYY',
        widgetPositioning: {
            horizontal: 'left',
            vertical: 'bottom'
        }
    });

    $("#forget_pass_btn").click(function(){
        $("#l-login").hide();
        $("#l-forget-password").show();
    });

    $("#return_reset_pass_btn").click(function(){
        $("#l-login").show();
        $("#l-forget-password").hide();
    });

});



function default_permissions(table){
    var can_edit = table.data('can-edit');
    var can_manage = table.data('can-manage');
    var can_print = table.data('can-print');
    var can_show = table.data('can-show');
    var can_invoice = table.data('can-invoice');
    var can_show_exp = table.data('can-show-exp');
    var can_delete = table.data('can-delete');
    if(!can_edit){
        table.find(".btn-edit").remove();
    }
    if(!can_print){
        table.find(".btn-print").remove();
    }
    if(!can_manage){
        table.find(".btn-manage").remove();
    }
    if(!can_invoice){
        table.find(".btn-invoice").remove();
    }
    if(!can_show_exp){
        table.find(".btn-show-exp").remove();
    }
    if(!can_show){
        table.find(".btn-show").remove();
    }
    if(!can_delete){
        table.find(".btn-delete").remove();
    }
}
function lookForTab() {
    if($("#medical_consultation_tab_header a").length > 0){
        clearInterval(lookTabInterval);
        $("#medical_consultation_tab_header a").click();
        lookBtnInterval = setInterval(lookForBtn,500);
    }

}

function lookForBtn(){
    if($("#btn-new-consultation").length > 0){
        clearInterval(lookBtnInterval);
        $("#btn-new-consultation").click();
    }
}

$(document).ready(function(){
    var url = window.location.href;
    if(url.indexOf("#abrir_nueva_consulta") != -1){
        lookTabInterval = setInterval(lookForTab, 500);
    }


    $.extend($.validator.messages, {
        required:"Este campo es requerido.",
        equalTo:"La contraseña es diferente.",
        email:"Por favor ingrese un email válido.",
    });

    $.validator.setDefaults({ ignore: '' });

    $("form.validate").validate();

    $("input.required").parent().find("label").prepend("<span class='c-red'>*</span> ");
});

function toCurrency(number){
    number = parseFloat(number);
    return "$"+ number.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

function showNotification(object) {
    var duration = 3; // seconds
    if (Notification) {

        if (Notification.permission !== "granted") {
            Notification.requestPermission()
        }
        var extra = {
            icon: notification_img_url,
            body: object.body
        }
        var noti = new Notification( object.title, extra)

        if(object.onclick != undefined){
            noti.onclick = object.onclick;
        }

        if(object.onclose != undefined){
            noti.onclose = object.onclose;
        }

        setTimeout( function() { noti.close() }, duration * 1000)
    }
}

function openChangeOfficeModal(isFirstlogin){
    $("#first_login").val(isFirstlogin);
    var office_count = $("#doc_num_office").val();
    if(office_count <= 0){
        swal("No se puede seleccionar un consultorio","Aun no tiene dado de alta un consultorio","error");
        return;
    }
    $("#change_office_modal").modal("show");
}

function setCurrentOffice(){
    var current_office = $("#set_new_current_office").val();

    if(current_office == null){
        swal("No se puede establecer el consultorio","Parece que no tiene permisos de corte de caja","error");
        return;
    }

    var url = $('#url_setCurrentOffice').val();
    $.ajax({
        url:url,
        data:{office_id:current_office},
        dataType:"json",
        success:function(resp){
            if(resp.done){
                if($("#first_login").val()=="1"){
                    window.location = $("#url_box_value").val();
                }else{
                    location.reload();
                }

            }
        }
    });
}


function changeStatusLinkRequest(request_id, new_status){
    $.ajax({
        method:"PUT",
        url:patient_expedients_path+"/"+request_id,
        dataType:"json",
        data:{
            new_status:new_status
        },
        success:function(data){
            $("#data_request_"+request_id).remove();
            swal(data.title,data.message,data.type);
            updateNotificationsLabel();
        }
    });

}

function  initHtmlEditor(selector,options){
    var height = 150;

    if(options != undefined && options.height != undefined){
        height = options.height;
    }

    $(selector).summernote({
        height:height,
        toolbar: [
            ['style', ['style','bold', 'italic', 'underline', 'clear']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'hr']],
            ['view', ['fullscreen', 'codeview']],
            ['help', ['help']]
        ],
        lineHeights: ['1.0', '1.2', '1.4', '1.5', '1.6', '1.8', '2.0', '3.0'],

    });

}


function deleteRecord(name, url, table, callback){
    swal({
            title: "¿Confirma que desea eliminar el registro \""+name+"\"?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        },
        function(){
            var ajax = $.ajax({
                type: 'DELETE',
                url: url
            });
            ajax.done(function(result, status){
                swal("Eliminado!", "El registro ha sido eliminado exitosamente.", "success");
                if(table != ""){
                    $(table).bootgrid("reload");
                }
                if(callback != undefined){
                    callback();
                }
            });

            ajax.fail(function(error, status, msg){
                swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
            });
        });
}



//-- DataTable --
$.extend( true, $.fn.dataTable.defaults, {
    dom: '<"row row-eq-height "<"col-sm-12 actionBar"<"search form-group"<"input-group"f>><"actions btn-group"lB>>>t<"row row-eq-height"<"col-md-6"p><"col-md-6"i>>',
    lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'Todo']],
    autoWidth: true,
    searching: true,
    ordering: true,
    processing: true,
    serverSide: true,
    deferRender: true,
    //stateSave: true,
    //select: true,
    responsive: true,
    //scrollX: true,
    language: {
        "processing": "Procesando",
        "lengthMenu": "_MENU_",
        "zeroRecords": "No se encontraron resultados",
        "emptyTable": "Ningún dato disponible en esta tabla",
        "info": "Mostrando del _START_ al _END_ de _TOTAL_ registros",
        "infoEmpty": "Mostrando del 0 al 0 de 0 registros",
        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
        "infoPostFix": "",
        "search": "",
        "url": "",
        "infoThousands": ",",
        "loadingRecords": "Cargando...",
        "paginate": {
            "previous": "<",
            "next": ">",
            "first": "...",
            "last": "..."
        }
    },
    paginationType: 'full_numbers',

    buttons: [
        { extend: 'colvis',
            text: '<i class="zmdi icon zmdi-view-module"></i>',
            className: 'btn btn-default center_column_visible',
            titleAttr: 'Mostrar/Ocultar columnas'
        },
        /*{
            extend: 'copy',
            text:      '<i class="zmdi zmdi-copy zmdi-hc-fw"></i>',
            className: 'btn btn-default',
            titleAttr: 'Copiar'
        },*/
        {
            extend:    'excelHtml5',
            text:      '<i class="zmdi zmdi-collection-text zmdi-hc-fw"></i>',
            className: 'btn btn-default',
            titleAttr: 'Excel',
            filename: function(){
                return $(".page_title").text();
            },
            /*exportOptions: {
                columns: ':visible'
            },*/

        },
        {
            extend: 'print',
            text:      '<i class="zmdi zmdi-print zmdi-hc-fw"></i>',
            className: 'btn btn-default',
            titleAttr: 'Imprimir'
        }
    ]
} );

function setPositionAndClassesOptionsDatatable(){

    $('.dataTables_filter').before('<i class="zmdi icon input-group-addon  zmdi-search "></i>');
    $('.dataTables_filter input').attr('placeholder', 'Buscar');
}

function initSelect2() {
    $(".search-cie-10").select2({
        minimumInputLength: 2,
        ajax: {
            url: $("#url_autocomplete_cie10").val(),
            dataType: 'json',
            type: "GET",
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
        }
    });
}

$(function () {
    $(document).on('click', '.checkbox_text_field .check', function(){
        if($(this).is(":checked")){
            $(this).parent().parent().find(".text").show();
        }else{
            $(this).parent().parent().find(".text").hide();
            $(this).parent().parent().find(".text").val("");
        }
    });
});



