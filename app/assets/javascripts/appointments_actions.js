/*Doctor layout approve appointments*/
var CANCEL_ACTIVITY_ID = 0;
var CANCEL_ACTIVITY_STATUS = 0;

// Se utilizan unicamente para mandar los mensajes de confirmación/cancelado/reagendado de la cita en las notificaciones
var CONFIRMED_MESSAGE = 1;
var RESCHEDULE_MESSAGE = 3;
var CANCEL_MESSAGE = 5;

var activity_status = {
    scheduled:1
};
function openCancelationModal(activity_id, status_id){
    new_schedule_params.new_schedule = false;
    CANCEL_ACTIVITY_ID = activity_id;
    CANCEL_ACTIVITY_STATUS = status_id;
    $("#cancel_appointment_modal").modal("show");
    $("#appointment_cancel_details").val("");
}

function confirmCancelAppointment(){
    $("#cancel_appointment_modal").modal("hide");
    updateAppointmentStatus(CANCEL_ACTIVITY_ID,CANCEL_ACTIVITY_STATUS);
}

function updateNotificationsLabel(){
    var count = $("#pending_appointments_list .lv-item").length;

    $("#pending_appointments_count").html(count);
    if(count == 0){
        $("#pending_appointments_count").hide();
    }else{
        $("#pending_appointments_count").show();
    }
}

function updateWaitingCount(){
    var count = $("#waiting_appointments_list .lv-item").length;

    $("#waiting_count").html(count);
    if(count == 0){
        $("#waiting_count").hide();
    }else{
        $("#waiting_count").show();
    }
}

function drawPendingAppointment(activity){

    $("#data_appointment_"+activity.id).remove();

    var start_date = stringToDate(activity.s_date,"yyyy-mm-dd hh:mn");
    var end_date = stringToDate(activity.e_date,"yyyy-mm-dd hh:mn");

    // values to display
    var date_str = dateToString(start_date,"dd/fm/yyyy");
    var start_hour = dateToString(start_date,"hh:mn pp");
    var end_hour = dateToString(end_date,"hh:mn pp");

    var html = "";
    html += '<a class="lv-item data_appointment" id="data_appointment_'+activity.id+'">';
    html += '<div class="media">';
    html += '<div class="media-body">';
    html += '<div class="lv-title">'+activity.title+'</div>';
    html += '<small class="lv-small" style="color: #3d3d3d;">';
    if(activity.user_patient_phone != ""){
        html += 'Teléfono: '+activity.user_patient_phone+'<br>';
    }
    html += 'Fecha: '+date_str+'<br>';
    html += 'Hora: '+start_hour+' - '+end_hour+'<br>';
    html += '<span class="actions_pending_appointment" style="font-size: 18px">';
    html += '<i class="zmdi zmdi-check-circle zmdi-hc-fw" title="Aprobar"';
    html += 'onclick="updateAppointmentStatus('+activity.id+','+act_status.created+')"></i>';
    html += '<i class="zmdi zmdi-close-circle zmdi-hc-fw" title="Cancelar"';
    html += 'onclick="openCancelationModal('+activity.id+','+act_status.cancelled+')"></i>';
    html += '<i class="zmdi zmdi-calendar-alt zmdi-hc-fw" title="Reagendar"';
    html += 'onclick="openNewScheduleModal('+activity.id+',\''+activity.title+'\',\''+dateToString(start_date,"yyyy-mm-dd hh:mn")+'\',\''+dateToString(end_date,"yyyy-mm-dd hh:mn")+'\')"></i>';
    html += '</span>';
    html += '</small>';
    html += '</div>';
    html += '</div>';
    html += '</a>';

    $("#pending_appointments_list").append(html);

    updateNotificationsLabel();

}


function drawWaitingNotification(activity){
    var html = '<a class="lv-item data_request" id="waiting_patient_'+activity.id+'">'
        +'<div class="media">'
        +'<div class="media-body">'
        +'<div class="lv-title">'+activity.title+'</div>'
        +'<small class="lv-small" style="color: #3d3d3d;">';

    if(activity.patient_name != ""){
        html += activity.patient_name+"<br>";
    }

    html += 'Hora: '+dateToString(activity.start,"hh:mn pp")+'<br>'
        +'</small>'
        +'</div>'
        +'</div>'
        +'</a>';

    $("#waiting_appointments_list").append(html);

    updateWaitingCount();
}

function openNewScheduleModal(activity_id,title, start,end){
    var start_date = stringToDate(start,"yyyy-mm-dd hh:mn");
    var end_date = stringToDate(end,"yyyy-mm-dd hh:mn");

    var displayDate = dateToString(start_date,"dd/fm/yyyy");
    var inputDate = dateToString(start_date,"dd/mm/yyyy");
    var sHour = dateToString(start_date,"hh:mn pp");
    var eHour = dateToString(end_date,"hh:mn pp");


    $("#new_schedule_activity_id").val(activity_id);

    $("#new_schedule_activity_name").html(title);

    $("#new_schedule_activity_date").html(displayDate);
    $("#new_schedule_activity_start_time").html(sHour);
    $("#new_schedule_activity_end_time").html(eHour);

    $("#new_schedule_start_date").val(inputDate);
    $("#new_schedule_start_time").val(sHour);
    $("#new_schedule_end_time").val(eHour);

    $("#new_schedule_appointment_modal").modal("show");
}

var new_schedule_params = {
    start_date:"",
    end_date:"",
    new_schedule:false
};

function setNewSchedule(){
    var activity_id = $("#new_schedule_activity_id").val();

    new_schedule_params.start_date = getNewScheduleDate($("#new_schedule_start_date").val(),$("#new_schedule_start_time").val());
    new_schedule_params.end_date = getNewScheduleDate($("#new_schedule_start_date").val(),$("#new_schedule_end_time").val());
    new_schedule_params.new_schedule = true;

    $("#new_schedule_appointment_modal").modal("hide");

    updateAppointmentStatus(activity_id, activity_status.scheduled);
}

function showMessageNotificationMedicalAppointment(status){

    switch(parseInt(status)){
        case CONFIRMED_MESSAGE:
            swal(" Se ha notificado por correo electrónico al paciente la confirmación de la cita ");
            break;
        case RESCHEDULE_MESSAGE:
            swal(" Se ha notificado por correo electrónico al paciente el cambio de horario de la cita ");
            break;
        case CANCEL_MESSAGE:
            swal(" Se ha notificado por correo electrónico al paciente la cancelación de la cita ");
            break;
        default:

            break;
    }
}

function updateAppointmentStatus(activity_id, new_status_id){
    var params = {
        activity_id : activity_id,
        start_date: new_schedule_params.start_date,
        end_date: new_schedule_params.end_date,
        activity_status_id: new_status_id,
        dragged: false,
        send_email: true,
        new_schedule: new_schedule_params.new_schedule,
        message: $("#appointment_cancel_details").val()
    };
    $.ajax({
        url:"/doctor/activities/update_status",
        data:params,
        dataType:"json",
        success:function(data){
            var result = JSON.parse(data);
            new_schedule_params.new_schedule = false;
            $("#data_appointment_"+activity_id).remove();
            updateNotificationsLabel();
            if(result.type_message_appointment != ""){
                showMessageNotificationMedicalAppointment(result.type_message_appointment);
            }
        }
    });
}



function getNewScheduleDate(str1,str2){
    // str1 debe tener formato dd/MM/yyyy
    // str2 debe tener formato hh:mm a  formato 12 hrs
    var dt1   = parseInt(str1.substring(0,2));
    var mon1  = parseInt(str1.substring(3,5));
    var yr1   = parseInt(str1.substring(6,10));

    var hourComponents = str2.split(" ");
    var hour = hourComponents[0];
    var format = hourComponents[1];
    var hComp = hour.split(":");

    var h = parseInt(hComp[0]);
    var m = parseInt(hComp[1]);

    if(format == "PM" && h != 12){
        h += 12;
    }else if(format == "AM" && h == 12){
        h = 0;
    }
    return new Date(yr1, mon1-1, dt1,h,m,0);
}