var monthNames = ["Enero","Febrero","Marzo","Abril","Mayo","Junio",
    "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];


function dateToString(date,format) {

    var year = date.getFullYear();
    var month = date.getMonth();
    var day  =date.getDate();
    var hour = date.getHours();
    var minutes = date.getMinutes();
    var monthName = monthNames[month];
    var period = "AM";
    month++;

    if(format.indexOf("pp") != -1){
        if(hour > 12){
            hour = hour - 12
            period = "PM";
        }else if(hour == 12){
            period = "PM";
        }
    }

    if(day < 10){
        day = "0"+day;
    }

    if(month < 10){
        month = "0"+month;
    }

    if(hour < 10){
        hour = "0"+hour;
    }

    if(minutes < 10){
        minutes = "0"+minutes;
    }

    format = format.replace(/dd/g, day);
    format = format.replace(/mm/g, month);
    format = format.replace(/yyyy/g, year);
    format = format.replace(/hh/g, hour);
    format = format.replace(/mn/g, minutes);
    format = format.replace(/pp/g, period);

    format = format.replace(/fm/g, monthName);

    return format;

}


function stringToDate(string,current_format){
    var year = 0, month = 0, day = 0, hour = 0, minute = 0;
    switch (current_format){
        case "yyyy-mm-dd hh:mn":
            var date_cmps = string.split(" ");
            var d_array = date_cmps[0].split("-");
            var h_array = date_cmps[1].split(":");
            year = d_array[0];
            month = parseInt(d_array[1]);
            day = d_array[2];
            hour = h_array[0];
            minute = h_array[1];
            month--;
            break;
        case "yyyy-mm-dd":
            var d_array = string.split("-");
            year = d_array[0];
            month = parseInt(d_array[1]);
            day = d_array[2];
            month--;
            break;
        case "dd/mm/yyyy hh:mn pp":
            var date_cmps = string.split(" ");
            var d_array = date_cmps[0].split("/");
            var h_array = date_cmps[1].split(":");
            year = d_array[2];
            month = parseInt(d_array[1]);
            day = d_array[0];
            hour = parseInt(h_array[0]);
            minute = h_array[1];
            var period = date_cmps[2];

            if((period == "pm" || period == "PM") && hour != 12){
                hour += 12;
            }

            month--;
            break;
        case "dd/mm/yyyy":
            var d_array = string.split("/");
            year = d_array[2];
            month = parseInt(d_array[1]);
            day = d_array[0];

            month--;
            break;
    }

    return new Date(year,month,day,hour,minute);
}

function dateServerFormat(date){
    return dateToString(date,"yyyy-mm-dd hh:mn");
}