/**
 * Created by equipo-01 on 2/06/16.
 */
var GRAPH_WEIGHT = 4;
var GRAPH_IMC = 3;
var GRAPH_HEAD_CIRCUMFERENCE = 2;
var GRAPH_SIZE_LENGTH = 1;
function initGraphPercentile(){

    $( ".main_menu_percentile" ).click(function() {
        //console.log($(this).data("section"));
        switch (parseInt($(this).data("section"))){
            case GRAPH_WEIGHT:
                setTimeout(graphWeight, 1);
                break;
            case GRAPH_HEAD_CIRCUMFERENCE:
                var type_percentile = $('#type_data_percentile').val();
                var type_cdc = 2;
                if(parseInt(type_percentile) == type_cdc){
                    // es imc por ser el tipo de datos cdc
                    setTimeout(graphIMC, 1);
                }else{
                    setTimeout(graphHeadCircumference, 1);
                }

                break;
            case GRAPH_SIZE_LENGTH:
                setTimeout(graphSizeLength, 1);
                break;
           /* case GRAPH_IMC:
                console.log("Tabla de imc");
                setTimeout(graphIMC, 1);
                break; */
            default:
                console.log("No se encontro el tab");
            break;
        }
    });

    $('.selectpicker').selectpicker();
    $("a[href='#graph-size-length']").trigger('click');
}
function print_ped_graph(medical_expedient_id, type){
    url_pdf = $('#url_print_ped_graph').val()+"?id="+medical_expedient_id+"&type="+type+"&data="+$('#type_data_percentile').val();
    window.open(url_pdf, '_blank');
}
function showTapsOMS(){
    var type_percentile = $('#type_data_percentile').val();
    var type_cdc = 2;
    if(parseInt(type_percentile) == type_cdc){
        // ocultar circunferencia de la cabeza y mostrar IMC
       $('#text_tab_percentile_imc_head').text('Indice de masa corporal');
    }else{
        $('#text_tab_percentile_imc_head').text('Circunferencia de la cabeza');
        // ocultar IMC y mostrar Circun...
    }
    $("a[href='#graph-size-length']").trigger('click');
}

function generateGraphLength(){
    $('#table_legth').click();
}

function graphWeight(){
    var url = $('#url_percentile_weight').val();
    var initYAxis = 0;
    callDataToGraph(url,'container_graph_weight', 'Peso', 'Peso (Kgs)','Kg', initYAxis);
}

function graphIMC(){
    var url = $('#url_percentile_imc').val();
    var initYAxis = 0;
    callDataToGraph(url,'container_graph_head', 'Indice de masa corporal', 'IMC (Kg/m²)','Kg/m²', initYAxis);
}

function graphSizeLength(){
    var url = $('#url_percentile_length').val();
    var initYAxis = 50;
    callDataToGraph(url, 'container_graph_length', 'Altura', 'Altura (Cms)', 'Cm', initYAxis);
}

function graphHeadCircumference(){
    var url = $('#url_percentile_head').val();
    var initYAxis = 30;
    callDataToGraph(url,'container_graph_head', 'Circunferencia de la cabeza', 'Circunferencia (Cms)', 'Cm', initYAxis);
}

function callDataToGraph(url, container,title, titleY, unit, minYAxis){
    var type_percentile = $('#type_data_percentile').val();
    $.ajax({
        url:url,
        method:"get",
        dataType:"json",
        data:{expedient_id: $('#expedient_id').val(), type_data: type_percentile},
        success:function(data){

            graph_percentile(data,container,title, titleY, unit, minYAxis);
        },
        error:function(error){
            console.log("ERROR en los percentiles");
        }
    });
}



function graph_percentile(Data, container, title, titleY, unit, minYAxis){
    Highcharts.setOptions({
        lang: {
            resetZoom: "Restablecer Zoom"
        }});
    var chart = new Highcharts.Chart({
        chart: {
            type: 'line',
            renderTo: container,
            zoomType: 'xy',
            events: {
                load: function() {
                    //$('.highcharts-scrollbar tspan').text("Edgar")
                }
            }
        },
        title: {
            text: 'Percentiles de '+title,
            x: -20 //center
        },
        subtitle: {
            text: Data.subtitle_graph,
            x: -20
        },
        xAxis: {
            title: {
                text: 'Años'
            },
            labels: {
                formatter: function() {
                    return this.value +' años'
                }
            }
            //categories: ['1 año', '2 años', '3 años', '4 años', '5 años']
        },
        yAxis: {
            scrollbar: {
                enabled: true
            },
            title: {
                text: titleY
            },
            labels: {
                formatter: function() {
                    return this.value +' '+unit
                }
            },
            min: minYAxis
        },
        tooltip: {
            crosshairs: [{
                width: 1.5,
                dashStyle: 'solid',
                color: 'blue'
            },{
                width: 1.5,
                dashStyle: 'solid',
                color: 'blue'
            }],
            headerFormat: '<b>{point.x:.0f} años</b><br>',
            valueSuffix: ' '+unit
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 10
        },
        scrollbar: {
            enabled: true
        },
        plotOptions: {
            line: {
                marker: {
                    enabled: false
                }
            }
        },
        series: Data.data
    });
}
