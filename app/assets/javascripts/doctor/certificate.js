jQuery(document).ready(function() {

    init_form_type_certificate();
    init_form_certificate();

});

function init_form_certificate(){

    if($("#data-table-certificate").length){
        var grid = $("#data-table-certificate").bootgrid({
            ajax: true,
            ajaxSettings: {
                method: "GET",
                cache: false
            },
            url: $('#data-table-certificate').data('source'),
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "commands": function(column, row) {
                    var options = "";

                    options += "<a href=\"" + row.show_url + "\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Ver\"><i class=\"zmdi zmdi-eye\"></i></a>";

                    options += "<a href=\"" + row.print_url + "\" target=\"_blank\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Imprimir\"><i class=\"zmdi zmdi-print\"></i></a>";

                    options += "<a href=\"" + row.edit_url + "\" class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>";

                    options += "<button data-url-delete=\"" + row.destroy_url + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
                    return options;
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function () {
            /* Executes after data is loaded and rendered */
            grid.find(".btn-delete").on("click", function (e) {
                var $tr_parent=$(this).parents("tr").first();
                var name = $tr_parent.children('td').eq(1).text();
                //var patient_id = $tr_parent.children('td').eq(0).text();
                var patient_id = $(this).data('row-id');
                var url_aux = $(this).data("url-delete");
                var text_confirm = "¿Esta seguro que quiere eliminar el certificado "+name+"?";
                swal({
                        title: text_confirm,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Eliminar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false
                    },
                    function(){
                        var ajax = $.ajax({
                            type: 'DELETE',
                            url: url_aux
                        });
                        ajax.done(function(result, status){
                            if(result.result){
                                swal("Eliminado!", "El Certificado ha sido eliminado exitosamente.", "success");
                                $('#data-table-certificate').bootgrid('reload');
                            }else{
                                swal("Eliminado!", result.message, "info");
                            }

                        });

                        ajax.fail(function(error, status, msg){
                            swal("Error!", "Ha ocurrido un error.", "error");
                            console.log("Error: " + error.responseText + msg);
                        });

                    });
            });
        });
    }

    if($('#container_html_preview').length){
        var code = $('#certificate_container_html').val();
        init_html_editor_certificate('container_html_preview',code);
        //set_value_type_certificate();
    }
}

function set_value_type_certificate(){
    var id = $('#certificate_type_certificate_id').val();
    if(id != ""){
        var url_aux = $('#url_type_certificate_container').val();
        var ajax = $.ajax({
            type: 'get',
            url: url_aux,
            data: {id: id}
        });
        ajax.done(function(result, status){
            $('#container_html_preview').summernote().code(result.container_html);
        });
    }
}

function init_form_type_certificate(){
    if($("#data-table-type-certificate").length){
        var grid = $("#data-table-type-certificate").bootgrid({
            ajax: true,
            ajaxSettings: {
                method: "GET",
                cache: false
            },
            url: $('#data-table-type-certificate').data('source'),
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "commands": function(column, row) {
                    var options = "";

                    options += "<a href=\"" + row.show_url + "\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Ver\"><i class=\"zmdi zmdi-eye\"></i></a>";

                    options += "<a href=\"" + row.edit_url + "\" class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>";

                    options += "<button data-url-delete=\"" + row.destroy_url + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
                    return options;
                }
            }
        }).on("loaded.rs.jquery.bootgrid", function () {
            /* Executes after data is loaded and rendered */
            grid.find(".btn-delete").on("click", function (e) {
                var $tr_parent=$(this).parents("tr").first();
                var name = $tr_parent.children('td').eq(1).text();
                //var patient_id = $tr_parent.children('td').eq(0).text();
                var patient_id = $(this).data('row-id');
                var url_aux = $(this).data("url-delete");
                var text_confirm = "¿Esta seguro que quiere eliminar el tipo "+name+"?";
                swal({
                        title: text_confirm,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Eliminar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false
                    },
                    function(){
                        var ajax = $.ajax({
                            type: 'DELETE',
                            url: url_aux
                        });
                        ajax.done(function(result, status){
                            if(result.result){
                                swal("Eliminado!", "El tipo de certificado ha sido eliminado exitosamente.", "success");
                                $('#data-table-type-certificate').bootgrid('reload');
                            }else{
                                swal("Eliminado!", result.message, "info");
                            }

                        });

                        ajax.fail(function(error, status, msg){
                            swal("Error!", "Ha ocurrido un error.", "error");
                            console.log("Error: " + error.responseText + msg);
                        });

                    });
            });
        });
    }



    if($('#container_html_edit').length){
        var code = $('#type_certificate_container_html').val();
        init_html_editor_certificate('container_html_edit',code);
    }

}

function init_html_editor_certificate(id,code){
    // init html editor
    var id_aux = "#"+id;

    $(id_aux).summernote({
        height:350,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['insert',['link','table','hr']],
            ['fontstyle',['fontname','fontsize']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
        disableDragAndDrop:true,
    });
    $(id_aux).summernote().code(code);

}

function set_value_container(){
  var html = $('#container_html_edit').summernote().code();
    $('#type_certificate_container_html').val(html);
}

function set_value_container_certificate(){
    var html = $('#container_html_preview').summernote().code();
    $('#certificate_container_html').val(html);
}