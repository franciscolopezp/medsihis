var ROWS_BY_PAGE = 5;

function initComponentsConsultationsUI(){
    $("#btn-new-consultation").click(loadFormNewConsultation);

    //setupByPaymentMethod();
    //$("#charge_payment_method").change(setupByPaymentMethod);
    $('body').on('click', '.medical-consultation', function(e){
        e.preventDefault();
        x = $(this).closest('.lv-header-alt').find('.lvh-search');

        x.fadeIn(300);
        x.find('.lvhs-input').focus();
    });

    $('body').on('click', '.search-consultation', function(){
        x = $(this).closest('.lv-header-alt').find('.lvh-search');

        x.fadeOut(300);
        setTimeout(function(){
            var search = x.find('.lvhs-input').val();
            if(search != ""){
                getMedicalConsultation('',1, true);
            }
            x.find('.lvhs-input').val('');

        }, 350);
    });

    $('#search_medical_consultation').keyup(function() {
        delay(function(){
            var phrase = $('#search_medical_consultation').val();
            getMedicalConsultation(phrase, 1, true);
        }, 800 );
    });

    $('.refresh-info-consultation').click(function(e)
    {
        e.preventDefault();
        loadConsultationsList();
        //getMedicalConsultation($('#search_medical_consultation').val(),1);
    });
    $('.first-medical-consultation').click(function(e)
    {
        e.preventDefault();
        // searchLastConsultation(); // del mas actual al mas antiguo
        getMedicalConsultation('',1, false); // del mas antiguo al mas actual
    });



}

function loadConsultationsList(){
    var url_list_consultations = $("#url_consultations_list").val();
    var searchPhrase = $('#search_medical_consultation').val();
    var page = 1;
    var order = "DESC";
    $.ajax({
        type: "get",
        url: url_list_consultations,
        data: {searchPhrase: searchPhrase, current: page, rowCount: 5, "sort[date_consultation]": order},
        dataType:'html',
        success: function (response) {
            $("#container_medical_consultation").html(response);
            addFunctionListMedicalConsultation();
            var infoConsultation = JSON.parse($('#info_consultation').val());
            updatePages(infoConsultation.total,infoConsultation.current);
        },
        error: function (error) {
            swal("Error!", "Error al buscar consultas", "error");
        }
    });
}

function addFunctionListMedicalConsultation(){
//boton imprimir
    $('.print_btn').click(function(){
        var url_has_template = $('#url_has_template_consultation').val();
        var consultation = $(this).data("id");
        var url_print = $(this).data("url");
        $.ajax({
            type: "get",
            url: url_has_template,
            dataType: 'json',
            data: {consultation_id: consultation},
            success: function (response2) {
                if (response2.result) {
                    window.open(url_print);
                }else{

                }
            }
        });
//        window.open(url_print);
    });

    //ajax para actualizar la bandera de imprimir diagnostico
    $('.is_print_diagnostic').click(function(){
        var id = $(this).val();
        var url = $('#url_update_print').val();
        var flag = false;
        if ($(this).is(":checked")){
            flag = true;
        }
        $.ajax({
            type: "put",
            url:url,
            dataType:"json",
            data:{
                id: id,
                flag: flag
            },
            success:function(data){
            }
        });
    });

    // se inicializa el tooltip
    $('[data-toggle="tooltip"]').tooltip({
        'selector': '',
        'placement': 'top',
        'container':'body'
    });

}

function loadFormNewConsultation(){
    $("#list_error_medical_consultation").html("");
    $("#btn-new-consultation").parent().parent().hide();
    var url = $('#url_new_consultation').val();
    $.ajax({
        type: "get",
        url: url,
        dataType:'html',
        success: function (response) {
            $("#form-new-medical-consultation").html(response);
            $("#new_consultation_form_holder").slideDown();
            initElementsConsultationForm();
        },
        error: function (error) {
            swal("Error","Error al cargar el formulario de consulta","error");
        }
    });
}


function closeNewConsultationForm(){
    $("#new_consultation_form_holder").slideUp();
    $("#btn-new-consultation").parent().parent().show();
}


var consultation_data = {
    diseases:[],
    medicaments:[]
};

/******  NEW CONSULTATION *******/
function initElementsConsultationForm(){
    consultation_data.diseases = [];  //se inicializa el arreglo de enfermedades
    consultation_data.medicaments = [];  //se inicializa el arreglo de medicamentos

    $(".search-disease").ts_autocomplete({ // inicializar campo para buscar enfermedades
        url:AUTOCOMPLETE_DISEASES_URL,
        in_modal:true,
        items:10,
        required:false,
        model:"una enfermedad",
        callback:function(id, name){
            consultation_data.diseases.push({
                name:name,
                id:id
            });

            setTimeout(function () {
                $("#consultation_disease").val("");
                $("#consultation_disease").focus();
            },100   );

            $("#consultation_disease_ts_autocomplete").val("");
            $("#consultation_disease_ts_autocomplete_text").val("");

            drawDiseasesTable();
        }
    });

    $(".search-medicament").ts_autocomplete({
        url:AUTOCOMPLETE_MEDICAMENTS_URL,
        in_modal:true,
        items:10,
        required:false,
        model:"un medicamento"
    });

    $("#add-medicament").unbind();
    $('#add-medicament').click(addMedicamentToTable);


    $('#div_fpp_consultation').empty();

    if($("#pregnancies_current_data")[0]){
        setTimeout(function () {
            var data = JSON.parse($("#pregnancies_current_data").val());
            generatePregnancyCharts(true,data);
        },500);
    }

    $("#show_vital_sign_btn").click(function(){
        $("#vital_sign_section").toggle();
        $('html, body').animate({
            scrollTop: $("#vital_sign_section").offset().top
        }, 500);
        $("#div_fpp_consultation").html("");

        if($("#vital_sign_section").is(":visible")){
            $("#div_fpp_consultation").parent().parent().attr("class","col-md-6");
        }else{
            $("#div_fpp_consultation").parent().parent().attr("class","col-md-12");
        }

        if($("#pregnancies_current_data")[0]){
            var data = JSON.parse($("#pregnancies_current_data").val());
            generatePregnancyCharts(true,data);
        }

    });

    $("#show_valoration_btn").click(function(){
        $("#valoration_section").toggle();
        $('html, body').animate({
            scrollTop: $("#valoration_section").offset().top
        }, 500);
    });

    $("#show_general_anotations_btn").click(function(){
        $("#anotations_section").toggle();
        $('html, body').animate({
            scrollTop: $("#anotations_section").offset().top
        }, 500);
    });

    $("#btn_add_valoration").click(function () {
        loadValorationFields();
        if($("#valoration_type_select").val()==1){
            $.ajax({
                url:$("#url_get_current_pv").val(),
                data:{patient_id:$("#consult_patient_id").val()},
                success: function(data){
                    if(data.done){
                       $.each(data.pvs, function(idx,info){
                           $("input[field_id=" + info.field_id +"]").val(info.data);
                       });
                    }
                }
            });
        }
    });

    initTemplateComponents();

    /********* codigo del SENSEI y parte de la antigua UI  ***********/

    var dateNow = new Date();
    var month = dateNow.getMonth()+1;
    var day = dateNow.getDate();

    var hours = dateNow.getHours();
    var minutes = dateNow.getMinutes();

    var output = (day<10 ? '0' : '') + day +'/'+ (month<10 ? '0' : '') + month  + '/' +
        dateNow.getFullYear() + ' ' + hours+':'+minutes;

    $('#medical_consultation_date_consultation').val(output);

    $("#medical_consultation_date_consultation").on("dp.change", function (e) {
        var date = $('#medical_consultation_date_consultation').val();
        var dateArray = date.split(' ');
        if (DATE_CONSULTATION_BEFORED != dateArray[0]){
            DATE_CONSULTATION_BEFORED = dateArray[0];
            $('#medical_consultation_date_consultation').data("DateTimePicker").hide();
        }
    });
    var dateAux = $('#medical_consultation_date_consultation').val();
    DATE_CONSULTATION_BEFORED = dateAux.split(' ')[0];
    $('#medical_consultation_date_consultation').click(function(){
        $('#medical_consultation_date_consultation').data("DateTimePicker").show();
    });

    $("#btn-save-consultation").unbind();
    $('#btn-save-consultation').click(function(e)
    {
        //validar que la fecha de la consulta no sea menor a la fecha de última regla
        if($("#pregnancy_last_menstruation_date_hidden").val() != undefined){
            var lastMenstruationDate = stringToDate($("#pregnancy_last_menstruation_date_hidden").val(),"dd/mm/yyyy");
            var dateConsultation = getDate($('#medical_consultation_date_consultation').val());
            if(dateConsultation < lastMenstruationDate){
                swal("Error","No se pueden agregar consultas anteriores a la fecha de última regla.","error");
                return;
            }
        }
        //bloquear el boton de guardar, mostrar loading
        $('#loading_btn_save').show();
        saveMedicalConsultation();
    });


    //cargar los json de los consultorios
    COSTS = jQuery.parseJSON($('#consultations_costs').val());
    //agregar el costo de la consulta seleccionada
    $('#medical_consultation_price').val(getPriceOfficeSelected());
    //evento cuando cambia de consultorio
    $( "#medical_consultation_office" ).change(function() {
        $('#medical_consultation_price').val(getPriceOfficeSelected());
    });
    // evento que cambia tipo de consulta
    $( "#medical_consultation_consultation_type_id" ).change(function() {
        $('#medical_consultation_price').val(getPriceOfficeSelected());
    });

    $("#btn-calculation-imc").unbind();
    $('#loading_table_imc').hide();
    $('#error_weight_height').hide();
    $('#error_generate_table_imc').hide();
    //set event click to btn calculation IMC
    $('#btn-calculation-imc').click(function(e){
        $('#loading_table_imc').show();
        $('#error_weight_height').hide();
        $('#error_generate_table_imc').hide();
        var weight = $('#medical_consultation_vital_sign_attributes_weight').val();
        var height = $('#medical_consultation_vital_sign_attributes_height').val();
        if(weight != '' && weight.length > 0 && height != '' && height.length > 0){
            loadingTableIMC();
        }else{
            $('#loading_table_imc').hide();
            $('#error_generate_table_imc').hide();
            $('#error_weight_height').show();
        }
    });
    $('#table_imc_consultation').hide();



    initHtmlEditor("#general_notes_consult");
}

function loadValorationFields(){
    $.ajax({
        url:$("#url_new_valoration").val(),
        data:{valoration_type_id:$("#valoration_type_select").val()},
        success: function(data){
            $("#valoration_fields").append(data);
            $(".delete_new_valoration").click(function(){
                $(this).parent().parent().parent().remove();
            });
        }
    });
}


function getPriceOfficeSelected(){

    var office = parseInt($('#medical_consultation_office').val());
    var type = parseInt($("#medical_consultation_consultation_type_id").val());
    var cost = "00.00";
    var is_editable = false;
    if ($("#is_doctor_consult").val()==1){
        $.each(COSTS,function(idx,c_cost){
            if(c_cost.consultation_type_id == type){
                cost = parseFloat(c_cost.cost).toFixed(2);
                is_editable = c_cost.is_editable_price;
            }
        });
    }else{
        $.each(COSTS,function(idx,c_cost){
            cost = parseFloat(c_cost.cost).toFixed(2);
            is_editable = c_cost.is_editable_price;
        });
    }
    if(is_editable){
        $('#medical_consultation_price').attr('readonly', true);
    }else{
        $('#medical_consultation_price').attr('readonly', false);
    }
    return cost;
}

function drawDiseasesTable(){
    var table = "#diseases-table";
    $(table).html("");
    $.each(consultation_data.diseases,function(idx, item){
        $(table).append("<tr>" +
            "<td style='width: 70%'>"+item.name+"</td>" +
            "<td><i idx='"+idx+"' class='zmdi zmdi-delete delete_disease_consult'></i></td>" +
            "</tr>");
    });

    $(".delete_disease_consult").click(function(){
        var idx = $(this).attr("idx");
        consultation_data.diseases.splice(idx,1);
        drawDiseasesTable();
    });
}


function drawMedicamentsTable(){
    var table = "#medicaments-table";
    $(table).html("");
    $.each(consultation_data.medicaments,function(idx, item){
        $(table).append("<tr>" +
            "<td style='width: 45%'>"+item.name+"</td>" +
            "<td style='width: 40%'>"+item.dose+"</td>" +
            "<td><i idx='"+idx+"' class='zmdi zmdi-delete delete_medicament_consult'></i></td>" +
            "</tr>");
    });

    $(".delete_medicament_consult").click(function(){
        var idx = $(this).attr("idx");
        consultation_data.medicaments.splice(idx,1);
        drawMedicamentsTable();
    });
}

function addMedicamentToTable(){
    var id = $('#consultation_medicament_ts_autocomplete').val();
    var dose = $('#medical_consultation_prescription_attributes_medical_indications_dose').val();
    var name = $('#consultation_medicament').val();

    if(id == "" || name == "" || id == "undefined"){
        swal("No se puede agregar el medicamento","Por favor seleccione un medicamento válido de la lista.","error");
        return;
    }else if(dose == ""){
        swal("No se puede agregar el medicamento","Por favor ingrese la dosis correctamente.","error");
        return;
    }

    consultation_data.medicaments.push({
        dose:$("#medical_consultation_prescription_attributes_medical_indications_dose").val(),
        name:$("#consultation_medicament").val(),
        id:$("#consultation_medicament_ts_autocomplete").val()
    });

    $('#consultation_medicament_ts_autocomplete').val("");
    $('#consultation_medicament').val("");
    $("#medical_consultation_prescription_attributes_medical_indications_dose").val("");

    drawMedicamentsTable();
}

function saveMedicalConsultation(){

    var diseases_array = [];
    var medicaments_array = [];

    var formConsultation = $('#new_medical_consultation');
    var objectForm = formConsultation.serializeArray();


    $.each(consultation_data.diseases,function(idx, item){
        diseases_array.push(item.id);
    });

    $.each(consultation_data.medicaments,function(idx, item){
        medicaments_array.push({id:item.id, dose:item.dose});
    });

    objectForm.push({
        name: "expedient_id",
        value: $("#expedient_id").val()
    });

    objectForm.push({
        name: "diseases",
        value: JSON.stringify(diseases_array)
    });

    objectForm.push({
        name: "medicaments",
        value: JSON.stringify(medicaments_array)
    });

    objectForm.push({
        name: "general_notes",
        value: $("#general_notes_consult").summernote().code()
    });


    var valorations_data = [];
    var valorations = $(".valoration_holder");
    $.each(valorations,function(idx, item){
        var fields = $(item).find(".v_field");
        var v_fields = [];
        $.each(fields,function (idx, field) {
            v_fields.push({
                id: $(field).attr("field_id"),
                val: $(field).val()
            });
        });
        valorations_data.push({
            valoration_type_id: parseInt($(item).attr("valoration_type_id")),
            fields:v_fields
        })
    });

    objectForm.push({
        'name':'valorations',
        'value':JSON.stringify(valorations_data)
    });

    var custom_fields = [];
    $.each($(".consultation_custom_field"),function (idx, item) {
        var type = $(this).attr("field_type");
        switch (type){
            case "checkbox":
                custom_fields.push({
                    id: $(item).attr("field_id"),
                    value: $(item).is(":checked") ? "true" : "false"
                });
                break;
            case "checkbox_text":
                custom_fields.push({
                    id: $(item).attr("field_id"),
                    value: $(item).is(":checked") ? $(item).parent().parent().find(".text").val() : ''
                });

                break;
            default:
                custom_fields.push({
                    id: $(item).attr("field_id"),
                    value: $(item).val()
                });
                break;
        }
    });

    objectForm.push({
        'name':'custom_fields',
        'value':JSON.stringify(custom_fields)
    });

    var url = $('#url_form_create_consultation').val();

    $("#loading_btn_save").show();
    $.ajax({
        type: "post",
        url: url,
        dataType:'json',
        data: objectForm,
        success: function (response) {
            $("#loading_btn_save").hide();
            loadConsultationsList();
            if(typeof response == "string"){
                response = JSON.parse(response);
            }
            if(response.ok == "true"){
                closeNewConsultationForm();
                searchLastConsultation();

                $("#btn-new-consultation").parent().parent().show();

                /*
                var url_has_template = $('#url_has_template_consultation').val();
                var office = $('#medical_consultation_office').val();
                var url_print = response.url_print;
                $.ajax({
                    type: "get",
                    url: url_has_template,
                    dataType: 'json',
                    data: {office_id: office},
                    success: function (response2) {
                        if(response2.result){
                            window.open(url_print);
                        }else{
                            showAlertTemplate("Consulta Guardada Exitosamente.");
                        }
                        $('#modalWider').modal('hide');
                        searchLastConsultation();
                    }
                });

                */
            }else{
                setErrorsModalConsultation(response.errors);
            }
        },
        error: function (error) {
            swal("Error!", "Error al guardar la consulta, consulte al administrador.", "error");
        }
    });
}

function loadFullConsultation(id){
    if($("#details_consult_"+id).attr("loaded") != "no"){
        $("#details_consult_"+id).slideDown();
        return;
    }
    $.ajax({
        url:$("#load_full_consultation_url").val(),
        data:{id:id},
        success:function(response){
            $("#details_consult_"+id).html(response);
            $("#details_consult_"+id).attr("loaded","yes");
            $("#details_consult_"+id).slideDown();
        }
    });
}

function closeConsultDetails(id){
    $("#details_consult_"+id).slideUp();
}

function editFullConsultation(id){

    $("#edit_full_consult_"+id).show();
    $('#loading_btn_edit_'+id).hide();
    $('#list_error_medical_consultation_edit_'+id).empty();
    loadingFormEditConsultation(id);
    $('#btn-edit-consultation_'+id).show();
    $('#btn-edit-consultation_'+id).attr('disabled',false);
    //$("#details_consult_"+id).attr("loaded","yes");
}

function loadingFormEditConsultation(id){
    //mostrar loading en el modal
    //bloquear boton de guardar
    var url = $('#url_form_edit_medical_consultation').val();
    $.ajax({
        type: "get",
        url: url,
        data:{medical_consultation_id:id},
        dataType:'html',
        success: function (response) {
            $("#form-edit-medical-consultation_"+id).html(response);
        },
        error: function (error) {
            swal("Error","Error al cargar el formulario de consulta","error");
        }
    });

    //ajax para el formulario, ocultar el loading del modal
}

function openSendPrescriptionModal(consultation_id, patient_email){
    $("#send_prescription_email").val(patient_email);
    $("#send_prescription_id").val(consultation_id);
    $("#send_prescription_modal").modal("show");
}

function sendPrescriptionPdf() {
    $("#send_prescription_loading_message").show();
    $.ajax({
        url:$("#send_medical_prescription_pdf").val(),
        data:{
            medical_consultation_id:$("#send_prescription_id").val(),
            email:$("#send_prescription_email").val()
        },
        dataType:"json",
        success:function(data){
            $("#send_prescription_loading_message").hide();
            if(data.done){
                $("#send_prescription_modal").modal("hide");
                swal("Receta enviada",data.message,"success");
            }else{
                swal("No se pudo enviar el email",data.message,"error");
            }
        }
    });
}

function getMedicalConsultation(searchPhrase, page, isDESC){
    var url = $('#url_consultations_list').val();
    var order = "ASC";
    if(isDESC){
        order = "DESC";
    }

    $.ajax({
        type: "get",
        url: url,
        data: {searchPhrase: searchPhrase, current: page, rowCount: ROWS_BY_PAGE, "sort[date_consultation]": order},
        dataType:'html',
        success: function (response) {
            $("#container_medical_consultation").html(response);
            var infoConsultation = JSON.parse($('#info_consultation').val());
            updatePages(infoConsultation.total,infoConsultation.current);
            addFunctionListMedicalConsultation();
        },
        error: function (error) {
            swal("Error!", "Error al buscar consultas", "error");
        }
    });
}

function updatePages(totalRecord,pageCurrent){
    var totalPages = Math.ceil(parseInt(totalRecord)/parseInt(ROWS_BY_PAGE));
    var arrayPage = $('#pagination-medical-consultation > li');
    if(parseInt(pageCurrent) == 1){
        //bloquear el boton izquierdo
        $(arrayPage[0]).addClass('disabled');
    }else{
        //desbloquear el boton izquierdo
        $(arrayPage[0]).removeClass('disabled');
    }
    if(parseInt(totalPages) == parseInt(pageCurrent)){
        //bloquear el ultimo boton
        $(arrayPage[arrayPage.length - 1]).addClass('disabled');
    }else{
        //desbloquear el ultimo boton
        $(arrayPage[arrayPage.length - 1]).removeClass('disabled');
    }
    var i;
    //remove all number
    for(i=0;i < arrayPage.length; i++){
        if(parseInt(i) != parseInt(0) && parseInt(i) != parseInt((arrayPage.length - 1))){
            $(arrayPage[i]).remove();
        }
    }
    //all number page
    var list = "";
    for(i=1;i <= parseInt(totalPages); i++){
        if(parseInt(pageCurrent) == i){
            list += '<li class="active"><a href="" data-number="'+i+'">'+i+'</a></li>'
        }else{
            list += '<li class=""><a href="" data-number="'+i+'">'+i+'</a></li>'
        }
    }
    $(list).insertAfter($('#pagination-medical-consultation > .previous_pagination'));

    //add event click
    $('#pagination-medical-consultation li').unbind( "click" );
    $('#pagination-medical-consultation li').click(function(e)
    {
        e.preventDefault();
        if(!$(this).hasClass("disabled")){
            var selected = getNumberPageSelected();
            var search = $('#search_medical_consultation').val();
            if($(this).hasClass("previous_pagination")){
                //page before
                var nextPage = selected - 1;
                getMedicalConsultation(search,nextPage,true);

            }else if($(this).hasClass("next_pagination")){
                //next page
                var nextPage = selected + 1;
                getMedicalConsultation(search,nextPage,true);
            }else{
                var links = $(this).find("a");
                var nextPage = $(links[0]).data('number');
                getMedicalConsultation(search,nextPage,true);
            }

        }
    });
}

var delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

function getNumberPageSelected(){
    var arrayPage = $('#pagination-medical-consultation > li');
    var i;
    var result = null;
    for(i=0;i < arrayPage.length; i++){
        if(parseInt(i) != parseInt(0) && parseInt(i) != parseInt((arrayPage.length - 1))){
            if($(arrayPage[i]).hasClass("active")){
                var links = $(arrayPage[i]).find("a");
                result = $(links[0]).data('number');
                break;
            }
        }
    }
    return result;
}


function loadingTableIMC(){
    var url = $('#url_table_imc').val();
    var weight = $('#medical_consultation_vital_sign_attributes_weight').val();
    var height = $('#medical_consultation_vital_sign_attributes_height').val();
    $('#table_imc_consultation').hide();
    $.ajax({
        type: "get",
        url: url,
        data: {height:height, weight:weight },
        success: function (response) {
            $('#loading_table_imc').hide();
            $('#table_imc_consultation').html(response.template);
            $('#medical_consultation_vital_sign_attributes_imc').val(response.imc_real);
            $('#medical_consultation_vital_sign_attributes_suggested_weight').val(response.value_suggested_weight);
            $('#table_imc_consultation').show("slow");
        },
        error : function (response) {
            $('#error_generate_table_imc').show();
        }
    });
}


function setErrorsModalConsultation(errors){
    //var keys = Object.keys(errors);
    var html = "<div id='error_explanation'> <h3>"+errors.length+" errores encontrados:</h3>";
    var i;
    html += "<ul>";
    for(i=0; i<errors.length; i++){
        // var error =errors[keys[i]];
        var error = errors[i];
        // var attribute = keys[i];
        // var attributeArray = attribute.split('.');
        // html += "<li>"+attributeArray[attributeArray.length - 1]+" - "+error+"</li>";
        html += "<li>"+error+"</li>";
    }
    html += "</ul>";
    html += "</div>";
    $('#list_error_medical_consultation').empty();
    $('#list_error_medical_consultation').append(html);
}

function closeNewConsultationFormEdit(id){
    $("#edit_full_consult_"+id).slideUp(500);
}

function saveEditedConsultation(id){
    saveMedicalConsultationEdited(id);
}

function saveMedicalConsultationEdited(id){
    var formConsultation = $('#edit_medical_consultation_'+id);
    var objectForm = formConsultation.serializeArray();

    var json = JSON.stringify(objectForm);
    var jsonReal = JSON.parse(json);

    var url = $('#url_form_update_consultation').val();


    $.ajax({
        type: "get",
        url: url,
        dataType:'json',
        data: jsonReal,
        success: function (response) {
            if(typeof response == "string"){
                response = JSON.parse(response);
            }
            if(response.ok == "true"){
                closeNewConsultationFormEdit(id);
                swal("Exito", "Consulta actualizada con exito.", "success");
                $("#load_consultations_link").trigger("click");
            }else{
            }
        },
        error: function (error) {
            swal("Error!", "Error al guardar la consulta, consulte al administrador.", "error");
        }
    });

}