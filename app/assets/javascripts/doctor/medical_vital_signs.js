var graph_type = "";
function showVitalSigns(){
    showLoading("vital-sign");
    var url = $('#url_data_medical_vital_sign').val();
    $.ajax({
        type: "get",
        url: url,
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("vital-sign",response);
           $("#data-table-vital_signs").bootgrid({
                ajax: true,
                ajaxSettings: {
                    method: "GET",
                    cache: false
                },
                url: $('#data-table-vital_signs').data('source'),
                css: {
                    icon: 'zmdi icon',
                    iconColumns: 'zmdi-view-module',
                    iconDown: 'zmdi-expand-more',
                    iconRefresh: 'zmdi-refresh',
                    iconUp: 'zmdi-expand-less'
                },

            });
        },
        error: function (error) {
            swal("Error!", "Error al cargar la sección de signos vitales.", "error");
        }
    });

}

function show_vital_sign(vital_sign_id){
       $.ajax({
        url:$('#url_get_vital_sign').val(),
        method:"GET",
        dataType:"json",
        data:{
            id: vital_sign_id
        },
        success:function(response){
            $("#vital_sign_height").val(response.height + " Cm");
            $("#vital_sign_weight").val(response.weight + " Kg");
            $("#vital_sign_blood_pressure").val((response.blood_pressure == null || response.blood_pressure == ''? '' : response.blood_pressure + " mmHg") );
            $("#vital_sign_temperature").val((response.temperature == null || response.temperature == ''? '' : response.temperature+ " °C") );
            $("#vital_sign_suggested_weight").val((response.suggested_weight == null || response.suggested_weight == '' ? '' : response.suggested_weight+ " Kg") );
            $("#vital_sign_glucose").val((response.glucose == null || response.glucose == '' ? '' : response.glucose+ " mg/dl") );
            $("#vital_sign_imc").val((response.imc == null || response.imc == ''? '' : response.imc+ " kg/m²") );
            $("#vital_sign_heart_breath").val((response.heart_rate == null || response.heart_rate == ''? '' : response.heart_rate+ " lpm") );
            $("#vital_sign_breathing_frecuency").val((response.breathing_frequency == null  || response.breathing_frequency == ''? '':response.breathing_frequency+ " rpm") );
            $("#show_vital_sign").modal("show");
          }
    });
}

function loadVitalSignsData(type){
    graph_type = type;
    var medical_expedient_id = $("#medical_expedient_id_vital_sign_data").val();
    $.ajax({
        url:$("#load_chart_data_url").val(),
        data:{
            medical_expedient_id:medical_expedient_id,
            type:type
        },
        dataType:"json",
        success:function(data){
            createChart(type,data);
        }
    });
}
function print_valoration_graph(){
    if (graph_type != ""){
        url_pdf = $('#url_print_valoration_graph').val()+"?medical_expedient_id="+$("#medical_expedient_id_vital_sign_data").val()+"&type="+graph_type;
        window.open(url_pdf, '_blank');
    }

}

function createChart(type,resp){
    if(resp.length == 0){
        return;
    }
    var firstDate = new Date(resp[0].date);
    var lastDate = new Date(resp[resp.length - 1].date);
    var maxDays = Math.round((lastDate-firstDate)/(1000*60*60*24));

    var currentDates = {};
    var days = 0;
    console.log(resp);
    $.each(resp, function (idx,item) {
        days = Math.round((new Date(item.date)-firstDate)/(1000*60*60*24));
        if(item.val != null){
            currentDates["d"+days] = item.val;
        }
    });
    var dataArray = [];
    var dataSystolicPressure = [];
    var dataDiastolicPressure = [];
    for(var i = 0; i <= maxDays ; i++){
        if(type == "blood_pressure"){
            if(currentDates["d"+i] != undefined){
                dataSystolicPressure[i] = currentDates["d"+i].systolic_pressure;
                dataDiastolicPressure[i] = currentDates["d"+i].diastolic_pressure;
            }else{
                dataSystolicPressure[i] = null;
                dataDiastolicPressure[i] = null;
            }
        }else{
            if(currentDates["d"+i] != undefined){
                dataArray[i] = currentDates["d"+i];
            }else{
                dataArray[i] = null;
            }
        }
    }

    var conf = {
        suffix:"",
        yTitle:"",
        title:"",
        subtitle:"De la primera a la última consulta",
        series_name:""
    };

    switch (type){
        case "weight":
            conf.suffix = " Kg";
            conf.yTitle = "Peso (Kg)";
            conf.title = "Historial de Peso";
            conf.series_name = "Peso";
            break;
        case "height":
            conf.suffix = " Cm";
            conf.yTitle = "Estatura (Cm)";
            conf.title = "Historial de Estatura";
            conf.series_name = "Estaura";
            break;
        case "temperature":
            conf.suffix = " °C";
            conf.yTitle = "Temperatura °C";
            conf.title = "Historial de temperatura";
            conf.series_name = "Temperatura";
            break;
        case "blood_pressure":
            conf.suffix = " mmHg";
            conf.yTitle = "Tensión Arterial";
            conf.title = "Historial de tensión arterial";
            conf.series_name = "Tensión Arterial";
            break;
    }

    var data = [];
    if(type == "blood_pressure"){
        data.push({
            name: "Presión Sistólica",
            data: dataSystolicPressure
        });
        data.push({
            name: "Presión Diastólica",
            data: dataDiastolicPressure
        });
    }else{
        data = [{
            name: conf.series_name,
            data: dataArray
        }];
    }


    $('#vital_sign_chart').highcharts({
        title: {
            text: conf.title,
            x: -20 //center
        },
        subtitle: {
            text: conf.subtitle,
            x: -20
        },
        xAxis: {
           // categories:["1","xxxxx"]
            labels: {
                formatter: function() {
                    return this.value +' días'
                }
            }
        },
        yAxis: {
            title: {
                text: conf.yTitle
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        plotOptions:{
            series:{
                connectNulls:true
            }
        },
        tooltip: {
            valueSuffix: conf.suffix
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: data
    });
}