//= require doctor/expedients_medical_history_percentile
var routine_medicaments = [];
function initComponentsMedicalHistoryUI(){

    $("#save_medical_history_btn").click(function(){
        var data = $(".medical_history_form").serializeArray();
        var r_medicaments = [];
        $.each(routine_medicaments,function(idx, item){
            r_medicaments.push(item.id);
        });
        data.push({
            name:"routine_medicaments",
            value:r_medicaments
        });
        var url = $(this).attr("save-url");
        var id = $(this).attr("record-id");
        $.ajax({
            dataType:"json",
            method: "PUT",
            url: url+"/"+id,
            data:data,
            success:function(data){
                if(data.done){
                    swal("Historial médico actualizado","Los datos de los antecedentes se actualizaon correctamente.","success");
                }
            }
        });
    });

    $("#save_urological_bk_btn").click(function(){
        var method = "POST";
        var url = $(this).attr("save-url");

        if($("#urological_bk_id").val() != ""){
            method = "PUT";
            url += "/" + $("#urological_bk_id").val()
        }

        var object = $("#urological_background_form").serializeArray();
        $.ajax({
            url:url,
            method:method,
            dataType:"json",
            data:object,
            success:function(resp){
                if(resp.done){
                    swal("Operación finalizada con éxito","Los datos del antecedente urológico se guardaron correctamente.","success");
                }
            }
        });
    });


    // antecedentes heredo familiares
    $("#save_ifb_btn").click(function(){
        var method = "POST";
        var url = $(this).attr("save-url");

        if($("#inherited_familiar_bk_id").val() != ""){
            method = "PUT";
            url += "/" + $("#inherited_familiar_bk_id").val()
        }

        var object = $("#inherited_background_form").serializeArray();
        $.ajax({
            url:url,
            method:method,
            dataType:"json",
            data:object,
            success:function(resp){
                if(resp.done){
                    swal("Operación finalizada con éxito",
                        "Los antecedentes Heredo Familiares se actualizaron correctamente.","success");
                }
            }
        });
    });

    // antecedentes gineco obstétricos
    $("#save_gob_btn").click(function(){
        var method = "POST";
        var url = $(this).attr("save-url");

        if($("#ginecologist_bk_id").val() != ""){
            method = "PUT";
            url += "/" + $("#ginecologist_bk_id").val()
        }

        var object = $("#ginecologysts_background_form").serializeArray();
        $.ajax({
            url:url,
            method:method,
            dataType:"json",
            data:object,
            success:function(resp){
                if(resp.done){
                    swal("Operación finalizada con éxito","Los datos del antecedentes Gineco - Obstétricos se actualizaron correctamente.","success");
                }
            }
        });
    });

    // antecedentes perinatales
    $("#save_perinatal_info_btn").click(function(){
        var method = "POST";
        var url = $(this).attr("save-url");
        if($("#perinatal_information_bk_id").val() != ""){
            method = "PUT";
            url += "/" + $("#perinatal_information_bk_id").val()
        }
        var object = $("#perinatal_info_form").serializeArray();
        $.ajax({
            url:url,
            method:method,
            dataType:"json",
            data:object,
            success:function(resp){
                if(resp.done){
                    $("#perinatal_information_bk_id").val(resp.data.id);
                    swal("Operación finalizada con éxito","Los datos de Antecedentes Perinatales se actualizaron correctamente.","success");
                }
            }
        });
    });

    // antecedentes no patologicos pediatria
    $("#save_child_non_patho_info_btn").click(function(){
        var method = "POST";
        var url = $(this).attr("save-url");
        if($("#child_non_patho_bk_id").val() != ""){
            method = "PUT";
            url += "/" + $("#child_non_patho_bk_id").val()
        }
        var object = $("#child_non_patho_form").serializeArray();
        $.ajax({
            url:url,
            method:method,
            dataType:"json",
            data:object,
            success:function(resp){
                if(resp.done){
                    $("#child_non_patho_bk_id").val(resp.data.id);
                    swal("Operación finalizada con éxito","Los datos de Antecedentes No Patológicos se actualizaron correctamente.","success");
                }
            }
        });
    });

    // datos de los padres
    $("#save_child_parent").click(function(){
        var method = "POST";
        var url = $(this).attr("save-url");
        if($("#parent_id").val() != ""){
            method = "PUT";
            url += "/" + $("#parent_id").val()
        }
        if(!validateParentInfo()){
            swal("Error!", "Nombre(s), apellidos y fecha de nacimiento son obligatorios.", "error");
            return;
        }

        var params = {
            parent_info:{
                first_name:$("#parent_first_name").val(),
                last_name:$("#parent_last_name").val(),
                gender:$("#parent_gender").val(),
                birth_date:$("#parent_birthdate").ts_date(),
                blood_type_id:$("#parent_blood_type_id").val(),
                academic_grade:$("#parent_academic_grade").val(),
                child_non_pathological_info_id:$("#child_non_pathological_info_id").val()
            }
        };

        $("#add_parent").modal("hide");

        //$("#add_parent").modal("hide");
        //$('body').removeClass('modal-open');
        //$('.modal-backdrop').remove();
        $.ajax({
            url:url,
            method:method,
            dataType:"json",
            data:params,
            success:function(data){
                location.reload();
            }
        });

    });

    $("#medicament_name").ts_autocomplete({
        url:AUTOCOMPLETE_MEDICAMENTS_URL,
        in_modal:true,
        items:10,
        required:true,
        model:"un medicamento",
        callback:function(id, name){
            routine_medicaments.push({
                id: id,
                name: name
            });
            drawMedicamentsTableBK();
            setTimeout(function(){
                $("#medicament_name").val("");
            },500);
        }
    });

    $(".search-city").ts_autocomplete({
        url:AUTOCOMPLETE_CITIES_URL,
        in_modal:true,
        items:10,
        required:true,
        model:"una ciudad"
    });

    if($("#medicaments_medical_history")[0]){
        routine_medicaments = [];
        var medicines_data = JSON.parse($("#medicaments_medical_history").val());
        $.each(medicines_data,function(idx, med){
            routine_medicaments.push({
                id: med.id,
                name: med.comercial_name + " (" +med.presentation+")"
            });
        });
        drawMedicamentsTableBK();
    }

}

function drawMedicamentsTableBK() {
    $("#medical-history-medicaments").html("");
    $.each(routine_medicaments,function(idx, item){
        $("#medical-history-medicaments").append("<li>"+item.name+" <i class='zmdi zmdi-delete delete-medicament' idx='"+idx+"'></i></li>");
    });
    $(".delete-medicament").unbind("click");
    $(".delete-medicament").click(function(){
        var idx = $(this).attr("idx");
        routine_medicaments.splice(idx,1);
        drawMedicamentsTableBK();
    });
}

function setupAddParent(gender){
    var title = gender == "Madre" ? "Datos de la Madre" :"Datos del Padre";
    $("#add_parent_title").html(title);
    $("#parent_gender").val(gender);
    $("#parent_id").val("");
    $("#add_parent").modal("show");
}


function setupEditParent(id, first_name, last_name, birth_date, academic_grade, blood_type, gender){
    $("#parent_id").val(id);
    $("#parent_first_name").val(first_name);
    $("#parent_last_name").val(last_name);
    $("#parent_gender").val(gender);
    $("#parent_birthdate").val(birth_date)
    $("#parent_blood_type_id").val(blood_type).selectpicker('refresh');
    $("#parent_academic_grade").val(academic_grade).selectpicker('refresh');

    var title = gender == "Madre" ? "Editar datos de la Madre" :"Editar datos del Padre";
    $("#add_parent_title").html(title);

    $("#add_parent").modal("show");

}

function validateParentInfo(){
    var data_valid = false;

    if($("#parent_first_name").val() != "" &&
        $("#parent_last_name").val() != "" &&
        $("#parent_birthdate").ts_date() != ""){
        data_valid = true;
    }
    return data_valid;

}

function deleteParent(id,gender){
    swal({
        title: "Eliminar "+gender,
        text: "¿Confirma que desea eliminar el registro?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelarr",
        confirmButtonClass:"btn btn-danger",
        cancelButtonClass:"btn",
        closeOnConfirm: false,
        closeOnCancel: true
    }, function(isConfirm){
        if (isConfirm) {
            swal("Eliminado!", "Se ha eliminado el registro sin ningún problema.", "success");
            var url = $('#url_destroy_parent').val();
            $.ajax({
                url:$("#save_child_parent").attr("save-url")+"/"+id,
                method:"DELETE",
                dataType:"json",
                success:function(data){
                    location.reload();
                }
            });
        } else {
        }
    });
}

