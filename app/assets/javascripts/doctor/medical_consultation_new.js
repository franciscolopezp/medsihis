/**
 * Created by equipo-01 on 20/04/16.
 */
var COSTS = new Array();
var consultation_data = {
    diseases:[],
    medicaments:[]
};
$(document).ready(function(){
    //$('#loading_btn_save').hide();
    $('#btn-new-consultation').click(function(e)
    {
        $("#new_consultation_form_holder").show();
        $('#loading_btn_save').hide();
        $('#list_error_medical_consultation').empty();
        loadingFormNewConsultation();
        $('#btn-save-consultation').show();
    });
});

function closeNewConsultationForm(){
    $("#new_consultation_form_holder").slideUp(500);
    $("#btn-new-consultation").parent().parent().show();
}


function loadingFormNewConsultation(){
    $("#btn-new-consultation").parent().parent().hide();
    //mostrar loading en el modal
    showLoading("form-new-medical-consultation");
    //bloquear boton de guardar
    setDisableBtn('btn-save-consultation', true);
    var url = $('#url_form_medical_consultation').val();
    $.ajax({
        type: "get",
        url: url,
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("form-new-medical-consultation",response);
            initElementsConsultationForm();
            initUltrasoundComponents();
        },
        error: function (error) {
            swal("Error","Error al cargar el formulario de consulta","error");
        }
    });

    //ajax para el formulario, ocultar el loading del modal
}

function getJsonDiagnostic(){
    var result = new Array();
    $.each(consultation_data.diseases,function(idx, item){
        result.push({'name': item.name,'id':item.id});
    });
    var jsonDiseases = {"name": 'medical_consultation[diseases]', "value": JSON.stringify(result)};
    return jsonDiseases;
}

function getJsonMedicament(){
    var result = new Array();
    $.each(consultation_data.medicaments,function(idx, item){
        result.push({'name': item.name,'id':item.id,'dose':item.dose});
    });
    var jsonMedicaments = {"name": 'medical_consultation[prescriptions][medical_indications][medicament]', "value": JSON.stringify(result)};
    return jsonMedicaments;
}

var DATE_CONSULTATION_BEFORED = "";


/*********************** ENFERMEDADES  **************************************/
function drawDiseasesTable(){
    var table = "#diseases-table";
    $(table).html("");
    $.each(consultation_data.diseases,function(idx, item){
        $(table).append("<tr>" +
            "<td style='width: 70%'>"+item.name+"</td>" +
            "<td><i idx='"+idx+"' class='zmdi zmdi-delete delete_disease_consult'></i></td>" +
            "</tr>");
    });

    $(".delete_disease_consult").click(function(){
        var idx = $(this).attr("idx");
        consultation_data.diseases.splice(idx,1);
        drawDiseasesTable();
    });
}

function addDiseaseToTable() {
    consultation_data.diseases.push({
        name:$("#consultation_disease_ts_autocomplete_text").val(),
        id:$("#consultation_disease_ts_autocomplete").val()
    });

    setTimeout(function () {
        $("#consultation_disease").val("");
        $("#consultation_disease").focus();
    },100   );

    $("#consultation_disease_ts_autocomplete").val("");
    $("#consultation_disease_ts_autocomplete_text").val("");

    drawDiseasesTable();
}


function drawMedicamentsTable(){
    var table = "#medicaments-table";
    $(table).html("");
    $.each(consultation_data.medicaments,function(idx, item){
        $(table).append("<tr>" +
            "<td style='width: 45%'>"+item.name+"</td>" +
            "<td style='width: 40%'>"+item.dose+"</td>" +
            "<td><i idx='"+idx+"' class='zmdi zmdi-delete delete_medicament_consult'></i></td>" +
            "</tr>");
    });

    $(".delete_medicament_consult").click(function(){
        var idx = $(this).attr("idx");
        consultation_data.medicaments.splice(idx,1);
        drawMedicamentsTable();
    });
}

function addMedicamentToTable(){
    var id = $('#consultation_medicament_ts_autocomplete').val();
    var dose = $('#medical_consultation_prescription_attributes_medical_indications_dose').val();
    var name = $('#consultation_medicament').val();

    if(id == "" || name == "" || id == "undefined"){
        swal("No se puede agregar el medicamento","Por favor seleccione un medicamento válido de la lista.","error");
        return;
    }else if(dose == ""){
        swal("No se puede agregar el medicamento","Por favor ingrese la dosis correctamente.","error");
        return;
    }

    consultation_data.medicaments.push({
        dose:$("#medical_consultation_prescription_attributes_medical_indications_dose").val(),
        name:$("#consultation_medicament").val(),
        id:$("#consultation_medicament_ts_autocomplete").val()
    });

    $('#consultation_medicament_ts_autocomplete').val("");
    $('#consultation_medicament').val("");
    $("#medical_consultation_prescription_attributes_medical_indications_dose").val("");

    drawMedicamentsTable();
}

function initElementsConsultationForm(){
    consultation_data.diseases = [];  //se inicializa el arreglo de enfermedades
    consultation_data.medicaments = [];  //se inicializa el arreglo de medicamentos

    $(".search-disease").ts_autocomplete({ // inicializar campo para buscar enfermedades
        url:AUTOCOMPLETE_DISEASES_URL,
        in_modal:true,
        items:10,
        required:false,
        model:"una enfermedad",
        on_select:addDiseaseToTable
    });

    $(".search-medicament").ts_autocomplete({
        url:AUTOCOMPLETE_MEDICAMENTS_URL,
        in_modal:true,
        items:10,
        required:false,
        model:"un medicamento"
    });

    $("#add-medicament").unbind();
    $('#add-medicament').click(addMedicamentToTable);


    $('#div_fpp_consultation').empty();
    if($("#pregnancies_current_data")[0]){
        setTimeout(function () {
            var data = JSON.parse($("#pregnancies_current_data").val());
            generatePregnancyCharts(true,data);
        },500);
    }

    $("#show_vital_sign_btn").click(function(){
        $("#vital_sign_section").toggle();
        $('html, body').animate({
            scrollTop: $("#vital_sign_section").offset().top
        }, 500);
        $("#div_fpp_consultation").html("");

        if($("#vital_sign_section").is(":visible")){
            $("#div_fpp_consultation").parent().parent().attr("class","col-md-6");
        }else{
            $("#div_fpp_consultation").parent().parent().attr("class","col-md-12");
        }

        var data = JSON.parse($("#pregnancies_current_data").val());
        generatePregnancyCharts(true,data);
    });

    $("#show_exploration_btn").click(function(){
        $("#exploration_section").toggle();
        $('html, body').animate({
            scrollTop: $("#exploration_section").offset().top
        }, 500);
    });

    $("#show_general_anotations_btn").click(function(){
        $("#anotations_section").toggle();
        $('html, body').animate({
            scrollTop: $("#anotations_section").offset().top
        }, 500);
    });

    $(".selectpicker").selectpicker();

    $("#save_uro_exploration").change(function(){
        if($(this).val() == "no"){
            $("#uro_exploration_form").hide();
        }else{
            $("#uro_exploration_form").show();
        }
    });

    /********* codigo del SENSEI y parte de la antigua UI  ***********/

    $('.selectpicker').selectpicker();
    var dateNow = new Date();
    var month = dateNow.getMonth()+1;
    var day = dateNow.getDate();

    var hours = dateNow.getHours();
    var minutes = dateNow.getMinutes();

    var output = (day<10 ? '0' : '') + day +'/'+ (month<10 ? '0' : '') + month  + '/' +
        dateNow.getFullYear() + ' ' + hours+':'+minutes;

    $('#medical_consultation_date_consultation').val(output);

    $('#medical_consultation_date_consultation').datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY hh:mm a'
    });

    $("#medical_consultation_date_consultation").on("dp.change", function (e) {
        var date = $('#medical_consultation_date_consultation').val();
        var dateArray = date.split(' ');
        if (DATE_CONSULTATION_BEFORED != dateArray[0]){
            DATE_CONSULTATION_BEFORED = dateArray[0];
            $('#medical_consultation_date_consultation').data("DateTimePicker").hide();
        }
    });
    var dateAux = $('#medical_consultation_date_consultation').val();
    DATE_CONSULTATION_BEFORED = dateAux.split(' ')[0];
    $('#medical_consultation_date_consultation').click(function(){
        $('#medical_consultation_date_consultation').data("DateTimePicker").show();
    });

    $("#btn-save-consultation").unbind();
    $('#btn-save-consultation').click(function(e)
    {
        //validar que la fecha de la consulta no sea menor a la fecha de última regla
        if($("#pregnancy_last_menstruation_date_hidden").val() != undefined){
            var lastMenstruationDate = stringToDate($("#pregnancy_last_menstruation_date_hidden").val(),"dd/mm/yyyy");
            var dateConsultation = getDate($('#medical_consultation_date_consultation').val());
            if(dateConsultation < lastMenstruationDate){
                swal("Error","No se pueden agregar consultas anteriores a la fecha de última regla.","error");
                return;
            }
        }
        //bloquear el boton de guardar, mostrar loading
        setDisableBtn('btn-save-consultation',true);
        $('#loading_btn_save').show();
        saveMedicalConsultation();
    });


    //cargar los json de los consultorios
    COSTS = jQuery.parseJSON($('#consultations_costs').val());
    //agregar el costo de la consulta seleccionada
    $('#medical_consultation_price').val(getPriceOfficeSelected());
    //evento cuando cambia de consultorio
    $( "#medical_consultation_office" ).change(function() {
        $('#medical_consultation_price').val(getPriceOfficeSelected());
    });
    // evento que cambia tipo de consulta
    $( "#medical_consultation_consultation_type_id" ).change(function() {
        $('#medical_consultation_price').val(getPriceOfficeSelected());
    });
    setDisableBtn('btn-save-consultation',false);

    $("#btn-calculation-imc").unbind();
    $('#loading_table_imc').hide();
    $('#error_weight_height').hide();
    $('#error_generate_table_imc').hide();
    //set event click to btn calculation IMC
    $('#btn-calculation-imc').click(function(e){
        $('#loading_table_imc').show();
        $('#error_weight_height').hide();
        $('#error_generate_table_imc').hide();
        var weight = $('#medical_consultation_vital_sign_attributes_weight').val();
        var height = $('#medical_consultation_vital_sign_attributes_height').val();
        if(weight != '' && weight.length > 0 && height != '' && height.length > 0){
            loadingTableIMC();
        }else{
            $('#loading_table_imc').hide();
            $('#error_generate_table_imc').hide();
            $('#error_weight_height').show();
        }
    });
    $('#table_imc_consultation').hide();


    $("#physical_exam").hide();
    $("#vaginal_exam").hide();
    $("#medical_consultation_vital_sign_attributes_oxygen_saturation").change(function(){
        sat_ox = $("#medical_consultation_vital_sign_attributes_oxygen_saturation").val();
        if(sat_ox < 0 || sat_ox > 100){
            swal("Error","La saturación de oxigeno es un valor entre 0 y 100","error");
            $("#medical_consultation_vital_sign_attributes_oxygen_saturation").val("");
        }
    });
    $("#medical_consultation_exploration_attributes_exploration_type_id").change(function(){
        switch ($(this).val()){
            case "":
                $("#physical_exam").hide();
                $("#vaginal_exam").hide();
                break;
            case "1":
                $("#physical_exam").show();
                $("#vaginal_exam").show();
                break;
            case "2":
                $("#physical_exam").show();
                $("#vaginal_exam").hide();
                break;
            case "3":
                $("#physical_exam").hide();
                $("#vaginal_exam").show();
                break;
        }
    });

    initHtmlEditor("#general_notes_consult");
}

function loadingTableIMC(){
    var url = $('#url_table_imc').val();
    var weight = $('#medical_consultation_vital_sign_attributes_weight').val();
    var height = $('#medical_consultation_vital_sign_attributes_height').val();
    $('#table_imc_consultation').hide();
    $.ajax({
        type: "get",
        url: url,
        data: {height:height, weight:weight },
        success: function (response) {
            $('#loading_table_imc').hide();
            $('#table_imc_consultation').html(response.template);
            $('#medical_consultation_vital_sign_attributes_imc').val(response.imc_real);
            $('#medical_consultation_vital_sign_attributes_suggested_weight').val(response.value_suggested_weight);
            $('#table_imc_consultation').show("slow");
        },
        error : function (response) {
            $('#error_generate_table_imc').show();
        }
    });
}

function closeTableImc(){
    $('#table_imc_consultation').hide("slow");
}

function openModalAddDisease(){
    $("#addnewdiseaseconfirmation").modal("hide");
    $("#addnewdiseasefromece").modal("show");
    $("#new_disease_name").val($("#disease_name").val());
    $("#new_disease_code").val("");

    //se agregar nueva funcion para agregar mas enfermedades
    $('#add_new_disease_ece_btn').unbind();
    $('#add_new_disease_ece_btn').click(function(){
        addNewDiseaseFromConsultation();
    });
}

function getPriceOfficeSelected(){

    var office = parseInt($('#medical_consultation_office').val());
    var type = parseInt($("#medical_consultation_consultation_type_id").val());
    var cost = "00.00";
    if ($("#is_doctor_consult").val()==1){
        $.each(COSTS,function(idx,c_cost){
            if(c_cost.consultation_type_id == type && c_cost.office_id == office){
                cost = parseFloat(c_cost.cost).toFixed(2)
            }
        });
    }else{
        $.each(COSTS,function(idx,c_cost){
                cost = parseFloat(c_cost.cost).toFixed(2)
        });
    }
    return cost;
}

function showAlertTemplate(title){
    var url_template = $('#url_select_prescription').val();
    swal({
        title: title,
        text: "No se encontro seleccionado ninguna plantilla para la receta ",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: "Seleccionar plantilla",
        cancelButtonText: "Cerrar",
        showLoaderOnConfirm: true, },
        function(){
            window.open(url_template);
        });
}

function saveMedicalConsultation(){

    var formConsultation = $('#new_medical_consultation');
    var disabled = formConsultation.find(':input:disabled').removeAttr('disabled');
    var objectForm = formConsultation.serializeArray();
    disabled.attr('disabled','disabled');

    var json = JSON.stringify(objectForm);
    var jsonReal = JSON.parse(json);
    var indexToDelete = new Array();
    var i;
    for(i=0;i<jsonReal.length; i++){
        var aux = jsonReal[i];
        if(aux.name == "consultations_costs" || aux.name == "offices_consultation" || aux.name == "medical_consultation[prescription_attributes][medical_indications][medicament]" ||
            aux.name == "medical_consultation[prescription_attributes][medical_indications][dose]" || aux.name == "medical_consultation[diseases]"){
            indexToDelete.push(i);
        }
    }
    for(i=0;i<indexToDelete.length; i++){
        delete jsonReal[indexToDelete[i]];
    }
    jsonReal.push(getJsonDiagnostic());
    jsonReal.push(getJsonMedicament());
    //jsonReal.push({"name": 'date_medical_consultation', "value": getDate($('#medical_consultation_date_consultation').val())});


    var str_date_consultation = $("#medical_consultation_date_consultation").val();
    jsonReal.push({
        'name':'date_medical_consultation',
        'value':stringToDate(str_date_consultation,"dd/mm/yyyy hh:mn pp")
    });

    jsonReal.push({
        'name':'general_notes',
        'value':$("#general_notes_consult").summernote().code()
    });

    var url = $('#url_form_create_consultation').val();

    $.ajax({
        type: "post",
        url: url,
        dataType:'json',
        data: jsonReal,
        success: function (response) {

            if(typeof response == "string"){
                response = JSON.parse(response);
            }

            if(response.ok == "true"){
                $("#btn-new-consultation").parent().parent().show();

                var url_has_template = $('#url_has_template_consultation').val();
                var office = $('#medical_consultation_office').val();
                var url_print = response.url_print;
                closeNewConsultationForm();
                $.ajax({
                    type: "get",
                    url: url_has_template,
                    dataType: 'json',
                    data: {office_id: office},
                    success: function (response2) {
                        if(response2.result){
                            var dateConsultation = getDate($('#medical_consultation_date_consultation').val());
                            $("#last_consultation_date_info").html(dateToString(dateConsultation,"dd/mm/yyyy"));
                            window.open(url_print);
                        }else{
                            showAlertTemplate("Consulta Guardada Exitosamente.");
                        }
                        $('#modalWider').modal('hide');
                        searchLastConsultation();
                    }
                });
            }else{
                setErrorsModalConsultation(response.errors);
            }
        },
        error: function (error) {
            swal("Error!", "Error al guardar la consulta, consulte al administrador.", "error");
        }
    });

}

function setErrorsModalConsultation(errors){

    //var keys = Object.keys(errors);
    var html = "<div id='error_explanation'> <h3>"+errors.length+" errores encontrados:</h3>";
    var i;
    html += "<ul>";
    for(i=0; i<errors.length; i++){
       // var error =errors[keys[i]];
        var error = errors[i];
       // var attribute = keys[i];
       // var attributeArray = attribute.split('.');
       // html += "<li>"+attributeArray[attributeArray.length - 1]+" - "+error+"</li>";
        html += "<li>"+error+"</li>";
    }
    html += "</ul>";
    html += "</div>";
    $('#list_error_medical_consultation').empty();
    $('#list_error_medical_consultation').append(html);
    setDisableBtn('btn-save-consultation',false);
    hideLoadingAndSetContainer("loading_btn_save","");
}

function setDisableBtn(id,boolean){
    var idAux = "#"+id;
    $(idAux).prop( "disabled", boolean );
}

//funcion para saber el tipo de dato
//uso : toType({a: 4}); //"object" toType([1, 2, 3]); //"array"
var toType = function(obj) {
    return ({}).toString.call(obj).match(/\s([a-z|A-Z]+)/)[1].toLowerCase()
}


function getDate(str1){
    // str1 debe tener formato dd/MM/yyyy
    // str2 debe tener formato hh:mm a  formato 12 hrs
    var hourComponents = str1.split(" ");
    var aux = hourComponents[0];
    var dt1   = parseInt(aux.substring(0,2));
    var mon1  = parseInt(aux.substring(3,5));
    var yr1   = parseInt(aux.substring(6,10));

    var hour = hourComponents[1];
    var format = hourComponents[2];
    var hComp = hour.split(":");

    var h = parseInt(hComp[0]);
    var m = parseInt(hComp[1]);

    if(format == "PM" && h != 12){
        h += 12;
    }else if(format == "AM" && h == 12){
        h = 0;
    }
    return new Date(yr1, mon1-1, dt1,h,m,0);
}

function addNewDiseaseFromConsultation(){
    var disease_category_id = $("#new_disease_category_select").val();
    var disease_code = $("#new_disease_code").val();
    var disease_name = $("#new_disease_name").val();

    if(disease_code == "" || disease_name == ""){
        swal("Error","Los campos no pueden estar vacios","error");
        return;
    }
    var url = $('#url_new_disease').val();
    var method = "GET";
    var params = {
        code: disease_code,
        name: disease_name,
        disease_category_id:disease_category_id
    };
    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(response){
            $('#medical_consultation_diseases').append($('<option>', {
                value: response.id,
                text: response.name
            }));
            $('#medical_consultation_diseases').selectpicker('refresh');
            $('#medical_consultation_diseases').selectpicker('val', response.id);

            consultation_data.diseases.push({
                id:response.id,
                name:response.name
            });
            drawDiseasesTable();

            //$('#medical_consultation_diseases').selectpicker('refresh');
            $("#addnewdiseasefromece").modal("hide");
        }
    });
}