var systems = {
    infection:"infection",
    ophthalmological:"ophthalmological",
    hematological:"hematological",
    metabolic:"metabolic",
    otolaryngology:"otolaryngology",
    nervous:"nervous",
    cardio:"cardio",
    respiratory:"respiratory",
    digestive:"digestive",
    urinary:"urinary",
    psychosomatic:"psychosomatic",
    genital:"genital",
    locomotor:"locomotor",
    sense_organ:"sense_organ"
};


function initComponentSystems(){
    $(".close_form_holder").click(function(){
        $("#system_buttons_holder").show();
        $(this).parent().hide();
    });

    $(".form_button").click(function(){
        $("#system_buttons_holder").hide();
        $("#"+$(this).attr("form_holder")).show();
    });

    $(".save_data_system").click(function(){
        var data = {};
        var form_name = $(this).attr("form");
        var form = form_name+"_form_holder";
        $("#"+form+' .save_data_system').prop("disabled",true);

        var conf_data = getFieldsAndUrl(form_name); // de acuerdo al tipo de formulario se ubtiene la url y los campos.

        $.each(conf_data.fields,function (idx,item) {
            var key = '#'+form+' textarea[name='+item+']';
            data[item] = $(key).val(); // se recaban los datos y se crea el objeto data genérico
        });

        var id_holder = $('#'+form+' input[name=id]'); // se ubica el elemento que va a contener el id del registro

        data.medical_history_id = $("#medical_history_id").val();

        var data = createData(form_name,data); // se crea el data específico para cada controlador
        saveItem(data,conf_data.url,id_holder,form);
    });
}

function getFieldsAndUrl(form_name){
    switch(form_name){
        case systems.infection:
            return {
                fields:infection_items,
                url:infection_ots_url
            }
            break;
        case systems.ophthalmological:
            return {
                fields:ophthalmological_items,
                url:ophthalmological_sis_url
            }
            break;
        case systems.hematological:
            return {
                fields:hematological_items,
                url:hematological_sis_url
            }
            break;
        case systems.metabolic:
            return {
                fields:metabolic_items,
                url:metabolic_sis_url
            }
            break;
        case systems.otolaryngology:
            return {
                fields:otolaryngology_items,
                url:otolaryngology_sis_url
            }
            break;
        case systems.nervous:
            return {
                fields:nervous_items,
                url:nervous_sis_url
            }
            break;
        case systems.cardio:
            return {
                fields:cardio_items,
                url:cardiovascular_aps_url
            }
            break;
        case systems.respiratory:
            return {
                fields: respiratory_items,
                url:respiratory_aps_url
            }
            break;
        case systems.digestive:
            return {
                fields: digestive_items,
                url:digestive_aps_url
            }
            break;
        case systems.urinary:
            return {
                fields: urinary_items,
                url:urinary_aps_url
            }
            break;
        case systems.psychosomatic:
            return {
                fields: psychosomatic_items,
                url:psychosomatic_sis_url
            }
            break;
        case systems.genital:
            return {
                fields: genital_items,
                url:genital_aps_url
            }
            break;
        case systems.locomotor:
            return {
                fields: locomotor_items,
                url:locomotor_sis_url
            }
            break;
        case systems.sense_organ:
            return {
                fields: sense_organ_items,
                url:sense_organ_url
            }
            break;
    }
}

function createData(form, data){
    switch(form){
        case systems.infection:
            return {
                infection_ot:data
            };
            break;
        case systems.ophthalmological:
            return {
                ophthalmological_si:data
            };
            break;
        case systems.hematological:
            return {
                hematological_si:data
            };
            break;
        case systems.metabolic:
            return {
                metabolic_si:data
            };
            break;
        case systems.otolaryngology:
            return {
                otolaryngology_si:data
            };
            break;
        case systems.nervous:
            return {
                nervous_si:data
            };
            break;
        case systems.cardio:
            return {
                cardiovascular_ap:data
            };
            break;
        case systems.respiratory:
            return {
                respiratory_ap:data
            };
            break;
        case systems.digestive:
            return {
                digestive_ap:data
            };
            break;
        case systems.urinary:
            return {
                urinary_ap:data
            };
            break;
        case systems.psychosomatic:
            return {
                psychosomatic_si:data
            };
            break;
        case systems.genital:
            return {
                genital_ap:data
            };
            break;
        case systems.locomotor:
            return {
                locomotor_si:data
            };
            break;
        case systems.sense_organ:
            return {
                sense_organ:data
            };
            break;
    }
}

function saveItem(data,url,id_holder,form){
    var method = "POST";
    if(id_holder.val() != ""){
        method = "PUT";
        url += "/"+id_holder.val();
    }

    $.ajax({
        method:method,
        url:url,
        data:data,
        dataType:"json",
        success:function(data){
            if(data.done){
                $('#'+form+' div.response_message').show();
                $('#'+form+' div.response_message').html("<div class='alert alert-success'>"+data.message+"</div>");
                $('#'+form+' div.response_message').delay( 1500 ).slideUp( 300 );

                if(data.id != undefined){
                    id_holder.val(data.id);
                }

                $("#"+form+' .save_data_system').prop("disabled",false);
            }
        }
    });
}