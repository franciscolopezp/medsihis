function saveInheritedFamilyBackground(){

    var medical_history_id = $("#medical_history_id_ifb").val();

    var inherited_family_background = {
        gynecological_diseases:$("#inherited_family_background_gynecological_diseases").is(":checked"),
        gynecological_diseases_description:$("#inherited_family_background_gynecological_diseases_description").val(),
        mammary_dysplasia:$("#inherited_family_background_mammary_dysplasia").is(":checked"),
        mammary_dysplasia_description:$("#inherited_family_background_mammary_dysplasia_description").val(),
        twinhood:$("#inherited_family_background_twinhood").is(":checked"),
        twinhood_description:$("#inherited_family_background_twinhood_description").val(),
        congenital_anomalies:$("#inherited_family_background_congenital_anomalies").is(":checked"),
        congenital_anomalies_description:$("#inherited_family_background_congenital_anomalies_description").val(),
        infertility:$("#inherited_family_background_infertility").is(":checked"),
        infertility_description:$("#inherited_family_background_infertility_description").val(),
        others:$("#inherited_family_background_others").is(":checked"),
        others_description:$("#inherited_family_background_others_description").val(),
        anotations:$("#inherited_family_background_anotations").val()
    };


    $.ajax({
        url:"/doctor/update_inherited_family_backgrounds",
        dataType:"json",
        data:{
            inherited_family_background:inherited_family_background,
            medical_history_id:medical_history_id
        },
        success:function(data){
            $("#success_message_inherited_family_background").show();
            setTimeout(function(){
                $("#success_message_inherited_family_background").hide();
            },1500);
        },
        error:function(){
            swal("Error","Ocurrio un error inesperado. Por favor intente otra vez.","error");
        }
    });

}