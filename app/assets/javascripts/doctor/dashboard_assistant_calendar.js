//= require doctor/calendar-doctor.js

var ACTIVITY_TYPE = 0;
var IS_OWNER = false;
var A_TYPE = {
    appointment : 1,
    surgery : 2
};

var act_status = {
    created:1,
    waiting:2,
    consult:3,
    attended:4,
    cancelled:5
};

var calendar_id = "#calendar_assistant";
var cal = {
    min:"col-sm-2",
    res:"col-sm-6",
    max:"col-sm-12"
};

var cal_session_key = "calendar_size";

var cal_sizes = {
    res:"res",
    max:"max"
};

var monthNames = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
    'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

var loaded_activities = false;
var is_doctor = false;

function resCalendar(){
    $(calendar_id).fullCalendar('option', 'height', 300);
    $(calendar_id).parent().attr("class",cal.res);
    $(calendar_id).show();
    $(calendar_id+"_min").hide();
    $.session.set(cal_session_key,cal_sizes.res);
    $("#min_calendar_assistant").show();
    $("#res_calendar_assistant").hide();
    $("#max_calendar_assistant").show();
    if(!loaded_activities){
        //loadAssistantActivities();
        loaded_activities = true;
    }
}

function maxCalendar(){
    $(calendar_id).fullCalendar('option', 'height', 600);
    $(calendar_id).parent().attr("class",cal.max);
    $(calendar_id).show();
    $(calendar_id+"_min").hide();
    $.session.set(cal_session_key,cal_sizes.max);
    $("#min_calendar_assistant").show();
    $("#res_calendar_assistant").show();
    $("#max_calendar_assistant").hide();
    if(!loaded_activities){
        //loadAssistantActivities();
        loaded_activities = true;
    }
}

$(function(){
    $("#res_calendar_assistant").click(resCalendar);
    $("#max_calendar_assistant").click(maxCalendar);
});


function loadAssistantActivities(conf){
    if($("#is_agenda_page").val() == "yes"){
        loadActivities(calendar_id,assistant_permissions.read,conf);
    }
}

$(document).ready(function() {
    loadAssistantActivities({
        date: new Date(),
        view:"month"
    });
    $(".fc-toolbar").css("background","#00BCD4");
    setLastWindowConf();
});


function loadActivityCallback(event){
    if(event.owner_type == 'Assistant' && event.owner_id == assistant_id){
        IS_OWNER  = true;
    }else{
        IS_OWNER = false;
    }

    var can_edit = false;
    var can_delete = false;

    $(".activity_office").hide();
    if(assistant_permissions.create.length < 2){
        $("#office_all").hide();
    }else{
        $("#office_all").show();
    }

    $.each(assistant_permissions.create,function(idx,o){
        $("#office_"+o).show();
    });

    $.each(event.office_id,function(idx,o){

        if(assistantHasPermission(o,"update")){
            $("#office_"+o).show();
            can_edit = true;
        }

        if(assistantHasPermission(o,"delete")){
            can_delete = true;
        }
    });

    if(!can_edit && !can_delete){
        swal("Hay un problema", "Usted no tiene permisos para modificar o eliminar esta actividad", "error");
        throw 'Usted no tiene permisos para realizar alguna acción';
    }

    if(can_edit){
        $("#update_activity_btn").show();
    }else{
        $("#update_activity_btn").hide();
    }

    if(can_delete){
        $("#delete_activity_btn").show();
    }else{
        $("#delete_activity_btn").hide();
    }

    if(event.activity_type_id == A_TYPE.appointment){
        $("#in_w_room_btn").show();
    }else{
        $("#in_w_room_btn").hide();
    }

    $("#activity_info").html(getActivityHtml(event));
    $("#activity_operation").modal("show");
}

function assistantHasPermission(office_id,action){
    if (assistant_permissions[action].indexOf(office_id) > -1){
        return true;
    }else{
        return false;
    }
}


var inputsValidate = ["#activity_name",
    "#activity_start_date","#activity_end_date",
    "#activity_start_time","#activity_end_time"];

function setPatientInWroom(){
    var params = {
        activity_id: ACTIVITY_UPDATE.id,
        activity_status_id : act_status.waiting
    };
    $("#activity_operation").modal("hide");
    $.ajax({
        url:"/doctor/activities/update_status",
        data:params,
        dataType:"json"
    });
}

function setAppointmentAsAttended(activity_id){
    var params = {
        activity_id: activity_id,
        activity_status_id : act_status.attended
    };
    $.ajax({
        url:"/doctor/activities/update_status",
        data:params,
        dataType:"json"
    });
}