var jqConfirm = function(msg, success) {
    var dialogObj = $("<div style='display:none'>"+msg+"</div>");
    $("body").append(dialogObj);
    $(dialogObj).dialog({
        resizable: false,
        height:140,
        modal: true,
        buttons: {
            "OK": function() {
                success();
                $( this ).dialog( "close" );
            },
            Cancel: function() {
                $( this ).dialog( "close" );
            }
        }
    });
};
jQuery(document).ready(function() {
    var grid = $("#data-table-clinicalgroup").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function()
        {
            var laboratory = $('#lab_id').val();
            return  $('#data-table-clinicalgroup').data('source')+"?lab_id="+laboratory;
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";

            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el grupo de analisis clínico "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        if(result.done){
                            swal("Eliminado!", "El registro ha sido eliminado exitosamente.", "success");
                            location.reload();
                        }else{
                            swal(result.error,result.message,'error');
                        }
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });




});

