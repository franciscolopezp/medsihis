$(function(){
   /* var doctor = jQuery.parseJSON($('#doctor_info').val());

    if(!doctor.accept_term_conditions){
        open_modal_terms();
    }*/
});

function open_modal_terms(){
    var url = $('#url_terms_condition').val();
    $.ajax({
        type: "get",
        url: url,
        dataType:'html',
        success: function (response) {
            $('#modal_terms').html(response);

            $("#modal_terms").modal('show');
            $('#accept_terms_conditions').click(function(){
                $( "#accept_terms_conditions" ).prop( "disabled", true );
                update_terms_conditions();
            });
            $('#cancel_terms_conditions').click(function(){
                log_out_medsi();
            });
        },
        error: function (error) {
            swal("Error","Error al cargar el formulario de consulta","error");
        }
    });
}

function update_terms_conditions(){
    var url = $('#update_terms').val();
    var doctor = jQuery.parseJSON($('#doctor_info').val());
    $.ajax({
        type: "put",
        url: url,
        dataType: 'json',
        data:{id:doctor.id},
        success: function (response) {
            window.location.reload(true);
        }
    });
}

function log_out_medsi(){
    window.location = $('#log_out').val();
}