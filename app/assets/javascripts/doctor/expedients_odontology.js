var pathologies = [];
var treatments = [];
var section_id = 0;
function initOdontology(){
    $("#table_pathologies").hide();
    $("#table_treatments").hide();
    $.ajax({
        url: $("#url_get_last_data").val(),
        dataType: "json",
        data: {medical_history_id:$("#medical_history_id").val()},
        success: function (data) {
            if(data.done){
                var array_sorted = data.history.sort(function(a,b){
                    return new Date(a.date) - new Date(b.date);
                });
                $.each(array_sorted,function (idx,info){
                    if(info.type == 0){
                        $('#div_section_'+info.section).css('background-color', 'red');
                    }else{
                        $('#div_section_'+info.section).css('background-color', 'blue');
                    }
                });
             /*   $.each(data.pathologies, function (idx, info) {
                    $('#div_section_'+info.section).css('background-color', 'red');
                });
                $.each(data.treatments, function (idx, info) {
                    $('#div_section_'+info.section).css('background-color', 'blue');
                });*/
            }else{
                swal("Error!", "Ocurrio un error al guardar el odontograma", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function show_original(){
    $(".horizontal_div").css('background-color', '#ffffff');
    if($('#show_original').is(':checked')){
        $.ajax({
            url: $("#url_get_original_odontogram").val(),
            dataType: "json",
            data: {medical_history_id:$("#medical_history_id").val()},
            success: function (data) {
                if(data.done){
                    $.each(data.pathologies, function (idx, info) {
                        $('#div_section_'+info.section).css('background-color', 'red');
                    });
                    $.each(data.treatments, function (idx, info) {
                        $('#div_section_'+info.section).css('background-color', 'blue');
                    });
                }else{
                    swal("Error!", "Ocurrio un error al guardar el odontograma", "error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else{
        $(".horizontal_div").css('background-color', '#ffffff');
        initOdontology();
    }

}
function save_odontogram(){
    $.ajax({
        url: $("#url_get_save_odontogram").val(),
        dataType: "json",
        data: {pathologies:pathologies,treatments:treatments,
            medical_history_id:$("#medical_history_id").val()},
        success: function (data) {
            if(data.done){
                swal("Exito", "Odontograma guardado con exito", "success");
                location.reload();
            }else{
                swal("Error!", "Ocurrio un error al guardar el odontograma", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}

function add_action(teeth_name,name,id){
    $("#indication_description").val("");
    $("#indication_type").val(1).selectpicker("refresh");
    section_id = id;
    $("#section_name").html(teeth_name);
    $("#add_action_modal").modal("show");
    $.ajax({
        url: $("#url_get_teeth_historial").val(),
        dataType: "json",
        data: {medical_history_id:$("#medical_history_id").val(),section_id:id},
        success: function (data) {
            if(data.done){
                if(data.pathologies.length > 0){
                    $("#pathologies_historical").html("");
                    $.each(data.pathologies, function (idx, info) {
                        $("#pathologies_historical").append("<tr ><td style='text-align: center'>"+
                            info.date+"</td><td  style='text-align: center'>"+info.teeth+"</td>"+
                            "<td  style='text-align: center' >"+info.description+"</td></tr>");
                        $("#table_pathologies").show();
                    });
                }else{
                    $("#table_pathologies").hide();
                }

                if (data.treatments.length > 0){
                    $("#treatments_historical").html("");
                    $.each(data.treatments, function (idx, info) {
                        $("#treatments_historical").append("<tr ><td style='text-align: center'>"+
                            info.date+"</td><td  style='text-align: center'>"+info.teeth+"</td>"+
                            "<td  style='text-align: center' >"+info.description+"</td></tr>");
                        $("#table_treatments").show();
                    });
                }else{
                    $("#table_treatments").hide();
                }

            }else{
                swal("Error!", "Ocurrio un error al guardar el odontograma", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function save_indication(){
    if($("#indication_description").val()!=""){
    if($("#indication_type").val() == 2){
        treatments.push({
            section_id:section_id,
            description:$("#indication_description").val()
        });
        $('#div_section_'+section_id).css('background-color', 'blue');
    }else{
        pathologies.push({
            section_id:section_id,
            description:$("#indication_description").val()
        });
        $('#div_section_'+section_id).css('background-color', 'red');
    }
    $("#add_action_modal").modal("hide");
    }else{
        swal("Error","Debe ingresar una descripción","error");
    }

}