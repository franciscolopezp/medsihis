var numberImgagesReport = 1;
$(document).ready(function(){
    $('#btn-new-analysis-order').click(function(e)
    {
        $('#loading_btn_save_analysis_order').hide();
        $('#list_error_analysis_order').empty();
        $('#btn-save-analysis-order').show();
        loadingFormNewAnalysisOrder();
    });

    $('#btn-new-report-analysis-order').click(function (e) {
        $('#loading_btn_save_report_analysis_order').hide();
        $('#list_error_report_analysis_order').empty();
        $('#btn-save-report-analysis-order').show();
        initHtmlEditor("#report_sumary");
    });
    $('#add_image_report').click(function (e){
       $("#report_images").append(
           "<div class='col-md-6' id='image_continer_"+numberImgagesReport+"'>"+
           "   <div class='' style='text-align: center'>"+
           "  <div class='fileinput fileinput-new ' data-provides='fileinput' id='report_img_"+numberImgagesReport+"'>"+
        "  <div class='fileinput-preview thumbnail' id='div_img_"+numberImgagesReport+"' data-trigger='fileinput'>"+
        "   </div>"+
        "  <div>"+
        "  <span class='btn btn-info btn-file waves-effect'>"+
        "  <span class='fileinput-new'><i class='zmdi zmdi-cloud-upload zmdi-hc-fw'></i></span>"+
        " <span class='fileinput-exists'><i class='zmdi zmdi-edit zmdi-hc-fw'></i></span>"+
        "  <input type='hidden'><input accept='image/png,image/gif,image/jpeg' class='' placeholder='' type='file' name='imagen_code_"+numberImgagesReport+"' id='imagen_code_"+numberImgagesReport+"'>"+
        " </span>"+
        " <a href='#' class='btn bgm-gray fileinput-exists waves-effect' data-dismiss='fileinput'><i class='zmdi zmdi-delete zmdi-hc-fw'></i></a>"+
        "  </div>"+
        "  </div>"+
        " </div>"+
        " <div class='row'>"+
        " <div class='col-md-10'>"+
        " <textarea class='form-control' rows='2' placeholder='Escriba sus anotaciones' name='anotation_"+numberImgagesReport+"' id='anotation_"+numberImgagesReport+"'></textarea>"+
        "</div>"+
           " <div class='col-md-2' style='text-align: left;'>"+
           " <a class='btn btn-danger btn-icon waves-effect waves-circle waves-float' onclick='deleteBlockImage("+numberImgagesReport+")'><i class='zmdi zmdi-delete zmdi-hc-fw'></i></a>"+
           "</div>"+
        "</div>"+
           "</div>"
       );
        numberImgagesReport++;
    });
});

function deleteBlockImage(id_section){
    $('#image_continer_'+id_section).remove();
}
function loadingFormNewAnalysisOrder(){
    showLoading("form-new-analysis-order");
    setDisableBtn('btn-save-analysis-order', true);
    var url = $('#url_form_analysis_order').val();
    var expId = $('#expedient_id').val();
    $.ajax({
        type: "get",
        url: url,
        data: {id:expId},
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("form-new-analysis-order",response);
            initElementAnalysis();
        },
        error: function (error) {
            swal("Error!", "Error al cargar el formulario de analisis clinico", "error");
        }
    });
}


var CLINICAL_ORDER_TYPE = null;
function initElementAnalysis(){
    $("input[name=clinical_order_type]:radio").change(function () {
        CLINICAL_ORDER_TYPE = $(this).val();

        $("option.lab").hide();
        $("option.type_"+$(this).val()).show();
        $("#laboratory_select").val("").selectpicker('refresh');    
        hideAnnotationOrResult($(this).val());

        console.log($(this).val());

        $("#doctor_clinical_study_type").html("").selectpicker('refresh');


        $('#list_clinical_analysis').html("");
        $('#list_clinical_analysis').trigger('bootstrapDualListbox.refresh' , true);
    });

    $('.selectpicker').selectpicker();

    $("#laboratory_select").change(loadCategoriesByLab);
    $("#doctor_clinical_study_type").change(loadAnalysisByCategory);

    $('#list_clinical_analysis').bootstrapDualListbox({
        nonSelectedListLabel: 'Lista de estudios',
        selectedListLabel: 'Estudios seleccionados',
        preserveSelectionOnMove: 'moved',
        moveOnSelect: false,
        nonSelectedFilter: '',
        filterPlaceHolder: 'Filtrar',
        filterTextClear: 'Mostrar todos',
        moveSelectedLabel: 'Mover seleccionado',
        moveAllLabel: 'Mover todo',
        infoTextFiltered: '<span class="label label-info">Filtrado</span> {0} de {1}',
        infoTextEmpty: '0 Estudios',
        infoText: '{0} Estudios'
    });

    $('#list_clinical_analysis').on('change',function(){
        $('#indications_study_selected').empty();
        var url = $('#show_indications').val();

        $.ajax({
            type: "get",
            url: url,
            dataType:'json',
            data: getJsonOrder(),
            success: function (response) {
                var indications = response.indications;
                var list = "<ul>";
                $.each(indications, function(index,val){
                    list = list + "<li>"+ val.description+ "</li>";
                });
                list = list + "</ul>";
                $('#indications_study_selected').html(list);
            },
            error: function (error) {
                swal("Error!", "Error al presentar la lista de indicaciones", "error");
            }
        });
    });

    $("#btn-save-analysis-order").unbind();
    $('#btn-save-analysis-order').click(function(e)
    {
        //bloquear el boton de guardar, mostrar loading
        setDisableBtn('btn-save-analysis-order',true);
        $('#loading_btn_save_analysis_order').show();
        saveClinicalAnalysisOrder();
    });

    $("#package_id").change(function(){

        if($("#package_id").val() == ""){
            return;
        }

        var selected = [];
        $.ajax({
            url: $("#url_load_studies_by_package").val(),
            data: {package_id: $("#package_id").val(), studies_selected: selected},
            dataType:'json',
            success: function (response) {
                $("#list_clinical_analysis").html("");
                $.each(response.result,function(idx, item){
                    $('#list_clinical_analysis').append($('<option>',{
                        value: item.id,
                        text: item.name
                    }));
                });
                $.each(response.selected,function(idx, item){
                    $('#list_clinical_analysis').append($('<option>',{
                        value: item.id,
                        text: item.name,
                        selected:'selected'
                    }));
                });
                $('#list_clinical_analysis').trigger('bootstrapDualListbox.refresh' , true);

            },
            error: function (error) {
                swal("Error!", "Error al cargar los analisis del laboratorio", "error");
            }
        });
    });

    setDisableBtn('btn-save-analysis-order',false);

}

function getJsonOrder() {
    var clinicalOrder = $("#list_clinical_analysis").val();
    var result = new Array();
    if(clinicalOrder != null){
        var i;
        for(i=0;i<clinicalOrder.length;i++){
            var id = clinicalOrder[i];
            result.push({'id':id});
        }
    }
    var jsonDiseases = {"name": 'analysis[clinical_analysis_order]', "value": JSON.stringify(result)};
    return jsonDiseases;
}

function loadAnalysisByCategory() {
    var category = $("#doctor_clinical_study_type").val();
    var url = $('#url_find_clinical_studies').val();
    var selected = $("#list_clinical_analysis").val();

    if(selected == null){
        selected = [];
    }

    $.ajax({
        type: "get",
        url: url,
        data: {study_type: category, study_selected: selected},
        dataType:'json',
        success: function (response) {


            $("#list_clinical_analysis").html("");
            $.each(response.result,function(idx, item){
                $('#list_clinical_analysis').append($('<option>',{
                    value: item.id,
                    text: item.name
                }));
            });

            $.each(response.selected,function(idx, item){
                $('#list_clinical_analysis').append($('<option>',{
                    value: item.id,
                    text: item.name,
                    selected:'selected'
                }));
            });
            $('#list_clinical_analysis').trigger('bootstrapDualListbox.refresh' , true);

        },
        error: function (error) {
            swal("Error!", "Error al cargar los analisis del laboratorio", "error");
        }
    });

}

function loadCategoriesByLab(){
    loadPackagesByLab();
    $('#list_clinical_analysis').html("");
    $('#list_clinical_analysis').trigger('bootstrapDualListbox.refresh' , true);
    var lab = $("#laboratory_select").val();
    var url = $("#url_load_categories").val();

    $.ajax({
        url:url,
        data:{lab_id:lab},
        dataType:"json",
        success:function(data){
            $("#doctor_clinical_study_type").html("");
            $('#doctor_clinical_study_type').append($('<option>',{
                value: '',
                text: 'Seleccione una categoría'
            }));
            $.each(data,function(idx, item){
                $('#doctor_clinical_study_type').append($('<option>',{
                    value: item.id,
                    text: item.name
                }));
            });
            $("#doctor_clinical_study_type").selectpicker("refresh");
        }
    });
}

function loadPackagesByLab(){
    var lab = $("#laboratory_select").val();
    var url = $("#url_load_packages").val();
    $.ajax({
        url:url,
        data:{laboratory_id:lab},
        dataType:"json",
        success:function(data){
            $("#package_id").html("");
            $('#package_id').append($('<option>',{
                value: '',
                text: 'Seleccione un paquete de estudios'
            }));
            $.each(data,function(idx, item){
                $('#package_id').append($('<option>',{
                    value: item.id,
                    text: item.name
                }));
            });
            $("#package_id").selectpicker("refresh");
        }
    });
}


function hideAnnotationOrResult(value){
    $('#doctor_annotations').val('');
    $('#doctor_result').val('');

    switch(parseInt(value)){
        case 1: // Estudio clinico
            $('#result_order').hide();
            $('#annotations_order').show();
            break;
        case 2: // Estudio de Gabinete
            $('#result_order').show();
            $('#annotations_order').hide();
            break;
        default:

            break;
    }
}


function saveClinicalAnalysisOrderReport(){
    var annotationArray = [];
    var imagesArray = [];

    var image_holders = $("#report_images .col-md-6");

    $("#report_images img").map(function(){
        imagesArray.push($(this).attr('src'));
    });
    $("#report_images textarea").map(function(){
        annotationArray.push($(this).val());
    });
    var url = $('#url_save_report_analysis_order').val();

    var cleanText = $("#report_sumary").summernote().code();

    if(image_holders.length != imagesArray.length){
        swal("Error","Por favor seleccione una imagen para continuar...","error");
        return;
    }

    if(cleanText == "<p><br></p>"){
        swal("Error","Por favor capture el reporte de resultados...","error");
        return;
    }

    setDisableBtn('btn-save-report-analysis-order',true);
    $('#loading_btn_save_report_analysis_order').show();

    $.ajax({
        type: "PUT",
        url: url,
        dataType:'json',
        data: {
            expedient_id:$("#expedient_id").val(),
            report_sumary:cleanText,
            clinical_study_cabinet:$("#clinical_study_cabinet").val(),
            office_id:$("#new_report_office").val(),
            images:JSON.stringify(imagesArray),
            annotations:JSON.stringify(annotationArray),
            laboratory_id:$("#clinical_study_cabinet_laboratory").val(),
            price:$("#cabinet_report_price").val()
        },
        success: function (response) {
            if(response.ok == "true"){
                var url_has_template = response.url_print;
                window.open(url_has_template);
                $('#modalReportAnalysis').modal('hide');
                searchLastAnalysisOrder();
                setDisableBtn('btn-save-report-analysis-order',false);
                $("#report_sumary").summernote().code("");
                $("#report_images").html("");
            }else{
                //setErrorsModalAnalysisOrders(response.errors);
            }
        },
        error: function (error) {
            swal("Error!", "Error al guardar el reporte, consulte al administrador", "error");
        }
    });
}
function saveClinicalAnalysisOrder(){
    var formClinical = $('#new_clinical_order').serializeArray();
    var json = JSON.stringify(formClinical);
    var jsonReal = JSON.parse(json);

    jsonReal.push(getJsonOrder());
    jsonReal.push({"name": 'analysis[laboratory_id]', "value": $("#laboratory_select").val()});
    jsonReal.push({"name": 'analysis[office_id]', "value": $('#order_office').val()});
    jsonReal.push({"name": 'analysis[annotations]', "value": $('#doctor_annotations').val()});
    jsonReal.push({"name": 'analysis[result]', "value": $('#doctor_result').val()});
    jsonReal.push({"name": 'analysis[clinical_order_type_id]', "value": CLINICAL_ORDER_TYPE});
    var url = $('#url_form_create_analysis_order').val();

    $.ajax({
        type: "post",
        url: url,
        dataType:'json',
        data: jsonReal,
        success: function (response) {
            if(response.ok == "true"){
                var url_has_template = response.url_print;
                window.open(url_has_template);
                $('#modalAnalysis').modal('hide');
                searchLastAnalysisOrder();
            }else{
                setErrorsModalAnalysisOrders(response.errors);
            }
        },
        error: function (error) {
            swal("Error!", "Error al guardar la orden, consulte al administrador", "error");
        }
    });
}

function setErrorsModalAnalysisOrders(errors){
    //var keys = Object.keys(errors);
    var html = "<div id='error_explanation'> <h3>"+errors.length+" errores encontrados:</h3>";
    var i;
    html += "<ul>";
    for(i=0; i<errors.length; i++){
        var error = errors[i];
        html += "<li>"+error+"</li>";
    }
    html += "</ul>";
    html += "</div>";
    $('#list_error_analysis_order').empty();
    $('#list_error_analysis_order').append(html);
    setDisableBtn('btn-save-analysis-order',false);
    hideLoadingAndSetContainer("loading_btn_save_analysis_order","");
}

