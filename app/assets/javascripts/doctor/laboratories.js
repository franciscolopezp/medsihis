jQuery(document).ready(function() {

    var grid = $("#data-table-laboratory").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-laboratory').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                if(!row.custom){
                    return "";
                }
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Configurar\"><i class=\"zmdi zmdi-settings zmdi-hc-fw\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            },
            "configs":function(column, row){
                if(!row.custom){
                    return "";
                }
                return "<a target='_blank' href=" + row.url_ca_groups + " class=\"btn bgm-green btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Grupos de Indicaciones\"><i class=\"zmdi zmdi-collection-item zmdi-hc-fw\"></i></a>" +
                    "<a target='_blank' href=" + row.url_indications + " class=\"btn bgm-bluegray btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Indicaciones\"><i class=\"zmdi zmdi-receipt zmdi-hc-fw\"></i></a>" +
                    "<a target='_blank' href=" + row.url_study_categories + " class=\"btn bgm-blue btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Categoría de Estudios\"><i class=\"zmdi zmdi-format-list-bulleted zmdi-hc-fw\"></i></a>" +
                    "<a target='_blank' href=" + row.url_studies + " class=\"btn bgm-purple btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Estudios\"><i class=\"zmdi zmdi-format-align-justify zmdi-hc-fw\"></i></a>"+
                    "<a target='_blank' href=" + row.url_packages + " class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Paquetes\"><i class=\"zmdi zmdi-case zmdi-hc-fw\"></i></a>";

            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar laboratorio "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        if(result.done){
                            swal("Eliminado!", "El registro ha sido eliminado exitosamente.", "success");
                            location.reload();
                        }else{
                            swal(result.error,result.message,'error');
                        }
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

});

var SELECTED_STUDY = 0;

$(function(){
    $("#study_category_select").change(function () {
        $("#study_indications_holder").html("");
        $.ajax({
            url:$("#studies_by_category_url").val(),
            dataType:"json",
            data:{category_id:$(this).val()},
            success:function(data){

                SELECTED_STUDY = 0;

                $("#studies_holder").html("");
                $.each(data,function(idx, study){
                    $("#studies_holder").append("<div class='list_item study' sid='"+study.id+"'>"+study.name+"</div>");
                });

                // cuando se haga click a un estudio cargar las indicaciones
                $(".list_item.study").click(function(){
                    $(".list_item.study").removeClass("active");
                    $(this).addClass("active");
                    var study_id = $(this).attr("sid");
                    loadStudyIndications(study_id)
                });
            }
        });
    });


    $("#group_indications_select").change(function(){
        $.ajax({
            url:$("#indications_by_group_url").val(),
            dataType:"json",
            data:{group_id:$(this).val()},
            success:function(data){
                $("#indications_holder").html("");
                $.each(data,function(idx, indication){
                    $("#indications_holder").append(
                        "<div class='list_item' iid='"+indication.id+"'>" +
                        "<i class='zmdi zmdi-arrow-left zmdi-hc-fw add_indication'></i>"+
                        indication.description+
                        "</div>"
                    );
                });

                $(".add_indication").click(function(){
                    var indication_id = $(this).parent().attr("iid");

                    if(SELECTED_STUDY == 0){
                        swal("No se puede continuar","Por favor selecciona un estudio","error");
                        return;
                    }

                    $.ajax({
                        url:$("#add_indication_url").val(),
                        dataType:"json",
                        data:{study_id:SELECTED_STUDY, indication_id:indication_id},
                        success:function(data){
                            if(data.done){
                                loadStudyIndications(SELECTED_STUDY);
                            }else{
                                swal(data.error,data.message,"error");
                            }
                        }
                    });
                });
            }
        });
    });

});

function loadStudyIndications(study_id){

    SELECTED_STUDY = study_id;
    $.ajax({
        url:$("#study_indications_url").val(),
        dataType:"json",
        data:{id:study_id},
        success:function(data){
            $("#study_indications_holder").html("");
            $.each(data,function(idx,indication){
                $("#study_indications_holder").append("" +
                    "<div id='ind_"+indication.id+"' ind_id='"+indication.id+"' class='list_item'>"+
                    indication.description+
                    "<i class='zmdi zmdi-close zmdi-hc-fw remove_indication'></i>"+
                    "</div>"
                );
            });

            $(".remove_indication").click(function(){
                var indication_id = $(this).parent().attr("ind_id");
                deleteIndication(indication_id);
            });
        }
    });
}

function deleteIndication(indication_id) {
    $.ajax({
        url:$("#delete_indication_url").val(),
        dataType:"json",
        data:{indication_id:indication_id,study_id:SELECTED_STUDY},
        success:function(data){
            if(data.done){
                $("#ind_"+indication_id).remove();
            }
        }
    });
}
