//= require highcharts
//= require highcharts/highcharts-more
//= require doctor/statistics_diseases

var consults_chart = false;
var chart_consult;
var chart_transac;
var chart_genre;
var chart_money;
var chart_age;
var chart_cities;
var bar_chart;

jQuery(document).ready(function() {
    if($('#chart_day').val()=="1") {
        var actual_date = new Date();
        var before_date = new Date();
        before_date.setDate(before_date.getDate() - 28);
        var start_date_set = before_date.getDate() + "/" + (before_date.getMonth() + 1) + "/" + before_date.getFullYear();
        var end_date_set = actual_date.getDate() + "/" + (actual_date.getMonth() + 1) + "/" + actual_date.getFullYear();
        $('#start_date').val(start_date_set);
        $('#end_date').val(end_date_set);

        var fecha_inicio = $('#start_date').val();
        var fecha_fin = $('#end_date').val();
        var from = fecha_inicio.split("/");
        var until = fecha_fin.split("/");
        var f = new Date(from[2], from[1] - 1, from[0]);
        var fecha_cadena = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();
        f = new Date(until[2], until[1] - 1, until[0]);
        var fecha_cadena_fin = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + (f.getDate() + 1);

        chart_consult = Morris.Line({
            element: 'consultation_chart',
            data: $('#consultation_chart').data('consultations'),
            xkey: 'date_consultation',
            ykeys: ['price', 'consults'],
            labels: ['Total', 'No. consultas']
        });
        chart_transac = Morris.Area({
            element: 'money_chart',
            data: $('#money_chart').data("transactions"),
            xkey: 'date',
            ykeys: ['egreso', 'entry', 'total'],
            labels: ['Egreso', 'Ingreso', 'Balance']
        });
        chart_genre = Morris.Donut({
            element: 'genre_chart',
            data: [
                {label: "Hombres", value: 0},
                {label: "Mujeres", value: 0}
            ]
        });
        chart_money = Morris.Donut({
            element: 'money_pie',
            data: [
                {label: "Ingresos", value: 0},
                {label: "Egresos", value: 0}
            ]
        });
        chart_age = Morris.Donut({
            element: 'age_chart',
            data: [
                {label: "Niños", value: 0},
                {label: "Adolescentes", value: 0},
                {label: "Adultos", value: 0},
                {label: "Adultos mayores", value: 0}
            ]
        });
        chart_cities = Morris.Donut({
            element: 'state_chart',
            data: [
                {label: "Merida", value: 0},
                {label: "Quintana roo", value: 0},
                {label: "Campeche", value: 0}
            ]
        });
        $.ajax({
            url: $('#url_get_genres_chart').val(),
            dataType: "json",
            data: {ini_date: fecha_cadena, end_date: fecha_cadena_fin},
            success: function (response) {
                if (response.result) {
                    $('#consultation_amount').text(response.consultations);
                    $('#consultation_amount_total').text(response.consultations_total);
                    $('#patients_amount').text(response.patients);
                    $('#laboratory_orders_amount').text(response.laboratory_orders);
                    $('#surgeries_amount').text(response.surgeries);
                    $('#invoices_amount_cancel').text(response.invoices_cancel);
                    $('#invoices_amount_stamp').text(response.invoices_stamp);
                    $('#balance_total').text(response.total_amount.toFixed(2));
                    $('#ingress').text(response.entry.toFixed(2));
                    $('#egress').text(response.egress.toFixed(2));
                    $('#patients_amount_total').text(response.patients_total);
                    $('#laboratory_orders_amount_total').text(response.laboratory_order_total);
                    $('#surgeries_amount_total').text(response.surgeries_total);
                    $('#invoices_amount_cancel_total').text(response.invoices_cancel_total);
                    $('#invoices_amount_stamp_total').text(response.invoice_stamp_total);
                    $('#balance_total_total').text(response.total_amount_total.toFixed(2));
                    $('#ingress_total').text(response.entry_total.toFixed(2));
                    $('#egress_total').text(response.egress_total.toFixed(2));

                    sparklinePie('stats-pie_co', [response.consultations_total, response.consultations], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_p', [response.patients_total, response.patients], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_l', [response.laboratory_order_total, response.laboratory_orders], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_c', [response.surgeries_total, response.surgeries], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_f', [response.invoice_stamp_total, response.invoices_stamp], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_fc', [response.invoices_cancel_total, response.invoices_cancel], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_b', [response.total_amount_total, response.total_amount], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_i', [response.entry_total, response.entry], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_e', [response.egress_total, response.egress], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);

                    chart_age.setData([
                        {label: "Niños", value: response.childrens},
                        {label: "Adolescentes", value: response.teenagers},
                        {label: "Adultos", value: response.adults},
                        {label: "Adultos mayores", value: response.oldpeople}
                    ]);
                    chart_genre.setData([
                        {label: "Hombres", value: response.mens},
                        {label: "Mujeres", value: response.women}
                    ]);
                    chart_money.setData([
                        {label: "Ingresos", value: response.entry},
                        {label: "Egresos", value: response.egress}
                    ]);
                } else {
                    showAlertTemplate("Ocurrio un problema al cargar grafica de generos.");
                }
            },
            error: function (response) {
            }
        });
        $.ajax({
            url: $('#url_get_cities_chart').val(),
            dataType: "json",
            data: {ini_date: fecha_cadena, end_date: fecha_cadena_fin},
            success: function (response) {
                cities = '[';
                $.each(response, function (idx, info) {
                    cities += '{"label":"' + info.name + '","value":' + info.value + '},';
                });
                cities = cities.substring(0, cities.length - 1);
                cities += "]";
                chart_cities.setData(JSON.parse(cities));

            },
            error: function (response) {
                console.log(response);
            }
        });
    }
/////////////////////////////////////////////COMPARATIVAS////////////////////////////////////////////////////
    else {
        var actual_date = new Date();
        actual_month = actual_date.getMonth()+1;
        $('#mes_ini_actual').val(actual_month);
        $('#mes_fin_actual').val(actual_month);
        $('#mes_ini_comp').val(actual_date.getMonth());
        $('#mes_fin_comp').val(actual_date.getMonth());
        $('#anio_actual').val(actual_date.getFullYear());
        $('#anio_comp').val(actual_date.getFullYear());
        if(actual_month==1){
            $('#mes_ini_comp').val(12);
            $('#mes_fin_comp').val(12);
            $('#anio_comp').val(actual_date.getYear()-1);
        }
        bar_chart =   Morris.Bar({
            element: 'comparative_barchart',
            data: [],
            xkey: 'date',
            ykeys: ['actual', 'past'],
            labels: [$('#anio_actual').val(), $('#anio_actual').val()-1],
            barColors: ["#FF0000", "#3104B4", "#1AB244", "#B29215"],
        });

        $.ajax({
            url: $('#url_comparative_chart').val(),
            dataType: "json",
            data: {
                mes_ini_actual: $('#mes_ini_actual').val(),
                mes_fin_actual: $('#mes_fin_actual').val(),
                mes_ini_comp: $('#mes_ini_comp').val(),
                mes_fin_comp: $('#mes_fin_comp').val(),
                anio_actual: $('#anio_actual').val(),
                anio_comp: $('#anio_comp').val()
            },
            success: function (response) {
                console.log(response);
                if (response.result) {
                    $('#consultation_amount').text(response.consultations);
                    $('#consultation_amount_total').text(response.consultations_comp);
                    $('#patients_amount').text(response.patients);
                    $('#laboratory_orders_amount').text(response.laboratory_orders);
                    $('#surgeries_amount').text(response.surgeries);
                    $('#invoices_amount_cancel').text(response.invoices_cancel);
                    $('#invoices_amount_stamp').text(response.invoices_stamp);
                    $('#balance_total').text(response.total_amount.toFixed(2));
                    $('#ingress').text(response.entry.toFixed(2));
                    $('#egress').text(response.egress.toFixed(2));
                    $('#patients_amount_total').text(response.patients_comp);
                    $('#laboratory_orders_amount_total').text(response.laboratory_order_comp);
                    $('#surgeries_amount_total').text(response.surgeries_comp);
                    $('#invoices_amount_cancel_total').text(response.invoices_cancel_comp);
                    $('#invoices_amount_stamp_total').text(response.invoice_stamp_comp);
                    $('#balance_total_total').text(response.total_amount_comp.toFixed(2));
                    $('#ingress_total').text(response.entry_comp.toFixed(2));
                    $('#egress_total').text(response.egress_comp.toFixed(2));

                    sparklinePie('stats-pie_co', [response.consultations_comp, response.consultations], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_p', [response.patients_comp, response.patients], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_l', [response.laboratory_orders_comp, response.laboratory_orders], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_c', [response.surgeries_comp, response.surgeries], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_f', [response.invoice_stamp_comp, response.invoices_stamp], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_fc', [response.invoices_cancel_comp, response.invoices_cancel], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_b', [response.total_amount_comp, response.total_amount], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_i', [response.entry_comp, response.entry], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                    sparklinePie('stats-pie_e', [response.egress_comp, response.egress], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);

                    bar_chart.setData(response.data_chart);
                    $('#barchart_title').html("Comparativa de ingresos")

                } else {
                    showAlertTemplate("Ocurrio un problema al cargar grafica de generos.");
                }
            },
            error: function (response) {
            }
        });
    }
});
function reload_barchart_data(){
    switch  ($('#chart_type').val()) {
        case "1": ingress_barchart();
            break;
        case "2": egress_barchart();
            break;
        case "3": consults_bartchart();
            break;
        default:console.log("error");
    }
}
function ingress_barchart(){
    $.ajax({
        url: $('#url_comparative_chart_ingress').val(),
        dataType: "json",
        data: {
            mes_ini_actual: $('#mes_ini_actual').val(),
            mes_fin_actual: $('#mes_fin_actual').val(),
            mes_ini_comp: $('#mes_ini_comp').val(),
            mes_fin_comp: $('#mes_fin_comp').val(),
            anio_actual: $('#anio_actual').val(),
            anio_comp: $('#anio_comp').val()
        },
        success: function (response) {
            console.log(response);
            if (response.result) {

                bar_chart.options.labels = [$('#anio_actual').val(), $('#anio_actual').val()-1];
                bar_chart.setData(response.data_chart);
                $('#barchart_title').html("Comparativa de ingresos")

            } else {
                showAlertTemplate("Ocurrio un problema al cargar grafica de generos.");
            }
        },
        error: function (response) {
        }
    });
}
function consults_bartchart(){
    $.ajax({
        url: $('#url_comparative_chart_consults').val(),
        dataType: "json",
        data: {
            mes_ini_actual: $('#mes_ini_actual').val(),
            mes_fin_actual: $('#mes_fin_actual').val(),
            mes_ini_comp: $('#mes_ini_comp').val(),
            mes_fin_comp: $('#mes_fin_comp').val(),
            anio_actual: $('#anio_actual').val(),
            anio_comp: $('#anio_comp').val()
        },
        success: function (response) {
            console.log(response);
            if (response.result) {

                bar_chart.options.labels = [$('#anio_actual').val(), $('#anio_actual').val()-1];
                bar_chart.setData(response.data_chart);
                $('#barchart_title').html("Comparativa de consultas")

            } else {
                showAlertTemplate("Ocurrio un problema al cargar grafica de generos.");
            }
        },
        error: function (response) {
        }
    });
}
function egress_barchart(){
    console.log("llego aqui");
    $.ajax({
        url: $('#url_comparative_chart_egress').val(),
        dataType: "json",
        data: {
            mes_ini_actual: $('#mes_ini_actual').val(),
            mes_fin_actual: $('#mes_fin_actual').val(),
            mes_ini_comp: $('#mes_ini_comp').val(),
            mes_fin_comp: $('#mes_fin_comp').val(),
            anio_actual: $('#anio_actual').val(),
            anio_comp: $('#anio_comp').val()
        },
        success: function (response) {
            console.log(response);
            if (response.result) {

                bar_chart.options.labels = [$('#anio_actual').val(), $('#anio_actual').val()-1];
                bar_chart.setData(response.data_chart);
                $('#barchart_title').html("Comparativa de egresos")

            } else {
                showAlertTemplate("Ocurrio un problema al cargar grafica de generos.");
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}
function sparklineBar(id, values, height, barWidth, barColor, barSpacing) {
    $('.'+id).sparkline(values, {
        type: 'bar',
        height: height,
        barWidth: barWidth,
        barColor: barColor,
        barSpacing: barSpacing
    })
}
function easyPieChart(id, trackColor, scaleColor, barColor, lineWidth, lineCap, size) {
    $('.'+id).easyPieChart({
        trackColor: trackColor,
        scaleColor: scaleColor,
        barColor: barColor,
        lineWidth: lineWidth,
        lineCap: lineCap,
        size: size
    });
}

function sparklineLine(id, values, width, height, lineColor, fillColor, lineWidth, maxSpotColor, minSpotColor, spotColor, spotRadius, hSpotColor, hLineColor) {
    $('.'+id).sparkline(values, {
        type: 'line',
        width: width,
        height: height,
        lineColor: lineColor,
        fillColor: fillColor,
        lineWidth: lineWidth,
        maxSpotColor: maxSpotColor,
        minSpotColor: minSpotColor,
        spotColor: spotColor,
        spotRadius: spotRadius,
        highlightSpotColor: hSpotColor,
        highlightLineColor: hLineColor
    });
}

function sparklinePie(id, values, width, height, sliceColors) {
    $('.'+id).sparkline(values, {
        type: 'pie',
        width: width,
        height: height,
        sliceColors: sliceColors,
        offset: 0,
        borderWidth: 0
    });
}
function show_consults_chart(){


}
function reload_statistics(){
    var fecha_inicio = $('#start_date').val();
    var fecha_fin = $('#end_date').val();
    var from = fecha_inicio.split("/");
    var until = fecha_fin.split("/");
    var f = new Date(from[2], from[1] - 1, from[0]);
    var fecha_cadena = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+ f.getDate();
    f = new Date(until[2], until[1] - 1, until[0]);
    var fecha_cadena_fin = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+ (f.getDate()+1);
    $.ajax({
        url: $('#url_consultation_chart').val(),
        dataType: "json",
        data: {ini_date:fecha_cadena,end_date:fecha_cadena_fin},
        success: function (response) {
            chart_consult.setData(response);
        },
        error: function(response){
            console.log("error");
        }
    });
    $.ajax({
        url: $('#url_transacs_chart').val(),
        dataType: "json",
        data: {ini_date:fecha_cadena,end_date:fecha_cadena_fin},
        success: function (response) {
            chart_transac.setData(response);
        },
        error: function(response){
            console.log("error");
        }
    });

    $.ajax({
        url: $('#url_get_genres_chart').val(),
        dataType: "json",
        data: {ini_date:fecha_cadena,end_date:fecha_cadena_fin},
        success: function (response) {
            if (response.result) {
                $('#consultation_amount').text(response.consultations);
                $('#patients_amount').text(response.patients);
                $('#laboratory_orders_amount').text(response.laboratory_orders);
                $('#surgeries_amount').text(response.surgeries);
                $('#invoices_amount_cancel').text(response.invoices_cancel);
                $('#invoices_amount_stamp').text(response.invoices_stamp);
                $('#balance_total').text(response.total_amount.toFixed(2));
                $('#ingress').text(response.entry.toFixed(2));
                $('#egress').text(response.egress.toFixed(2));
                sparklinePie('stats-pie_co', [response.consultations_total, response.consultations], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_p', [response.patients_total, response.patients], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_l', [response.laboratory_orders_total, response.laboratory_orders], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_c', [response.surgeries_total, response.surgeries], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_f', [response.invoices_stamp_total, response.invoices_stamp], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_fc', [response.invoices_cancel_total, response.invoices_cancel], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_b', [response.total_amount_total, response.total_amount], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_i', [response.entry_total, response.entry], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_e', [response.egress_total, response.egress], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);

                if(response.is_ped){
                    chart_age.setData([
                        {label: "Recien nacidos", value: response.newborn},
                        {label: "Lactantes menores", value: response.oneyears},
                        {label: "Lactantes mayores", value: response.lactantings},
                        {label: "Prescolares", value: response.preschool},
                        {label: "Escolares", value: response.school},
                        {label: "Adolescentes", value: response.teenagers},
                        {label: "Adultos", value: response.adults}
                    ]);
                }else{
                    chart_age.setData([
                        {label: "Niños", value: response.childrens},
                        {label: "Adolescentes", value: response.teenagers},
                        {label: "Adultos", value: response.adults},
                        {label: "Adultos mayores", value: response.oldpeople}
                    ]);
                }

                chart_genre.setData([
                    {label: "Hombres", value: response.mens},
                    {label: "Mujeres", value: response.women}
                ]);
                chart_money.setData([
                    {label: "Ingresos", value: response.entry},
                    {label: "Egresos", value: response.egress}
                ]);
            } else {
                showAlertTemplate("Ocurrio un problema al cargar grafica de generos.");
            }
        },
        error: function(response){
        }
    });
    $.ajax({
        url: $('#url_get_cities_chart').val(),
        dataType: "json",
        data: {ini_date:fecha_cadena,end_date:fecha_cadena_fin},
        success: function (response) {
            if(response.length != 0) {
                cities = '[';
                $.each(response, function (idx, info) {
                    cities += '{"label":"' + info.name + '","value":' + info.value + '},';
                });
                cities = cities.substring(0, cities.length - 1);
                cities += "]";
                chart_cities.setData(JSON.parse(cities));
            }
            else{
                chart_cities.setData([{label: "Ciudades", value: 0}]);
            }

        },
        error: function(response){
            console.log("error");
        }
    });
}
function reload_comparative(){
    $.ajax({
        url: $('#url_comparative_chart').val(),
        dataType: "json",
        data: {
            mes_ini_actual: $('#mes_ini_actual').val(),
            mes_fin_actual: $('#mes_fin_actual').val(),
            mes_ini_comp: $('#mes_ini_comp').val(),
            mes_fin_comp: $('#mes_fin_comp').val(),
            anio_actual: $('#anio_actual').val(),
            anio_comp: $('#anio_comp').val()
        },
        success: function (response) {
            console.log(response);
            if (response.result) {
                $('#consultation_amount').text(response.consultations);
                $('#consultation_amount_total').text(response.consultations_comp);
                $('#patients_amount').text(response.patients);
                $('#laboratory_orders_amount').text(response.laboratory_orders);
                $('#surgeries_amount').text(response.surgeries);
                $('#invoices_amount_cancel').text(response.invoices_cancel);
                $('#invoices_amount_stamp').text(response.invoices_stamp);
                $('#balance_total').text(response.total_amount.toFixed(2));
                $('#ingress').text(response.entry.toFixed(2));
                $('#egress').text(response.egress.toFixed(2));
                $('#patients_amount_total').text(response.patients_comp);
                $('#laboratory_orders_amount_total').text(response.laboratory_order_comp);
                $('#surgeries_amount_total').text(response.surgeries_comp);
                $('#invoices_amount_cancel_total').text(response.invoices_cancel_comp);
                $('#invoices_amount_stamp_total').text(response.invoice_stamp_comp);
                $('#balance_total_total').text(response.total_amount_comp.toFixed(2));
                $('#ingress_total').text(response.entry_comp.toFixed(2));
                $('#egress_total').text(response.egress_comp.toFixed(2));

                sparklinePie('stats-pie_co', [response.consultations_comp, response.consultations], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_p', [response.patients_comp, response.patients], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_l', [response.laboratory_orders_comp, response.laboratory_orders], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_c', [response.surgeries_comp, response.surgeries], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_f', [response.invoice_stamp_comp, response.invoices_stamp], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_fc', [response.invoices_cancel_comp, response.invoices_cancel], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_b', [response.total_amount_comp, response.total_amount], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_i', [response.entry_comp, response.entry], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);
                sparklinePie('stats-pie_e', [response.egress_comp, response.egress], 45, 45, ['#fff', 'rgba(255,255,255,0.7)', 'rgba(255,255,255,0.4)', 'rgba(255,255,255,0.2)']);


            } else {
                showAlertTemplate("Ocurrio un problema al cargar grafica de generos.");
            }
        },
        error: function (response) {
        }
    });
}
