jQuery(document).ready(function() {

    $('form#new_assistant').find("input[type=text], textarea").val("");
    $("#new_assistant").validate();
    var grid = $("#data-table-assistant").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-assistant').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                if (!row.shared) {
                    return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-edit\" title=\"Editar\"></i></a>" +
                        "<a href='#' onclick='show_assistant(" + row.id + ")' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>" +
                        "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
                }
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var last_name = $tr_parent.children('td').eq(2).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar al asistente "+name+" "+last_name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Eliminar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El asistente ha sido eliminado exitosamente.", "success");
                        $('#data-table-assistant').bootgrid('reload');
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

    $('#change_pass').change(function(){
        if($(this).is(':checked')){
            $('#modificate_pass').show();
        }else{
            $('#modificate_pass').hide();
        }
    });
    var today = new moment().add(-1,'days');
    $('.date-picker').data("DateTimePicker").maxDate(today);
});

$(function(){
    $("#change_permissions").change(function(){
        if($(this).is(":checked")){
            $(".assistant_permission").prop("disabled",false);
        }else{
            $(".assistant_permission").prop("disabled",true);
        }
    });
    $("#change_permissions_expedient").change(function(){
        if($(this).is(":checked")){
            $(".permissions_expedient").prop("disabled",false);
        }else{
            $(".permissions_expedient").prop("disabled",true);
        }
    });

});

function show_assistant(assistant_id){
    url = $('#url_show_assistant').val();
    $.ajax({
        url: url,
        dataType: "json",
        data: {assistant_id:assistant_id},
        success: function (data) {
            $.each(data.assist, function (idx, info) {
                $("#a_name").text(info.name);
                $("#a_last_name").text(info.last_name);
                $("#a_telephone").text(info.telephone);
                $("#a_cellphone").text(info.cellphone);
                $("#a_email").text(info.email);
                $("#a_birth_day").text(info.birth_day);
                $("#a_address").text(info.address);
                $("#a_city").text(info.city);
            });
            var tabla_per = "<table id='tablaper' class='table table-striped'><thead><tr>"+
                "<th>Ver</th>"+
                "<th>Agregar</th>"+
                "<th>Editar</th>"+
                "<th>Eliminar</th>"+
                "<th>Corte de caja</th>"+
                "</tr></thead><tbody><tr>";
            Ver = [];
            Agregar = [];
            Editar = [];
            Eliminar = [];
            corte_caja = [];
            $.each(data.permis, function (idx, info) {
                switch(info.code) {
                    case 1:
                        Ver.push(info.office);
                        break;
                    case 2:
                        Agregar.push(info.office);
                        break;
                    case 3:
                        Editar.push(info.office);
                        break;
                    case 4:
                        Eliminar.push(info.office);
                        break;
                    case 5:
                        corte_caja.push(info.office);
                        break;
                    default:console.log(info.office + " " + info.permision);
                }
            });
            cadena = "";
            for(cont=0; cont<Ver.length; cont++){
                cadena += "- "+Ver[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            for(cont=0; cont<Agregar.length; cont++){
                cadena += "- "+Agregar[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            for(cont=0; cont<Editar.length; cont++){
                cadena += "- "+Editar[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            for(cont=0; cont<Eliminar.length; cont++){
                cadena += "- "+Eliminar[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            for(cont=0; cont<corte_caja.length; cont++){
                cadena += "- "+corte_caja[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            tabla_per += "</tr></tbody></table>";
            $('#table_permissions').html(tabla_per);

            var html_pe = "";

            if(data.expedient_permissions.length > 0){
                $.each(data.expedient_permissions,function(index,value){
                    html_pe += "<div class='col-md-6'>";
                    html_pe += "<strong>";
                    html_pe += "*"+value.name;
                    html_pe += "</strong>";
                    html_pe += "</div>";
                });
            }else{
                html_pe += "<div class='col-md-6'><strong>No tiene permisos de expediente</strong></div>";
            }

            $('#table_permissions_expedient').empty();
            $('#table_permissions_expedient').html(html_pe);

        },
        error:function(data){
        }
    });
    $('#show_assistant_modal').modal('show');
}

function checkuser_exist(){
    var  editValue = $('#edit_assistant').val();
    var  new_user_name = $('#assistant_user_attributes_user_name').val();
    var assist_email = $('#assistant_user_attributes_email').val();
    var valid = true;
    $.ajax({
        url: $('#url_check_user').val(),
        dataType: "json",
        async:false,
        data: {user_name:new_user_name,edit_value:editValue,email:assist_email},
        success: function (data) {
            if(data.result){
                swal("Error!", "El nombre de usuario o correo electronico ya existe.", "error");
                valid =  false;
            }

        }
    });

    if(valid && $('.form_assistant').valid()){
        if(!$(".assistant_permission").is(':checked')){
            swal("Error!", "Agregue mínimo un permiso para la agenda.", "error");
            valid = false;
        }
    }

    return valid;

}