function closeNewConsultationFormEdit(id){
    $("#edit_full_consult_"+id).slideUp(500);
}
function  saveEditedConsultation(id){
    //bloquear el boton de guardar, mostrar loading
    setDisableBtn('btn-edit-consultation_'+id,true);
    $('#loading_btn_edit_'+id).show();
    saveMedicalConsultationEdited(id);
   // closeNewConsultationFormEdit(id);
}

function loadingFormEditConsultation(id){
    //mostrar loading en el modal
    showLoading("form-edit-medical-consultation_"+id);
    //bloquear boton de guardar
    setDisableBtn('btn-edit-consultation_'+id, true);
    var url = $('#url_form_edit_medical_consultation').val();
    $.ajax({
        type: "get",
        url: url,
        data:{medical_consultation_id:id},
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("form-edit-medical-consultation_"+id,response);
            initElementForEdit(id);

            $(".search-disease").ts_autocomplete({
                url:AUTOCOMPLETE_DISEASES_URL,
                in_modal:true,
                items:10,
                required:true,
                model:"una enfermedad"
            });
            $(".search-medicament").ts_autocomplete({
                url:AUTOCOMPLETE_MEDICAMENTS_URL,
                in_modal:true,
                items:10,
                required:true,
                model:"un medicamento"
            });

            $("#medical_consultation_vital_sign_attributes_oxygen_saturation").change(function(){
                sat_ox = $("#medical_consultation_vital_sign_attributes_oxygen_saturation").val();
                if(sat_ox < 0 || sat_ox > 100){
                    swal("Error","La saturación de oxigeno es un valor entre 0 y 100","error");
                    $("#medical_consultation_vital_sign_attributes_oxygen_saturation").val("");
                }
            });
            initHtmlEditor(".general_notes_consult_edit");
            $("#general_notes_consult_edit_"+id).summernote().code($("#general_notes_value_"+id).val());
        },
        error: function (error) {
            swal("Error","Error al cargar el formulario de consulta","error");
        }
    });

    //ajax para el formulario, ocultar el loading del modal
}
function initTableDiagnostic_edit(id){
    var grid = $("#data-table-diagnostic_edit_"+id).bootgrid({
        navigation: 2,
        labels: {
            noResults: "Sin resultados"
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<button type='button' onclick='removeDiseaseEditConsult("+row.id+","+id+")' class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    });
    var diseases_edit = JSON.parse($('#diseases_edit_'+id).val());
    $.each(diseases_edit,function(idx,data){
        var currentRows = $("#data-table-diagnostic_edit_"+id).bootgrid("getCurrentRows");
        var text = data.name;
        $("#data-table-diagnostic_edit_"+id).bootgrid("append",[{"name":text,"id":data.id,"idx": currentRows.length + 1}]);
    });
    $("#data-table-diagnostic_edit_"+id).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var rows = [];
            rows[0] = ($(this).data("row-id")).toString();

            $("#data-table-diagnostic_edit_"+id).bootgrid('remove',rows);
            $("#data-table-diagnostic_edit_"+id).bootgrid("reload");


            setTimeout(function(){
                var currentRows = $("#data-table-diagnostic_edit_"+id).bootgrid("getCurrentRows");
                $.each(currentRows,function(idx,val){
                    val.idx = (idx+1);
                });
                $("#data-table-diagnostic_edit_"+id).bootgrid("reload");
            },200);
        });
    });

}

function initElementForEdit(idc){
    $('.selectpicker').selectpicker();
    var dateNow = new Date();
    var month = dateNow.getMonth()+1;
    var day = dateNow.getDate();

    var hours = dateNow.getHours();
    var minutes = dateNow.getMinutes();

    var output = (day<10 ? '0' : '') + day +'/'+ (month<10 ? '0' : '') + month  + '/' +
        dateNow.getFullYear() + ' ' + hours+':'+minutes;

    $('#medical_consultation_date_consultation').val(output);

    $('#medical_consultation_date_consultation').datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY hh:mm a'
    });

    $("#medical_consultation_date_consultation").on("dp.change", function (e) {
        var date = $('#medical_consultation_date_consultation').val();
        var dateArray = date.split(' ');
        if (DATE_CONSULTATION_BEFORED != dateArray[0]){
            DATE_CONSULTATION_BEFORED = dateArray[0];
            $('#medical_consultation_date_consultation').data("DateTimePicker").hide();
        }
    });


    initTableDiagnostic_edit(idc);
    //event agregar btn enfermedades
    $("#add-disease_"+idc).unbind();
    $('#add-disease_'+idc).click(function(e)
    {
        var id = $('#consultation_disease_'+idc+'_ts_autocomplete').val();
        if(id != "" && id != "undefined"){
            var currentRows = $("#data-table-diagnostic_edit_"+idc).bootgrid("getCurrentRows");
            var text = $('#consultation_disease_'+idc).val();
            $("#data-table-diagnostic_edit_"+idc).bootgrid("append",[{"name":text,"id":id,"idx": currentRows.length + 1}]);
            $('#consultation_disease_'+idc).val("");
            $('#consultation_disease_'+idc+'_ts_autocomplete').val("");
        }else{
            swal("Error!", "Seleccione una enfermedad de la lista.", "error");

        }
    });

     //cargar los json de los consultorios
    COSTS = jQuery.parseJSON($('#consultations_costs').val());
    //agregar el costo de la consulta seleccionada
    $('#medical_consultation_price').val(getPriceOfficeSelected());
    //evento cuando cambia de consultorio
    $( "#medical_consultation_office" ).change(function() {
        $('#medical_consultation_price').val(getPriceOfficeSelected());
    });
    // evento que cambia tipo de consulta
    $( "#medical_consultation_consultation_type_id" ).change(function() {
        $('#medical_consultation_price').val(getPriceOfficeSelected());
    });
    setDisableBtn('btn-save-consultation',false);

    $("#btn-calculation-imc").unbind();
    $('#loading_table_imc').hide();
    $('#error_weight_height').hide();
    $('#error_generate_table_imc').hide();
    //set event click to btn calculation IMC
    $('#btn-calculation-imc').click(function(e){
        $('#loading_table_imc').show();
        $('#error_weight_height').hide();
        $('#error_generate_table_imc').hide();
        var weight = $('#medical_consultation_vital_sign_attributes_weight').val();
        var height = $('#medical_consultation_vital_sign_attributes_height').val();
        if(weight != '' && weight.length > 0 && height != '' && height.length > 0){
            loadingTableIMC();
        }else{
            $('#loading_table_imc').hide();
            $('#error_generate_table_imc').hide();
            $('#error_weight_height').show();
        }
    });
}

function removeDiseaseEditConsult(id,tableidx){
    var rows = Array();
    rows[0] = id;
    $("#data-table-diagnostic_edit_"+tableidx).bootgrid('remove', rows);

}

function saveMedicalConsultationEdited(id){
    var formConsultation = $('#edit_medical_consultation_'+id);
    var disabled = formConsultation.find(':input:disabled').removeAttr('disabled');
    var objectForm = formConsultation.serializeArray();
    disabled.attr('disabled','disabled');

    var json = JSON.stringify(objectForm);
    var jsonReal = JSON.parse(json);
    var indexToDelete = new Array();
    var i;
    for(i=0;i<jsonReal.length; i++){
        var aux = jsonReal[i];
        if(aux.name == "offices_consultation" || aux.name == "medical_consultation[prescription_attributes][medical_indications][medicament]" ||
            aux.name == "medical_consultation[prescription_attributes][medical_indications][dose]" || aux.name == "medical_consultation[diseases]"){
            indexToDelete.push(i);
        }
    }
    for(i=0;i<indexToDelete.length; i++){
        delete jsonReal[indexToDelete[i]];
    }
    jsonReal.push(getJsonDiagnosticEdit(id));
    var url = $('#url_form_update_consultation').val();
    var consult_obj = {"name": 'medical_consultation_id', "value": id};
    jsonReal.push(consult_obj);

    jsonReal.push({
        'name':'medical_consultation[notes]',
        'value':$("#general_notes_consult_edit_"+id).summernote().code()
    });
    $.ajax({
        type: "get",
        url: url,
        dataType:'json',
        data: jsonReal,
        success: function (response) {
            if(typeof response == "string"){
                response = JSON.parse(response);
            }

            if(response.ok == "true"){
                closeNewConsultationFormEdit(id);
                swal("Exito", "Consulta actualizada con exito.", "success");
                searchLastConsultation();
            }else{
                setErrorsModalEditConsultation(response.errors,id);
            }
        },
        error: function (error) {
            swal("Error!", "Error al guardar la consulta, consulte al administrador.", "error");
        }
    });

}

function setErrorsModalEditConsultation(errors,id){

    //var keys = Object.keys(errors);
    var html = "<div id='error_explanation'> <h3>"+errors.length+" errores encontrados:</h3>";
    var i;
    html += "<ul>";
    for(i=0; i<errors.length; i++){
        // var error =errors[keys[i]];
        var error = errors[i];
        // var attribute = keys[i];
        // var attributeArray = attribute.split('.');
        // html += "<li>"+attributeArray[attributeArray.length - 1]+" - "+error+"</li>";
        html += "<li>"+error+"</li>";
    }
    html += "</ul>";
    html += "</div>";
    var id_list = "#list_error_medical_consultation_edit_"+id;
    $(id_list).empty();
    $(id_list).append(html);
    var btn = "btn-edit-consultation_"+id;
    setDisableBtn(btn,false);
    var loading = "loading_btn_edit_"+id;
    hideLoadingAndSetContainer(loading,"");
}

function getJsonDiagnosticEdit(id){
    var diseases = $("#data-table-diagnostic_edit_"+id).bootgrid("getCurrentRows");
    var result = new Array();
    var i;
    for(i=0;i<diseases.length;i++){
        var aux = diseases[i];
        result.push({'name': aux.name,'id':aux.id});
    }
    var jsonDiseases = {"name": 'medical_consultation[diseases]', "value": JSON.stringify(result)};
    return jsonDiseases;
}