//= require croppie/croppie.js
var $uploadCrop;
jQuery(document).ready(function() {
    url_fields = $("#url_get_fields_toadd").val();

    if($('#custom_field_type').val() != ""){
        var fields_option =  "";
        $.ajax({
            url: url_fields,
            dataType: "json",
            data: {field_id:$('#custom_field_type').val()},
            success: function (data) {
                $.each(data.fields, function (idx, info) {
                    fields_option += "<option value = "+info.id+">"+info.name+"</option>";
                });
                $('#new_field').html(fields_option);
                $('#new_field').selectpicker('refresh');
                $('#new_field_report').html(fields_option);
                $('#new_field_report').selectpicker('refresh');

            },
            error:function(data){
                console.log("failee");
            }
        });
    }
    $('#custom_field_type_report').change(function(){
        setTimeout(function(){
            if($('#custom_field_type_report').val() == 1 ||  $('#custom_field_type_report').val() == 3){
                $("#new_field_div_report").show();
                $("#label_div_report").hide();
                $("#image_div_report").hide();
                var fields_option =  "";
                $.ajax({
                    url: url_fields,
                    dataType: "json",
                    data: {field_id:$('#custom_field_type_report').val()},
                    success: function (data) {
                        $.each(data.fields, function (idx, info) {
                            fields_option += "<option value = "+info.id+">"+info.name+"</option>";
                        });
                        $('#new_field_report').html(fields_option);
                        $('#new_field_report').selectpicker('refresh');
                    },
                    error:function(data){
                        console.log("fail");
                    }
                });
            }else if ($('#custom_field_type_report').val() == 2){
                $("#new_field_div_report").hide();
                $("#label_div_report").show();
                $("#image_div_report").hide();
            }else{
                $("#new_field_div_report").hide();
                $("#label_div_report").hide();
                $("#image_div_report").show();
            }
        },500);
    });

    $('#custom_field_type').change(function(){
        setTimeout(function(){
            if($('#custom_field_type').val() == 1 ||  $('#custom_field_type').val() == 3){
                $("#new_field_div").show();
                $("#label_div").hide();
                $("#image_div").hide();
                var fields_option =  "";
                $.ajax({
                    url: url_fields,
                    dataType: "json",
                    data: {field_id:$('#custom_field_type').val()},
                    success: function (data) {
                        $.each(data.fields, function (idx, info) {
                            fields_option += "<option value = "+info.id+">"+info.name+"</option>";
                        });
                        $('#new_field').html(fields_option);
                        $('#new_field').selectpicker('refresh');
                    },
                    error:function(data){
                        console.log("fail");
                    }
                });
            }else if ($('#custom_field_type').val() == 2){
                $("#new_field_div").hide();
                $("#label_div").show();
                $("#image_div").hide();
            }else{
                $("#new_field_div").hide();
                $("#label_div").hide();
                $("#image_div").show();
            }
        },500);
    });
    // mostrar tabla de configuracion para la plantilla personalizada
    $("#active_custom_prescription").change(function(){
        show_table_custom_prescription();
    });
    //show_table_custom_prescription();

    // mostrar tabla de configuracion para la plantilla personalizada del reporte de gabinete
    $("#active_custom_report").change(function(){
        show_table_custom_report();
    });
    show_table_custom_report();

    $(".edit_doctor").validate();
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('.upload-demo').addClass('ready');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 250,
            height: 200,
            type: 'square'
        },
        boundary: {
            width: 300,
            height: 275
        }
    });

    $('#upload').on('change', function () { readFile(this); });

    $('#change_pass').change(function(){
        if($(this).is(':checked')){
            $('#change_fiscal_datas').show();
            $('#admin_doctor_fiscal_information_certificate_password').val("");
            $('#actual_fiscal_datas').hide();
            $('#full_checkbox').hide();
        }else{
            $('#change_fiscal_datas').hide();
            $('#actual_fiscal_datas').show();
        }
    });
});

function show_table_custom_prescription(){
    if ($("#active_custom_prescription").is(":checked")) {
        // mostrar table
        $('#table_custom_template').show(1000);
    }else{
        // ocultar tabla
        $('#table_custom_template').hide(1000);
    }
}
function  show_table_custom_report(){
    if ($("#active_custom_report").is(":checked")) {
        // mostrar table
        $('#table_custom_template_report').show(1000);
    }else{
        // ocultar tabla
        $('#table_custom_template_report').hide(1000);
    }
}
function open_crop_image_modal(){
    $("#crop_image_modal").modal("show");
}
function crop64(){
    $uploadCrop.croppie('result',{
        type: 'canvas',
        size: 'viewport'
    }).then(function (result) {
        $('#result-image').attr('src', result);
        $('#crop_image_64').val(result);
    });
    $('#result-image').show();
    $('#upload_picture').hide();
    $("#crop_image_modal").modal("hide");
}
function open_camera(){

    streaming = false,
        video        = document.querySelector('#video'),
        canvas       = document.querySelector('#canvas'),
        photo        = document.querySelector('#photo'),
        width = 320,
        height = 0;

    navigator.getMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

    navigator.getMedia(
        {
            video: true,
            audio: false
        },
        function(stream) {
            if (navigator.mozGetUserMedia) {
                video.mozSrcObject = stream;
            } else {
                var vendorURL = window.URL || window.webkitURL;
                video.src = vendorURL.createObjectURL(stream);
            }
            video.play();

        },
        function(err) {
            console.log("An error occured! " + err);
        }
    );

    video.addEventListener('canplay', function(ev){
        if (!streaming) {
            height = video.videoHeight / (video.videoWidth/width);
            video.setAttribute('width', width);
            video.setAttribute('height', height);
            canvas.setAttribute('width', width);
            canvas.setAttribute('height', height);
            streaming = true;
        }
    }, false);

    $("#take_picture").modal("show");
}
function close_camera(){
    $("#take_picture").modal("hide");
}
function take_snapshot() {
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(video, 0, 0, width, height);
    var data = canvas.toDataURL('image/png');
    document.getElementById('results').innerHTML =
        '<div class="row"></div>'+
        '<img src="'+data+'" width="250" height="200"/>' +
        '<button type="button" class="btn btn-link" onclick="remove_picture()">Cancelar</button>';
    $('#picture64').val(data);
    $("#take_picture").modal("hide");
    $('#upload_picture').hide();
    $('#button_crop_size').hide();
    $('#results').show();
    $('#crop_image_64').val("");
    $('#result-image').hide();

    navigator.getMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

    navigator.getMedia(
        {
            video: true,
            audio: false
        },
        function(stream) {
            if (navigator.mozGetUserMedia) {
                video.mozSrcObject = stream;
                stream.getVideoTracks()[0].stop()
            } else {
                var vendorURL = window.URL || window.webkitURL;
                video.src = vendorURL.createObjectURL(stream);
                stream.getVideoTracks()[0].stop()
            }
            video.pause();

        },
        function(err) {
            console.log("An error occured! " + err);
        }
    );

}
function remove_picture(){
    $('#picture64').val("");
    $('#button_crop_size').show();
    $('#upload_picture').show();
    $('#results').hide();

}
function enable_fields(){
    $('#admin_doctor_fiscal_information_rfc').prop('disabled', false);
    $('#admin_doctor_fiscal_information_bussiness_name').prop('disabled', false);
    $('#admin_doctor_fiscal_information_fiscal_address').prop('disabled', false);
    $('#admin_doctor_fiscal_information_int_number').prop('disabled', false);
    $('#admin_doctor_fiscal_information_ext_number').prop('disabled', false);
    $('#admin_doctor_fiscal_information_suburb').prop('disabled', false);
    $('#admin_doctor_fiscal_information_locality').prop('disabled', false);
    $('#admin_doctor_fiscal_information_zip').prop('disabled', false);
    $('#admin_doctor_fiscal_information_city_id').prop('disabled', false);
    $('#admin_doctor_fiscal_information_serie').prop('disabled', false);
    $('#admin_doctor_fiscal_information_folio').prop('disabled', false);
    $('#admin_doctor_fiscal_information_tax_regime').prop('disabled', false);
    $('#admin_doctor_fiscal_information_iva').prop('disabled', false);

}


/** consult types  ***/
$(function(){
    $('#add_consultation_type_modal').on('hidden.bs.modal', function () {
        $("#id_consultation_type").val("");
        $("#name_consultation_type").val("");
        $("#add_consultation_title").html("Agregar tipo de consulta");
    });

    $("#charge_reports").change(function () {
        var charge = $(this).is(":checked") ? 1 : 0 ;
        $.ajax({
            url:"/doctor/update_charge_report",
            data:{charge:charge},
            dataType:"json",
            success:function(data){

            }
        });
    });
    $("#iva_reports").change(function () {
        var iva = $(this).is(":checked") ? 1 : 0 ;
        $.ajax({
            url:"/doctor/update_iva_report",
            data:{iva:iva},
            dataType:"json",
            success:function(data){

            }
        });
    });
    $("#iva_report_included").change(function () {
        var ivaincluded = $(this).is(":checked") ? 1 : 0 ;
        $.ajax({
            url:"/doctor/update_iva_report_included",
            data:{ivaincluded:ivaincluded},
            dataType:"json",
            success:function(data){

            }
        });
    });
});
function clearNewCurrency(){
    $("#currency_exchange_value").val("");
    $("#current_exchange_edit_id").val("");
    $("#new_currency_id").val(1);
    $("#new_currency_id").selectpicker('refresh');
    $('#currency_default').prop('checked', false);
}
function editCurrencyExchange(ce_id,value,is_default,currency){
    clearNewCurrency();
    $("#currency_exchange_value").val(value);
    $("#current_exchange_edit_id").val(ce_id);
    if(is_default){
        console.log("lol");
        $('#currency_default').prop('checked', true);
    }
    $("#new_currency_id").val(currency);
    $("#new_currency_id").selectpicker('refresh');
    $("#add_currency_modal").modal('show');
}
function deleteCurrency(cuex_id){
    var url = $("#url_delete_currency_exchange").val();
    var method = "GET";
    var data = {current_exchange_id:cuex_id};

    swal({
            title: "Eliminar tipo de cambio",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url:url,
                method:method,
                data:data,
                dataType:"json",
                success:function(response){
                        location.reload();
                                    }
            });

        });


}
function saveCurrencyExchange(){
    var url = $("#url_new_currency_exchange").val();
    var method = "GET";
    var is_default_currency = 0;

    if($("#currency_default").is(':checked')){
        is_default_currency = 1;
    }
    console.log(is_default_currency);
    if($("#currency_exchange_value").val() == ""){
        swal("Por favor ingrese un valor a la moneda.");
        return;
    }
    var data = {value:$("#currency_exchange_value").val(),
        currency_id:$("#new_currency_id").val(),
        is_default:is_default_currency,
        edit_id:$("#current_exchange_edit_id").val()};

    /*if($("#id_consultation_type").val() != ""){
        method = "PUT";
        url += "/"+$("#id_consultation_type").val();
    }*/
    $("btn_new_currency_exchange").prop( "disabled", true );
    $.ajax({
        url:url,
        method:method,
        data:data,
        dataType:"json",
        success:function(response){
            console.log(response);
            if(response.done){
                location.reload();
            }else{
                swal("Error!", "La moneda ya se encuentra agregada.", "error");
            }

        }
    });
}
function saveConsultationType(){
    var url = $("#consultation_types_path").val();
    var method = "POST";

    if($("#name_consultation_type").val() == ""){
        swal("Por favor ingrese un nombre.");
        return;
    }
    var data = {name:$("#name_consultation_type").val()};

    if($("#id_consultation_type").val() != ""){
        method = "PUT";
        url += "/"+$("#id_consultation_type").val();
    }

    $.ajax({
        url:url,
        method:method,
        data:data,
        dataType:"json",
        success:function(){
            location.reload();
        }
    });
}

function editConsultType(id,name){
    $("#id_consultation_type").val(id);
    $("#name_consultation_type").val(name);
    $("#add_consultation_title").html("Editar tipo de consulta");
    $('#add_consultation_type_modal').modal("show");
}

function saveConsultCosts(){
    var url = $("#update_cost_path").val();
    var data = [];

    var inputs = $(".office_cost");
    $.each(inputs,function(idx,input){
        data.push({
            office:$(input).attr("office"),
            consult_type:$(input).attr("consult_type"),
            cost:$(input).val()
        });
    });

    $.ajax({
        url:url,
        data:{data:data},
        dataType:"json",
        success:function(){
            $("#update_cost_success_message").show();
            setTimeout(function(){
                $("#update_cost_success_message").hide();
            },2000);
        }
    });
}

function validateNointerior(){
    no_int = $('#admin_doctor_fiscal_information_int_number').val().trim();
    if(no_int==""){
        $('#admin_doctor_fiscal_information_int_number').val("-");
    }
    if($('#admin_doctor_fiscal_information_ext_number').val()==""){
        $('#admin_doctor_fiscal_information_ext_number').val("-");
    }
    fiscal_city = $('#admin_doctor_fiscal_information_city_id_ts_autocomplete').val();
    if(fiscal_city=="" || fiscal_city == null || fiscal_city == "undefined"){
        $('#admin_doctor_fiscal_information_city_id_ts_autocomplete').val($('#admin_doctor_city_id_ts_autocomplete').val());
    }
}
function opne_add_new_custom_field(){
    $("#new_component_custom_prescription_modal").modal("show");
}
function opne_add_new_custom_field_report(){
    $("#new_component_custom_report_modal").modal("show");
}

function add_new_custom_field_prescription() {
    if ($("#custom_field_type").val()!=4) {
        $.ajax({
            url: $("#url_add_prescription_field").val(),
            dataType: "json",
            method: "PUT",
            data: {
                type: $("#custom_field_type").val(),
                newfield: $("#new_field").val(),
                label: $("#label_name").val()
            },
            success: function (data) {
                console.log(data);
                if (data.done) {
                    swal("Exito!", "Campo Agregado con exito", "success");
                    $("#new_component_custom_prescription_modal").modal("hide");
                    location.reload();
                } else {
                    swal("Error!", "Ocurrio un error al agregar el campo, por favor intente de nuevo", "error");
                }
            }
        });
    }else{
        if($("#image_pres").val() != ""){
            var data = new FormData();
            var files = $("#image_pres")[0].files;

            data.append('file_0', files[0]);
            var url = $('#url_add_file_prescription').val();
            $.ajax({
                url: url,
                data: data,
                cache: false,
                dataType: "json",
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(data){
                    console.log(data);
                    if (data.done) {
                        //swal("Exito!", "Campo Agregado con exito", "success");
                        $("#new_component_custom_prescription_modal").modal("hide");
                        location.reload();
                    } else {
                        swal("Error!", "Ocurrio un error al agregar el campo, por favor intente de nuevo", "error");
                    }
                }
            });
        }
    }
}
function view_field_image_report(id){
    url = $("#url_view_field_image_report").val()+"?id="+id;
    $("#image_field_img_report").attr("src",url);
    $("#view_image_field_report").modal("show");
}
function view_field_image(id){
    url = $("#url_view_field_image").val()+"?id="+id;
    $("#image_field_img").attr("src",url);
    $("#view_image_field").modal("show");
}
function deleteFieldPrescription(idfield){
    swal({
            title: "Eliminar campo",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },function () {
            $.ajax({
               url:$("#url_delete_field_prescription").val(),
                data:{idfield:idfield},
                dataType:"json",
                success:function(data){
                   if(data.done) {
                       $("#prescription_field_"+idfield).remove();
                       swal("Exito!","Campo eliminado con exito","success");
                   }else{

                       swal("Error!", "Ocurrio un error al eliminar el campo, por favor intente mas tarde.", "error");
                   }
                }
            });

        });
}
function deleteFieldeport(idfield){
    swal({
        title: "Eliminar campo",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Si, eliminar!",
        closeOnConfirm: false
    },function () {
        $.ajax({
            url:$("#url_delete_field_report").val(),
            data:{idfield:idfield},
            dataType:"json",
            success:function(data){
                if(data.done) {
                    $("#report_field_"+idfield).remove();
                    swal("Exito!","Campo eliminado con exito","success");
                }else{

                    swal("Error!", "Ocurrio un error al eliminar el campo, por favor intente mas tarde.", "error");
                }
            }
        });

    });
}

function add_new_field_report(){
    if ($("#custom_field_type_report").val()!=4) {
        $.ajax({
            url: $("#url_add_report_field").val(),
            dataType: "json",
            method: "PUT",
            data: {
                type: $("#custom_field_type_report").val(),
                newfield: $("#new_field_report").val(),
                label: $("#label_name_report").val()
            },
            success: function (data) {
                console.log(data);
                if (data.done) {
                    swal("Exito!", "Campo Agregado con exito", "success");
                    $("#new_component_custom_report_modal").modal("hide");
                    location.reload();
                } else {
                    swal("Error!", "Ocurrio un error al agregar el campo, por favor intente de nuevo", "error");
                }
            }
        });
    }else{
        if($("#image_pres_report").val() != ""){
            var data = new FormData();
            var files = $("#image_pres_report")[0].files;

            data.append('file_0', files[0]);
            var url = $('#url_add_file_report').val();
            $.ajax({
                url: url,
                data: data,
                cache: false,
                dataType: "json",
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(data){
                    console.log(data);
                    if (data.done) {
                        //swal("Exito!", "Campo Agregado con exito", "success");
                        $("#new_component_custom_report_modal").modal("hide");
                        location.reload();
                    } else {
                        swal("Error!", "Ocurrio un error al agregar el campo, por favor intente de nuevo", "error");
                    }
                }
            });
        }
    }
}
function create_custom_prescription(){
    $( "#create_custom_prescr_btn" ).prop( "disabled", true );
    $.ajax({
        url: $("#url_create_custom_prescription").val(),
        cache: false,
        dataType: "json",
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(data){
            if (data.done) {
                location.reload();
            } else {
                swal("Error!", "Ocurrio un error al generar la plantilla personzalida, intente mas tarde por favor", "error");
            }
        }
    });
}
function create_custom_report(){
    $( "#create_custom_report_btn" ).prop( "disabled", true );
    $.ajax({
        url: $("#url_create_custom_report").val(),
        cache: false,
        dataType: "json",
        contentType: false,
        processData: false,
        type: 'GET',
        success: function(data){
            if (data.done) {
                location.reload();
            } else {
                swal("Error!", "Ocurrio un error al generar la plantilla personzalida, intente mas tarde por favor", "error");
            }
        }
    });
}