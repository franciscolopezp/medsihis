$(function(){
    $("#add_package_btn").click(function () {
        $("#add_package_modal").modal("show");
    });

    $("#add_study_btn").click(function () {
        $("#add_study_modal").modal("show");
    });

    $("#save_package_btn").click(function () {
        saveUpdatePackage("POST");
    });

    $("#category_id").change(function () {
        $("#study_id").html("");
        $.ajax({
            url:$("#studies_by_category_url").val(),
            data:{
                category_id:$("#category_id").val()
            },
            dataType:"json",
            success:function(data){
                $.each(data,function(idx,item){
                    $("#study_id").append("<option value='"+item.id+"'>"+item.name+"</option>");
                });
                $("#study_id").selectpicker("refresh");
            }
        });
    });

    $("#save_study_btn").click(function(){
        $.ajax({
            url:$("#add_study_url").val(),
            data:{
                package_id:$("#package_id").val(),
                studies_id:$("#study_id").val()
            },
            dataType:"json",
            success:function(data){
                if(data.done){
                    $("#success_add_study_message").show();
                    $("#success_add_study_message").delay(1500).fadeOut(300);
                }else{
                    swal("No se pudo completar la operación",data.message,"error");
                }
            }
        });
    });

    $("#add_study_modal").on("hidden.bs.modal",function(){
        location.reload();
    });

    $(".delete_study_from_package").click(function(){
        var item = $(this);
        $.ajax({
            url:$("#delete_study_url").val(),
            data:{
                package_id:item.attr("package"),
                study_id:item.attr("study")
            },
            dataType:"json",
            success:function(data){
                item.parent().parent().remove();
            }
        });
    });
});

function saveUpdatePackage(method) {
    $.ajax({
        method:method,
        url:$("#packages_url").val(),
        data:{
            study_package:{
                name:$("#package_name").val(),
                laboratory_id:$("#package_lab_id").val()
            }
        },
        dataType:"json",
        success:function(data){
            if(data.done){
                location.reload();
            }
        }
    });
}