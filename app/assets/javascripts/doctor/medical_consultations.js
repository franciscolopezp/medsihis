jQuery(document).ready(function() {
    var grid = $("#data-table-consultations").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function(){
            var start_date = $('#start_date_search').val();
            var end_date = $("#end_date_search").val();
            var doctor = $("#doctor_search_ts_autocomplete").val();
            var office = $("#office_search").val();
            return $('#data-table-consultations').data('source')+"?doctor="+doctor+"&start_date="+start_date+"&end_date="+end_date+"&office_id="+office;
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {

    });

});

function searchConsults(){
    $("#data-table-consultations").bootgrid("reload");
}

function allConsults(){
    var ids = ["start_date_search","end_date_search","doctor_search","doctor_search_ts_autocomplete","office_search"];
    $.each(ids,function(idx,id){
        $("#"+id).val("");
    });
    $("#office_search").selectpicker("refresh");
    searchConsults();
}

function exportPdf(){
    var start_date = $('#start_date_search').val();
    var end_date = $("#end_date_search").val();
    var doctor = $("#doctor_search_ts_autocomplete").val();
    var office = $("#office_search").val();
    var url =  $("#consultation_report_pdf_url").val()+"?doctor="+doctor+"&start_date="+start_date+"&end_date="+end_date+"&office_id="+office;

    window.open(
        url,
        '_blank'
    );

}