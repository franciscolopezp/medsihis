/**
 * Created by juan on 21/04/16.
 */
var waiting_room_assistant_id = "#waiting_room_a";
var waiting_room_class = {
    min:"col-sm-2",
    res:"col-sm-6",
    max:"col-sm-12"
};
var waiting_room_session_key = "waiting_room_size";
var waiting_room_sizes = {
    min:"min",
    res:"res",
    max:"max"
};

$(document).ready(function(){

    if(is_dashboard()){
        loadActivitiesWaitingRoom();
    }

});

$(function(){
    $("#min_waiting_room_assistant").click(minWaitingRoom);
    $("#res_waiting_room_assistant").click(resWaitingRoom);
    $("#max_waiting_room_assistant").click(maxWaitingRoom);
});


function minWaitingRoom(){
    $(waiting_room_assistant_id).parent().attr("class",waiting_room_class.min);
    $.session.set(waiting_room_session_key,waiting_room_sizes.min);
    $("#min_waiting_room_assistant").hide();
    $("#res_waiting_room_assistant").show();
    $("#max_waiting_room_assistant").show();
}

function resWaitingRoom(){
    $(waiting_room_assistant_id).parent().attr("class",waiting_room_class.res);
    $.session.set(waiting_room_session_key,waiting_room_sizes.res);
    $("#min_waiting_room_assistant").show();
    $("#res_waiting_room_assistant").hide();
    $("#max_waiting_room_assistant").show();
}

function maxWaitingRoom(){
    $(waiting_room_assistant_id).parent().attr("class",waiting_room_class.max);
    $.session.set(waiting_room_session_key,waiting_room_sizes.max);
    $("#min_waiting_room_assistant").show();
    $("#res_waiting_room_assistant").show();
    $("#max_waiting_room_assistant").hide();
}

function loadActivitiesWaitingRoom(){
    var date = new Date();
    var month_int = date.getMonth();
    var year_int = date.getFullYear();
    var date_int = date.getDate();
    var office_id = $("#assistant_office_id").val();

    $.ajax({
        url:"/doctor/dashboard/waiting_room",
        method:"GET",
        dataType:"json",
        data:{
            year:year_int,
            month:month_int,
            date:date_int,
            office_id:assistant_permissions.read
        },
        success:function(activities){
            if(activities.length > 0){
                $("#no_wroom_items").hide();
                $.each(activities,function(idx,ac){
                    drawWaitingRoomItem(ac);
                });
            }
        }
    });
}