
function init_section_analysis_order(){
    $('body').on('click', '.analysis-order', function(e){
        e.preventDefault();
        x = $(this).closest('.lv-header-alt').find('.lvh-search');

        x.fadeIn(300);
        x.find('.lvhs-input').focus();
    });

    $('body').on('click', '.search-analysis-order', function(){
        x = $(this).closest('.lv-header-alt').find('.lvh-search');

        x.fadeOut(300);
        setTimeout(function(){
            var search = x.find('.lvhs-input').val();
            if(search != ""){
                getAnalysisOrder('',1);
            }
            x.find('.lvhs-input').val('');

        }, 350);
    });

    $('#search_analysis_order').keyup(function() {
        delay(function(){
            var phrase = $('#search_analysis_order').val();
            getAnalysisOrder(phrase, 1);
        }, 800 );
    });

    $('.refresh-info-analysis-order').click(function(e)
    {
        e.preventDefault();
        searchLastAnalysisOrder(null);
    });
    $('.first-analysis-order').click(function(e)
    {
        e.preventDefault();
        getAnalysisOrder('',1); // del mas antiguo al mas actual
    });

    $('.clinical-study-filter').click(function(e)
    {
        e.preventDefault();
        searchLastAnalysisOrder(1); // del mas antiguo al mas actual y solo los estudios clinicos
    });

    $('.clinical-cabinet-filter').click(function(e)
    {
        e.preventDefault();
        searchLastAnalysisOrder(2); // del mas antiguo al mas actual y solo los estudios de gabinete
    });


}

var force_charge_report = false;

function addChargeReport(){
    var payment_method = $("#charge_payment_method").val();
    var account = $("#charge_account").val();
    var currency = $("#doctor_currency").val();
    var amount = $("#charge_amount").val();
    var details = $("#charge_details").val();
    var report_id = $("#report_id").val();

    if(account == ""){
        swal("No se puede generar el cobro","Por favor seleccione una cuenta.","error");
        return;
    }


    if(!force_charge_report){

        var amount1 = parseFloat(amount);
        var amount2 = parseFloat($("#charge_amount2").val());

        if(amount1 > amount2){
            swal({
                    title: "El monto exede",
                    text: "El monto a cobrar exede por "+toCurrency((amount1 - amount2))+" ¿Desea agregar el cobro de todos modos?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: "Cancel",
                    confirmButtonText: "Aceptar",
                    closeOnConfirm: true },
                function(){
                    force_charge_report = true;
                    addChargeReport();
                });

            return;
        }

    }

    $.ajax({
        url:$("#add_charge_url").val(),
        dataType:"json",
        data:{
            type:"report",
            account_id:account,
            currency:currency,
            report_id:report_id,
            details:details,
            amount:amount,
            payment_method_id:payment_method
        },
        success:function(data){
            if(data.done){
                $("#charge_report_modal").modal("hide");
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                searchLastAnalysisOrder();
            }else{
                swal(data.error,data.message,"error");
            }
        }

    });
}
var REPORT_ID = 0;
function send_report_modal(id){
    REPORT_ID = id;
    $("#send_email_report_modal").modal("show");
}
function validarEmail( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ) {
        return false;
    }
    else{
        return true;
    }

}
function send_report_email(){
    if($('#email_sender').val()!=""){
        if (!validarEmail($('#email_sender').val())){
            swal("Error!", "Ingrese una direccion de correo valida.", "error");
            return;
        }
    }
    else{
        swal("Error!", "Debe ingresar un correo electronico.", "error");
        return;
    }
    $.ajax({
        url: $('#url_send_report_mail').val(),
        dataType: "json",
        data: {
            email:$('#email_sender').val(),
            id: REPORT_ID},
        success: function (data) {
            $('#email_receptor').val(data.email);
            swal("Exito", "El reporte ha sido enviado.", "success");
        },
        error:function(data){
            swal("Error!", "Ocurrio un error al intentar enviar el correo electronico, por favor intente mas tarde.", "error");
        }
    });
    $('#send_email_report_modal').modal('hide');
}
function openChargeReportModal(id, price){
    $("#charge_report_modal").modal("show");

    $("#report_id").val(id);
    $("#charge_amount").val(price);
    $("#charge_amount2").val(price);

    force_charge_report = false;
}

function searchLastAnalysisOrder(clinical_order_type){
    var url = $('#url_analysis_order').val();
    showLoading("container_analysis_order");

    var searchPhrase = $('#search_analysis_order').val();
    var page = 1;
    var order = "DESC";
    $.ajax({
        type: "get",
        url: url,
        data: {searchPhrase: searchPhrase,filter_clinical_order_type: clinical_order_type,current: page, rowCount: ROWS_BY_PAGE, "sort[folio]": order},
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("container_analysis_order",response);
            var infoAnalysis = JSON.parse($('#info_analysis_order').val());
            updatePagesAnalysis(infoAnalysis.total,infoAnalysis.current);
            addFunctionListAnalysisOrder();

            //init components of
            $(".selectpicker").selectpicker("refresh");

            $("#charge_report_btn").click(function(){
                addChargeReport();
            });

            $("#charge_payment_method").change(function(){
                $(".t_account").hide();
                if($("#payment_method_"+$(this).val()).attr("affect_local") == "1"){
                    $(".local_account").show();
                }else{
                    $(".bank_account").show();
                }
                $("#charge_account").selectpicker("refresh");
            });

            $("#clinical_study_cabinet_laboratory").change(function(){
                $.ajax({
                    url:$("#studies_by_laboratory_url").val(),
                    dataType:"json",
                    data:{laboratory_id:$(this).val()},
                    success:function(data){
                        $("#clinical_study_cabinet").html("");
                        $("#clinical_study_cabinet").append("<option value=''>Seleccione un estudio</option>");
                        $.each(data,function(idx, item){
                            $("#clinical_study_cabinet").append("<option id='cabinet_study_"+item.id+"' price='"+item.price+"' value='"+item.id+"'>"+item.name+"</option>");
                        });
                        $("#clinical_study_cabinet").selectpicker("refresh");
                    }
                });
            });

            $("#clinical_study_cabinet").change(function(){

                var price = $("#cabinet_study_"+$("#clinical_study_cabinet").val()).attr("price");
                $("#cabinet_report_price").val(price);

                $.ajax({
                    url:$("#study_template_url").val(),
                    data:{study_id:$(this).val()},
                    success:function(data){
                        $("#report_sumary").summernote().code(data);
                    }
                });
            });

        },
        error: function (error) {
            swal("Error!", "Error al buscar ordenes de estudios.", "error");
        }
    });

}


function getAnalysisOrder(searchPhrase, page){
    var url = $('#url_analysis_order').val();
    showLoading("container_analysis_order");

    $.ajax({
        type: "get",
        url: url,
        data: {searchPhrase: searchPhrase, current: page, rowCount: ROWS_BY_PAGE},
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("container_analysis_order",response);
            var infoAnalysis = JSON.parse($('#info_analysis_order').val());
            updatePagesAnalysis(infoAnalysis.total,infoAnalysis.current);
            addFunctionListAnalysisOrder();
        },
        error: function (error) {
            swal("Error!", "Error al buscar analisis clinicos.", "error");
        }
    });
}

function addFunctionListAnalysisOrder(){
    //boton imprimir
    $('.print_btn_analysis_order').unbind("click");
    $('.print_btn_analysis_order').click(function(){
        var url_print = $(this).data("url");
        window.open(url_print);
    });


    // class item-consultation funcion cuando da clic en una consulta, se muestra el detalle en el modal
    $('.item-analysis-order').unbind("click");
    $('.item-analysis-order').click(function(){
        $('#loading_btn_save_analysis_order').hide();
        $('#btn-save-analysis-order').hide();
        $('#list_error_analysis_order').empty();
        $('#modalAnalysis').modal('show');
        showLoading("form-new-analysis-order");
        var url = $(this).data("url");
        $.ajax({
            type: "get",
            url: url,
            dataType:'html',
            success: function (response) {
                hideLoadingAndSetContainer("form-new-analysis-order",response);
                //initTableDiagnostic();
                //initTablePrescription();
                $("#data-table-analysis-order").bootgrid({
                    navigation: 2,
                    labels: {
                        noResults: "Sin resultados"
                    },
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    }});
            },
            error: function (error) {
                swal("Error!", "Error al buscar analisis clinico", "error");
            }
        });
    });

    $('.add_file_analysis').unbind();
    $('.add_file_analysis').on("change", function(){
        if($(this).val() != ""){
            var data = new FormData();
            var files = $(this)[0].files;

            data.append('file_0', files[0]);
            var id = $(this).data("id");
            var id_loading = "#loading_"+id;
            $(id_loading).show();

            var id_input = "#input_file_"+id;
            $(id_input).prop( "disabled", true );

            data.append('id', id);
            var url = $('#url_add_file_analysis').val();
            $.ajax({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(data){
                    if(data){
                        showInfoAnalysisOrderPatient();
                    }
                }
            });
        }
    });
    $('.delete-file').unbind("click");
    $('.delete-file').click(function(e){
        e.preventDefault();
        delete_file($(this).attr('id'),$(this).data("analysis"));
    });
}

function delete_file(file_id,div_id){

    var id = file_id;
    var analysis_id = div_id;
    var url = $('#url_delete_file_analysis').val();
    $.ajax({
        type: "DELETE",
        url: url,
        data: {id: id},
        //dataType:'json',
        success: function (response) {
            if(response){
                var id_div = "#container_row_analysis_"+analysis_id;
                $(id_div).empty();
                $(id_div).html(response);
                addFunctionListAnalysisOrder();
            }
        },
        error: function (error) {
            swal("Error!", "Error al eliminar un archivo", "error");
        }
    });
}
/*
// overflow-y: hidden;
// overflow: hidden;
// overflow-x: hidden;
position: fixed;
top: 0;
left: 0;
right: 0;
// bottom: 0;
pointer-events: none;
 */

function viewer_file(id){
    $('body').addClass('modal-open');
    $('body').css('position','fixed');
    $('body').css('top','0');
    $('body').css('left','0');
    $('body').css('right','0');

    $('#box_view_file').show();
    var root = $('#url_show_doc').val()+"?id="+id;

    openFile(root);
}

function show_image(img_id){
    var root = $('#url_show_img').val()+"?id="+img_id;
    $("#open_image_modal").modal("show");
    $("#open_image_item").attr("src",root);
}

function closeViewFile(){
    $('body').removeClass('modal-open');
    $('body').css('position','');
    $('body').css('top','');
    $('body').css('left','');
    $('body').css('right','');

    $('#box_view_file').hide();
    closeFile();
}

function updatePagesAnalysis(totalRecord,pageCurrent){
    var totalPages = Math.ceil(parseInt(totalRecord)/parseInt(ROWS_BY_PAGE));
    var arrayPage = $('#pagination-analysis-order > li');
    if(parseInt(pageCurrent) == 1){
        //bloquear el boton izquierdo
        $(arrayPage[0]).addClass('disabled');
    }else{
        //desbloquear el boton izquierdo
        $(arrayPage[0]).removeClass('disabled');
    }
    if(parseInt(totalPages) == parseInt(pageCurrent)){
        //bloquear el ultimo boton
        $(arrayPage[arrayPage.length - 1]).addClass('disabled');
    }else{
        //desbloquear el ultimo boton
        $(arrayPage[arrayPage.length - 1]).removeClass('disabled');
    }
    var i;
    //remove all number
    for(i=0;i < arrayPage.length; i++){
        if(parseInt(i) != parseInt(0) && parseInt(i) != parseInt((arrayPage.length - 1))){
            $(arrayPage[i]).remove();
        }
    }
    //all number page
    var list = "";
    for(i=1;i <= parseInt(totalPages); i++){
        if(parseInt(pageCurrent) == i){
            list += '<li class="active"><a href="" data-number="'+i+'">'+i+'</a></li>'
        }else{
            list += '<li class=""><a href="" data-number="'+i+'">'+i+'</a></li>'
        }
    }
    $(list).insertAfter($('#pagination-analysis-order > .previous_pagination'));

    //add event click
    $('#pagination-analysis-order li').unbind( "click" );
    $('#pagination-analysis-order li').click(function(e)
    {
        e.preventDefault();
        if(!$(this).hasClass("disabled")){
            var selected = getNumberPageSelectedAnalysis();
            var search = $('#search_analysis_order').val();
            if($(this).hasClass("previous_pagination")){
                //page before
                var nextPage = selected - 1;
                getAnalysisOrder(search,nextPage);

            }else if($(this).hasClass("next_pagination")){
                //next page
                var nextPage = selected + 1;
                getAnalysisOrder(search,nextPage);
            }else{
                var links = $(this).find("a");
                var nextPage = $(links[0]).data('number');
                getAnalysisOrder(search,nextPage);
            }

        }
    });
}

function getNumberPageSelectedAnalysis(){
    var arrayPage = $('#pagination-analysis-order > li');
    var i;
    var result = null;
    for(i=0;i < arrayPage.length; i++){
        if(parseInt(i) != parseInt(0) && parseInt(i) != parseInt((arrayPage.length - 1))){
            if($(arrayPage[i]).hasClass("active")){
                var links = $(arrayPage[i]).find("a");
                result = $(links[0]).data('number');
                break;
            }
        }
    }
    return result;
}


var delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();


function openChangePriceReport(report_id, price){
    $("#change_report_price_modal").modal("show");
    $("#new_price_report").val(price);
    $("#new_price_report_id").val(report_id);
}

function changePriceReport() {
    $("#change_report_price_modal").modal("hide");
    $(".modal-backdrop").remove();
    $.ajax({
        url:$("#update_report_price_url").val(),
        data:{
            report_id:$("#new_price_report_id").val(),
            price:$("#new_price_report").val()
        },
        dataType:"json",
        success:function(data){
            showInfoAnalysisOrderPatient()
        }
    });
}


function setupByPaymentMethod(){
    var payment_method = $("#charge_payment_method").val();
    var affect_local = $("#payment_method_"+payment_method).attr("affect_local");
    $(".t_account").show();
    if(affect_local == 1){
        $(".bank_account").hide();
    }else{
        $(".local_account").hide();
    }
    $("#charge_account").val("").selectpicker("refresh");

}

