function initComponentsVaccineBooksUI(){
    $("#add_vaccine_book_to_patient").click(function(){
        $("#addbookpatient").modal("show");
    });

    $("#add_vaccine_book_to_expedient_btn").click(function(){
        $.ajax({
            type: "get",
            url: $('#url_add_vaccinebook_patient').val(),
            data: {patient:$('#patient_id').val(),vaccinebook:$('#vaccine_book_select').val() },
            success: function (response) {
                if(response.result){
                    $('#addbookpatient').modal('hide');
                    $(".modal-backdrop").remove();
                    $("#load_vaccine_books_link").trigger("click");
                }else{
                    swal("Error!",response.message, "error");
                }
            },
            error : function (response) {
                console.log("erro" + response)
                console.log("Error al cargar cartilla(s) de vacunación");
            }
        });
    });


    $(".delete_vaccine_date").click(function () {
        var input = $(this).parent().find("input");
        input.val("");
        update_vaccination_date(input.attr("patient"), input.attr("age"), "");
    });

    $(".vaccine_date").datepicker({
        onSelect: function(date) {
            var input = $(this);
            update_vaccination_date(input.attr("patient"), input.attr("age"), date);
        },
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sáb'],
        monthNamesShort: [ "Ene", "Feb", "Mar", "Abr",
            "May", "Jun", "Jul", "Ago", "Sep",
            "Oct", "Nov", "Dic" ],
        monthNames: [ "Enero", "Febrero", "Marzo", "Abril",
            "Mayo", "Junio", "Julio", "Agosto", "Septiembre",
            "Octubre", "Noviembre", "Diciembre" ]
    });
}

function update_vaccination_date(patient_id, application_age_id, date) {
    $.get($("#url_update_vaccination_date").val(),{
        patient_id: patient_id,
        application_age_id:application_age_id,
        date:date
    },function () {

    },"json");
}

function deleteBook(book_id,book_name){
    swal({
        title:'Eliminar cartilla ' +book_name,
        text: "¿Esta seguro que desea eliminar la cartilla de "+book_name+"?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Si, aceptar',
        cancelButtonText: 'No, cerrar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url: $('#url_delete_vaccinebook_patient').val(),
                dataType: "json",
                data: {vaccinebook:book_id,
                    patient: $('#patient_id').val()},
                success: function (data) {
                    if(data.result){
                        swal("Confirmado!", "La cartilla fue eliminada", "success");
                        $("#load_vaccine_books_link").trigger("click");
                    }
                    else {
                        swal("Error!", data.message, "error");
                    }

                }
            });

        } else {
            swal("Cancelado!", "Se canceló la operación.", "error");
        }
    });
}
function setModalVaccination(application_id){
    APPLICATION_AGE_ID = application_id;
    $("#vaccinate_date_picked").val("");
}

function setVaccinateDate(){
    if($("#vaccinate_date_picked").val()==""){
        swal("Error!","Debe seleccionar una fecha de vacunación", "error");
        return;
    }
    var url = $("#url_add_vaccine_date").val();
    var method = "GET";

    var params = {
        patient_id:$("#patient_id").val(),
        vaccinate_date:$("#vaccinate_date_picked").val(),
        application_age_id:APPLICATION_AGE_ID,
    };

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(response){
            $(".modal-backdrop").remove();
            $('#addVaccinateDateModal').modal('hide');
            $("#load_vaccine_books_link").trigger("click");
        }
    });
}