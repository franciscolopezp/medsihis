
var calendar_bk_color;
function loadFixedActivities(options,fixed_id){
    loadActivitiesfixed(calendar_id,options,fixed_id);
}
function addAssetActivityCalendar(force) {
    var start_date = $("#activity_start_date_fixed").val();
    var end_date  = $("#activity_end_date_fixed").val();
    var start_time = $("#activity_start_time_fixed").val();
    var end_time  = $("#activity_end_time_fixed").val();

    var startDate = getDate(start_date,start_time);
    var endDate = getDate(end_date,end_time);

    if(FIXED_ACTIVITY == 0){
        swal("No es posible guardar el registro","Por favor seleccione el tipo de actividad.","error");
        return;
    }


    var url = $("#asset_activities_url").val();
    var method = "POST";

    if($("#form_action_asset_activity").val() == "update"){
        url += "/" + $("#edit_asset_activity_id").val();
        method = "PUT";
    }

    $.ajax({
        method: method,
        dataType:"json",
        url:url,
        data:{
            force:force,
            asset_activity:{
                fixed_asset_item_id: $("#fixed_asset_item_id").val(),
                title:$("#activity_name_fixed").val(),
                details:$("#activity_details_fixed").val(),
                start_date:startDate,
                end_date:endDate,
                activity:FIXED_ACTIVITY

            }
        },
        success:function(data){
            if(data.done){
                show_fixed_schedule(FIXED_ASSET_ITEM_ID,FIXED_ASSET_ITEM_NAME, calendar_color);
                clean_asset_modal();
                $('#addNew-event_fixed').modal('hide');
            }else{
                swal({
                        title: "Parece que hay una actividad para el activo en el mismo horario. ¿Confirma que desea agregar?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Aceptar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: true
                    },
                    function(){
                        addAssetActivityCalendar(true);
                    });
            }
        }
    });
}
function getDate(str1,str2){
    var dt1   = parseInt(str1.substring(0,2));
    var mon1  = parseInt(str1.substring(3,5));
    var yr1   = parseInt(str1.substring(6,10));

    var hourComponents = str2.split(" ");
    var hour = hourComponents[0];
    var format = hourComponents[1];
    var hComp = hour.split(":");

    var h = parseInt(hComp[0]);
    var m = parseInt(hComp[1]);

    if(format == "PM" && h != 12){
        h += 12;
    }else if(format == "AM" && h == 12){
        h = 0;
    }
    return new Date(yr1, mon1-1, dt1,h,m,0);
}
/*function addAssetActivity(force) {
    console.log("asdadasdasda");
    $.ajax({
        method:"POST",
        dataType:"json",
        url:$("#asset_activities_url").val(),
        data:{
            force:force,
            asset_activity:{
                fixed_asset_item_id: $("#fixed_asset_item_id").val(),
                title:$("#activity_title_fixed").val(),
                details:$("#activity_details_fixed").val(),
                start_date:$("#activity_start_date_fixed").val(),
                end_date:$("#activity_end_date_fixed").val()
            }
        },
        success:function(data){
            if(data.done){
                show_fixed_schedule(FIXED_ASSET_ITEM_ID);
                clean_asset_modal();
                $('#addNew-event_fixed').modal('hide');
            }else{
                swal({
                        title: "Parece que hay una actividad para el activoadasdasdad en el mismo horario. ¿Confirma que desea agregar?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Aceptar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: true
                    },
                    function(){
                        addAssetActivity(true);
                    });
            }
        }
    });
}*/
function delete_fixed_asset_activity_modal(){
    var method = "GET";
    var url = $("#url_delete_fixed_activity").val();
    $.ajax({
        url:url,
        dataType:"json",
        method:method,
        data:{id:CURRENT_FIXED_ACTIVITY},
        success:function(data){
            if(data.done){
                show_fixed_schedule(FIXED_ASSET_ITEM_ID,FIXED_ASSET_ITEM_NAME, calendar_color);
                $("#addNew-event_fixed").modal("hide");
                swal("Eliminado!", "Se ha eliminado la actividad", "success");
            }
        }
    });
}

function setFixedAssetItemEdit(asset_id,id,name,description ,identifier ,state ,fixed_asset_activity,schedule ){
    $("#add_fixed_asset_item_btn").attr("onclick","saveFixedAssetItem("+asset_id+",'update',"+id+")");
    if(state == "true"){$("#schedule").prop('checked', true);}else{$("#schedule").prop('checked', false);}
    if(schedule == "true"){$("#state").prop('checked', true);}else{$("#state").prop('checked', false);}
    $("#name").val(name);
    $("#identifier").val(identifier);
    $("#description").val(description);
}
function setFixedAssetItem(id){
    $("#add_fixed_asset_item_btn").attr("onclick","saveFixedAssetItem("+id+",'add',0)");
    $("#schedule").prop('checked', false);
    $("#state").prop('checked', false);
    $("#name").val("");
    $("#identifier").val("");
    $("#description").val("");

}


$(function(){
    $(".delete_asset_activity_btn").click(function() {
        var id = $(this).attr("asset-activity");
        swal({
                title: "¿Confirma que desea eliminar la actividad?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, eliminar!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false
            },
            function(){
                var ajax = $.ajax({
                    type: 'DELETE',
                    url: $("#asset_activities_url").val()+"/"+id
                });
                ajax.done(function(result, status){
                    swal("Eliminado!", "La actividad se eliminó exitosamente", "success");
                    $("#asset_activity_"+id).remove();
                });

                ajax.fail(function(error, status, msg){
                    swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                });

            });

    });
});

function initMainCalendarfixed(calendar_id,options,fixed_id){
    var defaultView = "agendaWeek";
    var fc_height = "auto";
    var minTime = "06:00:00";
    var maxTime = "23:00:00";
    var events = [];


    $(calendar_id).fullCalendar("destroy");

    if(options != undefined && options.minTime != undefined){
        minTime = options.minTime;
    }

    if(options != undefined && options.maxTime != undefined){
        maxTime = options.maxTime;
    }

    if(options != undefined && options.view != undefined){
        defaultView = options.view;
        if(options.view == "month"){
            fc_height = 700;
        }

    }

    if(parseInt($("body").css("width")) < 480){
        defaultView = 'agendaDay';
        fc_height = 600;
        $("#small_calendar").parent().remove();
    }

    if(options != undefined && options.activities != undefined){
        events = options.activities;
    }

    $(calendar_id).fullCalendar({
        eventLimit: 5,
        eventLimitText:"más",
        height: fc_height,
        minTime:minTime,
        maxTime:maxTime,
        allDayText: 'Todo el día',
        defaultView:defaultView,
        monthNames:fcMonthNames,
        dayNames:fcDayNames,
        dayNamesShort:fcDayNamesShort,
        header: {
            right: '',
            center: 'title',
            left: 'prev,next'
        },
        selectable: true,
        selectHelper: true,
        editable: true,
        events: events,
        select: function(start, end, allDay) {
            /* ACTIVITY_UPDATE.id = null;
             ACTIVITY_ACTION = "add";
             resetFormAddActivity();
             $('#add_personal_support_div').hide();*/
            $("#delete_asset_activity_modal").hide();
            clean_asset_modal();
            loadFixedAssetStatus(FIXED_ASSET_ITEM_ID);
            $('#addNew-event_fixed').modal('show');
            $("#form_action_asset_activity").val("add");
            $("#addEventTitleAsset").html("Agregar actividad");
            $("#addEventCalendar_fixed").html("Agregar");

            var ed = end.toDate();
            $("#activity_start_date_fixed").val(dateToString(ed,"dd/mm/yyyy"));
            $("#activity_end_date_fixed").val(dateToString(ed,"dd/mm/yyyy"));
        },
        eventRender: function(event, element) {
            var str_data = getPopoverHtml(event);
            element.popover({
                title: event.title,
                placement: 'top',
                trigger:"hover",
                content: str_data,
                html:true
            });
        },
        eventClick: function(calEvent, jsEvent, view) {
            var id =calEvent.id;
            CURRENT_FIXED_ACTIVITY = id;
            $("#addEventTitleAsset").html("Editar actividad");
            $("#addEventCalendar_fixed").html("Actualizar");
            $("#form_action_asset_activity").val("update");
            $("#edit_asset_activity_id").val(id);


            $.get($("#load_fixed_activity").val()+"/"+id+".json",{},function (data) {
                $("#activity_name_fixed").val(data.title);
                $("#activity_details_fixed").val(data.details);


                var data_date = getDateValues(data);

                //$('#activity_name').val(ACTIVITY_UPDATE.title);
                //$("#activity_details").val(ACTIVITY_UPDATE.details);
                //var color = $('.event-tag > span.selected').attr('data-tag');
                $("#activity_start_date_fixed").val(data_date.start_date);
                $("#activity_end_date_fixed").val(data_date.end_date);
                $("#activity_start_time_fixed").val(data_date.start_time);
                $("#activity_end_time_fixed").val(data_date.end_time);

                setTimeout(function () {
                    $("span[activity-type-fixed="+data.fixed_asset_activity_id+"]").trigger("click");
                },500);

            },"json");
            $("#addNew-event_fixed").modal("show");
            $("#delete_asset_activity_modal").show();
            loadFixedAssetStatus(FIXED_ASSET_ITEM_ID);



        },
        eventDrop:function(event,dayDelta, minuteDelta, allDay, revertFunct){
            updateActivityDate(event,dayDelta);
        },
        viewRender:function(view, element){


            /**if(view.type == "agendaDay" || view.type == "agendaWeek"){
                $(calendar_id).fullCalendar('option', 'height', 700);
            }**/
        }
    });

    if(options != undefined && options.date != undefined){
        $(calendar_id).fullCalendar( 'gotoDate', options.date );
    }

    if(options != undefined && options.color != undefined){
        calendar_bk_color = "fc-toolbar "+options.color;
        $("#calendar_doctor .fc-toolbar").attr("class",calendar_bk_color);

        $("#schedule_title").attr("class",options.color);
        $("#schedule_title").css("color","white");

    }
    $(".fc-day-grid-event").css('background-color', options.color);
    $(calendar_id).find(".fc-toolbar").find(".fc-center").find("h2").attr("onclick","openChangeDateModal()");
    $(calendar_id).find(".fc-toolbar").find(".fc-center").find("h2").css("cursor","pointer");

    $(calendar_id).find(".fc-toolbar").find(".fc-left").find("button").addClass("btn");
    $(calendar_id).find(".fc-toolbar").find(".fc-left").find("button").css("margin-left","1px");

    $(calendar_id).find(".fc-toolbar").find(".fc-right").append("<button onclick='changeMainCalendarViewFixed(\"\",\"month\",\""+options.color+"\")'>Mes</button>");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").append("<button onclick='changeMainCalendarViewFixed(\"\",\"agendaWeek\",\""+options.color+"\")'>Semana</button>");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").append("<button onclick='changeMainCalendarViewFixed(\"\",\"agendaDay\",\""+options.color+"\")'>Día</button>");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").find("button").addClass("btn");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").find("button").css("margin-left","1px");



    //$(calendar_id).find(".fc-toolbar").css("background","rgb(0, 188, 212)");

    var o_color = options.color;
    $(calendar_id).find(".fc-toolbar").find(".fc-left").find(".fc-prev-button").click(function(){
        var options = {
            view:$(calendar_id).fullCalendar("getView").type,
            date:$(calendar_id).fullCalendar("getDate").toDate(),
            color: o_color
        };

        loadFixedActivities(options,FIXED_ASSET_ITEM_ID);

    });

    $(calendar_id).find(".fc-toolbar").find(".fc-left").find(".fc-next-button").click(function(){
        var options = {
            view:$(calendar_id).fullCalendar("getView").type,
            date:$(calendar_id).fullCalendar("getDate").toDate(),
            color: o_color
        };


        loadFixedActivities(options,FIXED_ASSET_ITEM_ID);

    });
}
function changeMainCalendarViewFixed(date,view,color){
    selected_view_main_calendar = view;
    var conf = {
        view:view,
        date:date,
        color: color
    };



    if(conf.date == ''){
        conf.date = $(calendar_id).fullCalendar( 'getDate' ).toDate();
    }

    if(conf.view == ''){
        //options.view = $(calendar_id).fullCalendar( 'getView' ).type;
    }

    loadFixedActivities(conf,FIXED_ASSET_ITEM_ID);


}
function loadActivitiesfixed(calendar_id,options,fixed_id){

    var date = options.date;
    var month_int = date.getMonth();
    var year_int = date.getFullYear();

    var params = {
        year:year_int,
        month:month_int,
        is_agenda:true,
        fixed_asset_item_id:fixed_id
    };
    $.ajax({
        url:$("#url_get_fixed_asset_activities").val(),
        method:"GET",
        dataType:"json",
        data:params,
        success:function(activities){
            initMainCalendarfixed(calendar_id,{
                color: options.color,
                date:date,
                activities:activities,
                view:options.view},fixed_id);
        }
    });


}
function getPopoverHtml(event){
    var date_start;
    var date_end;

    if (event.s_date != null){
        date_start = stringToDate(event.s_date,"yyyy-mm-dd hh:mn");
    }else{
        date_start = new Date();
    }
    if (event.e_date != null){
        date_end = stringToDate(event.e_date,"yyyy-mm-dd hh:mn");
    }else{
        date_end = new Date();
    }

    var ends_same_day = true;

    var str_sd = date_start.getDate()+""+date_start.getMonth()+""+date_start.getFullYear();
    var str_ed = date_end.getDate()+""+date_end.getMonth()+""+date_end.getFullYear();

    if(str_sd != str_ed){
        ends_same_day = false;
    }

    var str_data = "<div class='popover_evt'>";

    var start_date = dateToString(date_start,"dd/fm/yyyy");
    var start_hour = dateToString(date_start,"hh:mn pp");
    var end_date = dateToString(date_end,"dd/fm/yyyy");
    var end_hour = dateToString(date_end,"hh:mn pp");

    str_data += start_date;

    if(ends_same_day){
        str_data += "<br>";
    }else{
        str_data += " ";
    }

    str_data += start_hour;

    if(ends_same_day){
        str_data += " - ";
    }else{
        str_data += "<br>";
        str_data += end_date+" ";
    }

    str_data += end_hour;

    /*if(event.activity_type_id == A_TYPE.appointment && event.patient_name != "" ){
     str_data += "<br>Paciente: "+event.patient_name+"";
     }
     */
    str_data += "<br>Detalles: "+event.details

    return str_data;
}


function setupAssetActivity(id){

}