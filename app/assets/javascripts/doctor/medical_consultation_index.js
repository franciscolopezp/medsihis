/**
 * Created by equipo-01 on 18/04/16.
 */
var ROWS_BY_PAGE = 5;

function init_section_medical_consultation(){
    setupByPaymentMethod();
    $("#charge_payment_method").change(setupByPaymentMethod);
    $('body').on('click', '.medical-consultation', function(e){
        e.preventDefault();
        x = $(this).closest('.lv-header-alt').find('.lvh-search');

        x.fadeIn(300);
        x.find('.lvhs-input').focus();
    });

    $('body').on('click', '.search-consultation', function(){
        x = $(this).closest('.lv-header-alt').find('.lvh-search');

        x.fadeOut(300);
        setTimeout(function(){
            var search = x.find('.lvhs-input').val();
            if(search != ""){
                getMedicalConsultation('',1, true);
            }
            x.find('.lvhs-input').val('');

        }, 350);
    });

    $('#search_medical_consultation').keyup(function() {
        delay(function(){
            var phrase = $('#search_medical_consultation').val();
            getMedicalConsultation(phrase, 1, true);
        }, 800 );
    });

    $('.refresh-info-consultation').click(function(e)
    {
        e.preventDefault();
        searchLastConsultation();
        //getMedicalConsultation($('#search_medical_consultation').val(),1);
    });
    $('.first-medical-consultation').click(function(e)
    {
        e.preventDefault();
       // searchLastConsultation(); // del mas actual al mas antiguo
        getMedicalConsultation('',1, false); // del mas antiguo al mas actual
    });

/*
    $('#search_medical_consultation').keydown(function(){
        //filtro de busqueda
       // var url = $(this).data("url-search");
        var text = $(this).val();
        var page = 1;
        getMedicalConsultation(text,page);
    }); */
}

function searchLastConsultation(){
    var url = $('#url_medical_consultation').val();
    showLoading("container_medical_consultation");
    var searchPhrase = $('#search_medical_consultation').val();
    var page = 1;
    var order = "DESC";
    $.ajax({
        type: "get",
        url: url,
        data: {searchPhrase: searchPhrase, current: page, rowCount: ROWS_BY_PAGE, "sort[date_consultation]": order},
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("container_medical_consultation",response);
            var infoConsultation = JSON.parse($('#info_consultation').val());
            updatePages(infoConsultation.total,infoConsultation.current);
            addFunctionListMedicalConsultation();
        },
        error: function (error) {
            swal("Error!", "Error al buscar consultas", "error");
        }
    });
}

function getMedicalConsultation(searchPhrase, page, isDESC){
    var url = $('#url_medical_consultation').val();
    showLoading("container_medical_consultation");

    var order = "ASC";
    if(isDESC){
        order = "DESC";
    }

    $.ajax({
        type: "get",
        url: url,
        data: {searchPhrase: searchPhrase, current: page, rowCount: ROWS_BY_PAGE, "sort[date_consultation]": order},
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("container_medical_consultation",response);
            var infoConsultation = JSON.parse($('#info_consultation').val());
            updatePages(infoConsultation.total,infoConsultation.current);
            addFunctionListMedicalConsultation();
        },
        error: function (error) {
            swal("Error!", "Error al buscar consultas", "error");
        }
    });
}

function addFunctionListMedicalConsultation(){
    //boton imprimir
    $('.print_btn').click(function(){
        var url_has_template = $('#url_has_template_consultation').val();
        var consultation = $(this).data("id");
        var url_print = $(this).data("url");
        $.ajax({
            type: "get",
            url: url_has_template,
            dataType: 'json',
            data: {consultation_id: consultation},
            success: function (response2) {
                if (response2.result) {
                    //var open_link = window.open('','_blank');
                    //open_link.location=url_print;
                    window.open(url_print);
                } else {
                    showAlertTemplate("Ocurrio un problema.");
                }
            }
        });
//        window.open(url_print);
    });

    //ajax para actualizar la bandera de imprimir diagnostico
    $('.is_print_diagnostic').click(function(){
        var id = $(this).val();
        var url = $('#url_update_print').val();
        var flag = false;
        if ($(this).is(":checked")){
            flag = true;
        }
        $.ajax({
            type: "put",
            url:url,
            dataType:"json",
            data:{
                id: id,
                flag: flag
            },
            success:function(data){
            }
        });
    });

    // se inicializa el tooltip
    $('[data-toggle="tooltip"]').tooltip({
        'selector': '',
        'placement': 'top',
        'container':'body'
    });
}

function updatePages(totalRecord,pageCurrent){
    var totalPages = Math.ceil(parseInt(totalRecord)/parseInt(ROWS_BY_PAGE));
    var arrayPage = $('#pagination-medical-consultation > li');
    if(parseInt(pageCurrent) == 1){
        //bloquear el boton izquierdo
        $(arrayPage[0]).addClass('disabled');
    }else{
        //desbloquear el boton izquierdo
        $(arrayPage[0]).removeClass('disabled');
    }
    if(parseInt(totalPages) == parseInt(pageCurrent)){
        //bloquear el ultimo boton
        $(arrayPage[arrayPage.length - 1]).addClass('disabled');
    }else{
        //desbloquear el ultimo boton
        $(arrayPage[arrayPage.length - 1]).removeClass('disabled');
    }
    var i;
    //remove all number
    for(i=0;i < arrayPage.length; i++){
        if(parseInt(i) != parseInt(0) && parseInt(i) != parseInt((arrayPage.length - 1))){
            $(arrayPage[i]).remove();
        }
    }
    //all number page
    var list = "";
    for(i=1;i <= parseInt(totalPages); i++){
        if(parseInt(pageCurrent) == i){
            list += '<li class="active"><a href="" data-number="'+i+'">'+i+'</a></li>'
        }else{
            list += '<li class=""><a href="" data-number="'+i+'">'+i+'</a></li>'
        }
    }
    $(list).insertAfter($('#pagination-medical-consultation > .previous_pagination'));

    //add event click
    $('#pagination-medical-consultation li').unbind( "click" );
    $('#pagination-medical-consultation li').click(function(e)
    {
        e.preventDefault();
        if(!$(this).hasClass("disabled")){
            var selected = getNumberPageSelected();
            var search = $('#search_medical_consultation').val();
            if($(this).hasClass("previous_pagination")){
                //page before
                var nextPage = selected - 1;
                getMedicalConsultation(search,nextPage,true);

            }else if($(this).hasClass("next_pagination")){
                //next page
                var nextPage = selected + 1;
                getMedicalConsultation(search,nextPage,true);
            }else{
                var links = $(this).find("a");
                var nextPage = $(links[0]).data('number');
                getMedicalConsultation(search,nextPage,true);
            }

        }
    });
}

function getNumberPageSelected(){
    var arrayPage = $('#pagination-medical-consultation > li');
    var i;
    var result = null;
    for(i=0;i < arrayPage.length; i++){
        if(parseInt(i) != parseInt(0) && parseInt(i) != parseInt((arrayPage.length - 1))){
            if($(arrayPage[i]).hasClass("active")){
                var links = $(arrayPage[i]).find("a");
                result = $(links[0]).data('number');
                break;
            }
        }
    }
    return result;
}

var delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

var CHARGE_CONSULT_ID = 0;
function openChargeModal(id, cost){
    $("#charge_payment_method").val($("#charge_payment_method option:first").val()).selectpicker("refresh");
    $("#doctor_currency").val($("#doctor_currency option:first").val()).selectpicker("refresh");
    setupByPaymentMethod();
    CHARGE_CONSULT_ID = id;
    $("#charge_details").val("");
    $("#generate_charge_modal").modal("show");
    $("#charge_amount").val(cost);
    $("#consult_full_price").val(cost);

    force_charge = false;

}

//same method in dashboard_assistant_medical_consultation.js
var force_charge = false;
function addCharge(){
    var payment_method = $("#charge_payment_method").val();
    var affect_local = $("#payment_method_"+payment_method).attr("affect_local");
    var account_to_charge = $("#charge_account").val();
    var selected_account_currency = $("#account_"+account_to_charge).attr("currency");

    var amount = $("#charge_amount").val();
    var payment_method_id = $("#charge_payment_method").val();
    var details = $("#charge_details").val();
    var account = $("#charge_account").val();
    var currency = $("#doctor_currency").val();
    var currency_iso_code = $("#account_"+account_to_charge).attr("iso_code");
    if(account == ""){
        swal("No se puede guardar el cobro","Por favor seleccione una cuenta","error");
        return;
    }

    if(payment_method_id == ""){
        swal("No se puede guardar el cobro","Por favor seleccione un método de pago.","error");
        return;
    }

    if(amount == ""){
        swal("No se puede guardar el cobro","Por favor ingrese un monto válido","error");
        return;
    }
    if(affect_local!=1){
        if(selected_account_currency != currency){
            swal("No se puede guardar el cobro","La cuenta solo acepta tipo de cambio " + currency_iso_code,"error");
            return;
        }
    }

    var amount1 = parseFloat(amount);
    var amount2 = parseFloat($("#consult_full_price").val());
    if(!force_charge){
        if(amount1 > amount2){
            swal({
                    title: "El monto exede",
                    text: "El monto a cobrar exede por "+toCurrency((amount1 - amount2))+" ¿Desea agregar el cobro de todos modos?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: "Cancel",
                    confirmButtonText: "Aceptar",
                    closeOnConfirm: true },
                function(){
                    force_charge = true;
                    addCharge();
                });

            return;
        }
    }

    $.ajax({
        url:$("#add_charge_url").val(),
        dataType:"json",
        data:{
            account_id:account,
            consult_id:CHARGE_CONSULT_ID,
            amount:amount,
            type:"consult",
            currency:currency,
            payment_method_id:payment_method_id,
            details:details
        },
        success:function(data){
            var obj = null;
            if("string" == typeof data){
                obj = JSON.parse(data);
            }else{
                obj = data;
            }

            if(obj.done){
                $("#generate_charge_modal").modal("hide");
                showInfoConsultationPatient();
            }else{
                swal(obj.error, obj.message, "error");
            }
        }
    });
}

function openMissingDailyCashModal(){
    swal({
        title: "No es posible generar un cobro",
        text: "Es necesario abrir un corte de caja",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Abrir Caja",
        cancelButtonText: "Eliminar",
        confirmButtonClass:"btn btn-primary",
        cancelButtonClass:"btn",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function(isConfirm){
        if(isConfirm){
            location.href = "/doctor/dashboard/daily_cash";
        }
    });
}
function editFullConsultation(id){

    $("#edit_full_consult_"+id).show();
    $('#loading_btn_edit_'+id).hide();
    $('#list_error_medical_consultation_edit_'+id).empty();
    loadingFormEditConsultation(id);
    $('#btn-edit-consultation_'+id).show();
    $('#btn-edit-consultation_'+id).attr('disabled',false);
    //$("#details_consult_"+id).attr("loaded","yes");
}
function loadFullConsultation(id){

    if($("#details_consult_"+id).attr("loaded") != "no"){
        $("#details_consult_"+id).slideDown();
       return;
    }

    $.ajax({
        url:$("#load_full_consultation_url").val(),
        data:{id:id},
        success:function(response){
            $("#details_consult_"+id).html(response);
            $("#details_consult_"+id).attr("loaded","yes");
            $("#details_consult_"+id).slideDown();
        }
    });
}

function closeConsultDetails(id){
    $("#details_consult_"+id).slideUp();
}


function openSendPrescriptionModal(consultation_id, patient_email){
    $("#send_prescription_email").val(patient_email);
    $("#send_prescription_id").val(consultation_id);
    $("#send_prescription_modal").modal("show");
}

function sendPrescriptionPdf() {
    $("#send_prescription_loading_message").show();
    $.ajax({
        url:$("#send_medical_prescription_pdf").val(),
        data:{
            medical_consultation_id:$("#send_prescription_id").val(),
            email:$("#send_prescription_email").val()
        },
        dataType:"json",
        success:function(data){
            $("#send_prescription_loading_message").hide();
            if(data.done){
                $("#send_prescription_modal").modal("hide");
                swal("Receta enviada",data.message,"success");
            }else{
                swal("No se pudo enviar el email",data.message,"error");
            }

        }
    });
}