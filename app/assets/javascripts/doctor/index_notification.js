
$(function(){
    if($('#active_notification').length){
        if($.parseJSON($('#active_notification').val())){
            var title = $('#title_notification').val();
            var message = $('#message_notification').val();
            var type = $('#type_notification').val();
            setTimeout(function() {
                notify(type,title,message);
            },800);
        }
    }
});

function test_noti(){
    var title = $('#title_notification').val();
    var message = $('#message_notification').val();
    var type = $('#type_notification').val();

    notify(type,title,message);
}

function notify(type,title,message){

    var form_n = "top";
    var align = "center";
    var animIn = "animated fadeInDown";
    var animOut = "animated fadeOutDown";
    var icon = "fa fa-exclamation-triangle fa-2x";
    var background_color = "rgba(33, 150, 243, 1)";
    switch (type){
        case "info":
            background_color = "rgba(33, 150, 243, 1)";
            break;
        case "warning":
            background_color = "rgba(255, 193, 7, 1)";
            break;
        case "danger":
            background_color = "rgba(244, 67, 54, 1)";
            break;
        default:

            break;
    }
    $.growl({
        icon: icon,
        title: title,
        message: message,
        url: ''
    },{
        element: 'body',
        type: type,
        allow_dismiss: true,
        placement: {
            from: form_n,
            align: align
        },
        offset: {
            x: 20,
            y: 85
        },
        spacing: 10,
        z_index: 9999,
        delay: 4500,
        timer: 20000,
        url_target: '_blank',
        mouse_over: false,
        animate: {
            enter: animIn,
            exit: animOut
        },
        icon_type: 'class',
        template: '<div data-growl="container" class="alert" style="background-color: '+background_color+'" role="alert">' +
        '<span data-growl="icon"></span>' +
        '<span data-growl="title" style="font-size: 1.2em;"></span>' +
        '<span data-growl="message" style="font-size: 1.2em;"></span>' +
        '<a href="#" data-growl="url"></a>' +
        '</div>'
    });
};