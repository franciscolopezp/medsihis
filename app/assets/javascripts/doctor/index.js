//= require doctor/accept_term_conditions
//= require doctor/index_notification
//= require doctor/calendar-doctor.js
//= require doctor/fixed_asset_calendar.js
var DOCTOR_NAME = "";
var DOCTOR_ID_SELECTED = 0;
var calendar_id = "#calendar_doctor";
var FIXED_ASSET_ITEM_ID = 0;
var FIXED_ASSET_ITEM_NAME = "";
var FIXED_ACTIVITY = 0;
var CURRENT_FIXED_ACTIVITY = 0;
var inputsValidate = ["#activity_name","#activity_start_date","#activity_end_date",
    "#activity_start_time","#activity_end_time"];
var A_TYPE = {
    appointment : 1,
    surgery : 2
};

var is_doctor = true;
var selected_view_main_calendar = "agendaWeek";
var selected_view_main_calendar_doctor = "month";
var calendar_in_use = "";
var calendar_color = "";

$(document).ready(function(){

    $("#small_calendar .fc-other-month").html("");

    $("#addEventCalendar_fixed").click(function(){
        addAssetActivityCalendar(false);
    });
    $("#delete_asset_activity_modal").click(function(){
        delete_fixed_asset_activity_modal();
    });
    if($(".asset_item")[0]){
        $($(".asset_item")[0]).trigger("click");
    }

});
function show_fixed_schedule(id,item_name,color){
    calendar_in_use = "asset";
    calendar_color = color;

    /** Cambia el header del calendario inmediatamente, sin embargo se vuelven a cambiar cuando se carga otra vez el calendario
     * se hizo de esta manera para que no sea perceptible para el usuario **/
    var c_color = "fc-toolbar " + color;
    $("#calendar_doctor .fc-toolbar").attr("class",c_color);
    $("#schedule_title").attr("class",color);
    $("#schedule_title").css("color","white");

    FIXED_ASSET_ITEM_ID = id;
    FIXED_ASSET_ITEM_NAME = item_name;
    $("#schedule_title").html(item_name);
    $("#fixed_asset_item_id").val(id);
    loadFixedActivities({
        date: new Date(),
        view:selected_view_main_calendar,
        color: color
    },id);
    loadFixedAssetStatus(FIXED_ASSET_ITEM_ID);
}

function loadFixedAssetStatus(id){
    var activities_span = "";
    $.ajax({
        url:$("#url_load_asset_activities").val(),
        dataType:"json",
        method:'get',
        data:{id:id},
        success:function(data){
            if(data.activities.length > 0){
                $.each(data.activities, function (idx, info) {
                    activities_span += "<span data-tag='"+info.color+"' style='background: "+info.color+"' class='"+info.color+"' activity-type-fixed='"+info.id+"'></span><strong class='activity_type_name_fixed'>"+info.name+"</strong>";
                });
            }else{
                activities_span = "Es necesario agregar al menos una actividad al activo fijo";
            }
            $("#fixed_asset_acitivities_div").html(activities_span);
            $('body').on('click', '#fixed_asset_acitivities_div > span', function(){
                $('.event-tag > span').removeClass('selected');
                $(this).addClass('selected');
                FIXED_ACTIVITY = $(this).attr("activity-type-fixed");
            });
        }
    });
}


function show_schedule(id,doctor_name){
    calendar_in_use = "doctor";
    DOCTOR_NAME = doctor_name;
    DOCTOR_ID_SELECTED = id;
    $("#doctor_id").val(id);
    loadDoctorActivities({
        date: new Date(),
        view: selected_view_main_calendar_doctor
    },id);
    var offices_option = "";
    $.ajax({
        url:$("#url_load_doctor_offices").val(),
        dataType:"json",
        method:'get',
        data:{doctor_id:id},
        success:function(data){
            $.each(data.offices, function (idx, info) {
                offices_option += "<option id=office_"+info.id+" duration="+info.consult_duration+" value="+info.id+">"+info.name+"</option>"
            });
            $('#activity_office').html(offices_option);
            $('#activity_office').selectpicker('refresh');
            $("#schedule_title").html("Dr. " + doctor_name);
            $("#schedule_title").css("background","white");
            $("#schedule_title").css("color","gray");
            $("#schedule_title").attr("class","");
        }
    });
}

function loadDoctorActivities(options,doctor_id){
    loadActivities(calendar_id, $("#current_office_id").val(),options,doctor_id);
}

$(function(){
    $("#change_office_btn").click(function(){
        $("#current_office_select").show();
    });

    $("#current_office_id").change(function(){
        var text = $("#current_office_id option:selected").text();
        $("#current_office_name").html(text);
        $("#current_office_select").hide();
        loadDoctorActivities();
    });

    $(".fixed_asset_parent").click(function () {
        var holder = $(this).parent().find(".asset_items_holder");
        holder.toggle();
    });

});
function clean_asset_modal(){
    $("#activity_name_fixed").val("");
    $("#activity_details_fixed").val("");
    $("#activity_start_time_fixed").val("");
    $("#activity_end_time_fixed").val("");
}

function loadActivityCallback(event){
    var html_activity = getActivityHtml(event);
    $("#activity_information").html(html_activity);
    $("#activity_operation").modal("show");
}