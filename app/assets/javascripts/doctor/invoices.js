var FISCAL_AMOUNT = 0;
var subtotal = 0;
var IVA = 0;
var ISR = 0;
var iva_doc = 0;
var importe_iva = 0;
var row_counter = 1;
var importe_isr = 0;
var tipo_persona;
var MED_CONSULT_ID;
var datos_emisor = "";
var concepts_open_invoice = "";
jQuery(document).ready(function() {
   var actual_date = new Date();
   var actual_end_date = ""
    if ((actual_date.getMonth() + 1) < 10){
       actual_month = "0" + (actual_date.getMonth() + 1)
    }else{
       actual_month = (actual_date.getMonth() + 1)
    }
    actual_end_date = actual_date.getDate() + "/" + actual_month + "/" + actual_date.getFullYear();
    var actual_start_date = "01/" + actual_month + "/" + actual_date.getFullYear();
    $("#end_date").val(actual_end_date);
    $("#start_date").val(actual_start_date);
    var html = '<div class="col-md-offset-3 col-md-9">'+
        '<div class="preloader pl-xxl">'+
        '<svg class="pl-circular" viewBox="25 25 50 50">'+
        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
        '</svg>'+
        'Cancelando...'+
        '</div>'+
        '</div>';



    $('#cancel_invoice_loading').append(html)


    $('#cancel_invoice_loading').hide();



    var grid2 = $("#data-table-invoices_open").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function()
        {

            var fecha_cadena = "all";
            var fecha_cadena_end = "all";
            var patient_fi =  "all";//$('#person_for_search').val();
            var bussines_name = $('#bussines_name_search').val();
            if(bussines_name == "" || bussines_name == "undefined"){
                bussines_name = "all"
            }
            /*if(patient_fi == "" || patient_fi == "undefined"){
                patient_fi = "all"
            }*/
            var invoice_status = "all";//$('#invoice_status').val();
            /*if(invoice_status == ""){
                invoice_status = "all"
            }*/
            /*var start_date = $('#start_date').val();
            if(start_date == ""){
                fecha_cadena = "all"
            }
            else{
                var from = start_date.split("/");
                var f = new Date(from[2], from[1] - 1, from[0]);
                fecha_cadena = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+ f.getDate();
            }
            var end_date = $('#end_date').val();
            if(end_date == ""){
                fecha_cadena_end = "all"
            }else{
                var end = end_date.split("/");
                var fe = new Date(end[2], end[1] - 1, end[0]);
                fecha_cadena_end = fe.getFullYear()+"-"+(fe.getMonth()+1)+"-"+ (fe.getDate()+1);
            }*/
            return $('#data-table-invoices_open').data('source')+"?patient_fi="+patient_fi+"&invoice_status="+invoice_status+"&start_date="+fecha_cadena+"&end_date="+fecha_cadena_end+"&bussines_name="+bussines_name;
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        templates: {
            search: ""
        },
        formatters: {
            "commands": function(column, row) {
                if(!row.cancel){
                    return "<a href="+row.print+" target='_blank' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-file-text zmdi-hc-fw\" title=\"PDF\"></i></a>"+
                        "<a href="+row.download_xml+" target='_blank' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\"  ><img src=\""+$('#url_icon_xml').val()+"\" alt=\"XML\" style=\"width:22px;height:24px;padding-bottom:4px;\"></a>"+
                        "<a href='#' onclick='prepareInvoiceEmail("+row.id+")'  class=\"btn bgm-green btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-email zmdi-hc-fw\" title=\"Enviar correo\"></i></a>"+
                        "<a href='#' onclick='cancelInvoice("+row.id+")'  class=\"btn bgm-red btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-close zmdi-hc-fw\" title=\"Cancelar factura\"></i></a>"
                }
                else{
                    return "<a href="+row.print+" target='_blank' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-file-text zmdi-hc-fw\" title=\"PDF\"></i></a>"+
                        "<a href="+row.download_xml+" target='_blank' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\"  ><img src=\""+$('#url_icon_xml').val()+"\" alt=\"XML\" style=\"width:22px;height:24px;padding-bottom:4px;\"></a>";
                }

            }
        }
    });

    var grid = $("#data-table-invoices").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function()
        {

            var fecha_cadena = "";
            var fecha_cadena_end = "";
            var patient_fi = $('#search_business_name').val();

            var invoice_status = $('#invoice_status').val();
            if(invoice_status == ""){
                invoice_status = "all"
            }
            var start_date = $('#start_date').val();
            if(start_date == ""){
                fecha_cadena = "all"
            }else{
                var from = start_date.split("/");
                var f = new Date(from[2], from[1] - 1, from[0]);
                fecha_cadena = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+ f.getDate();
            }
            var end_date = $('#end_date').val();
            if(end_date == ""){
                fecha_cadena_end = "all"
            }else{
                var end = end_date.split("/");
                var fe = new Date(end[2], end[1] - 1, end[0]);
                fecha_cadena_end = fe.getFullYear()+"-"+(fe.getMonth()+1)+"-"+ (fe.getDate()+1);
            }
            return $('#data-table-invoices').data('source')+"?patient_fi="+patient_fi+"&invoice_status="+invoice_status+"&start_date="+fecha_cadena+"&end_date="+fecha_cadena_end;
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                if(!row.cancel){
                    return "<a href="+row.print+" target='_blank' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-file-text zmdi-hc-fw\" title=\"PDF\"></i></a>"+
                        "<a href="+row.download_xml+" target='_blank' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\"  ><img src=\""+$('#url_icon_xml').val()+"\" alt=\"XML\" style=\"width:22px;height:24px;padding-bottom:4px;\"></a>"+
                        "<a href='#' onclick='prepareInvoiceEmail("+row.id+")'  class=\"btn bgm-green btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-email zmdi-hc-fw\" title=\"Enviar correo\"></i></a>"+
                        "<a href='#' onclick='cancelInvoice("+row.id+")'  class=\"btn bgm-red btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-close zmdi-hc-fw\" title=\"Cancelar factura\"></i></a>"
                }
                else{
                    return "<a href="+row.print+" target='_blank' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-file-text zmdi-hc-fw\" title=\"PDF\"></i></a>"+
                        "<a href="+row.download_xml+" target='_blank' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\"  ><img src=\""+$('#url_icon_xml').val()+"\" alt=\"XML\" style=\"width:22px;height:24px;padding-bottom:4px;\"></a>";
                }

            }
        }
    });


});
var timeout = null;

function doDelayedSearch(val) {
    if (timeout) {
        clearTimeout(timeout);
    }
    timeout = setTimeout(function() {
        $("#data-table-invoices").bootgrid("reload");
    }, 500);
}
function reload(){
    $("#data-table-bills").bootgrid("reload");
}
function deleteemailfield(number){
    $("#newemaildiv_"+number).remove();
}
function addemailfield(){
    var d = new Date();
    var uniqnumber = d.valueOf();
    $("#text_emails_div").append('<div class="col-xs-12" id="newemaildiv_'+uniqnumber+'"><strong >Correo electronico:</strong> <br><input  type="text" class="form-control input-lg" id="email_receptor_added" >' +
        '<a  href="javascript:void(0);" onclick="deleteemailfield('+uniqnumber+')"><i class="zmdi zmdi-delete" style="font-size: 1.33em;"></i></a></div>');
}
function prepareInvoiceEmail(invoice_id){
    $('#email_receptor').val("");
    $("#emails_div").html("");
    $("#text_emails_div").html("");
    emailsset = '';
    $('#invoice_id').val(invoice_id);
    $.ajax({
        url: $('#url_get_receptor').val(),
        dataType: "json",
        data: {invoice_id:invoice_id},
        success: function (data) {
            $.each(data.emails, function(idx,info){
                emailsset += '<div class = "col-xs-12">';
                if(info.number == 0){
                    emailsset +=  '<h5>Correo del paciente</h5>';
                }else if(info.number == 1){
                    emailsset +=  '<h5>Correo fiscal 1</h5>';
                }else{
                    emailsset +=  '<h5>Correo fiscal 2</h5>';
                }
                emailsset +=  '<div class="checkbox">'+
                    '<label id="labele_'+info.number+'">'+
                    '<input type="checkbox"  name="emailcheck_'+info.number+'" id="emailcheck_'+info.number+'">'+
                    '<i class="input-helper"></i>'+
                    '<strong >'+info.email+'</strong>'+
                '</label>'+
                '</div>';
                emailsset += '</div>';
                $("#emails_div").html(emailsset);
            });
            //$('#email_receptor').val(data.email);
        },
        error:function(data){
            console.log("fail");
        }
    });
    $('#send_email_modal').modal('show');
}
function send_invoice_email(){
    if($('#email_receptor').val()!="" || $("#emailcheck_0").is(':checked') || $("#emailcheck_1").is(':checked') || $("#emailcheck_2").is(':checked')){

    }
    else{
        swal("Error!", "Debe ingresar o seleccionar al menos un correo electronico.", "error");
        return;
    }
    if($('#email_receptor').val()!="" ){
        if (!validarEmail($('#email_receptor').val())){
            swal("Error!", "Ingrese una direccion de correo valida.", "error");
            return;
        }
    }
    var extramails = "";
    $("#text_emails_div :text").each(function(){
        if (!validarEmail($(this).val())){
            swal("Error!", "Algun correo no es una direccion de correo valida.", "error");
            return;
        }else{
            extramails += $(this).val() + ";";
        }


    });
    var emailp = "";
    var emailf1 = "";
    var emailf2 = "";
    if ($("#emailcheck_0").is(':checked')){
        emailp = $('#labele_0').text();
    }
    if ($("#emailcheck_1").is(':checked')){
        emailf1 = $('#labele_1').text();
    }
    if ($("#emailcheck_2").is(':checked')){
        emailf2 = $('#labele_2').text();
    }
    $.ajax({
        url: $('#url_send_invoice_mail').val(),
        dataType: "json",
        data: {
            email:$('#email_receptor').val(),
            emailp:emailp,
            emailf1:emailf1,
            emailf2:emailf2,
            extramails:extramails,
            invoice_id: $('#invoice_id').val()},
        success: function (data) {
            swal("Exito", "La factura ha sido enviada con exito.", "success");
            $('#email_receptor').val(data.email);
        },
        error:function(data){
            swal("Error!", "Ocurrio un error al intentar enviar el correo electronico, por favor intente mas tarde.", "error");
        }
    });
    $('#send_email_modal').modal('hide');
}

function cancelInvoice(invoice_id,folio){
    swal({
        title:'Cancelación de factura',
        text: "¿Esta seguro que desea cancelar la factura?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Si, cancelar',
        cancelButtonText: 'No, cerrar!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm){
        if (isConfirm) {

            $.ajax({
                url: $('#url_cancel_invoice').val(),
                dataType: "json",
                beforeSend: function(){
                    $('#cancel_invoice_loading').show();
                },
                data: {invoice_id:invoice_id},
                success: function (data) {
                    if(data.result == "1"){
                        swal("Confirmado!", "La factura ha sido cancelada", "success");
                        $("#data-table-invoices").bootgrid("reload");
                        $("#data-table-invoices_open").bootgrid("reload");
                    }
                    else {
                        swal("Error!", data.message, "error");
                    }

                }
            });

        } else {
            swal("Cancelado!", "Se canceló la operación.", "error");
        }
    });

    $(".lead.text-muted").append("<div style='float: left;width: 25%'><span></span></div><div style='float:left;width: 50%' id='cancel_invoice_loading'></div><div style='float: left;width: 25%'><span></span></div>");





    var html = '<div class="col-md-offset-3 col-md-9">'+
        '<div class="preloader pl-xxl">'+
        '<svg class="pl-circular" viewBox="25 25 50 50">'+
        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
        '</svg>'+
        'Cancelando...'+
        '</div>'+
        '</div>';



    $('#cancel_invoice_loading').append(html);
    $('#cancel_invoice_loading').hide();

}
function validarEmail( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ) {
        return false;
    }
    else{
        return true;
    }

}
function reload_invoices(){
    $('#data-table-invoices').bootgrid('reload');
}
function reload_open_invoices(){
    $('#data-table-invoices_open').bootgrid('reload');
}
function clear_all(){
    $('#search_business_name').val("");
    $('#invoice_status').val("");
    $('#invoice_status').selectpicker("refresh");
    $('#start_date').val("");
    $('#end_date').val("");
    $('#data-table-invoices').bootgrid('reload');
}
function print_report_invoices(){
    var fecha_cadena = "";
    var fecha_cadena_end = "";
    var patient_fi = $('#search_business_name').val();

    var invoice_status = $('#invoice_status').val();
    if(invoice_status == ""){
        invoice_status = "all"
    }
    var start_date = $('#start_date').val();
    if(start_date == ""){
        fecha_cadena = "all"
    }else{
        var from = start_date.split("/");
        var f = new Date(from[2], from[1] - 1, from[0]);
        fecha_cadena = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+ f.getDate();
    }
    var end_date = $('#end_date').val();
    if(end_date == ""){
        fecha_cadena_end = "all"
    }else{
        var end = end_date.split("/");
        var fe = new Date(end[2], end[1] - 1, end[0]);
        fecha_cadena_end = fe.getFullYear()+"-"+(fe.getMonth()+1)+"-"+ (fe.getDate()+1);
    }
    url_pdf = $('#url_print_report').val()+"?patient_fi="+patient_fi+"&invoice_status="+invoice_status+"&start_date="+fecha_cadena+"&end_date="+fecha_cadena_end;
    window.open(url_pdf, '_blank');

}
