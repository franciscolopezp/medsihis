var ACTIVITY_UPDATE = {};
var ACTIVITY_ACTION = "";
var aux_calendar = "#aux_agenda_day";

$(function(){
    $("#all_day_activity").click(function(){
        if($(this).is(":checked")){
            $("#activity_start_time").parent().hide();
            $("#activity_end_time").parent().hide();
            if($("#activity_start_time").val() == "")
                $("#activity_start_time").val("12:01 AM");
            if($("#activity_end_time").val() == "")
                $("#activity_end_time").val("11:59 PM");
        }else{
            $("#activity_start_time").parent().show();
            $("#activity_end_time").parent().show();
        }
    });

    $("#activity_start_time").on("dp.change",function(){
        var office = $("#activity_office").val();
        if(office != "" && ACTIVITY_TYPE == A_TYPE.appointment){
            var duration = $("#office_"+office).attr("duration");
            if(duration != undefined){
                var date = getDate($("#activity_start_date").val(),$("#activity_start_time").val());
                var date2 = new Date(date.getTime() + duration*60000);
                var hour_end = dateToString(date2,"hh:mn pp");
                $("#activity_end_time").val(hour_end);
            }
        }
    });

    $("#activity_patient").change(function(){
        var value = $("#activity_patient").val();
        if(value != ""){
            $("#activity_name").val("Cita " + value);
        }else{
            $("#activity_name").val("");
        }
    });


    /*PrivatePub.subscribe("/messages/public", function(data) {
    });

    PrivatePub.subscribe("/messages/private/"+socket_params.doctor_username, function(data) {
        socketResponse(data);
    });*/

});

$(document).ready(function(){
    $('body').on('click', '.event-tag > span', function(){
        $('.event-tag > span').removeClass('selected');
        $(this).addClass('selected');
        updateStatusSelect($(this).attr("activity-type"));
        ACTIVITY_TYPE = $(this).attr("activity-type");

        if(parseInt($(this).attr("activity-type")) == A_TYPE.surgery){
            $('#add_personal_support_div').show();
        }else{
            $('#add_personal_support_div').hide();
        }

        if(parseInt($(this).attr("activity-type")) == A_TYPE.appointment){
            $("#patient_div").show();
            $("#activity_patient_ts_autocomplete").val("");
        }else{
            $("#activity_patient").val();
            $("#activity_patient_ts_autocomplete").val("");
            $("#patient_div").hide();
        }

    });

    $('body').on('click', '#addEvent', addActivity);
    $(".activity_status").hide();

});

function openChangeDateModal() {
    $("#change_date_modal").modal("show");
    setTimeout(function(){
        initSmallCalendar();
        $("#small_calendar_modal .fc-toolbar").css("background","#00BCD4");
    },500);
}

function getPopoverHtml(event){

    var date_start = stringToDate(event.s_date,"yyyy-mm-dd hh:mn");
    var date_end = stringToDate(event.e_date,"yyyy-mm-dd hh:mn");
    var ends_same_day = true;

    var str_sd = date_start.getDate()+""+date_start.getMonth()+""+date_start.getFullYear();
    var str_ed = date_end.getDate()+""+date_end.getMonth()+""+date_end.getFullYear();

    if(str_sd != str_ed){
        ends_same_day = false;
    }

    var str_data = "<div class='popover_evt'>";

    var start_date = dateToString(date_start,"dd/fm/yyyy");
    var start_hour = dateToString(date_start,"hh:mn pp");
    var end_date = dateToString(date_end,"dd/fm/yyyy");
    var end_hour = dateToString(date_end,"hh:mn pp");

    str_data += start_date;

    if(ends_same_day){
        str_data += "<br>";
    }else{
        str_data += " ";
    }

    str_data += start_hour;

    if(ends_same_day){
        str_data += " - ";
    }else{
        str_data += "<br>";
        str_data += end_date+" ";
    }

    str_data += end_hour;

    if(event.activity_type_id == A_TYPE.appointment && event.patient_name != "" ){
        str_data += "<br>Paciente: "+event.patient_name+"";
    }

    str_data += "<br>Consultorio: "+event.office_name;
    str_data += "</div>";

    return str_data;
}

function getActivityHtml(activity){
    var html_activity = "";
    html_activity += "<div class='row'>";
    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Tipo</strong><br>";
    html_activity += activity.activity_type_name+"<br><br>";
    html_activity += "</div>";

    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Consultorio</strong><br>";
    if(activity.office_id.length == 1){
        html_activity += $("#office_"+activity.office_id[0]).html()+"<br><br>";
    }else{
        html_activity += "Todos los consultorios<br><br>";
    }
    html_activity += "</div>";
    html_activity += "</div>";
    html_activity += "<div class='row'>";
    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Actividad</strong><br>";
    html_activity += activity.title+"<br><br>";
    html_activity += "</div>";

    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Detalles</strong><br>";
    html_activity += activity.details+"<br><br>";
    html_activity += "</div>";
    html_activity += "</div>";

    html_activity += "<div class='row'>";
    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Fecha de Inicio</strong><br>";

    if(activity.allDay){
        html_activity += dateToString(stringToDate(activity.s_date,"yyyy-mm-dd hh:mn"),"dd/fm/yyyy");
    }else{
        html_activity += dateToString(stringToDate(activity.s_date,"yyyy-mm-dd hh:mn"),"dd/fm/yyyy hh:mn pp");
    }

    html_activity += "<br><br>";
    html_activity += "</div>";

    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Fecha de Finalización</strong><br>";

    if(activity.allDay){
        html_activity += dateToString(stringToDate(activity.e_date,"yyyy-mm-dd hh:mn"),"dd/fm/yyyy");
    }else{
        html_activity += dateToString(stringToDate(activity.e_date,"yyyy-mm-dd hh:mn"),"dd/fm/yyyy hh:mn pp");
    }

    html_activity += "<br><br>";
    html_activity += "</div>";


    if(activity.activity_type_id == A_TYPE.appointment && activity.patient_name != "" ){
        html_activity += "<div class='col-md-6'>";
        html_activity += "<strong>Paciente</strong><br>"+activity.patient_name;
        html_activity += "</div>";
    }

    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Estado</strong><br>";
    html_activity += activity.activity_status_name;
    html_activity += "</div>";

    html_activity += "</div>";// end of class row

    if(activity.activity_type_id == A_TYPE.surgery){
        html_activity += "<br><strong>Personal de apoyo</strong><br>";
        html_activity += "<div class='row'>";
        html_activity += "<div class='col-md-10'>";
        html_activity += "<div>";
        $.each(activity.personal,function(idx, person){
            html_activity += person.role+"<br>";
            html_activity += "<label><input type='checkbox' class='resend_email' value='"+person.id+"'> "+person.full_name+" ("+person.email+")</label><br>";
        });
        html_activity += "</div>";
        html_activity += "</div>";
        html_activity += "<div class='col-md-2'>";
        html_activity += '<button class="btn bgm-lightgreen btn-icon waves-effect waves-circle waves-float" title="Reenviar correo de Invitación" onclick="sendEventEmail('+activity.id+')"><i class="zmdi zmdi-email zmdi-hc-fw"></i></button>';
        html_activity += "</div>";
        html_activity += "</div>";
    }

    return html_activity;
}

function sendEventEmail(activity_id){
    var persons = [];
    var checks = $( ".resend_email:checked" );
    $.each(checks,function(idx,c){
        persons.push($(c).val());
    });

    if(persons.length == 0){
        swal("No se puede enviar el correo","Por favor seleccione los participantes.","error");
        return;
    }

    $.ajax({
        url:"/doctor/activities/resend_surgery_email",
        data:{
            data:persons,
            activity_id:activity_id
        },
        dataType:"json",
        success:function(){
            swal("Notificación enviada","El correo se ha enviado satisfactoriamente.","success");
        }
    });
}


function validate_rank_date_appointment(startDate,endDate){
    var url = $('#url_validate_date').val();

    var aux1 = new Date(startDate.getTime());
    var aux2 = new Date(endDate.getTime());


    var now_date = new Date();
    var datenohour = new Date().setHours(0,0,0,0);
    var startnohour = aux1.setHours(0,0,0,0);
    var endnohour = aux2.setHours(0,0,0,0);
    now_date.setMinutes(now_date.getMinutes() - 20);


    if(ACTIVITY_ACTION == "add") {

        if (datenohour > startnohour) {
            swal("No se pueden agregar actividades con fechas anteriores a la actual");
            return true;
        }
        else {
            if(ACTIVITY_UPDATE.allDay) {

            }else{
                if (now_date.getTime() > startDate.getTime()) {
                    swal("No se pueden agregar actividades con una hora anterior a la actual");
                    return true;
                }
            }
        }
    }
    if(endnohour<startnohour){
        swal("La fecha final no puede ser menor a la actual");
        return true;
    }
    else {
        if (endDate.getTime() < startDate.getTime()) {
            swal("La hora final no puede ser menor a la actual");
            return true;
        }

    }

    var exist = false;
    if(ACTIVITY_ACTION == "add"){
        ACTIVITY_UPDATE.id = 0;
    }
    $.ajax({
        url:url,
        dataType:"json",
        method:"get",
        async: false,
        cache: false,
        data:{start_date:startDate, end_date:endDate,activity_id:ACTIVITY_UPDATE.id,doctor_id:$("#doctor_id").val()},
        success:function(act){
            exist = act.result;
            if(exist){
                swal(act.message+" Actividad: "+act.activity.name);

            }
        }
    });
return exist;

}

function loadFullActivity(id){
    $.ajax({
        url:"/doctor/activities/"+id+".json",
        dataType:"json",
        success:function(event){
            ACTIVITY_UPDATE = event;
            loadActivityCallback(event);
        }
    });

}

function getDate(str1,str2){
    // str1 debe tener formato dd/MM/yyyy
    // str2 debe tener formato hh:mm a  formato 12 hrs
    var dt1   = parseInt(str1.substring(0,2));
    var mon1  = parseInt(str1.substring(3,5));
    var yr1   = parseInt(str1.substring(6,10));

    var hourComponents = str2.split(" ");
    var hour = hourComponents[0];
    var format = hourComponents[1];
    var hComp = hour.split(":");

    var h = parseInt(hComp[0]);
    var m = parseInt(hComp[1]);

    if(format == "PM" && h != 12){
        h += 12;
    }else if(format == "AM" && h == 12){
        h = 0;
    }
    return new Date(yr1, mon1-1, dt1,h,m,0);
}

function getDateValues(activity){
    var s_date = stringToDate(activity.s_date,"yyyy-mm-dd hh:mn");
    var e_date = stringToDate(activity.e_date,"yyyy-mm-dd hh:mn");
    var start_date = dateToString(s_date,"dd/mm/yyyy");
    var end_date = dateToString(e_date,"dd/mm/yyyy");
    var start_time = dateToString(s_date,"hh:mn pp");
    var end_time = dateToString(e_date,"hh:mn pp");
    return {
        start_date:start_date,
        end_date:end_date,
        start_time:start_time,
        end_time:end_time
    };
}


function loadActivities(calendar_id,office_id,options,doctor_id){

    var date = options.date;
    var month_int = date.getMonth();
    var year_int = date.getFullYear();

    var params = {
        year:year_int,
        month:month_int,
        office_id:office_id,
        is_agenda:true,
        doctor_id:doctor_id
    };
    if(options.view == "agendaDay"){
        params.wroom_page = true;
        params.date = date.getDate();

        $.ajax({
            url:"/doctor/activities/get_day_activities",
            method:"GET",
            dataType:"json",
            data:params,
            success:function(data){
                initMainCalendar(calendar_id,{
                    date:date,
                    activities:data.activities,
                    minTime:data.start_h,
                    maxTime:data.end_h,
                    view:"agendaDay"
                });
            }
        });
    }else{
        $.ajax({
            url:"/doctor/activities",
            method:"GET",
            dataType:"json",
            data:params,
            success:function(activities){
                initMainCalendar(calendar_id,{
                    date:date,
                    activities:activities,
                    view:options.view});
            }
        });
    }


}

function updateActivityDate(event,dayDelta){

    var days = dayDelta._days; // número de días que se corrió la actividad puede ser positivo o negativo

    var current_start = stringToDate(event.s_date,"yyyy-mm-dd hh:mn");   // obtengo la fecha original de inicio
    var current_end = stringToDate(event.e_date,"yyyy-mm-dd hh:mn"); // fecha original de fin

    var update_start = new Date(current_start.getTime()); // se incializan las variable con las que se va a actualizar la fecha por default es la misma
    var update_end = new Date(current_end.getTime()); // se incializan las variable con las que se va a actualizar la fecha por default es la misma


    // se modifican las fechas actuales agregando los días de diferencia y se generan dos variables auxiliares para verificar la disponibilidad
    current_start = current_start.setTime(current_start.getTime() + (days * 24 * 60 * 60 * 1000));
    current_end = current_end.setTime(current_end.getTime() + (days * 24 * 60 * 60 * 1000));
    var n_start = new Date(current_start);
    var n_end = new Date(current_end);

    update_start = n_start;
    update_end = n_end;

/*
    if(!validate_rank_date_appointment(n_start,n_end)){
        // si está disponible se sobreescriben las variables update_start y update_end

    }*/

    $.ajax({
        url:"/doctor/activities/update_dates",
        data:{
            activity_id: event.id,
            start_date : update_start ,
            end_date : update_end,
            calendar: calendar_in_use
        },
        error: function(response){
            alert("Error: Recargue la página he intente nuevamente, si persiste consulte al administrador");
        }
    });
}

function resetFormAddActivity(){
    $(".event-tag .bgm-cyan").click();

    setTimeout(function(){
        $(".event-tag .bgm-cyan").addClass("selected");
        $(".option_status_1").show();
        $("#activity_status").selectpicker("refresh");
    },500);


    if(!is_doctor){//if is assistant show only valid offices
        $(".activity_office").hide();
        $("#office_all").html("TODOS LOS CONSULTORIOS");
        if(assistant_permissions.create.length > 1){
            $("#office_all").show();
        }else if (assistant_permissions.create.length == 1){
            $("#office_all").hide();
            $("#activity_office").val(assistant_permissions.create[0]);
        }else if(assistant_permissions.create.length == 0){
            swal("Hay un problema","Usted no tiene permisos para crear actividades","error");
            throw 'No tiene permisos para crear actividades';
        }
        $.each(assistant_permissions.create,function(idx,o){
            $("#office_"+o).show();
        });

        $("#activity_office").selectpicker('refresh');
    }

    $.each(inputsValidate,function(idx,name){
        $(name).closest('.form-group').removeClass('has-error');
        $(name).val("");
    });

    $("#activity_patient").val("");
    $("#activity_patient_ts_autocomplete").val("");
    $("#activity_details").val("");

    $("#personal_supports").val([]).selectpicker("refresh");

    $(".event-tag span").removeClass("selected");
    $("#office_id").val("");
    $("#all_day_activity").prop("checked",false);
    $("#activity_start_time").parent().show();
    $("#activity_end_time").parent().show();

    $("#addEvent").html("Guardar");
    $("#addEventTitle").html("Agregar Actividad");

    $(".activity_status").hide();

}

function setupUpdateModal(){

    var data = getDateValues(ACTIVITY_UPDATE);

    $('#activity_name').val(ACTIVITY_UPDATE.title);
    $("#activity_details").val(ACTIVITY_UPDATE.details);
    var color = $('.event-tag > span.selected').attr('data-tag');
    $("#activity_start_date").val(data.start_date);
    $("#activity_end_date").val(data.end_date);
    $("#activity_start_time").val(data.start_time);
    $("#activity_end_time").val(data.end_time);

    if(is_doctor){
        if(ACTIVITY_UPDATE.office_id.length == 1){
            $("#activity_office").val(ACTIVITY_UPDATE.office_id[0]).selectpicker('refresh');
        }else{
            $("#activity_office").val("").selectpicker('refresh');
        }
    }else{
        if(IS_OWNER){
            $("#office_all").html("TODOS LOS CONSULTORIOS");
            $("#activity_office").prop("disabled",false);
            if(ACTIVITY_UPDATE.office_id.length > 1){
                $("#activity_office").val("all");
            }else if(ACTIVITY_UPDATE.office_id.length == 1){
                $("#activity_office").val(ACTIVITY_UPDATE.office_id[0])
            }else{
                swal("Error!", "Error: Hay un problema para determinar el consultorio", "error");
            }
        }else{
            $("#office_all").html("NO PUEDE EDITAR ESTE DATO");
            $("#activity_office").val("all");
            $("#activity_office").prop("disabled",true);
        }
        $("#activity_office").selectpicker("refresh");
    }

    if(parseInt(ACTIVITY_UPDATE.activity_type_id) == A_TYPE.surgery){
        $('#personal_supports').selectpicker('val', ACTIVITY_UPDATE.personal_supports);
        $('#add_personal_support_div').show();
    }else{
        $('#add_personal_support_div').hide();
    }
    if(parseInt(ACTIVITY_UPDATE.activity_type_id) == A_TYPE.appointment){
        $("#patient_div").show();
        $("#activity_patient_ts_autocomplete").val(ACTIVITY_UPDATE.patient_id);
        $("#activity_patient").val(ACTIVITY_UPDATE.patient_name);
    }else{
        $("#patient_div").hide();
        $("#activity_patient_ts_autocomplete").val("");
        $("#activity_patient").val("");
    }


    $("#all_day_activity").prop("checked",ACTIVITY_UPDATE.allDay);
    if(ACTIVITY_UPDATE.allDay){
        $("#activity_start_time").parent().hide();
        $("#activity_end_time").parent().hide();
    }else{
        $("#activity_start_time").parent().show();
        $("#activity_end_time").parent().show();
    }
    $(".event-tag span").removeClass("selected");
    $(".event-tag ."+ACTIVITY_UPDATE.color).addClass('selected');

    updateStatusSelect(ACTIVITY_UPDATE.activity_type_id);
    $("#activity_status").val(ACTIVITY_UPDATE.activity_status_id).selectpicker('refresh');


    $("#addEvent").html("Actualizar");
    $("#addEventTitle").html("Editar Actividad");

    $('#addNew-event').modal('show');
}

function addActivity(){
    var validForm = true;
    $.each(inputsValidate,function(idx,name){
        if($(name).val() == ""){
            $(name).closest('.form-group').addClass('has-error');
            validForm = false;
        }
    });

    var activity_type = $('.event-tag > span.selected').attr('activity-type');
    var activity_status = $("#activity_status").val();

    if(!validForm){
        swal("Oops! Parece que hay campos sin llenar.");
        return;
    }
    if(activity_type == undefined){
        swal("Por favor seleccione un tipo de Actividad!");
        return;
    }
    if(activity_status == ""){
        swal("Por favor seleccione un estado para la Actividad!");
        return;
    }


    var start_date = $("#activity_start_date").val();
    var end_date  = $("#activity_end_date").val();
    var start_time = $("#activity_start_time").val();
    var end_time  = $("#activity_end_time").val();

    var startDate = getDate(start_date,start_time);
    var endDate = getDate(end_date,end_time);

    if(validate_rank_date_appointment(startDate,endDate)){
        swal({
            title:'Horario ocupado ',
            text: "¿Esta seguro que desea tener mas de una actividad en el mismo horario?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Si, acepto',
            cancelButtonText: 'No, cerrar!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                var name = $('#activity_name').val();
                var details = $("#activity_details").val();
                var all_day = $("#all_day_activity").is(":checked");
                var office_id = $("#activity_office").val();
                var patient_id = $("#activity_patient_ts_autocomplete").val();

                var activity_surgery = 2;
                var personal_selected = [];
                if(parseInt(activity_type) == activity_surgery){
                    personal_selected = $('#personal_supports').val();
                }

                if(is_doctor){

                }else{//assistant is trying to create an actitivty
                    if(office_id == "all"){
                        office_id = assistant_permissions.create;
                    }
                }

                var params = {
                    name:name,
                    doctor_id:DOCTOR_ID_SELECTED,
                    details:details,
                    start_date:startDate,
                    end_date:endDate,
                    all_day:all_day,
                    activity_type_id:activity_type,
                    activity_status_id:activity_status,
                    personal_supports: personal_selected
                };
                var method = "POST";
                var url = "/doctor/activities/";

                if(ACTIVITY_ACTION != "add"){
                    method = "PUT";
                    url += ACTIVITY_UPDATE.id;
                    $(calendar_id).fullCalendar('removeEvents', ACTIVITY_UPDATE.id);
                }else{
                    ACTIVITY_UPDATE.id = 0;
                }

                $.ajax({
                    url:url,
                    dataType:"json",
                    method:method,
                    data:{
                        activity:params,
                        office_id:office_id,
                        is_owner:"true",
                        patient_id:patient_id,
                        doctor_id:$("#doctor_id").val()
                    },
                    success:function(act){
                        show_schedule(DOCTOR_ID_SELECTED,DOCTOR_NAME);
                    }
                });

                resetFormAddActivity();
                $('#addNew-event').modal('hide');
            }
        });
        //return;

    }else{
        var name = $('#activity_name').val();
        var details = $("#activity_details").val();
        var all_day = $("#all_day_activity").is(":checked");
        var office_id = $("#activity_office").val();
        var patient_id = $("#activity_patient_ts_autocomplete").val();

        var activity_surgery = 2;
        var personal_selected = [];
        if(parseInt(activity_type) == activity_surgery){
            personal_selected = $('#personal_supports').val();
        }

        if(is_doctor){

        }else{//assistant is trying to create an actitivty
            if(office_id == "all"){
                office_id = assistant_permissions.create;
            }
        }

        var params = {
            name:name,
            details:details,
            start_date:startDate,
            end_date:endDate,
            all_day:all_day,
            activity_type_id:activity_type,
            activity_status_id:activity_status,
            personal_supports: personal_selected
        };
        var method = "POST";
        var url = "/doctor/activities/";

        if(ACTIVITY_ACTION != "add"){
            method = "PUT";
            url += ACTIVITY_UPDATE.id;
            $(calendar_id).fullCalendar('removeEvents', ACTIVITY_UPDATE.id);
        }else{
            ACTIVITY_UPDATE.id = 0;
        }

        $.ajax({
            url:url,
            dataType:"json",
            method:method,
            data:{
                activity:params,
                office_id:office_id,
                is_owner:"true",
                patient_id:patient_id,
                doctor_id:$("#doctor_id").val()
            },
            success:function(act){
                show_schedule(DOCTOR_ID_SELECTED,DOCTOR_NAME);
            }
        });

        resetFormAddActivity();
        $('#addNew-event').modal('hide');
    }

    /*if(activity_type == A_TYPE.appointment && $("#activity_patient_ts_autocomplete").val() == ""){
     swal("Por favor seleccione un paciente!");
     return;
     }*/


}



function deleteActivity(){
    var method = "DELETE";
    var url = "/doctor/activities/";
    url += ACTIVITY_UPDATE.id;
    $.ajax({
        url:url,
        dataType:"json",
        method:method,
        data:{id:ACTIVITY_UPDATE.id},
        success:function(act){
            $("#activity_operation").modal("hide");
            swal("Completado","Se ha eliminado el registro correctamente.","success");
        }
    });
}

function updateStatusSelect(activity_type){
    $(".activity_status").hide();
    $(".option_status_"+activity_type).show();
    $("#activity_status").val("").selectpicker('refresh');
}

function delete_activity_modal(){
    $("#activity_operation").modal("hide");
    deleteActivity();
    show_schedule(DOCTOR_ID_SELECTED,DOCTOR_NAME);
    swal("Eliminado!", "Se ha eliminado la actividad", "success");
}
function update_activity_modal(){
    $("#activity_operation").modal("hide");
    ACTIVITY_ACTION = "update";
    setupUpdateModal();
}

function changeMainCalendarView(date,view){
    selected_view_main_calendar = view;
    selected_view_main_calendar_doctor = view;
    var conf = {
        view:view,
        date:date
    };

    if(conf.date == ''){
        conf.date = $(calendar_id).fullCalendar( 'getDate' ).toDate();
    }

    if(conf.view == ''){
        //options.view = $(calendar_id).fullCalendar( 'getView' ).type;
    }

    if(is_doctor){
        loadDoctorActivities(conf,DOCTOR_ID_SELECTED);
    }else{
        loadAssistantActivities(conf);
    }

}

function initMainCalendar(calendar_id,options){
    var defaultView = "month";
    var fc_height = "auto";
    var minTime = "06:00:00";
    var maxTime = "23:00:00";
    var events = [];


    $(calendar_id).fullCalendar("destroy");

    if(options != undefined && options.minTime != undefined){
        minTime = options.minTime;
    }

    if(options != undefined && options.maxTime != undefined){
        maxTime = options.maxTime;
    }

    if(options != undefined && options.view != undefined){
        defaultView = options.view;
        if(options.view == "month"){
            fc_height = 700;
        }

    }

    if(parseInt($("body").css("width")) < 480){
        defaultView = 'agendaDay';
        fc_height = 600;
        $("#small_calendar").parent().remove();
    }

    if(options != undefined && options.activities != undefined){
        events = options.activities;
    }

    $(calendar_id).fullCalendar({
        eventLimit: 5,
        eventLimitText:"más",
        height: fc_height,
        minTime:minTime,
        maxTime:maxTime,
        allDayText: 'Todo el día',
        defaultView:defaultView,
        monthNames:fcMonthNames,
        dayNames:fcDayNames,
        dayNamesShort:fcDayNamesShort,
        header: {
            right: '',
            center: 'title',
            left: 'prev,next'
        },
        //theme: true, //Do not remove this as it ruin the design
        selectable: true,
        selectHelper: true,
        editable: true,
        events: events,
        select: function(start, end, allDay) {
            ACTIVITY_UPDATE.id = null;
            ACTIVITY_ACTION = "add";
            resetFormAddActivity();
            $('#add_personal_support_div').hide();
            $('#addNew-event').modal('show');

            var ed = end.toDate();
            $("#activity_start_date").val(dateToString(ed,"dd/mm/yyyy"));
            $("#activity_end_date").val(dateToString(ed,"dd/mm/yyyy"));
        },
        eventRender: function(event, element) {
            var str_data = getPopoverHtml(event);
            element.popover({
                title: event.title,
                placement: 'top',
                trigger:"hover",
                content: str_data,
                html:true
            });
        },
        eventClick: function(calEvent, jsEvent, view) {
            ACTIVITY_ACTION = "update";
            loadFullActivity(calEvent.id);
        },
        eventDrop:function(event,dayDelta, minuteDelta, allDay, revertFunct){
            updateActivityDate(event,dayDelta);
        },
        viewRender:function(view, element){
            if(is_doctor){
                //loadDoctorActivities();
            }else{
                //loadAssistantActivities();
            }

            /**if(view.type == "agendaDay" || view.type == "agendaWeek"){
                $(calendar_id).fullCalendar('option', 'height', 700);
            }**/
        }
    });

    if(options != undefined && options.date != undefined){
        $(calendar_id).fullCalendar( 'gotoDate', options.date );
    }

    $(calendar_id).find(".fc-toolbar").find(".fc-center").find("h2").attr("onclick","openChangeDateModal()");
    $(calendar_id).find(".fc-toolbar").find(".fc-center").find("h2").css("cursor","pointer");

    $(calendar_id).find(".fc-toolbar").find(".fc-left").find("button").addClass("btn");
    $(calendar_id).find(".fc-toolbar").find(".fc-left").find("button").css("margin-left","1px");

    $(calendar_id).find(".fc-toolbar").find(".fc-right").append("<button onclick='changeMainCalendarView(\"\",\"month\")'>Mes</button>");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").append("<button onclick='changeMainCalendarView(\"\",\"agendaWeek\")'>Semana</button>");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").append("<button onclick='changeMainCalendarView(\"\",\"agendaDay\")'>Día</button>");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").find("button").addClass("btn");
    $(calendar_id).find(".fc-toolbar").find(".fc-right").find("button").css("margin-left","1px");


    $(calendar_id).find(".fc-toolbar").css("background","rgb(0, 188, 212)");



    $(calendar_id).find(".fc-toolbar").find(".fc-left").find(".fc-prev-button").click(function(){
        var options = {
            view:$(calendar_id).fullCalendar("getView").type,
            date:$(calendar_id).fullCalendar("getDate").toDate()
        };
        if(is_doctor){
            loadDoctorActivities(options,DOCTOR_ID_SELECTED);
        }else{
            loadAssistantActivities(options);
        }
    });

    $(calendar_id).find(".fc-toolbar").find(".fc-left").find(".fc-next-button").click(function(){
        var options = {
            view:$(calendar_id).fullCalendar("getView").type,
            date:$(calendar_id).fullCalendar("getDate").toDate()
        };

        if(is_doctor){
            loadDoctorActivities(options,DOCTOR_ID_SELECTED);
        }else{
            loadAssistantActivities(options);
        }
    });
}



function initSmallCalendar(){
    $("#small_calendar_modal").fullCalendar({
        eventLimit: 5,
        eventLimitText:"más",
        height: 200,
        //minTime:minTime,
        //maxTime:maxTime,
        allDayText: 'Todo el día',
        defaultView:'month',
        monthNames:fcMonthNames,
        dayNames:fcDayNames,
        dayNamesShort:fcDayNamesShort,
        header: {
            right: '',
            center: 'title',
            left: 'prev,next'
        },
        //theme: true, //Do not remove this as it ruin the design
        selectable: true,
        selectHelper: true,
        editable: true,
        events: [],
        select: function(start, end, allDay) {
            $("#change_date_modal").modal("hide");
            $(calendar_id).fullCalendar('gotoDate',end.toDate());
            $(calendar_id).fullCalendar('changeView','agendaDay');
            selected_view_main_calendar = "agendaDay";
            selected_view_main_calendar_doctor = "agendaDay";
        },
        eventRender: function(event, element) {

        },
        eventClick: function(calEvent, jsEvent, view) {

        },
        eventDrop:function(event,dayDelta, minuteDelta, allDay, revertFunct){

        },
        viewRender:function(view, element){

        }
    });
}
