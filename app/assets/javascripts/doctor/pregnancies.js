jQuery(document).ready(function() {
    $("#month_start").val($('#current_month').val()).change();
    $("#month_end").val($('#current_month').val()).change();
    $("#year").val($('#current_year').val()).change();

    var fecha_actual = new Date();
    $('#mes').val(fecha_actual.getMonth()+1);
    $('#mes').selectpicker("refresh");
    $('#anio').val(fecha_actual.getFullYear());
    $('#anio').selectpicker("refresh");
    $('#anio').change(function(){
        reload();
    });
    $('#mes').change(function(){
        reload();
    });
    var grid = $("#data-table-pregnancies").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        labels: {
            search: "Paciente"
        },
        url: function()
        {
            return $('#data-table-pregnancies').data('source')+"?month_start="+$('#month_start').val()+"&month_end="+$('#month_end').val()+"&year="+$('#year').val();
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar la vacuna "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "La vacuna ha sido eliminado exitosamente.", "success");
                        location.reload();
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });




});

function exportPdf(){
    if(parseInt($('#month_start').val()) > parseInt($('#month_end').val())){
        // error
        $('#error_month').show();
    }else{
        $('#error_month').hide();
        var url =  $('#url_print_report').val()+".pdf?month_start="+$('#month_start').val()+"&month_end="+$('#month_end').val()+"&year="+$('#year').val();
        window.open(
            url,
            '_blank'
        );
    }
}
function searchPreganancies(){
    reload();
}
function reload(){
    $("#data-table-pregnancies").bootgrid("reload");
}