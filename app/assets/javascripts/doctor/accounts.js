

$(function(){
    $("#account_bank_id").change(updateAccountNumberLabel);
    if($('#account_balance').length){
        $('#account_balance').mask('0,000,000.00', {reverse: true});
    }
});

jQuery(document).ready(function() {
    $("#accounts_div").hide();
    $("#user_name").ts_autocomplete({
        url:AUTOCOMPLETE_PERSONS_URL,
        items:10,
        callback:function(val, text){
            resetChecks();
          loadAccounts(val);
        }
    });
    updateAccountNumberLabel();
    $('form#new_account').find("input[type=text], textarea").val("");
    $("#new_account").validate();
    var table = $("#data-table-accounts");
    var grid = $("#data-table-accounts").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $("#data-table-accounts").data("source"),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                var options =  "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
                if(row.print_account_balance){
                    options += "<a onclick='openDialogPrintAccountBalance("+row.id+")' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-print\"><i class=\"zmdi zmdi-print\" title=\"Imprimir Estado de Cuenta\"></i></a>";
                }
                return options;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar la cuenta "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "La cuenta se ha eliminado exitosamente.", "success");
                        location.reload();
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

});

function confirmDeleteAccount(id){
    swal({
        title: "Eliminar Cuenta",
        text: "¿Confirma que desea eliminar la cuenta?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
        confirmButtonClass:"btn btn-danger",
        closeOnConfirm: false,
        closeOnCancel: true
    }, function(isConfirm){
        if (isConfirm) {

            $.ajax({
                url:"/doctor/accounts/"+id,
                method:"DELETE",
                dataType:"json",
                success:function(resp){
                    if(resp.done){
                        $("#account_"+id).remove();
                        swal("Eliminado!", "Se ha eliminado la cuenta sin ningún problema.", "success");
                    }else{
                        swal("Ocurrió un problema!", "No se pudo eliminar la cuenta, por favor contacte al administrador.", "error");
                    }

                }
            });
        } else {
        }
    });

}

function updateAccountNumberLabel(){
    var b = $("#account_bank_id").val();
    var type = $("#bank_"+b).attr("type");
    if(type == "c"){
        $("#account_account_number").attr("placeholder","Nombre de la Caja");
        $("#account_numero_cuenta").html("Nombre de la Caja");
        $("#account_clabe").parent().parent().hide();

        $("#office_account").parent().show();

    }else{
        $("#account_account_number").attr("placeholder","Número de Cuenta");
        $("#account_numero_cuenta").html("Número de Cuenta");
        $("#account_clabe").parent().parent().show();
        $("#office_account").parent().hide();
    }
}

PRINT_ACCOUNT_BALANCE_ID = 0;
function openDialogPrintAccountBalance(id){
    PRINT_ACCOUNT_BALANCE_ID = id;
    $("#print_account_balance").modal("show");
}

function printAccountBalance(){
    var year = $("#account_balance_year").val();
    var month = $("#account_balance_month").val();
    var url = $('#url_print_account').val();
    $("#print_account_balance").modal("hide");
    window.open(url+"?id="+PRINT_ACCOUNT_BALANCE_ID+"&month="+month+"&year="+year);
}

function check_office(){
   /* if($('#office_account').val()!=null){
        return true;
    }else {
        swal("Error!", "Para una cuenta de tipo caja, es necesario seleccionar un consultorio.", "error");
        return false;
    }*/
   return true;
}
function resetChecks() {
    $.each($(".s_parent"),function(idx, item){
        if($(item).is(":checked")){
            $(item).prop('checked', false);
        }
    });
}
function saveAccounts(){
    var accounts = [];
    $.each($(".s_parent"),function(idx, item){
        if($(item).is(":checked")){
            accounts.push({
                account_id:$(item).val()
            });
        }
    });
    if (accounts.length == 0){
        swal("Error!", "Debe seleccionar un usuario y las cuentas a las cual tendra acceso.", "error");
        return;
    }
 $.ajax({
        url: $("#url_save_user_accounts").val(),
        dataType: "json",
        data: {user_id:$("#user_name_ts_autocomplete").val(),accounts:accounts},
        success: function (data) {
            swal("Exito!", "Las cuentas han sido guardadas.", "success");
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function loadAccounts(user){
    $.ajax({
        url: $("#url_get_user_accounts").val(),
        dataType: "json",
        data: {user_id:user},
        success: function (data) {
            $("#accounts_div").show();
            $.each(data, function (idx, info) {
                $("#"+info.name).prop('checked', true);
            });

        },
        error:function(data){
            console.log("error controller");
        }
    });
}