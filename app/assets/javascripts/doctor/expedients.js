//= require highstock
//= require highstock/highcharts-more
//= require highstock/themes/sand-signika

//= require doctor/expedients_medical_history
//= require doctor/expedients_systems
//= require doctor/expedients_consultations
//= require doctor/expedients_vaccine_books
//= require doctor/expedients_clinical_studies
//= require doctor/expedients_vital_signs
//= require doctor/expedients_odontology
//= require croppie/croppie

$(document).ready(function(){
    if($("a.section_menu")[0]){
        var url = $("a.section_menu").first().attr("load-url");
        var section = $("a.section_menu").first().attr("section");
        loadMainContent(section,url);
    }


    var $uploadCrop;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $uploadCrop = $('#crop-holder').croppie({
        viewport: {
            width: 200,
            height: 200,
        },
        boundary: {
            width: 300,
            height: 300
        }
    });

    $('#patient_image').on('change', function () { readFile(this); });
    $('#set_picture').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'original'
        }).then(function (resp) {
            $("#patient_base64").val(resp);
            $("#preview_image_patient").attr("src",resp);
            $("#preview_image_patient").show();
            $("#select_from_file").hide();
            $("#select_from_camera").hide();
        });
    });
});

$(function () {
    $("#sections_menu .section_menu").click(function(){
        var url = $(this).attr("load-url");
        var section = $(this).attr("section");
        loadMainContent(section, url);
    });

    $("#change_profile_pic_patient").click(function () {
        $("#change_patient_picture_modal").modal("show");
    });

    function init_camera() {
        var video = document.getElementById('video');
        if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            // Not adding `{ audio: true }` since we only want video now
            navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                videoStream = stream;
                video.src = window.URL.createObjectURL(stream);
                video.play();
            });
        }
    }

    function take_photo() {
        // Elements for taking the snapshot
        var canvas = document.getElementById('canvas');
        console.log(canvas);
        var context = canvas.getContext('2d');
        console.log(context);
        var video = document.getElementById('video');

        $("#select_from_camera").hide();
        $("#preview_image_patient").show();


        context.drawImage(video, 0, 0, 400, 300);

        var base64Img = canvas.toDataURL();
        console.log(base64Img);

        $("#patient_base64").val(base64Img);
        $("#preview_image_patient").attr("src",base64Img);
    }

    $(".select_new_picture").click(function () {
        $("#preview_image_patient").hide();
        var source = $(this).attr("source");
        if(source == "file"){
            $("#select_from_file").show();
            $("#select_from_camera").hide();
        }else if(source == "photo"){
            init_camera();
            $("#select_from_file").hide();
            $("#select_from_camera").show();
        }
    });

    $("#take_photo").click(take_photo);

    $("#save_new_picture").click(function () {
        var url = $(this).attr("url");
        $.post(url,{patient_id: $("#patient_id").val(), patient_picture:$("#patient_base64").val()},function (data) {
            if(data.status == "ok"){
                location.reload();
            }
        },"json");
    });

});

function loadMainContent(section,url){
    $.ajax({
        url:url,
        success:function(html){
            $("#expedient_content").html(html);
            switch (section){
                case "medical_history":
                    initComponentsMedicalHistoryUI();
                    initGraphPercentile();
                    initTemplateComponents();
                    initOdontology();
                    break;
                case "systems":
                    initComponentsSystemsUI();
                    initTemplateComponents();
                    break;
                case "consultations":
                    initComponentsConsultationsUI();
                    loadConsultationsList();
                    initTemplateComponents();
                    break;
                case "hospitalizations":
                    break;
                case "vaccine_books":
                    initComponentsVaccineBooksUI();
                    initTemplateComponents();
                    break;
                case "vital_signs":
                    initComponentsVitalSign();
                    break;
                case "clinical_studies":
                    initComponentsClinicalStudiesUI();
                    searchLastAnalysisOrder();
                    initTemplateComponents();
                    break;
                default:
                    break;
            }
        }
    });
}

function initTemplateComponents(){
    $(".selectpicker").selectpicker("refresh");
    $('.date-picker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $('.date-time-picker').datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY hh:mm a'
    });
}
function open_modal_new_cost(consultation_id,cost){
    $("#new_consultation_cost").val(cost);
    $('#consultation_id_for_update').val(consultation_id);
    $('#change_consultation_cost_modal').modal('show');
}
function update_consultation_cost(){
    consultation_id  = $('#consultation_id_for_update').val();
    if($('#new_consultation_cost').val()==""){
        swal("Error!", "El campo no puede estar vacio.", "error");
    }else {
        $.ajax({
            url: $('#url_update_consult_cost').val(),
            dataType: "json",
            data: {consultation_id: consultation_id, new_price: $('#new_consultation_cost').val()},
            success: function (data) {
                loadConsultationsList();
                $('#change_consultation_cost_modal').modal('hide');
            },
            error: function (data) {
            }
        });
    }
}