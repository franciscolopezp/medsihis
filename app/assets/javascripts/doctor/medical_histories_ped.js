// medical_histories.js
// metodo showInfoMedicalHistory();
// ahi se llama a este metodo

var PED_DIV = "#features_ped";

function initComponentsPed(){
    $("#perinatal_information_city_id").ts_autocomplete({
        url:AUTOCOMPLETE_CITIES_URL,
        in_modal:false,
        items:10,
        required:true,
        model:"una ciudad"
    });

    $("#perinatal_information_birthdate").datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $("#parent_birthdate").datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $("#parent_academic_grade").selectpicker();

    if($(".data_Padre").length > 0){
        $('#addparentbtn').hide();
    }

    if($(".data_Madre").length > 0){
        $('#addmotherbtn').hide();
    }



}

function showPedHistories(){
    var url = "/doctor/medical_ped_histories/?medical_history_id="+$("#medical_history_id").val();
    $.ajax({
        type: "get",
        url: url,
        dataType:'html',
        success: function (response) {
            $(PED_DIV).empty();
            $(PED_DIV).html(response);
            initComponentsPed();
            initGraphPercentile();
        },
        error: function (error) {
        }
    });

}

function savePerinatalInfo(){
    if(!$.isNumeric($("#perinatal_information_weight").val())){
        swal("Por favor ingrese un numero valido en 'Peso al nacer'");
        return;
    }

    if(!$.isNumeric($("#perinatal_information_height").val())){
        swal("Por favor ingrese un numero valido en 'Altura al nacer'");
        return;
    }


    var city_id = $("#perinatal_information_city_id_ts_autocomplete").val();

    if(city_id == 'undefined' || city_id == ''){
        swal("Por favor selecciona una ciudad válida");
        return;
    }
    var params = {
        perinatal_information:{
            hospital:$("#perinatal_information_hospital").val(),
            city_id:$("#perinatal_information_city_id_ts_autocomplete").val(),
            weight:$("#perinatal_information_weight").val(),
            height:$("#perinatal_information_height").val(),
            pregnat_risks:$("#perinatal_information_pregnat_risks").val(),
            other_info:$("#perinatal_information_other_info").val()
        }
    };

    $("#errors_perinatal_information").hide();

    $.ajax({
        url:"/doctor/perinatal_informations/"+$("#perinatal_information_id").val(),
        method:"PUT",
        dataType:"json",
        data:params,
        success:function(data){
            if(data.errors != undefined){
                $("#errors_perinatal_information").show();
                var html_errors = "";
                for(var prop in data.errors){
                    html_errors += "<li>"+$("label[for='perinatal_information_"+prop+"']").html() +" " + data.errors[prop]+"</li>";
                }

                $("#errors_perinatal_information .error_log ul").html(html_errors);
            }else{
                $("#errors_perinatal_information").hide();
                $("#success_perinatal_information").show();

                setTimeout(function(){
                    $("#success_perinatal_information").hide();
                },2000);
            }
        },
        error:function(error){
        }
    });
}

function saveNonPathologicalInfo(){
    if(!$.isNumeric($("#child_non_pathological_info_number_people").val())){
        swal("Por favor ingrese un numero valido en 'Numero de Personas'");
        return;
    }
    var params = {
        child_non_pathological_info:{
            child_academic_grade:$("#child_non_pathological_info_child_academic_grade").val(),
            number_people:$("#child_non_pathological_info_number_people").val(),
            house_type:$("#child_non_pathological_info_house_type").val()
        }
    };

    $("#errors_child_non_pathological_info").hide();

    $.ajax({
        url:"/doctor/child_non_pathological_infos/"+$("#child_non_pathological_info_id").val(),
        method:"PUT",
        dataType:"json",
        data:params,
        success:function(data){
            if(data.errors != undefined){
                $("#errors_child_non_pathological_info").show();
                var html_errors = "";
                for(var prop in data.errors){
                    html_errors += "<li>"+$("label[for='child_non_pathological_info_"+prop+"']").html() +" " + data.errors[prop]+"</li>";
                }

                $("#errors_child_non_pathological_info .error_log ul").html(html_errors);
            }else{
                $("#errors_child_non_pathological_info").hide();
                $("#success_child_non_pathological_info").show();

                setTimeout(function(){
                    $("#success_child_non_pathological_info").hide();
                },2000);
            }
        },
        error:function(error){
        }
    });
}

function setupAddParent(gender){
    var title = gender == "Madre" ? "Datos de la Madre" :"Datos del Padre";
    $("#add_parent_title").html(title);
    $("#parent_gender").val(gender);
    $("#parent_id").val("");
    $("#add_parent").modal("show");
}

function setupEditParent(id, first_name, last_name, birth_date, academic_grade, blood_type, gender){
    $("#parent_id").val(id);
    $("#parent_first_name").val(first_name);
    $("#parent_last_name").val(last_name);
    $("#parent_gender").val(gender);
    $("#parent_birthdate").val(birth_date)
    $("#parent_blood_type_id").val(blood_type).selectpicker('refresh');
    $("#parent_academic_grade").val(academic_grade).selectpicker('refresh');


    var title = gender == "Madre" ? "Editar datos de la Madre" :"Editar datos del Padre";
    $("#add_parent_title").html(title);

    $("#add_parent").modal("show");

}
function validateParentInfo(){
    data_valid = false;

    if($("#parent_first_name").val() != "" &&
       $("#parent_last_name").val() != "" &&
       $("#parent_birthdate").ts_date() != ""){
        data_valid = true;
    }
    return data_valid;

}
function saveChildParent(){

    if(!validateParentInfo()){
        swal("Error!", "Nombre(s), apellidos y fecha de nacimiento son obligatorios.", "error");
        return;
    }
    var isAdd = true;
    var id = $("#parent_id").val();
    if(id != ""){
        isAdd = false
    }
    var params = {
        parent_info:{
            first_name:$("#parent_first_name").val(),
            last_name:$("#parent_last_name").val(),
            gender:$("#parent_gender").val(),
            birth_date:$("#parent_birthdate").ts_date(),
            blood_type:$("#parent_blood_type").val(),
            academic_grade:$("#parent_academic_grade").val(),
            child_non_pathological_info_id:$("#child_non_pathological_info_id").val()
        }
    };
    var url = $('#url_create_parent').val();
    //var url = "/doctor/parent_infos/?id="+id;
    var method = "POST";
    if(!isAdd){
        url = $('#url_update_parent').val()+"?id="+id;
        method = "PUT";
        //params.id = id;
    }
    $("#add_parent").modal("hide");
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(data){
            showPedHistories();
            if($("#parent_gender").val() == "Madre"){
                $('#addmotherbtn').hide();
            }else{
                $('#addparentbtn').hide();
            }


        }
    });
}
function deleteParent(id,gender){
    swal({
        title: "Eliminar "+gender,
        text: "¿Confirma que desea eliminar el registro?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelarr",
        confirmButtonClass:"btn btn-danger",
        cancelButtonClass:"btn",
        closeOnConfirm: false,
        closeOnCancel: true
    }, function(isConfirm){
        if (isConfirm) {
            swal("Eliminado!", "Se ha eliminado el registro sin ningún problema.", "success");
            var url = $('#url_destroy_parent').val();
            $.ajax({
                url:url,
                data: {id: id},
                method:"DELETE",
                dataType:"json",
                success:function(data){
                    $("#parent_"+id).remove();
                    if(data.data.gender == "Madre"){
                        $('#addmotherbtn').show();
                    }else{
                        $('#addparentbtn').show();
                    }

                }
            });
        } else {
        }
    });
}

