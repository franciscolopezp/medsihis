var TRANSACTION_TYPES = {
    charge:1,
    transfer:2,
    other:3,
    report:4
}

var CLOSE_DAILY_CASH_OPTIONS = {
    close:1,
    withdraw:2,
    transfer:3
}

var MOVEMENT_TYPES = {
    other:14
}

var PAYMENT_METHODS = {
    cash:1,
    transfer:3
}

var DAILY_CASH_ACCOUNT = 0;

$(function(){
    $("#open_dc_btn").click(openOpenDailyCashModal);
    $("#create_dc_btn").click(openDailyCash);
    $(".open_close_dc_modal_btn").click(function(){
        var account_id = $(this).attr("acnt");
        var daily_cash_id = $(this).attr("dcid");

        openCloseDailyCashWindow(daily_cash_id,account_id);
    });

    $("#add_transaction_btn").click(addTransaction);

    $("#transaction_payment_method").change(setupByPaymentMethod);
    $("#transaction_type").change(setupByTransactionType);

    $("#t_type").change(setupByPaymentMethod);
    $("#transaction_report").change(setReportPrice());


    $(".add_currency_amount").change(function(){
        var v = $(this).val();
        if($(this).is(":checked")){
            $("#de_holder_"+v).show();
        }else{
            $("#de_holder_"+v).hide();
        }
    });

    $(".close_dc_currency").change(function(){
        var v = $(this).val();
        if($(this).is(":checked")){
            $("#amount_target_holder_"+v).show();
        }else{
            $("#amount_target_holder_"+v).hide();
        }

        var operation = $("#final_transaction").val();
        if(operation == CLOSE_DAILY_CASH_OPTIONS.transfer){
            $("#target_close_dc_"+v).show();
        }else{
            $("#target_close_dc_"+v).hide();
        }
    });


    $("#final_transaction").change(function(){
        var operation = $(this).val();
        if(operation != CLOSE_DAILY_CASH_OPTIONS.close){
            $("#close_data").show();
        }else{
            $("#close_data").hide();
        }

        if(operation == CLOSE_DAILY_CASH_OPTIONS.transfer){
            $(".target_close_cd_holder").show();
        }else{
            $(".target_close_cd_holder").hide();
        }
    });
});

$(document).ready(function(){
    $('.default_exchange').prop('readonly', true);
    setupByPaymentMethod();
    setupByTransactionType();
    setupByCloseOption();

    var grid = $("#data-table-history-daily-cash").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url:function(){
            var fecha_cadena = "";
            var fecha_cadena_end = "";
            var start_date = $('#start_date').val();
            if(start_date == ""){
                fecha_cadena = "all"
            }else{
                var from = start_date.split("/");
                var f = new Date(from[2], from[1] - 1, from[0]);
                fecha_cadena = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+ f.getDate();
            }
            var end_date = $('#end_date').val();
            if(end_date == ""){
                fecha_cadena_end = "all"
            }else{
                var end = end_date.split("/");
                var fe = new Date(end[2], end[1] - 1, end[0]);
                fecha_cadena_end = fe.getFullYear()+"-"+(fe.getMonth()+1)+"-"+ (fe.getDate()+1);
            }
            return $('#data-table-history-daily-cash').data('source')+"?start_date="+fecha_cadena+"&end_date="+fecha_cadena_end;
        },

        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_show +'?prev=history'+ " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>"+
                    "<a href=" + row.url_print + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" target='_blank'><i class=\"zmdi zmdi-print\" title=\"Imprimir\"></i></a>";
            }
        }
    });
});

function setupByTransactionType(){
    var transaction_type = parseInt($("#transaction_type").val());
    var daily_cash_account = $("#daily_cash_account").val();

    $("#transaction_consult").parent().hide();
    $("#transaction_report").parent().hide();
    $("#t_type").parent().hide();
    $("#movement_type").parent().hide();
    $("#to_account").parent().hide();
    $("#transaction_account").val(daily_cash_account).selectpicker('refresh');
    $("#transaction_payment_method").val(PAYMENT_METHODS.cash).selectpicker('refresh');
    switch (transaction_type){
        case TRANSACTION_TYPES.charge:
            $("#t_type").val(1);
            $("#transaction_consult").parent().show();
            $("#transaction_payment_method").parent().show();
            loadConsults();
            break;
        case TRANSACTION_TYPES.transfer:
            $(".t_account").show();
            $("#to_account").parent().show();
            $("#transaction_payment_method").val(PAYMENT_METHODS.transfer).selectpicker('refresh');
            $("#transaction_payment_method").parent().hide();
            break;
        case TRANSACTION_TYPES.other:
            $("#t_type").parent().show();
            $("#movement_type").parent().show();
            $("#movement_type").val("").selectpicker('refresh');
            $("#transaction_payment_method").parent().show();

            setupByPaymentMethod();
            break;
        case TRANSACTION_TYPES.report:
            $("#transaction_report").parent().show();
            $("#t_type").val(1);
            $("#transaction_payment_method").parent().show();
            loadReports();
            break;
    }

    $("#t_type").selectpicker('refresh');
}

function openCloseDailyCashWindow(id,account_id){
    DAILY_CASH_ACCOUNT = account_id;
    $("#close_daily_cash_window").modal("show");
    $("#close_daily_cash_id").val(id);
}

function closeDailyCash(){
    var option = parseInt($("#final_transaction").val());
    var dc_id = $("#close_daily_cash_id").val();

    var amounts = [];
    var amount_error = false;

    $("input[name=close_dc_currency]:checked").map(function(){
        var v = $(this).val();
        var amount = $("#close_dc_currency_"+v).val();
        amounts.push({
            currency:v,
            amount: amount,
            target_account: $("#target_account_close_dc_"+v).val()
        });

        if(amount == 0){
            amount_error = true;
        }
    });

    if(option != CLOSE_DAILY_CASH_OPTIONS.close && amounts.length == 0){
        swal("No se puede llevar a cabo la operación","Seleccione al menos un tipo de moneda.","error");
        return;
    }

    if(option != CLOSE_DAILY_CASH_OPTIONS.close && amount_error){
        swal("No se puede llevar a cabo la operación","Ingrese un monto válido.","error");
        return;
    }

    switch (option){
        case CLOSE_DAILY_CASH_OPTIONS.close:
            break;
        case CLOSE_DAILY_CASH_OPTIONS.withdraw:
            break;
        case CLOSE_DAILY_CASH_OPTIONS.transfer:
            // verificar que se quiera transferir a cuentas diferentes
            var error_target_account = false;
            var error_currency_account = false;
            $.each(amounts,function(idx, a){
                if(a.target_account == DAILY_CASH_ACCOUNT){
                    error_target_account = true;
                }

                var option = $("#target_close_dc_option_"+a.target_account);
                if(option.attr("bank") == "bank" && option.attr("currency") != a.currency){
                    error_currency_account = true;
                }

            });

            if(error_target_account){
                swal("No se puede llevar a cabo la operación","Seleccione una caja diferente para hacer la transacción final.","error");
                return;
            }

            if(error_currency_account){
                swal("No se puede llevar a cabo la operación","Seleccione un banco con el mismo tipo de cambio.","error");
                return;
            }

            break
    }

    $("#close_daily_cash_btn").prop("disabled",true);

    $.ajax({
        url:$("#daily_cashes_url").val()+"/"+dc_id,
        method:"PUT",
        dataType:"json",
        data:{
            option:option,
            amounts:amounts,
            transaction_type_id:PAYMENT_METHODS.transfer,
            t_type:0,
            account_id:DAILY_CASH_ACCOUNT
        },
        success:function(data){
            if(data.done){
                window.open(data.url_print);
                $("#close_daily_cash_window").modal("hide");
                $("#dc_"+dc_id).remove();
            }else{
                $("#close_daily_cash_btn").prop("disabled",false);
                swal(data.error, data.message,"error");
            }
        }
    });

}

function setupByPaymentMethod(){
    var payment_method = $("#transaction_payment_method").val();
    var affect_local = $("#payment_method_"+payment_method).attr("affect_local");
    $(".t_account").show();
    if(affect_local == "true"){
        $(".bank_account").hide();
    }else{
        $(".local_account").hide();
    }

    var transaction_type = parseInt($("#transaction_type").val());
    var t_type = parseInt($("#t_type").val());
    if(TRANSACTION_TYPES.other == transaction_type && t_type == 0){
        $(".t_account").show();
    }

    $("#transaction_account").val("").selectpicker("refresh");
    $("#movement_type").val("");

    if(t_type == 1){
        $(".ingress").show();
        $(".egress").hide();
    }else{
        $(".ingress").hide();
        $(".egress").show();
    }

    $("#movement_type").selectpicker('refresh');

}

function setupByCloseOption(){
    var option = parseInt($("#final_transaction").val());
    $("#available_amount").parent().hide();
    $("#close_transfer_account").parent().hide();
    $("#close_amount").parent().hide();
    switch (option){
        case CLOSE_DAILY_CASH_OPTIONS.close:
            $("#close_transfer_account").parent().hide();
            $("#close_amount").parent().hide();
            $("#available_amount").parent().hide();
            break;
        case CLOSE_DAILY_CASH_OPTIONS.withdraw:
            $("#close_amount").parent().show();
            $("#close_transfer_account").parent().hide();
            $("#available_amount").parent().show();
            break;
        case CLOSE_DAILY_CASH_OPTIONS.transfer:
            $("#close_amount").parent().show();
            $("#close_transfer_account").parent().show();
            $("#available_amount").parent().show();
            break
    }
}

var force_transaction = false;
function addTransaction(){
    var t_type = $("#t_type").val();
    var transaction_type = parseInt($("#transaction_type").val());
    var source_account = $("#transaction_account").val();
    var target_account = $("#to_account").val()
    var daily_cash_account = $("#daily_cash_account").val();
    var details = $("#transaction_details").val();
    var movement_type = $("#movement_type").val();

    var option_source = $("#account_"+source_account);
    var option_target = $("#target_account_"+target_account);

    var amounts = [];

    var amount_error = false;
    var new_charge = 0;
    $("input[name=add_currency_amount]:checked").map(function(){
        var amount = $("#currency_input_"+$(this).val()).val();
        var rate = $("#doctor_currency_"+$(this).val()).attr("exchange");
        amounts.push({
            currency:$(this).val(),
            amount: amount
        });
        if(amount == 0){
            amount_error = true;
        }

        new_charge += (parseFloat(rate) * parseFloat(amount));

    });

    switch (transaction_type){
        case TRANSACTION_TYPES.charge:
            if($("#transaction_consult").val() == null){
                swal("Consulta no válida","Por favor seleccione una consulta válida.","error");
                return;
            }

            if(!force_transaction){
                var debt = parseFloat($("#consult_"+$("#transaction_consult").val()).attr("debt"));
                var charged = parseFloat($("#consult_"+$("#transaction_consult").val()).attr("charged"));
                var price = parseFloat($("#consult_"+$("#transaction_consult").val()).attr("price"));
                var charge_sum = new_charge + charged;
                if(debt < charge_sum){
                    var extra = charge_sum - price;
                    swal({
                            title: "Confirmar operación",
                            text: "El monto cobrado exede por "+toCurrency(extra)+". ¿Desea agregera el cobro de todos modos?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Confirmar",
                            closeOnConfirm: true },
                        function(){
                            force_transaction = true;
                            addTransaction();
                        });

                    return;
                }
            }

            break;
        case TRANSACTION_TYPES.transfer:
            if(source_account == target_account){
                swal("No puede hacer una transferencia a la misma cuenta","Por favor elija cuentas diferentes para poder realizar la transferencia.","error");
                return;
            }

            // banco a banco
            if(option_source.hasClass('bank_account') && option_target.hasClass('target_bank_account')){ // si la transferencia es de banco a banco
                if(option_source.attr('currency') == option_target.attr('currency')){ // checar que el tipo de cambio sea correcto
                    if(amounts.length != 1){ // checar que se quiera transferir unicamente un tipo de cambio
                        swal("No se puede realizar la transferencia","Debe seleccionar el tipo de cambio correcto.","error");
                        return;
                    }else{
                        var a = amounts[0];
                        if(a.currency != option_source.attr('currency')){ // checar que el tipo de cambio seleccionado coincide con el de los bancos
                            swal("No se puede realizar la transferencia","El tipo de cambio es incorrecto.","error");
                            return;
                        }
                    }

                }else{
                    swal("No se puede realizar la transferencia","Los tipos de cambio de los bancos no coinciden","error");
                    return;
                }
            }

            break;
        case TRANSACTION_TYPES.other:
            $("#t_type").parent().show();
            $("#movement_type").parent().show();
            if(movement_type == ""){
                swal("Concepto no válido","Por favor seleccione un concepto de la lista.","error");
                return;
            }
            break;
        case TRANSACTION_TYPES.report:
            if($("#transaction_report").val() == null){
                swal("Reporte no válido","Por favor seleccione un reporte válido.","error");
                return;
            }
            break;
    }

    // checar si alguna cuenta es de banco
    if(option_source.hasClass('bank_account') || option_target.hasClass('target_bank_account')){
        if(amounts.length != 1){ // checar que se quiera transferir unicamente un tipo de cambio
            swal("No se puede realizar la transferencia","Debe seleccionar el tipo de cambio correcto.","error");
            return;
        }else{
            var a = amounts[0];
            if(option_source.hasClass('bank_account') && a.currency != option_source.attr('currency')){ // checar que el tipo de cambio seleccionado coincide con el de los bancos
                swal("No se puede realizar la transferencia","El tipo de cambio es incorrecto.","error");
                return;
            }else if(option_target.hasClass('target_bank_account') && a.currency != option_target.attr('currency')){
                swal("No se puede realizar la transferencia","El tipo de cambio es incorrecto.","error");
                return;
            }
        }
    }

    if(amount_error){
        swal("Monto inválido","Por favor ingrese un monto válido","error");
        return;
    }

    if(source_account == ""){
        swal("Datos incompletos","Por favor seleccione una cuenta.","error");
        return;
    }

    if(transaction_type == TRANSACTION_TYPES.charge){

    }

    var data = {
        t_type:t_type,
        transaction_type_id:$("#transaction_type").val(),
        payment_method_id:$("#transaction_payment_method").val(),
        movement_type_id: movement_type,
        account_id:source_account,
        details: details,
        consult_id:$("#transaction_consult").val(),
        report_id:$("#transaction_report").val(),
        to_account_id:target_account
    };

    data.amounts = amounts;

    $("#add_transaction_btn").prop("disabled",true);
    $.ajax({
        url:$("#add_transaction_url").val(),
        data:data,
        dataType:"json",
        success:function(data){
            if(data.done){
                window.open(data.url,'_blank');
                location.reload();
            }else{
                $("#add_transaction_btn").prop("disabled",false);
                swal(data.error,data.message,"error");
            }
        }
    });

}

function loadConsults(){
    $.ajax({
        url:"/doctor/dashboard/pending_consult",
        dataType:"json",
        success:function(data){
            var html = "";
            $.each(data,function(idx,consult){

                var d = stringToDate(consult.date,"yyyy-mm-dd hh:mn")

                html += "<option id='consult_"+consult.id+"' charged='"+consult.charged+"' debt='"+consult.debt+"' price='"+consult.price+"' value='"+consult.id+"'>"+dateToString(d,"dd/fm/yyyy hh:mn pp")+" - "+ consult.patient+"</option>";

            });
            $("#transaction_consult").html(html);
            $("#transaction_consult").selectpicker('refresh');

            setPrice();
        }
    });
}

function loadReports(){
    $.ajax({
        url:"/doctor/dashboard/pending_report",
        dataType:"json",
        data:{office_id:[$("#current_office_id").val()]},
        success:function(data){
            var html = "";
            $.each(data,function(idx,report){

                var d = stringToDate(report.date,"yyyy-mm-dd hh:mn")

                html += "<option id='report_"+report.id+"' price='"+report.price+"' value='"+report.id+"'>"+report.study+" | "+dateToString(d,"dd/fm/yyyy")+" - "+ report.patient+"</option>";

            });
            $("#transaction_report").html(html);
            $("#transaction_report").selectpicker('refresh');
            setReportPrice();
        }
    });
}

function setReportPrice(){
    var select_val = $("#transaction_report").val();
    var price = $("#report_"+select_val).attr("price");
    $(".default_exchange").val(price);
}

function setPrice(){
    var select_val = $("#transaction_consult").val();
    var price = $("#consult_"+select_val).attr("price");
    $(".default_exchange").val(price);
}

function openOpenDailyCashModal(){
    $("#open_daily_cash_modal").modal("show");
}

function openDailyCash(){
    var account = $("#daily_cash_account").val();
    var start_balance = $("#start_balance").val();
    if(account == null || account == "0"){
        swal("Seleccionar Cuenta", "Por favor seleccione una cuenta válida.", "error");
        return;
    }

    $.ajax({
        url:$("#daily_cashes_url").val(),
        method:"POST",
        dataType:"json",
        data:{
            account_id:account
        },
        success:function(data){
            if(data.done){
                location.reload();
            }else{
                $("#add_transaction_btn").prop("disabled",false);
                swal(data.error,data.message,"error");
            }
        }
    });
}

function reload_daylies(){
    $("#data-table-history-daily-cash").bootgrid("reload");
}
function clear_history_filters(){
    $('#start_date').val("");
    $('#end_date').val("");
    $('#data-table-history-daily-cash').bootgrid('reload');
}
function openAddTransactionModal(){
    $("#transaction_type").val($("#transaction_type option:first").val()).selectpicker('refresh');
    force_transaction = false;
    $("#add_transaction_btn").prop("disabled",false);
    $("#add_transaction_window").modal("show");
    $("#transaction_amount").val("");
    $("#transaction_details").val("");
    $("#transaction_type").val(TRANSACTION_TYPES.charge);
    setupByTransactionType();
}

function resetPrice(){
    if($("#transaction_type").val()==1){
        $('.default_exchange').prop('readonly', true);
        setPrice();
    }
    else{
        $("#transaction_amount").val("");
        $('.default_exchange').prop('readonly', false);
    }
}