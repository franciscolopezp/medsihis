var DISEASES = new Array();
var MEDICAMENTS = new Array();
var DISEASE_SELECTED = {};
var MEDICAMENT_SELECTED = {};
var FLAG_UPDATE_HISTORY = false; // esta bandera se usa para actualizar el historial medico cuando se cambia de sección
var DELETE_VACCINATION_ID = "";
var APPLICATION_AGE_ID = 0;

function showInfoMedicalHistory(){
    FLAG_UPDATE_HISTORY = true;

    showLoading("medical-history");
    var url = $('#url_data_medical_history').val();
    $.ajax({
        type: "get",
        url: url,
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("medical-history",response);
            setDropDownDiseases();
            setDropDownMedicaments();
            addToSectionNoPathological();

            DISEASES.length = 0;
            //inicializa los arreglos de enfermedades y medicamentos
            if($('#disease_medical_history').length){
                DISEASES = jQuery.parseJSON($('#disease_medical_history').val());
                $.each(DISEASES,function( index,value ) {
                    addToTable('table-diseases',value);
                });
            }

            MEDICAMENTS.length = 0;
            if($('#medicaments_medical_history').length){
                var medicaments_aux = jQuery.parseJSON($('#medicaments_medical_history').val());
                $.each(medicaments_aux,function( index,value ) {
                    var format_object = {};
                    format_object["id"] = value["id"];
                    format_object["name"] = value["comercial_name"];
                    format_object["presentation"] = value["presentation"];
                    MEDICAMENTS.push(format_object);
                    addToTable('table-medicaments',format_object);
                });
            }

            /* Toggle between adding and removing the "active" and "show" classes when the user clicks on one of the "Section" buttons. The "active" class is used to add a background color to the current button when its belonging panel is open. The "show" class is used to open the specific accordion panel */
            var acc = document.getElementsByClassName("accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].onclick = function(){
                    this.classList.toggle("active");
                    this.nextElementSibling.classList.toggle("show");
                }
            }

         //   if($("#has_ped").val() == "true"){
         //       showPedHistories();
         //   }

            $("#medical_history_no_pathological_attributes_marital_status_id").selectpicker();
            $("#gynecologist_obstetrical_background_menses_last_date").datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $("#gynecologist_obstetrical_background_papanicolau_last_date").datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $("#gynecologist_obstetrical_background_fup").datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $("#gynecologist_obstetrical_background_relationship_type").selectpicker("refresh");

            // mostrar la información de la sección seleccionada
            // dentro del tap Antecedentes médicos
            // puede ser Datos generales Vacunas, Pediatria, Ginecologia
            //se carga la primera pagina que se encuentre visible
            var sectionId = $('#list_menu_medical_history .active a').data("section");
            showSectionMedicalHistory(sectionId);
            $('.main_menu_medical_history').unbind();
            $( ".main_menu_medical_history" ).click(function() {
                //console.log($(this).data("section"));
                var sectionId = parseInt($(this).data("section"));
                showSectionMedicalHistory(sectionId);
            });

            initComponentSystems();
            $(".date-picker").datetimepicker({
                format: 'DD/MM/YYYY'
            });


        },
        error: function (error) {
            swal("Error!", "Error al cargar la sección de historial clinico", "error");
        }
    });
}

function showSectionMedicalHistory(sectionId){

    SECTIONS_MEDICAL_HISTORY["general_data"] = 0;
    SECTIONS_MEDICAL_HISTORY["vaccines"] = 1;
    SECTIONS_MEDICAL_HISTORY["pediatrics"] = 2;
    SECTIONS_MEDICAL_HISTORY["gynecology"] = 3;
    switch (parseInt(sectionId)) {
        case SECTIONS_MEDICAL_HISTORY["general_data"]:
            // no se descarga nada del servidor por que ya trae la información en el tap
            break;
        case SECTIONS_MEDICAL_HISTORY["vaccines"]:
            loadVaccineBooks($('#patient_id').val());
            break;
        case SECTIONS_MEDICAL_HISTORY["pediatrics"]:
            // no se descarga nada del servidor por que ya trae la información en el tap
            generateGraphLength();
            showPedHistories();
            break;
        case SECTIONS_MEDICAL_HISTORY["gynecology"]:
            // no se descarga nada del servidor por que ya trae la información en el tap
            break;
        default:
            break;
    }
}

function addToSectionNoPathological(){
    $('.no_pathological_check').change(function(){
        showAndHideInputsMedicalHistory($(this).attr("id"));
    });

    //inicializar los input
    $('.no_pathological_check').each(function( index ) {
        showAndHideInputsMedicalHistory($(this).attr("id"));
    });
}

function showAndHideInputsMedicalHistory(idElement){
    var id = "#"+idElement;
    var id_text = id+"_description";
    if($(id).is(':checked')){
        //mostrar campo
        if(id != "#family_disease_other_diseases"){
            $(id_text).prop( "disabled", false );
        }else{
            $('#menu_diseases_history').show("slow");
        }

    }else{
        if(id != "#family_disease_other_diseases"){
            $(id_text).val("");
            $(id_text).prop( "disabled", true );
        }else{
            // eliminar arreglo de enfermedades y eliminarlo de la vista
            $('#menu_diseases_history').hide("slow");
        }
    }
}
function loadVaccineBooks(patient_id){
    $.ajax({
        type: "get",
        url: $('#url_load_vaccinebooks').val(),
        data: {patient:patient_id },
        success: function (response) {
            //$('#vaccine_section').hide();
            $('#vaccine_section').html(response);
           /* $('#medical_consultation_vital_sign_attributes_imc').val(response.imc_real);
            $('#medical_consultation_vital_sign_attributes_suggested_weight').val(response.value_suggested_weight);
            $('#table_imc_consultation').show("slow");*/
        },
        error : function (response) {
            console.log("Error al cargar cartilla(s) de vacunación");
        }
    });
}
function setModalVaccination(application_id){
    APPLICATION_AGE_ID = application_id;
    $("#vaccinate_date_picked").val("");
}
function deleteBook(book_id,book_name){
    swal({
        title:'Eliminar cartilla ' +book_name,
        text: "¿Esta seguro que desea eliminar la cartilla de "+book_name+"?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Si, aceptar',
        cancelButtonText: 'No, cerrar',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm){
        if (isConfirm) {
            $.ajax({
                url: $('#url_delete_vaccinebook_patient').val(),
                dataType: "json",
                data: {vaccinebook:book_id,
                    patient: $('#patient_id').val()},
                success: function (data) {
                    if(data.result){
                        swal("Confirmado!", "La cartilla fue eliminada", "success");
                        loadVaccineBooks($("#patient_id").val());
                    }
                    else {
                        swal("Error!", data.message, "error");
                    }

                }
            });

        } else {
            swal("Cancelado!", "Se canceló la operación.", "error");
        }
    });
}
function setVaccinateDate(){
    if($("#vaccinate_date_picked").val()==""){
        swal("Error!","Debe seleccionar una fecha de vacunación", "error");
        return;
    }
    var url = $("#url_add_vaccine_date").val();
    var method = "GET";

    var params = {
        patient_id:$("#patient_id").val(),
        vaccinate_date:$("#vaccinate_date_picked").val(),
        application_age_id:APPLICATION_AGE_ID,
    };

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(response){
            loadVaccineBooks($("#patient_id").val());
            $('#addVaccinateDateModal').modal('hide');
        }
    });
}
function addVacinePatient(){

}
function addVacineBookPatient(operation){
    if(operation == "show"){
        $("#addbookpatient").modal("show");
    }
    else{
        $.ajax({
            type: "get",
            url: $('#url_add_vaccinebook_patient').val(),
            data: {patient:$('#patient_id').val(),vaccinebook:$('#vaccine_book_select').val() },
            success: function (response) {
                if(response.result){
                    loadVaccineBooks($("#patient_id").val());
                    $('#addbookpatient').modal('hide')
                }else{
                    swal("Error!",response.message, "error");
                }


            },
            error : function (response) {
                console.log("erro" + response)
                console.log("Error al cargar cartilla(s) de vacunación");
            }
        });
    }
}
function setDropDownDiseases(){
    var url = $('#url_search_disease').val();
    $('.disease_search').autocomplete({
        source: url,
        minLength: 2,
        select: function( event, ui ) {
            set_disease_id(null, ui.item.id, ui.item.value)
        }
    });


    $('.disease_search').change(function() {
        if($('#disease_name').val() == ""){
            $('#disease_id').val("");
        }
    });
}

function set_disease_id(item, val, text) {
    DISEASE_SELECTED["id"] = val;
    DISEASE_SELECTED["name"] = text;
}

function addnewdiseasefromece(){
    $("#addnewdiseaseconfirmation").modal("hide");
    $("#addnewdiseasefromece").modal("show");
    $("#new_disease_name").val($("#disease_name").val());
    $("#new_disease_name").parent().addClass("fg-toggled");
    $("#new_disease_code").val("");

    $('#add_new_disease_ece_btn').unbind();
    $('#add_new_disease_ece_btn').click(function(){
        saveDiseaseFromEce();
    });
}

function saveDiseaseFromEce(){
    var disease_category_id = $("#new_disease_category_select").val();
    var disease_code = $("#new_disease_code").val();
    var disease_name = $("#new_disease_name").val();

    var url = $('#url_operation_disease').val();
    var method = "GET";
    var params = {
        code: disease_code,
        name: disease_name,
        disease_category_id:disease_category_id
    };

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(response){
            if(response.result){
                insertDiseaseToArray(jQuery.parseJSON(response.disease));
                $("#new_disease_code").val("");
                //$("#disease_name").val("");
                $("#addnewdiseasefromece").modal("hide");
                DISEASE_SELECTED = {}; // se reinicia el objeto
            }else{
                swal("Error!", "Problemas al agreger la enfermedad, consulte al administrador.", "error");
            }
        }
    });
}


function add_disease_mh(){
    var diseaseName = $('#disease_name').val();

    if(diseaseName != ""){
        if(DISEASE_SELECTED["name"] != null && diseaseName == DISEASE_SELECTED["name"]){
            if(!existElementInArray(DISEASES, DISEASE_SELECTED)){
                // existe la enfermedad, se agregar al erreglo global
                insertDiseaseToArray(DISEASE_SELECTED);
                DISEASE_SELECTED = {}; // se reinicia el objeto
            }else{
                swal("Error","La enfermedad ya fue agregada");
            }
        }else{
            // no se selecciono ninguna enfermedad de la lista, pero puede ser que si exista en BD
            var result = existElementInDB(diseaseName,$('#url_exist_disease').val());

            if(result != null){
                if(!existElementInArray(DISEASES, result)){
                    // if(!existElementInArray(DISEASES, DISEASE_SELECTED)){
                    // si existe en la BD, se agrega al arreglo global
                    insertDiseaseToArray(result);
                    DISEASE_SELECTED = {}; // se reinicia el objeto
                }else{
                    swal("Error","La enfermedad ya fue agregada");
                }
            }else{
                // No existe en la BD
                // la enfermedad no esta en el catalogo. Agregar nuevo
                $("#addnewdiseaseconfirmation").modal("show");
            }

        }
    }else{
        swal("Agregue una enfermedad");
    }
}

function setDropDownMedicaments(){
    var url = $('#url_search_medicament').val();

    $('.medicament_search').autocomplete({
        source: url,
        minLength: 2,
        select: function( event, ui ) {
            set_medicament_id(null, ui.item.id, ui.item.value)
        }
    });

    $('.medicament_search').change(function() {
        if($('#medicament_name').val() == ""){
            $('#medicament_id').val("");
        }
    });
}

function set_medicament_id(item, val, text) {

    MEDICAMENT_SELECTED["id"] = val;
    MEDICAMENT_SELECTED["name"] = text;

}

function add_medicament_mh(){

    var medicamentName = $('#medicament_name').val();

    if(medicamentName != ""){
        if(MEDICAMENT_SELECTED["name"] != null && medicamentName == MEDICAMENT_SELECTED["name"]){
            if(!existElementInArray(MEDICAMENTS, MEDICAMENT_SELECTED)){
                // existe el medicamento, se agregar al erreglo global
                insertMedicamentToArray(MEDICAMENT_SELECTED);
                MEDICAMENT_SELECTED = {}; // se reinicia el objeto
            }else{
                swal("Error","El medicamento ya fue agregado");
            }
        }else{
            // no se selecciono ningun medicamento de la lista, pero puede ser que si exista en BD
            var result = existElementInDB(medicamentName,$('#url_exist_medicament').val());

            if(result != null){
                var format_object = {};
                format_object["id"] = result["id"];
                format_object["name"] = result["comercial_name"];
                format_object["presentation"] = result["presentation"];

                if(!existElementInArray(MEDICAMENTS, format_object)){
                    // si existe en la BD, se agrega al arreglo global
                    insertMedicamentToArray(format_object);
                    MEDICAMENT_SELECTED = {}; // se reinicia el objeto
                }else{
                    swal("Error","El medicamento ya fue agregado");
                }
            }else{
                // No existe en la BD
                // la enfermedad no esta en el catalogo. Agregar nuevo
                $("#addnewmedicamentconfirmation").modal("show");
            }
        }
    }else{
        swal("Agregue un medicamento");
    }
}

function addnewmedicamentfromece(origin){
    $("#origin_add_medicament").val(origin);
    $("#force_add_medicament").val("no");
    $("#addnewmedicamentconfirmation").modal("hide");
    $("#addnewmedicamentfromece").modal("show");
    $("#new_medicament_comercial_name").val($("#medicament_name").val());
    $("#new_medicament_comercial_name").parent().addClass("fg-toggled");
    $("#new_medicament_presentation").val("");
    $("#new_medicament_active_substance").val("");
    $("#new_medicament_contraindication").val("");
    $("#new_medicament_brand").val("");

    if(origin == 'medical_history'){
        $("#new_medicament_dose_div").hide();
    }else{
        $("#new_medicament_dose").val("");
        $("#new_medicament_dose_div").show();
    }
}

function saveMedicamentFromEce(){
    var origin = $("#origin_add_medicament").val();
    var url = $('#url_operation_medicament').val();
    var dose = $("#new_medicament_dose").val();
    var method = "GET";
    var params = {
        force:$("#force_add_medicament").val(),
        comercial_name: $('#new_medicament_comercial_name').val(),
        presentation: $('#new_medicament_presentation').val(),
        active_substance:$('#new_medicament_active_substance').val(),
        contraindication: $('#new_medicament_contraindication').val(),
        brand: $('#new_medicament_brand').val()
    };

    var valid_form = true;

    var validate_fields = ["new_medicament_comercial_name","new_medicament_presentation","new_medicament_active_substance","new_medicament_contraindication","new_medicament_brand"];
    $.each(validate_fields,function(idx,id){
        if($("#"+id).val() == ""){
            valid_form = false;
        }
    });

    if(!valid_form){
        swal("Por favor complete todos los campos");
        return;
    }

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(response){
            if(response.result){
                var object = jQuery.parseJSON(response.medicament);
                if(origin == "medical_history"){
                    addRoutineMedicament(object["id"],object["comercial_name"]+ " ("+object["presentation"]+")");
                }else if(origin == "new_consult"){
                    $("#addnewmedicamentfromece").modal("hide");
                    consultation_data.medicaments.push({
                        id:object["id"],
                        name:object["comercial_name"] + " ("+object["presentation"]+")",
                        dose: dose
                    });
                    drawMedicamentsTable();
                }
            }else{

                swal({
                    title: "Existe el medicamento en nuestra base de datos. ¿Aún desea agregar el medicamento?",
                    text: "Nombre comercial: "+response.medicament.comercial_name+" Sustancia activa: "+response.medicament.active_substance+" Presentación: "+response.medicament.presentation,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, agregar registro",
                    cancelButtonText: "No, usar el existente",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function(isConfirm){
                    if (isConfirm) {
                        $("#force_add_medicament").val("yes");
                        saveMedicamentFromEce();
                    } else {

                        if(origin == "medical_history"){
                            addRoutineMedicament(response.medicament.id,response.medicament.comercial_name)
                        }else if(origin == "new_consult"){
                            $("#addnewmedicamentfromece").modal("hide");
                            //cal method declared in medical_consultation_new.js
                            addMedicamentToGrid(response.medicament.id,response.medicament.comercial_name,dose);
                        }

                    }
                });

            }
        }
    });
}

function addRoutineMedicament(id, comercial_name){
    var format_object = {}
    format_object["id"] = id;
    format_object["name"] = comercial_name;

    insertMedicamentToArray(format_object);
    $("#new_medicament_presentation").val("");
    $("#new_medicament_active_substance").val("");
    $("#new_medicament_brand").val("");
    //$("#disease_name").val("");
    $("#addnewmedicamentfromece").modal("hide");
    MEDICAMENT_SELECTED = {}; // se reinicia el objeto
}

function existElementInDB(name, url){
    var result = null;
    $.ajax({
        url:url,
        method:"GET",
        async: false,
        cache: false,
        dataType:"json",
        data:{name_element: name},
        success:function(response){
            if(response.result){
                result = jQuery.parseJSON(response.model);
            }
        }
    });
    return result;
}

function existElementInArray(array,object){
    var i;
    for(i=0;i<array.length;i++){
        var aux = array[0];
        if(parseInt(object["id"]) == parseInt(aux["id"])){
            return true;
            break;
        }
    }
    return false;
}

function insertDiseaseToArray(object){
    $('#disease_name').val("");
    DISEASES.push(object);
    addToTable("table-diseases",object);
}

function insertMedicamentToArray(object){
    $('#medicament_name').val("");
    MEDICAMENTS.push(object);
    addToTable("table-medicaments",object);
}


function addToTable(table_id,object){
    var table = document.getElementById(table_id);

    var row = table.insertRow();
    row.id = object["id"];
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    if(table_id == "table-medicaments" && object["presentation"] != undefined){
        cell1.innerHTML = object["name"]+ " ("+object["presentation"]+")";
    }else{
        cell1.innerHTML = object["name"];
    }

    cell2.innerHTML = "<a data-toggle='modal' href='#' onclick=deleteRowTable('"+table_id+"',"+object["id"]+")><i class='zmdi zmdi-delete'></i></a>";
}

function deleteRowTable(table_id,row_id){
    var list_text = "";
    if(table_id == "table-diseases"){
        list_text = "¿Confirma que desea eliminar la enfermedad?";
    }else if(table_id == "table-medicaments"){
        list_text = "¿Confirma que desea eliminar el medicamento?";
    }

    swal({    title: "Eliminar",
            text: list_text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Eliminar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false },
        function(){
            swal.close();
            if(table_id == "table-diseases"){
                deleteElementArray(DISEASES, row_id);
                var id_delted = "#table-diseases > thead > tr[id="+row_id+"]"
                $(id_delted).remove();
            }else if(table_id == "table-medicaments"){
                deleteElementArray(MEDICAMENTS, row_id);
                var id_delted = "#table-medicaments > thead > tr[id="+row_id+"]"
                $(id_delted).remove();
            }
        });
}

function deleteElementArray(array,id){
    var i;
    for(i=0;i<array.length;i++){
        var aux = array[i];
        if(parseInt(id) == parseInt(aux["id"])){
            // eliminar elemento del arreglo
            array.splice(i,1);
            break;
        }
    }
}

function updateMedicalHistoryChangeSection(){
    if(FLAG_UPDATE_HISTORY){
        updateMedical_historyAjax();
        FLAG_UPDATE_HISTORY = false;
    }else{
        FLAG_UPDATE_HISTORY = false;
    }
}

function updateMedicalHistory(){
    updateMedical_historyAjax();
    swal({
        title: "EXITO",
        text: "Se a guardado correctamente la información de ANTECEDENTES MÉDICOS",
        type: "success",
        html: true
    });
}

function updateMedical_historyAjax(){
    //var url = $('#url_update_medical_history').val();

    var url = $('#url_update_medical_history').val();
    // info no patologicos
    var no_pathological = {};
    // valida si existe la información de no patologicos
    if($('#medical_history_no_pathological_attributes_smoking').length){
        no_pathological["smoking"] = $('#medical_history_no_pathological_attributes_smoking').is(':checked');
        no_pathological["smoking_description"] = $('#medical_history_no_pathological_attributes_smoking_description').val();

        no_pathological["alcoholism"] = $('#medical_history_no_pathological_attributes_alcoholism').is(':checked');
        no_pathological["alcoholism_description"] = $('#medical_history_no_pathological_attributes_alcoholism_description').val();

        no_pathological["drugs"] = $('#medical_history_no_pathological_attributes_drugs').is(':checked');
        no_pathological["drugs_description"] = $('#medical_history_no_pathological_attributes_drugs_description').val();

        no_pathological["play_sport"] = $('#medical_history_no_pathological_attributes_play_sport').is(':checked');
        no_pathological["play_sport_description"] = $('#medical_history_no_pathological_attributes_play_sport_description').val();

        no_pathological["annotations"] = $('#medical_history_no_pathological_attributes_annotations').val();
        no_pathological["marital_status_id"] = $('#medical_history_no_pathological_attributes_marital_status_id').val();
        no_pathological["religion"] = $('#medical_history_no_pathological_attributes_religion').val();
        no_pathological["scholarship"] = $('#medical_history_no_pathological_attributes_scholarship').val();

    }

    // familiares con enfermedades
    var family_disease = {};
    // valida si existe la información de familiares con enfermedades
    if($('#family_disease_diabetes').length){
        family_disease["diabetes"] = $('#family_disease_diabetes').is(':checked');
        family_disease["diabetes_description"] = $('#family_disease_diabetes_description').val();

        family_disease["hypertension"] = $('#family_disease_hypertension').is(':checked');
        family_disease["hypertension_description"] = $('#family_disease_hypertension_description').val();

        family_disease["overweight"] = $('#family_disease_overweight').is(':checked');
        family_disease["overweight_description"] = $('#family_disease_overweight_description').val();

        family_disease["asthma"] = $('#family_disease_asthma').is(':checked');
        family_disease["asthma_description"] = $('#family_disease_asthma_description').val();

        family_disease["cancer"] = $('#family_disease_cancer').is(':checked');
        family_disease["cancer_description"] = $('#family_disease_cancer_description').val();

        family_disease["other_diseases"] = $('#family_disease_other_diseases').is(':checked');
        family_disease["diseases_new"] = JSON.stringify(DISEASES);

        family_disease["annotations"] = $('#family_disease_annotations').val();


        family_disease["psychiatric_diseases"] = $('#family_disease_psychiatric_diseases').is(':checked');
        family_disease["psychiatric_diseases_description"] = $('#family_disease_psychiatric_diseases_description').val();
        family_disease["neurological_diseases"] = $('#family_disease_neurological_diseases').is(':checked');
        family_disease["neurological_diseases_description"] = $('#family_disease_neurological_diseases_description').val();
        family_disease["cardiovascular_diseases"] = $('#family_disease_cardiovascular_diseases').is(':checked');
        family_disease["cardiovascular_diseases_description"] = $('#family_disease_cardiovascular_diseases_description').val();
        family_disease["bronchopulmonary_diseases"] = $('#family_disease_bronchopulmonary_diseases').is(':checked');
        family_disease["bronchopulmonary_diseases_description"] = $('#family_disease_bronchopulmonary_diseases_description').val();
        family_disease["thyroid_diseases"] = $('#family_disease_thyroid_diseases').is(':checked');
        family_disease["thyroid_diseases_description"] = $('#family_disease_thyroid_diseases_description').val();
        family_disease["kidney_diseases"] = $('#family_disease_kidney_diseases').is(':checked');
        family_disease["kidney_diseases_description"] = $('#family_disease_kidney_diseases_description').val();
    }

    // cirugias
    var surgery = {};
    if($('#surgery_description').length){
        surgery["description"] = $('#surgery_description').val();
        surgery["hospitalizations"] = $('#surgery_hospitalizations').val();
        surgery["transfusions"] = $('#surgery_transfusions').val();
    }


    // alergias
    var pathological_allergy = {};
    if($('#pathological_allergy_allergy_medications').length){
        pathological_allergy["allergy_medications"] = $('#pathological_allergy_allergy_medications').is(':checked');
        pathological_allergy["allergy_medications_description"] = $('#pathological_allergy_allergy_medications_description').val();
        pathological_allergy["other_allergies"] = $('#pathological_allergy_other_allergies').val();
    }


    // padecimientos recientes
    var pathological_conditions = {};
    if($('#pathological_condition_description').length){
        pathological_conditions["description"] = $('#pathological_condition_description').val();
    }

    // medicamentos
    var pathological_medicine = {};
    if($('#pathological_medicine_other_medicines').length){
        pathological_medicine["other_medicines"] = $('#pathological_medicine_other_medicines').val();
        pathological_medicine["medicines_new"] = JSON.stringify(MEDICAMENTS);
    }

    var params = {
        no_pathological: no_pathological,
        family_disease: family_disease,
        surgery: surgery,
        pathological_allergy: pathological_allergy,
        pathological_conditions: pathological_conditions,
        pathological_medicine: pathological_medicine
    };

    $.ajax({
        url:url,
        method:"PUT",
        dataType:"json",
       // async: false,
       // cache: false,
        data:params,
        success:function(response){
            //showInfoMedicalHistory();
        }
    });
}

// ======================= CODIGO ANTERIOR ============================

function setupAddDialogVaccine(medical_history){
    $("#addVaccineModal").modal("show");
    MEDICAL_HISTORY_ID = medical_history;
}

function saveVaccine(action){
    var url = "/admin/vaccinations/";
    var method = "POST";
    var vaccine_select = document.getElementById("new_vaccine_select");
    var vaccine_name = vaccine_select.options[vaccine_select.selectedIndex].text;
    var vaccine_id = $("#new_vaccine_select").val();
    var vaccinate_date = $("#new_vaccinate_date").val();
    var medical_history_id = MEDICAL_HISTORY_ID;
    var id = $("#add_vaccine_id").val();

    var params = {
        vaccunation:{
            vaccinate_date:vaccinate_date,
            vaccine_id:vaccine_id,
            medical_history_id:medical_history_id
        }
    };

    if(action != "add"){
        url += id;
        method = "PUT";
    }
    $("#addVaccineModal").modal("hide");

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(response){
            addToTableVaccines(vaccine_name,response.id,vaccinate_date);
            $("#addVaccineModal").modal("hide");
        }
    });
}
function addToTableVaccines(name,object_id,vaccunate_date){
    var table = document.getElementById("table-vaccines");
    var row = table.insertRow();
    row.id = "admin_vaccination_"+object_id;
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var clicAction = "setupDeleteDialogVaccine(" +object_id +")";
    cell1.innerHTML = name;
    cell2.innerHTML = vaccunate_date;
    cell3.innerHTML = "<a data-toggle='modal' href='#deleteVaccineModal' onclick="+clicAction+"><i class='zmdi zmdi-delete'></i></a>";

}
function setupDeleteDialogVaccine(vaccine){
    DELETE_VACCINATION_ID = vaccine;
}
function deleteVaccine(){
    $("#deleteVaccineModal").modal("hide");
    $.ajax({
        url:"/admin/vaccinations/"+DELETE_VACCINATION_ID,
        method:"DELETE",
        dataType:"json",
        success:function(){
            $("#admin_vaccination_"+DELETE_VACCINATION_ID).remove();
        }
    });
}
