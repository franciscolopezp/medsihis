//= require highstock
//= require highstock/highcharts-more
//= require highstock/themes/sand-signika

//= require doctor/medical_consultation_index
//= require doctor/medical_consultation_edit
//= require doctor/medical_histories
//= require doctor/medical_histories_ped
//= require doctor/medical_vital_signs
//= require doctor/clinical_analysis_order_index
//= require doctor/clinical_analysis_order_new
//= require doctor/graph_percentile
//= require doctor/archives
//= require croppie/croppie.js
//= require doctor/gin_inherited_family_background
//= require doctor/gin_gynecologist_obstetrical_background
//= require doctor/gin_pregnancy
//= require doctor/medical_expedients_systems


var SECTIONS = new Array();
var $uploadCrop;
var streaming = false,
    video        = document.querySelector('#video'),
    canvas       = document.querySelector('#canvas'),
    photo        = document.querySelector('#photo'),
    width = 320,
    height = 0;
var photo_update_source = 0; //1 crop, 2 webcam
var SECTIONS_MEDICAL_HISTORY = new Array();

$(document).ready(function(){

    paginateIndex();
    SECTIONS["data_patient"] = 0;
    SECTIONS["medical_history"] = 1;
    SECTIONS["vital_sign"] = 2;
    SECTIONS["clinical_analysis"] = 3;
    SECTIONS["medical_consultation"] = 4;
    SECTIONS["general-observations"] = 5;
    SECTIONS["pregnancies"] = 6;

    SECTIONS_MEDICAL_HISTORY["general_data"] = 0;
    SECTIONS_MEDICAL_HISTORY["vaccines"] = 1;
    SECTIONS_MEDICAL_HISTORY["pediatrics"] = 2;
    SECTIONS_MEDICAL_HISTORY["gynecology"] = 3;

    fixSliderIndex();
    $( window ).resize(function() {
        fixSliderIndex();
    });
    $( ".main_menu" ).click(function() {
        //console.log($(this).data("section"));
        var sectionId = parseInt($(this).data("section"));
        init_menu_tap_expedient(sectionId);
    });
    //se carga la primera pagina que se encuentre visible
    var sectionId = $('.tab-nav .active a').data("section");
    init_menu_tap_expedient(sectionId);
//autocomplete enfermedades
    URL_ENFERMEDADES = $('#url_search_disease').val();
    URL_MEDICAMENTOS = $('#url_search_medicament').val();

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('.upload-demo').addClass('ready');
            }
            reader.readAsDataURL(input.files[0]);
        }
        $('#upload-demo').show();
        $('#image_preview_edit').hide();
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 185,
            height: 185,
            type: 'square'
        },
        boundary: {
            width: 275,
            height: 275
        }
    });

    $('#upload').on('change', function () { readFile(this); });
    $('#upload-demo').hide();

    init_table_certificate();
});

function loadUltrasounds(preg_id) {
    $("#ultrasounds_records tbody").html("");
    $("#date_ultrasound").val("");
    $("#pregnancy_weeks_ultrasound").val("");
    $.ajax({
        url:$("#url_load_ultrasounds").val(),
        data:{pregnancy_id:preg_id},
        dataType:"json",
        success:function(data){
            $("#fum_label").html(data.fum);
            $("#fpp_label").html(data.fpp);
            $.each(data.data,function(idx,item){
                var s = item.date;
                item.date = stringToDate(s,"yyyy-mm-dd");
                appendItemToUltrasoundTable(item);
            });
        }
    });
}


function initUltrasoundComponents(){

    $(".load_ultrasounds_btn").unbind( "click" );
    $("#save_ultrasound").unbind( "click" );
    
    $(".load_ultrasounds_btn").click(function(){
        $("#load_ultrasound_modal").modal("show");
        $("#pregnancy_id_ultrasound").val($(this).attr("pregnancy"));
        loadUltrasounds($(this).attr("pregnancy"));
    });

    $("#save_ultrasound").click(function(){
        if($("#pregnancy_weeks_ultrasound").val() == "" || $("#date_ultrasound").val() == ""){
            swal("No se puede agregar el registro","Por favor complete los campos.","error");
            return;
        }

        $.ajax({
            method:"POST",
            url:$("#url_ultrasounds").val(),
            data:{
                pregnancy_id:$("#pregnancy_id_ultrasound").val(),
                date:$("#date_ultrasound").ts_date(),
                weeks:$("#pregnancy_weeks_ultrasound").val()
            },
            dataType:"json",
            success:function(data){
                if(data.done){
                    appendItemToUltrasoundTable({
                        date:$("#date_ultrasound").ts_date(),
                        weeks:$("#pregnancy_weeks_ultrasound").val(),
                        id:data.rec.id
                    });
                }
            }
        });
    });
}


function init_table_certificate(){
    var grid = $("#data-table-certificate").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-certificate').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                var options = "";

              //  options += "<a href=\"" + row.show_url + "\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Ver\"><i class=\"zmdi zmdi-eye\"></i></a>";

                options += "<a href=\"" + row.print_url + "\" target=\"_blank\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Imprimir\"><i class=\"zmdi zmdi-print\"></i></a>";

              //  options += "<a href=\"" + row.edit_url + "\" class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>";

               // options += "<button data-url-delete=\"" + row.destroy_url + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
                return options;
            }
        }
    });
}

function open_menu_certificate(){
    //if(isOpen){
        $('#menu_certificate').modal('show');
    //}else{
    //    $('#menu_certificate').hide();
   // }
}

function init_menu_tap_expedient(sectionId){

    switch (parseInt(sectionId)){
        case SECTIONS["data_patient"]:
            updateMedicalHistoryChangeSection(); // se guarda los antecedentes medicos cuando se mueve de sección
            showInfoDataPatient();
            break;
        case SECTIONS["medical_history"]:
            //showLoading("medical-history");
            updateMedicalHistoryChangeSection(); // se guarda los antecedentes medicos cuando se mueve de sección
            showInfoMedicalHistory();
            break;
        case SECTIONS["vital_sign"]:
            //showLoading("vital-sign");
            updateMedicalHistoryChangeSection(); // se guarda los antecedentes medicos cuando se mueve de sección
            showVitalSigns();
            break;
        case SECTIONS["general-observations"]:
            //showLoading("vital-sign");
            updateMedicalHistoryChangeSection(); // se guarda los antecedentes medicos cuando se mueve de sección
            showGeneralObservations();
            break;
        case SECTIONS["clinical_analysis"]:
            updateMedicalHistoryChangeSection(); // se guarda los antecedentes medicos cuando se mueve de sección
            showInfoAnalysisOrderPatient();
            break;
        case SECTIONS["medical_consultation"]:
            updateMedicalHistoryChangeSection(); // se guarda los antecedentes medicos cuando se mueve de sección
            showInfoConsultationPatient();
            break;
        case SECTIONS["pregnancies"]:
            updateMedicalHistoryChangeSection(); // se guarda los antecedentes medicos cuando se mueve de sección
            showLoading("pregnacies_holder");
            loadPregnacies();
            break;
        default:
            console.log("No se encontro el tap");
            break;
    }
}

function open_camera(){
    $('#chose_update_photo_option').modal("hide");
    streaming = false,
        video        = document.querySelector('#video'),
        canvas       = document.querySelector('#canvas'),
        photo        = document.querySelector('#photo'),
        width = 320,
        height = 0;

    navigator.getMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

    navigator.getMedia(
        {
            video: true,
            audio: false
        },
        function(stream) {
            if (navigator.mozGetUserMedia) {
                video.mozSrcObject = stream;
            } else {
                var vendorURL = window.URL || window.webkitURL;
                video.src = vendorURL.createObjectURL(stream);
            }
            video.play();

        },
        function(err) {
            console.log("An error occured! " + err);
        }
    );

    video.addEventListener('canplay', function(ev){
        if (!streaming) {
            height = video.videoHeight / (video.videoWidth/width);
            video.setAttribute('width', width);
            video.setAttribute('height', height);
            canvas.setAttribute('width', width);
            canvas.setAttribute('height', height);
            streaming = true;
        }
    }, false);

    $("#take_picture").modal("show");
    /* Webcam.set({
     width: 320,
     height: 240,
     image_format: 'jpeg',
     jpeg_quality: 90
     });
     Webcam.attach( '#my_camera' );*/
}
function remove_picture(){
    $('#picture64').val("");
    $('#upload').show();
    $('#image_prev_upd').show();
    $('#results').hide();
    photo_update_source = 1;
}

function take_snapshot() {
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(video, 0, 0, width, height);
    var data = canvas.toDataURL('image/png');
    document.getElementById('results').innerHTML =
        '<div class="row"></div>'+
        '<img src="'+data+'" width="250" height="200"/>' +
        '<button type="button" class="btn btn-link" onclick="remove_picture()">Cancelar</button>';
    $('#picture64').val(data);
    $("#take_picture").modal("hide");
    $('#image_prev_upd').hide();
    $('#upload').hide();
    $('#upload_picture').hide();
    $('#results').show();
    $('#crop_image_64').val("");
    photo_update_source = 2;
    navigator.getMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

    navigator.getMedia(
        {
            video: true,
            audio: false
        },
        function(stream) {
            if (navigator.mozGetUserMedia) {
                video.mozSrcObject = stream;
                stream.getVideoTracks()[0].stop()
            } else {
                var vendorURL = window.URL || window.webkitURL;
                video.src = vendorURL.createObjectURL(stream);
                stream.getVideoTracks()[0].stop()
            }
            video.pause();

        },
        function(err) {
            console.log("An error occured! " + err);
        }
    );
    /*   var foto64;
     $("#upload_picture").hide();
     Webcam.snap( function(data_uri) {
     foto64 = data_uri;
     // display results in page
     document.getElementById('results').innerHTML =
     '<div class="row"></div>'+
     '<img src="'+data_uri+'" width="250" height="200"/>' +
     '<button type="button" class="btn btn-link" onclick="remove_picture()">Cancelar</button>';
     } );
     $('#picture64').val(foto64);
     Webcam.reset();
     $("#take_picture").modal("hide");
     $('#upload_picture').hide();
     $('#button_crop_size').hide();
     $('#results').show();*/
}

function close_camera(){
    $("#take_picture").modal("hide");
}
function open_crop_update(){
    $('#update_patient_foto_request').modal("show");
    $('#chose_update_photo_option').modal("hide");
}
function viewer_file_expedient(){
    //var url_expedient = $('#url_show_expedient').val();
    $('body').addClass('modal-open');
    $('body').css('position','fixed');
    $('body').css('top','0');
    $('body').css('left','0');
    $('body').css('right','0');

    $('#box_view_file').show();
    var root =  $('#url_show_expedient').val();

    openFile(root);
}
function updateGeneralObservations(){
    $.ajax({
        url: $('#url_update_general_obs').val(),
        dataType: "json",
        data: {general_observations:$('#expedient_observations').summernote().code(),
                medical_expedient:$('#expedient_id').val()},
        success: function (data) {
            if(data.result == "1"){
                swal("Éxito", "Las observaciones generales han sido actulizadas con éxito", "success");
            }
            else {
                swal("Error", "Ocurrió un error al actualizar las observaciones generales", "fail");
            }

        }
    });
}
function update_patient_picture(){
    if(photo_update_source == 2){
        $('#crop_image_64').val("");
    }else {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (result) {
            $('#result-image').attr('src', result);
            if (result.length > 10) {
                $('#crop_image_64').val(result);
            } else {
                $('#crop_image_64').val("");
            }

        });
    }
    $('#image_preview_edit').hide();

}

function fixSliderIndex(){
    var heightWindow = $(window).height();
    var heightNavigation = $("#header").height() +  (parseInt($("#footer").height()) * 2);
    var heightFixed = heightWindow - heightNavigation;
    $("#profile-main").css("min-height",heightFixed);	//first page
}

function showInfoConsultationPatient(){
    $(".search-consultation").trigger( "click" );
    //showLoading("container_medical_consultation");
    init_section_medical_consultation();
   // getMedicalConsultation('',1); //se muestran del mas antiguo al mas actual
    searchLastConsultation(); // del mas actual al mas antiguo
}

function showInfoAnalysisOrderPatient(){
    $(".search-analysis-order").trigger( "click" );
    init_section_analysis_order();
    searchLastAnalysisOrder();
}

function showInfoDataPatient(){
    showLoading("data-patient");
    var url = $('#url_data_patient').val();
    $.ajax({
        type: "get",
        url: url,
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("data-patient",response);

            /*init components studies*/
            $(".selectpicker").selectpicker("refresh");
        },
        error: function (error) {
            swal("Error!", "Error al cargar la sección de datos de paciente.", "error");
        }
    });

}
function showGeneralObservations(){
    showLoading("general-observations");
    var url = $('#url_data_general_obs').val();
    $.ajax({
        type: "get",
        url: url,
        dataType:'html',
        success: function (response) {
            hideLoadingAndSetContainer("general-observations",response);
            initHtmlEditor("#expedient_observations");

            $('#expedient_observations').summernote().code($("#expedient_observations_val").val());
        },
        error: function (error) {
            swal("Error!", "Error al cargar la sección de observaciones generales.", "error");
        }
    });
}
function paginateIndex(){
    var table = $("#data-table-medical-expedients");
    table.dataTable({

        ajax: {
            url: table.data('source'),
            data: function(d) {}
        },
        columns: [
            {data:'folio'   , name:'medical_expedients.folio', defaultContent:''},
            {data:'name', name:'patients.name|patients.last_name', defaultContent:''},
            {data:'gender'  , name:'genders.name', defaultContent:''},
            {data:'age' , name:'patients.birth_day', defaultContent:''},
            {data:'doctor' , name:'', orderable: false, searchable:false, defaultContent:''},
            {data:'last_attention' , name:'', orderable: false, searchable:false, defaultContent:''},
            {data: null,
                orderable:false,
                searchable:false,
                defaultContent:'',
                render : function(data, type, row, meta){
                    return "<a href=" + row.url_show_medical_expedient + " class=\"btn btn-primary btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Mostrar\"><i class=\"zmdi zmdi-assignment-o\"></i></a>";
                }
            }
        ],
        initComplete: function(settings, json){
            setPositionAndClassesOptionsDatatable();
        }
    });
}

function showLoading(idTap){
    var html = '<div class="col-md-offset-5 col-md-7">'+
        '<div class="preloader pl-xxl">'+
        '<svg class="pl-circular" viewBox="25 25 50 50">'+
        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
        '</svg>'+
        'Cargando...'+
        '</div>'+
        '</div>';

    var id= "#"+idTap;
    $(id).empty();
    $(id).append(html);
}

function hideLoadingAndSetContainer(idTap,container){
    var id= "#"+idTap;
    $(id).empty();
    $(id).append(container);
}


function openSection(evt, section) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tabcontent.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(section).style.display = "block";
      evt.currentTarget.className += " active";
}

function open_modal_new_cost(consultation_id,cost){
    $("#new_consultation_cost").val(cost);
    $('#consultation_id_for_update').val(consultation_id);
    $('#change_consultation_cost_modal').modal('show');
}
function update_consultation_cost(){
    consultation_id  = $('#consultation_id_for_update').val();
    if($('#new_consultation_cost').val()==""){
        swal("Error!", "El campo no puede estar vacio.", "error");
    }else {
        $.ajax({
            url: $('#url_update_consult_cost').val(),
            dataType: "json",
            data: {consultation_id: consultation_id, new_price: $('#new_consultation_cost').val()},
            success: function (data) {
                searchLastConsultation();
                $('#change_consultation_cost_modal').modal('hide');
            },
            error: function (data) {
            }
        });
    }
}


function setupByPaymentMethod(){
    var payment_method = $("#charge_payment_method").val();
    var affect_local = $("#payment_method_"+payment_method).attr("affect_local");
    $(".t_account").show();
    if(affect_local == 1){
        $(".bank_account").hide();
    }else{
        $(".local_account").hide();
    }
    $("#charge_account").val("").selectpicker("refresh");

}