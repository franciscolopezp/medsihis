//= require jquery-ui

var act_type = {
    appointment : 1
};

$(function(){
    $("#wroom_sha_doctor_id").change(function(){
        loadAppointments();
    });
    var today = $('#today_server').val();
    var today_format = new moment(today, "DD/MM/YYYY");
    $('#waiting_room_date_input').datetimepicker({
        locale: 'es',
        format: 'DD/MMMM/YYYY',
        date: today_format
    });
    $("#input_date_filter").click(function () {
        $('#waiting_room_date_input').data("DateTimePicker").show();
    });
    loadAppointments();
});

function loadAppointmentsToday(){

    var today = $('#today_server').val();
    var today_format = new moment(today, "DD/MM/YYYY");
    $('#waiting_room_date_input').data("DateTimePicker").date(today_format);
    loadAppointments();
}


function loadAppointments(){

    var date = $("#waiting_room_date_input").data("DateTimePicker").date();
    var month_int = parseInt(date.format("M")) - 1;
    var year_int = date.format("YYYY");
    var date_int = date.format("D");

    $("#pending ul").html("");
    $("#waiting ul").html("");
    $("#attended ul").html("");

    var params = {
        year:year_int,
        month:month_int,
        date:date_int,
        type:act_type.appointment,
        office_id:"",
        wroom_page:1
    };

    if($.isNumeric($("#wroom_sha_doctor_id").val())){
        params.doctor_id = $("#wroom_sha_doctor_id").val();
    }

    $.ajax({
        url:"/doctor/activities",
        method:"GET",
        dataType:"json",
        data:params,
        success:function(activities){
            $.each(activities,function(idx,ac){
                drawActivityCard(ac);
            });
        }
    });

}