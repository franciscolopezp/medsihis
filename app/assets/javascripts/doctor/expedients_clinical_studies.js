function initComponentsClinicalStudiesUI(){
    $("#btn-new-analysis-order").click(function(){
        $("#modalAnalysis").modal("show");
        var url = $(this).attr("url");

        $.ajax({
            type: "get",
            url: url,
            dataType:'html',
            success: function (response) {
                $("#form-new-analysis-order").html(response);
                initElementAnalysis();
            },
            error: function (error) {
                swal("Error!", "Error al cargar el formulario de analisis clinico", "error");
            }
        });
    });
}

var CLINICAL_ORDER_TYPE = null;
function initElementAnalysis(){
    $("input[name=clinical_order_type]:radio").change(function () {
        CLINICAL_ORDER_TYPE = $(this).val();

        $("option.lab").hide();
        $("option.type_"+$(this).val()).show();
        $("#laboratory_select").val("").selectpicker('refresh');
        hideAnnotationOrResult($(this).val());

        console.log($(this).val());

        $("#doctor_clinical_study_type").html("").selectpicker('refresh');


        $('#list_clinical_analysis').html("");
        $('#list_clinical_analysis').trigger('bootstrapDualListbox.refresh' , true);
    });

    $('.selectpicker').selectpicker();

    $("#laboratory_select").change(loadCategoriesByLab);
    $("#doctor_clinical_study_type").change(loadAnalysisByCategory);

    $('#list_clinical_analysis').bootstrapDualListbox({
        nonSelectedListLabel: 'Lista de estudios',
        selectedListLabel: 'Estudios seleccionados',
        preserveSelectionOnMove: 'moved',
        moveOnSelect: false,
        nonSelectedFilter: '',
        filterPlaceHolder: 'Filtrar',
        filterTextClear: 'Mostrar todos',
        moveSelectedLabel: 'Mover seleccionado',
        moveAllLabel: 'Mover todo',
        infoTextFiltered: '<span class="label label-info">Filtrado</span> {0} de {1}',
        infoTextEmpty: '0 Estudios',
        infoText: '{0} Estudios'
    });

    $('#list_clinical_analysis').on('change',function(){
        $('#indications_study_selected').empty();
        var url = $('#show_indications').val();

        $.ajax({
            type: "get",
            url: url,
            dataType:'json',
            data: getJsonOrder(),
            success: function (response) {
                var indications = response.indications;
                var list = "<ul>";
                $.each(indications, function(index,val){
                    list = list + "<li>"+ val.description+ "</li>";
                });
                list = list + "</ul>";
                $('#indications_study_selected').html(list);
            },
            error: function (error) {
                swal("Error!", "Error al presentar la lista de indicaciones", "error");
            }
        });
    });

    $("#btn-save-analysis-order").unbind();
    $('#btn-save-analysis-order').click(function(e)
    {
        //bloquear el boton de guardar, mostrar loading
        $('#loading_btn_save_analysis_order').show();
        saveClinicalAnalysisOrder();
    });

    $("#package_id").change(function(){

        if($("#package_id").val() == ""){
            return;
        }

        var selected = [];
        $.ajax({
            url: $("#url_load_studies_by_package").val(),
            data: {package_id: $("#package_id").val(), studies_selected: selected},
            dataType:'json',
            success: function (response) {
                $("#list_clinical_analysis").html("");
                $.each(response.result,function(idx, item){
                    $('#list_clinical_analysis').append($('<option>',{
                        value: item.id,
                        text: item.name
                    }));
                });
                $.each(response.selected,function(idx, item){
                    $('#list_clinical_analysis').append($('<option>',{
                        value: item.id,
                        text: item.name,
                        selected:'selected'
                    }));
                });
                $('#list_clinical_analysis').trigger('bootstrapDualListbox.refresh' , true);

            },
            error: function (error) {
                swal("Error!", "Error al cargar los analisis del laboratorio", "error");
            }
        });
    });


}

function loadCategoriesByLab(){
    loadPackagesByLab();
    $('#list_clinical_analysis').html("");
    $('#list_clinical_analysis').trigger('bootstrapDualListbox.refresh' , true);
    var lab = $("#laboratory_select").val();
    var url = $("#url_load_categories").val();

    $.ajax({
        url:url,
        data:{lab_id:lab},
        dataType:"json",
        success:function(data){
            $("#doctor_clinical_study_type").html("");
            $('#doctor_clinical_study_type').append($('<option>',{
                value: '',
                text: 'Seleccione una categoría'
            }));
            $.each(data,function(idx, item){
                $('#doctor_clinical_study_type').append($('<option>',{
                    value: item.id,
                    text: item.name
                }));
            });
            $("#doctor_clinical_study_type").selectpicker("refresh");
        }
    });
}

function loadPackagesByLab(){
    var lab = $("#laboratory_select").val();
    var url = $("#url_load_packages").val();
    $.ajax({
        url:url,
        data:{laboratory_id:lab},
        dataType:"json",
        success:function(data){
            $("#package_id").html("");
            $('#package_id').append($('<option>',{
                value: '',
                text: 'Seleccione un paquete de estudios'
            }));
            $.each(data,function(idx, item){
                $('#package_id').append($('<option>',{
                    value: item.id,
                    text: item.name
                }));
            });
            $("#package_id").selectpicker("refresh");
        }
    });
}


function hideAnnotationOrResult(value){
    $('#doctor_annotations').val('');
    $('#doctor_result').val('');

    switch(parseInt(value)){
        case 1: // Estudio clinico
            $('#result_order').hide();
            $('#annotations_order').show();
            break;
        case 2: // Estudio de Gabinete
            $('#result_order').show();
            $('#annotations_order').hide();
            break;
        default:

            break;
    }
}

function loadAnalysisByCategory() {
    var category = $("#doctor_clinical_study_type").val();
    var url = $('#url_find_clinical_studies').val();
    var selected = $("#list_clinical_analysis").val();

    if(selected == null){
        selected = [];
    }

    $.ajax({
        type: "get",
        url: url,
        data: {study_type: category, study_selected: selected},
        dataType:'json',
        success: function (response) {
            $("#list_clinical_analysis").html("");
            $.each(response.result,function(idx, item){
                $('#list_clinical_analysis').append($('<option>',{
                    value: item.id,
                    text: item.name
                }));
            });

            $.each(response.selected,function(idx, item){
                $('#list_clinical_analysis').append($('<option>',{
                    value: item.id,
                    text: item.name,
                    selected:'selected'
                }));
            });
            $('#list_clinical_analysis').trigger('bootstrapDualListbox.refresh' , true);

        },
        error: function (error) {
            swal("Error!", "Error al cargar los analisis del laboratorio", "error");
        }
    });

}


function searchLastAnalysisOrder(clinical_order_type){
    var url = $('#url_analysis_order').val();
    var searchPhrase = $('#search_analysis_order').val();
    var page = 1;
    var order = "DESC";
    $.ajax({
        type: "get",
        url: url,
        data: {searchPhrase: searchPhrase,filter_clinical_order_type: clinical_order_type,current: page, rowCount: ROWS_BY_PAGE, "sort[folio]": order},
        dataType:'html',
        success: function (response) {
            $("#clinical_studies_holder").html(response);
            var infoAnalysis = JSON.parse($('#info_analysis_order').val());
            updatePagesAnalysis(infoAnalysis.total,infoAnalysis.current);
            addFunctionListAnalysisOrder();

            //init components of
            $(".selectpicker").selectpicker("refresh");

            $("#charge_report_btn").click(function(){
                addChargeReport();
            });

            $("#charge_payment_method").change(function(){
                $(".t_account").hide();
                if($("#payment_method_"+$(this).val()).attr("affect_local") == "1"){
                    $(".local_account").show();
                }else{
                    $(".bank_account").show();
                }
                $("#charge_account").selectpicker("refresh");
            });

            $("#clinical_study_cabinet_laboratory").change(function(){
                $.ajax({
                    url:$("#studies_by_laboratory_url").val(),
                    dataType:"json",
                    data:{laboratory_id:$(this).val()},
                    success:function(data){
                        $("#clinical_study_cabinet").html("");
                        $("#clinical_study_cabinet").append("<option value=''>Seleccione un estudio</option>");
                        $.each(data,function(idx, item){
                            $("#clinical_study_cabinet").append("<option id='cabinet_study_"+item.id+"' price='"+item.price+"' value='"+item.id+"'>"+item.name+"</option>");
                        });
                        $("#clinical_study_cabinet").selectpicker("refresh");
                    }
                });
            });

            $("#clinical_study_cabinet").change(function(){

                var price = $("#cabinet_study_"+$("#clinical_study_cabinet").val()).attr("price");
                $("#cabinet_report_price").val(price);

                $.ajax({
                    url:$("#study_template_url").val(),
                    data:{study_id:$(this).val()},
                    success:function(data){
                        $("#report_sumary").summernote().code(data);
                    }
                });
            });

        },
        error: function (error) {
            swal("Error!", "Error al buscar ordenes de estudios.", "error");
        }
    });

}

function addFunctionListAnalysisOrder(){
    //boton imprimir
    $('.print_btn_analysis_order').unbind("click");
    $('.print_btn_analysis_order').click(function(){
        var url_print = $(this).data("url");
        window.open(url_print);
    });


    // class item-consultation funcion cuando da clic en una consulta, se muestra el detalle en el modal
    $('.item-analysis-order').unbind("click");
    $('.item-analysis-order').click(function(){
        $('#loading_btn_save_analysis_order').hide();
        $('#btn-save-analysis-order').hide();
        $('#list_error_analysis_order').empty();
        $('#modalAnalysis').modal('show');
        showLoading("form-new-analysis-order");
        var url = $(this).data("url");
        $.ajax({
            type: "get",
            url: url,
            dataType:'html',
            success: function (response) {
                hideLoadingAndSetContainer("form-new-analysis-order",response);
                //initTableDiagnostic();
                //initTablePrescription();
                $("#data-table-analysis-order").bootgrid({
                    navigation: 2,
                    labels: {
                        noResults: "Sin resultados"
                    },
                    css: {
                        icon: 'zmdi icon',
                        iconColumns: 'zmdi-view-module',
                        iconDown: 'zmdi-expand-more',
                        iconRefresh: 'zmdi-refresh',
                        iconUp: 'zmdi-expand-less'
                    }});
            },
            error: function (error) {
                swal("Error!", "Error al buscar analisis clinico", "error");
            }
        });
    });

    $('.add_file_analysis').unbind();
    $('.add_file_analysis').on("change", function(){
        if($(this).val() != ""){
            var data = new FormData();
            var files = $(this)[0].files;

            data.append('file_0', files[0]);
            var id = $(this).data("id");
            var id_loading = "#loading_"+id;
            $(id_loading).show();

            var id_input = "#input_file_"+id;
            $(id_input).prop( "disabled", true );

            data.append('id', id);
            var url = $('#url_add_file_analysis').val();
            $.ajax({
                url: url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(data){
                    if(data){
                        searchLastAnalysisOrder();
                    }
                }
            });
        }
    });
    $('.delete-file').unbind("click");
    $('.delete-file').click(function(e){
        e.preventDefault();
        delete_file($(this).attr('id'),$(this).data("analysis"));
    });
}

function delete_file(file_id,div_id){
    var id = file_id;
    var analysis_id = div_id;
    var url = $('#url_delete_file_analysis').val();
    $.ajax({
        type: "DELETE",
        url: url,
        data: {id: id},
        //dataType:'json',
        success: function (response) {
            if(response){
                searchLastAnalysisOrder();
            }
        },
        error: function (error) {
            searchLastAnalysisOrder();
        }
    });
}


function updatePagesAnalysis(totalRecord,pageCurrent){
    var totalPages = Math.ceil(parseInt(totalRecord)/parseInt(ROWS_BY_PAGE));
    var arrayPage = $('#pagination-analysis-order > li');
    if(parseInt(pageCurrent) == 1){
        //bloquear el boton izquierdo
        $(arrayPage[0]).addClass('disabled');
    }else{
        //desbloquear el boton izquierdo
        $(arrayPage[0]).removeClass('disabled');
    }
    if(parseInt(totalPages) == parseInt(pageCurrent)){
        //bloquear el ultimo boton
        $(arrayPage[arrayPage.length - 1]).addClass('disabled');
    }else{
        //desbloquear el ultimo boton
        $(arrayPage[arrayPage.length - 1]).removeClass('disabled');
    }
    var i;
    //remove all number
    for(i=0;i < arrayPage.length; i++){
        if(parseInt(i) != parseInt(0) && parseInt(i) != parseInt((arrayPage.length - 1))){
            $(arrayPage[i]).remove();
        }
    }
    //all number page
    var list = "";
    for(i=1;i <= parseInt(totalPages); i++){
        if(parseInt(pageCurrent) == i){
            list += '<li class="active"><a href="" data-number="'+i+'">'+i+'</a></li>'
        }else{
            list += '<li class=""><a href="" data-number="'+i+'">'+i+'</a></li>'
        }
    }
    $(list).insertAfter($('#pagination-analysis-order > .previous_pagination'));

    //add event click
    $('#pagination-analysis-order li').unbind( "click" );
    $('#pagination-analysis-order li').click(function(e)
    {
        e.preventDefault();
        if(!$(this).hasClass("disabled")){
            var selected = getNumberPageSelectedAnalysis();
            var search = $('#search_analysis_order').val();
            if($(this).hasClass("previous_pagination")){
                //page before
                var nextPage = selected - 1;
                getAnalysisOrder(search,nextPage);

            }else if($(this).hasClass("next_pagination")){
                //next page
                var nextPage = selected + 1;
                getAnalysisOrder(search,nextPage);
            }else{
                var links = $(this).find("a");
                var nextPage = $(links[0]).data('number');
                getAnalysisOrder(search,nextPage);
            }

        }
    });
}


function saveClinicalAnalysisOrder(){
    var clinicalOrder = $("#list_clinical_analysis").val();
    var result = new Array();
    if(clinicalOrder != null){
        var i;
        for(i=0;i<clinicalOrder.length;i++){
            var id = clinicalOrder[i];
            result.push({'id':id});
        }
    }

    if(result.length == 0){
        swal("No se puede guardar","Por favor seleccione estudios.","error");
        $("#loading_btn_save_analysis_order").hide();
        return;
    }


    var formClinical = $('#new_clinical_order').serializeArray();
    var json = JSON.stringify(formClinical);
    var jsonReal = JSON.parse(json);

    jsonReal.push(getJsonOrder());
    jsonReal.push({"name": 'analysis[laboratory_id]', "value": $("#laboratory_select").val()});
    jsonReal.push({"name": 'analysis[office_id]', "value": $('#order_office').val()});
    jsonReal.push({"name": 'analysis[annotations]', "value": $('#doctor_annotations').val()});
    jsonReal.push({"name": 'analysis[result]', "value": $('#doctor_result').val()});
    jsonReal.push({"name": 'analysis[clinical_order_type_id]', "value": CLINICAL_ORDER_TYPE});
    var url = $('#url_form_create_analysis_order').val();


    if(CLINICAL_ORDER_TYPE == "" || $('#order_office').val() == ""){
        swal("No se puede guardar","Por favor complete los campos correctamente.","error");
        $("#loading_btn_save_analysis_order").hide();
        return;
    }
    $.ajax({
        type: "post",
        url: url,
        dataType:'json',
        data: jsonReal,
        success: function (response) {
            if(response.ok == "true"){
                var url_has_template = response.url_print;
                window.open(url_has_template);
                $('#modalAnalysis').modal('hide');
                searchLastAnalysisOrder();
            }else{
                setErrorsModalAnalysisOrders(response.errors);
            }
        },
        error: function (error) {
            swal("Error!", "Error al guardar la orden, consulte al administrador", "error");
        }
    });
}

function getJsonOrder() {
    var clinicalOrder = $("#list_clinical_analysis").val();
    var result = new Array();
    if(clinicalOrder != null){
        var i;
        for(i=0;i<clinicalOrder.length;i++){
            var id = clinicalOrder[i];
            result.push({'id':id});
        }
    }
    var jsonDiseases = {"name": 'analysis[clinical_analysis_order]', "value": JSON.stringify(result)};
    return jsonDiseases;
}