// require custom/jquery-1.11.1.min.js
// solo se agrega cuando la visualización del pdf se abre en una ventana nueva y no sobre el mismo sistema
//= require viewFiles/viewpdf/compatibility
//= require viewFiles/viewpdf/l10n
//= require viewFiles/mozillapdf/pdf.js
//= require viewFiles/viewpdf/debugger
//= require viewFiles/viewpdf/viewer