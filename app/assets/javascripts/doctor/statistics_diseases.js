var TYPE_GRAPH_PERSON = 1;
var TYPE_GRAPH_INCIDENCE = 2;

$(document).ready(function(){
    // init graph
    if($("#graph_diseases_radio_one").length){
        start_by_type();
    }
});

function start_by_type(){
  //  graph_diseases();
    var type = 0;
    if ($("#graph_diseases_radio_one").is(":checked")) {
        type = TYPE_GRAPH_PERSON;
    }
    else {
        type = TYPE_GRAPH_INCIDENCE;
    }
    var url = $('#url_graph_diseases').val();
    var start_date = $('#start_date_graph_diseases').val();
    var end_date = $('#end_date_graph_diseases').val();
    var more_than = $('#more_than_diseases').val();
    $.ajax({
        url:url,
        method:"get",
        dataType:"json",
        data:{type_graph: type, start_date: start_date, end_date: end_date, more_than: more_than},
        success:function(data){
            graph_diseases(data);
        },
        error:function(error){
            console.log("ERROR en los percentiles");
        }
    });
}

function download_graph_diseases(){
    var type = 0;
    if ($("#graph_diseases_radio_one").is(":checked")) {
        type = TYPE_GRAPH_PERSON;
    }
    else {
        type = TYPE_GRAPH_INCIDENCE;
    }
    var url = $('#url_graph_diseases').val();
    var start_date = $('#start_date_graph_diseases').val();
    var end_date = $('#end_date_graph_diseases').val();
    var more_than = $('#more_than_diseases').val();
    var url = $('#url_download_graph_diseases').val()+"/?type_graph="+type+"&start_date="+start_date+"&end_date="+end_date+"&more_than="+more_than;
    var win = window.open(url, '_blank');
    if (win) {
        //Browser has allowed it to be opened
        win.focus();
    } else {
        //Browser has blocked it
        alert('Active la ventana emergente para este sitio web');
    }
}

function graph_diseases(data){

    Highcharts.chart('graph_diseases', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Gráfica de enfermedades'
        },
        xAxis: {
            categories: data.categories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total de personas con alguna enfermedad'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            },
            exporting: { enabled: true }
        },
        series:data.series
    });
}