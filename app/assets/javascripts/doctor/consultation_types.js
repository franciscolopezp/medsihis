function resetChecks() {
    $.each($(".s_parent"),function(idx, item){
        if($(item).is(":checked")){
            $(item).prop('checked', false);
        }
    });
}
function saveConsultationTypes(){
    var consultation_types = [];
    $.each($(".s_parent"),function(idx, item){
        if($(item).is(":checked")){
            consultation_types.push({
                consultation_type_id:$(item).val()
            });
        }
    });
    if (consultation_types.length == 0){
        swal("Error!", "Debe seleccionar un usuario y los tipos de consulta para el usuario.", "error");
        return;
    }
    $.ajax({
        url: $("#url_save_user_consultation_types").val(),
        dataType: "json",
        data: {user_id:$("#user_name_ts_autocomplete").val(),consultation_types:consultation_types},
        success: function (data) {
            swal("Exito!", "Los tipos de consulta fueron agregadas.", "success");
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function loadConsultationTypes(user){
    $.ajax({
        url: $("#url_get_user_consultation_types").val(),
        dataType: "json",
        data: {user_id:user},
        success: function (data) {
            $("#consultation_types_div").show();
            $.each(data, function (idx, info) {
                $("#"+info.name).prop('checked', true);
            });

        },
        error:function(data){
            console.log("error controller");
        }
    });
}
jQuery(document).ready(function() {
    $("#consultation_types_div").hide();
    $("#user_name").ts_autocomplete({
        url:AUTOCOMPLETE_PERSONS_URL,
        items:10,
        callback:function(val, text){
            resetChecks();
            loadConsultationTypes(val);
        }
    });
    var table = $("#data-table-consultation-types");
    var grid = $("#data-table-consultation-types").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-consultation-types').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a  onclick=\"setConsultationTypeId("+row.is_editable_price+ "," + row.id + ",'" + row.name + "'," + row.just_cost + ")\" class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\"  title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el tipo de consulta "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El tipo de consulta ha sido eliminada exitosamente.", "success");
                        $("#data-table-consultation-types").bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });




});

function saveConsultationType(){
    var url = $("#consultation_types_path").val();
    var method = "POST";

    if($("#name_consultation_type").val() == ""){
        swal("Por favor ingrese un nombre.");
        return;
    }
    if($("#cost_consultation_type").val() == ""){
        swal("Por favor ingrese un costo.");
        return;
    }
    var check_price_value = 0;
    if($("#is_editable_price").is(':checked')){
        check_price_value = 1;
    }
    var data = {name:$("#name_consultation_type").val(),cost:$("#cost_consultation_type").val(),is_editable:check_price_value};

    if($("#id_consultation_type").val() != ""){
        method = "PUT";
        url += "/"+$("#id_consultation_type").val();
    }

    $.ajax({
        url:url,
        method:method,
        data:data,
        dataType:"json",
        success:function(){
            $("#name_consultation_type").val("");
            $("#cost_consultation_type").val("");
            $("#add_consultation_type_modal").modal("hide");
            $("#data-table-consultation-types").bootgrid("reload");
        }
    });
}
function resetForm(){
    $("#name_consultation_type").val("");
    $("#cost_consultation_type").val("");
    $("#id_consultation_type").val("");
    $("#is_editable_price").prop('checked', false);
}

function setConsultationTypeId(editable,id,name,cost){
    if(editable){
        $("#is_editable_price").prop('checked', true);
    }else{
        $("#is_editable_price").prop('checked', false);
    }
    $("#name_consultation_type").val(name);
    $("#cost_consultation_type").val(cost);
    $("#id_consultation_type").val(id);
    $("#add_consultation_type_modal").modal("show");
}