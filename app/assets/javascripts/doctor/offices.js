//= require doctor/office_template_prescription
var office_to_template = 0;
$(function(){

    if($('#office_consult_cost').length){
        $('#office_consult_cost').mask('0000.00', {reverse: true});
    }

    $(".day_select").click(function(){
        if($(this).is(":checked")){
            $(this).addClass("recent_checked");
            $("#day_"+$(this).val()).show();
        }else{
            $(this).removeClass("recent_checked");
            $("#day_"+$(this).val()).hide();
        }
    });

    $("#schedule_changes").click(function(){
        if($(this).is(":checked")){
            enableScheduleChanges();
        }else{
            disableScheduleChanges();
        }
    });

    $("#save_schedule").click(function(){
        $("#save_schedule").attr('disabled','disabled');
        var blank_error = false;
        var time_error = false;
        var no_days_error = true;
        var time_extra_message = "";
        /*var data = {
            days: []
        }; */
        var data = new FormData();
        var data_form_error = false;
        var input_message = "";

        if($("#office_hospital_id").val()=="" || $("#office_hospital_id").val()==null){
            input_message += "El campo hospital no puede estar vacío <br>";
            data_form_error = true;
        }

        if($("#office_name").val()==""){
            input_message += "El campo nombre no puede estar vacío <br>";
            data_form_error = true;
        }
        if($("#office_address").val()==""){
            input_message += "El campo dirección no puede estar vacío <br>";
            data_form_error = true;
        }
        if($("#office_telephone").val()==""){
            input_message += "El campo teléfono no puede estar vacío <br>";
            data_form_error = true;
        }
        if($("#client_office_city_id").val()==""){
            input_message += "El campo ciudad no puede estar vacio <br>";
            data_form_error = true;
        }
        if($("#office_folio").val()==""){
            input_message += "El campo Folio no puede estar vacio <br>";
            data_form_error = true;
        }
        //if(isNaN($("#office_consult_cost").val())==true){
         //   input_message += "El campo costo solo acepta numeros  <br>";
          //  data_form_error = true;
        //}
        if($("#office_telephone").val().length<10){
            input_message += "El campo teléfono debe tener al menos 10 dígitos  <br>";
            data_form_error = true;
        }



        var checkdays = $(".day_select");
        var days = [];
        $.each(checkdays,function(idx,check){
            if($(check).is(":checked")){
                no_days_error = false;
                var day = $(check).val();
                var time_inputs = $("#day_"+day).find(".time-picker");
                var time_records = new Array();
                var last_start_time = "";
                var last_end_time = "";
                var last_start_time_s = "";
                var last_end_time_s = "";
                $.each(time_inputs,function(i,input){
                    var time = $(input).val();
                    if(time == ""){
                        blank_error = true;
                    }
                    if($(input).hasClass("end-time")){
                        last_end_time = getTimeValue(time);
                        last_end_time_s = time;
                        if(last_end_time <= last_start_time){
                            console.log(last_end_time +" - "+ last_start_time);
                            time_error = true;
                            time_extra_message = "La hora final no puede ser menor o igual a la inical.";
                        }
                        time_records.push({
                            start:last_start_time_s,
                            end:last_end_time_s
                        });

                    }else if($(input).hasClass("start-time")){
                        last_start_time = getTimeValue(time);
                        last_start_time_s = time;
                        if(last_end_time != "" &&   last_start_time < last_end_time){
                            time_error = true;
                            time_extra_message = "La hora inicial no puede ser menor a la última hora final";
                        }
                    }
                    if(blank_error || time_error){
                        $("#day_"+day).find(".form-control").css("color","red");
                        return false;
                    }else{
                        $("#day_"+day).find(".form-control").css("color","black");
                    }

                });

                //days.push({
                //    day:day,
                //    times:time_records
                //});
                var aux = new Array();
                aux["day"] = day;
                aux["times"] = time_records;
                data.append("days[]", JSON.stringify({
                        day:day,
                        times:time_records
                    }));
            }
        });
        //var format_json = JSON.stringify(days);
        //data.append("days[]", days);

        var message = "";
        if(blank_error){
            message = "Error! Parece que hay campos vacíos. Por favor verifique.";
        }
        if(time_error){
            message = "Error! Parece que hay información incongruente de horarios. "+time_extra_message;
        }

        if(no_days_error){
            message = "Error! Es necesario seleccionar al menos un día de la semana.";
        }

        if(blank_error || time_error || no_days_error || data_form_error){
            $("#text_errors_msg").html(input_message);
            $("#schedule_errors_msg").html(message);
            $("#schedule_errors").show();
            $("#text_errors_msg").show();
            $("#save_schedule").removeAttr("disabled");
        }else{
            $("#text_errors_msg").hide();
            $("#schedule_errors").hide();
            var url = $('#url_doctor_offices').val()+"/";
            var method = "POST";
            var office_id = $("#office_id").val();
            data.append("name", $("#office_name").val());
            data.append("address", $("#office_address").val());
            data.append("telephone", $("#office_telephone").val());
            data.append("consult_duration", $("#office_consult_duration").val());
            data.append("city_id", $("#client_office_city_id_ts_autocomplete").val());
            data.append("lat",$("#office_lat").val());
            data.append("lng",$("#office_lng").val());
            data.append("hospital_id",$("#office_hospital_id").val());
            data.append("folio",$("#office_folio").val());

            if(office_id != ""){
                url +=office_id;
                method = "PUT";
                data.append("office_id", office_id);
                data.append("update_schedule",$("#schedule_changes").is(":checked") ? 1 : 0)
            }
            var files = jQuery('#office_logo').prop('files');
            if ($('#file_logo').hasClass("fileinput-exists")){
                jQuery.each(files, function(i, file) {
                    data.append('logo', file);
                });
            }else{
                data.append("delete_logo","true");
            }


            $.ajax({
                url:url,
                method:method,
                dataType:"json",
                cache: false,
                contentType: false,
                processData: false,
                data:data,
                success:function(response){
                    console.log(response);
                    if(response.status == "error"){
                        $.each(response.errors, function(index,val){
                            input_message += val+" <br>"

                        });
                        message = response.message;

                        $("#text_errors_msg").html(input_message);
                        $("#schedule_errors_msg").html(message);
                        $("#schedule_errors").show();
                        $("#text_errors_msg").show();
                        $("#save_schedule").removeAttr("disabled");
                    }else{
                        location.href = $('#url_doctor_offices').val();
                    }
                }
            });
        }
    });
});

function getTimeValue(str){
    if(str == ""){
        return;
    }
    var hourCom = str.split(" ");
    var format = hourCom[1];

    var times = hourCom[0].split(":");
    var hour = parseInt(times[0]);
    var minute = parseInt(times[1]);

    if(format.toLowerCase() == "pm" && hour != 12){
        hour += 12;
    }
    if(format.toLowerCase() == "am" && hour == 12){
        hour = 0;
    }
    if(minute < 10){
        minute = "0"+minute;
    }

    var result = parseInt(hour+""+minute);
    return result;
}

function init(){
    $('.time-picker').datetimepicker({
        format: 'LT'
    });

    $(".delete-schedule-btn").click(function(){
        $(this).parent().parent().remove();
    });
}

function disableScheduleChanges(){
    $(".delete-schedule-btn").hide();
    $(".add-schedule-btn").hide();
    $(".day_select").prop("disabled",true);
    $(".hours .time-picker").prop("disabled",true);
    $(".hours .time-picker").css("color","black");
    $(".new-schedule-div").remove();
    var recent_checks = $(".recent_checked");
    $.each(recent_checks,function(idx,c){
        $("#day_"+$(c).val()).hide();
        $(c).removeClass("recent_checked");
        $(c).prop("checked",false);
    });
}
function enableScheduleChanges(){
    $(".delete-schedule-btn").show();
    $(".add-schedule-btn").show();
    $(".day_select").prop("disabled",false);
    $(".hours .time-picker").prop("disabled",false);
}

function addScheduleByDay(day){
    var html = '<div class="new-schedule-div"><div class="col-xs-4 time-input-holder">'+
        '<div class="input-group form-group">'+
        '<span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>'+
        '<div class="dtp-container fg-line">'+
        '<input type="text" class="form-control time-picker input-lg start-time" placeholder="H. Inicial">'+
        '</div>'+
        '</div>'+
        '</div>'+
        '<div class="col-xs-4 time-input-holder">'+
        '<div class="input-group form-group">'+
        '<span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>'+
        '<div class="dtp-container fg-line">'+
        '<input type="text" class="form-control time-picker input-lg end-time" placeholder="H. Final">'+
        '</div>'+
        '</div>'+
        '</div><div class="col-xs-4 time-input-holder"><i class="zmdi zmdi-delete delete-schedule-btn"></i></div></div>';

    $("#day_"+day+" .hours").append(html);
    init();
}
function loadOffices(user){
    $.ajax({
        url: $("#url_get_user_offices").val(),
        dataType: "json",
        data: {user_id:user},
        success: function (data) {
            $("#offices_div").show();
            $.each(data, function (idx, info) {
                $("#"+info.name).prop('checked', true);
            });

        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function saveOffices(){
    var offices = [];
    $.each($(".s_parent"),function(idx, item){
        if($(item).is(":checked")){
            offices.push({
                office_id:$(item).val()
            });
        }
    });
    if (offices.length == 0){
        swal("Error!", "Debe seleccionar un usuario y los consultorios a las cual tendra acceso.", "error");
        return;
    }
    $.ajax({
        url: $("#url_save_user_offices").val(),
        dataType: "json",
        data: {user_id:$("#user_name_ts_autocomplete").val(),offices:offices},
        success: function (data) {
            swal("Exito!", "Lo(s) consultorio(s) han sido guardado.", "success");
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function resetChecks() {
    $.each($(".s_parent"),function(idx, item){
        if($(item).is(":checked")){
            $(item).prop('checked', false);
        }
    });
}
jQuery(document).ready(function() {
    $("#offices_div").hide();
    $("#user_name").ts_autocomplete({
        url:AUTOCOMPLETE_PERSONS_URL,
        items:10,
        callback:function(val, text){
            resetChecks();
            loadOffices(val);
        }
    });
    $('form#new_office').find("input[type=text], textarea").val("")
    init();
    if($("#office_id").val() != ""){
        disableScheduleChanges();
    }
    var table = $("#data-table-office");
    var grid = $("#data-table-office").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-office').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-edit\" title=\"Ver\"></i></a>" +
                    "<a href='#' onclick='show_office("+row.id+")' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>" +
                    "<a  href='javascript:void(0)' onclick='set_office_template("+row.id+","+row.template_id+")' class=\"btn btn-success btn-icon waves-effect waves-circle waves-float btn-manage\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-receipt zmdi-hc-fw\" title=\"Plantilla\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\" ></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el consultorio "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Eliminar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El consultorio ha sido eliminado exitosamente.", "success");
                        location.reload();
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

    $('#btn-save-office-template').click(function(){
        var url_aux = $('#url_add_template').val();
        var template = $("input[type='radio']:checked").val();
        var ajax = $.ajax({
            type: 'post',
            url: url_aux,
            data: {id:office_to_template, template: template}
        });
        ajax.done(function(result, status){
            $("#newTemplate").modal("hide");
            swal("Exito!", "La plantilla de receta se ha asociado al consultorio.", "success");
        });

        ajax.fail(function(error, status, msg){
            swal("Error!", "Consulte al administrador.", "error");
        });

    });

});
function set_office_template(id,template_id){
    if(template_id != 0){
        if(template_id == 1){
            $("#template_a").prop("checked", true);
        }else if(template_id == 2){
            $("#template_b").prop("checked", true);
        }else{
            $("#template_c").prop("checked", true);
        }
    }
    office_to_template = id;
    $("#newTemplate").modal("show");
}
function dontSetLocationToOffice(){
    $("#office_lat").val("");
    $("#office_lng").val("");

    removeMarker();
}

function show_office(office_id){
    $.ajax({
        url: $('#url_show_office').val(),
        dataType: "json",
        data: {office_id:office_id},
        success: function (data) {
            $.each(data.office, function (idx, info) {
                $("#o_name").text(info.name);
                $("#o_cost").text(parseFloat(info.cost).toFixed(2));
                $("#o_duration").text(info.duration);
                $("#o_telephone").text(info.telephone);
                $("#o_hospital").text(info.hospital);
                $("#o_address").text(info.address);
                $("#o_city").text(info.city);
                $("#o_folio").text(info.folio);
            });
            var tabla_per = "<table id='tablatime' class='table table-striped'><thead><tr>"+
                "<th>Domingo</th>"+
                "<th>Lunes</th>"+
                "<th>Martes</th>"+
                "<th>Miercoles</th>"+
                "<th>Jueves</th>"+
                "<th>Viernes</th>"+
                "<th>Sabado</th>"+
                "</tr></thead><tbody><tr>";
            lunes = [];
            martes = [];
            miercoles = [];
            jueves = [];
            viernes = [];
            sabado = [];
            domingo = [];
            $.each(data.time, function (idx, info) {
                switch(info.day) {
                    case 0:
                        domingo.push(info.hour);
                        break;
                    case 1:
                        lunes.push(info.hour);
                        break;
                    case 2:
                        martes.push(info.hour);
                        break;
                    case 3:
                        miercoles.push(info.hour);
                        break;
                    case 4:
                        jueves.push(info.hour);
                        break;
                    case 5:
                        viernes.push(info.hour);
                        break;
                    case 6:
                        sabado.push(info.hour);
                        break;
                    default:console.log(info.hour + " " + info.hour);
                }
            });
            cadena = "";
            for(cont=0; cont<domingo.length; cont++){
                cadena += domingo[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            for(cont=0; cont<lunes.length; cont++){
                cadena += lunes[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            for(cont=0; cont<martes.length; cont++){
                cadena += martes[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            for(cont=0; cont<miercoles.length; cont++){
                cadena += miercoles[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            for(cont=0; cont<jueves.length; cont++){
                cadena += jueves[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            for(cont=0; cont<viernes.length; cont++){
                cadena += viernes[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            for(cont=0; cont<sabado.length; cont++){
                cadena += sabado[cont] + "<br>";
            }
            tabla_per += "<td >"+cadena+"</td>";
            cadena = "";
            tabla_per += "</tr></tbody></table>";
            $('#table_times').html(tabla_per);

        },
        error:function(data){
            console.log("error");
        }
    });
    $('#show_office_modal').modal('show');
}

