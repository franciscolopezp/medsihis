function saveGynecologistObstetricalBackground(){

    var medical_history_id = $("#medical_history_id_ifb").val();

    var gynecologist_obstetrical_background = {
        menstruation_start_age:$("#gynecologist_obstetrical_background_menstruation_start_age").val(),
        married:$("#gynecologist_obstetrical_background_married").is(":checked"),
        spouse_name:$("#gynecologist_obstetrical_background_spouse_name").val(),
        gestations:$("#gynecologist_obstetrical_background_gestations").val(),
        natural_birth:$("#gynecologist_obstetrical_background_natural_birth").val(),
        caesarean_surgery:$("#gynecologist_obstetrical_background_caesarean_surgery").val(),
        abortion_number:$("#gynecologist_obstetrical_background_abortion_number").val(),
        abortion_causes:$("#gynecologist_obstetrical_background_abortion_causes").val(),
        sexually_active:$("#gynecologist_obstetrical_background_sexually_active").is(":checked"),
        contraceptives_method:$("#gynecologist_obstetrical_background_contraceptives_method").val(),
        menopause:$("#gynecologist_obstetrical_background_menopause").val(),
        information_spouse:$("#gynecologist_obstetrical_background_information_spouse").val(),
        menses_last_date:$("#gynecologist_obstetrical_background_menses_last_date").ts_date(),
        papanicolau_last_date:$("#gynecologist_obstetrical_background_papanicolau_last_date").ts_date(),
        cycles:$("#gynecologist_obstetrical_background_cycles").val(),
        ivs:$("#gynecologist_obstetrical_background_ivs").val(),
        fup:$("#gynecologist_obstetrical_background_fup").ts_date(),
        sexual_partners:$("#gynecologist_obstetrical_background_sexual_partners").val(),
        relationship_type:$("#gynecologist_obstetrical_background_relationship_type").val(),
        std:$("#gynecologist_obstetrical_background_std").val(),
        anotations:$("#gynecologist_obstetrical_background_anotations").val()
    };


    $.ajax({
        url:"/doctor/update_gynecologist_obstetrical_backgrounds",
        dataType:"json",
        data:{
            gynecologist_obstetrical_background:gynecologist_obstetrical_background,
            medical_history_id:medical_history_id
        },
        success:function(data){
            $("#success_message_gynecologist_obstetrical_background").show();
            setTimeout(function(){
                $("#success_message_gynecologist_obstetrical_background").hide();
            },1500);
        },
        error:function(){
            swal("Error","Ocurrio un error inesperado. Por favor intente otra vez.","error");
        }
    });

}