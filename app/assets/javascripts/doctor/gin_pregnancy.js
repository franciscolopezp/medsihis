var pregnancies_url = "/doctor/pregnancies/";
function openAddPregnancyDialog(){
    $("#last_menstruation_date").val("");
    $("#probable_birth_date").val("");
    $("#baby_name").val("");
    $("#pregnancy_id").val("");
    $("#add_pregnancy_modal").modal("show");
}

function setupEditPregnancyModal(id, last_menstruation_date, probable_birth_date, initial_weight, baby_name) {
    $("#last_menstruation_date").val(last_menstruation_date);
    $("#probable_birth_date").val(probable_birth_date);
    $("#initial_weight").val(initial_weight);
    $("#baby_name").val(baby_name);
    $("#pregnancy_id").val(id);
    $("#add_pregnancy_modal_title").html("Editar datos del embarazo");

    $("#add_pregnancy_modal").modal("show");
}

function savePregnancyRecord(){

    var fields = ["#last_menstruation_date","#probable_birth_date","#initial_weight"];
    var validForm = true;
    $.each(fields,function(idx, field){
        if($(field).val() == ""){
            validForm = false;
        }
    });

    if(!validForm){
        swal("Por favor complete los campos correctamente");
        return;
    }

    var medical_expedient_id = $("#preg_medical_expedient_id").val();
    var pregnancy = {
        last_menstruation_date:$("#last_menstruation_date").ts_date(),
        probable_birth_date:$("#probable_birth_date").ts_date(),
        initial_weight:$("#initial_weight").val(),
        baby_name:$("#baby_name").val()
    };

    var method = "POST";
    var url = pregnancies_url


    if( $("#pregnancy_id").val() != "" ){
        method = "PUT";
        url +=  $("#pregnancy_id").val();
        pregnancy.id = $("#pregnancy_id").val();
    }

    $.ajax({
        url : url,
        dataType:"json",
        method:method,
        data:{
            medical_expedient_id:medical_expedient_id,
            pregnancy:pregnancy
        },
        success:function(data){
            $("#add_pregnancy_modal").modal("hide");
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            loadPregnacies();
        }
    });

}


function calculateProbableBirthDate(){
    if($("#last_menstruation_date").val() == ""){
        swal("Por favor indique la fecha de la última menstruación.");
        return;
    }
    var date = $("#last_menstruation_date").ts_date();
    var result = new Date(date);
    result.setDate(result.getDate() + 280);

    $("#probable_birth_date").val(dateToString(result,"dd/mm/yyyy"));
}

function loadPregnacies(){
    var medical_expedient_id = $("#preg_medical_expedient_id").val();
    $.ajax({
        url : pregnancies_url,
        data:{
            medical_expedient_id:medical_expedient_id
        },
        success:function(data){
            $("#pregnacies_holder").html(data);
            initPageComponents();
            initUltrasoundComponents();
        }
    });

}

function initPageComponents(){
    $('.date-picker').datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $("#probable_birth_date").click(calculateProbableBirthDate);
    var data = JSON.parse($("#pregnancies_data").val());
    generatePregnancyCharts(false,data);

    $("#birth_info_birth_date").datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $("#birth_info_birth_hour").datetimepicker({
        format: 'LT'
    });
    $("#neonate_info_birthdate").datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $("#neonate_info_birthhour").datetimepicker({
        format: 'LT'
    });

    $("#birth_info_birth_date_edit").datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $("#birth_info_birth_hour_edit").datetimepicker({
        format: 'LT'
    });

    $("#neonate_info_birthdate_edit").datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $("#neonate_info_birthhour_edit").datetimepicker({
        format: 'LT'
    });


    $("#abortion_info_date").datetimepicker({
        format: 'DD/MM/YYYY'
    });
    $("#abortion_info_hour").datetimepicker({
        format: 'LT'
    });

    $("input[name=pregnancy_finish]").change(function(){
        if($(this).val() == "new_birth"){
            $("#pregnancy_success").show();
            $("#pregnancy_abortion").hide();
        }else{
            $("#pregnancy_success").hide();
            $("#pregnancy_abortion").show();
        }
    });

}

function appendItemToUltrasoundTable(item){
    var diff = 40 - item.weeks;
    var dateAux = new Date();
    dateAux.setDate(item.date.getDate() + (diff * 7) );
    $("#ultrasounds_records tbody").append("<tr id='ultrasound_rec_"+item.id+"'><td>"+dateToString(item.date,"dd/mm/yyyy")+"</td>" +
        "<td>"+item.weeks+"</td>" +
        "<td>"+dateToString(dateAux,"dd/mm/yyyy")+"</td>"+
        "<td style='font-size: 20px;'><i class='zmdi zmdi-edit edit_ultrasound_record' rec='"+item.id+"'></i> <i class='zmdi zmdi-delete delete_ultrasound_record' rec='"+item.id+"'></i> " +
        "</td></tr>"
    );

    $(".delete_ultrasound_record").unbind("click");
    $(".delete_ultrasound_record").click(function(){
        var id = $(this).attr("rec");
        swal({
            title: "¿Eliminar registro de ultrasonido?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: true
        },
        function(){
            $.ajax({
                url:$("#url_ultrasounds").val()+"/"+id,
                method:"DELETE",
                dataType:"json",
                success:function(data){
                    $("#ultrasound_rec_"+id).remove();
                }
            });
        });
    });

    $(".edit_ultrasound_record").unbind("click");

    $(".edit_ultrasound_record").click(function(){
        var id = $(this).attr("rec");
        $("#ultrasound_rec_"+id).hide();
        $("#ultrasound_rec_"+id).after("<tr id='ultrasound_edit_"+id+"'>" +
            "<td><input style='width: 100%' type='text' value='"+$($("#ultrasound_rec_"+id+" td")[0]).html()+"'></td>" +
            "<td><input style='width: 100%' type='text' value='"+$($("#ultrasound_rec_"+id+" td")[1]).html()+"'></td>" +
            "<td>"+$($("#ultrasound_rec_"+id+" td")[2]).html()+"</td>"+
            "<td style='font-size: 20px;'><i class='zmdi zmdi-check-circle update_ultrasound' rec='"+id+"'></i> " +
            "<i class='zmdi zmdi-close-circle close_edit_ultrasound' rec='"+id+"'></i></td></tr>");


        $(".close_edit_ultrasound").unbind("click");
        $(".close_edit_ultrasound").click(function(){
            var id = $(this).attr("rec");
            $("#ultrasound_rec_"+id).show();
            $("#ultrasound_edit_"+id).remove();
        });

        $(".update_ultrasound").unbind("click");
        $(".update_ultrasound").click(function(){
            var id = $(this).attr("rec");
            $.ajax({
                url:$("#url_ultrasounds").val()+"/"+id,
                method:"PUT",
                data:{
                    date:$($("#ultrasound_edit_"+id+" input")[0]).val(),
                    weeks:$($("#ultrasound_edit_"+id+" input")[1]).val()
                },
                dataType:"json",
                success:function(data){
                    loadUltrasounds($("#pregnancy_id_ultrasound").val());
                }
            });
        });

    });
}


function confirmDeletePregnancy(id){
    swal({
            title: "Eliminar embarazo",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){

            $.ajax({
                url:pregnancies_url+id,
                method:"DELETE",
                dataType:"json",
                success:function(data){
                    swal("Eliminado!", "Los datos del embarazo se eliminaron correctamente.", "success");
                    loadPregnacies();
                }
            });



        });
}

function openFinishPregnancyModal(id){
    birthInfoData = {
        neonateInfos:[]
    };
    $("#finish_pregnancy_modal").modal("show");
    $("#pregnancy_end_id").val(id);
}


function finishPregnancy(){
    if($("input[name=pregnancy_finish]").filter(":checked").val() == undefined){
        swal("Error","Por favor seleccione el tipo de finalización del embarazo","error");
        return;
    }

    var success = $("#pregnancy_end_success").is(":checked");
    var data = {};
    var pregnancy_end_date = 0;

    if(success){
        var validForm = true;
        var validateFieldsBirthInfo = ["birth_info_birth_date","birth_info_birth_hour","birth_info_hospital"];
        $.each(validateFieldsBirthInfo,function(idx,input){
            if($("#"+input).val() == ""){
                validForm = false;
            }
        });
        if(!validForm){
            swal("Error","Por favor llene los datos del parto correctamente","error");
            return;
        }

        data = getBirthInfo();

        pregnancy_end_date = data.date;

        if(data.neonateInfos.length == 0){
            swal("Error","Por favor agregue información del neonato","error");
            return;
        }

    }else{
        var validForm = true;
        var validateFieldsBirthInfo = ["abortion_info_date","abortion_info_hour","abortion_info_hospital"];
        $.each(validateFieldsBirthInfo,function(idx,input){
            if($("#"+input).val() == ""){
                validForm = false;
            }
        });
        if(!validForm){
            swal("Error","Por favor llene los datos del aborto correctamente","error");
            return;
        }

        data = getAbortionInfo();
        pregnancy_end_date = data.date;
    }

    $("#finish_pregnancy_modal").modal("hide");
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();


    $.ajax({
        url:pregnancies_url+"finish",
        dataType:"json",
        data:{
            success:success,
            pregnancy_end_date:pregnancy_end_date,
            id:$("#pregnancy_end_id").val(),
            info:data
        },
        success:function(data){
            swal("Registro actualizado!", "Los datos del embarazo se actualziaron correctamente.", "success");
            loadPregnacies();
        }
    });
}

function openExplorationDetails(consult_id){
    $("#exploration_c"+consult_id).show(200);
    $("#icon_c"+consult_id).removeClass("zmdi-caret-right-circle");
    $("#icon_c"+consult_id).addClass("zmdi-caret-down-circle");
    $("#icon_c"+consult_id).attr("onclick","closeExplorationDetails("+consult_id+")");

}

function closeExplorationDetails(consult_id){
    $("#exploration_c"+consult_id).hide(200);
    $("#icon_c"+consult_id).removeClass("zmdi-caret-down-circle");
    $("#icon_c"+consult_id).addClass("zmdi-caret-right-circle");
    $("#icon_c"+consult_id).attr("onclick","openExplorationDetails("+consult_id+")");
}


var weight_per_week = [0/*added week 0 index to match values with correct index*/,
    0.00, 0.00, 0.00, 0.00,
    0.13, 0.25, 0.38, 0.50,
    0.70, 0.90, 1.10, 1.30,
    1.50, 1.75, 2.00, 2.25,
    2.50, 2.75, 3.00, 3.25,
    3.50, 3.83, 4.17, 4.50,
    4.83, 5.17, 5.50, 6.00,
    6.50, 7.00, 7.50, 8.00,
    8.50, 9.00, 9.50, 9.90,
    10.30, 10.70, 11.10, 11.50];

function generatePregnancyCharts(showConsultation,dataJson){

    var data = dataJson;
    $.each(data,function(idx,pregnancy){
        var records = [];
        records.push({
            date:pregnancy.last_menstruation_date,
            week:"Última Regla",
            current: pregnancy.initial_weight,
            recommended: pregnancy.initial_weight
        });

        $.each(pregnancy.consultations,function(index,consult){
            records.push({
                date:consult.date,
                week:"Semana "+consult.week ,
                current: parseFloat(consult.weight),
                recommended: parseFloat( pregnancy.initial_weight + weight_per_week[consult.week])
            });
        });
        var id = null;
        if(!showConsultation){
           id = "pregnancy_chart_"+pregnancy.id;
        }else{
           id = "div_fpp_consultation";
        }

        var pregnancy_chart = new Morris.Line({
            element: id,
            data: records,
            parseTime:false,
            xkey: 'week',
            ykeys: ['recommended','current'],
            ymin: pregnancy.initial_weight,
            xLabelMargin: 100,
            labels: ['Peso Sugerido','Peso Actual'],
            yLabelFormat: function (y) {
                return parseFloat(y).toFixed(2);
            },
            xLabelAngle: 45,
            hoverCallback: function(index, options, content) {
                var rec = options.data[index];
                var custom_html = "<strong>"+rec.week+"</strong><br>";
                custom_html += "<label style='color: #0b62a4;'> Peso sugerido: "+rec.recommended+"</label><br>";
                custom_html += "<label style='color: #CF1D46;'> Peso registrado: "+rec.current+"</label><br>";

                custom_html += "<strong>Fecha: "+rec.date+"</strong>";
                return(custom_html);
            },
            lineColors: ['#0b62a4','#CF1D46'],
            pointFillColors: ['#0b62a4','#CF1D46']
        });
    });

}


var birthInfoData = {
    neonateInfos:[]
};
function addNeonateInfo(){
    var fields = ["neonate_info_child_name","neonate_info_birthdate","neonate_info_birthhour","neonate_info_weight","neonate_info_height"];
    var valid = true;
    $.each(fields,function (idx,field) {
        if($("#"+field).val() == ""){
            valid = false;
        }
    });

    if(!valid){
        swal("Error","Por favor llene los datos del neonato correctamente","error");
        return;
    }


    var neonateInfo = {};
    neonateInfo.name = $("#neonate_info_child_name").val();
    neonateInfo.gender = $("#neonate_info_gender").val();
    neonateInfo.birthdate = $("#neonate_info_birthhour").ts_hour($("#neonate_info_birthdate").ts_date());
    neonateInfo.weight = $("#neonate_info_weight").val();
    neonateInfo.height = $("#neonate_info_height").val();
    neonateInfo.apgar_appearance = $("#neonate_info_apgar_appearance").val();
    neonateInfo.apgar_pulse = $("#neonate_info_apgar_pulse").val();
    neonateInfo.apgar_gesture = $("#neonate_info_apgar_gesture").val();
    neonateInfo.apgar_activity= $("#neonate_info_apgar_activity").val();
    neonateInfo.apgar_breathing = $("#neonate_info_apgar_breathing").val();
    neonateInfo.blood_type = $("#neonate_info_blood_type").val();
    neonateInfo.abnormalities = $("#neonate_info_abnormalities").val();
    neonateInfo.observations = $("#neonate_info_observations").val();
    neonateInfo.attended = $("#neonate_info_attended").val();
    neonateInfo.pediatrician = $("#neonate_info_pediatrician").val();


    var today = new Date();
    if(today < neonateInfo.birthdate){
        swal("Error","La fecha de nacimiento no puede ser posterior a la actual","error");
        return;
    }

    if(!neonateInfo.height.match(/^\d+$/)){
        swal("Error","La altura debe tener un valor entero. (En centímetros)","error");
        return;
    }

    if(isNaN(neonateInfo.weight)){
        swal("Error","El peso debe tener valores numéricos","error");
        return;
    }

    var apgar_fields = ["neonate_info_apgar_appearance","neonate_info_apgar_pulse","neonate_info_apgar_gesture","neonate_info_apgar_activity","neonate_info_apgar_breathing"];
    var valid_apgar = true;
    $.each(apgar_fields,function(idx, input){
        if($("#"+input).val() != "" && [0,1,2].indexOf(parseInt($("#"+input).val())) == -1){
            valid_apgar = false;
        }
    });
    if(!valid_apgar){
        swal("Error","Los valores del apgar deben estar entre 0 y 2","error");
        return;
    }

    birthInfoData.neonateInfos.push(neonateInfo);

    var html = "";
    html += "<div>" +
        "<strong>Nombre: </strong>"+ neonateInfo.name +
        "&nbsp;&nbsp;&nbsp;&nbsp;<strong>Género: </strong>"+ $("#neonate_info_"+neonateInfo.gender).html() +
        "&nbsp;&nbsp;&nbsp;&nbsp;<strong>Fecha de nacimiento: </strong>"+ dateToString(neonateInfo.birthdate,'dd fm yyyy hh:mn pp') +
        "<br><strong>Peso: </strong>"+ neonateInfo.weight +
        "&nbsp;&nbsp;&nbsp;&nbsp;<strong>Altura: </strong>"+ neonateInfo.height +
        "<br><strong>A: </strong>"+ neonateInfo.apgar_appearance +
        "&nbsp;&nbsp;<strong>P: </strong>"+ neonateInfo.apgar_pulse +
        "&nbsp;&nbsp;<strong>G: </strong>"+ neonateInfo.apgar_gesture +
        "&nbsp;&nbsp;<strong>A: </strong>"+ neonateInfo.apgar_activity +
        "&nbsp;&nbsp;<strong>R: </strong>"+ neonateInfo.apgar_breathing +
        "<br><strong>Tipo de sangre: </strong>"+ neonateInfo.blood_type+
        "<br><strong>Anormalidades: </strong>"+ neonateInfo.abnormalities +
        "<br><strong>Observaciones: </strong>"+ neonateInfo.observations +
        "<br><strong>Atendió: </strong>"+ neonateInfo.attended +
        "<br><strong>Pediatra: </strong>"+ neonateInfo.pediatrician +
        "</div>";

    $("#new_neonate_info_form").hide();
    $("#neonate_info_data").append(html);
}

function showNeonateForm(){
    var fields = ["neonate_info_child_name","neonate_info_birthdate","neonate_info_birthhour","neonate_info_weight","neonate_info_height",
    "neonate_info_apgar_appearance","neonate_info_apgar_pulse","neonate_info_apgar_gesture","neonate_info_apgar_activity","neonate_info_apgar_breathing",
        "neonate_info_abnormalities","neonate_info_observations","neonate_info_attended","neonate_info_pediatrician"];
    $.each(fields,function(idx,input){
        $("#"+input).val("");
    });
    $("#new_neonate_info_form").show();
}

function getBirthInfo(){
    birthInfoData.date = $("#birth_info_birth_hour").ts_hour($("#birth_info_birth_date").ts_date());
    birthInfoData.hospital = $("#birth_info_hospital").val();
    birthInfoData.birth_type = $("#birth_info_birth_type").val();
    birthInfoData.observations = $("#birth_info_observations").val();
    birthInfoData.rpm = $("#birth_info_rpm").val();
    birthInfoData.delivery_type = $("input[name=birth_info_delivery_type]").filter(':checked').val();
    birthInfoData.episiotomy = $("input[name=birth_info_episiotomy_type]").filter(':checked').val();
    birthInfoData.tear = $("input[name=birth_info_tear_type]").filter(':checked').val();
    birthInfoData.duration = $("input[name=birth_info_duration_type]").filter(':checked').val();
    birthInfoData.aditional_observations = $("#birth_info_aditional_observations").val();

    if(birthInfoData.delivery_type == undefined)
        birthInfoData.delivery_type = null;

    if(birthInfoData.episiotomy == undefined)
        birthInfoData.episiotomy = null;

    if(birthInfoData.tear == undefined)
        birthInfoData.tear = null;

    if(birthInfoData.duration == undefined)
        birthInfoData.duration = null;


    return birthInfoData;
}

function getAbortionInfo(){
    var abortion = {};
    abortion.date = $("#abortion_info_hour").ts_hour($("#abortion_info_date").ts_date());
    abortion.hospital = $("#abortion_info_hospital").val();
    abortion.type = $("#abortion_info_type").val();
    abortion.observations = $("#abortion_info_observations").val();

    return abortion;
}

function openEditBirthInfoModal(birth_info_id){
    $("#birth_info_id").val(birth_info_id);
    $("#edit_birth_info_modal").modal("show");
    $.ajax({
        url:$("#get_birth_info_url").val(),
        data:{id:birth_info_id},
        dataType:"json",
        success:function(data){
            console.log(data);

            $("#birth_info_birth_hour_edit").val(dateToString(new Date(data.date),"hh:mn pp"));
            $("#birth_info_birth_date_edit").val(dateToString(new Date(data.date),"dd/mm/yyyy"))
            $("#birth_info_hospital_edit").val(data.hospital);
            $("#birth_info_birth_type_edit").val(data.birth_type_id);
            $("#birth_info_observations_edit").val(data.observations);
            $("#birth_info_rpm_edit").val(data.rpm);
            if(data.delivery_type_id != undefined)
            $("input[name=birth_info_delivery_type_edit]").filter('[value='+data.delivery_type_id+']').prop("checked",true);
            if(data.episiotomy_type_id != undefined)
            $("input[name=birth_info_episiotomy_type_edit]").filter('[value='+data.episiotomy_type_id+']').prop("checked",true);
            if(data.tear_type_id != undefined)
            $("input[name=birth_info_tear_type_edit]").filter('[value='+data.tear_type_id+']').prop("checked",true);
            if(data.duration_type_id != undefined)
            $("input[name=birth_info_duration_type_edit]").filter('[value='+data.duration_type_id+']').prop("checked",true);
            $("#birth_info_aditional_observations_edit").val(data.aditional_observations);

        }
    });
}

function updateBirthInfo() {
    var params = {};
    params.id = $("#birth_info_id").val();
    params.date = $("#birth_info_birth_hour_edit").ts_hour($("#birth_info_birth_date_edit").ts_date());
    params.hospital = $("#birth_info_hospital_edit").val();
    params.birth_type = $("#birth_info_birth_type_edit").val();
    params.observations = $("#birth_info_observations_edit").val();
    params.rpm = $("#birth_info_rpm_edit").val();
    params.delivery_type = $("input[name=birth_info_delivery_type_edit]").filter(':checked').val();
    params.episiotomy = $("input[name=birth_info_episiotomy_type_edit]").filter(':checked').val();
    params.tear = $("input[name=birth_info_tear_type_edit]").filter(':checked').val();
    params.duration = $("input[name=birth_info_duration_type_edit]").filter(':checked').val();
    params.aditional_observations = $("#birth_info_aditional_observations_edit").val();

    if(params.delivery_type == undefined)
        params.delivery_type = null;

    if(params.episiotomy == undefined)
        params.episiotomy = null;

    if(params.tear == undefined)
        params.tear = null;

    if(params.duration == undefined)
        params.duration = null;

    $.ajax({
        url:$("#update_birth_info_url").val(),
        dataType:"json",
        data:params,
        success:function(data){
            $("#edit_birth_info_modal").modal("hide");
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            loadPregnacies();
        }
    });
}

function openEditNeonateInfoModal(neonate_info_id){
    $("#neonate_info_id").val(neonate_info_id);
    $("#edit_neonate_info_modal").modal("show");
    $.ajax({
        url:$("#get_neonate_info_url").val(),
        dataType:"json",
        data:{id:neonate_info_id},
        success:function(data){
            console.log(data);
            $("#neonate_info_child_name_edit").val(data.child_name);
            $("#neonate_info_gender_edit").val(data.gender_id);
            $("#neonate_info_birthdate_edit").val(dateToString(new Date(data.birthdate),"dd/mm/yyyy"));
            $("#neonate_info_birthhour_edit").val(dateToString(new Date(data.birthdate),"hh:mn pp"));
            $("#neonate_info_weight_edit").val(data.weight);
            $("#neonate_info_height_edit").val(data.height);
            $("#neonate_info_apgar_appearance_edit").val(data.apgar_appearance);
            $("#neonate_info_apgar_pulse_edit").val(data.apgar_pulse);
            $("#neonate_info_apgar_gesture_edit").val(data.apgar_gesture);
            $("#neonate_info_apgar_activity_edit").val(data.apgar_activity);
            $("#neonate_info_apgar_breathing_edit").val(data.apgar_breathing);
            $("#neonate_info_blood_type_edit").val(data.blood_type);
            $("#neonate_info_abnormalities_edit").val(data.abnormalities);
            $("#neonate_info_observations_edit").val(data.observations);
            $("#neonate_info_attended_edit").val(data.attended);
            $("#neonate_info_pediatrician_edit").val(data.pediatrician);
        }
    });
}

function updateNeonateInfo(){

    var params = {};
    params.id = $("#neonate_info_id").val();
    params.name = $("#neonate_info_child_name_edit").val();
    params.gender = $("#neonate_info_gender_edit").val();
    params.birthdate = $("#neonate_info_birthhour_edit").ts_hour($("#neonate_info_birthdate_edit").ts_date());
    params.weight = $("#neonate_info_weight_edit").val();
    params.height = $("#neonate_info_height_edit").val();
    params.apgar_appearance = $("#neonate_info_apgar_appearance_edit").val();
    params.apgar_pulse = $("#neonate_info_apgar_pulse_edit").val();
    params.apgar_gesture = $("#neonate_info_apgar_gesture_edit").val();
    params.apgar_activity= $("#neonate_info_apgar_activity_edit").val();
    params.apgar_breathing = $("#neonate_info_apgar_breathing_edit").val();
    params.blood_type = $("#neonate_info_blood_type_edit").val();
    params.abnormalities = $("#neonate_info_abnormalities_edit").val();
    params.observations = $("#neonate_info_observations_edit").val();
    params.attended = $("#neonate_info_attended_edit").val();
    params.pediatrician = $("#neonate_info_pediatrician_edit").val();


    var today = new Date();
    if(today < params.birthdate){
        swal("Error","La fecha de nacimiento no puede ser posterior a la actual","error");
        return;
    }

    if(!params.height.match(/^\d+$/)){
        swal("Error","La altura debe tener un valor entero. (En centímetros)","error");
        return;
    }

    if(isNaN(params.weight)){
        swal("Error","El peso debe tener valores numéricos","error");
        return;
    }

    var apgar_fields = ["neonate_info_apgar_appearance_edit","neonate_info_apgar_pulse_edit","neonate_info_apgar_gesture_edit","neonate_info_apgar_activity_edit","neonate_info_apgar_breathing_edit"];
    var valid_apgar = true;
    $.each(apgar_fields,function(idx, input){
        if($("#"+input).val() != "" && [0,1,2].indexOf(parseInt($("#"+input).val())) == -1){
            valid_apgar = false;
        }
    });
    if(!valid_apgar){
        swal("Error","Los valores del apgar deben estar entre 0 y 2","error");
        return;
    }

    $.ajax({
        url:$("#update_neonate_info_url").val(),
        dataType:"json",
        data:params,
        success:function(data){
            $("#edit_neonate_info_modal").modal("hide");
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            loadPregnacies();
        }
    });

}

function openEditAbortionInfoModal(abortion_id) {
    $("#edit_abortion_info_modal").modal("show");
    $("#abortion_info_id").val(abortion_id);
    $.ajax({
        url:$("#get_abortion_info_url").val(),
        dataType:"json",
        data:{id:abortion_id},
        success:function(data){
            console.log(data);
            $("#abortion_info_date_edit").val(dateToString(new Date(data.date),"dd/mm/yyyy"));
            $("#abortion_info_hour_edit").val(dateToString(new Date(data.date),"hh:mn pp"));
            $("#abortion_info_hospital_edit").val(data.hospital);
            $("#abortion_info_type_edit").val(data.abortion_type_id);
            $("#abortion_info_observations_edit").val(data.observations);
        }
    });

}

function updateAbortionInfo(){
    var params = {};
    params.id = $("#abortion_info_id").val();
    params.date = $("#abortion_info_hour_edit").ts_hour($("#abortion_info_date_edit").ts_date());
    params.hospital = $("#abortion_info_hospital_edit").val();
    params.type = $("#abortion_info_type_edit").val();
    params.observations = $("#abortion_info_observations_edit").val();

    $.ajax({
        url:$("#update_abortion_info_url").val(),
        dataType:"json",
        data:params,
        success:function(data){
            $("#edit_abortion_info_modal").modal("hide");
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            loadPregnacies();
        }
    });
}

