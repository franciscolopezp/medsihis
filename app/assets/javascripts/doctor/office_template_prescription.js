$(function(){
    $('#error_explanation').hide();
    var grid = $("#data-template-prescription").bootgrid({
        ajax: false,
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<button data-url-delete=\"" + row.commands + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" "+"><i class=\"zmdi zmdi-delete\" title='Eliminar'></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var office = $tr_parent.children('td').eq(0).text();
            var template = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar la plantilla "+template+" del consultorio "+office+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Eliminar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        location.reload();
                    });

                    ajax.fail(function(error, status, msg){
                        console.log("Error: " + error.responseText + msg);
                    });
                });
        });
    });

    $('#btn-save-template').click(function(){
        var office_selected = $('#office').val();
        if(office_selected != ""){
            $('#error_explanation').hide();
        }else{
            $('#error_explanation').show();
            return;
        }
        var url_aux = $('#url_add_template').val();
        var template = $("input[type='radio']:checked").val();
        var ajax = $.ajax({
            type: 'post',
            url: url_aux,
            data: {id:office_selected, template: template}
        });
        ajax.done(function(result, status){
            location.reload();
        });

        ajax.fail(function(error, status, msg){
            swal("Error!", "Consulte al administrador.", "error");
        });

    });

});