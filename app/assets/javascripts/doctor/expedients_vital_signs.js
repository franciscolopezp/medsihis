var graph_type = '';
function initComponentsVitalSign(){
    var table = $("#data-table-vital-signs");
    table.dataTable({
        ajax: {
            url: table.data('source'),
            data: function(d) {}
        },
        columns: [
            {data:'date', name:'view_patient_valorations.created_at', defaultContent:''},
            {data:'weight', name:'view_patient_valorations.peso', defaultContent:''},
            {data:'height', name:'view_patient_valorations.talla', defaultContent:''},
            {data:'temp', name:'view_patient_valorations.temperatura', defaultContent:''},
            {data:'ta', name:'view_patient_valorations.ta_sistolica', defaultContent:''},
            {data: null,
                orderable:false,
                searchable:false,
                defaultContent:'',
                render : function(data, type, row, meta){
                    return "<a href='javascript:void(0)' onclick='show_details("+row.id+",\""+row.date+"\")' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Ver\"><i class=\"zmdi zmdi-eye\"></i></a>";
                }
            }
        ],
        initComplete: function(settings, json){
            setPositionAndClassesOptionsDatatable();
        },
        drawCallback: function( settings ) {
        }
    });

    $("#new_valoration_btn").click(function () {
        var url = $(this).attr("url");
        $("#new_valoration_valoration_type_id").val(1);
        $.ajax({
            url:url,
            data:{valoration_type_id:1},
            success: function(data){
                $("#valoration_fields").html(data);
                $(".delete_new_valoration").remove();
                $("#add_valoration_modal").modal("show");
            }
        });
    });

    $("#save_valoration_btn").click(function(){
        var url = $(this).attr("url");
        var fields = $("#valoration_fields .valoration_holder").find(".v_field");

        var fields_data = [];
        $.each(fields,function(idx, item){
            if($(item).val() == ""){
                return;
            }
            fields_data.push({
                id: $(item).attr("field_id"),
                value: $(item).val()
            });
        });

        if(fields_data.length == 0){
            swal("No se puede guardar la valoración","Por favor verifique que los campos no estén vacíos.","error");
            return;
        }

        $.ajax({
            dataType:"json",
            method:"get",
            url:url,
            data:{
                expedient_id: $("#vital_expedient_id").val(),
                valoration_type:$("#new_valoration_valoration_type_id").val(),
                fields:fields_data
            },
            success: function(data){
                if(data.done){
                    table.DataTable().ajax.reload();
                    $("#add_valoration_modal").modal("hide");
                    $("#valoration_fields input").val("");
                }
            }
        });
    });
    $("#print_chart_sign").click(function(){
        print_valoration_graph();
    });

}
function show_details(id,date){
    $.ajax({
        url:$("#url_get_all_vital_signs").val(),
        data:{
            pv_id:id
        },
        dataType:"json",
        success:function(data){
            if(data.done){
                $("#data_fields_div").html(data.data);
                $("#label_date").html("Fecha: " + date);
                $("#show_details_modal").modal("show");
            }else{
                console.log("Error al obtener campos de valoraciones")
            }
        }
    });
}
function loadVitalSignsData(type){
    graph_type = type;
    var medical_expedient_id = $("#vital_expedient_id").val();
    $.ajax({
        url:$("#url_get_data_chart").val(),
        data:{
            medical_expedient_id:medical_expedient_id,
            type:type
        },
        dataType:"json",
        success:function(data){
            createChart(type,data);
        }
    });
}

function createChart(type,resp){
    if(resp.length == 0){
        return;
    }
    var firstDate = new Date(resp[0].date);
    var lastDate = new Date(resp[resp.length - 1].date);
    var maxDays = Math.round((lastDate-firstDate)/(1000*60*60*24));

    var currentDates = {};
    var days = 0;
    $.each(resp, function (idx,item) {
        days = Math.round((new Date(item.date)-firstDate)/(1000*60*60*24));
        if(item.val != null){
            currentDates["d"+days] = item.val;
        }
    });

    var dataArray = [];
    var dataSystolicPressure = [];
    var dataDiastolicPressure = [];
    for(var i = 0; i <= maxDays ; i++){
        if(type == "blood_pressure"){
            if(currentDates["d"+i] != undefined){
                dataSystolicPressure[i] = currentDates["d"+i].systolic_pressure;
                dataDiastolicPressure[i] = currentDates["d"+i].diastolic_pressure;
            }else{
                dataSystolicPressure[i] = null;
                dataDiastolicPressure[i] = null;
            }
        }else{
            if(currentDates["d"+i] != undefined){
                dataArray[i] = currentDates["d"+i];
            }else{
                dataArray[i] = null;
            }
        }
    }

    var conf = {
        suffix:"",
        yTitle:"",
        title:"",
        subtitle:"De la primera a la última consulta",
        series_name:""
    };

    switch (type){
        case "weight":
            conf.suffix = " Kg";
            conf.yTitle = "Peso (Kg)";
            conf.title = "Historial de Peso";
            conf.series_name = "Peso";
            break;
        case "height":
            conf.suffix = " Cm";
            conf.yTitle = "Estatura (Cm)";
            conf.title = "Historial de Estatura";
            conf.series_name = "Estaura";
            break;
        case "temperature":
            conf.suffix = " °C";
            conf.yTitle = "Temperatura °C";
            conf.title = "Historial de temperatura";
            conf.series_name = "Temperatura";
            break;
        case "blood_pressure":
            conf.suffix = " mmHg";
            conf.yTitle = "Tensión Arterial";
            conf.title = "Historial de tensión arterial";
            conf.series_name = "Tensión Arterial";
            break;
    }

    var data = [];
    if(type == "blood_pressure"){
        data.push({
            name: "Presión Sistólica",
            data: dataSystolicPressure
        });
        data.push({
            name: "Presión Diastólica",
            data: dataDiastolicPressure
        });
    }else{
        data = [{
            name: conf.series_name,
            data: dataArray
        }];
    }


    $('#vital_sign_chart').highcharts({
        title: {
            text: conf.title,
            x: -20 //center
        },
        subtitle: {
            text: conf.subtitle,
            x: -20
        },
        xAxis: {
            // categories:["1","xxxxx"]
            labels: {
                formatter: function() {
                    return dateToString(getNewDate(firstDate,this.value),"dd/mm/yyyy");
                }
            }
        },
        yAxis: {
            title: {
                text: conf.yTitle
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        plotOptions:{
            series:{
                connectNulls:true
            }
        },
        tooltip: {
            formatter: function() {
                days = this.x;
                if(days == maxDays){
                    days--;
                }
                return '<b>'+dateToString(getNewDate(firstDate,days),"dd/mm/yyyy") + ' - ' + this.y +' '+ conf.suffix +'</b><br/>';
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: data
    });
}
function getNewDate(date, add_days) {
    var result = new Date(date);
    result.setDate(result.getDate() + add_days);
    return result;
}
function print_valoration_graph(){
    if (graph_type != ""){
        var url_pdf = $('#url_print_valoration_graph').val()+"?medical_expedient_id="+$("#vital_expedient_id").val()+"&type="+graph_type;
        window.open(url_pdf, '_blank');
    }

}