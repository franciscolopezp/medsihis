/**
 * Created by equipo-01 on 6/04/16.
 */
document.addEventListener("DOMContentLoaded", function() {
    $('.sweet-overlay').parent().remove();
});
jQuery(document).ready(function() {
    /*
     * Login
     */
    if ($('.login-content')[0]) {
        //Add class to HTML. This is used to center align the logn box
        $('html').addClass('login-content');

        $('body').on('click', '.login-navigation > li', function(){
            var z = $(this).data('block');
            var t = $(this).closest('.lc-block');

            t.removeClass('toggled');

            setTimeout(function(){
                $(z).addClass('toggled');
            });

        })
    }

});
/*
 // test para el api
function test(){
    $.ajax({
        url:"/sessions/update_profile/2",
        method:"put",
        dataType:"json",
        data:{
            token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoiZWRnYXIifQ.",
            user_name: "doctor_edgar",
            email: "doctor@gmail.com",
            user: {
                name:"Edgar Uriel",
                last_name:"Rodríguez Cámara",
                cellphone:"9999853658",
                "birth_date":"1992-11-10"
            }

        },
        success:function(data){
          console.log("TODO BIEN");
          console.log(data);
        },
        error:function(data){
            console.log("TODO MAL");
            console.log(data);
        }
    });
}
*/
function reset_password(){
    var password = $("#password").val();
    var password_2 = $("#repeat_password").val();
    var user_id = $("#reset_password").val();

    if(password=="" || password_2==""){
        $('#email_reset_valid').text("Los campos no pueden estar vacios.")
        return;
    }
    if(password!=password_2){
        $('#email_reset_valid').text("Las contraseñas no son iguales.")
        return;
    }
     var params = {
     password:password,
     user_id:user_id
     };
    $.ajax({
        url:$('#url_reset_password').val(),
        method:"GET",
        dataType:"json",
        data:params,
        success:function(){
            window.location.href = $('#login_url').val();
        },
        error:function(){
        }
    });

}

function send_password_mail(){
    var params = {
        email: $('#user_email').val()
    };
    $.ajax({
        url:$('#url_send_new_password').val(),
        method:"GET",
        dataType:"json",
        data:params,
        success:function(){
            $('#response_email_success').text("Se ha enviado un correo para restablecer su contraseña, porfavor revise su bandeja de entrada.")
            $('#return_reset_pass_btn').click();
        },
        error:function(){
            $('#response_email_reset').text("Usuario no encontrado, intente de nuevo porfavor.")
        }
    });
}