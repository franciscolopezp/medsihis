(function ( $ ) {
    $.fn.ts_autocomplete = function( options ) {
        if(options.url == undefined || options.url == ""){
            throw "Please provide a valid URL to use the autocomplete plugin";
        }
        var settings = $.extend({
            // These are the defaults.
            display_field:"name",
            in_modal:false,
            items:30,
            required:true,
            model:"un elemento"
        }, options );
        this.filter( "input" ).each(function(idx,i) {
            var hidden_id = "";
            var hidden_name = "";
            var input = $( i );
            var input_id = input.attr("id");
            var city_id = input.attr("city");
            var current_value = input.attr("current_value");

            if(input.attr("name") != undefined && input.attr("name") != "")
                hidden_name = input.attr("name");

            input.removeAttr("name");
            hidden_id = input_id + "_ts_autocomplete";

            var hidden_value_html = '';
            if(city_id != undefined && city_id != ""){
                hidden_value_html = ' value="'+city_id+'"';
            }

            if(current_value != undefined && current_value != ""){
                hidden_value_html = ' value="'+current_value+'"';
            }

            var hidden_html = '<input type="hidden" ' +
                'name="'+hidden_name+'" ' +
                'id="'+hidden_id+'" '+hidden_value_html+' > ';

            input.parent().append(hidden_html);

            input.keyup(function(e){
                if(input.val() == ""){
                    $("#"+hidden_id).val("");
                }


            });

            input.keypress(function (e) {
                if(e.keyCode == 13){
                    e.preventDefault();
                }
            });

            input.change(function(){
                setTimeout(function(){
                    if($("#"+hidden_id).val() == "" && settings.required){
                        input.val("");
                        swal("","Por favor seleccione "+settings.model+" de la lista","error");
                    }
                },200);
            });

            var url = settings.url;

            input.autocomplete({
                source: url,
                minLength: 2,
                select: function( event, ui ) {
                    $("#"+hidden_id).val(ui.item.id);
                    if(settings.callback != undefined){
                        settings.callback(ui.item.id, ui.item.value, ui.item.type);
                    }
                }
            });
        });
        if(settings.in_modal){
            $("ul.ui-autocomplete").css("z-index",10000);
        }
        return this;
    };
}( jQuery ));