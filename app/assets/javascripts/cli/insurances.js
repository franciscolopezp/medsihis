jQuery(document).ready(function() {

    $("#hospital_service").change(search_prices);
    var table = $("#data-table-insurances");
    var grid = $("#data-table-insurances").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-insurances').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>"+
                    "<button  onclick='active_insurence("+row.id+")' class=\"btn bgm-green btn-icon waves-effect waves-circle waves-float btn-manage \" data-row-id=\"" + row.id + "\" title=\"Activar\"><i class=\"zmdi zmdi-refresh-alt zmdi-hc-fw\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Desactivar la aseguradora  "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "La aseguradora ha sido desactivada exitosamente.", "success");
                        $('#data-table-insurances').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });

$(".content2").hide();
    if($("#prices_div").val() != undefined){
        if($("#insurances_amount").val()>0){
            start_data();
        }
    }
    /*$(".field_price").blur(function() {

    });*/
});

function search_prices(){
    start_data();
}
function start_data(){
    var services_table = "<table id='services_table_content' class='table table-striped'><thead><tr><th>Servicio</th>";
    var service_id = "";
    if ($("#hospital_service").val() != "Todos"){
        service_id = $("#hospital_service").val();
    }
    $.ajax({
        url: $("#url_get_services").val(),
        dataType: "json",
        async:false,
        data: {service_id:service_id},
        success: function (data) {
            if(data.done){
                $.each(data.insurences, function (idx, info) {
                    services_table += "<th>"+info.name+"</th>"
                });
                services_table += "</tr></thead><tbody>";
                $.each(data.services, function (idx, info) {
                    services_table += "<tr>";
                    services_table += "<td id='"+info.id+"'>" + info.name + "</td>" ;
                    $.each(data.insurences, function (idx, insu) {
                        services_table += "<td ><input type='number'  asurance='"+insu.id+"' service='"+info.id+"' class='form-control input-lg field_price' value='0'  id='asurance_price_"+insu.id+"_"+info.id+"'></td>" ;
                    });
                    services_table +=   "</tr>";

                });
                services_table += "</tbody></table>";
                $('#prices_div').html(services_table);
                set_prices();
                $(".field_price").blur(function() {
                    if ($(this).val() != "" && $(this).val() != 0)
                    {
                        $.ajax({
                            url: $("#url_save_price_asurence").val(),
                            dataType: "json",
                            data: {
                                price: $(this).val(),
                                insurance_id: $(this).attr("asurance"),
                                service_id: $(this).attr("service")
                            },
                            success: function (data) {
                                if (data.done) {
                                    console.log("Exito");
                                        $(".content2").show();
                                        $(".content2").fadeOut(1500);
                                } else {
                                    swal("Error!", "Ocurrio un error al guardar el precio", "error");
                                }
                            },
                            error: function (data) {
                                console.log("error controller");
                            }
                        });
                    }
                });
            }else{
                swal("Error!", "Ocurrio un error al obtener los items", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function active_insurence(id){
    swal({
            title: "Activar aseguradora",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, activar!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url: $("#url_active_insurence").val(),
                dataType: "json",
                data: {id:id},
                success: function (data) {
                    if(data.done){
                        swal("Exito!", "Aseguradora activada.", "success");
                        $('#data-table-insurances').bootgrid("reload");

                    }else{
                        swal("Error!", "Ocurrio un error", "error");
                    }
                },
                error:function(data){
                    console.log("error controller");
                }
            });

        });
}
function set_prices(){
    $.ajax({
        url: $("#url_set_prices_asurence").val(),
        dataType: "json",
        data: {},
        success: function (data) {
            if(data.done){
                $.each(data.prices, function (idx, info) {
                    $("#"+info.field).val(info.price);
                });

            }else{
                swal("Error!", "Ocurrio un error al obtener los items", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });

}
