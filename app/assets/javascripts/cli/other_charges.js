var row_counter = 1;
var charge_items = [];
var total_discount = 0;
var total_amount = 0;
var to_change_discount = 0;
var amount_to_cancel = 0;
var charge_to_cancel = 0;
var charge_to_close = 0;
var show_all = 0;
var service_type ="";
jQuery(document).ready(function() {
    if($("#is_new").val() != undefined){
        if($("#is_new").val() != 0){
            load_charge_items($("#is_new").val());
        }
    }
    if ($("#hospital_service")[0]){
        $("#hospital_service").focus();
        search_services();
    }
    $('#service_discount').on('keypress', function (e) {
        if(e.which === 13){
            if($("#patient_name").val() != "") {
                $(this).prop('disabled', true);
            }
            e.preventDefault();
            add_items_to_table();
        }
    });

    var table = $("#data-table-other_charges");
    var grid = $("#data-table-other_charges").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function()
        {
            return $('#data-table-other_charges').data('source')+"?show_all="+show_all;
        },

        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                var avalible_links = "";
                if (row.is_closed){
                    avalible_links ="<a    href=" + row.url_show + " class=\"btn bgm-green btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Ver\"><i class=\"zmdi zmdi-eye zmdi-hc-fw\"></i></a>";
                }else{
                 avalible_links ="<a    href=" + row.url_edit + " class=\"btn bgm-blue btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-hospital-alt zmdi-hc-fw\"></i></a>";
                }

                return avalible_links;

            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el cobro de "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El cobro ha sido eliminado exitosamente.", "success");
                        $('#data-table-other_charges').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });
    $("#add_service_charge_btn").click(function(e){
        e.preventDefault();
        add_items_to_table();
        $(this).prop('disabled', true);
    });
});
function add_items_to_table(){
    if($("#patient_name").val() == ""){
        swal("Error!", "Primero debe ingresar el nombre de un paciente.", "error");
        return;
    }
    if(service_type!="service"){
        console.log("paquete");
        $.ajax({
            url: $("#url_get_services_package").val(),
            dataType: "json",
            async: false,
            data:{id:$("#hospital_service_ts_autocomplete").val()},
            success: function (data) {
                console.log(data);
                if(data.done) {
                    $.each(data.services, function (idx, info) {
                        charge_items.push({
                            id:row_counter,
                            service_id:info.service_id,
                            cost: info.service_cost,
                            discount:0,
                            total: info.service_cost
                        });
                    });
                    save_other_charge();
                }else{
                    swal("Error","El paquete seleccionado no tiene servicios.","error");
                }

            },
            error:function(data){
                console.log("error obtener servicios de paquete");
            }
        });
    }else {
        console.log("servicio");
        if ($('#service_discount').val() == "") {
            $('#service_discount').val(0);
        }
        var total = $('#service_cost').val() - parseFloat($('#service_discount').val());
        charge_items.push({
                id: row_counter,
                service_id: $("#hospital_service_ts_autocomplete").val(),
                cost: $('#service_cost').val(),
                discount: $('#service_discount').val(),
                total: total
            }
        );
        row_counter++;
        save_other_charge();
    }
    get_totals();
}
function search_services(){
    $("#hospital_service").ts_autocomplete({
        url: $('#url_search_services').val(),
        items: 10,
        in_modal: true,
        callback: function (val, text, type) {
            service_type = type;
            set_service_cost();
        }
    });
}
function show_all_values(){
    if (show_all == 0){
        show_all = 1;
        $('#data-table-other_charges').bootgrid("reload");
        $("#show_all_link").html("Mostrar abiertos");
    }else{
        show_all = 0;
        $('#data-table-other_charges').bootgrid("reload");
        $("#show_all_link").html("Mostrar todos");
    }

}

function set_service_cost(){
    if( $("#hospital_service").val()!= "") {
        $.ajax({
            url: $("#url_get_service_cost").val(),
            dataType: "json",
            data: {patient:$("#patient_id").val(),service: $("#hospital_service_ts_autocomplete").val(),type:service_type,admission:0},
            success: function (data) {
                $("#service_cost").val(data.cost);
                if(data.is_fixed_cost){
                    $('#service_cost').prop('disabled', false);
                }else{
                    $('#service_cost').prop('disabled', true);
                }
            },
            error: function (data) {
                console.log("error controller2");
            }
        });
    }
}

function setEditItemCharge(id){
    to_change_discount = id;
    $("#change_discount_modal").modal("show");
}
function update_discount(){
    if($("#new_service_discount").val() != "") {
        $.each(charge_items, function (idx, info) {
            if (info.id == to_change_discount) {
                info.discount = $("#new_service_discount").val();
                info.total = info.cost - $("#new_service_discount").val();
                $("#discount_cell_"+to_change_discount).html("$ " +$("#new_service_discount").val() );
                $("#total_cell_"+to_change_discount).html("$ " +info.total );
                swal("Exito!", "Descuento aplicado con exito.", "success");
                $("#change_discount_modal").modal("hide");
                $("#new_service_discount").val("");
                return false;
            }
        });
        get_totals();
        save_other_charge();
    }
    else{
        swal("Error!", "Debe especificar un descuento valido.", "error");
    }
    console.log(charge_items);
}
function get_totals(){
    total_amount = 0;
    total_discount = 0;
    $.each(charge_items, function (idx, info) {
        total_amount += parseFloat(info.cost - info.discount);
        total_discount += parseFloat(info.discount);
    });
    $("#total_discount").val(total_discount);
    $("#total_amount").val( total_amount);
}
function setDeleteItemCharge(id){
    swal({
            title: "¿Eliminar el servicio de la lista?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){
            $("#item_charge_"+id).remove();
            charge_items = $.grep(charge_items,
                function(o,i) { return o.id == id; },
                true);
            get_totals();
            swal("Eliminado!", "El servicio ha sido eliminado.", "success");
            save_other_charge();
        });
}
function save_other_charge(){
    var is_update = 0;
    if ($("#is_new").val() != 0){
       is_update = $("#is_new").val();
    }
    $.ajax({
        url: $("#url_get_save_other_charge").val(),
        dataType: "json",
        async: false,
        data: {services:charge_items,is_update:is_update,
            patient_name:$("#patient_name").val(),total:$("#total_amount").val(),
            discount:$("#total_discount").val()},
        success: function (data) {
            if(data.done){
                location.href = data.root;
            }else{
                swal("Error!", "Ocurrio un error al guardr la cobro", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function load_charge_items(id){
    $.ajax({
        url: $("#url_get_charge_items").val(),
        dataType: "json",
        data: {id:id},
        success: function (data) {
            if(data.done){
                var total_amount = 0;
                var total_discount = 0;
                $.each(data.items, function (idx, info) {
                    total_amount += parseFloat(info.total);
                    total_discount += parseFloat(info.discount);
                    charge_items.push({
                        id:info.id,
                        service_id:info.service_id,
                        cost: info.cost,
                        discount:info.discount,
                        total: info.total
                    });
                });
                $("#total_paid").val(data.total_paid);
                $('#total_amount').val(total_amount);
                $('#total_discount').val(total_discount);
            }else{
                swal("Error!", "Ocurrio un error al obtener los items", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}

function generate_payment(){
    var id = $("#is_new").val();
    if($("#payment_cost").val() != ""){
        $("#add_transaction_btn").prop("disabled",true);
        var amounts = [];
        amounts.push({
            currency:1,
            amount: $("#payment_cost").val()
        });

        $.ajax({
            url: $("#url_generate_payment_other_charge").val(),
            dataType: "json",
            data: {other_charge_id:id,
                amount:$("#payment_cost").val(),
                amounts:amounts,
                t_type:1,
                transaction_type_id:6,
                payment_method_id:$("#payment_method").val(),
                account_id:$("#account").val(),
                details: $("#details").val()},
            success: function (data) {
                if(data.done){
                    swal("Exito","Pago realizado con exito.","success");
                    location.reload();
                }else{
                    swal("Error","Ocurrio un error al realizar el pago","error");
                }
            },
            error: function (data) {
                console.log("error controller");
            }
        });
    }else{
        swal("Error","Debe ingresar un monto valido.","error");
    }
}

function setCancelPayment(id,amount) {
    amount_to_cancel = amount;
    charge_to_cancel = id;
    $("#user_name").val("");
    $("#password").val("");
    $("#cancel_payment_modal").modal("show");
}
function setCloseOtherCharge(id) {
    charge_to_close = id;
    $("#user_name_close").val("");
    $("#password_close").val("");
    $("#close_other_charge_modal").modal("show");
}
function confirmCancelPayment(id,amount,user_id){
    var amounts = [];
    amounts.push({
        currency:1,
        amount: amount
    });
    swal({
            title: "¿Esta seguro que desea cancelar el pago?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, cancelar!",
            closeOnConfirm: false
        },
        function(isConfirm){
            if(isConfirm) {
                $.ajax({
                    url: $("#url_cancel_payment_other_charge").val(),
                    dataType: "json",
                    data: {
                        other_charge_transaction_id: id,
                        amount: amount,
                        amounts: amounts,
                        t_type: 0,
                        user_id:user_id,
                        transaction_type_id: 3
                    },
                    success: function (data) {
                        if (data.done) {
                            swal("Exito", "Pago cancelado con exito.", "success");
                            location.reload();
                        } else {
                            swal("Error", "Ocurrio un error al cancelar el pago", "error");
                        }
                    },
                    error: function (data) {
                        console.log("error controller");
                    }
                });
            }else{
                $("#cancel_payment_btn").prop("disabled", false);
            }
        });
}
function close_other_charge(){
    if($("#user_name_close").val()!= "" && $("#password_close").val()!= "") {
        $("#close_charge_btn").prop("disabled", true);
        $.ajax({
            url: $("#url_check_close_permission").val(),
            dataType: "json",
            data: {user_name: $("#user_name_close").val(), password: $("#password_close").val()},
            success: function (data) {
                console.log(data);
                if (data.done) {
                    close_other_charge_confirm(charge_to_close);
                } else {
                    $("#close_charge_btn").prop("disabled", false);
                    swal("Error", data.msg, "error");
                }
            },
            error: function (data) {
                console.log("error controller");
            }
        });
    }else{
        swal("Error", "Debe ingresar su nombre de usuario y contraseña", "error");
    }
}
function close_other_charge_confirm(id){
    var total_paid = parseFloat($("#total_paid").val());
    var total_cost = parseFloat($("#total_amount").val());
    var current_balance = total_cost - total_paid;
    if (current_balance <= 0) {
        swal({
                title: "¿Esta seguro que cerrar el cobro?. Una vez hecho esto no podra realizar ninguna operación",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, cerrar!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: $("#url_close_other_charge").val(),
                    dataType: "json",
                    data: {id: id},
                    success: function (data) {
                        if (data.done) {
                            swal("Exito", "Cobro cerrado con exito.", "success");
                            location.href = data.root;
                        } else {
                            swal("Error", "Ocurrio un error al cerrar el cobro", "error");
                        }
                    },
                    error: function (data) {
                        console.log("error controller");
                    }
                });
            });
    }else{
        swal("Error", "Para cerrar un cargo los pagos deben cubrir el monto del mismo.", "error");
    }
}

function cancel_payment(){
    if($("#user_name").val()!= "" && $("#password").val()!= "") {
        $("#cancel_payment_btn").prop("disabled", true);
        $.ajax({
            url: $("#url_check_cancel_permission").val(),
            dataType: "json",
            data: {user_name: $("#user_name").val(), password: $("#password").val()},
            success: function (data) {
                console.log(data);
                if (data.done) {
                    confirmCancelPayment(charge_to_cancel,amount_to_cancel,data.user_id);
                } else {
                    $("#cancel_payment_btn").prop("disabled", false);
                    swal("Error", data.msg, "error");
                }
            },
            error: function (data) {
                console.log("error controller");
            }
        });
    }else{
        swal("Error", "Debe ingresar su nombre de usuario y contraseña", "error");
    }

}
function export_excel(){
    var fecha_inicio = $('#start_date').val();
    var fecha_fin = $('#end_date').val();
    if(fecha_inicio == "" || fecha_fin == ""){
        swal("Error!", "Los campos de fechas son obligatorios.", "error");
        return;
    }
    window.location.href  = $("#url_export_charges").val()+"?fecha_ini="+fecha_inicio+"&fecha_f="+fecha_fin

}
function export_pdf(){
    var fecha_inicio = $('#start_date').val();
    var fecha_fin = $('#end_date').val();
    if(fecha_inicio == "" || fecha_fin == ""){
        swal("Error!", "Los campos de fechas son obligatorios.", "error");
        return;
    }
    var url_pdf  = $("#url_print_charge_report").val()+"?fecha_ini="+fecha_inicio+"&fecha_f="+fecha_fin
    window.open(url_pdf, '_blank');
}
function search_charges(){
    var fecha_inicio = $('#start_date').val();
    var fecha_fin = $('#end_date').val();
    if(fecha_inicio == "" || fecha_fin == ""){
        swal("Por favor complete todos los campos", "Los campos de fechas son obligatorios.", "error");
        return;
    }
    var charges_table = "<table id='charges_table_content' class='table table-striped'><thead><tr>"+
        "<th>Folio</th>"+
        "<th>Fecha</th>"+
        "<th>Servicios</th>"+
        "<th>Detalles</th>"+
        "<th>Responsable</th>"+
        "<th>Monto</th>"+
        "</tr></thead><tbody>";
    var row = 1;
    $.ajax({
        url: $('#url_search_charges').val(),
        dataType: "json",
        data: {fecha_ini:fecha_inicio,
            fecha_f:fecha_fin},
        success: function (data) {
            if (data.length > 0) {
            $.each(data, function (idx, info) {
                charges_table += "<tr>" +
                    "<td >" + info.folio + "</td>" +
                    "<td >" + info.date + "</td>" +
                    "<td >" + info.service + "</td>" +
                    "<td >" + info.details + "</td>" +
                    "<td >" + info.responsible + "</td>" +
                    "<td >" + info.amount + "</td>" +
                    "</tr>";
                row++;
            });
        }else{
                swal("Datos no encontrados", "No se encontraron datos para ese rango de fechas, intente nuevamente", "info");
            }
            charges_table += "</tbody></table>";
            $('#charges_table').html(charges_table);
        },
        error:function(){
            console.log("error");
        }
    });
}