var row_counter_phar = 1;
var pharmacy_items = [];
var BEFORE_WAREHOUSE = 0;
var BEFORE_TYPE = 0
var is_entry = 0;
jQuery(document).ready(function() {
    $("#new_lot_div").hide();
    if($("#waste_type_select")[0]){
        check_waste_type();
        $("#waste_type_select").on('focus', function () {
            BEFORE_TYPE = this.value;
        }).change(check_waste_type);
    }
    if ($("#warehouse")[0]){
        $("#warehouse").on('focus', function () {
            BEFORE_WAREHOUSE = this.value;
        }).change(reset_items);
}
    $("#waste_type").change(reload_grid);
    var grid = $("#data-table-medicament_wastes").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function()
        {
            return $('#data-table-medicament_wastes').data('source')+"?waste_type="+$("#waste_type").val()

        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar  "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "eliminado exitosamente.", "success");
                        $('#data-table-medicament_wastes').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });
    if($("#current_lot_div")[0]){
        $("#current_lot_div").hide();
    }
    if ($("#item_medicament")[0]){
        $("#item_medicament").ts_autocomplete({
            url: $('#url_find_medicaments_pharmacy_items').val(),
            items: 10,
            in_modal: true,
            callback: function (val, text, type) {
                var lots_options = "";
                $('#lot').html("");
                item_type = type;
                if (type == "med") {
                    var lots_founded = 0;
                    $.ajax({
                        url: $("#url_get_lot").val(),
                        data: {medicament_id: val, warehouse: $("#warehouse").val()},
                        dataType: "json",
                        success: function (data) {
                            var is_avalible = false;
                            $.each(data.lots, function (idx, info) {
                                if (info.quantity > 0) {
                                    is_avalible = true;
                                    lots_founded += 1;
                                    lots_options += "<option exp_date="+info.exp_date+" value = " + info.id + ">" + info.name + "</option>";
                                }
                            });
                            if(is_entry){
                                $("#current_lot_div").show();
                                lots_options += "<option value='0'> Nuevo lote </option>";
                                $('#lot').html(lots_options);
                                $('#lot').selectpicker('refresh');
                                if(lots_founded == 0){
                                    $("#new_lot_div").show();
                                }
                            }else{
                                if(is_avalible) {
                                    $("#current_lot_div").show();
                                    $('#lot').html(lots_options);
                                    $('#lot').selectpicker('refresh');
                                    $("#item_quantity").focus();
                                }else{
                                    swal("Error!", "Ningun lote del almacen seleccionado tiene existencias de " + text, "warning");
                                    $("#item_medicament").val("");
                                    $("#item_medicament").focus();
                                    $("#item_medicament_ts_autocomplete").val("");
                                }
                            }

                        }
                    });
                } else {
                    $("#item_quantity").focus();
                    $("#current_lot_div").hide();
                }
            }
        });
    }
    $('#item_quantity').on('keypress', function (e) {
        if(e.which === 13){
            $(this).attr("disabled", "disabled");
            add_item_to_table();
            $(this).removeAttr("disabled");
        }
    });
    $("#lot").change(checknewlot);
});
function checknewlot(){
    if($("#lot").val()==0){
        $("#new_lot_div").show();
    }else{
        $("#new_lot_div").hide();
    }
}
function check_waste_type(){
    if($("#waste_type_select").val() > 0){
        if (pharmacy_items.length > 0) {
            $.ajax({
                url: $("#url_get_waste_type").val(),
                data: {waste_id: $("#waste_type_select").val()},
                dataType: "json",
                success: function (data) {
                    var aux = is_entry;
                    is_entry = data.is_entry;
                    if (aux != is_entry) {
                        swal({
                                title: "Esta cambiando de entrada a salida (o viceversa),la lista de articulos sera eliminada, ¿Esta de acuerdo?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Si, cambiar tipo",
                                closeOnConfirm: false
                            },
                            function (is_confirm) {
                                if (is_confirm) {
                                    $("#pharmacy_items_body").html("");
                                    pharmacy_items = [];
                                    swal("Lista de artículos restablecida")
                                } else {
                                    $("#waste_type_select").val(BEFORE_TYPE);
                                    $("#waste_type_select").selectpicker("refresh");
                                }
                            });
                    }
                }
            });
        }else{
            $.ajax({
                url: $("#url_get_waste_type").val(),
                data: {waste_id: $("#waste_type_select").val()},
                dataType: "json",
                success: function (data) {
                    is_entry = data.is_entry;
                }
            });
        }
    }
}
function reload_grid(){
    $('#data-table-medicament_wastes').bootgrid("reload");
}
function reset_items(){
    if (pharmacy_items.length > 0) {
        swal({
                title: "Al cambiar el almacen se eliminaran todos los artículos de la lista, ¿Esta de acuerdo?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, cambiar almacen",
                closeOnConfirm: false
            },
            function (is_confirm) {
                if(is_confirm) {
                    $("#pharmacy_items_body").html("");
                    pharmacy_items = [];
                    swal("Lista de artículos restablecida")
                }else{
                    $("#warehouse").val(BEFORE_WAREHOUSE);
                    $("#warehouse").selectpicker("refresh");
                }
            });
    }
}
function check_quantity(){
    var response = false;
    if($("#item_quantity").val() != "" && $("#item_quantity").val()>0){
        $.ajax({
            url: $("#url_check_quantity_waste").val(),
            dataType: "json",
            async:false,
            data: {quantity:$("#item_quantity").val(), warehouse:$("#warehouse").val(), lot:$("#lot").val()},
            success: function (data) {
                if(data.done){
                    response = true;
                }else{
                    response = false;
                    swal("Cantidad Insuficiente",data.msg,"error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else{
        swal("Especificar la cantidad correcta","Debe especificar una cantidad","error");
    }
    return response;
}
function check_quantity_pharmacy(){
    var response = false;
    if($("#item_quantity").val() != "" && $("#item_quantity").val()>0){
        $.ajax({
            url: $("#url_check_quantity_waste_pharmacy").val(),
            dataType: "json",
            async:false,
            data: {quantity:$("#item_quantity").val(), warehouse:$("#warehouse").val(), item:$("#item_medicament_ts_autocomplete").val()},
            success: function (data) {
                if(data.done){
                    response = true;
                }else{
                    response = false;
                    swal("Cantidad Insuficiente",data.msg,"error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else{
        swal("Especificar la cantidad","Debe especificar una cantidad","error");
    }

    return response;
}
function add_item_to_table(){
    var lot_id = 0;
    var lot_name = "N/A";
    var lot_exp_date = "N/A";
    if ($('#item_medicament').val() == "" || $('#item_quantity').val() == "" || $("#item_medicament_ts_autocomplete").val() == ""){
        swal("Error!", "Los campos estan vacios.", "error");
        return;
    }
    if ($('#item_quantity').val()<=0){
        swal("Error!", "La cantidad debe ser mayor a 0", "error");
        return;
    }
    if(item_type == "med"){
        if($("#lot").val() != 0){
            lot_name =  $("#lot option:selected").text();
            lot_id = $("#lot").val();
            lot_exp_date = $("#lot option:selected").attr('exp_date');

        }else {
            if($('#new_lot_name').val() == ""){
                swal("Error!", "Debe ingresar un nombre al lote nuevo", "error");
                return;
            }else{
                lot_name = $('#new_lot_name').val();
            }
            if($('#new_lot_expiration_date').val() == ""){
                swal("Error!", "Debe ingresar una fecha de caducidad para el lote nuevo", "error");
                return;
            }else{
                lot_exp_date = $('#new_lot_expiration_date').val();
            }
            lot_id = 0;
        }
        if(!is_entry) {
            if (!check_quantity()) {
                return;
            }
        }
    }else{
        if(!is_entry) {
            if (!check_quantity_pharmacy()) {
                return;
            }
        }
    }
    $("#pharmacy_items").append("<tr id='item_pharmacy_" + row_counter_phar + "'><td style='text-align: center'>" +
        $('#item_quantity').val() + "</td><td style='text-align: center'>" + $('#item_medicament').val() + "</td>" +
        "<td style='text-align: center' >" + lot_name + "</td>" + "<td style='text-align: center' >" + lot_exp_date + "</td>"+
        "<td><a data-toggle='modal' href='#delete_item_pharmacy_modal' onclick='setDeleteItemPharmacy(" + row_counter_phar + ")'>" +
        "<i class='zmdi zmdi-delete' style='font-size: 1.73em;' title='Eliminar'></i></a></td></tr>");

    pharmacy_items.push({
        id:row_counter_phar,
        item_id:$("#item_medicament_ts_autocomplete").val(),
        quantity:$('#item_quantity').val(),
        type:item_type,
        lot:lot_id,
        lot_name:lot_name,
        exp_date:lot_exp_date
        }
    );


    row_counter_phar++;
    $("#current_lot_div").hide();
    $("#item_medicament").val("");
    $("#item_quantity").val("");
    $("#item_medicament").focus();
    $("#item_medicament_ts_autocomplete").val("");
    $("#new_lot_name").val("");
    $("#new_lot_expiration_date").val("");
    $("#new_lot_div").hide();
}

function setDeleteItemPharmacy(row){
    swal({
            title: "¿Eliminar articulo de la lista?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){
            $("#item_pharmacy_"+row).remove();
            $.each(pharmacy_items, function(idx,info){
                if(info.id == row){
                    TOTAL_ASSORTMENT -= info.cost;
                    $("#cost").val(TOTAL_ASSORTMENT.toFixed(2));
                }
            });
            pharmacy_items = $.grep(pharmacy_items,
                function(o,i) { return o.id == row; },
                true);
            swal("Eliminado!", "El cobro de servicio ha sido eliminado.", "success");
            console.log(pharmacy_items);
        });
}



function save_movement(){
    if (pharmacy_items.length == 0){
        swal("Error!", "Debe ingresar al menos un artículo a la lista.", "error");
        return;
    }
    if ( $("#reason").val() == "" ){
        swal("Error!", "Debe ingresar un Motivo/Razón", "error");
        return;
    }
    $.ajax({
        url: $("#url_save_movement").val(),
        dataType: "json",
        data: {
            items:pharmacy_items,
            reason:$("#reason").val(),
            is_entry:is_entry,
            warehouse:$("#warehouse").val()
        },
        success: function (data) {
            if(data.done){
                swal("Exito", "Movimientos realizados con exito", "success");
                location.reload();
            }else{
                swal("Error!", "Ocurrio un error al guardar el movimiento", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}

