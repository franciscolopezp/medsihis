$(document).ready(function () {
    initHtmlEditor(".html_editor",{height:200});
    $.each($(".html_editor_value"),function (idx, item) {
        var editor = $(item).parent().find(".html_editor");
        $(editor).summernote().code($(item).val());
    });
});

$(function () {
    $("#save_settings").click(function () {
        $.post("",{settings:[
            {code:"PDF_HEADER",data:$("#PDF_HEADER").summernote().code()},
            {code:"PDF_FOOTER",data:$("#PDF_FOOTER").summernote().code()},
            {code:"TICKET_HEADER",data:$("#TICKET_HEADER").summernote().code()},
            {code:"TICKET_FOOTER",data:$("#TICKET_FOOTER").summernote().code()},
            {code:"TEMPLATE_CONSULTATION",data:$("#TEMPLATE_CONSULTATION").summernote().code()},
            {code:"TICKET_WIDTH",data:$("#TICKET_WIDTH").val()},
            {code:"TICKET_HEIGHT",data:$("#TICKET_HEIGHT").val()}
        ]},function (data) {
            if(data.done){
                swal("Configuraciones actualizadas","Las configuraciones se actualizaron correctamente.","success");
            }
        },"json");
    });


    $("#new_consultation_field_image").click(function () {
        $("#add_consultation_image_modal").modal("show");
    });

    $("#save_consultation_field_image").click(function () {
        $("#new_consultation_image_form").submit();
    });
});