jQuery(document).ready(function() {

    var table = $("#data-table-services_packages");
    var grid = $("#data-table-services_packages").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-services_packages').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>"+
                    "<a href=" + row.url_manage + " class=\"btn bgm-blue btn-icon waves-effect waves-circle waves-float btn-manage\" data-row-id=\"" + row.id + "\" title=\"Servicios\"><i class=\"zmdi zmdi-drink zmdi-hc-fw\"></i></a>" ;
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el paquete de "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El paquete ha sido eliminado exitosamente.", "success");
                        $('#data-table-services_packages').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });




});
function addService(){
    if($("#service_cost").val()!= "") {
        $.ajax({
            url: $("#url_add_service").val(),
            method: "GET",
            dataType: "json",
            data: {
                service_id: $("#service").val(),
                service_package_id: $("#service_package_id").val(),
                cost: $("#service_cost").val()
            },
            success: function (data) {
                if (data) {
                    swal("Exito", "Paquete actualizado", "success")
                    location.reload();
                } else {
                    swal("Error", "El paquete ya contiene el servicio.", "error")
                }

            }
        });
    }else{
        swal("Error", "Debe ingresar un costo.", "error")
    }
}
function deleteservice(id){
    swal({
            title: "¿Desea eliminar el servicio del paquete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url: $("#url_delete_service").val(),
                method: "GET",
                dataType: "json",
                data: {
                    id: id
                },
                success: function (data) {
                    if (data) {
                        swal("Exito", "Paquete actualizado", "success")
                        location.reload();
                    } else {
                        swal("Error", "Error al eliminar el servicio del paquete.", "error")
                    }

                }
            });

        });
}