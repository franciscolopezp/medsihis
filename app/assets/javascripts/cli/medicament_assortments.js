var new_item = 1;
var BEFORE_WAREHOUSE = 0;
var row_counter = 1;
var row_counter_phar = 1;
var item_to_edit;
var medicament_cost = 0;
var TOTAL_ASSORTMENT = 0;
var pharmacy_cost = 0;
var new_pharmacy_item = 0;
var pharmacy_items = [];
var item_type = "";
jQuery(document).ready(function() {
    $("#warehouse").on('focus', function () {
        // Store the current value on focus and on change
        BEFORE_WAREHOUSE = this.value;
    }).change(reset_items);

    if ($("#item_medicament")[0]){
        $("#item_medicament").ts_autocomplete({
            url: $('#url_find_medicaments_pharmacy_items').val(),
            items: 10,
            in_modal: true,
            callback: function (val, text, type) {
                var lots_options = "";
                $('#lot').html("");
                item_type = type;
                if (type == "med") {
                    $.ajax({
                        url: $("#url_get_lot").val(),
                        data: {medicament_id: val, warehouse: $("#warehouse").val()},
                        dataType: "json",
                        success: function (data) {
                            var is_avalible = false;
                            $.each(data.lots, function (idx, info) {
                                if (info.quantity > 0) {
                                    is_avalible = true;
                                    lots_options += "<option value = " + info.id + ">" + info.name + "</option>";
                                }
                            });
                            if(is_avalible) {
                                $("#current_lot_div").show();
                                $('#lot').html(lots_options);
                                $('#lot').selectpicker('refresh');
                                $("#item_quantity").focus();
                            }else{
                                swal("Error!", "Ningun lote del almacen seleccionado tiene existencias de " + text, "warning");
                                $("#item_medicament").val("");
                                $("#item_medicament").focus();
                                $("#item_medicament_ts_autocomplete").val("");
                            }
                        }
                    });
                } else {
                    $("#item_quantity").focus();
                    $("#current_lot_div").hide();
                }
            }
        });
}
    if($("#url_get_last_folio").val() != undefined){
        $.ajax({
            url: $("#url_get_last_folio").val(),
            dataType: "json",
            data: {},
            success: function (data) {
                $("#folio").val(data.folio);
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }
    var table = $("#data-table-medicament_assortments");
    var grid = $("#data-table-medicament_assortments").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-medicament_assortments').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar el surtido de "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El surtido ha sido eliminado exitosamente.", "success");
                        $('#data-table-medicament_assortments').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });
    if($("#current_lot_div")[0]){
        $("#current_lot_div").hide();
    }

    $('#item_quantity').on('keypress', function (e) {
        if(e.which === 13){
            $(this).attr("disabled", "disabled");
            add_item_to_table();
            $(this).removeAttr("disabled");
        }
    });
});
function reset_items(){
    if (pharmacy_items.length > 0) {
        swal({
                title: "Al cambiar el almacen se eliminaran todos los artículos de la lista, ¿Esta de acuerdo?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, cambiar almacen",
                closeOnConfirm: false
            },
            function (is_confirm) {
            if(is_confirm) {
                $("#pharmacy_items").html("");
                pharmacy_items = [];
                swal("Lista de artículos restablecida")
            }else{
                $("#warehouse").val(BEFORE_WAREHOUSE);
                $("#warehouse").selectpicker("refresh");
            }
            });
    }
}
function check_quantity(){
    var response = false;
    if($("#item_quantity").val() != "" && $("#item_quantity").val()>0){
        $.ajax({
            url: $("#url_check_quantity_waste").val(),
            dataType: "json",
            async:false,
            data: {quantity:$("#item_quantity").val(), warehouse:$("#warehouse").val(), lot:$("#lot").val()},
            success: function (data) {
                if(data.done){
                    response = true;
                }else{
                    response = false;
                    swal("Cantidad Insuficiente",data.msg,"error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else{
        swal("Especificar la cantidad correcta","Debe especificar una cantidad","error");
    }
    return response;
}
function check_quantity_pharmacy(){
    var response = false;
    if($("#item_quantity").val() != "" && $("#item_quantity").val()>0){
        $.ajax({
            url: $("#url_check_quantity_waste_pharmacy").val(),
            dataType: "json",
            async:false,
            data: {quantity:$("#item_quantity").val(), warehouse:$("#warehouse").val(), item:$("#item_medicament_ts_autocomplete").val()},
            success: function (data) {
                if(data.done){
                    response = true;
                }else{
                    response = false;
                    swal("Cantidad Insuficiente",data.msg,"error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else{
        swal("Especificar la cantidad","Debe especificar una cantidad","error");
    }

    return response;
}
function add_item_to_table(){
    var lot_id = 0;
    var lot_name = "N/A";
    if ($('#item_medicament').val() == "" || $('#item_quantity').val() == "" || $("#item_medicament_ts_autocomplete").val() == ""){
        swal("Error!", "Los campos estan vacios", "error");
        return;
    }
    if ($('#item_quantity').val()<=0){
        swal("Error!", "La cantidad debe ser mayor a 0", "error");
        return;
    }
    if(item_type == "med"){
        lot_id = $("#lot").val();
        lot_name = $( "#lot option:selected" ).text();
        if(!check_quantity()){
            return;
        }
    }else{
        if(!check_quantity_pharmacy()){
            return;
        }
    }
        $("#pharmacy_items").append("<tr id='item_pharmacy_" + row_counter_phar + "'><td style='text-align: center'>" +
            $('#item_quantity').val() + "</td><td style='text-align: center'>" + $('#item_medicament').val() + "</td>" +
            "<td style='text-align: center' >" + lot_name + "</td>" +
            "<td><a data-toggle='modal' href='#delete_item_pharmacy_modal' onclick='setDeleteItemPharmacy(" + row_counter_phar + ")'>" +
            "<i class='zmdi zmdi-delete' style='font-size: 1.73em;' title='Eliminar'></i></a></td></tr>");

        pharmacy_items.push({
                id: row_counter_phar,
                item_id: $("#item_medicament_ts_autocomplete").val(),
                lot: lot_id,
                type: item_type,
                quantity: $('#item_quantity').val()
            }
        );

        row_counter_phar++;
    $("#current_lot_div").hide();
    $("#item_medicament").val("");
    $("#item_quantity").val("");
    $("#item_medicament").focus();
    $("#item_medicament_ts_autocomplete").val("");
}

function setDeleteItemPharmacy(row){
    swal({
            title: "¿Eliminar articulo de la lista?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){
            $("#item_pharmacy_"+row).remove();
            $.each(pharmacy_items, function(idx,info){
                if(info.id == row){
                    TOTAL_ASSORTMENT -= info.cost;
                    $("#cost").val(TOTAL_ASSORTMENT.toFixed(2));
                }
            });
            pharmacy_items = $.grep(pharmacy_items,
                function(o,i) { return o.id == row; },
                true);
            swal("Eliminado!", "El cobro de servicio ha sido eliminado.", "success");
            console.log(pharmacy_items);
        });
}



function save_assortment(){
    if (pharmacy_items.length == 0){
        swal("Error!", "Debe ingresar al menos un artículo a la lista.", "error");
        return;
    }

    $.ajax({
        url: $("#url_get_save_assortment").val(),
        dataType: "json",
        type: "POST",
        data: {
            pharmacy_items:pharmacy_items,
            admission:$("#hospital_admission").val(),
            folio:$("#folio").val(),
            warehouse:$("#warehouse").val()
        },
        success: function (data) {
            if(data.done){
                swal("Exito", "Surtido realizado con exito", "success");
                location.reload();
            }else{
                swal("Error!", "Ocurrio un error al guardar el surtido", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
