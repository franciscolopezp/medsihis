jQuery(document).ready(function() {
    var table = $("#data-table-hospital_service_categories");
    var grid = $("#data-table-hospital_service_categories").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-hospital_service_categories').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar la categoría de "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "La categoría ha sido eliminada exitosamente.", "success");
                        $('#data-table-hospital_service_categories').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });
    $("#search_services_btn").click(function(){
        search_services();
    });
    $("#export_excel_btn").click(function () {
        export_excel();
    });
    $("#export_pdf_btn").click(function () {
        export_pdf();
    });
});
function search_services(){
    var fecha_inicio = $('#start_date').val();
    var fecha_fin = $('#end_date').val();
    if(fecha_inicio == "" || fecha_fin == ""){
        swal("Error!", "Los campos de fechas son obligatorios.", "error");
        return;
    }
    var services_table = "<table id='services_table_content' class='table table-striped'><thead><tr>"+
        "<th>Categoría</th>"+
        "<th>Número de veces</th>"+
        "<th>Costo total</th>"+
        "</tr></thead><tbody>";
    var row = 1;
    $.ajax({
        url: $('#url_search_services').val(),
        dataType: "json",
        data: {fecha_ini:fecha_inicio,
            fecha_f:fecha_fin},
        success: function (data) {
            console.log(data);
            if (data.length > 0) {
                $.each(data, function (idx, info) {
                    services_table += "<tr>" +
                        "<td >" + info.name + "</td>" +
                        "<td >" + info.number + "</td>" +
                        "<td >$" + parseFloat(info.total).toFixed(2) + "</td>" +
                        "</tr>";
                    row++;
                });
            }else{
                swal("Datos no encontrados", "No se encontraron datos para ese rango de fechas, intente nuevamente", "info");
            }
            services_table += "</tbody></table>";
            $('#services_table').html(services_table);
        },
        error:function(){
            console.log("error");
        }
    });

}
function export_excel(){
    var fecha_inicio = $('#start_date').val();
    var fecha_fin = $('#end_date').val();
    if(fecha_inicio == "" || fecha_fin == ""){
        swal("Error!", "Los campos de fechas son obligatorios.", "error");
        return;
    }
    window.location.href  = $("#url_export_services").val()+"?fecha_ini="+fecha_inicio+"&fecha_f="+fecha_fin

}
function export_pdf(){
    var fecha_inicio = $('#start_date').val();
    var fecha_fin = $('#end_date').val();
    if(fecha_inicio == "" || fecha_fin == ""){
        swal("Error!", "Los campos de fechas son obligatorios.", "error");
        return;
    }
    var url_pdf  = $("#url_print_services_report").val()+"?fecha_ini="+fecha_inicio+"&fecha_f="+fecha_fin
    window.open(url_pdf, '_blank');
}
