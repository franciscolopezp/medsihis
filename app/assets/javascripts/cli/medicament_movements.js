jQuery(document).ready(function() {
    $("#movement_type").change(reload_grid);
    var table = $("#data-table-medicament_movements");
        var grid = $("#data-table-medicament_movements").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url:  function()
        {
            return $('#data-table-medicament_movements').data('source')+"?movement_type="+$("#movement_type").val()

        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
            default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar  "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "eliminado exitosamente.", "success");
                        $('#data-table-medicament_movements').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });
});
function reload_grid(){
    $('#data-table-medicament_movements').bootgrid("reload");
}
function check_quantity(){
    var response = false;
    if($("#medicament_movement_quantity").val() != ""){
        $.ajax({
            url: $("#url_check_quantity_waste").val(),
            dataType: "json",
            async:false,
            data: {quantity:$("#medicament_movement_quantity").val(), warehouse:$("#medicament_movement_from_warehouse_id").val(), lot:$("#medicament_movement_lot_id").val()},
            success: function (data) {
                if(data.done){
                    response = true;
                }else{
                    response = false;
                    swal("Cantidad Insuficiente",data.msg,"error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else{
        swal("Especificar la cantidad","Debe especificar una cantidad","error");
    }
    if($("#medicament_movement_from_warehouse_id").val() == $("#medicament_movement_to_warehouse_id").val()){
        swal("Error","El almacen de origen no puede ser igual que el almacen de destino","error");
        response = false;
    }
    return response;
}
function check_quantity_pharmacy(){
    var response = false;
    if($("#pharmacy_item_movement_quantity").val() != ""){
        $.ajax({
            url: $("#url_check_quantity_waste_pharmacy").val(),
            dataType: "json",
            async:false,
            data: {quantity:$("#pharmacy_item_movement_quantity").val(), warehouse:$("#pharmacy_item_movement_from_warehouse_id").val(), item:$("#pharmacy_item_movement_pharmacy_item_id").val()},
            success: function (data) {
                if(data.done){
                    response = true;
                }else{
                    response = false;
                    swal("Cantidad Insuficiente",data.msg,"error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else{
        swal("Especificar la cantidad","Debe especificar una cantidad","error");
    }

    if($("#pharmacy_item_movement_from_warehouse_id").val() == $("#pharmacy_item_movement_to_warehouse_id").val()){
        swal("Error","El almacen de origen no puede ser igual que el almacen de destino","error");
        response = false;
    }
    return response;
}