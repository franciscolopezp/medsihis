//= require cli/hospital_admission_invoice

var ID_TO_PAY = 0;
var ID_ADMISSION = 0;
var TYPE_TO_PAY = 0;
var TO_STAMP = 0;
var FISCAL_AMOUNT = 0;
var subtotal = 0;
var IVA = 0;
var iva_report;
var iva_report_included;
var importe_iva = 0;
var new_concept = 1;
var importe_isr = 0;
var concept_to_edit;
var PATIENT_NAME = "";
var MED_CONSULT_ID;
var PAYMENT_METHODS = "";
var PAYMENT_METHODS_TEXT = "";
var PAYMENT_METHODS_NAME = "";
var service_type = "";
var to_change_discount = 0;
var charge_to_close = 0;
var to_change_discount_type = "";
var folio_siguiente = 0;
jQuery(document).ready(function() {
    if($('#fiscal_emisor_open').val()!= undefined){
        get_emisor_fiscal(0);
        load_payments_admissions();

    }




    var table = $("#data-table-hospital-admission");
    table.dataTable({
        ajax: {
            url: table.data('source'),
            data: function(d) {}
        },
        columns: [
            {data:'patient', name:'patients.name|patients.last_name', defaultContent:''},
            {data:'doctor', name:'people.name|people.last_name', defaultContent:''},
            {data:'triage', name:'triages.level', defaultContent:''},
            {data:'attention_type', name:'attention_types.name', defaultContent:''},
            {data:'admission_date', name:'hospital_admissions.admission_date', defaultContent:''},
            {data: null,
                orderable:false,
                searchable:false,
                defaultContent:'',
                render : function(data, type, row, meta){
                    return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                        "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Ver\"><i class=\"zmdi zmdi-eye\"></i></a>"+
                        "<a href="+row.print_pdf_admission+" target='_blank' class=\"btn bgm-indigo btn-icon waves-effect waves-circle waves-float btn-print\"  ><i class=\"zmdi zmdi-print zmdi-hc-fw\" title=\"Imprimir\"></i></a>" +
                        "<a href=" + row.url_invoice + " class=\"btn bgm-green btn-icon waves-effect waves-circle waves-float btn-invoice\" data-row-id=\"" + row.id + "\" title=\"Facturar\"><i class=\"zmdi zmdi-file-text zmdi-hc-fw\"></i></a>"   ;
                }
            }
        ],

        initComplete: function(settings, json){
            setPositionAndClassesOptionsDatatable();
        },
        drawCallback: function( settings ) {
            default_permissions(table);
            table.find(".btn-delete").on("click", function (e) {
                var $tr_parent=$(this).parents("tr").first();
                var name = $tr_parent.children('td').eq(0).text();
                //var patient_id = $tr_parent.children('td').eq(0).text();
                var patient_id = $(this).data('row-id');
                var url_aux = $(this).data("url-delete");
                var text_confirm = "¿Esta seguro que quiere eliminar el registro "+name+" ?";
                swal({
                        title: text_confirm,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Aceptar",
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false
                    },
                    function(){
                        var ajax = $.ajax({
                            type: 'DELETE',
                            url: url_aux
                        });
                        ajax.done(function(result, status){
                            swal("Eliminado!", "El registro se ha eliminado exitosamente.", "success");
                            table.DataTable().ajax.reload(); //For datable 1.10.x
                        });

                        ajax.fail(function(error, status, msg){
                            swal("Error!", "Ha ocurrido un error.", "error");
                            console.log("Error: " + error.responseText + msg);
                        });
                    }
                );
            });
        }
    });

    var table2 =  $("#data-table-hospital_admissions_closed");
    table2.dataTable({
        ajax: {
            url: table2.data('source'),
            data: function(d) {}
        },
        columns: [
            {data:'patient', name:'patients.name|patients.last_name', defaultContent:''},
            {data:'doctor', name:'people.name|people.last_name', defaultContent:''},
            {data:'triage', name:'triages.level', defaultContent:''},
            {data:'attention_type', name:'attention_types.name', defaultContent:''},
            {data:'admission_date', name:'hospital_admissions.admission_date', defaultContent:''},
            {data: null,
                orderable:false,
                searchable:false,
                defaultContent:'',
                render : function(data, type, row, meta){
                    var options = "";
                    options += "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>";
                    options += "<a href="+row.print_pdf_admission+" target='_blank' class=\"btn bgm-indigo btn-icon waves-effect waves-circle waves-float btn-print\"  ><i class=\"zmdi zmdi-print zmdi-hc-fw\" title=\"Imprimir\"></i></a>";
                    options += "<a href='javascript:void(0)' onclick='open_admission("+row.id+")' class=\"btn bgm-orange btn-icon waves-effect waves-circle waves-float btn\"  ><i class=\"zmdi zmdi-rotate-left zmdi-hc-fw\" title=\"Abrir\"></i></a>";

                    return options;
                }
            }
        ],
        initComplete: function(settings, json){
            setPositionAndClassesOptionsDatatable();
        },
        drawCallback: function( settings ) {

        }
    });

    var html = '<div class="col-md-offset-3 col-md-9">'+
        '<div class="preloader pl-xxl">'+
        '<svg class="pl-circular" viewBox="25 25 50 50">'+
        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
        '</svg>'+
        'Generando...'+
        '</div>'+
        '</div>';
    $('#loading_image').html(html);
    $('#loading_image').hide();

    $('#loading_image_open').append(html);
    $('#loading_image_open').hide();

    if ($("#hospital_service")[0]){
        $("#hospital_service").focus();
        search_services();
    }
    $('#observations').on('keypress', function (e) {
        if(e.which === 13){
            $(this).attr("disabled", "disabled");
            generate_charge();
            $(this).removeAttr("disabled");
        }
    });
    $("#add_service_to_admission_btn").click(function(){
       generate_charge();
    });
    $("#close_admissions_btn").click(function () {
        swal({
                title: "¿Esta seguro que desea cerrar las admisiones pagadas?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    url: $('#url_close_admissions').val(),
                    dataType: "json",
                    data: {},
                    success: function (data) {
                        console.log(data);
                        if(data.done){
                            swal("Exito", "Se han cerrado "+ data.count +" admisiones", "success");
                            $("#data-table-hospital-admission").DataTable().ajax.reload();
                        }else{
                            swal("Error!", "Ocurrio un error al intentar cerrar las admisiones.", "error");
                        }
                    },
                    error:function(data){
                        swal("Error!", "Ocurrio un error al intentar cerrar las admisiones.", "error");
                    }
                });
            });
    });
});


$(function () {
    $(".delete_service_admission").click(function () {
        var url = $(this).attr("url");


        swal({
            title:'¿Confirma que desea eliminar el servicio?',
            text: "Esta operación no se puede des hacer.",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            confirmButtonClass: 'btn btn-danger',
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                $.get(url,function (resp) {
                    if(resp.done){
                        swal.close();
                        location.reload();
                    }else{
                        swal("Error","Ocurrió un error al intentar eliminar el servicio","error");
                    }
                },"json");

            }
            else {
                swal.close();
            }
        });


    });
});
function open_admission(id) {
    swal({
            title: "¿Esta seguro de volver a abrir la admisión?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, aceptar!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                url: $('#url_open_admission').val(),
                dataType: "json",
                data: {id:id},
                success: function (data) {
                    if(data.done){
                        swal("Exito", "Admisión ingresada a la lista de admisiones abiertas.", "success");
                        $("#data-table-hospital_admissions_closed").DataTable().ajax.reload();
                    }else{
                        swal("Error!", "Ocurrio un error al intentar abrir la admisión.", "error");
                    }
                },
                error:function(data){
                    swal("Error!", "Ocurrio un error al intentar abrir la admisión.", "error");
                }
            });
        });
}
function search_services(){
    $("#hospital_service").ts_autocomplete({
        url: $('#url_search_services').val(),
        items: 10,
        in_modal: true,
        callback: function (val, text, type) {
            service_type = type;
            set_service_cost();
        }
    });
}
function set_service_cost(){
    if( $("#hospital_service").val()!= "") {
        $.ajax({
            url: $("#url_get_service_cost").val(),
            dataType: "json",
            data: {patient:$("#patient_id").val(),service: $("#hospital_service_ts_autocomplete").val(),type:service_type,admission:1},
            success: function (data) {
                $("#service_cost").val(data.cost);
                if(data.is_fixed_cost){
                    $('#service_cost').prop('disabled', false);
                }else{
                    $('#service_cost').prop('disabled', true);
                }
            },
            error: function (data) {
                console.log("error controller2");
            }
        });
    }
}
function generate_charge(){
    if($("#service_cost").val()!= "" && $("#hospital_service_ts_autocomplete").val() != '' ) {
        $.ajax({
            url: $("#url_generate_cost").val(),
            dataType: "json",
            data: {service: $("#hospital_service_ts_autocomplete").val(),cost:$("#service_cost").val(),
                observations:$("#observations").val(),admission:$("#hospital_admission_id").val(),type:service_type},
            success: function (data) {
              if(data.done){
                  swal("Exito","Servicio agregado con éxto.","success");
                  location.reload();
              }else{
                  swal("Error","Ocurrio un error al agregar el servicio.","error");
              }
            },
            error: function (data) {
                console.log("error controller3");
            }
        });
    }else{
        swal("Error","Debe ingresar un costo del servicio.","error");
    }
}

function generate_discount(){
    if($("#discount_amount").val()!= "") {
        $.ajax({
            url: $("#url_generate_discount").val(),
            dataType: "json",
            data: {
                amount:$("#discount_amount").val(),
                hospital_admission_id:$("#hospital_admission_id").val()
            },
            success: function (data) {
                if(data.done){
                    swal("Exito","Descuento agregado con éxto.","success");
                    location.reload();
                }else{
                    swal("Error","Ocurrio un error al agregar el descruento.","error");
                }
            },
            error: function (data) {
                console.log("error controller4");
            }
        });
    }else{
        swal("Error","Debe ingresar el monto del descuento.","error");
    }
}
function setPayment(id,type,admission_id){
    ID_TO_PAY = id;
    ID_ADMISSION = admission_id;
    TYPE_TO_PAY = type;
    $.ajax({
        url: $("#url_get_residue").val(),
        dataType: "json",
        data: {id: ID_TO_PAY,type:TYPE_TO_PAY},
        success: function (data) {
            $("#payment_cost").val(data.residue);
        },
        error: function (data) {
            console.log("error controller5");
        }
    });
}
function generate_payment(){
    if($("#payment_cost").val() != ""){
        $("#add_transaction_btn").prop("disabled",true);
        var amounts = [];
        amounts.push({
            currency:1,
            amount: $("#payment_cost").val()
        });

        $.ajax({
            url: $("#url_generate_payment").val(),
            dataType: "json",
            data: {hospital_admission_id:$("#hospital_admission_id").val(),
                amount:$("#payment_cost").val(),
                amounts:amounts,
                t_type:1,
                transaction_type_id:5,
                payment_method_id:$("#payment_method").val(),
                account_id:$("#account").val(),
                details: $("#details").val()},
            success: function (data) {
                if(data.done){
                    swal("Exito","Pago realizado con exito.","success");
                    location.reload();
                }else{
                    swal("Error","Ocurrio un error al realizar el pago","error");
                }
            },
            error: function (data) {
                console.log("error controller6");
            }
        });
    }
}
function set_transaction_to_stamp(id,amount,patient_id,patient_name){
    TO_STAMP = id;
    PATIENT_NAME = patient_name;
    $("#payment_method").val($("#payment_method option:first").val()).selectpicker("refresh");
    clearPaymentMethodsSelectec();
    $('#account_number').val("");
    $('#payment_methods_box').val("");
    subtotal = parseFloat(amount.replace("$","").replace(",",""));
    FISCAL_AMOUNT = 0;
    importe_isr = 0;
    importe_iva = 0;
    MED_CONSULT_ID = id;
    getClinicFiscalInfo();
    loadFiscalInformations(patient_id);
    $("#stamp_modal").modal("show");
}
function clearPaymentMethodsSelectec(){
    PAYMENT_METHODS = "";
    PAYMENT_METHODS_TEXT = "";
    PAYMENT_METHODS_NAME = "";
}
function loadFiscalInformations(patient_id){
    $.ajax({
        url: $('#url_get_fiscal_info').val(),
        dataType: "json",
        data: {patient_id: patient_id},
        success: function (data) {
            if (data.length == 0){

                $("#stamp_consult_invoice").prop("disabled",true);
                swal("Error", "El paciente no tiene ningun dato fiscal.", "error");
                $('#fiscal_receptor_info').html("");
            }else {
                $("#stamp_consult_invoice").prop("disabled",false);
                var html = "";
                $.each(data, function (idx, info) {
                    html += "<option id='info_" + info.id + "' type='" + info.type + "' value='" + info.id + "'>" + info.business_name.replace(",,", " ") + " - " + info.rfc + "</option>";
                });
                $("#fiscal_info_for_invoice").html(html);
                $("#fiscal_info_for_invoice").selectpicker('refresh');
                setFiscalInfo();
            }

        }
    });
}
function setFiscalInfo(){
    var datos_receptor = "";

    var nombre_cliente = "";

    var f = new Date();
    fecha_actual = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
    var tabla_emisor = "<table id='emisor_data' class='table table-condensed' ><thead>"+
        "</thead><tbody><tr>";


    var optionselected = $('#fiscal_info_for_invoice').find('option:selected');
    var optiontype = optionselected.attr("type");
    $.ajax({
        url: $('#url_set_fiscal_info').val(),
        dataType: "json",
        Async:false,
        data: {fiscal_info_id: $('#fiscal_info_for_invoice').val(),type:optiontype},
        success: function (data) {
            $('#fiscal_amount').val(FISCAL_AMOUNT);
            $.each(data, function (idx, info) {
                $('#fiscal_id_patient').val(info.id);
                $('#fiscal_business_name').val(info.business_name.replace(",,"," "));
                $('#fiscal_rfc').val(info.rfc);
                $('#fiscal_address').val(info.address);
                $('#fiscal_locality').val(info.locality);
                $('#fiscal_suburb').val(info.suburb);
                $('#fiscal_ext_number').val(info.ext_number);
                $('#fiscal_int_number').val(info.int_number);
                $('#fiscal_zip').val(info.zip);
                $('#fiscal_city').val(info.city);
                $('#fiscal_state').val(info.state);
                $('#fiscal_country').val(info.country);
                tipo_persona = info.person_type;
                datos_receptor += "<strong>Razón Social:</strong> " + info.business_name.replace(",,"," ") + "<br><strong>RFC:</strong> "  + info.rfc;
                datos_receptor += "<br><strong>Dirección: </strong>" + info.address+ " Núm. " + info.ext_number + " Int. " + info.int_number + " Col. " + info.suburb+". ";
                datos_receptor += info.city + ",\t" + info.state + ",\t" + info.country+ ". Loc: " + info.locality  +" C.P: " + info.zip;
                $('#fiscal_receptor_info').html(datos_receptor);
                nombre_cliente = info.business_name.replace(",,"," ");

            });
            FISCAL_AMOUNT = 0;
            importe_isr = 0;
            importe_iva = subtotal * (IVA/100);

            if(tipo_persona=="Moral"){
                importe_isr = subtotal * (ISR/100);
            }else{
                ISR = 0;
            }
            FISCAL_AMOUNT = subtotal - importe_isr + importe_iva;
            var tabla_facturacion = "<table  class='table'><thead><tr><th>Cantidad</th><th>Descripción</th>"+
                "<th>U.M</th><th>P.U</th><th>Importe</th></tr></thead><tbody>" +
                "<tr><td class='text-left'>1</td><td class='text-left'><div id='invoice_description' contenteditable>Honorarios médicos. Paciente: "+PATIENT_NAME+"</div></td><td class='text-left'>No aplica</td><td class='text-left'>$ "+subtotal.toFixed(2)+"</td><td class='text-left'>$ "+subtotal.toFixed(2)+"</td></tr>"+
                "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>Subtotal:</td><td class='text-left'>$"+subtotal.toFixed(2)+"</td></tr>"+
                "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>I.V.A</td><td class='text-left'>$ "+importe_iva.toFixed(2)+"</td></tr>"+
                "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>Ret I.S.R:</td><td class='text-left'>$ "+importe_isr.toFixed(2)+"</td></tr>"+
                "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>Total:</td><td class='text-left'>$"+FISCAL_AMOUNT.toFixed(2)+"</td></tr>"+
                "</tbody></table>";
            $('#table_invoice').html(tabla_facturacion);
        }
    });



}
function add_payment_method(){
    PAYMENT_METHODS += $('#payment_method').val()+",";
    PAYMENT_METHODS_NAME += $('#payment_method').find("option:selected").text() + ",";
    PAYMENT_METHODS_TEXT += $('#payment_method').val()+":" + $('#payment_method').find("option:selected").text();
    $('#payment_methods_box').val(PAYMENT_METHODS_TEXT);
    PAYMENT_METHODS_TEXT += ",";
    $('#select_pay_method').modal('hide');
    $('body').addClass('modal-open');

}
function clearPaymentMethods(){
    $('#payment_methods_box').val("");
    PAYMENT_METHODS = "";
    PAYMENT_METHODS_TEXT = "";
    PAYMENT_METHODS_NAME = "";
}
function openPaymentMethods(){
    $('#select_pay_method').modal('show');
}
function getClinicFiscalInfo(){
    $.ajax({
        url: $('#url_get_clinic_info').val(),
        dataType: "json",
        Async:false,
        data: {},
        success: function (data) {
            if (data.done) {
                $.each(data.data, function (idx, info) {
                    iva_doc = info.iva;
                    folio_siguiente = parseInt(info.folio) + 1;
                    datos_emisor += info.business_name.replace(",,", " ") + "<br>";
                    datos_emisor += info.rfc + "<br>";
                    datos_emisor += info.address + " No. Int: " + info.int_number + "  No. Ext: " + info.ext_number + "  Col. " + info.suburb + "<br>";
                    datos_emisor += info.locality + ", " + info.city + ", " + info.state + ", " + info.country + ". C.P. " + info.zip + "<br>";
                    datos_emisor += "<strong>Serie:</strong> " + info.serie + "&nbsp;&nbsp;&nbsp;&nbsp; <strong>Folio:</strong> " + folio_siguiente;
                    datos_emisor += "<br>Fecha: " + dateToString(new Date(), "dd fm yyyy") + "\t \t \t ";
                    $('#fiscal_emisor_info').html(datos_emisor);
                });
            }else{
                swal("Error!", "No se encontraron documentos fiscales: .cer y .key. Es necesario agregarlos en su información fiscal para poder emitir facturas.", "error");
                $("#stamp_consult_invoice").removeAttr("onclick");
            }
        }
    });
}
function stamp_invoice(){
    var accountnumber = "No aplica";
    if(PAYMENT_METHODS==""){
        swal("Error!", "Debe seleccionar al menos un metodo de pago.", "error");
        return;
    }
    if(PAYMENT_METHODS.search("02")>=0 || PAYMENT_METHODS.search("03")>=0 || PAYMENT_METHODS.search("04")>=0 || PAYMENT_METHODS.search("05")>=0
        || PAYMENT_METHODS.search("06")>=0 || PAYMENT_METHODS.search("08")>=0 || PAYMENT_METHODS.search("28")>=0 || PAYMENT_METHODS.search("29")>=0){

        if ($('#account_number').val()=="") {
            swal("Error!", "Para el metodo de pago es necesario ingresar un numero de cuenta.", "error");
            return;
        }else{
            accountnumber = $('#account_number').val();
        }
    }

    int_number_pa = "-";
    ext_number_pa = "-";
    if($('#fiscal_int_number').val() != ""){
        int_number_pa = $('#fiscal_int_number').val();
    }
    if($('#fiscal_ext_number').val() != ""){
        ext_number_pa = $('#fiscal_ext_number').val();
    }

    $.ajax({
        url: $('#url_stamp_action').val(),
        dataType: "json",
        beforeSend: function(){
            $('#loading_image').show();
        },
        complete: function(){
            console.log("complete");
        },
        data: {
            amount:FISCAL_AMOUNT.toFixed(2),
            business_name:$('#fiscal_business_name').val(),
            rfc:$('#fiscal_rfc').val().toUpperCase(),
            address:$('#fiscal_address').val(),
            locality:$('#fiscal_locality').val(),
            ext_number:ext_number_pa,
            int_number:int_number_pa,
            zip:$('#fiscal_zip').val(),
            city:$('#fiscal_city').val(),
            state:$('#fiscal_state').val(),
            country:$('#fiscal_country').val(),
            payment_type:$('#payment_type').val(),
            payment_method:PAYMENT_METHODS.substring(0,PAYMENT_METHODS.length-1),
            payment_method_name:PAYMENT_METHODS_NAME.substring(0,PAYMENT_METHODS_NAME.length-1),
            suburb:$('#fiscal_suburb').val(),
            account_number:accountnumber,
            sub_total:subtotal,
            patient_fiscal: $('#fiscal_id_patient').val(),
            iva:IVA,
            isr:ISR,
            importeiva:importe_iva.toFixed(2),
            importeisr:importe_isr.toFixed(2),
            transaction_id:TO_STAMP,
            descripcion_concepto:$("#invoice_description").html()
        },
        success: function (data) {
            if(data.done){
                swal("Exito!", "Factura emitida con exito", "success");
                $('#loading_image').hide();
                location.reload();
            }

         /*   $("#create_invoice_modal").modal("hide");
            $('#loading_image').hide();
            swal({
                title:'Factura Emitida',
                text: "Se ha emitido la factura de manera correcta, ahora puede enviarla por correo electrónico al paciente.",
                type: "success",
                showCancelButton: true,
                confirmButtonText: 'Si, enviar',
                cancelButtonText: 'No, cerrar!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm){
                if (isConfirm) {
                    prepareInvoiceEmail(data.invoice);
                    swal.close();
                }
                else {
                    swal.close();
                }
            });*/
        },
        error:function(data){
            $('#loading_image').hide();
            swal("Error!", data.responseText, "error");
        }
    });
}
function cancelInvoice(invoice_id){
    swal({
        title:'Cancelación de factura',
        text: "¿Esta seguro que desea cancelar la factura?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Si, cancelar',
        cancelButtonText: 'No, cerrar!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm){
        if (isConfirm) {

            $.ajax({
                url: $('#url_cancel_invoice').val(),
                dataType: "json",
                beforeSend: function(){
                    $('#cancel_invoice_loading').show();
                },
                data: {invoice_id:invoice_id},
                success: function (data) {
                    if(data.result == "1"){
                        swal("Confirmado!", "La factura ha sido cancelada", "success");
                        location.reload();
                    }
                    else {
                        swal("Error!", data.message, "error");
                    }

                }
            });

        } else {
            swal("Cancelado!", "Se canceló la operación.", "error");
        }
    });

    $(".lead.text-muted").append("<div style='float: left;width: 25%'><span></span></div><div style='float:left;width: 50%' id='cancel_invoice_loading'></div><div style='float: left;width: 25%'><span></span></div>");

    var html = '<div class="col-md-offset-3 col-md-9">'+
        '<div class="preloader pl-xxl">'+
        '<svg class="pl-circular" viewBox="25 25 50 50">'+
        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
        '</svg>'+
        'Cancelando...'+
        '</div>'+
        '</div>';



    $('#cancel_invoice_loading').append(html);
    $('#cancel_invoice_loading').hide();
}
function addemailfield(){
    var d = new Date();
    var uniqnumber = d.valueOf();
    $("#text_emails_div").append('<div class="col-xs-12" id="newemaildiv_'+uniqnumber+'"><strong >Correo electronico:</strong> <br><input  type="text" class="form-control input-lg" id="email_receptor_added" >' +
        '<a  href="javascript:void(0);" onclick="deleteemailfield('+uniqnumber+')"><i class="zmdi zmdi-delete" style="font-size: 1.33em;"></i></a></div>');
}
function deleteemailfield(number){
    $("#newemaildiv_"+number).remove();
}
function send_invoice_email(){
    if($('#email_receptor').val()!="" || $("#emailcheck_0").is(':checked') || $("#emailcheck_1").is(':checked') || $("#emailcheck_2").is(':checked')){

    }
    else{
        swal("Error!", "Debe ingresar o seleccionar al menos un correo electronico.", "error");
        return;
    }
    if($('#email_receptor').val()!="" ){
        if (!validarEmail($('#email_receptor').val())){
            swal("Error!", "Ingrese una direccion de correo valida.", "error");
            return;
        }
    }
    var extramails = "";
    $("#text_emails_div :text").each(function(){
        if (!validarEmail($(this).val())){
            swal("Error!", "Algun correo no es una direccion de correo valida.", "error");
            return;
        }else{
            extramails += $(this).val() + ";";
        }


    });
    var emailp = "";
    var emailf1 = "";
    var emailf2 = "";
    if ($("#emailcheck_0").is(':checked')){
        emailp = $('#labele_0').text();
    }
    if ($("#emailcheck_1").is(':checked')){
        emailf1 = $('#labele_1').text();
    }
    if ($("#emailcheck_2").is(':checked')){
        emailf2 = $('#labele_2').text();
    }
    $.ajax({
        url: $('#url_send_invoice_mail').val(),
        dataType: "json",
        data: {
            email:$('#email_receptor').val(),
            emailp:emailp,
            emailf1:emailf1,
            emailf2:emailf2,
            extramails:extramails,
            invoice_id: $('#invoice_id').val()},
        success: function (data) {
            swal("Exito", "La factura ha sido enviada con exito.", "success");
            $('#email_receptor').val(data.email);
        },
        error:function(data){
            swal("Error!", "Ocurrio un error al intentar enviar el correo electronico, por favor intente mas tarde.", "error");
        }
    });
    $('#send_email_modal').modal('hide');
}

function prepareInvoiceEmail(invoice_id){
    $('#email_receptor').val("");
    $("#emails_div").html("");
    $("#text_emails_div").html("");
    emailsset = '';
    $('#invoice_id').val(invoice_id);
    $.ajax({
        url: $('#url_get_receptor').val(),
        dataType: "json",
        data: {invoice_id:invoice_id},
        success: function (data) {
            $.each(data.emails, function(idx,info){
                emailsset += '<div class = "col-xs-12">';
                if(info.number == 0){
                    emailsset +=  '<h5>Correo del paciente</h5>';
                }else if(info.number == 1){
                    emailsset +=  '<h5>Correo fiscal 1</h5>';
                }else{
                    emailsset +=  '<h5>Correo fiscal 2</h5>';
                }
                emailsset +=  '<div class="checkbox">'+
                    '<label id="labele_'+info.number+'">'+
                    '<input type="checkbox"  name="emailcheck_'+info.number+'" id="emailcheck_'+info.number+'">'+
                    '<i class="input-helper"></i>'+
                    '<strong >'+info.email+'</strong>'+
                    '</label>'+
                    '</div>';
                emailsset += '</div>';
                $("#emails_div").html(emailsset);
            });
            //$('#email_receptor').val(data.email);
        },
        error:function(data){
            console.log("fail");
        }
    });
    $('#send_email_modal').modal('show');
}
function validarEmail( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ) {
        return false;
    }
    else{
        return true;
    }

}
function setCancelPayment(id,amount) {
    amount_to_cancel = amount;
    charge_to_cancel = id;
    $("#user_name").val("");
    $("#password").val("");
    $("#cancel_payment_modal").modal("show");
}
function confirmCancelPayment(id,amount,user_id){
    var amounts = [];
    amounts.push({
        currency:1,
        amount: amount
    });
    swal({
            title: "¿Esta seguro que desea cancelar el pago?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, cancelar!",
            closeOnConfirm: false
        },
        function(isConfirm){
            if(isConfirm) {
                $.ajax({
                    url: $("#url_cancel_payment_admission_payment").val(),
                    dataType: "json",
                    data: {
                        admission_transaction_id: id,
                        amount: amount,
                        amounts: amounts,
                        t_type: 0,
                        user_id:user_id,
                        transaction_type_id: 3
                    },
                    success: function (data) {
                        if (data.done) {
                            swal("Exito", "Pago cancelado con exito.", "success");
                            location.reload();
                        } else {
                            swal("Error", "Ocurrio un error al cancelar el pago", "error");
                        }
                    },
                    error: function (data) {
                        console.log("error controller");
                    }
                });
            }else{
                $("#cancel_payment_btn").prop("disabled", false);
            }
        });
}
function setEditItemCharge(id,type){
    to_change_discount = id;
    to_change_discount_type = type;
    $("#change_discount_modal").modal("show");
}
function update_discount(){
    if($("#new_service_discount").val() != "") {
        $.ajax({
            url: $("#url_update_service_discount").val(),
            dataType: "json",
            data: {service_id: to_change_discount,type:to_change_discount_type, discount: $("#new_service_discount").val()},
            success: function (data) {
                if (data.done) {
                    swal("Exito!", "El descuento ha sido aplicado con exito", "success");
                    location.reload();
                } else {
                    swal("Error", "Error al actualizar al agregar descuento", "error");
                }
            },
            error: function (data) {
                console.log("error controller");
            }
        });
    }
    else{
        swal("Error!", "Debe especificar un descuento valido.", "error");
    }
}
function cancel_payment(){
    if($("#user_name").val()!= "" && $("#password").val()!= "") {
        $("#cancel_payment_btn").prop("disabled", true);
        $.ajax({
            url: $("#url_check_cancel_permission").val(),
            dataType: "json",
            data: {user_name: $("#user_name").val(), password: $("#password").val()},
            success: function (data) {
                if (data.done) {
                    confirmCancelPayment(charge_to_cancel,amount_to_cancel,data.user_id);
                } else {
                    $("#cancel_payment_btn").prop("disabled", false);
                    swal("Error", data.msg, "error");
                }
            },
            error: function (data) {
                console.log("error controller");
            }
        });
    }else{
        swal("Error", "Debe ingresar su nombre de usuario y contraseña", "error");
    }

}
function setCloseOtherCharge(id) {
    charge_to_close = id;
    $("#user_name_close").val("");
    $("#password_close").val("");
    $("#close_other_charge_modal").modal("show");
}
function close_other_charge(){
    if($("#user_name_close").val()!= "" && $("#password_close").val()!= "") {
        $("#close_charge_btn").prop("disabled", true);
        $.ajax({
            url: $("#url_check_close_permission").val(),
            dataType: "json",
            data: {user_name: $("#user_name_close").val(), password: $("#password_close").val()},
            success: function (data) {
                if (data.done) {
                    close_other_charge_confirm(charge_to_close);
                } else {
                    $("#close_charge_btn").prop("disabled", false);
                    swal("Error", data.msg, "error");
                }
            },
            error: function (data) {
                console.log("error controller");
            }
        });
    }else{
        swal("Error", "Debe ingresar su nombre de usuario y contraseña", "error");
    }
}
function close_other_charge_confirm(id){
    var debt_admission = parseFloat($("#debt_admission").val().replace("$", ""));
    if (debt_admission >= 0) {
        swal({
                title: "¿Esta seguro de cerrar la admisión?. Una vez hecho esto no podra realizar ninguna operación",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, cerrar!",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: $("#url_close_admission").val(),
                    dataType: "json",
                    data: {id: id},
                    success: function (data) {
                        if (data.done) {
                            swal("Exito", "Admision cerrada con exito.", "success");
                            location.reload();
                        } else {
                            swal("Error", "Ocurrio un error al cerrar el cobro", "error");
                        }
                    },
                    error: function (data) {
                        console.log("error controller");
                    }
                });
            });
    }else{
        swal("Error", "Para cerrar la admisión los pagos deben cubrir el monto del mismo.", "error");
    }
}
function validate_hospital_admission(){
    var status = true;
    if($("#cli_hospital_admission_patient_id_ts_autocomplete").val()== ""){
        swal("Error!", "Debe seleccionar un paciente.", "error");
        status = false;
    }
    if($('#cli_hospital_admission_doctor_id_ts_autocomplete').val()== ""){
        swal("Error!", "Debe seleccionar un doctor", "error");
        status = false;
    }
    return status;
}
