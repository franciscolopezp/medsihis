var row_counter_phar = 1;
var pharmacy_items = [];
var BEFORE_WAREHOUSE = 0;
jQuery(document).ready(function() {
    $("#new_lot_div").hide();
    if ($("#warehouse")[0]){
        $("#warehouse").on('focus', function () {
            BEFORE_WAREHOUSE = this.value;
        }).change(reset_items);
    }
    var table = $("#data-table-item_transfer");
    var grid = $("#data-table-item_transfer").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function()
        {
            return $('#data-table-item_transfer').data('source')+"?waste_type="+$("#waste_type").val()

        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_show + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edsssssit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar  "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "eliminado exitosamente.", "success");
                        $('#data-table-item_transfer').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });
    if($("#current_lot_div")[0]){
        $("#current_lot_div").hide();
    }
    if ($("#item_medicament")[0]){
        $("#item_medicament").ts_autocomplete({
            url: $('#url_find_medicaments_pharmacy_items').val(),
            items: 10,
            in_modal: true,
            callback: function (val, text, type) {
                var lots_options = "";
                $('#lot').html("");
                item_type = type;
                if (type == "med") {
                    var lots_founded = 0;
                    $.ajax({
                        url: $("#url_get_lot").val(),
                        data: {medicament_id: val, warehouse: $("#warehouse").val()},
                        dataType: "json",
                        success: function (data) {
                            var is_avalible = false;
                            $.each(data.lots, function (idx, info) {
                                if (info.quantity > 0) {
                                    is_avalible = true;
                                    lots_founded += 1;
                                    lots_options += "<option exp_date="+info.exp_date+" value = " + info.id + ">" + info.name + "</option>";
                                }
                            });
                                if(is_avalible) {
                                    $("#current_lot_div").show();
                                    $('#lot').html(lots_options);
                                    $('#lot').selectpicker('refresh');
                                    $("#item_quantity").focus();
                                }else{
                                    swal("Error!", "Ningun lote del almacen seleccionado tiene existencias de " + text, "warning");
                                    $("#item_medicament").val("");
                                    $("#item_medicament").focus();
                                    $("#item_medicament_ts_autocomplete").val("");
                                }


                        }
                    });
                } else {
                    $("#item_quantity").focus();
                    $("#current_lot_div").hide();
                }
            }
        });
    }
    $('#item_quantity').on('keypress', function (e) {
        if(e.which === 13){
            $(this).attr("disabled", "disabled");
            add_item_to_table();
            $(this).removeAttr("disabled");
        }
    });
    $("#lot").change(checknewlot);

    $("#prepare_cancel_btn").click(function(){
        $("#confirm_cancel_modal").modal("show");
    });
    $("#cancel_transfer_btn").click(function(){
       cancel_transfer();
    });
    $("#prepare_accept").click(function(){
        $("#confirm_modal").modal("show");
    });
    $("#confirm_accept").click(function(){
        if($("#user_name").val()!= "" && $("#password").val()!= "") {
            $("#confirm_accept").prop("disabled", true);
            $.ajax({
                url: $("#url_check_user").val(),
                dataType: "json",
                data: {user_name: $("#user_name").val(), password: $("#password").val()},
                success: function (data) {
                    if (data.done) {
                        accept_transfer();
                    } else {
                        $("#confirm_accept").prop("disabled", false);
                        swal("Error", data.msg, "error");
                    }
                },
                error: function (data) {
                    console.log("error controller");
                }
            });
        }else{
            swal("Error", "Debe ingresar su nombre de usuario y contraseña", "error");
        }
    });
    $("#prepare_reject").click(function(){
        $("#confirm_reject_modal").modal("show");
    });
    $("#confirm_reject").click(function(){
        if($("#user_name_reject").val()!= "" && $("#password_reject").val()!= ""  && $("#reason_reject").val()!= "") {
            $("#confirm_accept").prop("disabled", true);
            $.ajax({
                url: $("#url_check_user").val(),
                dataType: "json",
                data: {user_name: $("#user_name_reject").val(), password: $("#password_reject").val(),reason:$("#reason_reject").val()},
                success: function (data) {
                    if (data.done) {
                        reject_transfer();
                    } else {
                        $("#confirm_accept").prop("disabled", false);
                        swal("Error", data.msg, "error");
                    }
                },
                error: function (data) {
                    console.log("error controller");
                }
            });
        }else{
            swal("Error", "Debe ingresar su nombre de usuario, contraseña y la razon del rechazo", "error");
        }
    });
});
function checknewlot(){
    if($("#lot").val()==0){
        $("#new_lot_div").show();
    }else{
        $("#new_lot_div").hide();
    }
}


function reset_items(){
    if (pharmacy_items.length > 0) {
        swal({
                title: "Al cambiar el almacen se eliminaran todos los artículos de la lista, ¿Esta de acuerdo?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Si, cambiar almacen",
                closeOnConfirm: false
            },
            function (is_confirm) {
                if(is_confirm) {
                    $("#pharmacy_items_body").html("");
                    pharmacy_items = [];
                    swal("Lista de artículos restablecida")
                }else{
                    $("#warehouse").val(BEFORE_WAREHOUSE);
                    $("#warehouse").selectpicker("refresh");
                }
            });
    }
}
function check_quantity(){
    var response = false;
    if($("#item_quantity").val() != "" && $("#item_quantity").val()>0){
        $.ajax({
            url: $("#url_check_quantity_waste").val(),
            dataType: "json",
            async:false,
            data: {quantity:$("#item_quantity").val(), warehouse:$("#warehouse").val(), lot:$("#lot").val()},
            success: function (data) {
                if(data.done){
                    response = true;
                }else{
                    response = false;
                    swal("Cantidad Insuficiente",data.msg,"error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else{
        swal("Especificar la cantidad correcta","Debe especificar una cantidad","error");
    }
    return response;
}
function check_quantity_pharmacy(){
    var response = false;
    if($("#item_quantity").val() != "" && $("#item_quantity").val()>0){
        $.ajax({
            url: $("#url_check_quantity_waste_pharmacy").val(),
            dataType: "json",
            async:false,
            data: {quantity:$("#item_quantity").val(), warehouse:$("#warehouse").val(), item:$("#item_medicament_ts_autocomplete").val()},
            success: function (data) {
                if(data.done){
                    response = true;
                }else{
                    response = false;
                    swal("Cantidad Insuficiente",data.msg,"error");
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else{
        swal("Especificar la cantidad","Debe especificar una cantidad","error");
    }

    return response;
}
function add_item_to_table(){
    var lot_id = 0;
    var lot_name = "N/A";
    var lot_exp_date = "N/A";
    if ($('#item_medicament').val() == "" || $('#item_quantity').val() == "" || $("#item_medicament_ts_autocomplete").val() == ""){
        swal("Error!", "Los campos estan vacios.", "error");
        return;
    }
    if ($('#item_quantity').val()<=0){
        swal("Error!", "La cantidad debe ser mayor a 0", "error");
        return;
    }
    if(item_type == "med"){
        if($("#lot").val() != 0){
            lot_name =  $("#lot option:selected").text();
            lot_id = $("#lot").val();
            lot_exp_date = $("#lot option:selected").attr('exp_date');

        }else {
            if($('#new_lot_name').val() == ""){
                swal("Error!", "Debe ingresar un nombre al lote nuevo", "error");
                return;
            }else{
                lot_name = $('#new_lot_name').val();
            }
            if($('#new_lot_expiration_date').val() == ""){
                swal("Error!", "Debe ingresar una fecha de caducidad para el lote nuevo", "error");
                return;
            }else{
                lot_exp_date = $('#new_lot_expiration_date').val();
            }
            lot_id = 0;
        }

            if (!check_quantity()) {
                return;
            }
    }else{

            if (!check_quantity_pharmacy()) {
                return;
            }
    }
    $("#pharmacy_items").append("<tr id='item_pharmacy_" + row_counter_phar + "'><td style='text-align: center'>" +
        $('#item_quantity').val() + "</td><td style='text-align: center'>" + $('#item_medicament').val() + "</td>" +
        "<td style='text-align: center' >" + lot_name + "</td>" + "<td style='text-align: center' >" + lot_exp_date + "</td>"+
        "<td><a data-toggle='modal' href='#delete_item_pharmacy_modal' onclick='setDeleteItemPharmacy(" + row_counter_phar + ")'>" +
        "<i class='zmdi zmdi-delete' style='font-size: 1.73em;' title='Eliminar'></i></a></td></tr>");

    pharmacy_items.push({
            id:row_counter_phar,
            item_id:$("#item_medicament_ts_autocomplete").val(),
            quantity:$('#item_quantity').val(),
            type:item_type,
            lot:lot_id,
            lot_name:lot_name,
            exp_date:lot_exp_date
        }
    );


    row_counter_phar++;
    $("#current_lot_div").hide();
    $("#item_medicament").val("");
    $("#item_quantity").val("");
    $("#item_medicament").focus();
    $("#item_medicament_ts_autocomplete").val("");
    $("#new_lot_name").val("");
    $("#new_lot_expiration_date").val("");
    $("#new_lot_div").hide();
}

function setDeleteItemPharmacy(row){
    swal({
            title: "¿Eliminar articulo de la lista?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){
            $("#item_pharmacy_"+row).remove();
            pharmacy_items = $.grep(pharmacy_items,
                function(o,i) { return o.id == row; },
                true);
            swal("Eliminado!", "El cobro de servicio ha sido eliminado.", "success");
            console.log(pharmacy_items);
        });
}



function save_movement(){
    if (pharmacy_items.length == 0){
        swal("Error!", "Debe ingresar al menos un artículo a la lista.", "error");
        return;
    }
    if ( $("#reason").val() == "" ){
        swal("Error!", "Debe ingresar un Motivo/Razón", "error");
        return;
    }
    $.ajax({
        url: $("#url_save_transfer").val(),
        dataType: "json",
        type:"POST",
        data: {
            items:pharmacy_items,
            from_warehouse:$("#warehouse").val(),
            to_warehouse:$("#to_warehouse").val()
        },
        success: function (data) {
            if(data.done){
                swal("Exito", "Movimientos realizados con exito", "success");
                location.reload();
            }else{
                swal("Error!", "Ocurrio un error al guardar el movimiento", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}

function accept_transfer(){
    $.ajax({
        url: $("#url_accept_transfer").val(),
        dataType: "json",
        data: {transfer_id:$("#item_transfer_id").val()},
        success: function (data) {
            if(data.done){
                location.href = data.url;
            }else{
                response = false;
                swal("Error","Ocurrio un error","error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function reject_transfer(){
    $.ajax({
        url: $("#url_reject_transfer").val(),
        dataType: "json",
        data: {transfer_id:$("#item_transfer_id").val(), reject_reason:$("#reason").val()},
        success: function (data) {
            if(data.done){
                location.href = data.url;
            }else{
                response = false;
                swal("Error","Ocurrio un error","error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function cancel_transfer(){
    $.ajax({
        url: $("#url_cancel_transfer").val(),
        dataType: "json",
        data: {transfer_id:$("#item_transfer_id").val(), reject_reason:$("#reason").val()},
        success: function (data) {
            if(data.done){
                location.href = data.url;
            }else{
                response = false;
                swal("Error","Ocurrio un error","error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}