//= require cli/medical_consultations_google_marker

var default_chart_options = {
    yaxis: {
        tickColor: '#eee',
        tickDecimals: 0,
        font :{
            lineHeight: 13,
            style: "normal",
            color: "#9f9f9f",
        },
        shadowSize: 0
    },
    xaxis: {
        tickColor: '#fff',
        tickDecimals: 0,
        font :{
            lineHeight: 13,
            style: "normal",
            color: "#9f9f9f"
        },
        shadowSize: 0,
    },
    legend: {
        noColumns: 1,
        //labelBoxBorderColor: "#000000",
        position: "nw"
    },
    grid : {
        borderWidth: 1,
        borderColor: '#eee',
        show : true,
        hoverable : true,
        clickable : true
    }
};

var default_pie_options =  {
    series: {
        pie: {
            show: true,
            stroke: {
                width: 2,
            },
            label: {
                show: true,
                radius: 2/3,
                formatter: labelFormatter,
                threshold: 0
            }
        }
    },
    legend: {
        backgroundOpacity: 0.5,
        noColumns: 1,
        backgroundColor: "white",
        lineWidth: 0
    },
    grid: {
        hoverable: true,
        clickable: true
    },
    tooltip: true,
    tooltipOpts: {
        content: "%s: %y.0 , %p.0%", // show percentages, rounding to 2 decimal places
        shifts: {
            x: 20,
            y: 0
        },
        defaultTheme: false,
        cssClass: 'flot-tooltip'
    }

};

var main_data;

$(document).ready(function () {
    loadDataForCharts();

    $("<div class='flot-tooltip' class='chart-tooltip'></div>").appendTo("body");
});

$(function () {
    $("#generate_chart_btn").click(loadDataForCharts);
    $(".icon_max_min_map").click(function () {
        var parent = $(this).parent().parent().parent().parent().parent().parent();
        var class_parent = parent.attr("class");

        if(class_parent == "col-md-5"){
            $(this).removeClass("zmdi zmdi-zoom-in");
            $(this).addClass("zmdi zmdi-zoom-out");
            parent.attr("class","col-md-12");
        }else{
            $(this).removeClass("zmdi zmdi-zoom-out");
            $(this).addClass("zmdi zmdi-zoom-in");
            parent.attr("class","col-md-5");
        }

        initialize();
        diseasesByCity(main_data.diseases_by_city);

    });
    $(".icon_max_min").click(function () {
        var parent = $(this).parent().parent().parent();
        var class_parent = parent.attr("class");
        var id_parent = parent.attr("id");

        if(class_parent == "col-md-4"){
            $(this).removeClass("zmdi zmdi-zoom-in");
            $(this).addClass("zmdi zmdi-zoom-out");
            parent.attr("class","col-md-12");
            parent.find(".chart_holder_small").css("height","450px");
        }else{
            $(this).removeClass("zmdi zmdi-zoom-out");
            $(this).addClass("zmdi zmdi-zoom-in");
            parent.attr("class","col-md-4");
            parent.find(".chart_holder_small").css("height","200px");
        }


        switch (id_parent){
            case "admissions_by_triage_holder":
                admissionsByAttention(main_data.admissions_by_attention);
                break;
            case "ece_by_date_holder":
                eceByDate(main_data.ece_by_date);
                break;
            case "consultations_by_type_holder":
                consultationsByType(main_data.consultations_by_type);
                break;
            case "consultations_by_office_holder":
                consultationsByOffice(main_data.consultations_by_office);
                break;
            case "consultations_by_age_holder":
                consultationsByAge(main_data.consultations_by_age);
                break;
            case "admissions_lines":
                admissionsByDayCount(main_data.admissions_by_day_count);
                admissionsByDayTriage(main_data.admissions_by_day_triage);
                break;


        }

    });
    
    $("#summary_disease_btn").click(function () {
        $("#summary_disease").toggle();

        if($("#summary_disease").is(":visible")){
            $("#summary_disease_btn").html("Cerrar");
        }else{
            $("#summary_disease_btn").html("Ver más ...");
        }
    });
    
});


function loadDataForCharts() {
    var url = $("#generate_chart_btn").attr("url");
    $.get(url,{
        start_date: $("#start_date").val(),
        end_date: $("#end_date").val()
    },function (data) {
        main_data = data;
        $("#total_ece").html(data.ece_count);
        eceByDate(data.ece_by_date);
        consultationsByType(data.consultations_by_type);
        consultationsByAge(data.consultations_by_age);
        consultationsByOffice(data.consultations_by_office);
        admissionsByAttention(data.admissions_by_attention);
        admissionsByDayTriage(data.admissions_by_day_triage);
        admissionsByDayCount(data.admissions_by_day_count);
        patientsByCity(data.patients_by_city);
        diseasesByCity(data.diseases_by_city);
        finance(data.finance);


    },"json");
}

function eceByDate(data){
    var data_chart = [];
    var ticks = [];
    $.each(data,function (idx, item) {
        data_chart.push([idx, item.count]);
        ticks.push([idx,item.period]);
    });

    var dataset = [
        {
            label: "Expedientes",
            data: data_chart, color: "#67B7DC" }
    ];

    var options = jQuery.extend(true, {}, default_chart_options);

    options.series = {
        bars: {
            show: true
        }
    };

    options.bars = {
        align: "center",
        barWidth: 0.8
};

    options.xaxis.ticks = ticks;
    options.yaxis.tickFormatter = function (v, axis) {
        return (v / 1000).toFixed(1) + "K";
    };

    var html_id = "#ece_by_date";
    if($(html_id)[0]){
        $.plot($(html_id),dataset,options);
        $(html_id).bind("plothover", function (event, pos, item) {
            if (item) {
                var y = item.datapoint[1];
                $(".flot-tooltip").html(item.series.label + ": " + y).css({top: item.pageY+5, left: item.pageX+5}).show();
            }
            else {
                $(".flot-tooltip").hide();
            }
        });

    }
}

function consultationsByType(data) {
    var pieData = [];
    $.each(data,function (idx, item) {
        pieData.push({
            data:item.count,
            label: item.type
        })
    });
    /* Pie Chart */
    var html_id = "#consultations_by_type";
    if($(html_id)[0]){
        $.plot(html_id, pieData, default_pie_options);
    }
}


function consultationsByOffice(data) {
    var options = jQuery.extend(true, {}, default_pie_options);
    options.series.pie.radius = 500;
    var pieData = [];
    $.each(data,function (idx, item) {
        pieData.push({
            data:item.count,
            label: item.office,
            color: item.color
        })
    });
    /* Pie Chart */
    var html_id = "#consultations_by_office";
    if($(html_id)[0]){
        $.plot(html_id, pieData, options);
    }
}


function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + Math.round(series.percent) + "%</div>";
}



function consultationsByAge(data){
    var data_chart = [];
    var ticks = [];
    $.each(data,function (idx, item) {
        data_chart.push([idx, item.count]);
        ticks.push([idx,item.name]);
    });

    var dataset = [
        { label: "Consultas", data: data_chart, color: "#3ACB35" }
    ];

    var options = jQuery.extend(true, {}, default_chart_options);
    options.series = {
        bars: {
            show: true
        }
    };
    options.bars = {
        align: "center",
        barWidth: 0.8
    };
    options.xaxis.ticks = ticks;
    options.yaxis.tickFormatter = function (v, axis) {
        return v;
    };
    var html_id = "#consultations_by_age";
    if($(html_id)[0]){
        $.plot($(html_id),dataset,options);
        $(html_id).bind("plothover", function (event, pos, item) {
            if (item) {
                var y = item.datapoint[1];
                $(".flot-tooltip").html(item.series.label + ": " + y).css({top: item.pageY+5, left: item.pageX+5}).show();
            }
            else {
                $(".flot-tooltip").hide();
            }
        });

    }
}

function admissionsByAttention(data) {
    var pieData = [];
    $.each(data,function (idx, item) {
        pieData.push({
            data:item.count,
            label: item.attention,
            color: item.color
        })
    });
    /* Pie Chart */
    var html_id = "#admissions_by_triage";
    if($(html_id)[0]){
        $.plot(html_id, pieData, default_pie_options);
    }
}


function admissionsByDayTriage(data){
    var data_chart = [];
    var ticks = [];
    var dataset = [];
    var is_first_time = true;

    $.each(data,function (idx, item) {
        var data_chart = [];
       $.each(item.data,function (idx_data,item_data) {
           data_chart.push([idx_data, item_data.count]);
           if(is_first_time){
               ticks.push(item_data.date);
           }
       });
        is_first_time  = false;
       dataset.push({
           lines: { show: true, fill: false, steps: true },
           label: item.triage,
           data: data_chart,
           color: "#"+item.color
       });
    });

    var options = jQuery.extend(true, {}, default_chart_options);
    options.xaxis.ticks = ticks;
    options.yaxis.tickFormatter = function (v, axis) {
        return v;
    };

    var html_id = "#admissions_by_day";
    if($(html_id)[0]){
        $.plot($(html_id),dataset,options);
        $(html_id).bind("plothover", function (event, pos, item) {
            if (item) {
                var idx = item.datapoint[0];
                var y = item.datapoint[1];
                $(".flot-tooltip").html(item.series.label +" "+ ticks[idx]+ ": " + y).css({top: item.pageY+5, left: item.pageX+5}).show();
            }
            else {
                $(".flot-tooltip").hide();
            }
        });

    }
}


function admissionsByDayCount(data){
    var ticks = [];
    var dataset = [];
    var data_chart = [];
    $.each(data,function (idx_data,item_data) {
        var count = null;
        count = item_data.count;

        data_chart.push([idx_data, count]);
        ticks.push(item_data.date);
    });
    dataset.push({
        lines: { show: true, fill: false, steps: true },
        label: "Admisiones",
        data: data_chart,
        color: "#2196F3"
    });

    var options = jQuery.extend(true, {}, default_chart_options);
    options.xaxis.ticks = [];
    options.yaxis.tickFormatter = function (v, axis) {
        return v;
    };

    var html_id = "#admissions_by_day_count";
    if($(html_id)[0]){
        $.plot($(html_id),dataset,options);
        $(html_id).bind("plothover", function (event, pos, item) {
            if (item) {
                var idx = item.datapoint[0];
                var y = item.datapoint[1];
                $(".flot-tooltip").html(item.series.label +" "+ ticks[idx]+ ": " + y).css({top: item.pageY+5, left: item.pageX+5}).show();
            }
            else {
                $(".flot-tooltip").hide();
            }
        });

    }
}


function finance(data){
    var ticks = [];
    var dataset = [];

    var cont_series = 1;

    $.each(data,function (idx, item) {
        var data_chart = [];
        $.each(item.data,function (idx2, item2) {
            data_chart.push([cont_series, item2.amount]);
            //ticks.push([idx,item.period]);
        });

        cont_series += 1;

        item.data = data_chart;
        dataset.push(item);
    });

    var options = jQuery.extend(true, {}, default_chart_options);

    options.series = {
        bars: {
            show: true
        }
    };

    options.bars = {
        align: "center",
    };

    options.xaxis.ticks = ticks;
    options.yaxis.tickFormatter = function (v, axis) {
        return (v / 1000).toFixed(1) + "K";
    };

    var html_id = "#finance";

    if($(html_id)[0]){
        $.plot($(html_id),dataset,options);
        $(html_id).bind("plothover", function (event, pos, item) {
            if (item) {
                var y = item.datapoint[1];
                $(".flot-tooltip").html(item.series.label + ": " + formatMoney(y,"$ ")).css({top: item.pageY+5, left: item.pageX+5}).show();
            }
            else {
                $(".flot-tooltip").hide();
            }
        });

    }
}



var max, min, diff, ranges;
var min_width = 12;
var rate_width = 2;
var ranges_count = 10;

var rate_change_width = 50 * 1000;

function diseasesByCity(data){

    $.each(data,function (idx, disease) {
       $.each(disease.cities,function (count, city) {
           if(max == undefined || city.count >= max){
               max = city.count;
           }

           if(min == undefined || city.count <= min){
               min = city.count;
           }
       });
    });


    diff = (max - min) / ranges_count;
    ranges = [];
    for(var i = 0; i < ranges_count ; i++){
        ranges[i] = min + (diff * i);
    }


    for(var i = 10 ; i > 0 ; i--){
        if(max > (rate_change_width * i)){
            rate_width = rate_width * i;
            break;
        }
    }


    $("#summary_disease table tbody").html("");

    $.each(data,function (idx, disease) {
        $("#summary_disease table tbody").append("<tr>" +
            "<td><label class='mark_table' style='background: "+disease.color+"'></label></td>" +
            "<td>"+disease.disease+"</td>" +
            "<td class='f-500'>"+disease.count+"</td>" +
            "</tr>");


        $.each(disease.cities,function (count, city) {
            var city_name = "No especificado";
            if(city.city != null){
                city_name = city.city;
            }

            $("#summary_disease table tbody").append("<tr>" +
                "<td></td>" +
                "<td>"+city_name+"</td>" +
                "<td class='text-right'>"+city.count+"</td>" +
                "</tr>");



            var myLatlng = new google.maps.LatLng(parseFloat(city.lat),parseFloat(city.lng));
            var width = getCircleWidthRange(city.count);


            var infowindow = new google.maps.InfoWindow({
                content: disease.disease+" "+city.count+" incidencias"
            });

            var overlay = new CustomMarker(
                myLatlng,
                diseases_map,
                {
                    marker_id: idx+'idx',
                    width: width,
                    color: disease.color,
                    label: "",
                    count: city.count
                }
            );


            overlay.addListener('click', function() {
                infowindow.open(diseases_map, overlay);
            });
        });
    });
}


function patientsByCity(data){
    $("#patients_by_city table tbody").html("");
    $.each(data,function (idx, item) {
        $("#patients_by_city table tbody").append("<tr><td>"+item.city+"</td><td>"+item.count+"</td></tr>");
    });
}


function getCircleWidthRange(count){
    if(count == max){
        return min_width + (rate_width * ranges_count);
    }
    for(var j = 0 ; j < ranges_count - 1 ; j++){
        if(count >= ranges[j] && count < ranges[j+1]){
            return min_width + (rate_width * j);
        }
    }
}

function handleLocationError(browserHasGeolocation, pos) {

}

function formatMoney(n, currency) {
    return currency + " " + n.toFixed(2).replace(/./g, function(c, i, a) {
            return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
}


