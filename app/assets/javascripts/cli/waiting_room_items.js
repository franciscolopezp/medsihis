$(function () {
    $("#new_waiting_room_item").click(function () {
        $("#add_patient_modal").modal("show");
    });

    $("#save_waiting_room_item").click(function () {
        var url = $(this).attr("url");
        $.post(url,{
            patient_id: $("#patient_name_ts_autocomplete").val()
        },function (data) {
            if(data.done){
                location.reload();
            }
        },"json");
    });

    $("#save_valoration_btn").click(function () {
        var url = $(this).attr("url");
        var fields = [];
        $.each($(".v_field"),function (idx, item) {
            fields.push({
                id: $(item).attr("field_id"),
                val: $(item).val()
            })
        });
        $.post(url,{
            waiting_room_item_id: $("#new_valoration_item_id").val(),
            patient_id: $("#new_valoration_patient_id").val(),
            valoration_type_id: $("#valoration_type_select").val(),
            fields:fields
        }, function (data) {
            if(data.done){
                location.reload();
            }
        },"json");
    });

    $("#refresh_dashboard").click(updateDashboard);
    $("#valoration_type_select").change(loadValorationFields);

});

function updateDashboard() {
    var url_items = $("#save_waiting_room_item").attr("url");
    $.get(url_items+".json",{date:$("#date_waiting_room").val()},function (data) {
        $(".waiting_room_list").html("");
        $.each(data,function (idx, item) {
            var html_triage = "";
            if(item.triage_color != ""){
                html_triage = "<label class='triage_color' style='background: "+item.triage_color+"'></label>";
            }

            var html_drop_down = "<span class='dropdown menu_waiting_room_item'>" +
                "<a href='#' data-toggle='dropdown' aria-expanded='true' class='btn btn-link item_menu_icon' status='"+item.status_id+"' patient='"+item.patient_id+"'>" +
                "<i class='zmdi zmdi-menu' title=''></i>" +
                "</a>" +
                "<ul class='dropdown-menu'>" +
                "</ul>" +
                "</span>";

            $("#list_"+item.status_id).append("<li status_id='"+item.status_id+"' status_order='"+item.status_order+"' item_id='"+item.id+"'>" +
                "<div class='patient_name'>"+html_triage+" "+item.patient+html_drop_down+"</div>" +
                "<div>"+item.date_str+ "</div>"+
                "<small>"+item.waiting_time+"</small>"+
                "</li>")
        });

        $(".item_menu_icon").unbind("click");
        $(".item_menu_icon").click(function () {
            var item_id = $(this).parent().parent().parent().attr("item_id");
            var status = $(this).attr("status");
            var patient_id = $(this).attr("patient");
            var dropdown = $(this).parent().find(".dropdown-menu");
            $.get($("#load_menu_url").val(),{
                item_id: item_id,
                status_id: status,
                patient_id: patient_id
            },function (html) {
                dropdown.html(html);
                $(".activity_item_menu").unbind("click");
                $(".activity_item_menu").click(function () {
                    var item_id = $(this).attr("waiting_room_item_id");
                    var activity = $(this).attr("activity");
                    var patient = $(this).attr("patient");
                    var patient_name = $(this).attr("patient_name");
                    if(activity == "CREATE_ADMISSION"){
                        $("#admission_doctor_name_ts_autocomplete").val("");
                        $("#admission_doctor_name").val("");
                        $("#admission_patient_name").val(patient_name);
                        $("#admission_patient_name").prop("disabled",true);
                        $("#admission_patient_name_ts_autocomplete").val(patient);
                        $("#create_admission_item_id").val(item_id);
                        $("#add_admission_modal").modal("show");

                        $.get($("#prev_triage_url").val(),{waiting_room_item_id: item_id},function (data) {
                            if(data.triage_id != ""){
                                $("#admission_triage_id").val(data.triage_id);
                                $("#admission_triage_id").prop("disabled",true);
                                $("#admission_triage_id").selectpicker("refresh");
                            }else{
                                $("#admission_triage_id").prop("disabled",false);
                                $("#admission_triage_id").selectpicker("refresh");
                            }
                        },"json");

                    }else if(activity == "ADD_VALORATION"){
                        $("#new_valoration_item_id").val(item_id);
                        $("#new_valoration_patient_id").val(patient);
                        loadValorationFields();
                        $("#add_valoration_modal").modal("show");
                    }
                });
            });

        });

        $("#save_admission_btn").click(function () {

            if($("#admission_doctor_name_ts_autocomplete").val() == ""){
                swal("No es posible guardar","Por favor seleccione un doctor de la lista.","error");
                return;
            }

            $.post($("#create_admission_url").val(),{
                waiting_room_item_id: $("#create_admission_item_id").val(),
                patient_id: $("#admission_patient_name_ts_autocomplete").val(),
                doctor_id: $("#admission_doctor_name_ts_autocomplete").val(),
                attention_type_id: $("#admission_attention_type_id").val(),
                triage_id: $("#admission_triage_id").val(),
            },function (data) {
                if(data.done){
                    $("#add_admission_modal").modal("hide");
                    swal("Admisión hospitalaria creada","Se ha generado la admisión hospitalaria correctamente.","success");
                }
            },"json");
        });


        $( ".waiting_room_list" ).sortable({
            connectWith: ".waiting_room_list",
            stop:function (event, ui) {
                var new_status_order = parseInt(ui.item.closest('ul').attr("order"));
                var new_status_id = parseInt(ui.item.closest('ul').attr("status_id"));

                var current_status_id = parseInt(ui.item.attr("status_id"));
                var current_status_order = parseInt(ui.item.attr("status_order"));
                var item_id = parseInt(ui.item.attr("item_id"));
                var do_ajax = false;
                var data_ajax = {};


                var from_list_items = $("#list_"+current_status_id+" li");
                var to_list_items = $("#list_"+new_status_id+" li");

                var from_ids = [];
                $.each(from_list_items,function (idx, item) {
                    from_ids.push($(item).attr("item_id"));
                });


                var to_ids = [];
                $.each(to_list_items,function (idx, item) {
                    to_ids.push($(item).attr("item_id"));
                });

                data_ajax.from_ids = from_ids;
                if(current_status_id!= new_status_id){
                    data_ajax.to_ids = to_ids;
                }

                // se valida que que solo pueda ir o regresar de columna en columna
                if([current_status_order+1, current_status_order-1].indexOf(new_status_order) != -1){
                    ui.item.attr("status_order",new_status_order);
                    ui.item.attr("status_id",new_status_id);
                    data_ajax.new_status_id = new_status_id;
                    do_ajax = true;
                }else if(current_status_order == new_status_order){
                    do_ajax = true;
                }else{
                    return false;
                }

                if(do_ajax){
                    ui.item.find('.item_menu_icon').attr('status',new_status_id)
                    var url = $("#save_waiting_room_item").attr("url");
                    $.ajax({
                        url: url + "/"+item_id,
                        data:data_ajax,
                        method:"PUT",
                        success:function (data) {

                        }
                    });
                }


            }
        }).disableSelection();
    },"json");
}

$(document).ready(function () {
    updateDashboard();
    var timerUpdate = setInterval(updateDashboard,1000 * 60);
});

function loadValorationFields() {
    $.get($("#new_valoration_fields").val(),{
        valoration_type_id: $("#valoration_type_select").val()
    },function (data) {
        $("#valoration_fields_holder").html(data);
        $(".delete_new_valoration").remove();
    });
}