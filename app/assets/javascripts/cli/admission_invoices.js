jQuery(document).ready(function() {


    var grid = $("#data-table-admission_invoices").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-admission_invoices').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                var links = "<a target='_blank' href=" + row.url_print_pdf + " class=\"btn bgm-green btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"PDF\"><i class=\"zmdi zmdi-collection-pdf zmdi-hc-fw\"></i></a>" +
                    "<a  href=" + row.url_print_xml + " class=\"btn bgm-blue btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"XML\"><i class=\"zmdi zmdi-code zmdi-hc-fw\"></i></a>"+
                    "<a href='#' onclick='prepareInvoiceEmail("+row.id+")'  class=\"btn bgm-green btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-email zmdi-hc-fw\" title=\"Enviar correo\"></i></a>";
                if(!row.is_cancel){
                    links += "<a href='javascript:void(0)' onclick='cancel_admission_invoice("+row.id+")' class=\"btn bgm-red btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Cancelar\"><i class=\"zmdi zmdi-close zmdi-hc-fw\"></i></a>";
                }
                return links;



            }
        }
    })


});
function prepareInvoiceEmail(invoice_id){
    $('#email_receptor').val("");
    $("#emails_div").html("");
    $("#text_emails_div").html("");
    emailsset = '';
    $('#invoice_id').val(invoice_id);
    $.ajax({
        url: $('#url_get_receptor').val(),
        dataType: "json",
        data: {invoice_id:invoice_id},
        success: function (data) {
            $.each(data.emails, function(idx,info){
                emailsset += '<div class = "col-xs-12">';
                if(info.number == 0){
                    emailsset +=  '<h5>Correo del paciente</h5>';
                }else if(info.number == 1){
                    emailsset +=  '<h5>Correo fiscal 1</h5>';
                }else{
                    emailsset +=  '<h5>Correo fiscal 2</h5>';
                }
                emailsset +=  '<div class="checkbox">'+
                    '<label id="labele_'+info.number+'">'+
                    '<input type="checkbox"  name="emailcheck_'+info.number+'" id="emailcheck_'+info.number+'">'+
                    '<i class="input-helper"></i>'+
                    '<strong >'+info.email+'</strong>'+
                    '</label>'+
                    '</div>';
                emailsset += '</div>';
                $("#emails_div").html(emailsset);
            });
            //$('#email_receptor').val(data.email);
        },
        error:function(data){
            console.log("fail");
        }
    });
    $('#send_email_modal').modal('show');
}
function deleteemailfield(number){
    $("#newemaildiv_"+number).remove();
}
function addemailfield(){
    var d = new Date();
    var uniqnumber = d.valueOf();
    $("#text_emails_div").append('<div class="col-xs-12" id="newemaildiv_'+uniqnumber+'"><strong >Correo electronico:</strong> <br><input  type="text" class="form-control input-lg" id="email_receptor_added" >' +
        '<a  href="javascript:void(0);" onclick="deleteemailfield('+uniqnumber+')"><i class="zmdi zmdi-delete" style="font-size: 1.33em;"></i></a></div>');
}
function send_invoice_email(){
    if($('#email_receptor').val()!="" || $("#emailcheck_0").is(':checked') || $("#emailcheck_1").is(':checked') || $("#emailcheck_2").is(':checked')){

    }
    else{
        swal("Error!", "Debe ingresar o seleccionar al menos un correo electronico.", "error");
        return;
    }
    if($('#email_receptor').val()!="" ){
        if (!validarEmail($('#email_receptor').val())){
            swal("Error!", "Ingrese una direccion de correo valida.", "error");
            return;
        }
    }
    var extramails = "";
    $("#text_emails_div :text").each(function(){
        if (!validarEmail($(this).val())){
            swal("Error!", "Algun correo no es una direccion de correo valida.", "error");
            return;
        }else{
            extramails += $(this).val() + ";";
        }


    });
    var emailp = "";
    var emailf1 = "";
    var emailf2 = "";
    if ($("#emailcheck_0").is(':checked')){
        emailp = $('#labele_0').text();
    }
    if ($("#emailcheck_1").is(':checked')){
        emailf1 = $('#labele_1').text();
    }
    if ($("#emailcheck_2").is(':checked')){
        emailf2 = $('#labele_2').text();
    }
    $.ajax({
        url: $('#url_send_invoice_mail').val(),
        dataType: "json",
        data: {
            email:$('#email_receptor').val(),
            emailp:emailp,
            emailf1:emailf1,
            emailf2:emailf2,
            extramails:extramails,
            invoice_id: $('#invoice_id').val()},
        success: function (data) {
            swal("Exito", "La factura ha sido enviada con exito.", "success");
            $('#email_receptor').val(data.email);
        },
        error:function(data){
            swal("Error!", "Ocurrio un error al intentar enviar el correo electronico, por favor intente mas tarde.", "error");
        }
    });
    $('#send_email_modal').modal('hide');
}
function validarEmail( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ) {
        return false;
    }
    else{
        return true;
    }

}
function cancel_admission_invoice(id){
    swal({
        title:'Cancelación de factura',
        text: "¿Esta seguro que desea cancelar la factura?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: 'Si, cancelar',
        cancelButtonText: 'No, cerrar!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm){
        if (isConfirm) {

            $.ajax({
                url: $('#url_cancel_invoice').val(),
                dataType: "json",
                beforeSend: function(){
                    $('#cancel_invoice_loading').show();
                },
                data: {invoice_id:id},
                success: function (data) {
                    if(data.result == "1"){
                        swal("Confirmado!", "La factura ha sido cancelada", "success");
                        location.reload();
                    }
                    else {
                        swal("Error!", data.message, "error");
                    }

                }
            });

        } else {
            swal("Cancelado!", "Se canceló la operación.", "error");
        }
    });

    $(".lead.text-muted").append("<div style='float: left;width: 25%'><span></span></div><div style='float:left;width: 50%' id='cancel_invoice_loading'></div><div style='float: left;width: 25%'><span></span></div>");

    var html = '<div class="col-md-offset-3 col-md-9">'+
        '<div class="preloader pl-xxl">'+
        '<svg class="pl-circular" viewBox="25 25 50 50">'+
        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
        '</svg>'+
        'Cancelando...'+
        '</div>'+
        '</div>';



    $('#cancel_invoice_loading').append(html);
    $('#cancel_invoice_loading').hide();
}