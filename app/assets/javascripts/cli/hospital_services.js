jQuery(document).ready(function() {
    var table = $("#data-table-hospital_services");
    table.dataTable({

        ajax: {
            url: table.data('source'),
            data: function(d) {}
        },
        columns: [
            {data:'code', name:'hospital_services.code', defaultContent:''},
            {data:'name', name:'hospital_services.name', defaultContent:''},
            {data:'cost', name:'hospital_services.cost', defaultContent:''},
            {data:'category', name:'hospital_service_categories.name', defaultContent:''},
            {data:'is_fixed_cost', name:'hospital_services.is_fixed_cost', defaultContent:''},
            {data: null,
                orderable:false,
                searchable:false,
                defaultContent:'',
                render : function(data, type, row, meta){
                    var options = "";

                    options += "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-edit\" title=\"Editar\"></i></a>" +
                        "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>";

                    options += "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Enviar a inactivo\"><i class=\"zmdi zmdi-delete\"></i></button>";
                    return options;
                }
            }
        ],
        initComplete: function(settings, json){
            setPositionAndClassesOptionsDatatable();
        },
        drawCallback: function( settings ) {
            default_permissions(table);
            table.find(".btn-delete").on("click", function (e) {
                var $tr_parent=$(this).parents("tr").first();
                var name = $tr_parent.children('td').eq(0).text();
                var url_aux = $(this).data("url-delete");
                var text_confirm = "¿Eliminar el servicio "+name+" ?";

                swal({
                        title: text_confirm,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Si, eliminar!",
                        closeOnConfirm: false
                    },
                    function(){
                        var ajax = $.ajax({
                            type: 'DELETE',
                            url: url_aux
                        });
                        ajax.done(function(result, status){
                            swal("Eliminado!", "El servicio ha sido eliminado exitosamente.", "success");
                            table.DataTable().ajax.reload(); //For datable 1.10.x
                        });

                        ajax.fail(function(error, status, msg){
                            swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        });

                    });
            });



        }
    });


    /*var table = $("#data-table-hospital_services");
    var grid = $("#data-table-hospital_services").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-hospital_services').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" title=\"Editar\"><i class=\"zmdi zmdi-edit\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Eliminar\"><i class=\"zmdi zmdi-delete\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        /* Executes after data is loaded and rendered
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");

            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El servicio ha sido eliminado exitosamente.", "success");
                        $('#data-table-hospital_services').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });*/


});
function search_services(){
    var fecha_inicio = $('#start_date').val();
    var fecha_fin = $('#end_date').val();
    if(fecha_inicio == "" || fecha_fin == ""){
        swal("Error!", "Los campos de fechas son obligatorios.", "error");
        return;
    }
    var services_table = "<table id='services_table_content' class='table table-striped'><thead><tr>"+
        "<th>Código</th>"+
        "<th>Servicio</th>"+
        "<th>Número de veces</th>"+
        "<th>Costo total</th>"+
        "</tr></thead><tbody>";
    var row = 1;
    $.ajax({
        url: $('#url_search_services').val(),
        dataType: "json",
        data: {
            fecha_ini:fecha_inicio,
            fecha_f:fecha_fin,
            hospital_service_id: $("#hospital_service_id").val(),
            code: $("#code").val()
        },
        success: function (data) {
            if (data.length > 0) {
                $.each(data, function (idx, info) {
                    services_table += "<tr>" +
                        "<td >" + info.code + "</td>" +
                        "<td >" + info.name + "</td>" +
                        "<td >" + info.cont + "</td>" +
                        "<td >$" +parseFloat(info.total).toFixed(2) + "</td>" +
                        "</tr>";
                    row++;
                });
            }else{
                swal("Datos no encontrados", "No se encontraron datos para ese rango de fechas, intente nuevamente", "info");
            }
            services_table += "</tbody></table>";
            $('#services_table').html(services_table);
        },
        error:function(){
            console.log("error");
        }
    });
}
function export_excel(){
    var params = params_report();
    if(!params.good){
        swal("Error!", "Los campos de fechas son obligatorios.", "error");
        return;
    }
    window.location.href  = $("#url_export_services").val()+ params.url

}
function export_pdf(){
    var params = params_report();
    if(!params.good){
        swal("Error!", "Los campos de fechas son obligatorios.", "error");
        return;
    }
    var url_pdf  = $("#url_print_services_report").val()+ params.url
    window.open(url_pdf, '_blank');
}

function params_report(){
    var resp = {
        good:true
    };
    var fecha_inicio = $('#start_date').val();
    var fecha_fin = $('#end_date').val();

    if(fecha_inicio == "" || fecha_fin == ""){
        resp.good = false;
    }

    resp.url = "?fecha_ini="+fecha_inicio+"&fecha_f="+fecha_fin+"&hospital_service_id="+$("#hospital_service_id").val()+"&code="+$("#code").val();
    return resp;
}