var datos_emisor = "";
var concepts = [];
var row_counter = 1;
var subtotal_open_invoice = 0;
var total_invoice_open = 0;
var iva_open_invoice = 0;
var total_invoice_with_iva = 0;
var isr_open_invoice = 0;
var total_invoice_with_isr = 0;
function get_emisor_fiscal(id){
    $.ajax({
        url: $('#url_get_emisor_info').val(),
        dataType: "json",
        Async:false,
        data: {id:id},
        success: function (data) {
            console.log(data);
            if (data.done) {
                datos_emisor = "";
                $.each(data.data, function (idx, info) {
                    folio_siguiente = parseInt(info.folio) + 1;
                    datos_emisor += info.business_name.replace(",,", " ") + "<br>";
                    datos_emisor += info.rfc + "<br>";
                    datos_emisor += info.address + " No. Int: " + info.int_number + "  No. Ext: " + info.ext_number + "  Col. " + info.suburb + "<br>";
                    datos_emisor += info.locality + ", " + info.city + ", " + info.state + ", " + info.country + ". C.P. " + info.zip + "<br>";
                    datos_emisor += "<strong>Serie:</strong> " + info.serie + "&nbsp;&nbsp;&nbsp;&nbsp; <strong>Folio:</strong> " + folio_siguiente;
                    datos_emisor += "<br>Fecha: " + dateToString(new Date(), "dd fm yyyy") + "\t \t \t ";
                    $('#fiscal_emisor_open').html(datos_emisor);
                    $("#emisor_data_id").val(info.id);
                });
            }else{
                swal("Error!", data.msg, "error");
                $("#stamp_open_invoice_btn").prop("disabled",true);
            }
        }
    });
}
function change_emisor_data(){
    get_emisor_fiscal($("#emisor_fiscal_data").val());
    $("#emisor_fiscal_info_modal").modal("hide");
}

function set_receptor_data(type){
    var id = 0;
    if(type == "patient"){
        id = $("#fiscal_patient_for_search").val();
    }else{
        id = $("#fiscal_for_search").val();
    }
    $.ajax({
        url: $("#url_get_patient_fiscal_to_set").val(),
        dataType: "json",
        data: {fiscal_id:id,type:type},
        success: function (data) {
            $.each(data.fiscals, function (idx, info) {
                $('#fiscal_business_name_open').val(info.business_name);
                $('#fiscal_rfc_open').val(info.rfc);
                $('#fiscal_address_open').val(info.address);
                $('#fiscal_locality_open').val(info.locality);
                $('#fiscal_ext_number_open').val(info.ext_number);
                $('#fiscal_int_number_open').val(info.int_number);
                $('#fiscal_zip_open').val(info.zip);
                $('#fiscal_city_open').val(info.city);
                $('#fiscal_state_open').val(info.state);
                $('#fiscal_country_open').val(info.country);
                $('#fiscal_suburb_open').val(info.suburb);
            });
            $("#add_patient_fiscal_data_modal").modal("hide");
            $("#add_general_fiscal_data_modal").modal("hide");
        },
        error:function(data){
            console.log("error controller");
        }
    });
}
function openPaymentMethodsopen(){
    $('#select_pay_method_open').modal('show');
}

function add_payment_methodopen(){
    PAYMENT_METHODS += $('#payment_method_open').val()+",";
    PAYMENT_METHODS_NAME += $('#payment_method_open').find("option:selected").text() + ",";
    PAYMENT_METHODS_TEXT += $('#payment_method_open').val()+":" + $('#payment_method_open').find("option:selected").text();
    $('#payment_methods_box_open').val(PAYMENT_METHODS_TEXT);
    PAYMENT_METHODS_TEXT += ",";
    $('#select_pay_method_open').modal('hide');
}
function clearPaymentMethodsopen(){
    $('#payment_methods_box_open').val("");
    PAYMENT_METHODS = "";
    PAYMENT_METHODS_TEXT = "";
    PAYMENT_METHODS_NAME = "";
}
function load_payments_admissions(){
console.log("asa");
    $.ajax({
        url: $("#url_get_payments").val(),
        dataType: "json",
        data: {id:$("#admission_id").val()},
        success: function (data) {
            $.each(data.payments, function (idx, info) {
                concepts.push({
                    id:info.id,
                    amount:info.amount,
                    details:info.details
                });
                $("#concepts_open").append("<tr id='concept_"+row_counter+"'><td style='text-align: center'>1</td><td style='text-align: center'>No aplica</td>"+
                    "<td style='text-align: center' contenteditable>"+info.details+"</td><td style='text-align: center' >$"+info.amount.toFixed(2)+"</td><td style='text-align: center' >$"+info.amount.toFixed(2)+"</td></tr>");
                row_counter++;
            });
            $('#with_iva').attr('checked', false);
            $('#with_isr').attr('checked', false);
            calculate_totals();
            $('#concept_quantity').val("");
            $('#concept_description').val("");
            $('#concept_amount').val("");
        },
        error:function(data){
            console.log("error controller");
        }
    });
}

function calculate_totals(){
    subtotal_open_invoice = 0;
    $('#concepts_open').find('tbody').find('tr').each(function () {
        var row = $(this);
        subtotal_open_invoice += parseFloat($(this).find("td").eq(4).html().replace("$",""));
    });
    $('#open_invoice_subtotal').val("$"+subtotal_open_invoice.toFixed(2));
    calculate_with_iva();
    calculate_with_isr();
}
function calculate_with_iva(){
    total_invoice_open = 0;
    iva_open_invoice = 0;
    if ($('#with_iva').is(':checked')){
        iva_open_invoice = $('#open_invoice_subtotal').val().replace("$","")*(16/100);
        $('#total_iva_amount').val("$"+iva_open_invoice.toFixed(2));
        if ($('#with_isr').is(':checked')){
            total_invoice_with_iva = (parseFloat($('#open_invoice_total').val().replace("$",""))+iva_open_invoice).toFixed(2);
        }else{
            total_invoice_with_iva = (parseFloat($('#open_invoice_subtotal').val().replace("$",""))+iva_open_invoice).toFixed(2);
        }
    }else{
        iva_open_invoice = $('#open_invoice_subtotal').val().replace("$","")*(-16/100);
        if ($('#with_isr').is(':checked')){
            total_invoice_with_iva = (parseFloat($('#open_invoice_total').val().replace("$",""))+iva_open_invoice).toFixed(2);
        }else{
            total_invoice_with_iva = (parseFloat($('#open_invoice_subtotal').val().replace("$",""))).toFixed(2);
        }
        $('#total_iva_amount').val("$0.00");
    }



    if(total_invoice_with_iva > 0){
        $('#open_invoice_total').val("$"+ total_invoice_with_iva);
    }else{
        $('#open_invoice_total').val("$0.00");
    }

}

function calculate_with_isr(){
    isr_open_invoice = 0;
    if ($('#with_isr').is(':checked')){
        isr_open_invoice = $('#open_invoice_subtotal').val().replace("$","")*(10/100);
        if ($('#with_iva').is(':checked')){
            total_invoice_with_isr = (parseFloat($('#open_invoice_total').val().replace("$",""))-isr_open_invoice).toFixed(2);
        }else{
            total_invoice_with_isr = (parseFloat($('#open_invoice_subtotal').val().replace("$",""))-isr_open_invoice).toFixed(2);
        }
        $('#total_isr_amount').val("$"+isr_open_invoice.toFixed(2));
    }else{
        isr_open_invoice = $('#open_invoice_subtotal').val().replace("$","")*(-10/100);
        if ($('#with_iva').is(':checked')){
            total_invoice_with_isr = (parseFloat($('#open_invoice_total').val().replace("$",""))-isr_open_invoice).toFixed(2);
        }else{
            total_invoice_with_isr = (parseFloat($('#open_invoice_subtotal').val().replace("$",""))).toFixed(2);
        }
        $('#total_isr_amount').val("$0.00");
    }



    if(total_invoice_with_isr > 0){
        $('#open_invoice_total').val("$"+ total_invoice_with_isr);
    }else{
        $('#open_invoice_total').val("$0.00");
    }
}
function ValidaRfc(rfcStr) {
    var strCorrecta;
    strCorrecta = rfcStr;
    if (rfcStr.length == 12){
        var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }else{
        var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }
    var validRfc=new RegExp(valid);
    var matchArray=strCorrecta.match(validRfc);
    if (matchArray==null) {
        return false;
    }
    else
    {
        return true;
    }

}
function stamp_open_invoice(){

   var iva_porcentage = 0;
   var  isr_porcentage = 0;
    if ($('#fiscal_business_name_open').val() == "" || $('#fiscal_rfc_open').val() == ""
        || $('#fiscal_address_open').val() == "" || $('#fiscal_locality_open').val() == ""
        || $('#fiscal_ext_number_open').val() == "" || $('#fiscal_zip_open').val() == ""
        || $('#fiscal_city_open').val() == "" || $('#fiscal_state_open').val() == ""
        || $('#fiscal_country_open').val() == "" || $('#fiscal_suburb_open').val() == "")
    {
        swal("Error!", "Los campos con * son obligatorios.", "error");
        return;
    }
    if (!ValidaRfc($('#fiscal_rfc_open').val())){
        swal("Error!", "El formato del RFC es incorrecto", "error");
        return;
    }
    if (concepts.length == 0){
        swal("Error!", "No se tienen pagos en la admisión hospitalaria, no es posible generar una factura.", "error");
        return;
    }

    if ($('#with_iva').is(':checked')){
        iva_porcentage = 16;
    }
    if ($('#with_isr').is(':checked')){
        isr_porcentage = 10;
    }
    var accountnumber = "No aplica";

    if(PAYMENT_METHODS==""){
        swal("Error!", "Debe seleccionar al menos un metodo de pago.", "error");
        return;
    }
    var int_number_open = "-";
    if($('#fiscal_int_number_open').val()!=""){
        int_number_open = $('#fiscal_int_number_open').val();
    }
    if(PAYMENT_METHODS.search("02")>=0 || PAYMENT_METHODS.search("03")>=0 || PAYMENT_METHODS.search("04")>=0 || PAYMENT_METHODS.search("05")>=0
        || PAYMENT_METHODS.search("06")>=0 || PAYMENT_METHODS.search("08")>=0 || PAYMENT_METHODS.search("28")>=0 || PAYMENT_METHODS.search("29")>=0){

        if ($('#account_number_open').val()=="") {
            swal("Error!", "Para el metodo de pago es necesario ingresar un numero de cuenta.", "error");
            return;
        }else{
            accountnumber = $('#account_number_open').val();
        }
    }
    $.ajax({
        url: $('#url_stamp_admission').val(),
        dataType: "json",
        beforeSend: function(){
            $('#loading_image_open').show();
            $('#stamp_admission_invoice_btn').prop('disabled', true);
        },
        complete: function(){
            console.log("complete");
        },
        data: {
            amount:$('#open_invoice_total').val().replace("$",""),
            business_name:$('#fiscal_business_name_open').val(),
            rfc:$('#fiscal_rfc_open').val().toUpperCase(),
            address:$('#fiscal_address_open').val(),
            locality:$('#fiscal_locality_open').val(),
            ext_number:$('#fiscal_ext_number_open').val(),
            int_number:int_number_open,
            zip:$('#fiscal_zip_open').val(),
            city:$('#fiscal_city_open').val(),
            state:$('#fiscal_state_open').val(),
            country:$('#fiscal_country_open').val(),
            payment_type:$('#payment_type_open').val(),
            payment_method:PAYMENT_METHODS.substring(0,PAYMENT_METHODS.length-1),
            payment_method_name:PAYMENT_METHODS_NAME.substring(0,PAYMENT_METHODS_NAME.length-1),
            suburb:$('#fiscal_suburb_open').val(),
            account_number:accountnumber,
            sub_total:$('#open_invoice_subtotal').val().replace("$",""),
            iva:iva_porcentage,
            isr:isr_porcentage,
            hospital_admission_id:$("#admission_id").val(),
            emisor_id:$("#emisor_data_id").val(),
            conceptos:concepts,
            importeiva:$('#total_iva_amount').val().replace("$",""),
            importeisr:$('#total_isr_amount').val().replace("$",""),
        },
        success: function (data) {
            if(data.done){
                swal("Exito!", "La factura ha sido timbrada de manera exitosamente.", "success");
                window.location.href = "/cli/admisiones_hospitalarias";
                $('#stamp_open_invoice_btn').prop('disabled', true);
                /*if (patient_fiscal_info != 0){
                    swal({
                        title:'Factura Emitida',
                        text: "Se ha emitido la factura de manera correcta, ahora puede enviarla por correo electrónico al paciente.",
                        type: "success",
                        showCancelButton: true,
                        confirmButtonText: 'Si, enviar',
                        cancelButtonText: 'No, cerrar!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        closeOnConfirm: false,
                        closeOnCancel: false
                    }, function(isConfirm){
                        if (isConfirm) {
                            prepareInvoiceEmail(data.invoice);
                            swal.close();
                        }
                        else {
                            window.location.href = "/doctor/facturas/facturas_abiertas";
                        }
                    });
                }else {
                    swal("Exito!", "La factura ha sido timbrada de manera exitosamente.", "success");
                    window.location.href = "/doctor/facturas/facturas_abiertas";
                }*/
            }
            else {
                $('#loading_image_open').hide();
                $('#stamp_admission_invoice_btn').prop('disabled', false);
                swal("Error!", data.error_response, "error");
            }

        },
        error:function(data){
            $('#loading_image_open').hide();
            swal("Error!", data.responseText, "error");
        }
    });
}