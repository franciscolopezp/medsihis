$(function(){
    $("#new_valoration_btn").click(function () {
        var url = $(this).attr("url");
        $("#new_valoration_valoration_type_id").val($("#valoration_type_select").val());
        $.ajax({
            url:url,
            data:{valoration_type_id:$("#valoration_type_select").val()},
            success: function(data){
                $("#valoration_fields").html(data);
                $(".delete_new_valoration").remove();
                $("#add_valoration_modal").modal("show");
            }
        });
    });

    $("#save_valoration_btn").click(function(){
        var url = $(this).attr("url");
        var fields = $("#valoration_fields .valoration_holder").find(".v_field");

        var fields_data = [];
        $.each(fields,function(idx, item){
            if($(item).val() == ""){
                return;
            }
            fields_data.push({
                id: $(item).attr("field_id"),
                value: $(item).val()
            });
        });

        if(fields_data.length == 0){
            swal("No se puede guardar la valoración","Por favor verifique que los campos no estén vacíos.","error");
            return;
        }

        $.ajax({
            dataType:"json",
            method:"POST",
            url:url,
            data:{
                hospitalization_id: $("#hospitalization_id").val(),
                valoration_type:$("#new_valoration_valoration_type_id").val(),
                fields:fields_data
            },
            success: function(data){
                if(data.done){
                    location.reload();
                }
            }
        });
    });

    $(".show_valoration_btn").click(function(){
        var valoration = $(this).attr("patient_valoration");
        var icon = $(this).find("i");
        $("#full_valoration_"+valoration).toggle();
        if($("#full_valoration_"+valoration).is(":visible")){
            $(icon).attr("class","zmdi zmdi-caret-down-circle");
        }else{
            $(icon).attr("class","zmdi zmdi-caret-right-circle");
        }
    });

    /*******************************/

    $("#new_annotation_btn").click(function () {
        var url = $(this).attr("url");
        $("#new_annotation_annotation_type_id").val($("#annotation_type_select").val());
        $.ajax({
            url:url,
            data:{annotation_type_id:$("#annotation_type_select").val()},
            success: function(data){
                $("#annotation_fields").html(data);
                $("#add_annotation_modal").modal("show");
                $(".selectpicker").selectpicker("refresh");

                $('.date-time-picker').datetimepicker({
                    format: 'DD/MM/YYYY hh:mm a'
                });

                $('.time-picker').datetimepicker({
                    format: 'LT'
                });

                $('.date-picker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });

                $("#new_valoration_medical_annotation_btn").click(function () {
                    var url = $(this).attr("url");
                    $.ajax({
                        url:url,
                        data:{valoration_type_id:$("#medical_annotation_valoration_type_id").val()},
                        success: function(data){
                            $("#medical_annotation_valorations").append(data);
                            $("#medical_annotation_valorations .delete_new_valoration").click(function(){
                                $(this).parent().parent().parent().remove();
                            });
                        }
                    });
                });

                initSelect2();
            }
        });
    });

    $("#save_annotation_btn").click(function(){
        var url = $(this).attr("url");
        var fields = $("#annotation_fields .annotation_holder").find(".v_field");
        var fields_data = [];
        $.each(fields,function(idx, item){
            if($(item).val() == ""){
                return;
            }
            fields_data.push({
                id: $(item).attr("field_id"),
                value: $(item).val()
            });
        });

        $.each($("#annotation_fields .annotation_holder").find(".checkbox_holder"),function(idx, item){
            var checked = "";
            var name = $(this).attr("name");
            $('input[name="'+name+'"]:checked').each(function() {
                checked += this.value+"|";
            });

            fields_data.push({
                id: $(item).attr("field_id"),
                value: checked.slice(0, -1)
            });
        });

        if(fields_data.length == 0){
            swal("No se puede guardar la valoración","Por favor verifique que los campos no estén vacíos.","error");
            return;
        }

        // linked fields
        var linked_fields = $("#annotation_fields").find(".linked_field");
        var linked_fields_data = [];
        $.each(linked_fields,function(idx, item){

            var selected = $(item).val();
            var value = "";
            if(selected != undefined){
                value = selected.join(",")
            }

            if($(item).val() == ""){
                return;
            }
            linked_fields_data.push({
                id: $(item).attr("field_id"),
                value: value
            });
        });

        // end linked fields

        var valorations_data = [];
        var valorations = $("#medical_annotation_valorations .valoration_holder");
        $.each(valorations,function(idx, item){
            var fields = $(item).find(".v_field");
            var v_fields = [];
            $.each(fields,function (idx, field) {
                if($(field).val() != ""){
                    v_fields.push({
                        id: $(field).attr("field_id"),
                        value: $(field).val()
                    });
                }
            });
            valorations_data.push({
                valoration_type_id: parseInt($(item).attr("valoration_type_id")),
                fields:v_fields
            })
        });

        $.ajax({
            dataType:"json",
            method:"POST",
            url:url,
            data:{
                hospitalization_id: $("#hospitalization_id").val(),
                annotation_type:$("#new_annotation_annotation_type_id").val(),
                fields:fields_data,
                linked_fields: linked_fields_data,
                valorations:valorations_data
            },
            success: function(data){
                if(data.done){
                    location.reload();
                }
            }
        });
    });

    $(".show_annotation_btn").click(function(){
        var annotation = $(this).attr("annotation");
        var icon = $(this).find("i");
        $("#full_annotation_"+annotation).toggle();
        if($("#full_annotation_"+annotation).is(":visible")){
            $(icon).attr("class","zmdi zmdi-caret-down-circle");
        }else{
            $(icon).attr("class","zmdi zmdi-caret-right-circle");
        }
    });

    $("#save_annotation_file_btn").click(function () {
        if($("#medical_annotation_file_document").val() == ""){
            swal("No se puede agregar el archivo","Por favor seleccione un archivo válido.","error");
            return;
        }
        $("#new_medical_annotation_file").submit();
    });

    $(".upload_annotation_file").click(function(){
        $("#add_annotation_file_modal").modal("show");
        $("#medical_annotation_file_medical_annotation_id").val($(this).attr("annotation_id"));
    });
});