var new_item = 1;
var row_counter = 1;
var row_counter_phar = 1;
var item_to_edit;
var pharmacy_items = [];
var item_type = "";
var lots_founded = 0;
var total_price_purchase = 0;
jQuery(document).ready(function() {
    $("#current_lot_div").hide();
    $("#new_lot_div").hide();

    $("#item_price_buy").blur(function () {
        if ($("#item_price_buy").val() > 0) {
            $("#item_price").val($("#item_price_buy").val() * $("#item_quantity").val());
        }
    });
    $("#phar_item_price_buy").blur(function () {
        if ($("#phar_item_price_buy").val() > 0) {
            $("#phar_item_price").val($("#phar_item_price_buy").val() * $("#phar_item_quantity").val());
        }
    });
    if ($("#item_medicament")[0]){
            $("#item_medicament").ts_autocomplete({
                url: $('#url_find_medicaments_pharmacy_items').val(),
                items: 10,
                in_modal: true,
                callback: function (val, text, type) {
                    item_type = type;
                    if (type == "med") {
                        $("#current_lot_div").show();
                        $("#item_quantity").focus();
                        var lots_options = "";
                        $('#lot').html("");
                        $.ajax({
                            url: $("#url_get_medicament_lot").val(),
                            data: {medicament_id: val},
                            dataType: "json",
                            success: function (data) {
                                lots_founded = 0;
                                $.each(data.lots, function (idx, info) {
                                    lots_founded += 1;
                                    lots_options += "<option exp_date="+info.exp_date+" value = " + info.id + ">" + info.name + "</option>";
                                });
                                lots_options += "<option value='0'> Nuevo lote </option>";
                                $('#lot').html(lots_options);
                                $('#lot').selectpicker('refresh');
                                if(lots_founded == 0){
                                    $("#new_lot_div").show();
                                }
                            }
                        });
                    }else{
                        $("#current_lot_div").hide();

                    }
                    $("#item_quantity").focus();
                }
            });
        }
    if($("#url_get_last_folio").val() != undefined){
        $.ajax({
            url: $("#url_get_last_folio").val(),
            dataType: "json",
            data: {},
            success: function (data) {
                $("#folio").val(data.folio);
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }
    var table = $("#data-table-medicament_purchases");
    var grid = $("#data-table-medicament_purchases").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-medicament_purchases').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        default_permissions(table);
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar la compra con folio "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, eliminar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El proveedor ha sido eliminado exitosamente.", "success");
                        $('#data-table-medicament_purchases').bootgrid("reload");
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });
    if($("#lot_selector_div")[0]){
        $("#lot_selector_div").hide();
    }
    if($("#current_lot_div")[0]){
        $("#current_lot_div").hide();
    }
    $('#item_price_buy').on('keypress', function (e) {
        if(e.which === 13){
            $(this).attr("disabled", "disabled");
            add_item_to_table();
            $(this).removeAttr("disabled");
        }
    });
    $("#lot").change(checknewlot);
});
function checknewlot(){
    if($("#lot").val()==0){
        $("#new_lot_div").show();
    }else{
        $("#new_lot_div").hide();
    }
}
function add_item_to_table(){
    var lot_name = "N/A";
    var lot_id = 0;
    var lot_exp_date = "N/A";
    if ($("#item_medicament_ts_autocomplete").val() == ""){
        swal("Error!", "Debe seleccionar un artículo", "error");
        return;
    }
    if ($('#item_quantity').val() == "" || $('#item_quantity').val() <= 0){
        swal("Error!", "Debe ingresar una cantidad mayor a 0", "error");
        return;
    }
    if ($('#item_price_buy').val() == "" || $('#item_price_buy').val() <= 0){
        swal("Error!", "Debe ingresar un costo unitario mayor a 0", "error");
        return;
    }
    if(item_type == "med"){
        if($("#lot").val() != 0){
                lot_name =  $("#lot option:selected").text();
                lot_id = $("#lot").val();
                lot_exp_date = $("#lot option:selected").attr('exp_date');

        }else {
            if($('#new_lot_name').val() == ""){
                swal("Error!", "Debe ingresar un nombre al lote nuevo", "error");
                return;
            }else{
                lot_name = $('#new_lot_name').val();
            }
            if($('#new_lot_expiration_date').val() == ""){
                swal("Error!", "Debe ingresar una fecha de caducidad para el lote nuevo", "error");
                return;
            }else{
                lot_exp_date = $('#new_lot_expiration_date').val();
            }
            lot_id = 0;
        }
    }
    var total_price = parseFloat($('#item_price_buy').val()) * $('#item_quantity').val();
    var item_buy_cost = parseFloat($('#item_price_buy').val());
    $("#pharmacy_items").append("<tr id='item_pharmacy_"+row_counter_phar+"'><td style='text-align: center'>"+
        $('#item_quantity').val()+"</td><td style='text-align: center'>"+$('#item_medicament').val()+"</td>"+
            "<td style='text-align: center'>"+lot_name+"</td><td style='text-align: center'>"+lot_exp_date+"</td>"+
        "<td style='text-align: center' >$ "+item_buy_cost.toFixed(2)+"</td>" +
        "<td style='text-align: center' >$ "+total_price.toFixed(2)+"</td>" +
        "<td><a data-toggle='modal' href='#delete_item_pharmacy_modal' onclick='setDeleteItemPharmacy("+row_counter_phar+")'>" +
        "<i class='zmdi zmdi-delete' style='font-size: 1.73em;' title='Eliminar'></i></a></td></tr>");

    pharmacy_items.push({
            id:row_counter_phar,
            item_id:$("#item_medicament_ts_autocomplete").val(),
            cost: total_price.toFixed(2),
            quantity:$('#item_quantity').val(),
            buy_cost:item_buy_cost.toFixed(2),
            type:item_type,
            lot:lot_id,
            lot_name:lot_name,
            exp_date:lot_exp_date
        }
    );
    total_price_purchase += total_price;
    row_counter_phar++;
    $("#cost").val("$ " +total_price_purchase.toFixed(2));
    $("#item_medicament_ts_autocomplete").val("");
    $("#item_medicament").val("");
    $('#item_quantity').val("");
    $('#item_price_buy').val("")
    $("#current_lot_div").hide();
    $("#new_lot_div").hide();
    $("#new_lot_name").val("");
    $("#new_lot_expiration_date").val("");
}
function add_medicament_to_table(){
    var lot = "";
    var expiration_date = "";
    var lot_id = 0;
    var correct_lot = true;
    if ($('#item_quantity').val() == "" || $('#item_price_buy').val() == "" || $('#item_price').val() == "" || $("#item_medicament_ts_autocomplete").val() == ""){
        swal("Error!", "Debe llenar todos los campos", "error");
        return;
    }

    if($('#exist_lot').is(':checked')){
        $.ajax({
            url: $("#url_get_lot").val(),
            dataType: "json",
            async:false,
            data: {lot_id:$('#lot').val()},
            success: function (data) {
                lot = data.lot_name;
                expiration_date = data.expiration_date;
                lot_id = $('#lot').val()
                if(data.medicament_id != $("#item_medicament_ts_autocomplete").val()){
                    correct_lot = false;
                }
            },
            error:function(data){
                console.log("error controller");
            }
        });
    }else if ($('#new_lot').is(':checked')){
        lot = $('#new_lot_name').val();
            expiration_date = $('#new_lot_expiration_date').val();
            lot_id = 0;
    }else{
        swal("Error!", "Debe seleccionar un lote", "error");
        return;
    }
    if (!correct_lot){
        swal("Error!", "El lote pertenece a otro medicamento", "error");
        return;
    }
    if(new_item == 1){
        $("#medicaments_items").append("<tr id='item_"+row_counter+"'><td style='text-align: center'>"+
            $('#item_quantity').val()+"</td><td medicamnt = "+$("#item_medicament_ts_autocomplete").val()+" style='text-align: center'>"+$('#item_medicament').val()+"</td>"+
            "<td lot_id = "+lot_id+" style='text-align: center'>"+lot+"</td><td style='text-align: center' >"+expiration_date+"</td><td style='text-align: center' >$ "+$('#item_price').val()+"</td><td style='text-align: center' >$ "+$('#item_price_buy').val()+"</td>" +
            "<td><a data-toggle='modal' href='#delete_concept_modal' onclick='setDeleteItem("+row_counter+")'>" +
            "<i class='zmdi zmdi-delete' style='font-size: 1.73em;' title='Eliminar'></i></a></td></tr>");
           /* +"<a style='margin-left: 10px' data-toggle='modal' href='#add_concept_modal' onclick='setEditItem("+row_counter+")'>" +
            "<i class='zmdi zmdi-edit' style='font-size: 1.73em;' title='Editar'></i></a></td></tr>");*/
        row_counter++;
    }else{
        $('#concept_'+concept_to_edit).each(function () {
            var row = $(this);
            $(this).find("td").eq(0).html($('#concept_quantity').val());
            $(this).find("td").eq(1).html($('#concept_unity').val());
            $(this).find("td").eq(2).html($('#concept_description').val());
            $(this).find("td").eq(3).html('$'+parseFloat($('#concept_amount').val()).toFixed(2));
            $(this).find("td").eq(4).html('$'+total_concept.toFixed(2));
        });
    }
    $('#add_medicament_modal').modal("hide");
    clear_add_medicament();
}
function clear_add_item(){
    $('#item_pharmacy').val("");
    $('#phar_item_price').val("");
    $('#phar_item_price_buy').val("");
    $('#phar_item_quantity').val("");
}
function clear_add_medicament(){
    $('#exist_lot').attr('checked', false);
    $('#new_lot').attr('checked', false);
    $("#current_lot_div").hide();
    $("#new_lot_div").hide();
    $('#item_medicament').val("");
    $('#item_price').val("");
    $('#item_price_buy').val("");
    $('#item_quantity').val("");
    $('#new_lot_expiration_date').val("");
    $('#new_lot_name').val("");
    new_concept = 1;
}
function setDeleteItemPharmacy(row){
    swal({
            title: "¿Eliminar articulo de la lista?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){
            $("#item_pharmacy_"+row).remove();
            pharmacy_items = $.grep(pharmacy_items,
                function(o,i) { return o.id == row; },
                true);
            swal("Eliminado!", "El articulo ha sido eliminado.", "success");
            console.log(pharmacy_items);
        });
}
function setDeleteItem(row){
    swal({
            title: "¿Eliminar medicamento de la lista?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Si, eliminar!",
            closeOnConfirm: false
        },
        function(){
            $("#item_"+row).remove();
                swal("Eliminado!", "El medicamento ha sido eliminado.", "success");
        });
}
function setEditItem(row){
    new_concept = 0;
    $('#item_'+row).each(function () {
        $('#item_quantity').val($(this).find("td").eq(0).html());
        $('#concept_description').val($(this).find("td").eq(2).html());
        $('#concept_amount').val($(this).find("td").eq(3).html().replace("$",""));
    });
    item_to_edit = row;
}
function load_new_lot(){
    if($('#new_lot').is(':checked')){
        $('#exist_lot').attr('checked', false);
        $("#new_lot_div").show();
        $("#current_lot_div").hide();
    }else{
        $("#new_lot_div").hide();
    }
}
function load_lots(){
    if($('#exist_lot').is(':checked')){
        $('#new_lot').attr('checked', false);
        $("#new_lot_div").hide();
        $("#current_lot_div").show();
    }else{
        $("#current_lot_div").hide();
    }
}
function save_purchase(){
       if ($('#cost').val() == "" || $('#cost').val() <= 0)
    {
        swal("Error!", "El costo no puede ser menor o igual a 0.", "error");
        return;
    }
    if ($('#invoice_number').val() == "" )
    {
        swal("Error!", "Debe ingresar un número de factura.", "error");
        return;
    }
    if (pharmacy_items.length == 0){
        swal("Error!", "Debe ingresar al menos un medicamento o artículo a la lista de compras.", "error");
        return;
    }
    $.ajax({
        url: $("#url_get_save_purchase").val(),
        dataType: "json",
        type: "POST",
        data: {pharmacy_items:pharmacy_items,
            supplier:$("#supplier").val(),cost:total_price_purchase,
            folio:$("#folio").val(),invoice_number:$("#invoice_number").val(),warehouse:$("#warehouse").val()},
        success: function (data) {
            if(data.done){
                swal("Exito", "Compra guardada con exito", "success");
                location.reload();
            }else{
                swal("Error!", "Ocurrio un error al guardr la compra", "error");
            }
        },
        error:function(data){
            console.log("error controller");
        }
    });
}