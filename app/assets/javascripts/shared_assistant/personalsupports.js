jQuery(document).ready(function() {

    $('form#new_personalsupport').find("input[type=text], textarea").val("")
    $("#new_personalsupport").validate();
    var grid = $("#data-table-personal").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-personal').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-edit\" title=\"Editar\"></i></a>" +
                    "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\"><i class=\"zmdi zmdi-delete\" title=\"Eliminar\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var last_name = $tr_parent.children('td').eq(2).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar al personal "+name+" "+last_name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Eliminar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El personal ha sido eliminado exitosamente.", "success");
                        grid.bootgrid('reload');
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });


    $('#new_personalsupport').submit(function(e){
        $( "#submit_new_personal" ).prop( "disabled", true );
        if($('#new_personalsupport').valid()){
            return;
        }else{
            e.preventDefault();
            $( "#submit_new_personal" ).prop( "disabled", false );
        }
    });

});
