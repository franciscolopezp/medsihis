var DOCTOR_ID_MODAL = 0;
var med_consultations_id = "#med_consult_a";
var med_consult_class = {
    min:"col-sm-3",
    res:"col-sm-6",
    max:"col-sm-12"
};
var med_consult_session_key = "med_consult_size";
var med_consult_sizes = {
    min:"min",
    res:"res",
    max:"max"
};

var CHARGE_CONSULT_ID = 0;
var CHARGE_REPORT_ID = 0;

$(document).ready(function(){
    if(is_dashboard()){
        loadPendingConsults();
        loadPendingReports();
        $("#charge_payment_method").change(setupByPaymentMethod);
    }
});

$(function(){
    $("#min_med_consultations").click(minConsultations);
    $("#res_med_consultations").click(resConsultations);
    $("#max_med_consultations").click(maxConsultations);
});


function minConsultations(){
    $(med_consultations_id).parent().attr("class",med_consult_class.min);
    $.session.set(med_consult_session_key,med_consult_sizes.min);
    $("#min_med_consultations").hide();
    $("#res_med_consultations").show();
    $("#max_med_consultations").show();
}

function resConsultations(){
    $(med_consultations_id).parent().attr("class",med_consult_class.res);
    $.session.set(med_consult_session_key,med_consult_sizes.res);
    $("#min_med_consultations").show();
    $("#res_med_consultations").hide();
    $("#max_med_consultations").show();
}

function maxConsultations(){
    $(med_consultations_id).parent().attr("class",med_consult_class.max);
    $.session.set(med_consult_session_key,med_consult_sizes.max);
    $("#min_med_consultations").show();
    $("#res_med_consultations").show();
    $("#max_med_consultations").hide();
}

function loadPendingConsults(){
    var offices_consults = [0];
    if(assistant_permissions.daily_cash.length > 0){
        offices_consults = assistant_permissions.daily_cash;
    }
    $.ajax({
        url:"/asistente/index/pending_medical_consult",
        dataType:"json",
        data:{office_id:offices_consults},
        success:function(data){
            if(data.length > 0){
                //$('#message_pending_medical_consult').hide();
                $.each(data,function(idx,consult){
                    drawConsult(consult,"consult"); // socket para las consultas
                });
            }else{
                //$('#message_pending_medical_consult').show();
            }
        }
    });
}
function loadPendingReports(){
    var offices_consults = [0];
    if(assistant_permissions.daily_cash.length > 0){
        offices_consults = assistant_permissions.daily_cash;
    }

   /* if(!charge_reports){
        return;
    }*/

    $.ajax({
        url:"/asistente/index/pending_report",
        dataType:"json",
        data:{office_id:offices_consults},
        success:function(data){
            $.each(data,function(idx,consult){
                drawConsult(consult,"report");
            });
        }
    });
}
function setupByPaymentMethod(){
    var payment_method = $("#charge_payment_method").val();
    var affect_local = $("#payment_method_"+payment_method).attr("affect_local");
    var all_accounts = JSON.parse($('#all_accounts_doctors').val());
    $('#charge_account').html("");
    $('#charge_account').append($('<option>', {
        id: "account_",
        value: 0,
        text : "Seleccione una cuenta",
        iso_code: "",
        currency: 0
    }));

    $.each(all_accounts,function(index,val){
        if(parseInt(val.doctor_id) == parseInt(DOCTOR_ID_MODAL)){
            if(affect_local == val.affect_local) {
                if(val.affect_local) {
                    $('#charge_account').append($('<option>', {
                        id: "account_" + val.id,
                        value: val.id,
                        text: val.account_number,
                        iso_code: val.iso_code,
                        currency: val.currency
                    }));
                }else{
                    $('#charge_account').append($('<option>', {
                        id: "account_" + val.id,
                        value: val.id,
                        text: val.bank,
                        iso_code: val.iso_code,
                        currency: val.currency
                    }));
                }
            }
        }
    });
    $('#charge_account').selectpicker("refresh");

}
function openChargeModal(consult_id,price,doctor_id,type){
    $("#charge_payment_method").val($("#charge_payment_method option:first").val()).selectpicker("refresh");
    $("#doctor_currency").val($("#doctor_currency option:first").val()).selectpicker("refresh");
    DOCTOR_ID_MODAL = doctor_id;
    var payment_method = $("#charge_payment_method").val();
    var affect_local = $("#payment_method_"+payment_method).attr("affect_local");
    CHARGE_CONSULT_ID = consult_id;
    $("#quick_charge_type").val(type);
    CHARGE_REPORT_ID =consult_id;
    var all_accounts = JSON.parse($('#all_accounts_doctors').val());
    var all_exchanges = JSON.parse($('#all_exchanges_doctors').val());

    $('#charge_account').html("");
    $('#doctor_currency').html("");
    $('#charge_account').append($('<option>', {
        id: "open_account_0",
        value: "",
        text : "Seleccione una cuenta local",
        iso_code: "",
        currency: 0
    }));

    $.each(all_accounts,function(index,val){
        if(parseInt(val.doctor_id) == parseInt(doctor_id)){
            if(affect_local == val.affect_local) {
                $('#charge_account').append($('<option>', {
                    id: "account_" + val.id,
                    value: val.id,
                    text: val.account_number,
                    iso_code: val.iso_code,
                    currency: val.currency
                }));
            }
        }
    });
    $.each(all_exchanges, function(index,val){
       if(parseInt(val.doctor_id) == parseInt(doctor_id)){
           $('#doctor_currency').append($('<option>',{
               id:"currency_doc_" + val.currency_id,
               value: val.currency_id,
               text: val.iso_code
           }))
       }
    });
    $('#charge_account').selectpicker("refresh");
    $('#doctor_currency').selectpicker("refresh");

    $("#generate_charge_modal").modal("show");
    $("#charge_amount").val(price);
    $("#charge_amount2").val(price);
    $("#charge_details").val("");
    force_charge = false;
}

var force_charge = false;

function addCharge(){
    var payment_method = $("#charge_payment_method").val();
    var affect_local = $("#payment_method_"+payment_method).attr("affect_local");
    var account_to_charge = $("#charge_account").val();
    var selected_account_currency = $("#account_"+account_to_charge).attr("currency");

    var amount = $("#charge_amount").val();
    var payment_method_id = $("#charge_payment_method").val();
    var details = $("#charge_details").val();
    var account = $("#charge_account").val();
    var currency = $("#doctor_currency").val();
    var currency_iso_code = $("#account_"+account_to_charge).attr("iso_code");
    console.log(selected_account_currency);
    console.log(currency);
    console.log(currency_iso_code);
    if(account == ""){
        swal("No se puede generar el cobro","Por favor seleccione una cuenta.","error");
        return;
    }
    if(affect_local!=1){
        if(selected_account_currency != currency){
            swal("No se puede guardar el cobro","La cuenta solo acepta tipo de cambio " + currency_iso_code,"error");
            return;
        }
    }

    var amount1 = parseFloat(amount);
    var amount2 = parseFloat($("#charge_amount2").val());
    if(!force_charge){
        if(amount1 > amount2){
            swal({
                    title: "El monto exede",
                    text: "El monto a cobrar exede por "+toCurrency((amount1 - amount2))+" ¿Desea agregar el cobro de todos modos?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: "Cancel",
                    confirmButtonText: "Aceptar",
                    closeOnConfirm: true },
                function(){
                    force_charge = true;
                    addCharge();
                });

            return;
        }
    }


  $.ajax({
        url:$("#add_charge_url").val(),
        dataType:"json",
        data:{
            account_id:account,
            consult_id:CHARGE_CONSULT_ID,
            report_id:CHARGE_REPORT_ID,
            amount:amount,
            type:$("#quick_charge_type").val(),
            currency:currency,
            payment_method_id:payment_method_id,
            details:details
        },
        success:function(data){
            var obj = null;
            if("string" == typeof data){
                obj = JSON.parse(data);
            }else{
                obj = data;
            }

            if(obj.done){
                $("#generate_charge_modal").modal("hide");
                $("#consultation_items").html("");
                loadPendingConsults();
                loadPendingReports();
            }else{
                swal(obj.error, obj.message, "error");
            }
        }
    });
}