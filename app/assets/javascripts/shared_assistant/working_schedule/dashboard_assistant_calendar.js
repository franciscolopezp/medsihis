var MEDICAL_APPOINTMENT = 1;
var ACTIVITY_TYPE = 0;
var IS_OWNER = false;
var A_TYPE = {
    appointment : 1,
    surgery : 2
};

var act_status = {
    created:1,
    waiting:2,
    consult:3,
    attended:4,
    cancelled:5,
    confirm_pending:12
};

var calendar_assistant_id = "#calendar_assistant";
var cal = {
    min:"col-sm-2",
    res:"col-sm-6",
    max:"col-sm-12"
};

var cal_session_key = "calendar_size";
var cal_sizes = {
    min:"min",
    res:"res",
    max:"max"
};
var monthNames = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
    'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

var loaded_activities = false;

function minCalendar(){
    $(calendar_assistant_id).parent().attr("class",cal.min);
    $(calendar_assistant_id).hide();
    $(calendar_assistant_id+"_min").show();
    $.session.set(cal_session_key,cal_sizes.min);
    $("#min_calendar_assistant").hide();
    $("#res_calendar_assistant").show();
    $("#max_calendar_assistant").show();
}

function resCalendar(){
    $(calendar_assistant_id).fullCalendar('option', 'height', 300);
    $(calendar_assistant_id).parent().attr("class",cal.res);
    $(calendar_assistant_id).show();
    $(calendar_assistant_id+"_min").hide();
    $.session.set(cal_session_key,cal_sizes.res);
    $("#min_calendar_assistant").show();
    $("#res_calendar_assistant").hide();
    $("#max_calendar_assistant").show();
    if(!loaded_activities){
        loadActivities();
    }
}

function maxCalendar(){
    $(calendar_assistant_id).fullCalendar('option', 'height', 600);
    $(calendar_assistant_id).parent().attr("class",cal.max);
    $(calendar_assistant_id).show();
    $(calendar_assistant_id+"_min").hide();
    $.session.set(cal_session_key,cal_sizes.max);
    $("#min_calendar_assistant").show();
    $("#res_calendar_assistant").show();
    $("#max_calendar_assistant").hide();
    if(!loaded_activities){
        loadActivities();
    }
}

$(function(){
    $("#min_calendar_assistant").click(minCalendar);
    $("#res_calendar_assistant").click(resCalendar);
    $("#max_calendar_assistant").click(maxCalendar);

    $("#all_day_activity").click(function(){
        if($(this).is(":checked")){
            $("#activity_start_time").parent().hide();
            $("#activity_end_time").parent().hide();
            if($("#activity_start_time").val() == "")
                $("#activity_start_time").val("12:01 AM");
            if($("#activity_end_time").val() == "")
                $("#activity_end_time").val("11:59 PM");
        }else{
            $("#activity_start_time").parent().show();
            $("#activity_end_time").parent().show();
        }
    });

    /*
    * $("#change_office_btn").click(function(){
     $("#current_office_select").show();
     });

     $("#current_office_id").change(function(){
     var text = $("#current_office_id option:selected").text();
     $("#current_office_name").html(text);
     $("#current_office_select").hide();
     loadActivities();
     });*/

    $('body').on('click', 'button.fc-prev-button', function() {
        loadActivities();
    });

    $('body').on('click', 'button.fc-next-button', function() {
        loadActivities();
    });

    $("#activity_patient").change(function(){
        var value = $("#activity_patient_ts_autocomplete_text").val();
        if(value != ""){
            $("#activity_name").val("Cita " + value);
        }else{
            $("#activity_name").val("");
        }
    });

    $("#activity_start_time").on("dp.change",function(){
        var office = $("#activity_office").val();
        if(office != "" && ACTIVITY_TYPE == MEDICAL_APPOINTMENT){
            var duration = $("#office_"+office).attr("duration");
            if(duration != undefined){
                var date = getDate($("#activity_start_date").val(),$("#activity_start_time").val());
                var date2 = new Date(date.getTime() + duration*60000);
                var hour_end = dateToString(date2,"hh:mn pp");
                $("#activity_end_time").val(hour_end);
            }
        }
    });


    $("#doctor_agenda_select").change(function(){
        loadActivities();
    });

});

$(document).ready(function() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    var cId = $(calendar_assistant_id);

    //Generate the Calendar
    cId.fullCalendar({
        minTime:'06:00:00',
        allDayText: 'Todo el día',
        monthNames:monthNames,
        dayNames:['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
        dayNamesShort:['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
        header: {
            right: '',
            center: 'prev, title, next',
            left: ''
        },
        theme: true, //Do not remove this as it ruin the design
        selectable: true,
        selectHelper: true,
        editable: true,
        events: [],
        select: function(start, end, allDay) {
            ACTIVITY_ACTION = "add";
            $("#doctor_current").prop("disabled",false);
            $("#doctor_current").selectpicker('refresh');
            resetFormAddActivity();
            $('#addNew-event').modal('show');
            var sd = start._d;
            sd.setTime(sd.getTime() + (1 * 24 * 60 * 60 * 1000));
            $("#activity_start_date").val(dateToString(sd,"dd/mm/yyyy"));
            var ed = end._d;
            $("#activity_end_date").val(dateToString(ed,"dd/mm/yyyy"));
        },
        eventRender: function(event, element) {
            var str_data = getPopoverHtml(event);

            element.popover({
                title: event.title,
                placement: 'top',
                trigger:"hover",
                content: str_data,
                html:true
            });

        },
        eventClick: function(calEvent, jsEvent, view) {
            loadFullActivity(calEvent.id);
        },
        eventDrop:function(event,dayDelta, minuteDelta, allDay, revertFunct){
            var days = dayDelta._days; // número de días que se corrió la actividad puede ser positivo o negativo

            var current_start = stringToDate(event.s_date,"yyyy-mm-dd hh:mn");   // obtengo la fecha original de inicio
            var current_end = stringToDate(event.e_date,"yyyy-mm-dd hh:mn"); // fecha original de fin

            var update_start = new Date(current_start.getTime()); // se incializan las variable con las que se va a actualizar la fecha por default es la misma
            var update_end = new Date(current_end.getTime()); // se incializan las variable con las que se va a actualizar la fecha por default es la misma


            // se modifican las fechas actuales agregando los días de diferencia y se generan dos variables auxiliares para verificar la disponibilidad
            current_start = current_start.setTime(current_start.getTime() + (days * 24 * 60 * 60 * 1000));
            current_end = current_end.setTime(current_end.getTime() + (days * 24 * 60 * 60 * 1000));
            var n_start = new Date(current_start);
            var n_end = new Date(current_end);


            if(!validate_rank_date_appointment(n_start,n_end,event.doctor_id)){
                // si está disponible se sobreescriben las variables update_start y update_end
                update_start = n_start;
                update_end = n_end;
            }

            $.ajax({
                url:"/doctor/activities/update_dates",
                data:{
                    activity_id: event.id,
                    start_date : update_start ,
                    end_date : update_end,
                    doctor_id : event.doctor_id
                }
            });

        }
    });

    //Create and ddd Action button with dropdown in Calendar header.
    var actionMenu = '<ul class="actions actions-alt" id="fc-actions">' +
        '<li class="dropdown">' +
        '<a href="" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>' +
        '<ul class="dropdown-menu dropdown-menu-right">' +
        '<li class="active">' +
        '<a data-view="month" href="">Mes</a>' +
        '</li>' +
        '<li>' +
        '<a data-view="basicWeek" href="">Semana</a>' +
        '</li>' +
        '<li>' +
        '<a data-view="basicDay" href="">Día</a>' +
        '</li>' +
        '<li>' +
        '<a data-view="agendaDay" href="">Agenda del día</a>' +
        '</li>' +
        '<li>' +
        '<a data-view="agendaWeek" href="">Agenda Semanal</a>' +
        '</li>' +
        '</ul>' +
        '</div>' +
        '</li>';


    cId.find('.fc-toolbar').append(actionMenu);
    //Event Tag Selector
    (function(){
        $('body').on('click', '.event-tag > span', function(){
            var activity_type = $(this).attr("activity-type");
            $('.event-tag > span').removeClass('selected');
            $(this).addClass('selected');
            updateStatusSelect(activity_type);
            ACTIVITY_TYPE = activity_type;
            $("#activity_name").val("");
            if(activity_type != MEDICAL_APPOINTMENT){
                $("#activity_patient").val("");
                $("#activity_patient_ts_autocomplete").val("");
                $("#activity_patient").parent().parent().hide();
            }else{
                $("#activity_patient").parent().parent().show();
            }


            if(activity_type == A_TYPE.surgery){
                $("#add_personal_support_div").show();
            }else{
                $("#add_personal_support_div").hide();
            }

        });
    })();
    //Add new Event
    $('body').on('click', '#addEvent', addActivity);
    //Calendar views
    $('body').on('click', '#fc-actions [data-view]', function(e){
        e.preventDefault();
        var dataView = $(this).attr('data-view');
        $('#fc-actions li').removeClass('active');
        $(this).parent().addClass('active');
        cId.fullCalendar('changeView', dataView);
    });

    $(".activity_status").hide();
    $(".fc-toolbar").css("background","#00BCD4");
    setLastWindowConf();


    // add event, change doctor to add activity
    $('#doctor_current').change(function(){
        resetPersonalAndOffices();
    });
});


function loadFullActivity(id){

    $.ajax({
        url:"/doctor/activities/"+id+".json",
        dataType:"json",
        success:function(calEvent){
            ACTIVITY_UPDATE = calEvent;
            ACTIVITY_ACTION = "update";
            if(ACTIVITY_UPDATE.owner_type == 'Assistant' && ACTIVITY_UPDATE.owner_id == assistant_id){
                IS_OWNER  = true;
            }else{
                IS_OWNER = false;
            }

            var can_edit = false;
            var can_delete = false;

            $.each(ACTIVITY_UPDATE.office_id,function(idx,o){
                if(assistantHasPermission(o,"update")){
                    can_edit = true;
                }
                if(assistantHasPermission(o,"delete")){
                    can_delete = true;
                }
            });

            if(!can_edit && !can_delete){
                swal("Hay un problema", "Usted no tiene permisos para modificar o eliminar esta actividad", "error");
                throw 'Usted no tiene permisos para realizar alguna acción';
            }

            if(can_edit){
                $("#update_activity_btn").show();
            }else{
                $("#update_activity_btn").hide();
            }

            if(can_delete){
                $("#delete_activity_btn").show();
            }else{
                $("#delete_activity_btn").hide();
            }

            if(ACTIVITY_UPDATE.activity_type_id == A_TYPE.appointment){
                $("#in_w_room_btn").show();
            }else{
                $("#in_w_room_btn").hide();
            }

            $("#activity_info").html(getActivityHtml());
            $("#activity_actions_modal").modal("show");
        }
    });
}

function resetPersonalAndOffices(){
    // filtrar consultorio
    var personal_supports = JSON.parse($('#personal_supports_assistant_shared').val());
    // filtrar oparticipantes
    var offices = JSON.parse($('#offices_doctors').val());
    var doctor_select = $('#doctor_current').val();
    $('#activity_office').html("");
    $('#personal_supports').html("");

    $.each(personal_supports, function(index,val){
        if(parseInt(val.id) == parseInt(doctor_select)){
            $.each(val.personalsupports, function(index,personal){
                $('#personal_supports').append($('<option>', {
                    value: personal.id,
                    text : personal.full_name
                }));
            });
        }
    });

    $.each(offices, function(index,val){
        if(parseInt(val.doctor_id) == parseInt(doctor_select)){
            $('#activity_office').append($('<option>', {
                value: val.id,
                text : val.name
            }));
        }
    });

    $("#personal_supports").val([]).selectpicker("refresh");
    $("#activity_office").val([]).selectpicker("refresh");

    // reset paciente
    $('#activity_patient').val("");
    $('#activity_patient_ts_autocomplete').val("");
}


function assistantHasPermission(office_id,action){
    //if (assistant_permissions[action].indexOf(office_id) > -1){
        return true;
    //}else{
    //    return false;
    //}
}


var inputsValidate = ["#activity_name",
    "#activity_start_date","#activity_end_date",
    "#activity_start_time","#activity_end_time"];

var ACTIVITY_UPDATE = {};
var ACTIVITY_ACTION = "";

function getDate(str1,str2){
    // str1 debe tener formato dd/MM/yyyy
    // str2 debe tener formato hh:mm a  formato 12 hrs
    var dt1   = parseInt(str1.substring(0,2));
    var mon1  = parseInt(str1.substring(3,5));
    var yr1   = parseInt(str1.substring(6,10));

    var hourComponents = str2.split(" ");
    var hour = hourComponents[0];
    var format = hourComponents[1];
    var hComp = hour.split(":");

    var h = parseInt(hComp[0]);
    var m = parseInt(hComp[1]);

    if(format == "PM" && h != 12){
        h += 12;
    }else if(format == "AM" && h == 12){
        h = 0;
    }
    return new Date(yr1, mon1-1, dt1,h,m,0);
}

function getDateValues(){
    var s_date = stringToDate(ACTIVITY_UPDATE.s_date,"yyyy-mm-dd hh:mn");
    var e_date = stringToDate(ACTIVITY_UPDATE.e_date,"yyyy-mm-dd hh:mn");
    var start_date = dateToString(s_date,"dd/mm/yyyy");
    var end_date = dateToString(e_date,"dd/mm/yyyy");
    var start_time = dateToString(s_date,"hh:mn pp");
    var end_time = dateToString(e_date,"hh:mn pp");
    return {
        start_date:start_date,
        end_date:end_date,
        start_time:start_time,
        end_time:end_time
    };
}

function resetFormAddActivity(){

    $(".event-tag .bgm-cyan").click();
    setTimeout(function(){
        $(".event-tag .bgm-cyan").addClass("selected");
        $(".option_status_1").show();
        $("#activity_status").selectpicker("refresh");
    },500);

    var doctor_id = $("#doctor_agenda_select").val();
    if(doctor_id != ""){
        $("#doctor_current").val(doctor_id).selectpicker('refresh');
    }

    $.each(inputsValidate,function(idx,name){
        $(name).closest('.form-group').removeClass('has-error');
        $(name).val("");
    });

    $("#activity_patient_ts_autocomplete").val("");
    $("#activity_details").val("");

    resetPersonalAndOffices();
    $("#personal_supports").val([]).selectpicker("refresh");

    $(".event-tag span").removeClass("selected");
    $("#office_id").val("");
    $("#all_day_activity").prop("checked",false);
    $("#activity_start_time").parent().show();
    $("#activity_end_time").parent().show();
    $("#activity_patient").val("");
    $("#activity_status").val("").selectpicker('refresh');

    $("#addEvent").html("Guardar");
    $("#addEventTitle").html("Agregar Actividad");

    $(".activity_status").hide();

    $(".activity_office").hide();
    $("#office_all").html("TODOS LOS CONSULTORIOS");
    $("#activity_office").prop("disabled",false);
    setupOfficeSelect();
}


function setupOfficeSelect(){
    $("#office_all").html("TODOS LOS CONSULTORIOS");
    if(assistant_permissions.create.length > 1){
        $("#office_all").show();
    }else if (assistant_permissions.create.length == 1){
        $("#office_all").hide();
        $("#activity_office").val(assistant_permissions.create[0]);
    }else if(assistant_permissions.create.length == 0){
        swal("Hay un problema","Usted no tiene permisos para crear actividades","error");
        throw 'No tiene permisos para crear actividades';
    }
    $.each(assistant_permissions.create,function(idx,o){
        $("#office_"+o).show();
    });

    $("#activity_office").selectpicker('refresh');
}

function setupUpdateModal(){
    ACTIVITY_ACTION = "update";
    $("#activity_actions_modal").modal("hide");
    var data = getDateValues();
    if(IS_OWNER){
        $("#office_all").html("TODOS LOS CONSULTORIOS");
        $("#activity_office").prop("disabled",false);
        if(ACTIVITY_UPDATE.office_id.length > 1){
            $("#activity_office").val("all");
        }else if(ACTIVITY_UPDATE.office_id.length == 1){
            $("#activity_office").val(ACTIVITY_UPDATE.office_id[0])
        }else{
            swal("Error!", "Error: Hay un problema para determinar el consultorio", "error");
        }
    }else{
        $("#office_all").html("NO PUEDE EDITAR ESTE DATO");
        $("#activity_office").val("all");
        $("#activity_office").prop("disabled",true);
    }

    $("#activity_office").selectpicker('refresh');

    $('#activity_name').val(ACTIVITY_UPDATE.title);
    $("#activity_details").val(ACTIVITY_UPDATE.details);
    var color = $('.event-tag > span.selected').attr('data-tag');
    $("#activity_start_date").val(data.start_date);
    $("#activity_end_date").val(data.end_date);
    $("#activity_start_time").val(data.start_time);
    $("#activity_end_time").val(data.end_time);

    if(parseInt(ACTIVITY_UPDATE.activity_type_id) == A_TYPE.surgery){
        $('#personal_supports').selectpicker('val', ACTIVITY_UPDATE.personal_supports);
        $('#add_personal_support_div').show();
    }else{
        $('#add_personal_support_div').hide();
    }



    $("#all_day_activity").prop("checked",ACTIVITY_UPDATE.allDay);
    if(ACTIVITY_UPDATE.allDay){
        $("#activity_start_time").parent().hide();
        $("#activity_end_time").parent().hide();
    }else{
        $("#activity_start_time").parent().show();
        $("#activity_end_time").parent().show();
    }
    $(".event-tag span").removeClass("selected");
    $(".event-tag ."+ACTIVITY_UPDATE.color).addClass('selected');

    if(ACTIVITY_UPDATE.activity_type_id == MEDICAL_APPOINTMENT){
        $("#activity_patient").val(ACTIVITY_UPDATE.patient_name);
        $("#activity_patient_ts_autocomplete").val(ACTIVITY_UPDATE.patient_id);
    }else{
        $("#activity_patient").val("");
        $("#activity_patient_ts_autocomplete").val("");
        $("#activity_patient").parent().parent().hide();
    }

    updateStatusSelect(ACTIVITY_UPDATE.activity_type_id);
    $("#activity_status").val(ACTIVITY_UPDATE.activity_status_id).selectpicker('refresh');

    $("#doctor_current").prop("disabled",true);
    $('#doctor_current').val(ACTIVITY_UPDATE.doctor_id).selectpicker('refresh');

    resetPersonalAndOffices();

    $("#addEvent").html("Actualizar");
    $("#addEventTitle").html("Editar Actividad");

    $('#addNew-event').modal('show');
}

function addActivity(){
    var validForm = true;
    $.each(inputsValidate,function(idx,name){
        if($(name).val() == ""){
            $(name).closest('.form-group').addClass('has-error');
            validForm = false;
        }
    });

    var activity_type = $('.event-tag > span.selected').attr('activity-type');
    var activity_status = $("#activity_status").val();

    if(!validForm){
        swal("Oops! Parece que hay campos sin llenar.");
        return;
    }
    if(activity_type == undefined){
        swal("Por favor seleccione un tipo de Actividad!");
        return;
    }
    if(activity_status == ""){
        swal("Por favor seleccione un estado para la Actividad!");
        return;
    }

    var start_date = $("#activity_start_date").val();
    var end_date  = $("#activity_end_date").val();
    var start_time = $("#activity_start_time").val();
    var end_time  = $("#activity_end_time").val();

    var startDate = getDate(start_date,start_time);
    var endDate = getDate(end_date,end_time);

    if(validate_rank_date_appointment(startDate,endDate,$('#doctor_current').val())){
        swal({
            title:'Horario ocupado ',
            text: "¿Esta seguro que desea tener mas de una actividad en el mismo horario?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Si, acepto',
            cancelButtonText: 'No, cerrar!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm){
            if (isConfirm) {
                var name = $('#activity_name').val();
                var details = $("#activity_details").val();
                var all_day = $("#all_day_activity").is(":checked");
                var office_id = $("#activity_office").val();
                var patient_id = $("#activity_patient_ts_autocomplete").val();

                if(office_id == "all"){
                    office_id = assistant_permissions.create;
                }

                var personal_selected = [];
                if(parseInt(activity_type) == A_TYPE.surgery){
                    personal_selected = $('#personal_supports').val();
                }

                var params = {
                    name:name,
                    details:details,
                    start_date: startDate,
                    end_date:endDate,
                    all_day:all_day,
                    activity_type_id:activity_type,
                    activity_status_id:activity_status,
                    personal_supports: personal_selected
                };

                var method = "POST";
                var url = "/doctor/activities/";

                if(ACTIVITY_ACTION == "add"){
                    ACTIVITY_UPDATE.id = 0;
                }else{
                    method = "PUT";
                    url += ACTIVITY_UPDATE.id;
                    $(calendar_assistant_id).fullCalendar('removeEvents', ACTIVITY_UPDATE.id);
                }
                $.ajax({
                    url:url,
                    dataType:"json",
                    method:method,
                    data:{
                        activity:params,
                        office_id:office_id,
                        patient_id:patient_id,
                        is_owner:IS_OWNER,
                        doctor_id: $('#doctor_current').val()
                    },
                    success:function(data){
                    }
                });
                resetFormAddActivity();
                $('#addNew-event').modal('hide');
            }
        });

    }else{
        var name = $('#activity_name').val();
        var details = $("#activity_details").val();
        var all_day = $("#all_day_activity").is(":checked");
        var office_id = $("#activity_office").val();
        var patient_id = $("#activity_patient_ts_autocomplete").val();

        if(office_id == "all"){
            office_id = assistant_permissions.create;
        }

        var personal_selected = [];
        if(parseInt(activity_type) == A_TYPE.surgery){
            personal_selected = $('#personal_supports').val();
        }

        var params = {
            name:name,
            details:details,
            start_date: startDate,
            end_date:endDate,
            all_day:all_day,
            activity_type_id:activity_type,
            activity_status_id:activity_status,
            personal_supports: personal_selected
        };

        var method = "POST";
        var url = "/doctor/activities/";

        if(ACTIVITY_ACTION == "add"){
            ACTIVITY_UPDATE.id = 0;
        }else{
            method = "PUT";
            url += ACTIVITY_UPDATE.id;
            $(calendar_assistant_id).fullCalendar('removeEvents', ACTIVITY_UPDATE.id);
        }
        $.ajax({
            url:url,
            dataType:"json",
            method:method,
            data:{
                activity:params,
                office_id:office_id,
                patient_id:patient_id,
                is_owner:IS_OWNER,
                doctor_id: $('#doctor_current').val()
            },
            success:function(data){
            }
        });
        resetFormAddActivity();
        $('#addNew-event').modal('hide');
    }


}

function validate_rank_date_appointment(startDate,endDate,doctor_id){
    var url = $('#url_validate_date').val();

    var aux1 = new Date(startDate.getTime());
    var aux2 = new Date(endDate.getTime());


    var now_date = new Date();
    var datenohour = new Date().setHours(0,0,0,0);
    var startnohour = aux1.setHours(0,0,0,0);
    var endnohour = aux2.setHours(0,0,0,0);
    now_date.setMinutes(now_date.getMinutes() - 20);


    if(ACTIVITY_ACTION == "add") {
        if (datenohour > startnohour) {
            swal("No se pueden agregar actividades con fechas anteriores a la actual");
            return true;
        }
        else {
            if (now_date.getTime() > startDate.getTime()) {
                swal("No se pueden agregar actividades con una hora anterior a la actual");
                return true;
            }
        }
    }
    if(endnohour<startnohour){
        swal("La fecha final no puede ser menor a la actual");
        return true;
    }
    else {
        if (endDate.getTime() < startDate.getTime()) {
            swal("La hora final no puede ser menor a la actual");
            return true;
        }

    }

    var exist = false;
    if(ACTIVITY_ACTION == "add"){
        ACTIVITY_UPDATE.id = 0;
    }
    $.ajax({
        url:url,
        dataType:"json",
        method:"get",
        async: false,
        cache: false,
        data:{
            start_date:startDate, end_date:endDate,activity_id:ACTIVITY_UPDATE.id,doctor_id:doctor_id},
        success:function(act){
            exist = act.result;
            if(exist){
                swal(act.message+" Actividad: "+act.activity.name);
            }
        }
    });

    return exist;
}

function deleteActivity(){
    $("#activity_actions_modal").modal("hide");
    var method = "DELETE";
    var url = "/doctor/activities/";
    url += ACTIVITY_UPDATE.id;
    $.ajax({
        url:url,
        dataType:"json",
        method:method,
        data:{id:ACTIVITY_UPDATE.id, doctor_id: $('#doctor_current').val()},
        success:function(act){
            loadActivitiesWaitingRoom();
        }
    });
}

function loadActivities(){
    loaded_activities = true;
    $(calendar_assistant_id).fullCalendar( 'removeEvents',function(){
        return true;
    });
    var date = $(calendar_assistant_id).fullCalendar('getDate');
    date = date._d;
    var month_int = date.getMonth();
    var year_int = date.getFullYear();

    if(assistant_permissions.read.length > 0){
        $.ajax({
            url:"/asistente/index/activities",
            method:"GET",
            dataType:"json",
            data:{
                year:year_int,
                month:month_int,
                office_id:assistant_permissions.read,
                doctor_id:$("#doctor_agenda_select").val(),
                is_agenda:true
            },
            success:function(activities){
                $(calendar_assistant_id).fullCalendar('addEventSource', activities);
            }
        });
    }
}

function updateStatusSelect(activity_type){
    $(".activity_status").hide();
    $(".option_status_"+activity_type).show();
    $("#activity_status").val("").selectpicker('refresh');
}

function getActivityHtml(){
    var html_activity = "";
    html_activity += "<div class='row'>";
    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Tipo</strong><br>";
    html_activity += ACTIVITY_UPDATE.activity_type_name+"<br><br>";
    html_activity += "</div>";

    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Consultorio</strong><br>";
    if(ACTIVITY_UPDATE.office_id.length == 1){
        html_activity += ACTIVITY_UPDATE.first_office_name+"<br><br>";
    }else{
        html_activity += "Todos los consultorios<br><br>";
    }
    html_activity += "</div>";
    html_activity += "</div>";
    html_activity += "<div class='row'>";
    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Actividad</strong><br>";
    html_activity += ACTIVITY_UPDATE.title+"<br><br>";
    html_activity += "</div>";

    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Detalles</strong><br>";
    html_activity += ACTIVITY_UPDATE.details+"<br><br>";
    html_activity += "</div>";
    html_activity += "</div>";

    html_activity += "<div class='row'>";
    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Fecha de Inicio</strong><br>";
    if(ACTIVITY_UPDATE.allDay){
        html_activity += dateToString(stringToDate(ACTIVITY_UPDATE.s_date,"yyyy-mm-dd hh:mn"),"dd/fm/yyyy");
    }else{
        html_activity += dateToString(stringToDate(ACTIVITY_UPDATE.s_date,"yyyy-mm-dd hh:mn"),"dd/fm/yyyy hh:mn pp");
    }

    html_activity += "<br><br>";
    html_activity += "</div>";

    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Fecha de Finalización</strong><br>";
    if(ACTIVITY_UPDATE.allDay){
        html_activity += dateToString(stringToDate(ACTIVITY_UPDATE.e_date,"yyyy-mm-dd hh:mn"),"dd/fm/yyyy");
    }else{
        html_activity += dateToString(stringToDate(ACTIVITY_UPDATE.e_date,"yyyy-mm-dd hh:mn"),"dd/fm/yyyy hh:mn pp");
    }

    html_activity += "<br><br>";
    html_activity += "</div>";


    if(ACTIVITY_UPDATE.activity_type_id == A_TYPE.appointment && ACTIVITY_UPDATE.patient_name != "" ){
        html_activity += "<div class='col-md-6'>";
        html_activity += "<strong>Paciente</strong><br>"+ACTIVITY_UPDATE.patient_name;
        html_activity += "</div>";
    }

    html_activity += "<div class='col-md-6'>";
    html_activity += "<strong>Estado</strong><br>";
    html_activity += ACTIVITY_UPDATE.activity_status_name;
    html_activity += "</div>";

    html_activity += "</div>";// end of class row

    return html_activity;
}

function setPatientInWroom(){
    var params = {
        doctor_id: ACTIVITY_UPDATE.doctor_id,
        activity_id: ACTIVITY_UPDATE.id,
        activity_status_id : act_status.waiting
    };
    $("#activity_actions_modal").modal("hide");
    $.ajax({
        url:"/doctor/activities/update_status",
        data:params,
        dataType:"json"
    });
}

function setAppointmentAsAttended(activity_id){
    var params = {
        activity_id: activity_id,
        activity_status_id : act_status.attended
    };
    $.ajax({
        url:"/doctor/activities/update_status",
        data:params,
        dataType:"json"
    });
}

function getPopoverHtml(event){

    var date_start = stringToDate(event.s_date,"yyyy-mm-dd hh:mn");
    var date_end = stringToDate(event.e_date,"yyyy-mm-dd hh:mn");
    var ends_same_day = true;

    var str_sd = date_start.getDate()+""+date_start.getMonth()+""+date_start.getFullYear();
    var str_ed = date_end.getDate()+""+date_end.getMonth()+""+date_end.getFullYear();

    if(str_sd != str_ed){
        ends_same_day = false;
    }

    var str_data = "<div class='popover_evt'>";

    var start_date = dateToString(date_start,"dd/fm/yyyy");
    var start_hour = dateToString(date_start,"hh:mn pp");
    var end_date = dateToString(date_end,"dd/fm/yyyy");
    var end_hour = dateToString(date_end,"hh:mn pp");

    str_data += start_date;

    if(ends_same_day){
        str_data += "<br>";
    }else{
        str_data += " ";
    }

    str_data += start_hour;

    if(ends_same_day){
        str_data += " - ";
    }else{
        str_data += "<br>";
        str_data += end_date+" ";
    }

    str_data += end_hour;

    if(event.activity_type_id == A_TYPE.appointment && event.patient_name != "" ){
        str_data += "<br>Paciente: "+event.patient_name+"";
    }

    str_data += "<br>Consultorio: "+event.office_name;
    str_data += "</div>";

    return str_data;
}