
// var RELATIVE_DASHBOARD_URL = '/doctor/dashboard/assistant';
// var CURRENT_URL = window.location.href;
var APPLICATION_AGE_ID = 0;
function is_dashboard(){
    return $('#addNew-event').length
}


jQuery(document).ready(function() {
    var grid = $("#data-table-vaccines-books").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-vaccines-books').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title=\"Mostrar\"><i class=\"zmdi zmdi-eye\"></i></a>";
            }
        }
    })

});

function addVacineBookPatient(){
    $("#addbookpatient").modal("show");
}
function saveVaccioneBook(){
    var url = $('#url_vaccine_book_patient').val();
    var method = "GET";
    var params = {
        patient_id: $("#vaccinebook-patient_ts_autocomplete").val(),
        vaccine_book_id: $("#vaccine_book_select").val()
    };

    $.ajax({
       url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(){
            $("#addbookpatient").modal("hide");
            location.reload();
        },
        error:function(){

        }
    });

}

function setLastWindowConf(){

    if(!is_dashboard()){
        return;
    }

    var date = new Date();
    $("#calendar_a_min").html(dateToString(date,"dd/fm/yyyy"));
    $("#calendar_a_min").hide();

    var cal_s = $.session.get(cal_session_key);
    if(cal_s == cal_sizes.min){
        minCalendar(); // if calendar is hidden activities are not displayed
    }else if(cal_s == cal_sizes.max){
        loadActivities();
        maxCalendar();
    }else{
        loadActivities();
        resCalendar();
    }

    var w_room_s = $.session.get(waiting_room_session_key);
    if(w_room_s == waiting_room_sizes.min){
        minWaitingRoom();
    }else if(w_room_s == waiting_room_sizes.max){
        maxWaitingRoom();
    }else{
        resWaitingRoom();
    }

    var consults_s = $.session.get(med_consult_session_key);
    if(consults_s == med_consult_sizes.min){
        minConsultations();
    }else if(consults_s == med_consult_sizes.max){
        maxConsultations();
    }else{
        resConsultations();
    }
}
function setModalVaccination(application_id){
    APPLICATION_AGE_ID = application_id;
}
function setVaccinateDate(){
    var url = $("#url_add_vaccine_date").val();
    var method = "GET";

    var params = {
        patient_id:$("#patient_id").val(),
        vaccinate_date:$("#vaccinate_date_picked").val(),
        application_age_id:APPLICATION_AGE_ID,
    };

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(response){
            location.reload();
        }
    });
}