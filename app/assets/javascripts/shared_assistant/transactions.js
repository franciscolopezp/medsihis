var FISCAL_AMOUNT = 0;
var subtotal = 0;
var IVA = 0;
var ISR = 10;
var iva_doc = 0;
var iva_report;
var iva_report_included;
var importe_iva = 0;
var iva_for_report = 0;
var row_counter = 1;
var new_concept = 1;
var importe_isr = 0;
var tipo_persona;
var MED_CONSULT_ID;
var datos_emisor = "";
var PATIENT_NAME = ""
var concepts_open_invoice = "";
var PAYMENT_METHODS = "";
var PAYMENT_METHODS_TEXT = "";
var PAYMENT_METHODS_NAME = "";
var TOINVOICETYPE = "";
jQuery(document).ready(function() {
    loadFiscalInfoDoctor();
    var url_get_patient_fiscal = $('#url_get_patient_fiscal').val();
    var url = $('#url_get_doctor_patients').val();
    $('#type_to_invoicee').change(function(){
        var fecha_inicio = $('#start_date').val();
        var fecha_fin = $('#end_date').val();
        if(fecha_inicio != "" || fecha_fin != ""){
            search_invoices();
        }
    });
    $('#type_to_invoice').change(function(){
        reload();
    });
    $("#set_data_patient_fiscal").click(function(e)
    {
        var optionselectedopen = $('#fiscal_for_search').find('option:selected');
        var optiontypeopen = optionselectedopen.attr("type");
        $.ajax({
            url: $("#url_get_patient_fiscal_to_set").val(),
            dataType: "json",
            data: {fiscal_id:$('#fiscal_for_search').val(),type:optiontypeopen},
            success: function (data) {
                $.each(data.fiscals, function (idx, info) {
                    $('#fiscal_business_name_open').val(info.business_name);
                    $('#fiscal_rfc_open').val(info.rfc);
                    $('#fiscal_address_open').val(info.address);
                    $('#fiscal_locality_open').val(info.locality);
                    $('#fiscal_ext_number_open').val(info.ext_number);
                    $('#fiscal_int_number_open').val(info.int_number);
                    $('#fiscal_zip_open').val(info.zip)
                    $('#fiscal_city_open').val(info.city);
                    $('#fiscal_state_open').val(info.state);
                    $('#fiscal_country_open').val(info.country);
                    $('#fiscal_suburb_open').val(info.suburb);
                });
                $("#add_patient_data_modal").modal("hide");
            },
            error:function(data){
                console.log("error controller");
            }
        });

    });
    if($('#doctor_for_search').val() != ""){
        var patients_option =  "<option value = ''>TODO</option>";
        $.ajax({
            url: url,
            dataType: "json",
            data: {doctor_id:$('#doctor_for_search').val()},
            success: function (data) {
                $.each(data.patients, function (idx, info) {
                    patients_option += "<option value = "+info.id+">"+info.name+"</option>";
                });
                $('#patient_for_search').html(patients_option);
                $('#patient_for_search').selectpicker('refresh');

            },
            error:function(data){
            }
        });
    }else{
        $('#patient_for_search').html("");
        $('#patient_for_search').selectpicker('refresh');
    }
    var fiscal_general_option;
    $.ajax({
        url: url_get_patient_fiscal,
        dataType: "json",
        data: {patient_id:$('#patient_data_search').val(),doctor_id:$("#doctor_for_search_open").val()},
        success: function (data) {
            $.each(data.fiscals, function (idx, info) {
                fiscal_general_option += "<option value = "+info.id+" type = "+info.type+">"+info.business_name+"</option>";
            });
            $('#fiscal_for_search').html(fiscal_general_option);
            $('#fiscal_for_search').selectpicker('refresh');
        },
        error:function(data){
            console.log("error controller");
        }
    });
    $('#patient_data_search').change(function(){
        setTimeout(function(){
                var fiscals_option =  "";
                $.ajax({
                    url: url_get_patient_fiscal,
                    dataType: "json",
                    data: {patient_id:$('#patient_data_search').val(),doctor_id:$("#doctor_for_search_open").val()},
                    success: function (data) {
                        $.each(data.fiscals, function (idx, info) {
                            fiscals_option += "<option value = "+info.id+" type = "+info.type+">"+info.business_name+"</option>";
                        });
                        $('#fiscal_for_search').html(fiscals_option);
                        $('#fiscal_for_search').selectpicker('refresh');
                    },
                    error:function(data){
                        console.log("error controller");
                    }
                });


        },1000);
    });
    $('#doctor_for_search_general').change(function(){
        $('#start_date').val("");
        $('#end_date').val("");
        loadFiscalInfoDoctor();
        var tabla_consultas = "<table id='invoices_general_table' class='table table-striped'><thead><tr>"+
            "<th>Id</th>"+
            "<th>Paciente</th>"+
            "<th>Fecha consulta</th>"+
            "<th>Costo</th>"+
            "<th></th>"+
            "</tr></thead><tbody>";

                tabla_consultas += "</tbody></table>";
                $('#general_invoices_table').html("");
    });
    $('#doctor_for_search').change(function(){


        setTimeout(function(){

            if($('#doctor_for_search').val() != ""){
                var patients_option =  "<option value = ''>TODO</option>";
                $.ajax({
                    url: url,
                    dataType: "json",
                    data: {doctor_id:$('#doctor_for_search').val()},
                    success: function (data) {
                        $.each(data.patients, function (idx, info) {
                            patients_option += "<option value = "+info.id+">"+info.name+"</option>";
                        });
                        $('#patient_for_search').html(patients_option);
                        $('#patient_for_search').selectpicker('refresh');
                        search_consults();

                    },
                    error:function(data){
                    }
                });
            }else{
                $('#patient_for_search').html("");
                $('#patient_for_search').selectpicker('refresh');
            }

        },1000);
    });
    if ($('#isOpenInvoice').val() == "1"){
        datos_emisor ="";
        var f = new Date();
        fecha_actual = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
        $.ajax({
            url: $('#url_get_docinfo_action').val(),
            dataType: "json",
            Async:false,
            data: {doctor_id: $('#doctor_for_search_open').val()},
            success: function (data) {
                $.each(data, function (idx, info) {
                    iva_doc = info.iva;
                    folio_siguiente =  parseInt(info.folio) + 1;
                    datos_emisor +=  info.business_name.replace(",,"," ") +  "<br>";
                    datos_emisor += info.rfc + "<br>";
                    datos_emisor += info.address+ " No. Int: " + info.int_number + "  No. Ext: " + info.ext_number + "  Col. " + info.suburb + "<br>";
                    datos_emisor += info.locality + ", " + info.city + ", " + info.state + ", " + info.country + ". C.P. " + info.zip +"<br>";
                    datos_emisor += "<strong>Serie:</strong> " + info.serie + "&nbsp;&nbsp;&nbsp;&nbsp; <strong>Folio:</strong> " +folio_siguiente;
                    datos_emisor += "<br>Fecha: "+dateToString(new Date(), "dd fm yyyy")+ "\t \t \t ";
                    $('#fiscal_emisor_open').html(datos_emisor);
                    $('#ge_iva').val(info.iva);
                });
            }
        });
    }
    $('#doctor_for_search_open').change(function(){
        datos_emisor ="";
        var f = new Date();
        fecha_actual = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
        $.ajax({
            url: $('#url_get_docinfo_action').val(),
            dataType: "json",
            Async:false,
            data: {doctor_id: $('#doctor_for_search_open').val()},
            success: function (data) {
                $.each(data, function (idx, info) {
                    iva_doc = info.iva;
                    iva_report_included = info.iva_report_included;
                    iva_report = info.iva_report;
                    iva_for_report = info.iva;
                    folio_siguiente =  parseInt(info.folio) + 1;
                    datos_emisor +=  info.business_name.replace(",,"," ") +  "<br>";
                    datos_emisor += info.rfc + "<br>";
                    datos_emisor += info.address+ " No. Int: " + info.int_number + "  No. Ext: " + info.ext_number + "  Col. " + info.suburb + "<br>";
                    datos_emisor += info.locality + ", " + info.city + ", " + info.state + ", " + info.country + ". C.P. " + info.zip +"<br>";
                    datos_emisor += "<strong>Serie:</strong> " + info.serie + "&nbsp;&nbsp;&nbsp;&nbsp; <strong>Folio:</strong> " +folio_siguiente;
                    datos_emisor += "<br>Fecha: "+dateToString(new Date(), "dd fm yyyy")+ "\t \t \t ";
                    $('#fiscal_emisor_open').html(datos_emisor);
                    $('#ge_iva').val(info.iva);
                });
            }
        });
        var fiscal_general_option;
        $.ajax({
            url: url_get_patient_fiscal,
            dataType: "json",
            data: {patient_id:$('#patient_data_search').val(),doctor_id:$("#doctor_for_search_open").val()},
            success: function (data) {
                $.each(data.fiscals, function (idx, info) {
                    fiscal_general_option += "<option value = "+info.id+" type = "+info.type+">"+info.business_name+"</option>";
                });
                $('#fiscal_for_search').html(fiscal_general_option);
                $('#fiscal_for_search').selectpicker('refresh');
            },
            error:function(data){
                console.log("error controller");
            }
        });
    });
    var html = '<div class="col-md-offset-3 col-md-9">'+
        '<div class="preloader pl-xxl">'+
        '<svg class="pl-circular" viewBox="25 25 50 50">'+
        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
        '</svg>'+
        'Generando...'+
        '</div>'+
        '</div>';


    $('#loading_image').append(html);
    $('#ge_loading_image').append(html);
    $('#loading_image_open').append(html);
    $('#cancel_invoice_loading').append(html)

    $('#loading_image').hide();
    $('#ge_loading_image').hide();
    $('#loading_image_open').hide();
    $('#cancel_invoice_loading').hide();




    var grid = $("#data-table-bills").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function()
        {

            var patient = $('#patient_for_search').val();
            if(patient == ""){
                patient = "all"
            }
            var doctor = $('#doctor_for_search').val();
            if(doctor == ""){
                doctor = "all"
            }
            return $('#data-table-bills').data('source')+"?patient="+patient+"&doctor_id="+doctor+"&type="+$('#type_to_invoice').val();;
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        templates: {
            search: ""
        },
        formatters: {
            "commands": function(column, row) {
                if(row.invoiced){
                    return "<a href="+row.print+" target='_blank' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-file-text zmdi-hc-fw\" title=\"PDF\"></i></a>"+
                           "<a href="+row.download_xml+" target='_blank' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-code zmdi-hc-fw\" title=\"XML\"></i></a>"+
                        "<a href='#' onclick='prepareInvoiceEmail("+row.id+","+row.folio+")'  class=\"btn bgm-green btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-email zmdi-hc-fw\" title=\"Enviar correo\"></i></a>"+
                        "<a href='#' onclick='cancelInvoice("+row.id+","+row.folio+")'  class=\"btn bgm-red btn-icon waves-effect waves-circle waves-float btn-show\"  ><i class=\"zmdi zmdi-close zmdi-hc-fw\" title=\"Cancelar factura\"></i></a>"
                }
                else{
                    return "<a href='#' onclick='openCreateInvoiceModal("+row.id+",\""+row.amount+"\","+row.patient_id+",\""+row.type+"\",\""+row.patient_name+"\")' class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-mail-send zmdi-hc-fw\" title=\"Factura\"></i></a>";
                }

            }
        }
    })


});
function search_consults(){
    $("#data-table-bills").bootgrid("reload");
}
function reload(){
    $("#data-table-bills").bootgrid("reload");
}
function cancelSendmeail(){
    reload();
}
function send_invoice_email(){
    if($('#email_receptor').val()!="" || $("#emailcheck_0").is(':checked') || $("#emailcheck_1").is(':checked') || $("#emailcheck_2").is(':checked')){

    }
    else{
        swal("Error!", "Debe ingresar o seleccionar al menos un correo electronico.", "error");
        return;
    }
    if($('#email_receptor').val()!="" ){
        if (!validarEmail($('#email_receptor').val())){
            swal("Error!", "Ingrese una direccion de correo valida.", "error");
            return;
        }
    }
    var extramails = "";
    $("#text_emails_div :text").each(function(){
        if (!validarEmail($(this).val())){
            swal("Error!", "Algun correo no es una direccion de correo valida.", "error");
            return;
        }else{
            extramails += $(this).val() + ";";
        }


    });
    var emailp = "";
    var emailf1 = "";
    var emailf2 = "";
    if ($("#emailcheck_0").is(':checked')){
        emailp = $('#labele_0').text();
    }
    if ($("#emailcheck_1").is(':checked')){
        emailf1 = $('#labele_1').text();
    }
    if ($("#emailcheck_2").is(':checked')){
        emailf2 = $('#labele_2').text();
    }
    $.ajax({
        url: $('#url_send_invoice_mail').val(),
        dataType: "json",
        data: {
            email:$('#email_receptor').val(),
            emailp:emailp,
            emailf1:emailf1,
            emailf2:emailf2,
            extramails:extramails,
            invoice_id: $('#invoice_id').val()},
        success: function (data) {
            swal("Exito", "La factura ha sido enviada con exito.", "success");
            reload();
        },
        error:function(data){
            swal("Error!", "Ocurrio un error al intentar enviar el correo electronico, por favor intente mas tarde.", "error");
        }
    });
    $('#send_email_modal').modal('hide');
}

function loadFiscalInfoDoctor(){
    datos_emisor ="";
    var f = new Date();
    docc_id="";
    if($('#doctor_for_search_general').val() != "" && $('#doctor_for_search_general').val() != undefined){
        docc_id = $('#doctor_for_search_general').val();
    }else{
        docc_id = $('#doctor_for_search').val();
    }
    fecha_actual = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
    $.ajax({
        url: $('#url_get_docinfo_action').val(),
        dataType: "json",
        Async:false,
        data: {doctor_id: docc_id},
        success: function (data) {
            $.each(data, function (idx, info) {
                iva_doc = info.iva;
                iva_report_included = info.iva_report_included;
                iva_report = info.iva_report;
                iva_for_report = info.iva;
                folio_siguiente =  parseInt(info.folio) + 1;
                datos_emisor +=  info.business_name.replace(",,"," ") +  "<br>";
                datos_emisor += info.rfc + "<br>";
                datos_emisor += info.address+ " No. Int: " + info.int_number + "  No. Ext: " + info.ext_number + "  Col. " + info.suburb + "<br>";
                datos_emisor += info.locality + ", " + info.city + ", " + info.state + ", " + info.country + ". C.P. " + info.zip +"<br>";
                datos_emisor += "<strong>Serie:</strong> " + info.serie + "&nbsp;&nbsp;&nbsp;&nbsp; <strong>Folio:</strong> " +folio_siguiente;
                datos_emisor += "<br>Fecha: "+dateToString(new Date(), "dd fm yyyy")+ "\t \t \t ";
                $('#ge_fiscal_emisor').html(datos_emisor);
                $('#fiscal_emisor_info').html(datos_emisor);
                $('#fiscal_emisor_open').val(datos_emisor);
                $('#ge_iva').val(info.iva);
            });
        }
    });
}
function openCreateInvoiceModal(consultation_id,amount,patient_id,type,patient_name){
    PATIENT_NAME = patient_name;
    $("#payment_method").val($("#payment_method option:first").val()).selectpicker("refresh");;
    clearPaymentMethodsSelectec();
    $('#ge_fiscal_emisor').val("");
    $('#fiscal_emisor').val("");
    $('#fiscal_emisor_open').val("");
    TOINVOICETYPE = type;
    loadFiscalInfoDoctor();
    $('#account_number').val("");
    $('#payment_methods_box').val("");
    subtotal = parseFloat(amount.replace("$",""));
    FISCAL_AMOUNT = 0;
    importe_isr = 0;
    importe_iva = 0;
    MED_CONSULT_ID = consultation_id;
    $('#create_invoice_modal').modal('show');
    loadFiscalInformations(patient_id);
}
function clearPaymentMethodsSelectec(){
    PAYMENT_METHODS = "";
    PAYMENT_METHODS_TEXT = "";
    PAYMENT_METHODS_NAME = "";
}
function setFiscalInfo(){
    var datos_receptor = "";

    var nombre_cliente = "";

    var f = new Date();
    fecha_actual = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
    var tabla_emisor = "<table id='emisor_data' class='table table-condensed' ><thead>"+
        "</thead><tbody><tr>";


    var optionselected = $('#fiscal_info_for_invoice').find('option:selected');
    var optiontype = optionselected.attr("type");
    $.ajax({
        url: $('#url_set_fiscal_info').val(),
        dataType: "json",
        Async:false,
        data: {fiscal_info_id: $('#fiscal_info_for_invoice').val(),type:optiontype},
        success: function (data) {
            $('#fiscal_amount').val(FISCAL_AMOUNT);
            $.each(data, function (idx, info) {
                $('#fiscal_id_patient').val(info.id);
                $('#fiscal_business_name').val(info.business_name.replace(",,"," "));
                $('#fiscal_rfc').val(info.rfc);
                $('#fiscal_address').val(info.address);
                $('#fiscal_locality').val(info.locality);
                $('#fiscal_suburb').val(info.suburb);
                $('#fiscal_ext_number').val(info.ext_number);
                $('#fiscal_int_number').val(info.int_number);
                $('#fiscal_zip').val(info.zip);
                $('#fiscal_city').val(info.city);
                $('#fiscal_state').val(info.state);
                $('#fiscal_country').val(info.country);
                tipo_persona = info.person_type;
                datos_receptor += "<strong>Razón Social:</strong> " + info.business_name.replace(",,"," ") + "<br><strong>RFC:</strong> "  + info.rfc;
                datos_receptor += "<br><strong>Dirección: </strong>" + info.address+ " Núm. " + info.ext_number + " Int. " + info.int_number + " Col. " + info.suburb+". ";
                datos_receptor += info.city + ",\t" + info.state + ",\t" + info.country+ ". Loc: " + info.locality  +" C.P: " + info.zip;
                $('#fiscal_receptor_info').html(datos_receptor);
                nombre_cliente = info.business_name.replace(",,"," ");



            });
            FISCAL_AMOUNT = 0;
            importe_isr = 0;
            importe_iva = 0;
            importe_iva = subtotal * (IVA/100);
            if(TOINVOICETYPE == "report") {

                if (iva_report) {
                    if (iva_report_included) {
                        importe_iva = subtotal * (iva_for_report / 100);
                        subtotal = subtotal - importe_iva;
                    } else {
                        importe_iva = subtotal * (iva_for_report / 100);
                    }
                    IVA = iva_for_report;
                } else {
                    importe_iva = subtotal * (IVA / 100);
                }
            }
            if(tipo_persona=="Moral"){
                importe_isr = subtotal * (ISR/100);
            }else{
                ISR = 0;
            }
            FISCAL_AMOUNT = subtotal - importe_isr + importe_iva;
            var tabla_facturacion = "<table  class='table'><thead><tr><th>Cantidad</th><th>Descripción</th>"+
                "<th>U.M</th><th>P.U</th><th>Importe</th></tr></thead><tbody>" +
                "<tr><td class='text-left'>1</td><td class='text-left'><div id='invoice_description' contenteditable>Honorarios médicos. Paciente: "+PATIENT_NAME+"</div></td><td class='text-left'>No aplica</td><td class='text-left'>$ "+subtotal.toFixed(2)+"</td><td class='text-left'>$ "+subtotal.toFixed(2)+"</td></tr>"+
                "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>Subtotal:</td><td class='text-left'>$"+subtotal.toFixed(2)+"</td></tr>"+
                "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>I.V.A</td><td class='text-left'>$ "+importe_iva.toFixed(2)+"</td></tr>"+
                "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>Ret I.S.R:</td><td class='text-left'>$ "+importe_isr.toFixed(2)+"</td></tr>"+
                "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>Total:</td><td class='text-left'>$"+FISCAL_AMOUNT.toFixed(2)+"</td></tr>"+
                "</tbody></table>";
            $('#table_invoice').html(tabla_facturacion);
        }
    });



}
function stamp_invoice(){
    var accountnumber = "No aplica";
    //if($('#payment_method').val()=="Efectivo" || $('#payment_method').val()=="No identificado"){
    if(PAYMENT_METHODS==""){
        swal("Error!", "Debe seleccionar al menos un metodo de pago.", "error");
        return;
    }
    if(PAYMENT_METHODS.search("02")>=0 || PAYMENT_METHODS.search("03")>=0 || PAYMENT_METHODS.search("04")>=0 || PAYMENT_METHODS.search("05")>=0
            || PAYMENT_METHODS.search("06")>=0 || PAYMENT_METHODS.search("08")>=0 || PAYMENT_METHODS.search("28")>=0 || PAYMENT_METHODS.search("29")>=0){

            if ($('#account_number').val()=="") {
                swal("Error!", "Para el metodo de pago es necesario ingresar un numero de cuenta.", "error");
                return;
            }else{
                accountnumber = $('#account_number').val();
            }
    }
    int_number_pa = "-";
    ext_number_pa = "-";
    if($('#fiscal_int_number').val() != ""){
        int_number_pa = $('#fiscal_int_number').val();
    }
    if($('#fiscal_ext_number').val() != ""){
        ext_number_pa = $('#fiscal_ext_number').val();
    }
    $.ajax({
        url: $('#url_stamp_action').val(),
        dataType: "json",
        beforeSend: function(){
            $('#loading_image').show();
        },
        complete: function(){
            console.log("complete");
        },
        data: {
            doctor_id: $('#doctor_for_search').val(),
            amount:FISCAL_AMOUNT.toFixed(2),
            business_name:$('#fiscal_business_name').val(),
            rfc:$('#fiscal_rfc').val().toUpperCase(),
            address:$('#fiscal_address').val(),
            locality:$('#fiscal_locality').val(),
            ext_number:ext_number_pa,
            int_number:int_number_pa,
            zip:$('#fiscal_zip').val(),
            city:$('#fiscal_city').val(),
            state:$('#fiscal_state').val(),
            country:$('#fiscal_country').val(),
            payment_type:$('#payment_type').val(),
            payment_method:PAYMENT_METHODS.substring(0,PAYMENT_METHODS.length-1),
            payment_method_name:PAYMENT_METHODS_NAME.substring(0,PAYMENT_METHODS_NAME.length-1),
            suburb:$('#fiscal_suburb').val(),
            account_number:accountnumber,
            sub_total:subtotal,
            patient_fiscal: $('#fiscal_id_patient').val(),
            iva:IVA,
            isr:ISR,
            importeiva:importe_iva.toFixed(2),
            importeisr:importe_isr.toFixed(2),
            transaction_id:MED_CONSULT_ID,
            descripcion_concepto:$("#invoice_description").html(),
            type:TOINVOICETYPE
            },
        success: function (data) {
            $("#create_invoice_modal").modal("hide");
            $('#loading_image').hide();
            reload();
            swal({
                title:'Factura Emitida',
                text: "Se ha emitido la factura de manera correcta, ahora puede enviarla por correo electrónico al paciente.",
                type: "success",
                showCancelButton: true,
                confirmButtonText: 'Si, enviar',
                cancelButtonText: 'No, cerrar!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm){
                if (isConfirm) {
                    prepareInvoiceEmail(data.invoice);
                    swal.close();
                }
                else {
                    swal.close();
                }
            });
        },
        error:function(data){
            $('#loading_image').hide();
            swal("Error!", data.responseText, "error");
        }
    });
}
function deleteemailfield(number){
    $("#newemaildiv_"+number).remove();
}
function addemailfield(){
    var d = new Date();
    var uniqnumber = d.valueOf();
    $("#text_emails_div").append('<div class="col-xs-12" id="newemaildiv_'+uniqnumber+'"><strong >Correo electronico:</strong> <br><input  type="text" class="form-control input-lg" id="email_receptor_added" >' +
        '<a  href="javascript:void(0);" onclick="deleteemailfield('+uniqnumber+')"><i class="zmdi zmdi-delete" style="font-size: 1.33em;"></i></a></div>');
}
function prepareInvoiceEmail(invoice_id){
    $('#email_receptor').val("");
    $("#emails_div").html("");
    $("#text_emails_div").html("");
    emailsset = '';
    $('#invoice_id').val(invoice_id);
    $.ajax({
        url: $('#url_get_receptor').val(),
        dataType: "json",
        data: {invoice_id:invoice_id},
        success: function (data) {
            $.each(data.emails, function(idx,info){
                emailsset += '<div class = "col-xs-12">';
                if(info.number == 0){
                    emailsset +=  '<h5>Correo del paciente</h5>';
                    emailsset +=  '<div class="checkbox">'+
                        '<label id="labele_'+info.number+'">'+
                        '<input type="checkbox"  name="emailcheck_'+info.number+'" id="emailcheck_'+info.number+'" checked>'+
                        '<i class="input-helper"></i>'+
                        '<strong >'+info.email+'</strong>'+
                        '</label>'+
                        '</div>';
                    emailsset += '</div>';
                }else if(info.number == 1){
                    emailsset +=  '<h5>Correo fiscal 1</h5>';
                    emailsset +=  '<div class="checkbox">'+
                        '<label id="labele_'+info.number+'">'+
                        '<input type="checkbox"  name="emailcheck_'+info.number+'" id="emailcheck_'+info.number+'">'+
                        '<i class="input-helper"></i>'+
                        '<strong >'+info.email+'</strong>'+
                        '</label>'+
                        '</div>';
                    emailsset += '</div>';
                }else{
                    emailsset +=  '<h5>Correo fiscal 2</h5>';
                    emailsset +=  '<div class="checkbox">'+
                        '<label id="labele_'+info.number+'">'+
                        '<input type="checkbox"  name="emailcheck_'+info.number+'" id="emailcheck_'+info.number+'">'+
                        '<i class="input-helper"></i>'+
                        '<strong >'+info.email+'</strong>'+
                        '</label>'+
                        '</div>';
                    emailsset += '</div>';
                }

                $("#emails_div").html(emailsset);
            });
            //$('#email_receptor').val(data.email);
        },
        error:function(data){
            console.log("fail");
        }
    });
    $('#send_email_modal').modal('show');
}
function loadFiscalInformations(patient_id){
    console.log("asidna");
    $.ajax({
        url: $('#url_get_fiscal_info').val(),
        dataType: "json",
        data: {patient_id: patient_id,doctor_id:$("#doctor_for_search").val()},
        success: function (data) {
            if (data.length == 0){
                $("#stamp_consult_invoice").prop("disabled",true);
                swal("Error", "El paciente no tiene ningun dato fiscal.", "error");
                $('#fiscal_receptor_info').html("");
            }else {
                $("#stamp_consult_invoice").prop("disabled",false);
                var html = "";
                $.each(data, function (idx, info) {
                    html += "<option id='info_" + info.id + "' type='" + info.type + "' value='" + info.id + "'>" + info.business_name.replace(",,", " ") + " - " + info.rfc + "</option>";
                });
                $("#fiscal_info_for_invoice").html(html);
                $("#fiscal_info_for_invoice").selectpicker('refresh');

                setFiscalInfo();
            }
        }
    });
}

function search_invoices(){
    var fecha_inicio = $('#start_date').val();
    var fecha_fin = $('#end_date').val();
    var from = fecha_inicio.split("/");
    var until = fecha_fin.split("/");
    var f = new Date(from[2], from[1] - 1, from[0]);
    var fecha_cadena = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+ f.getDate();
    f = new Date(until[2], until[1] - 1, until[0]);
    var fecha_cadena_fin = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+ (f.getDate()+1);

    var tabla_consultas = "<table id='invoices_general_table' class='table table-striped'><thead><tr>"+
        "<th>Id</th>"+
        "<th>Paciente</th>"+
        "<th>Fecha consulta</th>"+
        "<th>Costo</th>"+
        "<th>Método de pago</th>"+
        "<th></th>"+
        "</tr></thead><tbody>";


    if(fecha_inicio == "" || fecha_fin == ""){
        swal("Error!", "Los campos de fechas son obligatorios.", "error");
        return;
    }
    var row = 1;
    $.ajax({
        url: $('#url_search_invoices').val(),
        dataType: "json",
        data: {fecha_ini:fecha_cadena,
               fecha_f:fecha_cadena_fin,
               doctor_id:$('#doctor_for_search_general').val(),
               type:$('#type_to_invoicee').val()},
        success: function (data) {
            $.each(data, function (idx, info) {
                tabla_consultas += "<tr>"+
                    "<td >"+info.folio+"</td>"+
                    "<td >"+info.patient+"</td>"+
                    "<td >"+info.date+"</td>"+
                    "<td >"+info.amount+"</td>"+
                    "<td >"+info.payment_method+"</td>"+
                    "<td > <input type='checkbox' name='selected' id= 'invoice_selected_"+row+"'  /></td>"+
                    "</tr>";
                row++;
            });
            tabla_consultas += "</tbody></table>";
            $('#general_invoices_table').html(tabla_consultas);
        },
        error:function(){
            console.log("error");
        }
    });

}

function create_general_invoice(){
    $("#payment_method_general").val($("#payment_method_general option:first").val()).selectpicker("refresh");;
    clearPaymentMethodsSelectec();
    $("#payment_methods_box_general").val("");
    $('#ge_fiscal_emisor').val("");
    loadFiscalInfoDoctor();
    var cantidad_total = 0;
    var contador = 1;
    var precios = [];
    var costo = 0;
    var transactions_id = "";
    var rowCount = $('#invoices_general_table tr').length;
    $('#loading_image').show();
    $('#invoices_general_table').find('tr').each(function () {
        var row = $(this);
        if (row.find('input[type="checkbox"]').is(':checked') ) {
            costo = parseFloat($(this).find("td").eq(3).html().replace("$",""));
            transactions_id += parseInt($(this).find("td").eq(0).html()) + ",";
            //$(this).find("td").eq(0).html();//nombre paciente
            //$(this).find("td").eq(1).html();//fecha consulta
            cantidad_total += costo;//costo consulta
            precios.push(costo);
            contador++;
        }

    });
    if(transactions_id==""){
        swal("Error!", "Debe seleccionar al menos un cobro.", "error");
        return;
    }
    $('#transactions_id').val(transactions_id.substring(0,transactions_id.length-1));
    var len=precios.length,
        out=[],
        counts={};

    for (var i=0;i<len;i++) {
        var item = precios[i];
        counts[item] = counts[item] >= 1 ? counts[item] + 1 : 1;
    }
    iva_doc = 0;

    var subtotal = 0;
    var importe_ivaa = 0;
    var total_factura = 0;
    var conceptos_xml = "";
    var cantidad_conceptos = 0;
    var tabla_facturacion_general = "<table  class='table'><thead><tr><th>Cantidad</th><th>Descripción</th>"+
        "<th>U.M</th><th>P.U</th><th>Importe</th></tr></thead><tbody>";// +
    $('#ge_table_invoice').html(tabla_facturacion_general);
    for (var item in counts) {
        subtotal += (item*counts[item]);
        tabla_facturacion_general += "<tr><td class='text-left'>"+counts[item]+"</td><td class='text-left'>Honorarios médicos</td><td class='text-left'>No aplica</td><td class='text-left'>$ "+parseInt(item).toFixed(2)+"</td><td class='text-left'>$ "+(item*counts[item]).toFixed(2)+"</td></tr>";
        conceptos_xml +=  counts[item] + "," + item + "," + (item*counts[item]) + ","
        cantidad_conceptos++
        if(counts[item] > 1)
            out.push(item);
    }
    if($('#type_to_invoicee').val()==1) {
        importe_ivaa = subtotal * ($('#ge_iva').val() / 100);
        total_factura = subtotal;
    }else{
        if(iva_report){
            if(iva_report_included){
                importe_ivaa = subtotal * (iva_for_report/100);
                subtotal = subtotal - importe_ivaa;
                total_factura = subtotal + importe_ivaa;
            }else{
                importe_ivaa = subtotal * (iva_for_report/100);
                total_factura = subtotal + importe_ivaa;
            }
        }else{
            importe_ivaa = subtotal * ($('#ge_iva').val() / 100);
            total_factura = subtotal;
        }
    }
    tabla_facturacion_general += "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>subtotal:</td><td class='text-left'>$"+subtotal.toFixed(2)+"</td></tr>"+
        "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>I.V.A</td><td class='text-left'>$ "+ importe_ivaa.toFixed(2)+"</td></tr>"+
        "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>Ret I.S.R:</td><td class='text-left'>$ 0.00</td></tr>"+
        "<tr><td class='text-left'></td><td class='text-left'></td><td class='text-left'></td><td class='text-left'>Total:</td><td class='text-left'>$"+total_factura.toFixed(2)+"</td></tr>"+
        "</tbody></table>";
    $('#ge_table_invoice').html(tabla_facturacion_general);
    $('#ge_total').val(subtotal);
    $('#ge_iva_importe').val(importe_ivaa.toFixed(2));
    $('#ge_total').val(total_factura.toFixed(2));
    $('#ge_conceptos').val(conceptos_xml.substring(0,conceptos_xml.length-1));
    $('#ge_cant_conceptos').val(cantidad_conceptos);
    $('#ge_subtotal').val(subtotal);

    $('#create_invoice_general_modal').modal("show");
    $('#loading_invoicing').modal("show");

}

function stamp_invoice_general(){

    var accountnumber = "No aplica";
    if(PAYMENT_METHODS==""){
        swal("Error!", "Debe seleccionar al menos un metodo de pago.", "error");
        return;
    }
    if(PAYMENT_METHODS.search("02")>=0 || PAYMENT_METHODS.search("03")>=0 || PAYMENT_METHODS.search("04")>=0 || PAYMENT_METHODS.search("05")>=0
        || PAYMENT_METHODS.search("06")>=0 || PAYMENT_METHODS.search("08")>=0 || PAYMENT_METHODS.search("28")>=0 || PAYMENT_METHODS.search("29")>=0){

        if ($('#ge_account_number').val()=="") {
            swal("Error!", "Para el metodo de pago es necesario ingresar un numero de cuenta.", "error");
            return;
        }else{
            accountnumber = $('#ge_account_number').val();
        }
    }

    $.ajax({
        url: $('#url_stamp_general_action').val(),
        dataType: "json",
        beforeSend: function(){
            $('#ge_loading_image').show();
        },
        complete: function(){
            console.log("complete");
        },
        data: {
            payment_type:$('#ge_payment_type').val(),
            payment_method:PAYMENT_METHODS.substring(0,PAYMENT_METHODS.length-1),
            account_number:accountnumber,
            sub_total:parseFloat($('#ge_total').val()).toFixed(2),
            iva:$('#ge_iva').val(),
            doctor_id:$('#doctor_for_search_general').val(),
            importe_iva:  $('#ge_iva_importe').val(),
            total: parseFloat($('#ge_total').val()).toFixed(2),
            conceptos:$('#ge_conceptos').val(),
            transactions_id: $('#transactions_id').val(),
            type:$('#type_to_invoicee').val()
        },
        success: function () {
            swal("Exito!", "La factura ha sido timbrada de manera exitosamente.", "success");
            location.reload();
        },
        error:function(data){
            $('#ge_loading_image').hide();
            swal("Error!", data.responseText, "error");
        }
    });
}

function showLoading(){
    var html = '<div class="col-md-offset-5 col-md-7">'+
        '<div class="preloader pl-xxl">'+
        '<svg class="pl-circular" viewBox="25 25 50 50">'+
        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
        '</svg>'+
        'Generando...'+
        '</div>'+
        '</div>';

    $('#loading_image').append(html);
}

function print_pdf(transac_id){
    $.ajax({
        url: $('#url_print_pdf').val(),
        dataType: "json",
        data: {transac_id:transac_id},
        success: function (response) {
            if (response.result) {
                window.open(url_print);
            } else {
                showAlertTemplate("Ocurrio un problema.");
            }
        },
        error: function(response){
        }
    });
}

function cancelInvoice(transaction_id,folio){
   swal({
     title:'Cancelar factura ' +folio,
     text: "¿Esta seguro que desea cancelar la factura?",
     type: "warning",
     showCancelButton: true,
     confirmButtonText: 'Si, cancelar',
     cancelButtonText: 'No, cerrar!',
       confirmButtonClass: 'btn btn-success',
     cancelButtonClass: 'btn btn-danger',
     closeOnConfirm: false,
     closeOnCancel: false
     }, function(isConfirm){
         if (isConfirm) {
             $.ajax({
                 url: $('#url_cancel_invoice').val(),
                 dataType: "json",
                 beforeSend: function(){
                     $('#cancel_invoice_loading').show();
                 },
                 data: {transac_id:transaction_id,
                     folio: folio},
                 success: function (data) {
                     if(data.result == "1"){
                         swal("Confirmado!", "La factura ha sido cancelada", "success");
                         location.reload();
                     }
                     else {
                         swal("Error!", data.message, "error");
                     }

                 }
             });

         } else {
             swal("Cancelado!", "Se canceló la operación.", "error");
         }
     });

    $(".lead.text-muted").append("<div style='float: left;width: 25%'><span></span></div><div style='float:left;width: 50%' id='cancel_invoice_loading'></div><div style='float: left;width: 25%'><span></span></div>");





    var html = '<div class="col-md-offset-3 col-md-9">'+
        '<div class="preloader pl-xxl">'+
        '<svg class="pl-circular" viewBox="25 25 50 50">'+
        '<circle class="plc-path" cx="50" cy="50" r="20"></circle>'+
        '</svg>'+
        'Generando...'+
        '</div>'+
        '</div>';



    $('#cancel_invoice_loading').append(html);
    $('#cancel_invoice_loading').hide();

}
function add_concept_to_table(){
    if ($('#concept_quantity').val() == "" || $('#concept_amount').val() == ""){
        swal("Error!", "Debe llenar todos los campos", "error");
        return;
    }
    total_concept = $('#concept_quantity').val() * $('#concept_amount').val();
    if(new_concept == 1){
        $("#concepts_open").append("<tr id='concept_"+row_counter+"'><td style='text-align: center'>"+$('#concept_quantity').val()+"</td><td style='text-align: center'>"+$('#concept_unity').val()+"</td>"+
            "<td style='text-align: center' contenteditable>"+$('#concept_description').val()+"</td><td style='text-align: center' >$"+parseFloat($('#concept_amount').val()).toFixed(2)+"</td><td style='text-align: center' >$"+total_concept.toFixed(2)+"</td><td><a data-toggle='modal' href='#delete_concept_modal' onclick='setDeleteConcept("+row_counter+")'><i class='zmdi zmdi-delete' style='font-size: 1.73em;'></i></a><a style='margin-left: 10px' data-toggle='modal' href='#add_concept_modal' onclick='setEditConcept("+row_counter+")'><i class='zmdi zmdi-edit' style='font-size: 1.73em;'></i></a></td></tr>");
        row_counter++;
    }else{
        $('#concept_'+concept_to_edit).each(function () {
            var row = $(this);
            $(this).find("td").eq(0).html($('#concept_quantity').val());
            $(this).find("td").eq(1).html($('#concept_unity').val());
            $(this).find("td").eq(2).html($('#concept_description').val());
            $(this).find("td").eq(3).html('$'+parseFloat($('#concept_amount').val()).toFixed(2));
            $(this).find("td").eq(4).html('$'+total_concept.toFixed(2));
        });
    }
    $('#add_concept_modal').modal("hide");
    $('#with_iva').attr('checked', false);
    $('#with_isr').attr('checked', false);
    calculate_totals();
    $('#concept_quantity').val("");
    $('#concept_description').val("");
    $('#concept_amount').val("");
}
function clear_add_concept(){
    $('#concept_quantity').val("");
    $('#concept_description').val("");
    $('#concept_amount').val("");
    new_concept = 1;
}
function setEditConcept(rowId){
    $('#concept_'+rowId).each(function () {
        var row = $(this);
        $('#concept_quantity').val($(this).find("td").eq(0).html());
        $('#concept_unity').val($(this).find("td").eq(1).html());
        $('#concept_description').val($(this).find("td").eq(2).html());
        $('#concept_amount').val($(this).find("td").eq(3).html().replace("$",""));
    });
    new_concept = 0;
    concept_to_edit = rowId;
}
function setDeleteConcept(concept_id){
    $('#concept_id_counter').val(concept_id);

}
function delete_concept(){
    $('#concept_'+$('#concept_id_counter').val()).remove();
    $('#delete_concept_modal').modal('hide');
    $('#with_iva').attr('checked', false);
    $('#with_isr').attr('checked', false);
    calculate_totals();
}
function setEditConcept(rowId){
    $('#concept_'+rowId).each(function () {
        var row = $(this);
        $('#concept_quantity').val($(this).find("td").eq(0).html());
        $('#concept_unity').val($(this).find("td").eq(1).html());
        $('#concept_description').val($(this).find("td").eq(2).html());
        $('#concept_amount').val($(this).find("td").eq(3).html().replace("$",""));
    });
    new_concept = 0;
    concept_to_edit = rowId;
}
function delete_concept(){
    $('#concept_'+$('#concept_id_counter').val()).remove();
    $('#delete_concept_modal').modal('hide');
    calculate_totals();
}
function calculate_totals(){
    subtotal_open_invoice = 0;
    $('#concepts_open').find('tbody').find('tr').each(function () {
        var row = $(this);
        subtotal_open_invoice += parseFloat($(this).find("td").eq(4).html().replace("$",""));
    });
    $('#open_invoice_subtotal').val("$"+subtotal_open_invoice.toFixed(2));
    calculate_with_iva();
    calculate_with_isr();
}
function calculate_with_iva(){
    total_invoice_open = 0;
    iva_open_invoice = 0;
    if ($('#with_iva').is(':checked')){
        iva_open_invoice = $('#open_invoice_subtotal').val().replace("$","")*(16/100);
        $('#total_iva_amount').val("$"+iva_open_invoice.toFixed(2));
        if ($('#with_isr').is(':checked')){
            total_invoice_with_iva = (parseFloat($('#open_invoice_total').val().replace("$",""))+iva_open_invoice).toFixed(2);
        }else{
            total_invoice_with_iva = (parseFloat($('#open_invoice_subtotal').val().replace("$",""))+iva_open_invoice).toFixed(2);
        }
    }else{
        iva_open_invoice = $('#open_invoice_subtotal').val().replace("$","")*(-16/100);
        if ($('#with_isr').is(':checked')){
            total_invoice_with_iva = (parseFloat($('#open_invoice_total').val().replace("$",""))+iva_open_invoice).toFixed(2);
        }else{
            total_invoice_with_iva = (parseFloat($('#open_invoice_subtotal').val().replace("$",""))).toFixed(2);
        }
        $('#total_iva_amount').val("$0.00");
    }



    if(total_invoice_with_iva > 0){
        $('#open_invoice_total').val("$"+ total_invoice_with_iva);
    }else{
        $('#open_invoice_total').val("$0.00");
    }

}
function calculate_with_isr(){
    isr_open_invoice = 0;
    if ($('#with_isr').is(':checked')){
        isr_open_invoice = $('#open_invoice_subtotal').val().replace("$","")*(10/100);
        if ($('#with_iva').is(':checked')){
            total_invoice_with_isr = (parseFloat($('#open_invoice_total').val().replace("$",""))-isr_open_invoice).toFixed(2);
        }else{
            total_invoice_with_isr = (parseFloat($('#open_invoice_subtotal').val().replace("$",""))-isr_open_invoice).toFixed(2);
        }
        $('#total_isr_amount').val("$"+isr_open_invoice.toFixed(2));
    }else{
        isr_open_invoice = $('#open_invoice_subtotal').val().replace("$","")*(-10/100);
        if ($('#with_iva').is(':checked')){
            total_invoice_with_isr = (parseFloat($('#open_invoice_total').val().replace("$",""))-isr_open_invoice).toFixed(2);
        }else{
            total_invoice_with_isr = (parseFloat($('#open_invoice_subtotal').val().replace("$",""))).toFixed(2);
        }
        $('#total_isr_amount').val("$0.00");
    }



    if(total_invoice_with_isr > 0){
        $('#open_invoice_total').val("$"+ total_invoice_with_isr);
    }else{
        $('#open_invoice_total').val("$0.00");
    }
}
function stamp_open_invoice(){
    var int_number_open = "-";
    if($('#fiscal_int_number_open').val()!=""){
        int_number_open = $('#fiscal_int_number_open').val();
    }
    concepts_open_invoice = "";
    $('#concepts_open').find('tbody').find('tr').each(function () {
        var row = $(this);
        concepts_open_invoice += $(this).find("td").eq(0).html() + "|" + $(this).find("td").eq(1).html() + "|" + $(this).find("td").eq(2).html() + "|" + $(this).find("td").eq(3).html().replace("$","") + "|" + $(this).find("td").eq(4).html().replace("$","") + "|";
    });
    iva_porcentage = 0;
    isr_porcentage = 0;
    if ($('#fiscal_business_name_open').val() == "" || $('#fiscal_rfc_open').val() == ""
        || $('#fiscal_address_open').val() == "" || $('#fiscal_locality_open').val() == ""
        || $('#fiscal_ext_number_open').val() == "" || $('#fiscal_zip_open').val() == ""
        || $('#fiscal_city_open').val() == "" || $('#fiscal_state_open').val() == ""
        || $('#fiscal_country_open').val() == "" || $('#fiscal_suburb_open').val() == "")
    {
        swal("Error!", "Los campos con * son obligatorios.", "error");
        return;
    }
    if (!ValidaRfc($('#fiscal_rfc_open').val())){
        swal("Error!", "El formato del RFC es incorrecto", "error");
        return;
    }
    if (concepts_open_invoice == ""){
        swal("Error!", "Debe ingresar al menos un concepto.", "error");
        return;
    }

    if ($('#with_iva').is(':checked')){
        iva_porcentage = 16;
    }
    if ($('#with_isr').is(':checked')){
        isr_porcentage = 10;
    }
    var accountnumber = "No aplica";

    if(PAYMENT_METHODS==""){
        swal("Error!", "Debe seleccionar al menos un metodo de pago.", "error");
        return;
    }
    if(PAYMENT_METHODS.search("02")>=0 || PAYMENT_METHODS.search("03")>=0 || PAYMENT_METHODS.search("04")>=0 || PAYMENT_METHODS.search("05")>=0
        || PAYMENT_METHODS.search("06")>=0 || PAYMENT_METHODS.search("08")>=0 || PAYMENT_METHODS.search("28")>=0 || PAYMENT_METHODS.search("29")>=0){

        if ($('#account_number_open').val()=="") {
            swal("Error!", "Para el metodo de pago es necesario ingresar un numero de cuenta.", "error");
            return;
        }else{
            accountnumber = $('#account_number_open').val();
        }
    }
    $.ajax({
        url: $('#url_stamp_openinvoice_action').val(),
        dataType: "json",
        beforeSend: function(){
            $('#loading_image_open').show();
        },
        complete: function(){
            console.log("complete");
        },
        data: {
            amount:$('#open_invoice_total').val().replace("$",""),
            business_name:$('#fiscal_business_name_open').val(),
            rfc:$('#fiscal_rfc_open').val().toUpperCase(),
            address:$('#fiscal_address_open').val(),
            locality:$('#fiscal_locality_open').val(),
            ext_number:$('#fiscal_ext_number_open').val(),
            int_number:int_number_open,
            zip:$('#fiscal_zip_open').val(),
            city:$('#fiscal_city_open').val(),
            state:$('#fiscal_state_open').val(),
            country:$('#fiscal_country_open').val(),
            payment_type:$('#payment_type_open').val(),
            payment_method:PAYMENT_METHODS.substring(0,PAYMENT_METHODS.length-1),
            payment_method_name:PAYMENT_METHODS_NAME.substring(0,PAYMENT_METHODS_NAME.length-1),
            suburb:$('#fiscal_suburb_open').val(),
            account_number:accountnumber,
            sub_total:$('#open_invoice_subtotal').val().replace("$",""),
            iva:iva_porcentage,
            isr:isr_porcentage,
            doctor_id: $('#doctor_for_search_open').val(),
            conceptos:concepts_open_invoice.substring(0,concepts_open_invoice.length-1),
            importeiva:$('#total_iva_amount').val().replace("$",""),
            importeisr:$('#total_isr_amount').val().replace("$",""),
        },
        success: function (data) {
            if(data == 1){
                swal("Exito!", "La factura ha sido timbrada de manera exitosamente.", "success");
                window.location.href = "/asistente/facturas/facturas_abiertas";
            }
            else {
                $('#loading_image_open').hide();
                swal("Error!", data, "error");
            }

        },
        error:function(data){
            $('#loading_image_open').hide();
            swal("Error!", data.responseText, "error");
        }
    });
}
function validarEmail( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ) {
        return false;
    }
    else{
        return true;
    }

}
function ValidaRfc(rfcStr) {
    var strCorrecta;
    strCorrecta = rfcStr;
    if (rfcStr.length == 12){
        var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }else{
        var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }
    var validRfc=new RegExp(valid);
    var matchArray=strCorrecta.match(validRfc);
    if (matchArray==null) {
        return false;
    }
    else
    {
        return true;
    }

}

function openPaymentMethods(){
    $('#select_pay_method').modal('show');
}
function clearPaymentMethods(){
    $('#payment_methods_box').val("");
    PAYMENT_METHODS = "";
    PAYMENT_METHODS_TEXT = "";
    PAYMENT_METHODS_NAME = "";
}
function add_payment_method(){
    PAYMENT_METHODS += $('#payment_method').val()+",";
    PAYMENT_METHODS_NAME += $('#payment_method').find("option:selected").text() + ",";
    PAYMENT_METHODS_TEXT += $('#payment_method').val()+":" + $('#payment_method').find("option:selected").text();
    $('#payment_methods_box').val(PAYMENT_METHODS_TEXT);
    PAYMENT_METHODS_TEXT += ",";
    $('#select_pay_method').modal('hide');
    $('body').addClass('modal-open');
}
function openPaymentMethodsopen(){
    $('#select_pay_method_open').modal('show');
}
function openPaymentMethodsgeneral(){
    $('#select_pay_method_general').modal('show');
}
function clearPaymentMethodsopen(){
    $('#payment_methods_box_open').val("");
    PAYMENT_METHODS = "";
    PAYMENT_METHODS_TEXT = "";
    PAYMENT_METHODS_NAME = "";
}
function clearPaymentMethodsgeneral(){
    $('#payment_methods_box_general').val("");
    PAYMENT_METHODS = "";
    PAYMENT_METHODS_TEXT = "";
    PAYMENT_METHODS_NAME = "";
}
function add_payment_methodopen(){
    PAYMENT_METHODS += $('#payment_method_open').val()+",";
    PAYMENT_METHODS_NAME += $('#payment_method_open').find("option:selected").text() + ",";
    PAYMENT_METHODS_TEXT += $('#payment_method_open').val()+":" + $('#payment_method_open').find("option:selected").text();
    $('#payment_methods_box_open').val(PAYMENT_METHODS_TEXT);
    PAYMENT_METHODS_TEXT += ",";
    $('#select_pay_method_open').modal('hide');
}
function add_payment_methodgeneral(){
    PAYMENT_METHODS += $('#payment_method_general').val()+",";
    PAYMENT_METHODS_NAME += $('#payment_method_general').find("option:selected").text() + ",";
    PAYMENT_METHODS_TEXT += $('#payment_method_general').val()+":" + $('#payment_method_general').find("option:selected").text();
    $('#payment_methods_box_general').val(PAYMENT_METHODS_TEXT);
    PAYMENT_METHODS_TEXT += ",";
    $('#select_pay_method_general').modal('hide');
    $('body').addClass('modal-open');
}
