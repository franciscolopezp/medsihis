jQuery(document).ready(function() {

    $("#general_fiscal_info_rfc").focusout(function(){
        rfc_check = $("#general_fiscal_info_rfc").val();
        if(rfc_check != ""){
            if (!ValidaRfc(rfc_check)){
                $("#general_fiscal_info_rfc").val("");
                swal("Error!","El formato del RFC es incorrecto, por favor ingrese uno valido", "error");
            }
        }
    });
    $('form#new_general_fiscal_info').find("input[type=text], textarea").val("")
    $("#new_general_fiscal_info").validate();
    var grid = $("#data-table-general_fiscal_info").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-general_fiscal_info').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a title='Editar' href=" + row.url_edit + "  class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-edit\" title=\"Editar\"></i></a>" +
                    "<a href='#' onclick='show_data("+row.id+")' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" title='Mostrar'><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title='Eliminar'><i class=\"zmdi zmdi-delete\" title=\"Eliminar\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var business_name = $tr_parent.children('td').eq(0).text();
            var rfc = $tr_parent.children('td').eq(1).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar la informacion fiscal "+business_name+"/"+rfc+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Eliminar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "La información fiscal ha sido eliminado exitosamente.", "success");
                        grid.bootgrid('reload');
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });


    $('#new_general_fiscal_info').submit(function(e){
        $( "#submit_new_info" ).prop( "disabled", true );
        if($('#new_general_fiscal_info').valid()){
            return;
        }else{
            e.preventDefault();
            $( "#submit_new_info" ).prop( "disabled", false );
        }
    });

});

function ValidaRfc(rfcStr) {
    var strCorrecta;
    strCorrecta = rfcStr;
    if (rfcStr.length == 12){
        var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }else{
        var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }
    var validRfc=new RegExp(valid);
    var matchArray=strCorrecta.match(validRfc);
    if (matchArray==null) {
        return false;
    }
    else
    {
        return true;
    }

}
function show_data(data_id){
    $.ajax({
        url: $('#url_show_contact').val(),
        dataType: "json",
        data: {id:data_id},
        success: function (data) {
            $.each(data.data, function (idx, info) {
                $("#o_business_name").text(info.business_name);
                $("#o_rfc").text(info.rfc);
                $("#o_address").text(info.address);
                $("#o_locality").text(info.locality);
                $("#o_ext_number").text(info.ext_number);
                $("#o_int_number").text(info.int_number);
                $("#o_zip").text(info.zip);
                $("#o_suburb").text(info.suburb);
                $("#o_city").text(info.city);
            });
        },
        error:function(data){
        }
    });
    $('#show_data_modal').modal('show');
}