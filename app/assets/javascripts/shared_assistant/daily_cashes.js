var TRANSACTION_TYPES = {
    charge:1,
    transfer:2,
    other:3,
    report:4
}

var CLOSE_DAILY_CASH_OPTIONS = {
    close:1,
    withdraw:2,
    transfer:3
}

var MOVEMENT_TYPES = {
    other:14
}

var PAYMENT_METHODS = {
    cash:1,
    transfer:3
}

var DAILY_CASH_ACCOUNT = 0;

$(function(){
    $("#open_dc_btn").click(openOpenDailyCashModal);
    $("#create_dc_btn").click(openDailyCash);
    $(".open_close_dc_modal_btn").click(function(){
        var account_id = $(this).attr("acnt");
        var daily_cash_id = $(this).attr("dcid");
        var doctor_id = $(this).attr("doctor");

        openCloseDailyCashWindow(daily_cash_id,account_id,doctor_id);
    });

    $("#transaction_doctor").change(setupByDoctor);

    $("#add_transaction_btn").click(addTransaction);
    $("#transaction_type").change(setupByTransactionType);
    $("#t_type").change(setupByType);
    $("#transaction_payment_method").change(setupByPaymentMethod);

    $("#close_daily_cash_btn").click(closeDailyCash);

    $(".add_currency_amount").change(function(){
        var v = $(this).val();
        var doctor_id = $("#doctor_id_add_transaction").val();
        if($(this).is(":checked")){
            $("#de_"+doctor_id+"_holder_"+v).show();
        }else{
            $("#de_"+doctor_id+"_holder_"+v).hide();
        }
    });



    $(".close_dc_currency").change(function(){
        var v = $(this).val();
        var doctor_id = $("#doctor_id_daily_cash").val();
        if($(this).is(":checked")){
            $("#amount_target_holder_"+v+"_"+doctor_id).show();
        }else{
            $("#amount_target_holder_"+v+"_"+doctor_id).hide();
        }

        var operation = $("#final_transaction").val();
        if(operation == CLOSE_DAILY_CASH_OPTIONS.transfer){
            $("#target_close_dc_"+v+"_"+doctor_id).show();
        }else{
            $("#target_close_dc_"+v+"_"+doctor_id).hide();
        }
    });



    $("#final_transaction").change(function(){
        var operation = $(this).val();
        var doctor_id = $("#doctor_id_daily_cash").val();

        if(operation != CLOSE_DAILY_CASH_OPTIONS.close){
            $("#close_data").show();
            $("#exchanges_data_"+doctor_id).show();
        }else{
            $("#close_data").hide();
            $("#exchanges_data_"+doctor_id).hide();
        }

        if(operation == CLOSE_DAILY_CASH_OPTIONS.transfer){
            $(".target_close_cd_holder").show();
            $(".close_account_doctor").hide();
            $(".close_account_doctor_"+doctor_id).show();
            $(".close_target_account").selectpicker("refresh");
        }else{
            $(".target_close_cd_holder").hide();
        }

    });

    $("#transaction_consult").change(function(){
        var price = $("#consult_"+$(this).val()).attr("price");
        $(".default_exchange").val(price);
    });


});

$(document).ready(function(){
    setupByDoctor();
    $('#doctor_for_search').change(reload);
    var grid = $("#data-table-history-daily-cash").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url:function(){
            var doctor = $('#doctor_for_search').val();
            if(doctor == ""){
                doctor = "all"
            }
            var fecha_cadena = "";
            var fecha_cadena_end = "";
            var start_date = $('#start_date').val();
            if(start_date == ""){
                fecha_cadena = "all"
            }else{
                var from = start_date.split("/");
                var f = new Date(from[2], from[1] - 1, from[0]);
                fecha_cadena = f.getFullYear()+"-"+(f.getMonth()+1)+"-"+ f.getDate();
            }
            var end_date = $('#end_date').val();
            if(end_date == ""){
                fecha_cadena_end = "all"
            }else{
                var end = end_date.split("/");
                var fe = new Date(end[2], end[1] - 1, end[0]);
                fecha_cadena_end = fe.getFullYear()+"-"+(fe.getMonth()+1)+"-"+ (fe.getDate()+1);
            }
            return $('#data-table-history-daily-cash').data('source')+"?doctor_id="+doctor+"&start_date="+fecha_cadena+"&end_date="+fecha_cadena_end;
        },

        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>"+
                    "<a href=" + row.url_print + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" target='_blank'><i class=\"zmdi zmdi-print\" title=\"Imprimir\"></i></a>";
            }
        }
    });
});
function reload(){
    $('#data-table-history-daily-cash').bootgrid("reload");
}

function openCloseDailyCashWindow(id,account_id,doctor_id){
    $("#final_transaction").val(CLOSE_DAILY_CASH_OPTIONS.close);
    $("#final_transaction").selectpicker("refresh");
    DAILY_CASH_ACCOUNT = account_id;
    $("#close_daily_cash_window").modal("show");
    $("#close_daily_cash_id").val(id);
    $("#doctor_id_daily_cash").val(doctor_id);

    $(".close_account_doctor").hide();
    $(".close_account_doctor_"+doctor_id).show();

    $(".close_target_account").val("");
    $(".close_target_account").selectpicker("refresh");

    $(".exchanges_data_doctor").hide();
}

function closeDailyCash(){
    var option = parseInt($("#final_transaction").val());
    var dc_id = $("#close_daily_cash_id").val();
    var doctor_id = $("#doctor_id_daily_cash").val();

    var amounts = [];
    var amount_error = false;
    var target_account_error = false;

    $("input[name=close_dc_currency_"+doctor_id+"]:checked").map(function(){
        var v = $(this).val();
        var amount = $("#close_dc_currency_"+v+"_"+doctor_id).val();
        amounts.push({
            currency:v,
            amount: amount,
            target_account: $("#target_account_close_dc_"+v+"_"+doctor_id).val()
        });

        if(amount == 0){
            amount_error = true;
        }

        if($("#target_account_close_dc_"+v+"_"+doctor_id).val() == ""){
            target_account_error = true;
        }
    });

    if(option != CLOSE_DAILY_CASH_OPTIONS.close && amounts.length == 0){
        swal("No se puede llevar a cabo la operación","Seleccione al menos un tipo de moneda.","error");
        return;
    }

    if(option != CLOSE_DAILY_CASH_OPTIONS.close && amount_error){
        swal("No se puede llevar a cabo la operación","Ingrese un monto válido.","error");
        return;
    }

    switch (option){
        case CLOSE_DAILY_CASH_OPTIONS.close:
            break;
        case CLOSE_DAILY_CASH_OPTIONS.withdraw:
            break;
        case CLOSE_DAILY_CASH_OPTIONS.transfer:
            // verificar que se quiera transferir a cuentas diferentes
            var error_target_account = false;
            var error_currency_account = false;
            $.each(amounts,function(idx, a){
                if(a.target_account == DAILY_CASH_ACCOUNT){
                    error_target_account = true;
                }

                var option = $("#target_close_dc_option_"+a.target_account);
                if(option.attr("bank") == "bank" && option.attr("currency") != a.currency){
                    error_currency_account = true;
                }

            });

            if(error_target_account){
                swal("No se puede llevar a cabo la operación","Seleccione una caja diferente para hacer la transacción final.","error");
                return;
            }

            if(error_currency_account){
                swal("No se puede llevar a cabo la operación","Seleccione un banco con el mismo tipo de cambio.","error");
                return;
            }

            if(target_account_error){
                swal("No se puede llevar a cabo la operación","Por favor seleccione correctamente la cuenta destino.","error");
                return;
            }

            break
    }

    $("#close_daily_cash_btn").prop("disabled",true);

    $.ajax({
        url:$("#daily_cashes_url").val()+"/"+dc_id,
        method:"PUT",
        dataType:"json",
        data:{
            option:option,
            amounts:amounts,
            transaction_type_id:PAYMENT_METHODS.transfer,
            t_type:0,
            account_id:DAILY_CASH_ACCOUNT
        },
        success:function(data){

            return;
            if(data.done){
                var url_print = $("#print_daily_cash_url").val();
                url_print = url_print.replace("/0","/");
                window.open(url_print+dc_id+".pdf");
                location.reload();
            }else{
                swal(data.error, data.message,"error");
            }
        }
    });

}





function loadConsults(){
    var doctor_id = $("#transaction_doctor").val();
    var office_id = $("#transaction_doctor_"+doctor_id).attr("office");
    $.ajax({
        url:"/doctor/dashboard/pending_consult",
        dataType:"json",
        data:{office_id:[office_id]},
        success:function(data){
            var html = "";
            $.each(data,function(idx,consult){
                html += "<option id='consult_"+consult.id+"' charged='"+consult.charged+"' debt='"+consult.debt+"' price='"+consult.price+"' value='"+consult.id+"'>"+dateToString(new Date(consult.date),"dd/fm/yyyy hh:mn pp")+" - "+ consult.patient+"</option>";
            });

            $("#transaction_consult").html(html);
            $("#transaction_consult").selectpicker('refresh');

            var consult_id = $("#transaction_consult").val();
            var price = $("#consult_"+consult_id).attr("price");

            $(".default_exchange").val(price);

        }
    });
}

function openOpenDailyCashModal(){
    $("#open_daily_cash_modal").modal("show");
}

function openDailyCash(){
    var account = $("#daily_cash_account").val();
    if(account == null || account == "0"){
        swal("Seleccionar Cuenta", "Por favor seleccione una cuenta válida.", "error");
        return;
    }
    $.ajax({
        url:$("#daily_cashes_url").val(),
        method:"POST",
        dataType:"json",
        data:{
            account_id:account
        },
        success:function(data){
            if(data.done){
                location.reload();
            }else{
                $("#add_transaction_btn").prop("disabled",false);
                swal(data.error,data.message,"error");
            }
        }
    });
}

function openAddTransactionModal(){
    $("#transaction_type").val($("#transaction_type option:first").val()).selectpicker('refresh');
    force_transaction = false;
    $("#add_transaction_btn").prop("disabled",false);
    $("#add_transaction_window").modal("show");
    $("#transaction_details").val("");
}




function setupByTransactionType(){

    var transaction_type = parseInt($("#transaction_type").val());
    $("#t_type").parent().hide();
    $("#to_account").parent().hide();
    $("#transaction_consult").parent().hide();
    $("#transaction_report").parent().hide();
    $("#movement_type").parent().hide();
    $("#transaction_payment_method").parent().show();
    $("#transaction_payment_method").val(PAYMENT_METHODS.cash);
    switch (transaction_type){
        case TRANSACTION_TYPES.charge:
            $("#transaction_consult").parent().show();
            $("#t_type").val(1);
            setupByPaymentMethod();
            break;
        case TRANSACTION_TYPES.transfer:
            $("#to_account").parent().show();
            $("#transaction_payment_method").parent().hide();
            $("#transaction_payment_method").val(PAYMENT_METHODS.transfer);
            $("#t_type").val(0);


            var doctor_id = $("#transaction_doctor").val();
            $(".t_account").hide();
            $(".account_doctor_"+doctor_id).show();

            break;
        case TRANSACTION_TYPES.other:
            $("#t_type").parent().show();
            $("#movement_type").parent().show();
            setupByPaymentMethod();
            break;
        case TRANSACTION_TYPES.report:
            $("#transaction_report").parent().show();
            $("#t_type").val(1);
            $("#transaction_payment_method").parent().show();
            loadReports();
            break;
    }

    $("#t_type").selectpicker('refresh');
    $("#transaction_payment_method").selectpicker('refresh');



}

function setupByType(){
    $("#movement_type").val("");
    var type = parseInt($("#t_type").val());
    if(type == 1){
        $(".egress").hide();
        $(".ingress").show();
    }else{
        $(".egress").show();
        $(".ingress").hide();

        var doctor_id = $("#transaction_doctor").val();
        $(".account_doctor_"+doctor_id).show();
    }

    setupByPaymentMethod();

    // show all accounts when is egress
}

function setupByPaymentMethod(){
    $("#transaction_account").val("");
    var payment_type_id = $("#transaction_payment_method").val();
    var affect_local = $("#payment_method_"+payment_type_id).attr("affect_local");

    var doctor_id = $("#transaction_doctor").val();
    $(".t_account").hide();

    if("true" == affect_local){
        $(".account_doctor_"+doctor_id+".local_account").show();
    }else{
        $(".account_doctor_"+doctor_id+".bank_account").show();
    }

    $("#transaction_account").selectpicker("refresh");
}

function setupByDoctor(){
    $("#transaction_type").val(TRANSACTION_TYPES.charge);
    $("#transaction_type").selectpicker("refresh");

    var doctor_id = $("#transaction_doctor").val();

    $("#doctor_id_add_transaction").val(doctor_id);

    setupByTransactionType();
    setupByType();
    setupByPaymentMethod();


    $(".target_account").hide();
    $(".target_account_doctor_"+doctor_id).show();

    $(".doctor_currencies").hide();
    $("#currencies_"+doctor_id).show();

    loadConsults();


    //verificar si el doctor cobra estudios

    if($("#transaction_doctor_"+doctor_id).attr("charge_report") == "true"){
        $("#transaction_type_"+TRANSACTION_TYPES.report).show();
    }else{
        $("#transaction_type_"+TRANSACTION_TYPES.report).hide();
    }

    $("#transaction_type").selectpicker("refresh");

}

var force_transaction = false;
function addTransaction(){
    var doctor_id = $("#transaction_doctor").val();

    var t_type = $("#t_type").val();
    var transaction_type = parseInt($("#transaction_type").val());
    var source_account = $("#transaction_account").val();
    var target_account = $("#to_account").val()
    var daily_cash_account = $("#daily_cash_account").val();
    var details = $("#transaction_details").val();
    var movement_type = $("#movement_type").val();

    var option_source = $("#account_"+source_account);
    var option_target = $("#target_account_"+target_account);

    var amounts = [];

    var amount_error = false;
    var new_charge = 0;
    $("input[name=add_currency_amount_"+doctor_id+"]:checked").map(function(){
        var amount = $("#currency_doctor_"+doctor_id+"_input_"+$(this).val()).val();
        var rate = $("#doctor_"+doctor_id+"_currency_"+$(this).val()).attr("exchange");
        amounts.push({
            currency:$(this).val(),
            amount: amount
        });
        if(amount == 0){
            amount_error = true;
        }

        new_charge += (parseFloat(rate) * parseFloat(amount));
    });

    switch (transaction_type){
        case TRANSACTION_TYPES.charge:
            if($("#transaction_consult").val() == null){
                swal("Consulta no válida","Por favor seleccione una consulta válida.","error");
                return;
            }

            console.log(new_charge);

            if(!force_transaction){
                var debt = parseFloat($("#consult_"+$("#transaction_consult").val()).attr("debt"));
                var charged = parseFloat($("#consult_"+$("#transaction_consult").val()).attr("charged"));
                var price = parseFloat($("#consult_"+$("#transaction_consult").val()).attr("price"));
                var charge_sum = new_charge + charged;
                if(debt < charge_sum){
                    var extra = charge_sum - price;
                    swal({
                            title: "Confirmar operación",
                            text: "El monto cobrado exede por "+toCurrency(extra)+". ¿Desea agregera el cobro de todos modos?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Confirmar",
                            closeOnConfirm: true },
                        function(){
                            force_transaction = true;
                            addTransaction();
                        });

                    return;
                }
            }

            break;
        case TRANSACTION_TYPES.transfer:
            if(source_account == target_account){
                swal("No puede hacer una transferencia a la misma cuenta","Por favor elija cuentas diferentes para poder realizar la transferencia.","error");
                return;
            }

            // banco a banco
            if(option_source.hasClass('bank_account') && option_target.hasClass('target_bank_account')){ // si la transferencia es de banco a banco
                if(option_source.attr('currency') == option_target.attr('currency')){ // checar que el tipo de cambio sea correcto
                    if(amounts.length != 1){ // checar que se quiera transferir unicamente un tipo de cambio
                        swal("No se puede realizar la transferencia","Debe seleccionar el tipo de cambio correcto.","error");
                        return;
                    }else{
                        var a = amounts[0];
                        if(a.currency != option_source.attr('currency')){ // checar que el tipo de cambio seleccionado coincide con el de los bancos
                            swal("No se puede realizar la transferencia","El tipo de cambio es incorrecto.","error");
                            return;
                        }
                    }

                }else{
                    swal("No se puede realizar la transferencia","Los tipos de cambio de los bancos no coinciden","error");
                    return;
                }
            }

            break;
        case TRANSACTION_TYPES.other:
            $("#t_type").parent().show();
            $("#movement_type").parent().show();
            if(movement_type == ""){
                swal("Concepto no válido","Por favor seleccione un concepto de la lista.","error");
                return;
            }
            break;
        case TRANSACTION_TYPES.report:
            if($("#transaction_report").val() == null){
                swal("Reporte no válido","Por favor seleccione un reporte válido.","error");
                return;
            }
            break;
    }

    // checar si alguna cuenta es de banco
    if(option_source.hasClass('bank_account') || option_target.hasClass('target_bank_account')){
        if(amounts.length != 1){ // checar que se quiera transferir unicamente un tipo de cambio
            swal("No se puede realizar la transferencia","Debe seleccionar el tipo de cambio correcto.","error");
            return;
        }else{
            var a = amounts[0];
            if(option_source.hasClass('bank_account') && a.currency != option_source.attr('currency')){ // checar que el tipo de cambio seleccionado coincide con el de los bancos
                swal("No se puede realizar la transferencia","El tipo de cambio es incorrecto.","error");
                return;
            }else if(option_target.hasClass('target_bank_account') && a.currency != option_target.attr('currency')){
                swal("No se puede realizar la transferencia","El tipo de cambio es incorrecto.","error");
                return;
            }
        }
    }

    if(amount_error){
        swal("Monto inválido","Por favor ingrese un monto válido","error");
        return;
    }

    if(source_account == ""){
        swal("Datos incompletos","Por favor seleccione una cuenta.","error");
        return;
    }

    var data = {
        t_type:t_type,
        transaction_type_id:$("#transaction_type").val(),
        payment_method_id:$("#transaction_payment_method").val(),
        movement_type_id: movement_type,
        account_id:source_account,
        details: details,
        consult_id:$("#transaction_consult").val(),
        report_id:$("#transaction_report").val(),
        to_account_id:target_account
    };

    data.amounts = amounts;

    $("#add_transaction_btn").prop("disabled",true);
    $.ajax({
        url:$("#add_transaction_url").val(),
        data:data,
        dataType:"json",
        success:function(data){
            if(data.done){
                location.reload();
            }else{
                $("#add_transaction_btn").prop("disabled",false);
                swal(data.error,data.message,"error");
            }
        }
    });
}

function loadReports(){
    var doctor_id = $("#transaction_doctor").val();
    var office_id = $("#transaction_doctor_"+doctor_id).attr("office");

    $.ajax({
        url:"/asistente/index/pending_report",
        dataType:"json",
        data:{office_id:[office_id]},
        success:function(data){
            var html = "";
            $.each(data,function(idx,report){

                var d = stringToDate(report.date,"yyyy-mm-dd hh:mn")

                html += "<option id='report_"+report.id+"' price='"+report.price+"' value='"+report.id+"'>"+report.study+" | "+dateToString(d,"dd/fm/yyyy")+" - "+ report.patient+"</option>";

            });
            $("#transaction_report").html(html);
            $("#transaction_report").selectpicker('refresh');
            setReportPrice();
        }
    });
}

function setReportPrice(){
    var select_val = $("#transaction_report").val();
    var price = $("#report_"+select_val).attr("price");
    $(".default_exchange").val(price);
}
function reload_daylies(){
    $("#data-table-history-daily-cash").bootgrid("reload");
}
function clear_history_filters(){
    $('#start_date').val("");
    $('#end_date').val("");
    $('#data-table-history-daily-cash').bootgrid('reload');
}



