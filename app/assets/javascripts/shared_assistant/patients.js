
/**
 * Created by equipo-01 on 6/04/16.
 */
//= require demo_js/webcam.js
//= require croppie/croppie.js
var fiscal_info_delete;
var $uploadCrop;
var streaming = false,
    video        = document.querySelector('#video'),
    canvas       = document.querySelector('#canvas'),
    photo        = document.querySelector('#photo'),
    width = 320,
    height = 0;
jQuery(document).ready(function() {
    if ($("#person_type").val()=="Física"){
        $("#Fisica").show();
        $("#Moral").hide();
    } else {
        $("#Moral").show();
        $("#Fisica").hide();
    }

    $("#patient_phone").on("propertychange change", function(){
        $( "#submit_new_patient" ).prop( "disabled", false );
    });
    $("#patient_cell").on("propertychange change", function(){
        $( "#submit_new_patient" ).prop( "disabled", false );
    });

    $('form#new_patient').find("input[type=text], textarea").val("");
    $("#new_patient").validate();
    var grid = $("#data-table-patient").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-patient').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit_assistant_shared + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-edit\" title=\"Editar\"></i></a>" +
                    "<a href=" + row.url_show_assistant_shared + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var last_name = $tr_parent.children('td').eq(1).text();
            //var patient_id = $tr_parent.children('td').eq(0).text();
            var patient_id = $(this).data('row-id');
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Esta seguro que quiere enviar a inactivo al paciente "+name+" "+last_name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Inactivo",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'GET',
                        data:{
                            patient_id:patient_id,
                            active:"1"
                        },
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El paciente ha sido enviado a inactivos exitosamente.", "success");
                        $('#data-table-patient').bootgrid('reload');
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });
        });
    });

    //table de inactivos
    var gridInactives = $("#data-table-patient-inactives").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: $('#data-table-patient-inactives').data('source'),
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_show + " class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>" +
                    "<a href=" + row.url_show_medical_expedient + " class=\"btn btn-primary btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-assignment-o\" title=\"Abrir expediente\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\" title=\"Restaurar\"><i class=\"zmdi zmdi-mail-reply zmdi-hc-fw\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        gridInactives.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(1).text();
            var patient_id = $tr_parent.children('td').eq(0).text();
            var last_name = $tr_parent.children('td').eq(2).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Restaurar paciente "+name+" "+last_name+" ?";
            swal({
                    title: text_confirm,
                    //text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Si, restaurar!",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'GET',
                        data:{
                            patient_id:patient_id,
                            active:"0"
                        },
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El paciente y expediente ha sido restaurado exitosamente.", "success");
                        $('#data-table-patient-inactives').bootgrid('reload');
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar restaurar el paciente.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });
        });
    });
    //fin de tabla de inactivos
    var url = $('#url_search_city').val();

    $('.city_search').typeahead({
        ajax: {
            url: url,
            method: 'get',
            triggerLength: 1,
            loadingClass: 'loading',
            displayField: 'name',
        },
        items: 5,
        itemSelected: set_city_id,
        matcher: function (item) {
            if (item.toLowerCase().indexOf(this.query.toLowerCase()) == 0 || clear_accent(item).indexOf(clear_accent(this.query)) == 0) {
                return -1;
            }
        }
        //    item: ''
    });
    $('.city_search').change(function() {
        if($('#city_name').val() == ""){
            $('#patient_city_id').val("");
        }
    });

    if($("#patient_birth_day").length > 0 && $("#patient_birth_day").val() != ""){
        calculatePatientAge();
    }

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('.upload-demo').addClass('ready');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 185,
            height: 185,
            type: 'square'
        },
        boundary: {
            width: 275,
            height: 275
        }
    });

    $('#upload').on('change', function () { readFile(this); });


    $('#submit_new_patient').click(function(e){
        $('#form_patient').submit();
        e.preventDefault();
        $("#submit_new_patient").attr('disabled','disabled');
        if($('#form_patient').valid()){
            $('#form_patient').submit();
        }else{
            $("#submit_new_patient").removeAttr("disabled");
        }
    });
});
function crop64(){
    $uploadCrop.croppie('result',{
        type: 'canvas',
        size: 'viewport'
    }).then(function (result) {
        $('#result-image').attr('src', result);
        $('#crop_image_64').val(result);
    });
    $('#upload_picture').hide();
    $("#crop_image_modal").modal("hide");
}
function open_crop_image_modal(){
    $("#crop_image_modal").modal("show");
}
function calculatePatientAge(){
    $.ajax({
        url:$("#calculate_age_url").val(),
        method:"GET",
        dataType:"json",
        data:{
            date:dateToString($("#patient_birth_day").ts_date(),"yyyy-mm-dd")
        },
        success:function(data){
            $("#age_label").html(data.age);
        }
    });
}

$(function(){
    $("#patient_birth_day").on("dp.change",calculatePatientAge);
});

function set_city_id(item, val, text) {
    $('#patient_city_id').val(val);
}

function take_snapshot() {
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(video, 0, 0, width, height);
    var data = canvas.toDataURL('image/png');
    document.getElementById('results').innerHTML =
        '<div class="row"></div>'+
        '<img src="'+data+'" width="250" height="200"/>' +
        '<button type="button" class="btn btn-link" onclick="remove_picture()">Cancelar</button>';
    $('#picture64').val(data);
    $("#take_picture").modal("hide");
    $('#upload_picture').hide();
    $('#button_crop_size').hide();
    $('#results').show();
    $('#crop_image_64').val("");

    navigator.getMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

    navigator.getMedia(
        {
            video: true,
            audio: false
        },
        function(stream) {
            if (navigator.mozGetUserMedia) {
                video.mozSrcObject = stream;
                stream.getVideoTracks()[0].stop()
            } else {
                var vendorURL = window.URL || window.webkitURL;
                video.src = vendorURL.createObjectURL(stream);
                stream.getVideoTracks()[0].stop()
            }
            video.pause();

        },
        function(err) {
            console.log("An error occured! " + err);
        }
    );
    /*   var foto64;
     $("#upload_picture").hide();
     Webcam.snap( function(data_uri) {
     foto64 = data_uri;
     // display results in page
     document.getElementById('results').innerHTML =
     '<div class="row"></div>'+
     '<img src="'+data_uri+'" width="250" height="200"/>' +
     '<button type="button" class="btn btn-link" onclick="remove_picture()">Cancelar</button>';
     } );
     $('#picture64').val(foto64);
     Webcam.reset();
     $("#take_picture").modal("hide");
     $('#upload_picture').hide();
     $('#button_crop_size').hide();
     $('#results').show();*/
}
function remove_picture(){
    $('#picture64').val("");
    $('#button_crop_size').show();
    $('#upload_picture').show();
    $('#results').hide();
}

function open_camera(){

    streaming = false,
        video        = document.querySelector('#video'),
        canvas       = document.querySelector('#canvas'),
        photo        = document.querySelector('#photo'),
        width = 320,
        height = 0;

    navigator.getMedia = ( navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

    navigator.getMedia(
        {
            video: true,
            audio: false
        },
        function(stream) {
            if (navigator.mozGetUserMedia) {
                video.mozSrcObject = stream;
            } else {
                var vendorURL = window.URL || window.webkitURL;
                video.src = vendorURL.createObjectURL(stream);
            }
            video.play();

        },
        function(err) {
            console.log("An error occured! " + err);
        }
    );

    video.addEventListener('canplay', function(ev){
        if (!streaming) {
            height = video.videoHeight / (video.videoWidth/width);
            video.setAttribute('width', width);
            video.setAttribute('height', height);
            canvas.setAttribute('width', width);
            canvas.setAttribute('height', height);
            streaming = true;
        }
    }, false);

    $("#take_picture").modal("show");
    /* Webcam.set({
     width: 320,
     height: 240,
     image_format: 'jpeg',
     jpeg_quality: 90
     });
     Webcam.attach( '#my_camera' );*/
}

function close_camera(){
    Webcam.reset();
    $("#take_picture").modal("hide");
}

function showinfo(){
    if ($("#person_type").val()=="Física"){
        $("#Fisica").show();
        $("#Moral").hide();
    } else {
        $("#Moral").show();
        $("#Fisica").hide();
    }
}
function validarEmail( email ) {
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( !expr.test(email) ) {
        return false;
    }
    else{
        return true;
    }

}
function saveFiscalInformation(patient_id,action,fi_id){
    if(!ValidaRfc($("#fi_rfc").val())){
        swal("Error!", "El formato del RFC es incorrecto", "error");
        return;
    }
    if($("#person_type").val()=="Física"){
        if($("#fi_name").val()=="" || $("#fi_last_name").val()=="" ||
            $("#fi_rfc").val()=="" ||
            $("#fi_address").val()=="" ||
            $("#fi_ext_number").val() =="" ||
            $("#fi_suburb").val() =="" ||
            $("#fi_zip").val() =="" ||
            $("#fi_city_id_ts_autocomplete").val()==""){
            swal("Error!", "Los campos con * son obligatorios.", "error");
            return;
        }
    }
    else{
        if($("#fi_bussines_name").val()=="" ||
            $("#fi_rfc").val()=="" ||
            $("#fi_address").val()=="" ||
            $("#fi_ext_number").val() =="" ||
            $("#fi_suburb").val() =="" ||
            $("#fi_zip").val() =="" ||
            $("#fi_city_id_ts_autocomplete").val()==""){
            swal("Error!", "Los campos con * son obligatorios.", "error");
            return;
        }
    }
    if($("#fi_email").val()!=""){
        if(!validarEmail($("#fi_email").val())){
            swal("Error","El primer correo no tiene el formato correcto","error");
            return;
        }
    }
    if($("#fi_email2").val()!=""){
        if(!validarEmail($("#fi_email2").val())){
            swal("Error","El segundo correo no tiene el formato correcto","error");
            return;
        }
    }

    var url = "/doctor/informacion_fiscal_paciente/";
    var method = "POST";
    bussines_name = "";
    if($("#person_type").val()!="Física"){
        bussines_name = $("#fi_bussines_name").val();
    }
    else {
        bussines_name = $("#fi_name").val()+ ",," +$("#fi_last_name").val();
    }
    var params = {
        patient_fiscal_information:{
            person_type:$("#person_type").val(),
            rfc:$("#fi_rfc").val(),
            business_name: bussines_name,
            address:$("#fi_address").val(),
            ext_number:$("#fi_ext_number").val(),
            int_number:$("#fi_int_number").val(),
            suburb:$("#fi_suburb").val(),
            locality:$("#fi_suburb").val(),
            zip:$("#fi_zip").val(),
            email:$("#fi_email").val(),
            email2:$("#fi_email2").val(),
            patient_id:patient_id,
            city_id:$("#fi_city_id_ts_autocomplete").val()
        }
    };

    if(action != "add"){
        url += fi_id;
        method = "PUT";
    }
    $("#addfiscalinfo").modal("hide");

    $.ajax({
        url:url,
        method:method,
        dataType:"json",
        data:params,
        success:function(){
            location.reload();
        }
    });
}
function setupFiscalInfoEdit(fi_id,person_type,business_name,rfc,address,suburb,locality,int_number,ext_number,zip,city_id,city_name,patient_id,email,email2){
    $("#add_fiscal_info_btn").attr("onclick","saveFiscalInformation("+patient_id+",'update',"+fi_id+")");

    $("#fi_city_id").val(city_name);

    if(person_type!="Física"){
        $("#fi_bussines_name").val(business_name);
    }
    else {
        full_name = business_name.split(",,");
        $("#fi_name").val(full_name[0]);
        $("#fi_last_name").val(full_name[1]);
    }
    $("#person_type").val(person_type).selectpicker("refresh");
    if ($("#person_type").val()=="Física"){
        $("#fi_bussines_name").val("");
        $("#Fisica").show();
        $("#Moral").hide();
    } else {
        $("#fi_name").val("");
        $("#fi_last_name").val("");
        $("#Moral").show();
        $("#Fisica").hide();
    }
    $("#fi_rfc").val(rfc);

    $("#fi_address").val(address);
    $("#fi_suburb").val(suburb);
    $("#fi_ext_number").val(ext_number);
    $("#fi_int_number").val(int_number);
    $("#fi_zip").val(zip);
    $("#fi_email").val(email);
    $("#fi_email2").val(email2);
    $("#fi_locality").val(locality);
    $("#fi_city_id_ts_autocomplete").val(city_id);
}

function setupdeletefiscalinfo(fi_id,patient_id){
    fiscal_info_delete = fi_id;
}
function deleteFiscalInformation(){
    $("#deletefiscalinfo").modal("hide");
    $.ajax({
        url:"/doctor/informacion_fiscal_paciente/"+fiscal_info_delete,
        method:"DELETE",
        dataType:"json",
        success:function(){
            $("#patient_fiscal_info_"+fiscal_info_delete).remove();
        }
    });
}

function setAddFiscalInfoModal(patient_id){
    $("#add_fiscal_info_btn").attr("onclick","saveFiscalInformation("+patient_id+",'add',0)");

    $("#person_type").val("Física").change();;
    $("#fi_rfc").val("");
    $("#fi_name").val("");
    $("#fi_last_name").val("");
    $("#fi_bussines_name").val("");
    $("#fi_address").val("");
    $("#fi_suburb").val("");
    $("#fi_ext_number").val("");
    $("#fi_int_number").val("");
    $("#fi_zip").val("");
    $("#fi_email").val("");
    $("#fi_email2").val("");
    $("#fi_locality").val("");
    $("#fi_city_id_ts_autocomplete").val("");
}

function ValidaRfc(rfcStr) {
    var strCorrecta;
    strCorrecta = rfcStr;
    if (rfcStr.length == 12){
        var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }else{
        var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }
    var validRfc=new RegExp(valid);
    var matchArray=strCorrecta.match(validRfc);
    if (matchArray==null) {
        return false;
    }
    else
    {
        return true;
    }

}
function checkone_number(){
    cellphone = $('#patient_cell').val();
    telephone = $('#patient_phone').val();

    if(cellphone == "" && telephone == ""){
        swal("Error!", "Al menos debe incluir un numero telefonico o celular.", "error");
        $( "#submit_new_patient" ).prop( "disabled", false );
        $("#submit_new_patient").removeAttr("disabled");
        return false;
    }else{
        return true;
        $( "#submit_new_patient" ).prop( "disabled", true );
    }
}