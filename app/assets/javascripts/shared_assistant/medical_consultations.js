jQuery(document).ready(function() {
    var url = $('#url_get_doctor_patients').val();
    if($('#doctor_for_search').val() != ""){
        var patients_option =  "<option value = ''>TODO</option>";
        $.ajax({
            url: url,
            dataType: "json",
            data: {doctor_id:$('#doctor_for_search').val()},
            success: function (data) {
                $.each(data.patients, function (idx, info) {
                    patients_option += "<option value = "+info.id+">"+info.name+"</option>";
                });
                $('#patient_for_search').html(patients_option);
                $('#patient_for_search').selectpicker('refresh');

            },
            error:function(data){
            }
        });
    }else{
        $('#patient_for_search').html("");
        $('#patient_for_search').selectpicker('refresh');
    }
    $('#doctor_for_search').change(function(){


        setTimeout(function(){

            if($('#doctor_for_search').val() != ""){
                var patients_option =  "<option value = ''>TODO</option>";
                $.ajax({
                    url: url,
                    dataType: "json",
                    data: {doctor_id:$('#doctor_for_search').val()},
                    success: function (data) {
                        $.each(data.patients, function (idx, info) {
                            patients_option += "<option value = "+info.id+">"+info.name+"</option>";
                        });
                        $('#patient_for_search').html(patients_option);
                        $('#patient_for_search').selectpicker('refresh');
                        searchConsults();

                    },
                    error:function(data){
                    }
                });
            }else{
                $('#patient_for_search').html("");
                $('#patient_for_search').selectpicker('refresh');
            }

        },1000);
    });

    var grid = $("#data-table-consultations").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function(){
            var start_date = $('#start_date_search').val();
            var end_date = $("#end_date_search").val();
            var patient = $("#patient_for_search").val();
            var office = $("#office_search").val();
            var doctor = $('#doctor_for_search').val();
            if(doctor == ""){
                doctor = "all"
            }
            return $('#data-table-consultations').data('source')+"?patient="+patient+"&start_date="+start_date+"&end_date="+end_date+"&office_id="+office+"&doctor_id=" + doctor;
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {

    });

});

function searchConsults(){
    $("#data-table-consultations").bootgrid("reload");
}

function allConsults(){
    var ids = ["start_date_search","end_date_search"];
    $.each(ids,function(idx,id){
        $("#"+id).val("");
    });
    $('#patient_for_search').val("");
    $("#patient_for_search").selectpicker("refresh");
    searchConsults();
}

function exportPdf(){
    var start_date = $('#start_date_search').val();
    var end_date = $("#end_date_search").val();
    var patient = $("#patient_for_search").val();
    var doctor = $('#doctor_for_search').val();
    if(doctor == ""){
        doctor = "all"
    }
    var url =  $('#url_print_report').val()+".pdf?patient="+patient+"&start_date="+start_date+"&end_date="+end_date+"&doctor_id="+doctor;

    window.open(
        url,
        '_blank'
    );

}