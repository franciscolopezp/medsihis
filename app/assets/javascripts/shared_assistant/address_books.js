jQuery(document).ready(function() {
    $('#doctor_for_search').change(function(){
        reload();
    });
    $('form#new_address_book').find("input[type=text], textarea").val("")
    $("#new_address_book").validate();
    var grid = $("#data-table-address_book").bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "GET",
            cache: false
        },
        url: function()
        {
            return $('#data-table-address_book').data('source')+"?user_type="+$('#user_type').val()+"&doctor_id="+$("#doctor_for_search").val();
        },
        css: {
            icon: 'zmdi icon',
            iconColumns: 'zmdi-view-module',
            iconDown: 'zmdi-expand-more',
            iconRefresh: 'zmdi-refresh',
            iconUp: 'zmdi-expand-less'
        },
        formatters: {
            "commands": function(column, row) {
                return "<a href=" + row.url_edit + " class=\"btn bgm-amber btn-icon waves-effect waves-circle waves-float btn-edit\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-edit\" title=\"Editar\"></i></a>" +
                    "<a href='#' onclick='show_data("+row.id+")' class=\"btn btn-info btn-icon waves-effect waves-circle waves-float btn-show\" data-row-id=\"" + row.id + "\" ><i class=\"zmdi zmdi-eye\" title=\"Mostrar\"></i></a>" +
                    "<button data-url-delete=\"" + row.url_destroy + "\" class=\"btn bgm-deeporange btn-icon waves-effect waves-circle waves-float btn-delete\" data-row-id=\"" + row.id + "\"><i class=\"zmdi zmdi-delete\" title=\"Eliminar\"></i></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        /* Executes after data is loaded and rendered */
        grid.find(".btn-delete").on("click", function (e) {
            var $tr_parent=$(this).parents("tr").first();
            var name = $tr_parent.children('td').eq(0).text();
            var url_aux = $(this).data("url-delete");
            var text_confirm = "¿Eliminar al contacto "+name+" ?";
            swal({
                    title: text_confirm,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Eliminar",
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false
                },
                function(){
                    var ajax = $.ajax({
                        type: 'DELETE',
                        url: url_aux
                    });
                    ajax.done(function(result, status){
                        swal("Eliminado!", "El contacto ha sido eliminado exitosamente.", "success");
                        grid.bootgrid('reload');
                    });

                    ajax.fail(function(error, status, msg){
                        swal("Error!", "Ha ocurrido un error al intentar eliminar.", "error");
                        console.log("Error: " + error.responseText + msg);
                    });

                });

        });
    });


    $('#new_address_book').submit(function(e){
        $( "#submit_new_personal" ).prop( "disabled", true );
        if($('#new_address_book').valid()){
            return;
        }else{
            e.preventDefault();
            $( "#submit_new_personal" ).prop( "disabled", false );
        }
    });

});
function reload(){
    $("#data-table-address_book").bootgrid("reload");
}
function show_data(data_id){
    $.ajax({
        url: $('#url_show_contact').val(),
        dataType: "json",
        data: {data_id:data_id},
        success: function (data) {
            $.each(data.data, function (idx, info) {
                $("#o_name").text(info.name);
                $("#o_telephone").text(info.telephone);
                $("#o_hospital").text(info.hospital);
                $("#o_address").text(info.address);
                $("#o_email").text(info.email);
                $("#o_office").text(info.office);
                $("#o_cellphone").text(info.cellphone);
            });
        },
        error:function(data){
        }
    });
    $('#show_data_modal').modal('show');
}