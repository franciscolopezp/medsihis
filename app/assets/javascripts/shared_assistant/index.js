//= require shared_assistant/working_schedule/index


//= require shared_assistant/working_schedule/dashboard_assistant_calendar
//require shared_assistant/working_schedule/dashboard_assistant_waitingroom
//require shared_assistant/working_schedule/dashboard_assistant_medical_consultation
//require shared_assistant/working_schedule/daily_cash
//= require shared_assistant/working_schedule/dashboard

$(function(){
    $("#activity_patient").ts_autocomplete({
        url:AUTOCOMPLETE_PATIENTS_URL,
        in_modal:true,
        items:10,
        required:true,
        model:"un paciente",
        filterByDoctor:true
    });
});


