/**
 * Created by equipo1 on 5/05/16.
 */

(function ( $ ) {
    $.fn.ts_date = function( options ) {
        var settings = $.extend({
            // These are the defaults.
            hour:false
        }, options );

        var str = $(this).val();

        var year = parseInt(str.substring(6,10));
        var month = parseInt(str.substring(3,5));
        var date = parseInt(str.substring(0,2));
        var hour = 0;
        var minute = 0;

        if(settings.hour){
            var dateComponents = str.split(" ");
            var format = dateComponents[2];
            var hComp = dateComponents[1].split(":");

            var h = parseInt(hComp[0]);
            var m = parseInt(hComp[1]);

            if(format.toLowerCase() == "pm" && h != 12){
                h += 12;
            }else if(format.toLowerCase() == "am" && h == 12){
                h = 0;
            }

            hour = h;
            minute = m;
        }
        return new Date(year, month - 1, date,hour,minute,0);

    };

    $.fn.ts_hour = function( date ) {

        var hour = 0;
        var minute = 0;
        var year = 0;
        var month = 0;
        var day = 0;

        if(date != undefined){
            year = date.getFullYear();
            month = date.getMonth();
            day = date.getDate();
        }

        var str = $(this).val();

        var hourComps = str.split(" ");
        var timeComps = hourComps[0].split(":");

        hour = parseInt(timeComps[0]);
        minute = parseInt(timeComps[1]);

        if(hourComps[1].toLowerCase() == "pm" && hour != 12){
            hour += 12;
        }

        return new Date(year,month,day,hour,minute,0);

    };

}( jQuery ));
