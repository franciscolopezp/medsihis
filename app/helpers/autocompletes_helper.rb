module AutocompletesHelper
  def search_medicaments_pharmacy_items(word)
    items = []
    Medicament.joins(:active_substances).where("name like '%#{word}%' or code like '%#{word}%' or comercial_name like '%#{word}%' or tags like '%#{word}%'").limit(20).each do |c|
      items.push({id: c.id, value: c.comercial_name+' ('+c.presentation+')',type:"med"})
    end
    PharmacyItem.where("code like '%#{word}%' or description like '%#{word}%' or tags like '%#{word}%'").limit(20).each do |c|
      items.push({id: c.id, value: c.code+' ('+c.description+')',type:"item"})
    end
    items
  end

  def search_hospital_services(word)
    items = []
    HospitalService.where("name like '%#{word}%'").limit(20).each do |c|
      items.push({id: c.id, value: c.name,type:'service'})
    end
    ServicePackage.where("name like '%#{word}%'").limit(20).each do |c|
      items.push({id: c.id, value: c.name,type:'package'})
    end
    items
  end


  def search_diseases(word)
    items = Disease.select('id, name').where('name like ?',"%#{word}%").limit(15)
    items
  end
end