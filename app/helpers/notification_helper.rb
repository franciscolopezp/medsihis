module NotificationHelper
  require 'fcm'

  def notification_private_web(key, message)
    channel = "/messages/private/#{key}"

    PrivatePub.publish_to channel, message
  end

  def notification_app(user,message)
    key_fire_base = ENV['TOKEN_FIREBASE']
    fcm = FCM.new(key_fire_base)

    message[:userId] = user.id
    registration_ids= user.user_tokens.map{|token| token.token} # an array of one or more client registration tokens
    if registration_ids.size > 0
      options = {data: message, collapse_key: "message_medsi"}
      response = fcm.send(registration_ids, options)
      puts "RESPONSE MESSAGE APP"
      puts response
    end
  end

  def notification_all(key_web, message_web, user, message_app)
    notification_private_web key_web, message_web
    notification_app user, message_app
  end

  # se llama cuando:
  # Doctor confirma cita
  # Doctor cancela cita
  # Doctor reagenda cita
  def notification_doctor_and_patient(key_web_doctor, message_web, user_patient_app, activity, flag_schedule = nil, date_before = nil)
    case activity.activity_status.id
      when ActivityStatus::SCHEDULED
        # confirmado o reagendado
        if flag_schedule
          # reagendado
          message_app = message_reschedule_appointment_app date_before, activity.start_date
        else
          # confirmado
          message_app = message_confirm_appointment_app activity
        end
        notification_all(key_web_doctor, message_web, user_patient_app, message_app)
      # notification_app @activity.user_patients.first.user, message_app
      when ActivityStatus::CANCELED
        # cancelado
        message_app = message_cancel_appointment_app activity
        notification_all(key_web_doctor, message_web, user_patient_app, message_app)
      #notification_app @activity.user_patients.first.user, message_app
    end
  end

  def notification_public

  end

  # se envia al doctor
  def message_create_appointment_app activity
    {
        id: 0, # <- Se agrego el id para que en el movil pueda agregarle su propio id
        title: "Cita por confirmar",
        body: "Tiene una cita por confirmar del paciente "+activity.user_patients.first.full_name
    }
  end

  def message_confirm_appointment_app activity
    {
        id: 0,
        title: "Cita confirmada",
        body: "La cita del "+I18n.localize(activity.start_date, :format => :notification)+" ha sido confirmada"
    }
  end

  def message_cancel_appointment_app activity
  {
      id: 0,
      title: "Cancelación de cita",
      body: "El doctor "+activity.doctor.full_name+" acaba de cancelar su cita"
  }
  end

  def message_reschedule_appointment_app date_before, date_after
    {
       id: 0,
       title: "Cita reagendado",
       body: "La cita del "+I18n.localize(date_before,:format => :notification)+" se a reagendado a "+I18n.localize(date_after,:format => :notification)
    }
  end

end