module SharedAssistant::WorkingScheduleHelper


  def get_permission_assistant
    @accounts = []
    @daily_cashes = []

    #arreglo de ids de consultorios a los que tiene permiso de corte de caja

    daily_cash_permission = []
    assistant = current_user.assistant

    show_daily_cashes = assistant.permissions.exists?(name:'daily_cash')

    assistant.office_permissions.where("permission_id = ?",Permission::DAILY_CASH).each do |office_permission|
      daily_cash_permission << office_permission.office_id
    end

    if show_daily_cashes
      @daily_cashes = DailyCash.open_by_office current_user.assistant.offices.map{|office| office.id}
      # se consultan las cuentas locales que tiene el consultorio
      local_accounts = Account.local_accounts current_user.assistant.offices.map{|office| office.id}
      # de las cuentas obtenidas se filtran aquellas que no tengan corte de caja abierto
      # esta es la variable que se usa para pintar el select al momento de abrir cortes de caja
      @accounts = get_no_dc_accounts(local_accounts)
    end
    @all_exchanges = []

    @all_accounts = []

    current_user.assistant.doctors.each do |doctor|
      doctor_exchanges = doctor.currency_exchanges
      doctor_exchanges.each do |exch|
        @all_exchanges.push(format_currency exch)
      end
      doctor_accounts = Account.by_doctor doctor.id
      doctor_accounts.each do |a|
        if !a.bank.is_local || daily_cash_permission.include?(a.office_id)
          @all_accounts.push(format_account a)
        end
      end
    end
  end

  def format_currency cu
    {
        :doctor_id => cu.doctor_id,
        :currency_id => cu.currency_id,
        :iso_code => cu.currency.iso_code
    }
  end
  def format_account a
    {
        :id => a.id,
        :account_number => a.account_number,
        :balance => a.balance,
        :bank => a.bank.name + " (" + a.currency.iso_code + ")",
        :doctor_id => a.doctor.id,
        :affect_local => a.bank.is_local,
        :currency => a.currency.id,
        :iso_code => a.currency.iso_code
    }
  end

  def get_no_dc_accounts accounts
    resp = []
    accounts.each do |a|
      show_account = true
      @daily_cashes.each do |dc|
        if dc.account_id == a.id
          show_account = false
        end
      end
      if show_account
        resp.push(a)
      end
    end
    resp
  end

  #todas las actividades de los consultorios asignados al asistente compartido, en un mes determinado
  def get_activities
    # filtrar las actividades por:
    # consultorios.
    # rango de fechas o el mes donde se encuentre
    #
    offices = params[:office_id]
    month = params[:month].to_i + 1
    month = (month.to_i < 10)? "0"+month.to_s : month

    activities = Activity.joins(:offices).where(:offices => {id: offices})
                     .where("DATE_FORMAT(activities.start_date,'%Y-%m') = ? or DATE_FORMAT(activities.end_date,'%Y-%m') = ?","#{params[:year].to_s+"-"+month.to_s}","#{params[:year].to_s+"-"+month.to_s}")

    if params[:doctor_id] != ''
      activities = activities.where('activities.doctor_id = ?',params[:doctor_id])
    end

    activities
  end

  def activities_waiting_room

    offices = params[:office_id]
    month = params[:month].to_i + 1
    month = (month.to_i < 10)? "0"+month.to_s : month

    day = params[:date].to_i
    day = (day.to_i < 10)? "0"+day.to_s : day

    activities = Activity.joins(:offices).where(:offices => {id: offices}).where("DATE_FORMAT(activities.start_date,'%Y-%m-%d') = ?","#{params[:year].to_s+"-"+month.to_s+"-"+day.to_s}")
    .where(activity_status_id: ActivityStatus::WAITING)
    result = []
    activities.each do |a|
      act = format_wroom a
      result.push(act)
    end
    return result

  end

  def format_wroom a
    patient_name = ""
    patient_image = ''
    if a.patients.size == 1
      p = a.patients.first
      patient_name = p.name+" "+p.last_name
      if p.picture != nil && p.picture != ''
        patient_image = download_image_doctor_patients_path p.id
      end
    end
    {
        :id => a.id,
        :name => a.name,
        :details => a.details,
        :start_date => a.start_date,
        :end_date => a.end_date,
        :patient => patient_name,
        :picture => patient_image
    }
  end

  def pending_reports
    consults = []
    doctors_offices = params[:office_id]
    doctors_offices.each do |office|
      doc_office = Office.find(office)
      if doc_office.doctor.charge_reports
        medical_consultations = CabinetReport.pending_by_offices office
        medical_consultations.each do |c|
          consults.push(format_pending_report c)
        end
      end
    end
    consults
  end
  def pending_consults
    consults = []
    medical_consultations = MedicalConsultation.where('is_charged is not true').where(office: params[:office_id])
    medical_consultations.each do |c|
      consults.push(format_consult c)
    end
    consults
  end

  def format_pending_report r
    {
        :id => r.id,
        :patient => r.clinical_analysis_order.medical_expedient.patient.full_name,
        :patient_id => r.clinical_analysis_order.medical_expedient.patient.id,
        :date => r.created_at.strftime('%Y-%m-%d %H:%M'),
        :price => r.price,
        :study => r.clinical_analysis_order.clinical_studies.first.name,
        :doctor_id => r.clinical_analysis_order.medical_expedient.doctor_id,
        :balance => (r.price - r.get_total)
    }
  end

  def format_consult c
    {
        :id => c.id,
        :patient => c.medical_expedient.patient.full_name,
        :patient_id => c.medical_expedient.patient.id,
        :date => c.date_consultation,
        :price => c.price,
        :doctor_id => c.doctor.id,
        :balance => (c.price - c.get_total)
    }
  end
end
