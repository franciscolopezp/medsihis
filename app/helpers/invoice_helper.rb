module InvoiceHelper
  include InvoiceTsHelper
  XML_BASE = 'xml_base.xml'
  ORIGINAL_STRING = 'cadena_original.txt'
  ORIGINAL_XSLT = 'cadenaoriginal_3_2.xslt'

  def create_invoice(params)
    fiscal_catalog = EmisorFiscalInformation.find(params[:emisor_id])

    invoice_data = get_invoice_data params, fiscal_catalog

    dir = fiscal_catalog_path(fiscal_catalog)
    path_info = {
        :root_path => dir,
        :xml_base => dir.join(XML_BASE),
        :xslt_file => Rails.root.join(ORIGINAL_XSLT),
        :original_string => dir.join(ORIGINAL_STRING),
        :pem_file => dir+"certificate_key/key.pem"
    }

    invoice = InvoiceTs.new(invoice_data,path_info)
    finkok_response = invoice.stamp


    status = "error"
    invoice = nil
    message = ''

    if finkok_response[:done]
      qr_plain_string = "?re=#{invoice_data[:transmitter][:rfc]}&rr=#{invoice_data[:receiver][:rfc]}&tt=#{"%0.6f" % invoice_data[:total]}&id=#{finkok_response[:uuid]}"
      path_qr = "uploads/clinic_info/#{fiscal_catalog.id}/"
      qr_name = Time.now.to_i.to_s+".png"
      create_qr(qr_plain_string,path_qr,qr_name)
      invoice_data[:qr_code] = "/"+path_qr + qr_name
      puts invoice_data[:qr_code]
      invoice_data[:xml_base] = dir.join(XML_BASE)
      status = "ok"
      message = "La factura se ha generado exitosamente"
      original_string = File.read(path_info[:original_string])
      invoice = save_invoice_record(invoice_data, finkok_response,original_string.to_s)
      fiscal_catalog.update(folio: fiscal_catalog.folio + 1)
    else
      message = finkok_response[:message]
    end

    {
        :status => status,
        :invoice => invoice,
        :message => message
    }
  end

  def save_invoice_record(invoice_data, finkok_response, original_string)
    inv = AdmissionInvoice.new(
                     :emisor_fiscal_information_id => invoice_data[:fiscal_catalog_id],
                     :serie => invoice_data[:serie],
                     :folio => invoice_data[:folio],
                     :subtotal => invoice_data[:sub_total],
                     :iva => invoice_data[:iva_amount],
                     :isr => invoice_data[:isr_amount],
                     :payment_condition => invoice_data[:payment_condition],
                     :payment_way_id => invoice_data[:payment_condition_id],
                     :payment_methos_name => PaymentMethod.where('code IN (?)', invoice_data[:payment_method].split(",")).map{|pm| pm.name}.join(", "),
                     :account_number => invoice_data[:account_number],
                     :UUID => finkok_response[:uuid],
                     :xml => finkok_response[:xml],
                     :xml_base => invoice_data[:xml_base],
                     :certification_date => invoice_data[:time],
                     :sat_seal => finkok_response[:timbre],
                     :no_cert_sat => finkok_response[:no_certificado_sat],
                     :transmitter_seal => finkok_response[:transmitter_seal],
                     :original_string_text => original_string,
                     :qr_code => invoice_data[:qr_code],
                     :user_id => current_user.id
    )

    if inv.valid?
      inv.save
      invoice_data[:transmitter][:admission_invoice_id] = inv.id
      invoice_data[:receiver][:admission_invoice_id] = inv.id

      AdmissionInvoiceEmisor.create(invoice_data[:transmitter])
      AdmissionInvoiceReceptor.create(invoice_data[:receiver])

      invoice_data[:concepts].each do |c|
        AdmissionInvoiceConcept.create(
            :admission_invoice_id => inv.id,
            :quantity => c[:quantity],
            :unit => c[:unit_measure],
            :description => c[:description],
            :unit_price => c[:unit_price],
            :cost => c[:total]
        )
      end
    end
    inv
  end

  def get_next_folio(fiscal_information)
    folio = fiscal_information.folio + 1
    fiscal_information.update(folio: folio)
    folio
  end

  def get_invoice_data(params,fiscal_catalog)
    if params[:receptor_type_id] == "general"
      receiver = GeneralFiscalInfo.find(params[:receptor_id])
    else
      receiver = PatientFiscalInformation.find(params[:receptor_id])
    end


    data = {
        :fiscal_catalog_id => fiscal_catalog.id,
        :serie => fiscal_catalog.serie,
        :folio => fiscal_catalog.folio,
        :time => Time.now,
        :payment_condition => PaymentWay.find(params[:payment_way_id]).name,
        :payment_condition_id => params[:payment_way_id],
        :payment_method => PaymentMethod.where('id IN (?)', params[:payment_method_id]).map{|pm| pm.code}.join(","),
        :certificate_number => fiscal_catalog.certificate_number,
        :certificate_base64 => fiscal_catalog.certificate_base64,
        :sub_total => params[:sub_total],
        :total => params[:total],
        :expedition_city => fiscal_catalog.city.name,
        :expedition_state => fiscal_catalog.city.state.name,
        :account_number => params[:account_number],
        :isr_rate => params[:isr_rate],
        :isr_amount => params[:isr],
        :iva_rate => params[:iva_rate],
        :iva_amount => params[:iva],
        :tax_regime => fiscal_catalog.tax_regime,
        :transmitter => {
            :business_name => fiscal_catalog.bussiness_name,
            :rfc => fiscal_catalog.rfc,
            :address => fiscal_catalog.fiscal_address,
            :no_ext => fiscal_catalog.ext_number,
            :no_int => fiscal_catalog.int_number.present? ? fiscal_catalog.int_number : "-",
            :country => fiscal_catalog.city.state.country.name,
            :state => fiscal_catalog.city.state.name,
            :city => fiscal_catalog.city.name,
            :suburb => fiscal_catalog.suburb,
            :locality => fiscal_catalog.locality,
            :zip => fiscal_catalog.zip,
            :tax_regime => fiscal_catalog.tax_regime
        },
        :receiver => {
            :business_name => receiver.business_name.gsub(',,', ' '),
            :rfc => receiver.rfc.upcase,
            :address => receiver.address,
            :no_ext => receiver.ext_number,
            :no_int => receiver.int_number.present? ? receiver.int_number : "-",
            :country => receiver.city.state.country.name,
            :state => receiver.city.state.name,
            :city => receiver.city.name,
            :suburb => receiver.suburb,
            :locality => receiver.locality,
            :zip => receiver.zip
        },
        :concepts => params[:invoice_concepts].collect {|c|{
            :quantity => c[:quantity],
            :unit_measure => c[:measure],
            :description => c[:description],
            :unit_price => c[:unit_price],
            :total => c[:total]
        }},
    }

    data
  end

  def cancel_invoice invoice
    if invoice.nil?
      return {
          :status => "error",
          :message => "No se encuentra la factura"
      }
    end

    cert = get_certificate_base64(invoice.fiscal_catalog.certificate_stamp.url)
    pem_file_path = invoice.fiscal_catalog.pem_file

    finkok = Finkok.new
    resp = finkok.cancel(invoice.uuid,invoice.fiscal_catalog.rfc,pem_file_path, cert)

    cancel_response = {}
    if resp[:done] && resp[:data].present?
      invoice.update(xml: resp[:data], active: false, cancel_date: Time.now)
      cancel_response = {
          :status => "ok",
          :message => "La factura se canceló con éxito",
          :response => resp
      }
    else
      cancel_response = {
          :status => "error",
          :message => "No es posible cancelar la factura",
          :response => resp
      }
    end

    cancel_response

  end

  def create_qr(cadena,ruta,nombre)
    qrcode = RQRCode::QRCode.new(cadena)
# With default options specified explicitly
    png = qrcode.as_png(
        resize_gte_to: false,
        resize_exactly_to: false,
        fill: 'white',
        color: 'black',
        size: 180,
        border_modules: 1,
        module_px_size: 6,
        file: nil # path to write
    )
    FileUtils.mkdir_p(ruta) unless File.directory?(ruta)

    File.open(ruta+nombre, 'wb') do |file|
      file.write(png.to_s)
    end
  end

end
