module DatatableHelper

  def fetch_data
    @data ||= fetch_records
  end

  def as_json(options = {})
    {
        data: data,
        recordsTotal: fetch_data.count,
        recordsFiltered: fetch_data.total_entries
    }
  end

  def filter(data_set)

    if params[:search][:value].present? && params[:columns].size > 0

      filter_query = ''
      params[:columns].each do |key, value|
        if value[:name].present? && value[:searchable].present? && value[:searchable] == 'true'

          if value[:name].include? '|'
            composite_column = value[:name].to_s.split('|')
            if composite_column.count > 1
              composite_column.each do |v|
                filter_query << v.to_s + ' LIKE :search OR '
              end
            end
          else
            filter_query << value[:name].to_s + ' LIKE :search OR '
          end
        end
      end

      filter_query = filter_query.first(-4)
      data_set = data_set.where(filter_query, search: "%#{params[:search][:value]}%")
    end

    #Case All registers
    if params['length'].to_i <= 0
      params['length'] = data_set.count
    end

    data_set = data_set.order("#{sort_column} #{sort_direction}").page(page).per_page(per_page)
    data_set

  end

  def sort_column
    s_columns = params[:columns][params[:order][:'0'][:column].to_s][:name].to_s.split('|')
    if s_columns.count > 1 #Si se tienen varias columnas, se tomara como prioridad de ordenamiento la primera.
      return s_columns.first
    else
      return params[:columns][params[:order][:'0'][:column].to_s][:name]
    end
  end

  def sort_direction
    return params[:order][:'0'][:dir]
  end

  def page
    params[:start].to_i/per_page + 1
  end

  def per_page
    params[:length].to_i > 0 ? params[:length].to_i : 10
  end

end
