module Admin::DoctorsHelper

  def save_picture_cam param
    if !@doctor.picture.nil?
      if !param.nil?
        uploaded_cam = param
        save_picture_captured uploaded_cam
      end
    else
      if !param.nil?
        uploaded_cam = param
        save_picture_captured uploaded_cam
      end
    end
  end

  def save_picture_captured picture64
    dir = Rails.root.join('uploads',@doctor.id.to_s)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)
    picture_name = @doctor.id.to_s+'.jpg'
    File.open(dir.join(picture_name), 'wb') do |file|
      image_data = Base64.decode64(picture64['data:image/png;base64,'.length .. -1])
      file.write(image_data)
    end
    @doctor.update(picture: dir.join(picture_name))
  end

  def find_doctor str
    doctors = []
    Doctor.joins(:user => :person).where("people.name like '%#{str}%' or people.last_name like '%#{str}%'").each do |c|
      doctors.push({id:c.id, value:c.user.person.name + " " +c.user.person.last_name})
    end
    doctors
  end


end
