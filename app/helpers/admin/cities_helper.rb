module Admin::CitiesHelper

  def find_city str
    cities = []
    City.find_by_name(str).limit(15).each do |c|
      cities.push({id:c.id, value:c.format_city_state_and_country})
    end
    cities
  end



end