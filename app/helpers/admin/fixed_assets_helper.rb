module Admin::FixedAssetsHelper
  def get_fixed_activities_response(params)

      activities = AssetActivity.where("fixed_asset_item_id = #{params[:fixed_asset_item_id]}")

    activities = activities.order('asset_activities.start_date ASC')

    result = []
    activities.each do |a|

        act = fullcalendar_activity a

      result.push(act)
    end
     result
  end

  def fullcalendar_activity a
    activity_color = a.fixed_asset_activity.color

=begin
    if a.activity_status_id == ActivityStatus::CONFIRM_PENDING
      activity_color = 'bgm-gray'
    elsif a.activity_status_id == ActivityStatus::CANCELED
      activity_color = 'bgm-bluegray'
    end
=end


    {
        :id => a.id,
        :title => a.title,
        :start => a.start_date,
        :end =>  a.end_date,
        :s_date => a.start_date.strftime('%Y-%m-%d %H:%M'),
        :e_date => a.end_date.strftime('%Y-%m-%d %H:%M'),
        :allDay => false,
        :color => activity_color,
        :details => a.details,
        :fixed_asset_id => a.fixed_asset_item.fixed_asset_id
    }
  end

end