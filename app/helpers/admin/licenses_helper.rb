module Admin::LicensesHelper

  def generate_activation_code(size = 8)
    charset = %w{0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z}
    (0...size).map{ charset.to_a[rand(charset.size)] }.join
  end

  def create_license params
    @license = License.create(start_date:params[:start_date],
                              end_date: params[:end_date],
                              status: License::ACTIVE,
                              current: true,
                              doctor_id: params[:doctor_id],
                              detail: params[:detail], distribution_type_id: params[:distribution_type])

    begin
      code = generate_activation_code
      record = License.where('code = ?',code).first
    end while !record.nil?

    #@license.save
    features_license = []
    features = JSON.parse(params[:features])
    if features.size > 0
      features.each do |f|
        license_feature = LicenseFeature.new
        license_feature.feature_id = f["license"].to_i
        license_feature.cost = f["price"].to_f
        features_license << license_feature
      end
    end

    Doctor.find(params[:doctor_id]).licenses.where.not(id:@license).update_all(status: License::ENDED, current: 0)

    if @license.update(license_features: features_license, code:code)
      {result: true}
    else
      {result: false, errors: @license.errors.full_messages}
    end
  end

  def update_license params
    license_features = [];
    features = JSON.parse(params[:features])

    if features.size > 0
      features.each do |f|

          lf = LicenseFeature.new
          lf.license_id = @license.id
          lf.feature_id = f["license"].to_i
          lf.cost = f["price"].to_f

        license_features.push(lf)
      end
    end

    license = License.find(params[:id])
=begin
    if  ActiveRecord::ConnectionAdapters::Column::TRUE_VALUES.include?(params[:current])
      current = 1
    else
      current = 0
    end
=end
    if license.update(start_date: params[:start_date], end_date: params[:end_date], status: params[:status], current: params[:current], distribution_type_id:params[:distribution_type].to_i, detail:params[:detail])
      license.license_features.destroy_all

      license.update(:license_features => license_features)
      {result:true}
    else
      {result:false, errors: license.errors.full_messages}
    end

  end

end