module ApplicationHelper
  def set_city_model params, modelo
    #se relaciona la ciudad
    if params[:city_id].blank?
      if !params[:city_name].blank?
        #intentar buscar la ciudad y relacionar lo
        search_city_array = search_city(params[:city_name])
        modelo.city_id = search_city_array.first[:id] if search_city_array.count > 0
      end
    end

  end

  def wicked_pdf_image_tag_for_public(img, options={})
=begin
    if img[0] == "/"
      # Remove the leading slash
      new_image = img.slice(1..-1)
      image_tag "file:///#{Rails.root.join('public',new_image).to_s.gsub("/", "\\")}"
    else
      image_tag "file://#{Rails.root.join('public', 'images', img).to_s.gsub("/", "\\")}}", options
    end
=end
    new_image = img.slice(1..-1)
    image_tag "file://#{Rails.root.join(new_image)}", options
  end

  def wicked_pdf_image_tag_no_asset(img, options={})
    image_tag "file://#{img}", options
  end

  def abbreviation_dr
    if current_user.is_doctor && current_user.person.gender.id == Gender::MAN
      "Dr."
    elsif current_user.is_doctor && current_user.person.gender.id == Gender::WOMEN
      "Dra."
    else
      ""
    end
  end

  def abbreviation_dr_by_doctor doctor
    if doctor.user.person.gender.id == Gender::MAN
      "Dr."
    else
      "Dra."
    end
  end
end
