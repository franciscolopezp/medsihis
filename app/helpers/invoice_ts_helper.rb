module InvoiceTsHelper

  def fiscal_catalog_path(fiscal_catalog)
    Rails.root.join('uploads')+"clinic_info/#{fiscal_catalog.id}"
  end

  def get_certificate_base64(cert_path)
    certificate =  %x'openssl x509 -inform DER -in #{cert_path}'
    certificate
  end

  def get_clean_certificate_base64(cert_path)
    certificate =  get_certificate_base64(cert_path)
    certificate_components =  certificate.split("\n")
    certificate_components.shift
    certificate_components.pop
    certificate_base_64 = certificate_components.join.to_s
    certificate_base_64
  end

  def get_cert_number(cert_path)
    cert =  %x'openssl x509 -serial -noout -inform DER -in #{cert_path}'
    data = cert.to_s.split('=')
    number_array = data[1].to_s.split(//)
    serial_number = []
    (1..39).step(2) do |n|
      serial_number.push(number_array[n])
    end
    serial_number.join.to_s
  end


  def generate_pem_file(key_file_path, password, output)
    resp = {
        :done => true,
        :message => "El archivo pem fue generado exitosamente"
    }
    console_output = %x'openssl pkcs8 -inform DER -in #{key_file_path} -passin pass:#{password}'
    if console_output.length > 3
      %x'openssl pkcs8 -inform DER -in #{key_file_path} -passin pass:#{password} > #{output}'
    else
      resp[:done] = false
      resp[:message] = "La contraseña del certificado es incorrecta"
    end
    resp
  end

  class InvoiceTs

    def initialize(data, files_path)
      @data = data
      @files_path = files_path
    end

    def stamp
      xml = generate_xml
      write_xml_base xml
      generate_original_string_file
      seal = get_seal
      @data[:stamp] = seal
      xml_with_stamp = generate_xml
      write_xml_base xml_with_stamp

      xml2 = File.read(@files_path[:xml_base])
      finkok_service = Finkok.new
      finkok_response = finkok_service.stamp xml2
      finkok_response[:transmitter_seal] = seal
      finkok_response
    end

    private
    def generate_xml
      xml = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.send(:'cfdi:Comprobante',
                 'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
                 'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
                 'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
                 'TipoCambio'=>"0.00",
                 'Moneda'=>"PESOS",
                 'version'=>"3.2",
                 'serie'=>@data[:serie],
                 'folio'=>@data[:folio],
                 'fecha'=>@data[:time].strftime('%Y-%m-%d')+"T"+@data[:time].strftime('%H:%M:%S'),
                 'sello'=>@data[:stamp].present? ? @data[:stamp] : '',
                 'formaDePago'=>@data[:payment_condition],
                 'noCertificado'=>@data[:certificate_number],
                 'certificado'=>@data[:certificate_base64],
                 'condicionesDePago'=>"Contado",
                 'subTotal'=>@data[:sub_total],
                 'descuento'=>"0.00",
                 'total'=>@data[:total],
                 'metodoDePago'=>@data[:payment_method],
                 'tipoDeComprobante'=>"ingreso",
                 'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
                 'LugarExpedicion'=>@data[:expedition_city] + ", " + @data[:expedition_state],
                 'NumCtaPago'=>@data[:account_number]){
          xml.send(:'cfdi:Emisor','rfc'=>@data[:transmitter][:rfc], 'nombre'=>@data[:transmitter][:business_name]){
            xml.send(:'cfdi:DomicilioFiscal','calle'=>@data[:transmitter][:address], 'noExterior'=>@data[:transmitter][:no_ext], 'noInterior'=>@data[:transmitter][:no_int], 'colonia'=>@data[:transmitter][:suburb], 'municipio'=>@data[:transmitter][:city], 'estado'=>@data[:transmitter][:state], 'pais'=>@data[:transmitter][:country], 'codigoPostal'=>@data[:transmitter][:zip]){
              xml.text('')
            }
            xml.send(:'cfdi:RegimenFiscal','Regimen'=>@data[:tax_regime])
          }
          xml.send(:'cfdi:Receptor','rfc'=>@data[:receiver][:rfc], 'nombre'=>@data[:receiver][:business_name]){
            xml.send('cfdi:Domicilio','calle'=>@data[:receiver][:address],'codigoPostal'=>@data[:receiver][:zip], 'noExterior'=>@data[:receiver][:no_ext], 'noInterior'=>@data[:receiver][:no_int],
                     'colonia'=>@data[:receiver][:suburb], 'municipio'=>@data[:receiver][:city],'pais'=>@data[:receiver][:country] )
          }
          xml.send(:'cfdi:Conceptos'){
            @data[:concepts].each do |c|
              xml.send(:'cfdi:Concepto','cantidad'=>c[:quantity], 'unidad'=>c[:unit_measure], 'noIdentificacion'=>"1", 'descripcion'=>c[:description], 'valorUnitario'=>c[:unit_price], 'importe'=>c[:total])
            end
          }
          xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>@data[:isr_rate], 'totalImpuestosTrasladados'=>@data[:isr_amount]){
            xml.send(:'cfdi:Traslados'){
              xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>@data[:iva_rate],'importe'=>@data[:iva_amount])
            }
          }
          xml.send(:'cfdi:Complemento')
        }
      end
      xml
    end

    def write_xml_base(xml)
      FileUtils.mkdir_p(@files_path[:root_path]) unless File.directory?(@files_path[:root_path])
      File.open(@files_path[:xml_base], 'wb') do |file|
        file.write(xml.to_xml)
      end
    end

    def generate_original_string_file
      %x' xsltproc #{@files_path[:xslt_file]} #{@files_path[:xml_base]} > #{@files_path[:original_string]}'
    end

    def get_seal
      stamp =  %x' openssl dgst -sha1 -sign #{@files_path[:pem_file]} #{@files_path[:original_string]} | openssl enc -base64'
      stamp_components = stamp.split("\n")
      s = stamp_components.join.to_s
      s
    end

  end

  class Finkok
    ERRORS_STAMP = {
        '300' => 'Usuario y contraseña inválidos.',
        '301' => 'XML mal formado.',
        '302' => 'Sello mal formado.',
        '303' => 'Sello no corresponde al emisor.',
        '304' => 'Certificado revocado o caduco.',
        '305' => 'Fecha de emisión no está dentro de la vigencia del Certificado.',
        '401' => 'Fecha y hora de generación fuera de rango.',
        '702' => 'El prefijo del namespace no es el especificado por el SAT.',
        '705' => 'Estructura de XML mal formada (Error de Sintaxis).',
        '708' => 'Error de conexión con el SAT.',
        '712' => 'El Certificado de timbre no se pudo recuperar del SAT.',
    }


    def register_rfc(rfc)
      client = Savon.client(wsdl: "#{ENV['finkok_url']}/registration.wsdl")
      client.operations
      response = client.call(:add, message: {
          :reseller__username => ENV['username'],
          :reseller__password => ENV['password'],
          :taxpayer__id => rfc  })
      response.body
    end

    def stamp (xml)
      finkok_config = FinkokConfig.first

      f_url = finkok_config.test_wsdl
      f_user = finkok_config.test_username
      f_pass = finkok_config.test_password

      if finkok_config.environment != "pruebas"
        f_url = finkok_config.prod_wsdl
        f_user = finkok_config.prod_username
        f_pass = finkok_config.prod_password
      end
      

      url = f_url + '/stamp.wsdl'
      client = Savon.client(wsdl: url, log_level: :error)

      msg = {
          xml: Base64::encode64(xml.gsub(/\n/, '')),
          username: f_user,
          password: f_pass
      }

      response = client.call :stamp, message: msg
      data = response.body[:stamp_response][:stamp_result]

      if data[:incidencias]
        error = data[:incidencias][:incidencia]
        error = error[0] unless !(error.is_a? Array)
        return ({
            :done => false,
            :error_code => error[:codigo_error],
            :message => ERRORS_STAMP[error[:codigo_error]]
        })
      else
        return ({
            :done => true,
            message: "1",
            uuid: data[:uuid],
            timbre: data[:sat_seal],
            no_certificado_sat: data[:no_certificado_sat],
            xml: data[:xml]
        })
      end
    end

    def cancel uuid, rfc, pem_file_path, certificate
      url = ENV['finkok_url'] + '/cancel.wsdl'
      uuid = [uuid] unless uuid.is_a? Array
      client = Savon.client(wsdl: url, log_level: :debug, log: false)
      uuid.map! do |u|
        u = "<tns:string>#{u}</tns:string>"
      end

      # Usamos xml derecho porque savon se caga con el wsdl
      xml = <<XML
<tns:UUIDS>
	<tns:uuids>
  #{uuid.join "\n"}
	</tns:uuids>
</tns:UUIDS>
<tns:username>#{ENV['finkok_username']}</tns:username>
<tns:password>#{ENV['finkok_password']}</tns:password>
<tns:taxpayer_id>#{rfc}</tns:taxpayer_id>
<tns:cer>#{Base64::encode64(certificate.to_s)}</tns:cer>
<tns:key>#{Base64::encode64(open(pem_file_path) { |io| io.read })}</tns:key>
XML

      response = client.call(:cancel, message: xml)
      data = response.hash[:envelope][:body][:cancel_response][:cancel_result]

      finkok_resp = {}
      if data[:folios]
        case data[:folios][:folio][:estatus_uuid]
          when '201'
            finkok_resp = {
                :done => true,
                :data => data[:acuse]
            }
          when '900'
            finkok_resp = {
                :done => false,
                :data => 'Error del PAC'
            }
          else
            finkok_resp = {
                :done => false,
                :data => data[:folios][:folio][:estatus_uuid]
            }
        end
      end

      finkok_resp
    end #end of cancel method

  end #end of Finkok Class



end