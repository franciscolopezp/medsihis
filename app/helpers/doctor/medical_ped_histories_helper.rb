module Doctor::MedicalPedHistoriesHelper

    # sacar todos los puntos X y Y de los percentiles para 3, 10, 25, 50, 75, 90, y 97, marcar todos los puntos como no visibles
    # sacar todos los puntos X y Y de por cada consulta que no sea mayor a 5 años,
    # por cada consulta sacar la edad que tenia en esa consulta y encontrar en que columna se encuentra (que percentil cae), marcar esos puntos como visibles
    # EJEMPLO
=begin
http://stackoverflow.com/questions/14642779/highcharts-how-can-i-turn-off-the-points
  http://jsfiddle.net/aeZ6P/

http://stackoverflow.com/questions/16061032/highcharts-series-data-array
http://jsfiddle.net/Cm3Ps/

http://jsfiddle.net/gh/get/jquery/1.9.1/highslide-software/highcharts.com/tree/master/samples/highcharts/demo/spline-irregular-time/

http://jsfiddle.net/gh/get/jquery/1.9.1/highslide-software/highcharts.com/tree/master/samples/highcharts/demo/spline-plot-bands/

flags - etiquetas en las graficas
http://www.highcharts.com/docs/getting-started/frequently-asked-questions#highstock-features-in-highcharts
=end

=begin
  { x = Edad
    y = Percentil

  data => {
      name: px
      data: [{x,y},...]
    },
    {
      name: px + n + 1
      data: [{x,y}...]
    }
Percentiles a mostrar
3, 10, 25, 50, 75, 90, y 97
=end
=begin
    Tabla de percentiles
      1 mes = 30.4375
      => 1 año = 365.25
=end
  def generate_date_graph(type_percentile, title_div, type_data, show_ticket = false)
      if @medical_expedient.patient.age <= 2
        # Tabla OMS
      result =  graph_oms type_percentile, title_div, show_ticket
      result[:subtitle_graph] = 'El rango de edad es de 1 a 5 años'
      else
        # Tabla OMS o CDC
        if type_data.to_i == TypePercentile::DATA_PERCENTILE_CDC
          # los tipos de percentiles pueden ser talla, imc, peso
          result =  graph_cdc type_percentile, title_div, show_ticket
          #result =  graph_cdc_combine title_div, show_ticket
          result[:subtitle_graph] = 'El rango de edad es de 2 a 20 años'
        else
          # los tipos de percentiles pueden ser talla, circunferencia de la cabeza, peso
          result =  graph_oms type_percentile, title_div, show_ticket
          result[:subtitle_graph] = 'El rango de edad es de 1 a 5 años'
        end
      end
    result
  end
  def generate_date_graph_combine(type_percentile, title_div, type_data, show_ticket = false)
    result =  graph_cdc_combine title_div, show_ticket
    result[:subtitle_graph] = 'El rango de edad es de 2 a 20 años'
    result
  end
  def graph_cdc_combine(title_div,show_ticket)
    result = {}
    gender = @medical_expedient.patient.gender.id
    all_data = []
    max_month_table = 240
    max_year = PercentileCdc.where("months <= ?", max_month_table).where(type_percentile: 3).where(gender: gender).order(:months)
    PercentileCdc::PERCENTILE_GRAPHIC.each do |p|
      all_data << {name:'P'+p.to_s, data: generate_point_graphic(max_year, 'p'+p.to_s, TypePercentile::DATA_PERCENTILE_CDC)}
    end
    max_year2 = PercentileCdc.where("months <= ?", max_month_table).where(type_percentile: 1).where(gender: gender).order(:months)
    PercentileCdc::PERCENTILE_GRAPHIC.each do |p|
      all_data << {name:'P'+p.to_s, data: generate_point_graphic_combine(max_year2, 'p'+p.to_s, TypePercentile::DATA_PERCENTILE_CDC)}
    end
    all_consultation = @medical_expedient.medical_consultations.where("date_consultation <= ?", @medical_expedient.patient.max_date_to_percentile_cdc).order(:date_consultation)

    one_year_percentile = 365.25
    data_flag = []
    data_patient = []
    all_consultation.each do |consu|
      point = {}
      # sacar los años que tenia en base a la fecha de la consulta
      age_in_day_consultation = consu.age_in_days_patient_in_consultation
      point_x = (age_in_day_consultation.to_f/one_year_percentile.to_f).round(4) # x
      point[:x] = point_x
          # unit = " cm"
          # point_y = (consu.vital_sign.height.nil?)? 0 : consu.vital_sign.height.to_f.round(4) # y
          unit = " kg"
          point_y = (consu.vital_sign.weight.nil?)? 0 : consu.vital_sign.weight.to_f.round(4) # y
      point[:y] = point_y
      point[:marker] = {enabled: true}
      data_patient << point
      text_title = (point_y.to_i == 0)? "N/C" : title_div+point_y.to_s+unit
      data_flag << {
          x: point_x,      # Point where the flag appears
          y: point_y,
          title: text_title, # Title of flag displayed on the chart
          text: 'Fecha: '+consu.date_consultation.strftime('%d/%m/%Y')   # Text displayed when the flag are highlighted.
      }
    end

    all_data << {name:'Peso', data: data_patient, id: 'patient_p'}
    data_flag2 = []
    data_patient2 = []
    all_consultation.each do |consu|
      point = {}
      # sacar los años que tenia en base a la fecha de la consulta
      age_in_day_consultation = consu.age_in_days_patient_in_consultation
      point_x = (age_in_day_consultation.to_f/one_year_percentile.to_f).round(4) # x
      point[:x] = point_x
      unit = " cm"
      point_y = ((consu.vital_sign.height.nil?)? 0 : consu.vital_sign.height.to_f.round(4)-30) # y
      point[:y] = point_y
      point[:marker] = {enabled: true}
      data_patient2 << point
      text_title = (point_y.to_i == 0)? "N/C" : title_div+point_y.to_s+unit
      data_flag2 << {
          x: point_x,      # Point where the flag appears
          y: point_y,
          title: text_title, # Title of flag displayed on the chart
          text: 'Fecha: '+consu.date_consultation.strftime('%d/%m/%Y')   # Text displayed when the flag are highlighted.
      }
    end
    all_data << {name:'Altura', data: data_patient2, id: 'patient_p'}

    all_data << {
        type: 'flags',
        name: 'Etiquetas',
        data: data_flag,
        onSeries: 'patient_p',
        shape: 'squarepin',
        width: 80,
        showInLegend: true,  # mostrar las etiquedas en la peleta de colores de menu
        color: '#95ceff', #same as onSeries
        fillColor:  '#ffffff',
        style: { # text style
                 color: 'black'
        },
        states: {
            hover: {
                fillColor: '#395C84' # darker
            }
        },
        visible: show_ticket
    }

    result[:data] = all_data
    puts result
    result
  end
  def graph_cdc(type_percentile, title_div, show_ticket)
    result = {}
    gender = @medical_expedient.patient.gender.id
    all_data = []
    max_month_table = 240
    max_year = PercentileCdc.where("months <= ?", max_month_table).where(type_percentile: type_percentile).where(gender: gender).order(:months)
    PercentileCdc::PERCENTILE_GRAPHIC.each do |p|
      all_data << {name:'P'+p.to_s, data: generate_point_graphic(max_year, 'p'+p.to_s, TypePercentile::DATA_PERCENTILE_CDC)}
    end

    all_consultation = @medical_expedient.medical_consultations.where("date_consultation <= ?", @medical_expedient.patient.max_date_to_percentile_cdc).order(:date_consultation)

    one_year_percentile = 365.25
    data_flag = []
    data_patient = []
    all_consultation.each do |consu|
      point = {}
      # sacar los años que tenia en base a la fecha de la consulta
      age_in_day_consultation = consu.age_in_days_patient_in_consultation
      point_x = (age_in_day_consultation.to_f/one_year_percentile.to_f).round(4) # x
      point[:x] = point_x
      case type_percentile
        when TypePercentile::SIZE_LENGTH
          unit = " cm"
          point_y = 0 # y
        when TypePercentile::WEIGHT
          unit = " kg"
          point_y =0 # y
        when TypePercentile::IMC
          unit = " Kg/m²"
          point_y = 0 # y
      end
      point[:y] = point_y
      point[:marker] = {enabled: true}
      data_patient << point
      text_title = (point_y.to_i == 0)? "N/C" : title_div+point_y.to_s+unit
      data_flag << {
          x: point_x,      # Point where the flag appears
          y: point_y,
          title: text_title, # Title of flag displayed on the chart
          text: 'Fecha: '+consu.date_consultation.strftime('%d/%m/%Y')   # Text displayed when the flag are highlighted.
      }
    end

    all_data << {name:'Paciente', data: data_patient, id: 'patient_p'}

    all_data << {
        type: 'flags',
        name: 'Etiquetas',
        data: data_flag,
        onSeries: 'patient_p',
        shape: 'squarepin',
        width: 80,
        showInLegend: true,  # mostrar las etiquedas en la peleta de colores de menu
        color: '#95ceff', #same as onSeries
        fillColor:  '#ffffff',
        style: { # text style
                 color: 'black'
        },
        states: {
            hover: {
                fillColor: '#395C84' # darker
            }
        },
        visible: show_ticket
    }
    result[:data] = all_data
    result
  end

  def graph_oms(type_percentile, title_div, show_ticket = false)
    result = {}

    gender = @medical_expedient.patient.gender.id

    all_data = []
    max_days_table = 1856
    max_five_year = PercentileChild.where("age_day <= ?", max_days_table).where(type_percentile: type_percentile).where(gender: gender).order(:age_day)
    PercentileChild::PERCENTILE_GRAPHIC.each do |p|
      all_data << {name:'P'+p.to_s, data: generate_point_graphic(max_five_year, 'p'+p.to_s, TypePercentile::DATA_PERCENTILE_OMS)}
    end
    all_consultation = @medical_expedient.medical_consultations.where("date_consultation <= ?", @medical_expedient.patient.max_date_to_percentile).order(:date_consultation)

    one_year_percentile = 365.25
    data_flag = []
    data_patient = []
    all_consultation.each do |consu|
      point = {}
      # sacar los años que tenia en base a la fecha de la consulta
      age_in_day_consultation = consu.age_in_days_patient_in_consultation
      point_x = (age_in_day_consultation.to_f/one_year_percentile.to_f).round(4) # x
      point[:x] = point_x
      case type_percentile
        when TypePercentile::SIZE_LENGTH
          unit = " cm"
          point_y = 0 # y
        when TypePercentile::WEIGHT
          unit = " kg"
          point_y = 0 # y
        when TypePercentile::HEAD_CIRCUMFERENCE
          unit = " cm"
          point_y = 0 # y
      end
      point[:y] = point_y
      point[:marker] = {enabled: true}
      data_patient << point
      text_title = (point_y.to_i == 0)? "N/C" : title_div+point_y.to_s+unit
      data_flag << {
          x: point_x,      # Point where the flag appears
          y: point_y,
          title: text_title, # Title of flag displayed on the chart
          text: 'Fecha: '+consu.date_consultation.strftime('%d/%m/%Y')   # Text displayed when the flag are highlighted.
      }
    end

    all_data << {name:'Paciente', data: data_patient, id: 'patient_p'}

    all_data << {
        type: 'flags',
        name: 'Etiquetas',
        data: data_flag,
        onSeries: 'patient_p',
        shape: 'squarepin',
        width: 80,
        showInLegend: true,  # mostrar las etiquedas en la peleta de colores de menu
        color: '#95ceff', #same as onSeries
        fillColor:  '#ffffff',
        style: { # text style
                 color: 'black'
        },
        states: {
            hover: {
                fillColor: '#395C84' # darker
            }
        },
        visible: show_ticket
    }
    result[:data] = all_data
    result
  end

  def generate_medical_ped_history medical_history
    if medical_history.perinatal_information == nil
      patient = medical_history.medical_expedient.patient
      perinatal_information = PerinatalInformation.new
      perinatal_information.medical_history_id = medical_history.id
      perinatal_information.save
      @perinatal_information = perinatal_information
    else
      @perinatal_information = medical_history.perinatal_information
    end

    if medical_history.child_non_pathological_info == nil
      child_non_pathological_info = ChildNonPathologicalInfo.new
      child_non_pathological_info.medical_history_id = medical_history.id
      child_non_pathological_info.save
      @child_non_pathological_info = child_non_pathological_info
    else
      @child_non_pathological_info = medical_history.child_non_pathological_info
    end
  end

  private
    def generate_point_graphic(array_percentile, column, type_data)
=begin
      Tabla de percentiles
      1 mes = 30.4375
      => 1 año = 365.25
=end
      one_year_percentile = 365.25
      one_month_percentile = 30.4375
      point_not_visible = []
      array_percentile.each do |row|
        if !row[column].nil?
          point = []
          if type_data.to_i == TypePercentile::DATA_PERCENTILE_OMS
            # las x se sacan por los dias entre un año
          point << (row.age_day.to_f/one_year_percentile.to_f).round(4) # x
          else
            # las x se sacan por los meses
            point_aux =  row.months.to_i.to_f * one_month_percentile
            if row.months.modulo(1) > 0
              # se suma medio mes (.5) de la tabla
              point_aux += one_month_percentile / 2.0
            end
            point << (point_aux.to_f/one_year_percentile.to_f).round(4) # x
          end
          point << row[column]  # y
          point_not_visible << point
        end
      end
      point_not_visible
  end
  def generate_point_graphic_combine(array_percentile, column, type_data)
=begin
      Tabla de percentiles
      1 mes = 30.4375
      => 1 año = 365.25
=end
    one_year_percentile = 365.25
    one_month_percentile = 30.4375
    point_not_visible = []
    array_percentile.each do |row|
      if !row[column].nil?
        row[column] = row[column] - 30
        point = []
        if type_data.to_i == TypePercentile::DATA_PERCENTILE_OMS
          # las x se sacan por los dias entre un año
          point << (row.age_day.to_f/one_year_percentile.to_f).round(4) # x
        else
          # las x se sacan por los meses
          point_aux =  row.months.to_i.to_f * one_month_percentile
          if row.months.modulo(1) > 0
            # se suma medio mes (.5) de la tabla
            point_aux += one_month_percentile / 2.0
          end
          point << (point_aux.to_f/one_year_percentile.to_f).round(4) # x
        end
        point << row[column] # y
        point_not_visible << point
      end
    end
    point_not_visible
  end
end