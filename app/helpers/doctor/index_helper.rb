module Doctor::IndexHelper
  include ActionView::Helpers::DateHelper

  # se utiliza para validar si el usuario doctor a aceptado los terminos y condiciones
  def validate_user_doctor
    if logged_in? and doctor?
      unless get_doctor_session.accept_term_conditions
        redirect_to doctor_index_path unless request.path == doctor_index_path
      end
    else
      redirect_to login_url unless assistant?
    end
  end

  # para validar si las licencias ya estan a punto de caducar
  def generate_message_notification_license

    licenses_active = get_doctor_session.licenses.where(status: License::ACTIVE)

    licenses_active.each do |license|
      diff_days = Date.today.mjd - license.end_date.mjd
      diff_days = (diff_days * -1) if diff_days < 0
      case diff_days.to_i
        when 5
          @title_notification = "Vigencia de licencia. "
          @message_notification = "Estimado usuario #{get_doctor_session.full_name} su licencia expirará en 5 días (#{I18n.localize(license.end_date,:format =>:patient)}), para continuar con los beneficios de Medsi le invitamos a renovar su licencia, comuníquese con su distribuidor o al teléfono 9991947595 "
          @type_notification = "info"

        when 3
          @title_notification = "Vigencia de licencia. "
          @message_notification = "Estimado usuario #{get_doctor_session.full_name} su licencia expirará en 3 días (#{I18n.localize(license.end_date,:format =>:patient)}), para continuar con los beneficios de Medsi le invitamos a renovar su licencia, comuníquese con su distribuidor o al teléfono 9991947595 "
          @type_notification = "warning"

        when 0
          @title_notification = "Vigencia de licencia. "
          @message_notification = "Estimado usuario #{get_doctor_session.full_name} su licencia expirará el día de hoy (#{I18n.localize(license.end_date,:format =>:patient)}), para continuar con los beneficios de Medsi le invitamos a renovar su licencia, comuníquese con su distribuidor o al teléfono 9991947595"
          @type_notification = "danger"

        else

      end
    end
    # get_doctor_session.licenses.each do |license|
    #   license.
    # end
  end
end