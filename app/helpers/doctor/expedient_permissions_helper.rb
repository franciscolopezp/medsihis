module Doctor::ExpedientPermissionsHelper

  def permission_view_patient_data_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::VIEW_PATIENT_DATA)
      else
        false
      end
  end

  def permission_edit_patient_data_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::EDIT_PATIENT_DATA)
    else
      false
    end
  end

  def permission_view_studies_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::VIEW_STUDIES)
    else
      false
    end
  end

  def permission_edit_studies_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::EDIT_STUDIES)
    else
      false
    end
  end

  def permission_view_medical_consultations_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::VIEW_MEDICAL_CONSULTATIONS)
    else
      false
    end
  end

  def permission_edit_medical_consultations_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::EDIT_MEDICAL_CONSULTATIONS)
    else
      false
    end
  end

  def permission_view_pregnancy_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::VIEW_PREGNANCY)
    else
      false
    end
  end

  def permission_edit_pregnancy_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::EDIT_PREGNANCY)
    else
      false
    end
  end

  def permission_view_observations_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::VIEW_OBSERVATIONS)
    else
      false
    end
  end

  def permission_edit_observations_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::EDIT_OBSERVATIONS)
    else
      false
    end
  end

  def permission_view_reviews?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::VIEW_REVIEWS)
    else
      false
    end
  end

  def permission_edit_general_data_medical_background_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::EDIT_GENERAL_DATA_MEDICAL_BACKGROUND)
    else
      false
    end
  end

  def permission_edit_vaccines_medical_background_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::EDIT_VACCINES_MEDICAL_BACKGROUND)
    else
      false
    end
  end

  def permission_edit_pediatrics_medical_background_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::EDIT_PEDIATRICS_MEDICAL_BACKGROUND)
    else
      false
    end
  end

  def permission_edit_gynecology_medical_background_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(ExpedientPermission::EDIT_GYNECOLOGY_MEDICAL_BACKGROUND)
    else
      false
    end
  end

  # valida que exista activado un permiso para los antecedentes medicos (datos generales, vacunas, etc.)
  def permission_medical_background_current_user?
    if doctor?
      true
    elsif admin?
      true
    elsif assistant?
      get_assistant_session.expedient_permissions.exists?(id: [ExpedientPermission::EDIT_GENERAL_DATA_MEDICAL_BACKGROUND,
                                                           ExpedientPermission::EDIT_VACCINES_MEDICAL_BACKGROUND,
                                                           ExpedientPermission::EDIT_PEDIATRICS_MEDICAL_BACKGROUND,
                                                           ExpedientPermission::EDIT_GYNECOLOGY_MEDICAL_BACKGROUND])
    else
      false
    end
  end

  end