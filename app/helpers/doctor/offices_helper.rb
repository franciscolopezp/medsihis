module Doctor::OfficesHelper
  def create_office params
    office = Office.new(logo: params[:logo])
    office.name = params[:name]
    office.address = params[:address]
    office.telephone = params[:telephone]
    office.consult_duration = params[:consult_duration]
    office.city_id = set_city_office params
    office.lat = params[:lat]
    office.lng = params[:lng]
    office.hospital_id = params[:hospital_id]
    office.active = true
    office.folio = params[:folio].to_i


    office.save

    create_schedule(office.id,params)

  end

  def update_office params
    office = Office.find(params[:id])
    city_id = set_city_office params
    if office.update(:name => params[:name],
                  :address => params[:address],
                  :telephone => params[:telephone],
                  :consult_duration => params[:consult_duration],
                  :logo => params[:logo],
                  :city_id => city_id,
                  :lat => params[:lat],
                  :lng => params[:lng],
                  :hospital_id => params[:hospital_id], :folio => params[:folio].to_i)

      if !params[:delete_logo].nil?
        #eliminar logo
        office.remove_logo!
        office.save
      else
        if !params[:logo].nil?
          office.remove_logo!
          office.save
          # agregar nuevo logo
          office.update(:logo => params[:logo])
        end
      end

      if params[:update_schedule] == "1"
        create_schedule(params[:office_id],params)
      end
      {result: true}
    else

      {result: false, errors: office.errors.full_messages}
    end
  end

  def set_city_office params
    #se relaciona la ciudad
    if params[:city_id].blank?
      if !params[:city_name].blank?
        #intentar buscar la ciudad y relacionar lo
        search_city_array = find_city(params[:city_name])
        city_id = search_city_array.first[:id] if search_city_array.count > 0
      end
      city_id
    else
      params[:city_id]
    end
  end

  def create_schedule(office_id,params)
    OfficeTime.where(office_id: office_id).destroy_all
    days = params[:days]
    days.each do |days_aux,k|
      d = JSON.parse(days_aux)
      times = d['times']
      times.each do |t,a|
        office_time = OfficeTime.new
        office_time.start = Time.parse(t['start'])
        office_time.end = Time.parse(t['end'])
        office_time.office_id = office_id
        office_time.day = d['day']
        office_time.save
      end
    end
  end

  def schedule_has_day(office,day)
    office.office_times.each do |ot|
      if ot.day == day
        return true
      end
    end
    return false
  end

  def schedule_by_day(office,day)
    schedules = []
    office.office_times.each do |ot|
      if ot.day == day
        schedules.push(ot)
      end
    end
    return schedules
  end

end