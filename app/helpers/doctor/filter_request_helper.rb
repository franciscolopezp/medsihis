module Doctor::FilterRequestHelper
  def filter_medical_expedient
    unless get_doctor_session.medical_expedients.exists?(id:params[:id])
      redirect_to url_for(:controller => "doctor/medical_expedients", :action => "index")
    end
  end

  def filter_medical_consultations
    #unless get_doctor_session.medical_consultations.exists?(id:params[:id])
     # redirect_to url_for(:controller => "doctor/medical_expedients", :action => "index")
    #end
  end

  def filter_daily_cash

    if session[:role] == 'doctor' || session[:role] == 'assistant'
      unless get_doctor_session.daily_cashes.exists?(id:params[:id])
        redirect_to url_for(:controller => "doctor/dashboard", :action => "daily_cashes")
      end
    elsif session[:role] == 'shared_assistant'
      unless get_assistant_session.user.daily_cashes.exists?(id:params[:id])
        redirect_to url_for(:controller => "shared_assistant/index", :action => "index")
      end
    end


  end

  def filter_patient
    unless get_doctor_session.patients.exists?(id:params[:id])
      redirect_to url_for(:controller => "doctor/patients", :action => "index")
    end
  end

  def filter_assistant
    unless get_doctor_session.assistants.exists?(id:params[:id])
      redirect_to url_for(:controller => "doctor/assistants", :action => "index")
    end
  end

  def filter_account_bank
    #unless get_doctor_session.accounts.exists?(id:params[:id])
     # redirect_to url_for(:controller => "doctor/accounts", :action => "index")
    #end
  end

  def filter_invoice

  end

  def filter_print_analysis_order
    #unless get_doctor_session.clinical_analysis_orders.exists?(id:params[:id])
     # redirect_to url_for(:controller => "doctor/medical_expedients", :action => "index")
    #end
  end

  def filter_office
    #unless get_doctor_session.offices.exists?(id:params[:id])
     # redirect_to url_for(:controller => "doctor/offices", :action => "index")
    #end
  end

  def filter_personal_support

    if session[:doctor_id]!=nil
      unless get_doctor_session.personalsupports.exists?(id:params[:id])
        redirect_to url_for(:controller => "doctor/personalsupports", :action => "index")
      end
    else
      itbelongs = true;
      asistant = Assistant.find(session[:assistant_id])
      asistant.doctors.each do |doc|
        if doc.personalsupports.exists?(id:params[:id])
          itbelongs = false
          break
        end
      end
      if itbelongs
        redirect_to url_for(:controller => "shared_assistant/personalsupports", :action => "index")
      end
    end

  end

  def filter_patient_assistant_shared
    exists = false
    get_doctors_active_assistant_shared.each do |doctor|
      exists = doctor.patients.exists?(id:params[:id])
      break if exists
    end
    unless exists
      redirect_to url_for(:controller => "shared_assistant/patients", :action => "index")
    end

  end


end