module Doctor::DashboardHelper
  def get_activities_waiting_room params
    query = "activities.doctor_id = "+ session[:doctor_id].to_s;

    if params[:office_id] != nil && params[:office_id].kind_of?(Array)
    query += " AND activities_offices.office_id IN ("+params[:office_id].join(",")+") ";
    end

    month = params[:month].to_i
    month += 1
    if month < 10
      month = "0"+month.to_s
    end

    start_date = Date.parse(params[:year]+"-"+month.to_s+"-"+params[:date])
    end_date = start_date + 1.day

    query += " AND (activities.start_date >= '"+start_date.to_s+"'"
    query += " AND activities.start_date < '"+end_date.to_s+"')"
    query += "AND activities.activity_status_id = " +ActivityStatus::WAITING.to_s


    activities = Activity.joins(:offices).where(query).distinct
    result = []
    activities.each do |a|
      act = format_activity_wroom a
      result.push(act)
    end
    return result

  end

  def format_activity_wroom a
    patient_name = ""
    patient_image = ''
    if a.patients.size == 1
      p = a.patients.first
      patient_name = p.name+" "+p.last_name
      if p.picture != nil && p.picture != ''
        patient_image = download_image_doctor_patients_path p.id
      end
    end
    {
        :id => a.id,
        :name => a.name,
        :details => a.details,
        :start_date => a.start_date,
        :end_date => a.end_date,
        :patient => patient_name,
        :picture => patient_image
    }
  end

  def get_pending_consults(user)
    offices = user.offices
    consults = []
    medical_consultations = MedicalConsultation.where('office_id IN (?)',offices.map{|o| o.id}).where('price > 0').where('is_charged = 0')
    medical_consultations.each do |c|
      consults.push(format_pending_consult  c)
    end
    consults
  end

  def get_pending_reports office_id
    consults = []
    if session[:role] == 'assistant' || session[:role] == 'shared_assistant'
      medical_consultations = CabinetReport.pending_by_offices office_id
    else
      medical_consultations = CabinetReport.pending_by_doctor session[:doctor_id]
    end
    medical_consultations.each do |c|
      consults.push(format_pending_report  c)
    end
    consults
  end

  def format_pending_consult c
    {
        :id => c.id,
        :patient => c.medical_expedient.patient.full_name,
        :patient_id => c.medical_expedient.patient.id,
        :date => c.date_consultation.strftime('%Y-%m-%d %H:%M'),
        :price => c.price,
        :charged => c.get_total,
        :debt => c.debt,
        :user_id => c.user.id,
        :balance => (c.price - c.get_total)
    }
  end

  def format_pending_report r
    {
        :id => r.id,
        :patient => r.clinical_analysis_order.medical_expedient.patient.full_name,
        :patient_id => r.clinical_analysis_order.medical_expedient.patient.id,
        :date => r.created_at.strftime('%Y-%m-%d %H:%M'),
        :price => r.price,
        :study => r.clinical_analysis_order.clinical_studies.first.name,
        :doctor_id => r.clinical_analysis_order.medical_expedient.doctor_id,
         :balance => (r.price - r.get_total)
    }
  end

end