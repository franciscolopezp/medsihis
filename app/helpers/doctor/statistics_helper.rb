module Doctor::StatisticsHelper
  def consultations_chart_data(start = 3.weeks.ago)

    consults_a_day = MedicalConsultation.total_grouped_by_day_auto(start, session[:doctor_id])
    (start.to_date..Date.today).map do |date|
      if consults_a_day[date] != nil
        price = consults_a_day[date].first.try(:total_price) || 0
        consults = consults_a_day[date].first.try(:consults) || 0
      else
        price = 0
        consults = 0
      end
      {
          date_consultation: date,
          price: price,
          consults: consults,
      }
    end

  end
  def transactions_chart_data(start = 3.weeks.ago)
    transactions_a_day = Transaction.total_grouped_by_day_auto(start,session[:doctor_id])
    (start.to_date..Date.today).map do |date|
      if transactions_a_day[date] != nil
        egreso = transactions_a_day[date].first.try(:egreso) || 0
        total = transactions_a_day[date].first.try(:total) || 0
        entry = transactions_a_day[date].first.try(:ingreso) || 0
        transacs = transactions_a_day[date].first.try(:transacs) || 0
      else
        egreso = 0
        entry = 0
        total = 0
        transacs = 0
      end
      {
          date: date,
          egreso: egreso,
          entry: entry,
          total: total,
          transacs: transacs,
      }
    end
  end

  def comparative_ingress_chart(start = 3.weeks.ago)
    transactions_a_day = Transaction.total_grouped_by_day_auto(start,session[:doctor_id])
    (start.to_date..Date.today).map do |date|
      if transactions_a_day[date] != nil
        egreso = transactions_a_day[date].first.try(:egreso) || 0
        total = transactions_a_day[date].first.try(:total) || 0
        entry = transactions_a_day[date].first.try(:ingreso) || 0
        transacs = transactions_a_day[date].first.try(:transacs) || 0
      else
        egreso = 0
        entry = 0
        total = 0
        transacs = 0
      end
      {
          date: date,
          egreso: egreso,
          entry: entry,
          total: total,
          transacs: transacs,
      }
    end
  end

end
