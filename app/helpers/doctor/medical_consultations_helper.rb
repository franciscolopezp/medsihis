module Doctor::MedicalConsultationsHelper

  def has_template_office
    if !params[:office_id].nil?
      office = Office.find(params[:office_id])

    else if !params[:consultation_id].nil?
           consultation = MedicalConsultation.find(params[:consultation_id])
           office = consultation.office
         end
    end

    if !office.nil?  or !office.template_prescription.nil?
      true
    else
      false
    end
  end

  def generate_imc(patient, height = 0, weight = 0)

    if patient.age_month > ImcChildrenMax19Year::END_TABLE_IMC_TO_CHILDREN
      # el paciente es adulto, se toma la tabla IMC para adultos
      imc = calculation_imc height, weight
      imc_aux = nil
      ImcAdult.all.each do |imc_adult|
        if imc_adult.rank_imc_adult.id == RankImcAdult::MIN
          max_imc = imc_adult.associate
          if !max_imc.nil?
            if imc_adult.imc <= imc and imc <= max_imc.imc
              # estan en el rango de minimo y maximo de los IMC
              # calcular en que columna se encuentra mas cerca, si es un min o max
              diff_min = (imc_adult.imc - imc).round(1)
              diff_max = (max_imc.imc - imc).round(1)
              diff_max = (-1 * diff_max) if diff_max < 0
              if diff_min >= diff_max
                # lado derecho (max)
                imc_aux = max_imc
              else
                # lado izquiero (min)
                imc_aux = imc_adult
              end
              break;
            end
          else
            # no tiene maximo, por lo tanto es la ultima columna de IMC
            if imc >= imc_adult.imc
              # cae en la ultima columna, obesidad grado III
              imc_aux = imc_adult
            end
          end
        end
      end

      if !imc_aux.nil?
        #se encontro el valor en la tabla de IMC
        table_imc = generate_table_imc_adult imc_aux,height, weight
        {result: true, table: table_imc[:table], imc_real: imc, suggested_weight: table_imc[:suggested_weight_min].to_s+" - "+table_imc[:suggested_weight_max].to_s, flag_children:false}
      else
        {result: false}
      end
    else
      # el paciente es menor, se toma la tabla del menor
      if patient.age_month >= ImcChildrenMax19Year::START_TABLE_IMC_CHILDREN_IN_MONTH
        # los datos son en IMC
        data_table_imc = generate_table_imc_children patient.gender, patient.age_month, height, weight
        {result: true, table: data_table_imc[:table], imc_real: data_table_imc[:imc_real], suggested_weight: data_table_imc[:suggested_weight], gender: data_table_imc[:gender], flag_children:true}
      else
        # los datos son en Kg
        data_table_imc = generate_table_kg_children patient.gender, patient.age_month, height, weight
        {result: true, table: data_table_imc[:table], imc_real: data_table_imc[:imc_real], suggested_weight: data_table_imc[:suggested_weight], gender: data_table_imc[:gender], flag_children:true}
      end
    end
  end

  def colors_header_table_adult index
    color = ""
    case index.to_i
      when 0
        color = "#00FFFF"

      when 1
        color = "#31B404"

      when 2
        color = "#8A4B08"

      when 3
        color = "#DF0101"

      when 4
        color = "#DF0101"

      when 5
        color = "#DF0101"

    end
    color
  end

  def colors_min_max index
    case index.to_i
      when 0
        color = "#31B404"
      else
        if index%2 == 0
          color = "#F7819F"
        else
          color = "#F2F5A9"
        end
    end
    color
  end

  def colors_value_row index
    case index.to_i
      when 0
        color = "#CEF6E3"
      when 1
        color = "#31B404"
      when 2
        color = "#31B404"
      when 3
        color = "#8A4B08"
      when 4
        color = "#8A4B08"
      else
        color = "#DF0101"
    end
    color
  end

  def color_header_table_children index
    color = ""
    case index.to_i
      when 0
        color = "#00FFFF"
      when 1
        color = "#F2F5A9"
      when 2
        color = "#31B404"
      when 3
        color = "#F2F5A9"
      when 4
        color = "#DF0101"
    end
    color
  end


  private
    def calculation_imc(height, weight)
      m = height.to_f * height.to_f
      imc = weight/m
      imc.round(1)
    end

    def calculation_weight(imc, height)
      square_meter = height * height
      weight = square_meter * imc
      weight.round(1)
    end

    def generate_table_kg_children(gender,age_month,height,weight)
      data_imc = {}
      table = []
      column = []
      column << {value: "EDAD"}
      column << {value: "RIESGO DE DESNUTRICIÓN"}
      column << {value: "NORMAL"}
      column << {value: "SOBREPESO"}
      column << {value: "OBESIDAD"}

      table << column

      gender_id = gender.id

      data_imc[:gender] = gender_id
      if age_month >= ImcChildrenMax19Year::START_TABLE_TWO_KG_CHILDREN_IN_MONTH
        # cae entre 1 año y 5 años
        childrenKgArray = ImcChildrenMax19Year.where("gender_id =?",gender_id).where("age_month >= ?",ImcChildrenMax19Year::START_TABLE_TWO_KG_CHILDREN_IN_MONTH)
                               .where("age_month < ?", ImcChildrenMax19Year::START_TABLE_IMC_CHILDREN_IN_MONTH)
      else
        #cae entre 0 meses y 11 meses
        childrenKgArray = ImcChildrenMax19Year.where("gender_id =?",gender_id).where("age_month >= ?",ImcChildrenMax19Year::START_TABLE_KG_CHILDREN_IN_MONTH)
                               .where("age_month < ?", ImcChildrenMax19Year::START_TABLE_TWO_KG_CHILDREN_IN_MONTH)
      end

      data_imc_aux = generate_row_table_children weight, childrenKgArray, age_month, height, true

      data_imc[:table] = data_imc_aux[:table]
      data_imc[:imc_real] = weight
      data_imc[:suggested_weight] = data_imc_aux[:suggested_weight]
      data_imc
    end

    def generate_table_imc_children(gender,age_month,height,weight)
        imc_real = calculation_imc height,weight
        data_imc = {}

        gender_id = gender.id

        data_imc[:gender] = gender_id
        if age_month > ImcChildrenMax19Year::END_TABLE_IMC_CHILDREN_IN_MONTH
          # cae entre 9 años 6 se meses y 19 años
          childrenImcArray = ImcChildrenMax19Year.where("gender_id =?",gender_id).where("age_month > ?",ImcChildrenMax19Year::END_TABLE_IMC_CHILDREN_IN_MONTH)
          .where("age_month <= ?", ImcChildrenMax19Year::END_TABLE_IMC_TO_CHILDREN)
        else
          # cae entre 5 años 6 se meses y 9 años 6 meses
          childrenImcArray = ImcChildrenMax19Year.where("gender_id =?",gender_id).where("age_month >= ?",ImcChildrenMax19Year::START_TABLE_IMC_CHILDREN_IN_MONTH)
                                 .where("age_month <= ?",ImcChildrenMax19Year::END_TABLE_IMC_CHILDREN_IN_MONTH)
        end

        data_imc_aux = generate_row_table_children imc_real, childrenImcArray, age_month, height

        data_imc[:table] = data_imc_aux[:table]
        data_imc[:imc_real] = imc_real
        data_imc[:suggested_weight] = data_imc_aux[:suggested_weight]
        data_imc
    end

    def generate_row_table_children(value, array_table_children, age_month, height, is_kg = false)
        data_result = {}
        table = []
        column = []
        column << {value: "EDAD"}
        column << {value: "RIESGO DE DESNUTRICIÓN"}
        column << {value: "NORMAL"}
        column << {value: "SOBREPESO"}
        column << {value: "OBESIDAD"}

        table << column
        array_table_children.each do |imc|
          column = []
          selected_flag = false
          if age_month.to_i == imc.age_month.to_i
            # el niño cae en este row de la tabla IMC o en esta iteración
            flag_age = true
            column << {value: format_age_month(imc.age_month), selected: true}

            # valor sugerido
            if is_kg
              data_result[:suggested_weight] = imc.normal
            else
              data_result[:suggested_weight] = calculation_weight imc.normal,height
            end

          else
            column << {value: format_age_month(imc.age_month)}
          end

          selected_flag = true if !flag_age.nil? and flag_age
          if value <= imc.danger_malnutrition
            # riesgo de desnutricion
            column << {value: 8804.chr+imc.danger_malnutrition.to_s, selected: selected_flag}
          else
            column << {value: 8804.chr+imc.danger_malnutrition.to_s}
          end
          if value > imc.danger_malnutrition and value < imc.overweight
            #normal
            column << {value: imc.normal, selected: selected_flag}
          else
            column << {value: imc.normal}
          end

          if value >= imc.overweight and value < imc.obesity
            # sobrepeso
            column << {value: imc.overweight.to_s+8805.chr, selected: selected_flag}
          else
            column << {value: imc.overweight.to_s+8805.chr}
          end

          if value >= imc.obesity
            #obesidad
            column << {value: imc.obesity.to_s+8805.chr, selected: selected_flag}
          else
            column << {value: imc.obesity.to_s+8805.chr}
          end

          flag_age = nil
          table << column
        end
        data_result[:table] = table
        data_result
    end

      def format_age_month age_month
        if age_month == 0
          "Al nacer"
        else
          year = age_month/12
          month = age_month%12
          if month.to_i > 0
            if year.to_i > 0
              year.to_s+" años "+month.to_s+" meses"
            else
              month.to_s+" meses"
            end
          else
            year.to_s+" años "
          end
        end
      end

      def generate_table_imc_adult(imc_selected, height, weight)
          center_height = height.to_f.round(2)
          data_table_imc = {}
          table = []
          column = []
          column << {value: "PESO"}
          CategoryImc.all.order(:id).each do |category|
            column << {value: category.name}
          end
          table << column
          column = []
          column << {value: "IMC"}
          imc_adult_array = ImcAdult.all.order(:imc)
          imc_adult_array.each do |imc_a|
            column << {value: imc_a.imc}
          end
          table << column
          column = []
          column <<  {value: "Estatura"}
          rank_array = RankImcAdult.all.order(:id)
          (0..3).each do |v|
            rank_array.reverse_each do |rank|
              column << {value: rank.name}
            end
          end
          column << {value: RankImcAdult.second.name}
          table << column
          # datos de la tabla
          # este while son todos los datos mayores a 1.44 y menores que el del paciente
          center_height = center_height - 0.02
          count_row = 0
          columns_aux = []
          while center_height >= 1.44 and count_row < 3 #numero maximo de rows para arriba
            column = []
            column << {value: center_height.round(2)} # este es la altura, primera columna
            imc_adult_array.each do |imc_adult|
              cell_weight = calculation_weight imc_adult.imc, center_height
              column << {value: cell_weight}
            end
            center_height = center_height - 0.02
            columns_aux << column
            count_row = count_row + 1
          end

          columns_aux.reverse_each do |co|
          table << co
          end

          #este dato es el marcado como el peso del paciente.
          column = []
          column << {value: height.to_f.round(2), selected: true}
          # se generar la estructura de la tabla, y se marca cual celda debe estar marcada para el paciente
          imc_adult_array.each do |imc_adult|
            if imc_adult.id == imc_selected.id #cae en la misma columna IMC

              column << {value: weight.round(1), selected: true}
              cell_weight = calculation_weight imc_adult.imc, height.to_f.round(2)

              # esta condicion indica que debe tomar el peso sugerido si el imc cae en normal y el calculo del peso es menor al indicado en la tabla ya
              # establecido
              cell_weight = weight.round(1) if imc_adult.category_imc.id == CategoryImc::NORMAL and imc_adult.rank_imc_adult.id == RankImcAdult::MIN or imc_adult.rank_imc_adult.id == RankImcAdult::MAX and cell_weight.to_f.round(1) < weight.round(1)

            else
              cell_weight = calculation_weight imc_adult.imc, height.to_f.round(2)

              #data_table_imc[:suggested_weight_min] = cell_weight if imc_adult.category_imc.id == CategoryImc::NORMAL and imc_adult.rank_imc_adult.id == RankImcAdult::MIN # determina cual es el rango de peso sugerido
              #data_table_imc[:suggested_weight_max] = cell_weight if imc_adult.category_imc.id == CategoryImc::NORMAL and imc_adult.rank_imc_adult.id == RankImcAdult::MAX # determina cual es el rango de peso sugerido
              column << {value: cell_weight}
            end

            data_table_imc[:suggested_weight_min] = cell_weight if imc_adult.category_imc.id == CategoryImc::NORMAL and imc_adult.rank_imc_adult.id == RankImcAdult::MIN # determina cual es el rango de peso sugerido
            data_table_imc[:suggested_weight_max] = cell_weight if imc_adult.category_imc.id == CategoryImc::NORMAL and imc_adult.rank_imc_adult.id == RankImcAdult::MAX # determina cual es el rango de peso sugerido

          end
          table << column
          #este while son todos los datos mayores al del paciente hasta 1.84 que es el maximo en la tabla
          center_height = height.to_f.round(2)
          center_height = center_height + 0.02
          count_row = 0
          while center_height <= 1.84 and count_row < 3 #numero maximo de rows para abajo
            column = []
            column << {value: center_height.round(2)} # este es la altura, primera columna
            imc_adult_array.each do |imc_adult|
              cell_weight = calculation_weight imc_adult.imc, center_height
              column << {value: cell_weight}
            end
            table << column
            center_height = center_height + 0.02
            count_row = count_row + 1
          end
          data_table_imc[:table] = table
          data_table_imc
      end
end
