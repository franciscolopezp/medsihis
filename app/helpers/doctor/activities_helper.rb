module Doctor::ActivitiesHelper
  def json_from_model a
    office_id = []
    personal = []
    office_name = "Todos los consultorios"
    patient_id = ""
    patient_name = ""
    expedient_id = 0
    user_patient_phone = ""
    activity_color = a.activity_type.color
    a.offices.active_offices.each do |o|
      office_id.push(o.id)
    end

    if a.offices.active_offices.size == 1
      office_name = a.offices[0].name
    end

    a.personalsupports.each do |ps|
      personal.push({
          'full_name' => ps.name+' '+ps.lastname,
          'role' => ps.operation_role.name,
          'id' => ps.id,
          'email' => ps.email
                    })
    end

    if a.patients.size == 1
      p = a.patients[0]
      patient_id = p.id
      patient_name = p.full_name
      expedient_id = p.medical_expedient.id
    end

    if a.user_patients.size == 1
      user_patient_phone = a.user_patients.first.phone
    end

    if a.activity_status_id == ActivityStatus::CONFIRM_PENDING
      activity_color = 'bgm-gray'
    elsif a.activity_status_id == ActivityStatus::CANCELED
      activity_color = 'bgm-bluegray'
    end

    {
        :first_office_name => a.offices.first.present? ? a.offices.first.name : "Todos",
        :doctor_id => a.doctor_id,
        :id => a.id,
        :personal_supports => a.personal_supports_ids,
        :title => a.name,
        :details => a.details,
        :s_date => a.start_date.strftime('%Y-%m-%d %H:%M'),
        :e_date => a.end_date.strftime('%Y-%m-%d %H:%M'),
        :allDay => a.all_day,
        :office_id => office_id,
        :office_name => office_name,
        :className => activity_color,
        :color => a.activity_type.color, #used in edit feature of activity, we are rendering activity with gray if it is CANCELED we need the original color
        :activity_type_id => a.activity_type.id,
        :activity_type_name => a.activity_type.name,
        :activity_status_id => a.activity_status.id,
        :activity_status_name => a.activity_status.name,
        :patient_id => patient_id,
        :patient_name => patient_name,
        :owner_type => a.owner_type,
        :owner_id => a.owner_id,
        :expedient_id => expedient_id,
        :personal => personal,
        :user_patient_phone => user_patient_phone
    }
  end


  def fullcalendar_activity a
    office_name = "Todos los consultorios"
    activity_color = a.activity_type.color
    if a.offices.active_offices.size == 1
      office_name = a.offices[0].name
    end

    if a.activity_status_id == ActivityStatus::CONFIRM_PENDING
      activity_color = 'bgm-gray'
    elsif a.activity_status_id == ActivityStatus::CANCELED
      activity_color = 'bgm-bluegray'
    end


    {
        :id => a.id,
        :title => a.name,
        :start => a.start_date,
        :end => a.all_day ? (a.end_date + 1.day) : a.end_date,
        :s_date => a.start_date.strftime('%Y-%m-%d %H:%M'),
        :e_date => a.end_date.strftime('%Y-%m-%d %H:%M'),
        :allDay => a.all_day,
        :className => activity_color,
        :office_name => office_name,
        :doctor_id => a.doctor_id
    }
  end

  def get_query_activities params
    doc_id = session[:doctor_id] != nil ? session[:doctor_id].to_s : params[:doctor_id]
    query = "activities.doctor_id = " + doc_id


    month = params[:month].to_i
    month += 1
    if month < 10
      month = "0"+month.to_s
    end

    date = "01"
    if params[:wroom_page] != nil #in waiting_room page we ask for specific date
      date = params[:date].to_i
      if date < 10
        date = "0"+date.to_s
      end

      if params[:type] != nil
        query += " AND activities.activity_type_id = "+params[:type]
      end
    end

    start_date = Date.parse(params[:year]+"-"+month.to_s+"-"+date.to_s)

    if params[:wroom_page] != nil #in waiting_room page we ask for specific date not all the month
      end_date = start_date + 1.day
    else
      end_date = start_date + 1.month
    end

    query += " AND ((activities.start_date >= '"+start_date.to_s+"'"
    query += " AND activities.start_date < '"+end_date.to_s+"')"
    query += " OR ( activities.end_date >= '"+start_date.to_s+"'"
    query += " AND activities.end_date < '"+end_date.to_s+"' ))"

    return query
  end

  def get_offices params
    offices = []
    if params[:office_id].present?
      offices =[Office.find(params[:office_id])]
    end
    return offices
  end

  def get_patients params
    patients = []
    if params[:patient_id] != nil && params[:patient_id] != ''
      patients.push(Patient.find(params[:patient_id]))
    end

    return patients
  end

  # retorna un json, :result = true/false, si es true retorna otro valor :activity => es la actividad con la que choca la fecha
  def exist_rank_date(start_date,end_date,doctor,activity_id)
    act_id = activity_id.to_i
    result = {}
    if activity_id.to_i > 0
      activities_filter = Activity.where(doctor_id: doctor.id).where("id not in ( #{act_id} )")
    else
      activities_filter = Activity.where(doctor_id: doctor.id)
    end

    activities_filter = activities_filter.where('activity_status_id != ?',ActivityStatus::CANCELED)
    activities_range_start = activities_filter.where("DATE_FORMAT(start_date, '%Y-%m-%d') <= DATE_FORMAT('#{start_date}', '%Y-%m-%d')
    and DATE_FORMAT(end_date, '%Y-%m-%d') > DATE_FORMAT('#{start_date}', '%Y-%m-%d')")

    activities_range_end = activities_filter.where("DATE_FORMAT(start_date, '%Y-%m-%d') <= DATE_FORMAT('#{end_date}', '%Y-%m-%d')
    and DATE_FORMAT(end_date, '%Y-%m-%d') > DATE_FORMAT('#{end_date}', '%Y-%m-%d')")
    if activities_range_start.count == 0 && activities_range_end.count == 0
    activities = activities_filter.where("DATE_FORMAT(start_date, '%Y-%m-%d') = DATE_FORMAT('#{start_date}', '%Y-%m-%d')")
    if activities.count > 0
      activities = activities.where("DATE_FORMAT(DATE_ADD('#{start_date}',INTERVAL 1 minute),'%H:%i:%s')  between DATE_FORMAT(start_date,'%H:%i:%s') and DATE_FORMAT(end_date,'%H:%i:%s')")
      if activities.count > 0
        result[:result] = true
        result[:activity] = activities.first
        return result
      else
        activities = activities_filter.where("DATE_FORMAT(start_date, '%Y-%m-%d') = DATE_FORMAT('#{start_date}', '%Y-%m-%d')").where("DATE_FORMAT(DATE_ADD('#{end_date}',INTERVAL -1 minute),'%H:%i:%s')  between DATE_FORMAT(start_date,'%H:%i:%s') and DATE_FORMAT(end_date,'%H:%i:%s')")
        if activities.count > 0
          result[:result] = true
          result[:activity] = activities.first
          return result
        else
          result[:result] = false
          return result
        end
      end
    else
      result[:result] = false
      return result
    end
    else
      result[:result] = true
      if activities_range_end.count > 0
        result[:activity] = activities_range_end.first
      end
      if activities_range_start.count > 0
        result[:activity] = activities_range_start.first
      end
      return result
    end

  end

end
