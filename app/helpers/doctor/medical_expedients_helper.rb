module Doctor::MedicalExpedientsHelper

  def search_disease word
    diseases = []
    Disease.where("name like '%#{word}%' or code like '%#{word}%' ").limit(20).each do |c|
      complete_value =  c.code + " - " + c.name
      diseases.push({id: c.id, value: complete_value})
    end
    diseases
  end

  def search_medicament word
    medicaments = []
    Medicament.joins(:active_substances).where("name like '%#{word}%' or code like '%#{word}%' or comercial_name like '%#{word}%' or tags like '%#{word}%'").limit(20).each do |c|
      medicaments.push({id: c.id, value: c.comercial_name+' ('+c.presentation+')'})
    end
    medicaments
  end

  def doctor_has_feature feature_key
    true
  end

  def create_medical_consultation_record
    #1 crear medical_consultation
    #2 crear lista de enfermedades (desease)
    #3 crear una prescription
    #4 crear lista de indications (medicamentos) y agregar el arreglo a prescription
    #5 crea signos vitales
    #6 guardar medical consultation
    #7 asociar consulta a un embarazo

    expedient = MedicalExpedient.find params[:expedient_id]
    medical_consultation = MedicalConsultation.new(params.require(:medical_consultation).permit(
        :consultation_type_id,:diagnostic, :current_condition, :price,:notes))

    date_cons = ActiveSupport::TimeZone["Mexico City"].parse(params[:medical_consultation][:date_consultation])
    medical_consultation.date_consultation = date_cons
    medical_consultation.invoiced = false
    medical_consultation.user = current_user
    medical_consultation.notes = params[:general_notes]
    medical_consultation.patient = expedient.patient
    if !params[:medical_consultation][:office].blank?
      office = Office.find params[:medical_consultation][:office]
      medical_consultation.office = office
      medical_consultation.folio = office.folio
    end

    if medical_consultation.price == 0
      medical_consultation.is_charged = true
    else
      medical_consultation.is_charged = false
    end

    medical_consultation.medical_expedient = expedient
    medical_consultation.print_diagnostic = false

    #print medical_consultation.office.to_json
    diseases = []
    json_diseases = JSON.parse params[:diseases]
    json_diseases.each do |deseas|
      diseases << Disease.find(deseas)
    end
    medical_consultation.diseases = diseases

    medical_indications = []
    json_medicaments = JSON.parse params[:medicaments]

    data_recomendation = params[:medical_consultation][:prescription_attributes].present? ? params[:medical_consultation][:prescription_attributes][:recommendations] : ""
    prescription = Prescription.new(recommendations: data_recomendation)

    json_medicaments.each do |medicament|
      medical_indications << MedicalIndication.new(medicament: Medicament.find(medicament["id"]), dose:medicament["dose"])
    end


    prescription.medical_indications = medical_indications
    medical_consultation.prescription = prescription


    if medical_consultation.save
      # se actualiza el folio para la siguente consulta
      office.update(folio: office.folio.to_i + 1)

      #guardar valoraciones del paciente
      valorations = []
      JSON.parse(params[:valorations]).each do |valoration|
        vt = PatientValoration.create(valoration_type_id: valoration['valoration_type_id'], patient_id: expedient.patient_id, user_id: current_user.id)
        valoration['fields'].each do |f|
          if f['val'].present?
            PatientValorationField.create(patient_valoration_id: vt.id, valoration_field_id: f['id'], data:f['val'])
          end
        end
        valorations.push(vt)
      end
      medical_consultation.update(patient_valorations:valorations)
      #fin de guardar valoraciones


      #guardar campos custom
      custom_fields = JSON.parse(params[:custom_fields])
      custom_fields.each do |custom_field|
        consultation_info = ConsultationInfo.create(medical_consultation_id: medical_consultation.id, consultation_field_id: custom_field["id"], data: custom_field["value"])
      end
      #fin de guardar campos custom

      #patient_updated = Patient.find(medical_consultation.patient.id)
      #patient_updated.update(weight: medical_consultation.vital_sign.weight,height: medical_consultation.vital_sign.height,systolic_pressure: medical_consultation.vital_sign.systolic_pressure,diastolic_pressure: medical_consultation.vital_sign.diastolic_pressure)
      if doctor_has_feature Feature::GIN
        current_pregnancy = Pregnancy.current_by_expedient(expedient.id).first
        if !current_pregnancy.nil?
          medical_consultation.update({:pregnancies => [current_pregnancy]})
        end
      end
       {result: true, consultation_id: medical_consultation.id}
    else
       {result:false, errors: medical_consultation.errors.full_messages}
    end
  end


  def uro_exploration_params
    params.require(:medical_consultation).require(:uro_exploration).permit(:lymph_nodes,:abdomen,:bladder, :lumbar_region,:scrotum_testicles,:hernias,
                                                                           :epididymis_vessels,:penis,:injuries,:prostate,:seminal_vesicles,:notes)
  end

  def exploration_params
    params.require(:medical_consultation).require(:exploration_attributes).permit(:exploration_type_id, :annexed,:breasts,:vulva,:abdomen,:cervix,:uterine_height,:ovaries,:edemas,:head,:lungs, :heart,
    :extremities, :other, :vagina, :pelvis, :rectum)
  end

  def format_pregnancy_table(pregnancies_array)
    pregnancies = []
    pregnancies_array.each do |preg|
      pregnancy = {
          :id => preg.id,
          :initial_weight => preg.initial_weight,
          :last_menstruation_date => preg.last_menstruation_date != nil ? preg.last_menstruation_date.strftime('%d/%m/%Y') : ''
      }

      consultations = []

      preg.medical_consultations.order('date_consultation ASC').each do |mc|
        weeks = 0
        if mc.date_consultation != nil
          weeks = (((mc.date_consultation - preg.last_menstruation_date.to_time) / 1.day).to_i ) / 7
        end

        consultations.push({
                               :date => mc.date_consultation.strftime('%d/%m/%Y'),
                               :weight => mc.vital_sign.weight,
                               :week => weeks.to_i
                           })

      end

      pregnancy['consultations'] = consultations
      pregnancies.push(pregnancy)
    end
    pregnancies
  end
end
