module Doctor::MedicalHistoryHelper

  def update_medical_history
    update_no_pathological if params[:no_pathological].present?
    update_family_disease if  params[:family_disease].present?
    update_pathological_allergies if params[:pathological_allergy].present?
    update_surgery if params[:surgery].present?
    update_pathological_condition if params[:pathological_conditions].present?
    update_pathological_medicines if params[:pathological_medicine].present?
  end



  private
    def update_family_disease
      # se actualizan los campos de family_disease como las relaciones a diseases
      # cuando no existe ninguna enfermedad agregada, no trae el parametro con el array
      family_disease = params.fetch(:family_disease).permit(:diabetes, :diabetes_description, :hypertension, :hypertension_description,
                                                              :overweight, :overweight_description, :asthma, :asthma_description,
      :cancer, :cancer_description, :other_diseases, :annotations,:psychiatric_diseases,:psychiatric_diseases_description,
                                                            :neurological_diseases,:neurological_diseases_description,
                                                            :cardiovascular_diseases,:cardiovascular_diseases_description,
                                                            :bronchopulmonary_diseases,:bronchopulmonary_diseases_description,
                                                            :thyroid_diseases,:thyroid_diseases_description,
                                                            :kidney_diseases,:kidney_diseases_description)

      if @medical_history.family_disease.update(family_disease)
          if !params[:family_disease][:diseases_new].nil?
            @medical_history.family_disease.diseases.destroy_all
            json_array = JSON.parse(params[:family_disease][:diseases_new])
            json_array.each do |disease|
              @medical_history.family_disease.diseases << Disease.find(disease["id"].to_i)
            end
          end
        true
      else
        false
      end
    end

    def update_pathological_allergies
      pathological_allergy = params.fetch(:pathological_allergy).permit(:allergy_medications, :allergy_medications_description, :other_allergies)
      if @medical_history.pathological_allergy.update(pathological_allergy)
        true
      else
        false
      end
    end

    def update_surgery
      surgery = params.fetch(:surgery).permit(:description,:hospitalizations,:transfusions)
      if @medical_history.surgery.update(surgery)
        true
      else
        false
      end
    end

    def update_pathological_condition
      pathological_condition = params.fetch(:pathological_conditions).permit(:description)
      if @medical_history.pathological_condition.update(pathological_condition)
        true
      else
        false
      end
    end

    def update_pathological_medicines
      # se actualizan los campos de pthological_medicines como las relaciones a medicaments
      # cuando no existe ningun medicamento agregado, no trae el parametro con el array
      pathological_medicine = params.fetch(:pathological_medicine).permit(:other_medicines)

      if @medical_history.pathological_medicine.update(pathological_medicine)
        if !params[:pathological_medicine][:medicines_new].nil?
          @medical_history.pathological_medicine.medicaments.destroy_all
          json_array = JSON.parse(params[:pathological_medicine][:medicines_new])
          json_array.each do |medicament|
            @medical_history.pathological_medicine.medicaments << Medicament.find(medicament["id"].to_i)
          end
        end
        true
      else
        false
      end
    end

    def update_no_pathological
      no_pathological = params.fetch(:no_pathological).permit(:smoking, :smoking_description, :alcoholism, :alcoholism_description, :drugs, :drugs_description,
                                                              :play_sport, :play_sport_description, :annotations,:marital_status_id,:religion,:scholarship)
      if @medical_history.no_pathological.update(no_pathological)
        true
      else
        false
      end
    end

end

