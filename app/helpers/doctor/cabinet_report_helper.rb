module Doctor::CabinetReportHelper


  def delete_picture picture
    dir = Rails.root.join('app','assets','images','uploads',picture.to_s)
    if File.exist? dir
      FileUtils.rm_rf(dir)
    end
  end



  def save_picture_report (picture64, analysis_id,picture_index,cabinet_report)
    dir = Rails.root.join('uploads','archives','analysis_'+analysis_id.to_s)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)
    puts picture64
    picture_name = 'image_report_'+picture_index+'.jpg'
    File.open(dir.join(picture_name), 'wb') do |file|
      image_data = Base64.decode64(picture64['data:image/png;base64,'.length .. -1])
      file.write(image_data)
    end
    picture_root = "/uploads/archives/analysis_"+analysis_id.to_s+"/"+picture_name
    cabinet_report.update(picture_root: picture_root)
  end

  def generate_medical_history expedient
    # se genera el historial medico, los modelos de no_pathological,
    # pathological, family_dusease,  pathological_allergies, surgery,
    # pathological_medicines
    pathologicals = []

    pathological_family = Pathological.new(section: FamilyDisease.new, subcategory_pathological: SubcategoryPathological.find(SubcategoryPathological::FAMILY_DISEASE), order:0)
    pathologicals << pathological_family

    pathological_surgery = Pathological.new(section: Surgery.new, subcategory_pathological: SubcategoryPathological.find(SubcategoryPathological::PATIENT_CONDITION), order:1)
    pathologicals << pathological_surgery

    pathological_allergy = Pathological.new(section: PathologicalAllergy.new, subcategory_pathological: SubcategoryPathological.find(SubcategoryPathological::PATIENT_CONDITION), order:2)
    pathologicals << pathological_allergy

    pathological_condition = Pathological.new(section: PathologicalCondition.new, subcategory_pathological: SubcategoryPathological.find(SubcategoryPathological::PATIENT_CONDITION), order:2)
    pathologicals << pathological_condition

    pathological_medicine = Pathological.new(section: PathologicalMedicine.new, subcategory_pathological: SubcategoryPathological.find(SubcategoryPathological::MEDICINES), order:4)
    pathologicals << pathological_medicine

    medical_history = MedicalHistory.new(no_pathological:NoPathological.new, pathologicals: pathologicals)
    expedient.medical_history = medical_history

    expedient
  end
end
