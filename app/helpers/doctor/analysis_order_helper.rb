module Doctor::AnalysisOrderHelper

  def create_clinical_analysis_order
    clinical_order = ClinicalAnalysisOrder.new(params.require(:analysis).permit(:laboratory_id, :annotations, :result, :clinical_order_type_id,:office_id))
    clinical_order.date_order = DateTime.now.to_s(:db)
    clinical_order.user = current_user
    clinical_order.medical_expedient = @medical_expedient
    last_folio = MedicalExpedient.joins(:clinical_analysis_orders).where('clinical_analysis_orders.medical_expedient_id = ?', @medical_expedient.id).maximum("clinical_analysis_orders.folio")
    if last_folio.nil?
      next_folio = 1
    else
      next_folio = last_folio.to_i + 1
    end
    clinical_order.folio = next_folio
    json_analysis = JSON.parse params[:analysis][:clinical_analysis_order]
    analysis_order = []
    json_analysis.each do |study|
      analysis_order << ClinicalStudy.find(study["id"])
    end
    clinical_order.clinical_studies = analysis_order
    if clinical_order.save
      {result: true, analysis: clinical_order}
    else
      {result:false, errors: clinical_order.errors.full_messages}
    end
  end


  #all_analysis, analysis_to_filter
  def filter_indications_by_analysis(all_indications)
    indications = []

    CaGroup.all.each do |group|
      indications_aux =group.indications.where(id: all_indications.map{ |indication| indication.id}).order(:priority).group("indications.id")
      indications_aux.each_with_index { |indication, index|
        if indication.priority.to_i == 0
          indications << indication
        else
          if index.to_i == 0
            indications << indication
          end
        end
      }
    end
    indications
  end

  def generate_analysis_as_json analysis
    {
        rows: [{"id" => analysis.id,
                "folio" => analysis.folio,
                "patient_name" => analysis.medical_expedient.patient.full_name,
                "date_analysis" => analysis.date_order,
                "laboratory" => '',
                "annotations" => (analysis.annotations.length > 200 )? analysis.annotations[0..200].to_s+"..." : analysis.annotations,
                "url_show_analysis"=>doctor_analysis_order_path(analysis)}]
    }
  end
end
