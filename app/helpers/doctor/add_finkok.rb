require 'savon'
require 'pp'
require 'rubygems'
module Doctor::ADD_FINKOK
class Clientes2
  def register_in_finkok rfc
    entorno = FinkokConfig.first.environment
    usuario = ""
    password = "";
    base = (entorno.eql?'produccion' )? FinkokConfig.first.prod_wsdl : FinkokConfig.first.test_wsdl
    client = Savon.client(wsdl: "#{base}/registration.wsdl")
    if entorno.eql?'produccion'
      usuario = FinkokConfig.first.prod_username
      password = FinkokConfig.first.prod_password
    else
      usuario = FinkokConfig.first.test_username
      password = FinkokConfig.first.test_password
    end
    # lista las funciones disponibles en el web service
    client.operations
    # Realiza la operacion add para RFCs pasando los datos necesarios
    response = client.call(:add, message: { :reseller__username => usuario, :reseller__password => password, :taxpayer__id => rfc  })
    #retorno de la peticion a la funcion del web service
    response.body
  end

  def prueba
    puts "hola prueba"
  end


end
  end