module Doctor::AccountSettingsHelper

  def update_settings_doctor(params)
    # doctor = Doctor.find(current_user.doctor.id)





    # se actualiza los campos para la plantilla personalizada
    custom_template = CustomTemplatePrescription.first

    flag_update_prescription = false
    flag_update_prescription = true unless custom_template.update(footer_cm: params["footer_cm_#{custom_template.id.to_s}"].to_f, header_cm: params["header_cm_#{custom_template.id.to_s}"].to_f, orientation: params["orientation_#{custom_template.id.to_s}"], page_size: params["page_size_#{custom_template.id.to_s}"])

    flag_field_template_error = false
    #flag_field_template_success = false
    custom_template.custom_template_prescription_fields.each do |field|
      unless field.update(font: params["custom_field_font_#{field.id.to_s}"],
                          is_bold: params["is_bold_#{field.id.to_s}"].present? ? true : false,
                          position_left_cm: params["position_left_cm_#{field.id.to_s}"].to_f,
                          position_top_cm: params["position_top_cm_#{field.id.to_s}"].to_f,
                          show_field: params["show_field_#{field.id.to_s}"].present? ? true : false,
                          size: params["custom_field_size_#{field.id.to_s}"].to_i,
                          section: params["section_document_#{field.id.to_s}"],
                          width_box: params["width_cm_#{field.id.to_s}"].to_f)
        flag_field_template_error = true
      end
    end



    if flag_field_template_error or flag_update_prescription
      result = false
      error = "Algunos elementos no se guardaron correctamente, asegure que ingresar valores correctos y reintente nuevamente."
    else
      result = true
      error = ""
    end

    {result: result, error: error}

  end

  def create_custom_template_doctor
    if @doctor.custom_template_prescription.nil?
      # se crea la configuracion de plantilla personalizada
      custom_template = CustomTemplatePrescription.new(header_cm: 0, footer_cm: 0, doctor: @doctor)
      #section_id = CustomTemplatePrescriptionField::SECTIONS.first.keys.first

=begin
      FieldsPrescription.all.each do |field|
        custom_template.custom_template_prescription_fields << CustomTemplatePrescriptionField.new(fields_prescription: field, font: FieldsPrescription::FONT_FAMILY.first, is_bold: false, size:12, position_top_cm: 0, position_left_cm: 0, show_field: false, section: section_id)
      end
=end
      custom_template.save
=begin
    else
      section_id = CustomTemplatePrescriptionField::SECTIONS.first.keys.first
      custom_template = @doctor.custom_template_prescription
      FieldsPrescription.all.each do |field|
        unless custom_template.custom_template_prescription_fields.exists?(fields_prescription_id: field.id)
          # se crea nueva campo para la plantilla
          custom_template.custom_template_prescription_fields << CustomTemplatePrescriptionField.new(fields_prescription: field, font: FieldsPrescription::FONT_FAMILY.first, is_bold: false, size:12, position_top_cm: 0, position_left_cm: 0, show_field: false, section: section_id)
        end
      end
=end
    end
  end
end