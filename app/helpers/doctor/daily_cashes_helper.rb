module Doctor::DailyCashesHelper

  def open_daily_cash account_id
    daily_cash = DailyCash.new
    daily_cash.account_id = account_id
    daily_cash.start_date = Time.now
    daily_cash.status = DailyCash::OPEN
    daily_cash.user_id = current_user.id
    daily_cash.save

    account = Account.find(account_id)


    last_dc = DailyCash.last_by_account(account_id,DailyCash::CLOSED).first

    ClinicInfo.first.currency_exchanges.each do |ce|
      if last_dc != nil
        balance = last_dc.get_balance(ce.currency_id)
        DcBalance.create(start:balance, end:balance, daily_cash_id: daily_cash.id, currency_id:ce.currency_id)
      else
        if ce.currency_id == account.currency.id
          balance = account.balance
        else
          balance = 0
        end
        DcBalance.create(start:balance, end:balance, daily_cash_id: daily_cash.id, currency_id:ce.currency_id)
      end

    end

    daily_cash
  end


  def generate_transaction data

    account= Account.find(data['account_id'])
    accounts = [account]
    amount = data['amount'].to_f

    transaction = Transaction.new
    transaction.t_type = data['t_type']
    transaction.user_id = current_user.id
    transaction.transaction_type_id = data['transaction_type']
    transaction.payment_method_id = data['payment_method']
    transaction.accounts = accounts
    transaction.invoiced = 0
    transaction.amount = data['amount']
    transaction.description = data['description']
    transaction.date = Time.now
    transaction.currency_id = data['currency_id']
    transaction.currency_exchange = data['currency_exchange']

    #update account balance
    balance = account.balance
    transaction.init_balance = balance
    if data['t_type'] == '1'
      balance = balance + amount
    else
      balance = balance - amount
    end

    transaction.end_balance = balance
    transaction.chargeable = data['chargeable']
    transaction.save

    if account.bank.is_local
      current_daily_cash = DailyCash.open_by_account(data['account_id']).first
      if current_daily_cash == nil
        current_daily_cash = open_daily_cash data['account_id']
      end

      balance = current_daily_cash.dc_balances.where('currency_id = ?',data['currency_id']).first

      if balance.nil?
        balance = DcBalance.create(daily_cash_id: current_daily_cash.id, currency_id: data['currency_id'], start: 0, end: 0)
      end

      dc_balance = balance.end

      if data['t_type'] == '1'
        dc_balance = dc_balance + amount
      else
        dc_balance = dc_balance - amount
      end

      balance.update({:end => dc_balance})

      transaction.update({:daily_cashes => [current_daily_cash]})
    else
      #update bank balance
      init_balance = account.balance

      if data['t_type'] == '1'
        init_balance = init_balance + amount
      else
        init_balance = init_balance - amount
      end

      account.update({:balance => init_balance})

    end

    transaction

  end

  def generate_movement(params)
    amounts = params[:amounts]
    new_transactions = nil
    amounts.each do |idx, val|
      data = Hash.new
      t_description = ''
      account = Account.find(params[:account_id])

      if params[:transaction_type_id].to_i == TransactionType::OTHER
        movement_type = MovementType.find(params[:movement_type_id])
        t_description = 'Pago de '+movement_type.name
        chargeable = OtherMovement.new
        chargeable.movement_type_id = params[:movement_type_id]
        chargeable.details = params[:details]
      elsif params[:transaction_type_id].to_i == TransactionType::CONSULTATION
        t_description = 'Cobro de Consulta'
        chargeable = Charge.new
        chargeable.medical_consultation_id = params[:consult_id]
        chargeable.details = params[:details]

      elsif params[:transaction_type_id].to_i == TransactionType::TRANSFER
        source_account = Account.find(params[:account_id])
        target_account = Account.find(params[:to_account_id])
        t_description = params[:details]

        chargeable = Transfer.new
        chargeable.from = source_account
        chargeable.to = target_account
      elsif params[:transaction_type_id].to_i == TransactionType::REPORT
        t_description = 'Cobro de reporte de estudio'
        chargeable = ChargeReportStudy.new
        chargeable.cabinet_report_id = params[:report_id]
        chargeable.details = params[:details]
      end

      chargeable.save

      data['chargeable'] = chargeable
      data['description'] = t_description
      data['t_type'] = params[:t_type]
      data['account_id'] = params[:account_id]
      data['transaction_type'] = params[:transaction_type_id]
      data['payment_method'] = params[:payment_method_id]

      amount = val[:amount].to_f
      currency_id = val[:currency].to_i

      ex = CurrencyExchange.where('currency_id = ?',currency_id).first

      data['currency_id'] = currency_id
      data['currency_exchange'] = ex.value
      data['amount'] = amount
      if params[:transaction_type_id].to_i == TransactionType::TRANSFER
        data['t_type'] = '0'
        new_transactions = generate_transaction(data)

        data['t_type'] = '1'
        data['account_id'] = params[:to_account_id]
        new_transactions = generate_transaction(data)
      else
        new_transactions = generate_transaction(data)
      end

    end


    if params[:transaction_type_id].to_i == TransactionType::CONSULTATION
      consultation = MedicalConsultation.find(params[:consult_id])
      if consultation.is_paid
       sql = "update medical_consultations set is_charged = true where id = "+params[:consult_id]
       ActiveRecord::Base.connection.execute(sql)
      end
    elsif params[:transaction_type_id].to_i == TransactionType::REPORT
      report = CabinetReport.find(params[:report_id])
      if report.is_paid
        sql = "update cabinet_reports set is_charged = true where id = "+params[:report_id]
        ActiveRecord::Base.connection.execute(sql)
      end
    end
    new_transactions
  end

  def close_dcash params
    daily_cash = DailyCash.find(params[:id])
    if params[:amounts].present?
      params[:amounts].each do |idx, val|
        data = Hash.new
        option = params[:option].to_i
        t_description = ''
        if option != DailyCash::CLOSE_ONLY
          case option
            when DailyCash::WITHDRAW
              movement_type = MovementType.find_by name: 'Retiro de utilidad'
              t_description = 'Transacción final de corte de caja'
              chargeable = OtherMovement.new
              chargeable.movement_type_id = movement_type.id
              chargeable.details = t_description
              data['t_type'] = '0'
              data['transaction_type'] = TransactionType::OTHER
              data['payment_method'] = PaymentMethod::CASH
            when DailyCash::TRANSFER
              data['transaction_type'] = TransactionType::TRANSFER
              data['payment_method'] = PaymentMethod::TRANSFER
              source_account = Account.find(daily_cash.account.id)
              target_account = Account.find(val[:target_account])
              t_description = 'Transacción final de corte de caja'
              chargeable = Transfer.new
              chargeable.from = source_account
              chargeable.to = target_account
          end

          chargeable.save

          data['chargeable'] = chargeable
          data['description'] = t_description
          data['account_id'] = daily_cash.account.id

          amount = val[:amount].to_f
          currency_id = val[:currency].to_i
          ex = CurrencyExchange.where('currency_id = ?',currency_id).first

          data['currency_id'] = currency_id
          data['currency_exchange'] = ex.value
          data['amount'] = amount

          if option == DailyCash::TRANSFER
            data['t_type'] = '0'
            generate_transaction data

            data['t_type'] = '1'
            data['account_id'] = val[:target_account]
            generate_transaction data
          else
            generate_transaction data
          end
        end
      end
    end


    daily_cash.update(
        :close_user_id => session[:user_id],
        :status => DailyCash::CLOSED,
        :end_date => Time.now
    )

  end

  def generate_charge params

    account = Account.find(params[:account_id])
    doctor_currency = CurrencyExchange.where(doctor_id: account.doctor.id,currency_id: params[:currency]).first
    default_exchange = account.doctor.currency_exchanges.where('is_default is true').first
    if default_exchange.nil?
      return {:done => false, :error => 'No se puede generar el cobro', :message => 'Por favor configure su moneda default'}
    end

    data = Hash.new
    #poner if para reportes laboratorio
    #y para doctor en la seccion de expedientes mandar un parametro mas para saber que es consulta
    if params[:type] == "consult"
      t_description = 'Cobro de Consulta'
      chargeable = Charge.new
      chargeable.medical_consultation_id = params[:consult_id]
      chargeable.details = params[:details]
      data['transaction_type'] = TransactionType::CONSULTATION
    else
      t_description = 'Cobro de reporte de estudio'
      chargeable = ChargeReportStudy.new
      chargeable.cabinet_report_id = params[:report_id]
      chargeable.details = params[:details]
      data['transaction_type'] = TransactionType::REPORT
    end

    chargeable.save

    data['t_type'] = '1'
    data['chargeable'] = chargeable
    data['description'] = t_description
    data['account_id'] = params[:account_id]
    data['amount'] = params[:amount]

    data['payment_method'] = params[:payment_method_id]
    account = Account.find(params[:account_id])
    data['doctor_id'] = account.doctor_id

    data['currency_id'] = params[:currency]
    data['currency_exchange'] = doctor_currency.value

    generate_transaction data

    if params[:type] == "consult"
      consult = MedicalConsultation.find(params[:consult_id])
      if consult.is_paid
        sql = "update medical_consultations set is_charged = true where id = "+params[:consult_id]
        ActiveRecord::Base.connection.execute(sql)
      else
      end

      return {:done => true, :consult => consult}
    else
      cabinet_report = CabinetReport.find(params[:report_id])
      if cabinet_report.is_paid
        sql = "update cabinet_reports set is_charged = true where id = "+params[:report_id]
        ActiveRecord::Base.connection.execute(sql)
      end
      return {:done => true, :consult => consult}
    end


  end

  def get_no_dc_accounts accounts
    resp = []
    accounts.each do |a|
      show_account = true
      @daily_cashes.each do |dc|
        if dc.account_id == a.id
          show_account = false
        end
      end
      if show_account
        resp.push(a)
      end
    end
    resp
  end

end