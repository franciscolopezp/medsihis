module Doctor::PatientsHelper

  def search_patients word,limit
    patients = []
    Patient.by_match(word,limit).each do |p|
      patients.push({id:p.id, value:p.full_name})
    end
    patients
  end

  def search_city word
    cities = []
    City.find_by_name(word).each do |c|
      cities.push({id: c.id, value: c.format_city_state_and_country})
    end
    cities
  end

  def set_city_patient params
    #se relaciona la ciudad
    if params[:city_id].blank?
      if !params[:city_name].blank?
        #intentar buscar la ciudad y relacionar lo
        search_city_array = search_city(params[:city_name])
        @doctor_patient.city_id = search_city_array.first[:id] if search_city_array.count > 0
      end
    end

  end

  def save_picture_cam param
    if !@doctor_patient.picture.nil?
      if !param.nil?
      uploaded_cam = param
      save_picture_captured uploaded_cam
      end
    else
      delete_picture @doctor_patient.picture
      if !param.nil?
        uploaded_cam = param
        save_picture_captured uploaded_cam
      end
    end
  end

  def save_picture_patient params
    # se guarda la imagen
    if @doctor_patient.id.nil?
      if !params[:patient][:picture].nil?
        uploaded_io = params[:patient][:picture]
        save_picture uploaded_io
      end
    else
      #verificar si existe imagen guardada
      if !@doctor_patient.picture.nil?
        if !params[:patient][:picture].nil?
          #borrar la imagen anterior y guardar nueva
          delete_picture @doctor_patient.picture
          #FileUtils.rm_rf(Rails.root.join('app','assets','images','uploads',@doctor_patient.picture))
          uploaded_io = params[:patient][:picture]
          save_picture uploaded_io
        end
      else
        #preguntar si existe nueva imagen
        if !params[:patient][:picture].nil?
          uploaded_io = params[:patient][:picture]
          save_picture uploaded_io
        end
      end
    end
  end

  def delete_picture picture
    dir = Rails.root.join('app','assets','images','uploads',picture.to_s)
    if File.exist? dir
      FileUtils.rm_rf(dir)
    end
  end

  def save_patient_folio(current_user,gender_id)
    @city_name = params[:city_name]
    @text_button = "Agregar"

    #@medicalHistory = MedicalHistory.new
    expedient = MedicalExpedient.new(folio: MedicalExpedient.generate_folio, user: current_user,active:1)
    expedient = generate_medical_history(expedient,gender_id)
    @doctor_patient.medical_expedient = expedient

    #@medicalHistory.medical_expedient = expedient
  end
  def save_default_backgrounds patient
    no_pathological = patient.medical_expedient.medical_history.no_pathological
    defaul_no_pathological = DefaultNoPathological.first
    if defaul_no_pathological != nil
      no_pathological.update(smoking:defaul_no_pathological.smoking,smoking_description:defaul_no_pathological.smoking_description,alcoholism:defaul_no_pathological.alcoholism,alcoholism_description:defaul_no_pathological.alcoholism_description,drugs:defaul_no_pathological.drugs,drugs_description:defaul_no_pathological.drugs_description,play_sport:defaul_no_pathological.play_sport,play_sport_description: defaul_no_pathological.play_sport_description,
                             scholarship:defaul_no_pathological.scolarship,marital_status_id: defaul_no_pathological.marital_status_id, religion:defaul_no_pathological.religion,annotations:defaul_no_pathological.annotations)
    end
    pathological  = patient.medical_expedient.medical_history.family_disease
    default_pathological = DefaultPathological.first
    if default_pathological != nil
      pathological.update(diabetes:default_pathological.diabetes,diabetes_description:default_pathological.diabetes_description,overweight:default_pathological.overweight,
                          overweight_description:default_pathological.overweight_description,hypertension:default_pathological.hypertension,hypertension_description:default_pathological.hypertension_description,
                          asthma:default_pathological.asthma,asthma_description:default_pathological.asthma_description,cancer:default_pathological.cancer,cancer_description:default_pathological.cancer_description,annotations:default_pathological.annotations,
                          other_diseases:default_pathological.other_diseases,
                          psychiatric_diseases:default_pathological.psychiatric_diaseases,psychiatric_diseases_description:default_pathological.psychiatric_diaseases_description,
                          neurological_diseases:default_pathological.neurological_diseases,
                          neurological_diseases_description:default_pathological.neurological_diseases_description,cardiovascular_diseases:default_pathological.cardiovascular_diseases,
                          cardiovascular_diseases_description:default_pathological.cardiovascular_diseases_description,
                          bronchopulmonary_diseases:default_pathological.bronchopulmonary_diseases,
                          bronchopulmonary_diseases_description:default_pathological.bronchopulmonary_diseases_description,thyroid_diseases:default_pathological.thyroid_diseases,
                          thyroid_diseases_description:default_pathological.thyroid_diseases_description,kidney_diseases:default_pathological.kidney_diseases,
                          kidney_diseases_description:default_pathological.kidney_diseases_description)
      medical_history = patient.medical_expedient.medical_history
      medical_history.update(surgeries:default_pathological.surgeries,transfusions:default_pathological.transfusions,
                             allergy_medicament:default_pathological.allergy,allergy_medicament_description:default_pathological.allergy_description,recent_diseases:default_pathological.recent_diseases,other_allergies:default_pathological.other_allergies,other_medicines:default_pathological.other_medicaments)
    end
    default_ginecology = DefaultGinecologyBackground.first
    if default_ginecology != nil

      inherited = patient.medical_expedient.medical_history.inherited_family_background
      inherited.update(gynecological_diseases:default_ginecology.ginecopatia,
                       gynecological_diseases_description:default_ginecology.ginecopatia_description,
                       mammary_dysplasia:default_ginecology.mastopatia,
                       mammary_dysplasia_description:default_ginecology.mastopatia_description,
                       twinhood:default_ginecology.gemelaridad,
                       twinhood_description:default_ginecology.gemelaridad_description,
                       congenital_anomalies:default_ginecology.congenital_anomalies,
                       congenital_anomalies_description:default_ginecology.congenital_anomalies_description,
                       infertility:default_ginecology.infertility,
                       infertility_description:default_ginecology.infertility_description,
                       others:default_ginecology.others,
                       others_description:default_ginecology.others_diseases,
                       anotations:default_ginecology.annotations)

      gynecology = patient.medical_expedient.medical_history.gynecologist_obstetrical_background
      if gynecology != nil
      gynecology.update(
          menstruation_start_age:default_ginecology.menstruation_start_age,
          married:default_ginecology.married,
          spouse_name:default_ginecology.spouse_name,
          gestations:default_ginecology.gestations,
          natural_birth: default_ginecology.natural_births,
          caesarean_surgery:default_ginecology.cesareans,
          abortion_number: default_ginecology.abortions,
          abortion_causes: default_ginecology.abortion_causes,
          menstruation_type: default_ginecology.menstruation_type,
          sexually_active:  default_ginecology.sexually_active,
          contraceptives_method:  default_ginecology.contraceptives_method,
          menopause:    default_ginecology.menopause,
          anotations:      default_ginecology.obstetrical_annotations,
          information_spouse: default_ginecology.spouse_information,
          cycles:    default_ginecology.cycles,
          ivs:        default_ginecology.ivs,
          sexual_partners:          default_ginecology.sexual_partners,
          std:         default_ginecology.std)
      end

    end
    default_perinatal = DefaultPerinatalBackground.first
    if default_perinatal != nil
      perinatal = patient.medical_expedient.medical_history.perinatal_information
      perinatal.update(hospital:default_perinatal.hospital,weight:default_perinatal.weight,height:default_perinatal.height,pregnat_risks:default_perinatal.pregnancy_risks,other_info:default_perinatal.other_info)

      extra_perinatal = patient.medical_expedient.medical_history.child_non_pathological_info
      extra_perinatal.update(child_academic_grade:default_perinatal.scholarship,house_type:default_perinatal.house_type,
                             number_people:default_perinatal.hounse_inmates)
    end
    default_urology = DefaultUrologicalBackground.first
    if default_urology != nil
      urological = patient.medical_expedient.medical_history.urological_background
      urological.update(testicular_pain:default_urology.testicular_pain,
                        impotence:default_urology.impotence,
                        erection_dificulty:default_urology.erection_dificulty,
                        premature_ejaculation:default_urology.premature_ejaculation,
                        vaginal_discharge:default_urology.vaginal_discharge,
                        anormal_hair_growth:default_urology.anormal_hair_growth,
                        kidneys_pain:default_urology.kidneys_pain,
                        pelvic_pain:default_urology.pelvic_pain,
                        low_abdomen_pain:default_urology.low_abdomen_pain,
                        kidney_bk:default_urology.kidney_bk,
                        kidney_stones:default_urology.kidney_stones,
                        venereal_diseases:default_urology.venereal_diseases,
                        self_examination:default_urology.self_examination,
                        prostate:default_urology.prostate,
                        last_prostate_examination:default_urology.last_prostate_examination,
                        fum:default_urology.fum,
                        gestations:default_urology.gestations,
                        last_pap_date:default_urology.last_pap_date,
                        contraceptive_method:default_urology.contraceptive_method,
                        sexually_active:default_urology.sexually_active,
                        sex_life_start:default_urology.sex_life_start,
                        sexual_partners:default_urology.sexual_partners,
                        relationship_type:default_urology.relationship_type,
                        satisfying_sex:default_urology.satisfying_sex,
                        intercourse_pain:default_urology.intercourse_pain)
    end
  end
  private
    def save_picture picture
      dir = Rails.root.join('uploads','paciente_'+@doctor_patient.id.to_s)
      FileUtils.mkdir_p(dir) unless File.directory?(dir)

      File.open(dir.join(picture.original_filename), 'wb') do |file|
        file.write(picture.read)
      end
      #@doctor_patient.picture = uploaded_io.original_filename
      @doctor_patient.update(picture: dir.join(picture.original_filename))
    end

    def save_picture_captured picture64
      dir = Rails.root.join('uploads','paciente_'+@doctor_patient.id.to_s)
      FileUtils.mkdir_p(dir) unless File.directory?(dir)
      puts picture64
      picture_name = 'Patient_'+@doctor_patient.id.to_s+'.jpg'
      File.open(dir.join(picture_name), 'wb') do |file|
        image_data = Base64.decode64(picture64['data:image/png;base64,'.length .. -1])
        file.write(image_data)
      end
      @doctor_patient.update(picture: dir.join(picture_name))
    end

    def generate_medical_history(expedient,gender_id)
      # se genera el historial medico, los modelos de no_pathological,
      # pathological, family_dusease,  pathological_allergies, surgery,
      # pathological_medicines

      medical_history = MedicalHistory.new(no_pathological:NoPathological.new, family_disease: FamilyDisease.new)
      medical_history.urological_background = UrologicalBackground.new
      if gender_id == Gender::WOMEN
      medical_history.gynecologist_obstetrical_background = GynecologistObstetricalBackground.new
      end
      medical_history.child_non_pathological_info = ChildNonPathologicalInfo.new
      medical_history.perinatal_information = PerinatalInformation.new
      medical_history.inherited_family_background = InheritedFamilyBackground.new
      expedient.medical_history = medical_history
      expedient
    end
end
