module Patient::IndexHelper

  def validate_user_patient
    if logged_in? and patient?
      unless current_user.user_patient.accept_term_conditions or current_user.user_patient.is_active
        redirect_to patient_index_index_url unless request.path == patient_index_index_path
      end
    else
      redirect_to login_url
    end
  end


end
