module Patient::AppointmentsHelper
  include Doctor::ActivitiesHelper

  def availability_medical_consultations params
    date = Date.parse(params[:date])

    activity_count = Activity.joins(:offices).where('activities.doctor_id = ? AND all_day is true',params[:doctor_id])
                         .where('activities_offices.office_id = ?',params[:office_id])
                         .where('activities.start_date < ? and activities.end_date > ?', date, date).count

    office = Office.find(params[:office_id])
    doctor = Doctor.find(office.doctor_id)
    cont = 1

    day_of_week = date.strftime('%w')
    offices_times = OfficeTime.where('day = ? and office_id = ?',day_of_week,params[:office_id])

    data = Hash.new
    data['activities'] = []

    time = nil

    data['start_time'] = offices_times.first.start.strftime('%H:%M:%S')

    offices_times.each do |ot| #por cada periodo de tiempo se va a checar la disponibilidad

      time = ot.start

      while time < ot.end
        end_time = time + office.consult_duration.to_i * 60

        times = Hash.new
        times['start'] = DateTime.new(date.year, date.month, date.day, time.hour, time.min, time.sec)
        times['end'] = DateTime.new(date.year, date.month, date.day, end_time.hour, end_time.min, end_time.sec)

        res = exist_rank_date(times['start'],times['end'] ,doctor,0)

        if res[:result] && res[:activity].activity_status_id != ActivityStatus::CANCELED
          activity = res[:activity]
          data['activities'].push(format_activity(cont,true, activity, times))
        elsif activity_count > 0
          data['activities'].push(format_activity(cont,true, nil, times))
        else
          data['activities'].push(format_activity(cont,false, nil, times))
        end

        cont += 1

        time = end_time
      end # end while

    end # end office_times.each

    if time != nil
      data['end_time'] = time.strftime('%H:%M:%S') # se toma el ultimo valor de finalizacion
    else
      data['end_time'] = ''
    end

    data
  end

  def availability_medical_consultations_mobile params


    office = Office.find(params[:office_id])
    doctor = Doctor.find(office.doctor_id)
    Time.zone = doctor.time_zone.rails_name
    date = Date.parse(params[:date])

    activity_count = Activity.joins(:offices).where('activities.doctor_id = ? AND all_day is true',office.doctor_id)
                         .where('activities_offices.office_id = ?',params[:office_id])
                         .where('activities.start_date < ? and activities.end_date > ?', date, date).count


    cont = 1

    day_of_week = date.strftime('%w')
    offices_times = OfficeTime.where('day = ? and office_id = ?',day_of_week,params[:office_id])

    data = Hash.new
    data['activities'] = []

    first_time  = true
    time = nil

    offices_times.each do |ot| #por cada periodo de tiempo se va a checar la disponibilidad

      if first_time
        data['start_time'] = ot.start.strftime('%H:%M:%S')
        first_time = false
      end

      time = ot.start

      while time < ot.end
        end_time = time + office.consult_duration.to_i * 60

        times = Hash.new
        times['start'] = DateTime.new(date.year, date.month, date.day, time.hour, time.min, time.sec)
        times['end'] = DateTime.new(date.year, date.month, date.day, end_time.hour, end_time.min, end_time.sec)

        res = exist_rank_date(times['start'],times['end'] ,doctor,0)

        if res[:result] && res[:activity].activity_status_id != ActivityStatus::CANCELED
          activity = res[:activity]
          data['activities'].push(format_activity(cont,true, activity, times))
        elsif activity_count > 0
          data['activities'].push(format_activity(cont,true, nil, times))
        else
          data['activities'].push(format_activity(cont,false, nil, times))
        end

        cont += 1

        time = end_time
      end # end while

    end # end office_times.each

    if time != nil
      data['end_time'] = time.strftime('%H:%M:%S') # se toma el ultimo valor de finalizacion
    else
      data['end_time'] = ''
    end

    data['activities']
  end

  def check_availability_office(is_call_api = false)

    not_avaliable = 'ND'
    medium_available = 'MD'
    available = 'AD'
    availability = ''


    office = Office.find(params[:office_id])
    doctor = office.doctor

    response = Hash.new
    response_to_api = []
    month = params[:month].to_i
    if month < 10
      month = '0'+month.to_s
    else
      month = month.to_s
    end

    string_start_date = params[:year]+"-"+month+"-01"
    date = Date.parse(string_start_date) #fecha en la que va a iniciar el loop
    end_loop = date + 1.month # fecha en la que finaliza el loop es justo al final del mes


    today = Date.today # se hace una reasignacion de la fecha inicial
    if today > date
      date = today
    end

    while date < end_loop
      day_of_week = date.strftime('%w')
      offices_times = OfficeTime.where('day = ? and office_id = ?',day_of_week,params[:office_id])

      if offices_times.size == 0 # no hay horarios establecidos para ese día
        availability = not_avaliable #caso1.- El dr no trabaja ese día
      else
        last_schedule_availability = ''
        offices_times.each do |ot| #por cada periodo de tiempo se va a checar la disponibilidad

          total_seconds = ot.end - ot.start
          minutes = (total_seconds / 60).to_i
          how_many_appointments = (minutes / office.consult_duration.to_i).to_i

          s_date = DateTime.new(date.year, date.month, date.day, ot.start.hour, ot.start.min, ot.start.sec)
          e_date = DateTime.new(date.year, date.month, date.day, ot.end.hour, ot.end.min, ot.end.sec)

          activities_size = Activity.joins(:offices).where('activities.doctor_id = ?',params[:doctor_id])
                                .where('activities_offices.office_id = ?',params[:office_id])
                                .where('activities.start_date > ? and activities.start_date < ?', s_date, e_date).count


          if activities_size == 0 #no hay actividades en ese periodo de tiempo

            #si no hay actividades en ese tiempo revisar si hay actividades de todo el día
            activity_count = Activity.joins(:offices).where('activities.doctor_id = ? AND all_day is true',params[:doctor_id])
                                 .where('activities_offices.office_id = ?',params[:office_id])
                                 .where('activities.start_date < ? and activities.end_date > ?', s_date, s_date).count

            if activity_count.to_i == 0
              availability = available #caso2.- El dr trabaja ese día y no hay actividades
            else
              availability = not_avaliable #no hay actividades pero hay actividaes de todo el día
            end

          else
            time = ot.start
            cont = 0

            while time < ot.end
              end_time = time + office.consult_duration.to_i * 60
              res = exist_rank_date(DateTime.new(date.year, date.month, date.day, time.hour, time.min, time.sec),
                                    DateTime.new(date.year, date.month, date.day, end_time.hour, end_time.min, end_time.sec),doctor,0)

              if res[:result]
                cont += 1
              end

              time = end_time
            end # end while

            if cont > (how_many_appointments / 2) &&  (how_many_appointments - cont) >= 1
              availability = medium_available #caso3.- hay mas de la mitad de citas en ese rango de fechas pero aun hay espacio
            elsif how_many_appointments - cont < 1
              availability = not_avaliable
            else
              availability = available #caso4.- hay menos de la mitad de citas
            end
          end


          if last_schedule_availability == available && availability != available
            last_schedule_availability = available
          elsif last_schedule_availability == medium_available && availability == not_avaliable
            last_schedule_availability = medium_available
          else
            last_schedule_availability = availability
          end

        end # end office_times.each

        if is_call_api
          response_to_api << {day: Time.parse(date.to_s), availability: last_schedule_availability}
        else
          response[date.strftime('%-d')] = last_schedule_availability
        end
      end

      date = date + 1.day
    end


    if is_call_api
      response_to_api
    else
      response
    end
  end

  def format_activity id,exist, a, data
    activity = {
        'id' => id,
        'start' => data['start'],
        'end' => data['end'],
        's_date' => data['start'].strftime('%Y-%m-%d %H:%M'),
        'e_date' => data['end'].strftime('%Y-%m-%d %H:%M')
    }

    if exist
      activity['title'] = 'No Disponible'
      activity['className'] = 'bgm-red'
      activity['available'] = false

    else
      activity['title'] = ' Disponible'
      activity['className'] = 'bgm-green'
      activity['available'] = true
    end
    activity
  end
end


