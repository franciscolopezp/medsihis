module DateHelper

  # dates formatters
  # 13/03/2017 09:54 AM
  def date_hour(date)
    return '' if date.nil?
    I18n.localize date, format: :date_hour
  end

  # 13/03/2017
  def only_date(date)
    return '' if date.nil?
    I18n.localize date, format: :only_date
  end

  # 13/Marzo/2017
  def date_month_name(date)
    return '' if date.nil?
    I18n.localize date, format: :date_month_name
  end

  # 13/Mar/2017
  def date_month_abbr(date)
    return '' if date.nil?
    I18n.localize date, format: :date_month_abbr
  end

  # 09:54 AM
  def only_hour(date)
    return '' if date.nil?
    I18n.localize date, format: :only_hour
  end

  # 13 de Marzo de 2017 09:53 AM
  def human_full(date)
    return '' if date.nil?
    I18n.localize date, format: :human_full
  end

  # Mar 2017
  def month_abbr_year(date)
    return '' if date.nil?
    I18n.localize date, format: :month_abbr_year
  end
  # end dates formatters

end