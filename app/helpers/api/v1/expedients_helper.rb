module Api::V1::ExpedientsHelper
  include ActionView::Helpers::NumberHelper

  def expedient_info patient_id
    patient = Patient.find(patient_id)
    name_city = ""
    if !patient.city.nil?
      name_city = patient.city.format_city_state_and_country
    end

    {
        id: patient.id,
        name: patient.name,
        lastName: patient.last_name,
        gender: patient.gender.name,
        birthDay: patient.birth_day,
        weight: patient.weight,
        height: patient.height,
        bloodType: patient.blood_type.name,
        phone: patient.phone,
        cell: patient.cell,
        email: patient.email,
        birthPlace: patient.birthplace,
        city: name_city,
        address: patient.address
    }
  end


  def expedient_consultation patient_id
    patient = Patient.find(patient_id)

    consultations = MedicalConsultation.joins(:medical_expedient => :doctor)
                        .joins(:medical_expedient => :patient)
                        .where(:patients => {:id => patient.id}).order('date_consultation DESC')


    consultations.map do |consultation|
      {
          id: consultation.id,
          date: consultation.date_consultation,
          price: number_to_currency(consultation.price),
          office: consultation.office.name,
          currentCondition: consultation.current_condition
      }
    end
  end

  def full_expedient_consultation consultation_id

    consultation = MedicalConsultation.find(consultation_id)

    {
        id: consultation.id,
        date: consultation.date_consultation,
        price: number_to_currency(consultation.price),
        office: consultation.office.name,
        currentCondition: consultation.current_condition,
        diagnostic: consultation.diagnostic,
        diseases: consultation.diseases.map{|disease| {id: disease.id, name: disease.name}},
        recommendations: consultation.prescription.recommendations,
        medicaments: consultation.prescription.medical_indications.map{|medical_indication| {id:medical_indication.medicament.id , name:medical_indication.medicament.comercial_name.to_s+" - "+medical_indication.medicament.presentation, dose: medical_indication.dose}}
    }
  end

end
