module Api::V1::ActivitiesHelper

  def find_all_activity params

    activities = Activity.includes(:doctor, :offices, :activity_status, :personalsupports, :user_patients).where(doctors: {id: @user.doctor.id})

    activities = activities.where("STR_TO_DATE(:start_date,'%d/%m/%Y') >= DATE(activities.start_date) and STR_TO_DATE(:end_date,'%d/%m/%Y') <= DATE(activities.end_date)", {start_date: params[:start_date], end_date: params[:end_date]}) if params[:start_date].present? and params[:end_date].present?

    activities = activities.where(activity_status_id: params[:activity_status_id]) if params[:activity_status_id].present?

    activities = activities.where(activity_type_id: params[:activity_type_id]) if params[:activity_type_id].present?

    activities = activities.order("start_date ASC")

    activities_json = []
    activities.each do |activity|
      activities_json << format_json_activity(activity)
    end
    activities_json
  end


  # se utiliza para agregar la zona horaria del doctor cuando se utiliza el api
  def set_time_zone_api
    if !@user.nil?
      # cuando el usuario de la app es el doctor
      Time.zone = @user.doctor.time_zone.rails_name if  !@user.doctor.nil?
      # cuando el usuario de la app es el paciente, y se genera una cita, para agregar la zona horaria
      Time.zone = Office.find(params[:office_id]).doctor.time_zone.rails_name if  !@user.user_patient.nil? and params[:office_id].present?
    end
  end

end
