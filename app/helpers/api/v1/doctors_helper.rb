module Api::V1::DoctorsHelper
  include ActionView::Helpers::NumberHelper

  def find_all_doctors params
    doctors = Doctor.includes(:specialties, :city)

    doctors = doctors.where(specialties: {id: params[:speciality_id].to_i}) if params[:speciality_id].present?

    doctors = doctors.where(cities: {id: params[:city_id].to_i}) if params[:city_id].present?

    doctors = doctors.where("CONCAT_WS('',doctors.name,doctors.last_name) like :name_doctor", name_doctor: "%#{params[:name_doctor]}%") if params[:name_doctor].present?

    doctors.map do |doctor|
      {
          doctorId: doctor.id,
          fullName: doctor.full_name_title,
          specialities: doctor.specialties.map{|s| s.name}.join(","),
          profilePictureUrl: url_for(controller: '/index', action: 'picture_doctor', user_id: doctor.user.id),
          isFavorite: @user.user_patient.doctors.exists?(id: doctor.id) ? '1' : '0'
      }
    end
  end

  def patients_doctors(patient_name,page)
    patients = []
    partial_name = patient_name.split(" ")
    all_patients = @user.doctor.patients.where(active: 1)
    partial_name.each do |name|
      all_patients = all_patients.where("CONCAT_WS(' ',patients.name,patients.last_name) like :name ", name: "%#{name}%")
    end
    all_patients = all_patients.order("patients.name ASC")
    patients = all_patients.page(page.to_i).per_page(15)

    # doctor = @user.doctor
    # patients = doctor.patients.where(query).order("name ASC")

    patients.map do |patient|
      {
          id:patient.id,
          name:patient.full_name,
          profile_picture_url: url_for(controller: '/index', action: 'picture_patient', user_id: 0) # <- el usuario es 0, porque el paciente no tiente user, son los del expediente.
      }
    end
  end

  def find_patient_by_name(name)
    patients = []
    partial_name = name.split(" ")
    all_patients = @user.doctor.patients
    partial_name.each do |name|
      all_patients = all_patients.where("CONCAT_WS(' ',patients.name,patients.last_name) like :name ", name: "%#{name}%")
    end
    all_patients.each do |p|
      patients.push({id:p.id, name: p.full_name})
    end
    patients
  end

  def find_personal_support_by_name(name)
    personal_supports = []
    partial_name = name.split(" ")
    all_personal = @user.doctor.personalsupports
    partial_name.each do |name|
      all_personal = all_personal.where("CONCAT_WS(' ',personalsupports.name,personalsupports.lastname) like :name ", name: "%#{name}%")
    end
    all_personal.each do |p|
      personal_supports.push({id:p.id, name: p.full_name})
    end
    personal_supports
  end


end
