module Api::V1::UsersHelper

  def validate_access(user_name, password)
    user = User.find_by(user_name: user_name.downcase)
    user = User.find_by(email: user_name.downcase) if user.nil?
    validate = {
                    status: 'error',
                    response:{},
                    message: "",
                    errors: []
                }

      if !user.nil? and !user.password_digest.nil?
        if user.authenticate(password)
         if user.is_doctor
              doctor = user.doctor
              if active_license_doctor? doctor
                validate[:status] = 'ok'
                validate[:response] = get_profile(user.id)
              end
          else
              if active_license_user_patient?(user.user_patient)
                validate[:status] = 'ok'
                validate[:response] = get_profile(user.id)
              else
                validate[:errors] = ["Acepte terminos y condiciones"]
              end
              validate[:errors] = ["Usuario o contraseña incorrecto"]
          end
        else
          validate[:errors] = ["Usuario o contraseña incorrecto"]
        end
      else
        validate[:errors] = ["Usuario o contraseña incorrecto"]
      end
    validate
  end


  def get_profile user_id
    user = User.includes(:doctor,:user_patient).find(user_id)
    data_json = nil
    if user.is_doctor
        data_json = {
            id: user.id,
            name: user.doctor.name,
            lastName: user.doctor.last_name,
            birthDate: user.doctor.birth_date.strftime("%d-%m-%Y"),
            gender: user.doctor.gender.id,
            email: user.email,
            username: user.user_name,
            type: user.role.name.downcase,
            phone: user.doctor.telephone,
            token: TypeUser::TOKEN_API
        }
    else
      data_json = {
            id: user.id,
            name: user.user_patient.name,
            lastName: user.user_patient.last_name,
            birthdate: user.user_patient.birth_date.strftime("%d-%m-%Y"),
            gender: user.user_patient.gender.id,
            email: user.email,
            username: user.user_name,
            type: user.role.name.downcase,
            phone: user.user_patient.phone,
            premiumAccount: (user.user_patient.premium_account)? 1 : 0,
            token: TypeUser::TOKEN_API
        }
    end
    data_json
  end


  def update_profile_patient_doctor(params)
    result = {
        status: :ok,
        response:{},
        message: "",
        errors: []
    }

    begin

      User.transaction do
        user = User.find(params[:user_id])
        if user.is_doctor
            doctor = user.doctor
            user.user_name = params[:user_name]
            user.email = params[:email]
            if doctor.update!(user: user,
                              name: params[:user][:name],
                              last_name: params[:user][:last_name],
                              cellphone: params[:user][:cellphone],
                              birth_date: params[:user][:birth_date])
              result[:status] = :ok
              result[:message] = "Se actualizo correctamente"
            else
              result[:status] = 'error'
              result[:message] = "Ups tenemos un problema"
            end
         else
            patient = user.user_patient
            user.user_name = params[:user_name]
            user.email = params[:email]
            if patient.update!(user: user,
                               name: params[:user][:name],
                               last_name: params[:user][:last_name],
                               cellphone: params[:user][:cellphone],
                               birth_date: params[:user][:birth_date])
              result[:status] = :ok
              result[:message] = "Se actualizo correctamente"
            else
              result[:status] = 'error'
              result[:message] = "Ups tenemos un problema"
            end

        end
      end
      result[:status] = :ok
      result[:message] = "Se actualizo correctamente"
      result

    rescue ActiveRecord::RecordNotFound => invalid
      # Paso por aqui cuando se intente buscar algun registro en BD y no exista, por ejemplo un record con id=0
      result[:status] = 'error'
      result[:message] = "Ups tenemos un problema"
      result

    rescue ActiveRecord::RecordInvalid => invalid

      all_errors = []
      if !doctor.nil?
        if doctor.errors.count > 0
          all_errors = all_errors + doctor.errors.full_messages
        end
      end

      if !patient.nil?
        if patient.errors.count > 0
          all_errors = all_errors + patient.errors.full_messages
        end
      end
      result[:status] = :ok
      result[:message] = "Ups tenemos un problema"
      result[:errors] = all_errors

    end
    result
  end

end
