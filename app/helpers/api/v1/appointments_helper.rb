module Api::V1::AppointmentsHelper

  def get_appointments_by_user user, params
    activities_json = []
    if user.is_doctor
        # citas por confirmar

      else
        # Todas las actividades del paciente. incluyen las citas confirmadas, por confirmar o canceladas
       activities = user.user_patient.activities.includes(:doctor, :activity_status)
       activities = activities.where("DATE_FORMAT(activities.updated_at,'%d/%m/%Y %H:%i') >= :last_date", {last_date: params[:last_date]}) if params[:last_date].present?

       activities.each do |activity|
        activities_json << format_json_activity(activity)
      end
    end
    activities_json
  end

  def format_json_activity activity
    patient = nil
    patient = activity.patients.first if activity.patients.size > 0
    patient = activity.user_patients.first if activity.user_patients.size > 0
    office = 'Todos los consultorios'
    if activity.offices.size == 1
      office = activity.offices.first.name
    end

    { id: 0,
      startDate: activity.start_date,
      endDate: activity.end_date,
      allDay:activity.all_day ? 1 : 0,
      name: activity.name,
      details: activity.details,
      doctorName: activity.doctor.full_name_title,
      doctorId: activity.doctor.id,
      activityStatus: activity.activity_status_id,
      activityId: activity.id,
      activityType: activity.activity_type_id,
      updateDate: activity.updated_at,
      patientName: patient.nil? ? "" : patient.full_name,
      patientId: patient.nil? ? 0 : patient.id,
      officeId: activity.offices.size == 1 ? activity.offices.first.id : 0 ,
      officeName: office,
      personalSupportsIds: activity.personalsupports.size > 0 ? activity.personalsupports.map{|p| p.id} : [],
      personalSupportsNames: activity.personalsupports.size > 0 ? activity.personalsupports.map{|p| p.full_name} : []
    }
  end

  def create_appointment params
    patient = @user.user_patient
    office = Office.find(params[:office_id])
    doctor = office.doctor

    @activity = Activity.new(params.permit(:name, :details, :start_date, :end_date))
    @activity.doctor = doctor
    @activity.all_day = false
    @activity.activity_status = ActivityStatus.find(ActivityStatus::CONFIRM_PENDING)
    @activity.activity_type = ActivityType.find(ActivityType::APPOINTMENT)

    res = exist_rank_date(@activity.start_date,@activity.end_date,doctor,0)
    errors = nil
    if res[:result]
      #send_response 'error', {}, "Error al crear la cita", ['Por favor verifique la disponibilidad nuevamente.']
      errors = ['Por favor verifique la disponibilidad nuevamente.']
    else
      @activity.owner_type = 'Patient'
      @activity.owner_id = patient.id
    end
    errors
  end



end
