module Api::V1::HealthHistoriesHelper

  def create_health_histories
    history = HealthHistory.new
    history.name = params[:name]
    history.user_patient = @user.user_patient
    response = {}
    errors = []

    valid = true
    if @user.user_patient.health_histories.size == 0
      valid = true
    elsif !@user.user_patient.premium_account
      #valid = false # version con cuenta premium
      valid = true
    end

    if valid
      if history.save
        status = :ok
        response = {id: 0,healthHistoryId: history.id, name: history.name, userId: @user.id}
      else
        status = 'error'
        errors << "Ocurrio un error, intente nuevamente"
      end
    else
      status = 'error'
      errors << "Actualmente cuenta con una versión free de MEDSI, le recomendamos comprar la versión premium para agregar más historiales médicos"
    end

      {status: status, response:response, message: "", errors: errors}
  end

  def expedients_by_health_history(health_history_id)
    history = HealthHistory.includes(:expedient_requests).find_by(id: health_history_id)
    user_patient = history.user_patient
    user_id = user_patient.user.id
    if user_patient.premium_account
      medical_expedients = history.medical_expedients.where(expedient_requests: {status: ExpedientRequest::APPROVED}).map { |m_expedient|
        get_format_expedient(m_expedient,user_id, history)
      }
    else
      # solo se permiten 3 expedientes en el historial de salud para la app
      medical_expedients = history.medical_expedients.where(expedient_requests: {status: ExpedientRequest::APPROVED}).first(3).map { |m_expedient|
        get_format_expedient(m_expedient,user_id, history)
      }
    end
    medical_expedients
  end

  def format_medical_history patientId
    patient = Patient.includes(:vaccine_books, :medical_expedient).find(patientId)
    medical_history = patient.medical_expedient.medical_history
    no_pathological = []
    obj_no_pathological = medical_history.no_pathological
    no_pathological << {name: "Tabaquismo", active: (obj_no_pathological.smoking) ? 1 : 0 , description: obj_no_pathological.smoking_description}
    no_pathological << {name: "Alcoholismo", active: (obj_no_pathological.alcoholism) ? 1 : 0 , description: obj_no_pathological.alcoholism_description}
    no_pathological << {name: "Toxicomanías", active: (obj_no_pathological.drugs) ? 1 : 0 , description: obj_no_pathological.drugs_description}
    no_pathological << {name: "¿Practica algún deporte?", active: (obj_no_pathological.play_sport) ? 1 : 0 , description: obj_no_pathological.play_sport_description}
    no_pathological << {name: "Escolaridad", active:  -1 , description: obj_no_pathological.scholarship}
    no_pathological << {name: "Estado Civil", active:  -1 , description: (obj_no_pathological.marital_status.nil?) ? "" : obj_no_pathological.marital_status.name}
    no_pathological << {name: "Religión", active: -1, description: obj_no_pathological.religion}
    no_pathological << {name: "Anotaciones", active: -1, description: obj_no_pathological.annotations}

    no_pathological_category = {
        name:'Antecedentes no patológicos',
        items:no_pathological
    }

    pathological_family_category = {
        name:'',
        items:[]
    }

    pathological_allergy_category = {
        name:'',
        items:[]
    }

    pathological_Surgery_category = {
        name:'',
        items:[]
    }

    pathological_medicine_category = {
        name:'',
        items:[]
    }

    pathological_condition_category = {
        name: '',
        items: []
    }

    pathological_family = []
    pathological_allergy = []
    pathological_Surgery = []
    pathological_medicine = []
    pathological_condition = []
   # obj_pathological = medical_history.pathologicals

    subcategories = SubcategoryPathological.joins(:pathologicals).where(:pathologicals => {medical_history_id: medical_history.id}).order("pathologicals.order").group(:id)
    subcategories.each do |category|

      pathologicals_sections = Pathological.where(medical_history_id: medical_history.id, subcategory_pathological_id: category.id).order(:order)
      pathologicals_sections.each do |pathological|
        d = pathological.section
        case pathological.section_type.to_s
          when "FamilyDisease"
              pathological_family << {name: "Diabetes", active: (d.diabetes) ? 1 : 0, description: d.diabetes_description}
              pathological_family << {name: "Hipertensión", active: (d.hypertension) ? 1 : 0, description: d.hypertension_description}
              pathological_family << {name: "Sobrepeso", active: (d.overweight) ? 1 : 0, description: d.overweight_description}
              pathological_family << {name: "Asma", active: (d.asthma) ? 1 : 0, description: d.asthma_description}
              pathological_family << {name: "Cáncer", active: (d.cancer) ? 1 : 0, description: d.cancer_description}
              pathological_family << {name: "Enfermedades Psiquiátricas", active: (d.psychiatric_diseases) ? 1 : 0, description: d.psychiatric_diseases_description}
              pathological_family << {name: "Enfermedades Neurológicas", active: (d.neurological_diseases) ? 1 : 0, description: d.neurological_diseases_description}
              pathological_family << {name: "Enfermedades Cardiovasculares", active: (d.cardiovascular_diseases) ? 1 : 0, description: d.cardiovascular_diseases_description}
              pathological_family << {name: "Enfermedades Broncopulmonares", active: (d.bronchopulmonary_diseases) ? 1 : 0, description: d.bronchopulmonary_diseases_description}
              pathological_family << {name: "Enfermedades Tiroideas", active: (d.thyroid_diseases) ? 1 : 0, description: d.thyroid_diseases_description}
              pathological_family << {name: "Enfermedades Renales", active: (d.kidney_diseases) ? 1 : 0, description: d.kidney_diseases_description}
              pathological_family << {name: "Otras enfermedades", active: (d.other_diseases) ? 1 : 0, description: d.diseases.map{|e| e.name}.join(", ")}
              pathological_family << {name: "Anotaciones", active: -1, description: d.annotations}
              pathological_family_category = {
                  name:"Familiares con Enfermedades",
                  items:pathological_family
              }
          when "PathologicalAllergy"
            pathological_allergy << {name: "¿Alérgico algún medicamento?", active: (d.allergy_medications) ? 1 : 0, description: d.allergy_medications_description}
            pathological_allergy << {name: "Otras alergias", active: -1, description: d.other_allergies}
            pathological_allergy_category = {
                name:"Alergias",
                items:pathological_allergy
            }
          when "Surgery"
            pathological_Surgery << { name: "Hospitalizaciones", active: -1, description: d.hospitalizations}
            pathological_Surgery << {name: "Cirugías", active: -1, description: d.description}
            pathological_Surgery << {name: "Transfusiones", active: -1, description: d.transfusions}
            pathological_Surgery_category = {
                name:"Cirugías y Hospitalizaciones",
                items:pathological_Surgery
            }
          when "PathologicalMedicine"
            pathological_medicine << {name: "Medicamentos de rutina", active: -1, description: d.medicaments.map{|m| m.comercial_name}.join(",")}
            pathological_medicine << {name: "Otros medicamentos", active: -1, description: d.other_medicines}
            pathological_medicine_category = {
                name:"Medicamentos de Rutina",
                items:pathological_medicine
            }
          when "PathologicalCondition"
            pathological_condition << {name: "Padecimientos recientes", active: -1, description: d.description}
            pathological_condition_category = {
                name: "Padecimientos recientes",
                items: pathological_condition
            }
        end

      end
    end

    vaccines_books = patient.vaccine_books

    vaccines_books_json = []
    vaccines_books.each do |books|
      all_vaccine_book = []
      books.vaccines.each do |vaccine|
        application_ages = vaccine.application_ages
        vaccine_doses = []
        application_ages.each do |application|

          application_vaccine = patient.vaccinations.find_by(application_age_id: application.id)
          date = ""
          date = I18n.localize(application_vaccine.vaccinate_date, :locale => :es, :format => :health_history_api) if !application_vaccine.nil?

          vaccine_doses << {
              doses: application.dose,
              application_age: application.age,
              dateApplied: date
          }
        end

        all_vaccine_book << {
            vaccine_name: vaccine.name,
            preventDisease: vaccine.prevent_disease,
            vaccines: vaccine_doses
        }
      end

      vaccines_books_json << {
          book_name: books.name,
          vaccineBook: all_vaccine_book
      }
    end

    doctor = patient.doctor

    ped_history_perinatal_category = nil
    ped_history_pathological_category = nil
    if active_license_feature doctor, Feature::PED
      ped_history_perinatal_json = []
      if !medical_history.perinatal_information.nil?
        # cargar información de pediatria
        ped_info = medical_history.perinatal_information
        ped_history_perinatal_json << {name: "Hospital / Clínica", active: -1, description: ped_info.hospital}
        city_name = ""
        city_name = ped_info.city.format_city_state_and_country if !ped_info.city.nil?
        ped_history_perinatal_json << {name: "Ciudad de Nacimiento", active: -1, description: city_name}

        birth_date = ""
        birth_date = patient.birth_day.strftime('%d/%m/%Y') if !patient.birth_day.nil?
        ped_history_perinatal_json << {name: "Fecha de Nacimiento", active: -1, description: birth_date}
        ped_history_perinatal_json << {name: "Altura al Nacer", active: -1, description: ped_info.height}
        ped_history_perinatal_json << {name: "Riesgos en el embarazo", active: -1, description: ped_info.pregnat_risks}
        ped_history_perinatal_json << {name: "Otra Información", active: -1, description: ped_info.other_info}

        ped_history_perinatal_category = {
            name: 'Antecedentes Perinatales',
            items: ped_history_perinatal_json
        }

      end

      ped_history_pathological_json = []
      info_parent = []
      if !medical_history.child_non_pathological_info.nil?
        # cargar información no patologicos
        ped_pathological_info = medical_history.child_non_pathological_info
        ped_history_pathological_json << {name: "Escolaridad del menor", active: -1, description: ped_pathological_info.child_academic_grade}
        ped_history_pathological_json << {name: "Tipo de vivienda", active: -1, description: ped_pathological_info.house_type}
        ped_history_pathological_json << {name: "Número de personas en la vivienda", active: -1, description: ped_pathological_info.number_people}


        medical_history.child_non_pathological_info.parent_infos.each do |parent|
          date = ""
          date = parent.birth_date.strftime('%d/%m/%Y') if !parent.birth_date.nil?

          ped_history_pathological_json << {name: parent.gender, active: -1, description: parent.first_name+" "+parent.last_name + ", "+date+", "+parent.academic_grade+", "+parent.blood_type.name}
        end


        ped_history_pathological_category = {
            name: 'Antecedentes No Patológicos',
            items: ped_history_pathological_json
        }
      end


    end

    gin_inherited_category = nil
    gin_obstetrical_category = nil
    if active_license_feature doctor, Feature::GIN
      if patient.gender.id == Gender::WOMEN
        inherited_family= medical_history.inherited_family_background
        if !inherited_family.nil?
          gin_inherited_family = []
          gin_inherited_family << {name: "Ginecopatías", active: (inherited_family.gynecological_diseases) ? 1 : 0, description: inherited_family.gynecological_diseases_description}
          gin_inherited_family << {name: "Mastopatías", active: (inherited_family.mammary_dysplasia) ? 1 : 0, description: inherited_family.mammary_dysplasia_description}
          gin_inherited_family << {name: "Gemelaridad", active: (inherited_family.twinhood) ? 1 : 0, description: inherited_family.twinhood_description}
          gin_inherited_family << {name: "Defectos congénitos y malformaciones", active: (inherited_family.congenital_anomalies) ? 1 : 0, description: inherited_family.congenital_anomalies_description}
          gin_inherited_family << {name: "Infertilidad", active: (inherited_family.infertility) ? 1 : 0, description: inherited_family.infertility_description}
          gin_inherited_family << {name: "Otros", active: (inherited_family.others) ? 1 : 0, description: inherited_family.others_description}
          gin_inherited_family << {name: "Anotaciones", active: -1, description: inherited_family.anotations}

          gin_inherited_category = {
              name: "Antecedentes Heredo - Familiares",
              items: gin_inherited_family
          }
        end

        if !medical_history.gynecologist_obstetrical_background.nil?
        gynecologist_obstetrical= medical_history.gynecologist_obstetrical_background
          gin_obstetrical = []
          gin_obstetrical << {name: "Menarca", active: -1, description: gynecologist_obstetrical.menstruation_start_age}
          gin_obstetrical << {name: "Ciclos", active: -1, description: gynecologist_obstetrical.cycles}
          gin_obstetrical << {name: "IVS", active: -1, description: gynecologist_obstetrical.ivs}
          gin_obstetrical << {name: "G", active: -1, description: gynecologist_obstetrical.gestations}
          gin_obstetrical << {name: "P", active: -1, description: gynecologist_obstetrical.natural_birth}
          gin_obstetrical << {name: "Ab", active: -1, description: gynecologist_obstetrical.abortion_number}
          gin_obstetrical << {name: "Ce", active: -1, description: gynecologist_obstetrical.caesarean_surgery}
          gin_obstetrical << {name: "Causas del aborto", active: -1, description: gynecologist_obstetrical.abortion_causes}

        date = ""
          date = gynecologist_obstetrical.fup.strftime('%d/%m/%Y') if !gynecologist_obstetrical.fup.nil?
          gin_obstetrical << {name: "FUP", active: -1, description: date}

          date = ""
          date = gynecologist_obstetrical.menses_last_date.strftime('%d/%m/%Y') if !gynecologist_obstetrical.menses_last_date.nil?
          gin_obstetrical << {name: "FUM", active: -1, description: date}

          date = ""
          date = gynecologist_obstetrical.papanicolau_last_date.strftime('%d/%m/%Y') if !gynecologist_obstetrical.papanicolau_last_date.nil?
          gin_obstetrical << {name: "FUPAP", active: -1, description: date}

          gin_obstetrical << {name: "Menopausia", active: -1, description: gynecologist_obstetrical.menopause}

          marital_status = medical_history.no_pathological.marital_status_id == MaritalStatus::MARRIED

          gin_obstetrical << {name: "¿Casada?", active: (marital_status) ? 1 : 0, description: ""}
          gin_obstetrical << {name: "Nombre del cónyuge", active: -1, description: gynecologist_obstetrical.spouse_name}
          gin_obstetrical << {name: "Información del cónyuge", active: -1, description: gynecologist_obstetrical.information_spouse}

          gin_obstetrical << {name: "¿Sexualmente activa?", active: (gynecologist_obstetrical.sexually_active) ? 1 : 0, description: ""}
          gin_obstetrical << {name: "Número de compañeros sexuales", active: -1, description: gynecologist_obstetrical.sexual_partners}
          gin_obstetrical << {name: "Tipo de relación", active: -1, description: gynecologist_obstetrical.relationship_type}
          gin_obstetrical << {name: "Enfermedades de transmisión sexual", active: -1, description: gynecologist_obstetrical.std}
          gin_obstetrical << {name: "Anotaciones", active: -1, description: gynecologist_obstetrical.anotations}

          gin_obstetrical_category = {
              name: "Antecedentes Gineco - Obstétricos",
              items: gin_obstetrical
          }
        end
      end
    end
    {
      noPathological: no_pathological_category,
      pathologicalFamily: pathological_family_category,
      pathologicalAllergy: pathological_allergy_category,
      pathologicalSurgery: pathological_Surgery_category,
      pathologicalMedicine: pathological_medicine_category,
      pathologicalCondition: pathological_condition_category,
      vaccinesBooks: vaccines_books_json,
      pedHistoryPerinatal: ped_history_perinatal_category,
      pedHistoryNoPathological: ped_history_pathological_category,
      ginInherited: gin_inherited_category,
      ginObstetrical: gin_obstetrical_category
    }
  end

  private
    def get_format_expedient(m_expedient, user_id, history)
      {
          id:0,
          healthHistoryId: history.id,
          patientName: m_expedient.patient.full_name,
          doctorName: m_expedient.doctor.full_name,
          expedientId: m_expedient.id,
          name: m_expedient.patient.full_name,
          gender: m_expedient.patient.gender.name,
          birthDate: m_expedient.patient.birth_day,
          weight: m_expedient.patient.weight,
          height: m_expedient.patient.height,
          bloodType: m_expedient.patient.blood_type.name,
          telephone: m_expedient.patient.phone,
          cellphone: m_expedient.patient.cell,
          email: m_expedient.patient.email,
          birthPlace: m_expedient.patient.birthplace,
          address: m_expedient.patient.address,
          cityName: m_expedient.patient.city.name,
          userId: user_id,
          doctorId: m_expedient.doctor.id,
          requestId: m_expedient.expedient_requests.find_by(health_history_id: history.id).id,

      }
    end

    def active_license_feature(doctor, feature)

      doctor.licenses.where(current: true).where(status: License::ACTIVE).each do |lic|
          lic.license_features.each do |lf|
            if lf.feature.key == feature
              return true
            end
          end
      end

      return false
    end

end
