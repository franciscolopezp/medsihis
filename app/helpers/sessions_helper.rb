module SessionsHelper
  # Permite iniciar sesión: (guarda la info en variables de sesión)
  def log_in(user)

    session[:user_id] = user.id
    current_user
  end

  def enter
    if current_user.is_doctor
      doctor = Doctor.find_by(user_id: session[:user_id])
      features = []
      session[:doctor_features] = features
      session[:doctor_id]  = doctor.id
      session[:doctor_username] = current_user.user_name
      current_office =  current_user.offices.first
      if current_office != nil
        session[:current_office_id] = current_office.id
      #elsif doctor.offices.active_offices.size > 0
       # current_office = doctor.offices[0]
        #session[:current_office_id] = current_office.id
      else
        session[:current_office_id] = ''
      end

        session[:active_notification] = true # para activar las notificaciones una unica vez, solo cuando se accede al sistema la primera vez

      #set timezone
      session[:time_zone] = doctor.time_zone.rails_name
      Time.zone = doctor.time_zone.rails_name


    end

    redirect_to admin_index_path


  end

  def current_user
    user_id = session[:user_id]
    @current_user ||= User.find_by(id: user_id)
  end

  def logged_in?
    !current_user.nil?
  end



  def get_doctor_session
    if doctor?
      return current_user.doctor
    else
      if assistant?
        return current_user.assistant.doctors.first
      end
    end
  end

  def get_assistant_session
    current_user.assistant
  end

  def get_doctors_active_assistant_shared
    doctors_active = []
    current_user.doctors.each do |doctor|
          doctors_active << doctor
    end
    doctors_active
    end



  def log_out
    session.delete(:user_id)
    session.delete(:user_type)
    reset_session
    @current_user = nil
  end

  def verify_session_login
    unless logged_in?
      redirect_to root_url
    end
  end

  def active_license_doctor? doctor
    active = doctor.licenses.exists?(status: License::ACTIVE)
    if active
      licenses_active = doctor.licenses.where(status: License::ACTIVE)
      active = false
      licenses_active.each do |license|
        unless license.active_to_day?
          license.update(status: License::ENDED) # si la licensia no esta activa el dia de hoy se actualiza a un estatus vencida
        else
          active = true
        end
      end
    end
    active
  end

  def active_license_user_patient? patient
    patient.accept_term_conditions and patient.is_active
  end



end
