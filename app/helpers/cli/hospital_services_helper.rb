module Cli::HospitalServicesHelper
  def get_all_services

    start_date = Time.parse(params[:fecha_ini])
    end_date = Time.parse(params[:fecha_f]) + 1.day

    services = HospitalService.all_services(start_date, end_date).select('id, code, name, sum(cont) as cont, sum(total) as total').order('name ASC').group('code,id,name')

    if params[:hospital_service_id].present?
      services = services.where('id = ?',params[:hospital_service_id])
    end

    if params[:code].present?
      services = services.where('code like ?',"%#{params[:code]}%")
    end

    services
  end


  def get_all_services_by_category
    start_date = Time.parse(params[:fecha_ini])
    end_date = Time.parse(params[:fecha_f]) + 1.day

    services = []
    OtherChargeItem.joins(:hospital_service => :hospital_service_category).where("other_charge_items.created_at >= '#{start_date}' AND other_charge_items.created_at < '#{end_date}'").select("COUNT(*) as number,hospital_service_category_id,hospital_service_categories.name as service,sum(total) as total").group(:hospital_service_category_id).each do |charge|
      services.push({
                        :id => charge.hospital_service_category_id,
                        :name => charge.service,
                        :total => charge.total,
                        :number => charge.number
                    })
    end
    ServiceCharge.joins(:hospital_service => :hospital_service_category).where("service_charges.created_at >= '#{start_date}' AND service_charges.created_at < '#{end_date}' ").select("COUNT(*) as number,hospital_service_category_id,hospital_service_categories.name as service,sum(residue) as total").group(:hospital_service_category_id).each do |charge|
      services.push({
                        :id => charge.hospital_service_category_id,
                        :name => charge.service,
                        :total => charge.total,
                        :number => charge.number
                    })
    end

    all_services = services.group_by { |item| [item[:id],item[:name]] }.values.flat_map { |items| items.first.merge(total: items.sum { |h| h[:total] },number: items.sum { |h| h[:number] }) }
    all_services
  end
end