module Cli::MedicamentHelper
  def update_log(lot_id,warehouse_id,is_income,quantity,user_id,traceable)
    medicament_log = MedicamentLog.new
    medicament_log.quantity = quantity
    medicament_log.income = is_income
    medicament_log.warehouse = Warehouse.find(warehouse_id)
    medicament_log.lot = Lot.find(lot_id)
    medicament_log.user = User.find(user_id)
    medicament_log.traceable = traceable
    medicament_log.save
  end
  def update_pharmacy_log(pharmacy_item_id,warehouse_id,is_income,quantity,user_id,traceable)
    medicament_log = PharmacyItemLog.new
    medicament_log.quantity = quantity
    medicament_log.income = is_income
    medicament_log.warehouse = Warehouse.find(warehouse_id)
    medicament_log.pharmacy_item = PharmacyItem.find(pharmacy_item_id)
    medicament_log.user = User.find(user_id)
    medicament_log.traceable = traceable
    medicament_log.save
  end
end