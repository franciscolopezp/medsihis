module Cli::IndicatorsHelper

  class IndicatorReport
    include DateHelper
    def initialize(view, start_date, end_date)
      @view = view
      @start_date = start_date
      @end_date = end_date

      @consultations = MedicalConsultation.joins(:patient, :office).all
      @expedients = MedicalExpedient.all
      @admissions = HospitalAdmission.joins(:triage, :attention_type).all
      @patients = Patient.joins(:city).all
      @transactions = Transaction.all

      if @start_date.present?
        @consultations = @consultations.where('medical_consultations.created_at >= ?',@start_date)
        @expedients = @expedients.where('medical_expedients.created_at >= ?',@start_date)
        @admissions = @admissions.where('hospital_admissions.created_at >= ?',@start_date)
        #@patients = @patients.where('patients.created_at >= ?',@start_date)
        @transactions = @transactions.where('transactions.created_at >= ?',@start_date)
      end


      if @end_date.present?
        @consultations = @consultations.where('medical_consultations.created_at < ?',@end_date)
        @expedients = @expedients.where('medical_expedients.created_at < ?',@end_date)
        @admissions = @admissions.where('hospital_admissions.created_at < ?',@end_date)
        #@patients = @patients.where('patients.created_at < ?',@end_date)
        @transactions = @transactions.where('transactions.created_at < ?',@end_date)
      end


      @colors = ["#F44336","#9C27B0","#3F51B5","#03A9F4","#009688","#8BC34A","#FFEB3B","#FF9800","#795548","#607D8B","#E91E63","#673AB7","#2196F3","#00BCD4","#4CAF50","#CDDC39","#9E9E9E","#FFC107","#FF5722"]
    end


    def ece_count
      count = MedicalExpedient.count
      @view.number_with_delimiter(count)
    end

    def ece_by_date
      resp = []
      if @expedients.size > 0
        first_expedient_date = @expedients.first.created_at
        last_expedient_date = @expedients.last.created_at
        start_month = first_expedient_date.beginning_of_month
        end_month = last_expedient_date.beginning_of_month + 1.month
        current_month = start_month
        while(current_month <= end_month)
          end_period = current_month + 1.month
          expedients_in_period = MedicalExpedient.where('created_at >= ? and created_at < ?', current_month, end_period).count
          if expedients_in_period > 0
            resp.push({
                          :period => month_abbr_year(current_month),
                          :count => expedients_in_period
                      })
          end
          current_month = end_period
        end
      end
      resp
    end

    def consultations_by_type
      resp = []
      types = ConsultationType.all
      types.each do |t|
        resp.push({
            :type => t.name,
            :count => @consultations.where(:consultation_type_id => t.id).count
                  })
      end

      resp
    end

    def consultations_by_age
      res = []

      today = Date.today
      lac_limit = today - 1.year
      presc_limit = today - 4.year
      esc_limit = today - 9.year
      adol_limit = today - 19.year
      adul_limit = today - 59.year

      res.push({
          :name => "Lact.",
          :count => @consultations.where('patients.birth_day > ?', lac_limit).count
               })

      res.push({
                   :name => "Preesc.",
                   :count => @consultations.where('patients.birth_day > ? AND patients.birth_day <= ?', presc_limit, lac_limit).count
               })

      res.push({
                   :name => "Esc.",
                   :count => @consultations.where('patients.birth_day > ? AND patients.birth_day <= ?',  esc_limit, presc_limit).count
               })

      res.push({
                   :name => "Adol.",
                   :count => @consultations.where('patients.birth_day > ? AND patients.birth_day <= ?', adol_limit, esc_limit).count
               })

      res.push({
                   :name => "Adul.",
                   :count => @consultations.where('patients.birth_day > ? AND patients.birth_day <= ?', adul_limit, adol_limit).count
               })

      res.push({
                   :name => "A. Mayor",
                   :count => @consultations.where('patients.birth_day < ?', adul_limit).count
               })


      res
    end


    def diseases_by_city
      resp = []
      limit = 10
      query_dates = nil

      if @start_date.present? && @end_date.present?
        query_dates = " mc.date_consultation >= '"+@start_date.strftime('%Y-%m-%d')+"' AND "
        query_dates << " mc.date_consultation < '"+@end_date.strftime('%Y-%m-%d')+"' "
      end




      query_diseases = "SELECT
  count(d.id) as count,
  d.id, d.name
FROM medical_consultations mc
  JOIN diseases_medical_consultations  mcd on mc.id = mcd.medical_consultation_id
  JOIN diseases d on mcd.disease_id = d.id"

      if query_dates.present?
        query_diseases << " WHERE " + query_dates
      end

      query_diseases << " GROUP BY d.name, d.id
 order by count DESC LIMIT  #{limit}"


      diseases = Disease.find_by_sql(query_diseases)


      idx_color = 0
      diseases.each do |d|
        color = @colors[idx_color]

        if color.present?
          idx_color += 1
        else
          color = @colors[0]
          idx_color = 1
        end


        query_cities = "SELECT
  count(d.id) as number,
  c.name as city, c.lat, c.lng
FROM medical_consultations mc
  JOIN medical_expedients exp on mc.medical_expedient_id = exp.id
  JOIN patients p on exp.patient_id = p.id
  LEFT JOIN cities c on p.city_id = c.id
  JOIN diseases_medical_consultations  mcd on mc.id = mcd.medical_consultation_id
  JOIN diseases d on mcd.disease_id = d.id
  WHERE d.id = #{d[:id]}"

        if query_dates.present?
          query_cities << " AND " + query_dates
        end

        query_cities << " GROUP BY d.id, d.name, c.name, c.lat, c.lng
ORDER BY d.name ASC"

        cities = MedicalConsultation.find_by_sql(query_cities)

        data_cities = []
        cities.each do |c|
          data_cities.push({
              :city => c[:city],
              :count => c[:number],
              :lat => c[:lat],
              :lng => c[:lng]
                           })
        end

        resp.push({
            :color => color,
            :disease => d[:name],
            :count => d[:count],
            :cities => data_cities
                  })

      end


      resp
    end

    def admissions_by_attention
      resp = []
      data = @admissions.select('count(hospital_admissions.id) as count, attention_type_id, attention_types.name as attention')
                 .group('attention_type_id, attention_types.name')

      idx_color = 0
      data.each do |a|
        color = @colors[idx_color]
        if !color.present?
          idx_color = 0
          color = @colors[idx_color]
        end


        resp.push({
            :attention => a.attention_type.name,
            :count => a.count,
            :color => color
                  })

        idx_color += 1
      end
      resp
    end

    def consultations_by_office
      resp = []
      data = @consultations.select('count(medical_consultations.id) as count, office_id, offices.name')
                 .group('office_id, offices.name')

      idx_color = @colors.size - 1
      data.each do |a|
        color = @colors[idx_color]
        if !color.present?
          idx_color = @colors.size - 1
          color = @colors[idx_color]
        end
        resp.push({
                      :office => a.name,
                      :count => a.count,
                      :color => color
                  })

        idx_color -= 1
      end
      resp
    end

    def admissions_by_day_triage
      resp = []
      if @admissions.size > 0
        start_date = @admissions.first.created_at.beginning_of_day
        end_date = @admissions.last.created_at.beginning_of_day + 1.day


        triages = Triage.all
        triages.each do |t|
          data = []
          total = 0
          date = start_date

          while date < end_date
            count = @admissions.where('hospital_admissions.created_at >= ? and hospital_admissions.created_at < ?',date, date + 1.day).where(:triage_id => t.id).count
            data.push({
                          :date => only_date(date),
                          :count => count
                      })
            date = date + 1.day
            total += count
          end


          resp.push({
                        :triage => t.level,
                        :color => t.color,
                        :data => data,
                        :count => total
                    })


        end
      end
      resp
    end


    def admissions_by_day_count
      resp = []
      if @admissions.size > 0
        start_date = @admissions.first.created_at.beginning_of_day
        end_date = @admissions.last.created_at.beginning_of_day + 1.day
        date = start_date


        while date < end_date
          count = @admissions.where('hospital_admissions.created_at >= ? and hospital_admissions.created_at < ?',date, date + 1.day).count
          resp.push({
                        :date => only_date(date),
                        :count => count
                    })
          date = date + 1.day
        end
      end
      resp
    end

    def patients_by_city
      resp = []
      data = @patients.select('count(patients.id) as count, cities.name as city').order('cities.name ASC')
                 .group('cities.name')
      data.each do |a|
        resp.push({
            :count => a[:count],
            :city => a[:city]
        })
      end
      resp
    end

    def finance
      resp = []
      incomes = @transactions.where(:t_type => 1).sum(:amount)
      expenses = @transactions
                     .joins('JOIN other_movements on transactions.chargeable_id = other_movements.id')
                     .where('other_movements.movement_type_id != ?', 21)
                     .where(:t_type => 0).sum(:amount)

      balance = incomes - expenses

      resp.push({
                    :label => "Ingresos",
                    :color => "#4CAF50",
                    :data => [{:amount => incomes}]
                })

      resp.push({
                    :label => "Egresos",
                    :color => "#F44336",
                    :data => [{:amount => expenses * (-1)}]
                })


      resp.push({
                    :label => "Balance",
                    :color => "#2196F3",
                    :data => [{:amount => balance}]
                })

      resp
    end


    def to_json
      {
          :ece_count => ece_count,
          :ece_by_date => ece_by_date,
          :consultations_by_type => consultations_by_type,
          :consultations_by_age => consultations_by_age,
          :consultations_by_office => consultations_by_office,
          :diseases_by_city => diseases_by_city,
          :admissions_by_day_triage => admissions_by_day_triage,
          :admissions_by_day_count => admissions_by_day_count,
          :admissions_by_attention => admissions_by_attention,
          :patients_by_city => patients_by_city,
          :finance => finance
      }
    end

  end

end