module Api::V1::AppointmentsDoc
  extend BaseDoc

  def_param_group :general do
    param :authorization, String, desc: "Token del Sistema", required: true
    param :user_id, Integer, desc: "Id del usuario"
  end

  def_param_group :data_activity do
    param :start_date, String, desc: "Fecha inicio de la cita", required: true
    param :end_date, String, desc: "Fecha final de la cita", required: true
    param :name, String, desc: "Titulo de la cita", required: true
    param :details, String, desc: "Detalle de la cita", required: true
    param :doctor_name, String, desc: "Nombre del doctor", required: true
    param :doctor_id, String, desc: "Id del doctor", required: true
    param :activity_id, Integer, desc: "Id de la cita", required: true
    param :office_id, Integer, desc: "Id del consultorio", required: true
  end

  doc_for :index do
    api! 'Permite obtener todas las actividades de tipo cita que no se an realizado o la fecha de terminación no halla pasado'

    description <<-EOS
    === Descripción
     Permite autenticarte al sistema mediante un Token.
     El Token de test es:
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoiZWRnYXIifQ."
    EOS

    formats ['json']
    error 500, "Server crashed", meta: {status: "Estatus 500", message: "Error interno del servidor"}

    example '
   === Petición al servidor ===
{
    authorization: "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoiZWRnYXIifQ.",
    user_id: 1,
}'
    example '
   === Respuesta del servidor existe un problema ===
{
    status: "error",
    response: {},
    message: "Ups tenemos problemas"
    errors: ["Token invalido"]
}'

    example '
    === Respuesta del servidor cuando la autenticación es correcto ===
{
    status: :ok,
    response: [
  {
    start_date: "10-10-2016 12:40:00",
    end_date: "10-10-2016 13:10:00",
    name: "Cita con Edgar Rodriguez",
    details: "Estudios clinicos",
    doctor_name: "Dr. Uriel",
    doctor_id: "2",
    activity_id: 2
  },{
    start_date: "10-10-2016 12:40:00",
    start_date: "10-10-2016 13:10:00",
    name: "Cita con Alan Morfin",
    details: "Estudios clinicos",
    doctor_name: "Dr. Uriel",
    doctor_id: "2",
    activity_id: 3
  },
],
    message: "",
    errors: []
}
  '

    param_group :general
    param_group :data_activity

  end

  doc_for :create do
    api! 'Permite obtener todas las actividades de tipo cita que no se an realizado o la fecha de terminación no halla pasado'

    description <<-EOS
    === Descripción
     Permite autenticarte al sistema mediante un Token.
     El Token de test es:
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoiZWRnYXIifQ."
    EOS

    formats ['json']
    error 500, "Server crashed", meta: {status: "Estatus 500", message: "Error interno del servidor"}

    example '
   === Petición al servidor ===
{
    authorization: "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoiZWRnYXIifQ.",
    user_id: 1,
    start_date: "10-10-2016 12:40:00",
    end_date: "10-10-2016 13:10:00",
    name: "Cita con Edgar Rodriguez",
    details: "Estudios clinicos",
    doctor_name: "Dr. Uriel",
    doctor_id: 2,
    office_id: 2
}'

    end

end
