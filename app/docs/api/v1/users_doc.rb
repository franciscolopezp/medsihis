module Api::V1::UsersDoc
  extend BaseDoc

  def_param_group :general do
    param :authorization, String, desc: "Token del Sistema", required: true
  end
# only: [:user_name, :email], include: [doctor: {only: [:phone, :name, :last_name, :birth_date]}])
  def_param_group :data_user do
    param :user_name, String, desc: "Nombre del usuario", required: true
    param :email, String, desc: "Correo electronico", required: true
    param :user, Hash do
      param :phone, String, desc: "Número telefonico", required: true
      param :name, String, desc: "Nombre del doctor", required: true
      param :last_name, String, desc: "Apellido del doctor", required: true
      param :birth_date, String, desc: "Formato de fecha", required: true
    end
  end

  doc_for :login do
    api! 'Permite autenticarte en el sistema'

    description <<-EOS
    === Descripción
     Permite autenticarte al sistema mediante un Token.
     El Token de test es:
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoiZWRnYXIifQ."
    EOS

    formats ['json']
=begin
    error 401, "Unauthorized", meta: {status: "status", message: "error_message"}
    error 404, "Not Found", meta: {status: "status", message: "error_message"}
=end
    error 500, "Server crashed", meta: {status: "Estatus 500", message: "Error interno del servidor"}

    example '
   === Petición al servidor ===
{
    authorization: "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoiZWRnYXIifQ.",
    user_name: "erodriguez",
    password: "medsi123"
}'
    example '
   === Respuesta del servidor existe un problema ===
{
    status: "error",
    response: {},
    message: "Ups tenemos problemas"
    errors: ["Token invalido"]
}'

    example '
    === Respues del servidor cuando los datos de acceso son incorrectos
{
    status: :ok,
    response: {},
    message: ""
    errors: ["Usuario o contraseña incorrecto"]
}
'

    example '
    === Respuesta del servidor cuando la autenticación es correcto ===
{
    status: :ok,
    response: {
            id: 1,
            name: EDGAR URIEL,
            lastName: RODRIGUEZ CAMARA,
            birthDate: "10-11-1992",
            gender: "HOMBRE",
            email: "edgarrodriguez@stj2.com.mx",
            username: "doctor",
            type: "DOCTOR",
            phone: "9999999999"
        },
    message: "",
    errors: []
}
  '

    param_group :general
    param :user_id, Integer, desc: "Id del usuario"

  end

  doc_for :update_profile do
    api! 'Permite actualizar los datos de perfil del usuario registrado'

    description <<-EOS
    === Descripción
     Permite actualizar los datos de perfil del usuario registrado.
     El Token de test es:
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoiZWRnYXIifQ."
    EOS

    formats ['json']

    error 500, "Server crashed", meta: {status: "Estatus 500", message: "Error interno del servidor"}

    example '
   === Petición al servidor ===
{
      authorization: "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoiZWRnYXIifQ.",
      user_name: "Edgar",
      email: "edgarrodriguez@stj2.com.mx",
      name: "Edgar Uriel",
      last_name: "Rodriguez Camara",
      cellphone: "9999999999",
      birth_date: "1992-11-10"
}'
    example '
   === Respuesta del servidor existe un problema ===
{
    status: "error",
    errors: ["Error en el servidor"]
}'

    example '
    === Respuesta del servidor cuando No existe errores ===
{
   status: :ok,
   response: true
}
  '

    param_group :general
    param :user_id, Integer, desc: "Id del usuario"
    param_group :data_user

  end



end