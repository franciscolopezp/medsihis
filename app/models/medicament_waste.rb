class MedicamentWaste < ActiveRecord::Base
  has_one :medicament_log, as: :traceable
  belongs_to :warehouse
  belongs_to :lot
  belongs_to :user
  belongs_to :item_movement
end
