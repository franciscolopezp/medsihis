class Certificate < ActiveRecord::Base
  belongs_to :office
  belongs_to :patient
  belongs_to :type_certificate

  validates :name, :container_html, :office, :type_certificate, presence: true

end
