class ExpedientRequest < ActiveRecord::Base
  belongs_to :health_history
  belongs_to :medical_expedient

  scope :by_patient, -> (patient_id){ joins(:health_history).where('health_histories.user_patient_id = '+patient_id.to_s) }

  REJECTED = -1
  PENDING = 0
  APPROVED = 1

end
