class LicenseFeature < ActiveRecord::Base
  belongs_to :feature
  belongs_to :license

  validates :cost, presence: :true
end
