class OtherMovement < ActiveRecord::Base
  has_many :transactions, as: :chargeable
  belongs_to :movement_type

  def get_transaction
    self.transactions.first
  end
end
