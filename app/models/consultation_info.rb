class ConsultationInfo < ActiveRecord::Base
  belongs_to :medical_consultation
  belongs_to :consultation_field
end
