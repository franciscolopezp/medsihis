class AdmissionTransaction < ActiveRecord::Base
  has_many :transactions, as: :chargeable
  belongs_to :user
  belongs_to :hospital_admission
  belongs_to :clinic_invoice

end
