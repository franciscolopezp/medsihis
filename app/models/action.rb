class Action < ActiveRecord::Base
  belongs_to :section
  has_and_belongs_to_many :roles, :join_table => :roles_actions
  has_one :menu_item_child
end
