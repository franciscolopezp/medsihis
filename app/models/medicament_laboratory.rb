class MedicamentLaboratory < ActiveRecord::Base
  has_many :medicaments
end
