class PaymentWay < ActiveRecord::Base
  has_many :invoices
  has_many :clinic_invoices
  has_many :admission_invoices
end
