class PathologicalMedicine < ActiveRecord::Base
  has_one :pathological, as: :section
  has_and_belongs_to_many :medicaments
end
