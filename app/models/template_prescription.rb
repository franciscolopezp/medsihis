class TemplatePrescription < ActiveRecord::Base
  has_many :offices

  TEMPLATE_HORIZONTAL_A = 1
  TEMPLATE_VERTICAL_B = 2
  TEMPLATE_VERTICAL_SIZE_CARTA_C = 3

end
