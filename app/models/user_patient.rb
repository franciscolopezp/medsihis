class UserPatient < ActiveRecord::Base
  belongs_to :user
  belongs_to :city
  belongs_to :gender
  has_and_belongs_to_many :activities

  has_many :health_histories
  has_many :medical_expedients, through: :health_histories

  has_and_belongs_to_many :doctors

  validates :name, :last_name, :gender, presence: true
  validates_inclusion_of :premium_account, :in => [true, false]

  accepts_nested_attributes_for :user

  def full_name
    name+" "+last_name
  end

end
