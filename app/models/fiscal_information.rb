# La clase "FiscalInformation" representa un registro de tipo "Información Fiscal".
class FiscalInformation < ActiveRecord::Base

  mount_uploader :certificate_stamp, FiscalInformationUploader
  mount_uploader :certificate_key, FiscalInformationUploader
  before_create :encrypt_password

  # @!attribute city
  # @return [City] Registro de tipo "Ciudad" asociado a la información fiscal.
  belongs_to :city

  # @!attribute doctor
  # @return [Doctor] Registro de tipo "Doctor" al cual pertenece la información fiscal.
  has_one :doctor
  
  # Método que retorna la contraseña fiscal encriptada.
  #
  # @return [string] String que representa la contraseña fiscal del doctor encriptada.
  def encrypt_password
    if !self.certificate_password.nil? and !self.certificate_password.blank?
      self.certificate_password= AESCrypt.encrypt(self.certificate_password, ENV['TS_KEY'])
    end
    self.certificate_password
  end

  # Método que retorna la contraseña fiscal desencriptada.
  #
  # @return [string] String que representa la contraseña fiscal del doctor desencriptada.
  def decrypt_password
    self.certificate_password= AESCrypt.decrypt(self.certificate_password, ENV['TS_KEY'])
  end
end
