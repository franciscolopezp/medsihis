class MedicalAnnotationLinkedField < ActiveRecord::Base
  has_and_belongs_to_many :medical_annotation_types, :join_table => "medical_annotations_linked_fields"


  has_many :annotation_linked_fields
  has_many :medical_annotations, :through => :annotation_linked_fields
end
