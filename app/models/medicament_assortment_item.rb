class MedicamentAssortmentItem < ActiveRecord::Base
  has_one :medicament_log, as: :traceable
  belongs_to :lot
  belongs_to :medicament
  belongs_to :medicament_assortment
end
