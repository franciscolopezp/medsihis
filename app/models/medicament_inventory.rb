class MedicamentInventory < ActiveRecord::Base
  belongs_to :warehouse
  belongs_to :lot
  belongs_to :user
end
