class Ability
  include CanCan::Ability

  def initialize(user)
=begin
    user ||= User.new # guest user (not logged in)
    user.role.module_permissions.each do |secper|
      can :index, secper.section.name.constantize
      secper.action_permissions.each do |actperm|
        can actperm.action.name.to_sym, actperm.action.section.name.constantize
      end
    end
=end
    user.role.actions.each do |action|
      can action.name.to_sym, action.section.name.constantize
    end
  end


end
