class MedicalExpedient < ActiveRecord::Base
  belongs_to :patient
  belongs_to :user
  has_many :medical_consultations
  has_many :clinical_analysis_orders
  has_one :medical_history
  has_many :pregnancies
  has_many :vital_signs, through: :medical_consultations

  has_many :expedient_requests
  has_many :health_histories, :through => :expedient_requests
  has_and_belongs_to_many :patient_valorations, :join_table => 'expedients_valorations'
  accepts_nested_attributes_for :medical_history

  #scope :last_folio_analysis_by_doctor_id, lambda {|doctor| MedicalExpedient.joins(:clinical_analysis_orders).where(doctor_id: doctor).maximum("clinical_analysis_orders.folio")}
  scope :search_last_folio_by_current_user, lambda {  MedicalExpedient.maximum("folio").to_i }
  #@current_user.doctor.id
  before_create :generate_code

  def generate_code
    self.code = loop do
      random_code = DateTime.now.strftime('%Q')
      break random_code unless MedicalExpedient.exists?(code: random_code)
    end
  end

  def self.generate_folio
    last_folio = MedicalExpedient.search_last_folio_by_current_user
    last_folio + 1
  end

  def last_consult
    last_consult = self.medical_consultations.order('date_consultation DESC').limit(3).first
    if last_consult != nil
      last_consult.date_consultation.strftime('%d/%m/%Y')
    else
      ''
    end
  end
end
