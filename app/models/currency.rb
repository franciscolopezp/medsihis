class Currency < ActiveRecord::Base
  has_many :currency_exchanges
  has_many :clinic_infos, through: :currency_exchanges
  has_many :general_accounts
  has_many :dc_balances
  has_many :daily_cashes, :through => :dc_balances

  has_many :accounts

  has_many :transactions

  def name_iso
    self.name+' ('+self.iso_code+')'
  end
end
