class GeneralAccount < ActiveRecord::Base
  belongs_to :bank
  belongs_to :currency
end
