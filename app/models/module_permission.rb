class ModulePermission < ActiveRecord::Base
  belongs_to :role
  belongs_to :section
  has_many :action_permissions
  belongs_to :parent, :class_name => 'ModulePermission'
  has_many :children, :class_name => 'ModulePermission', :foreign_key => 'parent_id'
end
