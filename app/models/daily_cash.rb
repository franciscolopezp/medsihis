# La clase "DailyCash" representa un registro de tipo "Corte de Caja".
class DailyCash < ActiveRecord::Base

  # Estado de un corte de caja - Abierto.
  OPEN = 1
  # Estado de un corte de caja - Cerrado.
  CLOSED = 0

  # Operación al cerrar un corte de caja - Cerrar.
  CLOSE_ONLY = 1
  # Operación al cerrar un corte de caja - Retirar.
  WITHDRAW = 2
  # Operación al cerrar un corte de caja - Transferir.
  TRANSFER = 3

  # @!attribute account
  # @return [Account] Registro de tipo "Cuenta de Médico" al cual pertenece el corte de caja.
  belongs_to :account

  # @!attribute doctor
  # @return [Doctor] Registro de tipo "Doctor" al cual pertenece el corte de caja.
  has_one :doctor, through: :account

  # @!attribute user
  # @return [User] Registro de tipo "Usuario" al cual pertenece el corte de caja, es quien fue responsable de abrir el corte de caja.
  belongs_to :user

  # @!attribute movements
  # @return [array(Transaction)] Array de registros de tipo "Transacción" que pertenecen al corte de caja.
  has_and_belongs_to_many :movements, :class_name => 'Transaction', :foreign_key => 'daily_cash_id'

  # @!attribute close_user
  # @return [User] Registro de tipo "Usuario" el cual fue responsable de cerrar el corte de caja.
  belongs_to :close_user, :class_name => 'User', :foreign_key => 'close_user_id'

  has_many :dc_balances
  has_many :currencies, :through => :dc_balances

  
  # Método que filtra registros de tipo "Corte de Caja", restringe a que los registros pertenezcan a cuentas de determinado consultorio y que se encuentren en estado "Abierto".
  #
  # @param office_id [integer] Entero que representa el "id" de un registro de tipo "Consultorio".
  scope :open_by_office, -> (office_id){
    joins(account:[accounts_offices: :office])
        .where(offices:{id: office_id.to_s})
        .where(daily_cashes:{status: DailyCash::OPEN})
  }

  #la asistente compartida debe ver cortes de caja activos de un conjunto de consultorios, que ella/el haya abierto
  scope :open_by_offices, -> (offices,user_id){
    joins(account:[accounts_offices: :office])
        .where('daily_cashes.user_id = ?',user_id)
        .where('accounts_offices.office_id IN ('+offices.join(',')+')')
        .where(daily_cashes:{status: DailyCash::OPEN})
  }

  # Método que filtra registros de tipo "Corte de Caja", restringe a que los registros pertenezcan a una cuenta y que se encuentren en estado "Abierto".
  #
  # @param account_id [integer] Entero que representa el "id" de un registro de tipo "Cuenta".
  scope :open_by_account, -> (account_id){
    where({account_id: account_id, status: DailyCash::OPEN})
  }

  scope :last_by_account, -> (account_id,status){
    where({account_id: account_id, status: status}).limit(2).order('created_at DESC')
  }

  # Método que filtra registros de tipo "Corte de Caja", restringe a que los registros pertenezcan a un usuario y que se encuentren en estado "Abierto".
  #
  # @param user_id [integer] Entero que representa el "id" de un registro de tipo "Usuario".
  scope :current, -> (user_id){
    where({user_id:user_id})
    .where({status:DailyCash::OPEN})
  }

  # Método que filtra registros de tipo "Corte de Caja", restringe a que los registros pertenezcan a una cuenta y que haya sido abierto en una fecha previa a la actual.
  #
  # @param account_id [integer] Entero que representa el "id" de un registro de tipo "Cuenta de Médico".
  scope :previous_open, -> (account_id){
    where('start_date < ?',DateTime.now.to_date)
    .where({account_id: account_id, status: DailyCash::OPEN})
  }

  def get_balance currency_id
    balance = self.dc_balances.where('currency_id = ?',currency_id).first
    if balance.nil?
      return 0
    else
      return balance.end
    end
  end

  def get_start_balance
    start = ""
    self.dc_balances.each do |dc|
      start   += dc.currency.symbol+" "+dc.start.to_s + " "+dc.currency.iso_code + "<br>"
    end
    start
  end

  def get_end_balance
    end_bal = ""
    self.dc_balances.each do |dc|
      end_bal += dc.currency.symbol+" "+dc.end.to_s + " "+dc.currency.iso_code + "<br>"
    end
    end_bal
  end
end
