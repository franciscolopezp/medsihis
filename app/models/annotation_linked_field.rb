class AnnotationLinkedField < ActiveRecord::Base
  belongs_to :medical_annotation
  belongs_to :medical_annotation_linked_field
end
