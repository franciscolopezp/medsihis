class ChargeReportStudy < ActiveRecord::Base
  belongs_to :cabinet_report

  has_one :transaction_item, :class_name => 'Transaction', as: :chargeable

  def get_study
    if self.cabinet_report.nil?
      return 'No disponible'
    end
    self.cabinet_report.clinical_analysis_order.clinical_studies.first.name
  end

  def get_patient
    if self.cabinet_report.nil?
      return 'No disponible'
    end
    self.cabinet_report.clinical_analysis_order.medical_expedient.patient.full_name
  end

end
