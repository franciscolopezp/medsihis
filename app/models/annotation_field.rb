class AnnotationField < ActiveRecord::Base
  belongs_to :medical_annotation
  belongs_to :medical_annotation_field
end
