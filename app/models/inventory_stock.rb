class InventoryStock < ActiveRecord::Base
  has_one :medicament_log, as: :traceable
  belongs_to :lot
  belongs_to :inventory
end
