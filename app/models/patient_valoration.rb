class PatientValoration < ActiveRecord::Base
  has_many :patient_valoration_fields
  belongs_to :valoration_type
  belongs_to :patient
  belongs_to :user
  has_and_belongs_to_many :medical_expedients, :join_table => 'expedients_valorations'
  has_and_belongs_to_many :medical_consultations, :join_table => 'consultations_valorations'
  has_and_belongs_to_many :hospital_admissions, :join_table => 'hospitalizations_valorations'
  has_and_belongs_to_many :medical_annotations, :join_table => 'medical_annotations_valorations'
  has_and_belongs_to_many :waiting_room_items, :join_table => 'wroom_items_valorations'
end
