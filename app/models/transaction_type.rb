class TransactionType < ActiveRecord::Base
  has_many :transactions

  CONSULTATION = 1
  TRANSFER = 2
  OTHER = 3
  REPORT = 4
  HOSPITAL_ADMISSION = 5
  OTHER_CHARGES = 6

end
