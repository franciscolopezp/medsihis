# La clase "IndexComment" representa un registro de tipo "Cédula Profesional".
class IndexComment < ActiveRecord::Base

  # Tipo de Persona que emite un comentario de la aplicación - Doctor
  DOCTOR = 1

  # Tipo de Persona que emite un comentario de la aplicación - Paciente
  PATIENT = 2

  mount_uploader :photo, IndexCommentUploader

  

  def full_name(include_prefix)
    name = ''
    if include_prefix
      name << self.prefix + ' '
    end
    name << self.first_name + ' '
    name << self.last_name
  end
end
