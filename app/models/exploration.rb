class Exploration < ActiveRecord::Base
  belongs_to :medical_consultation
  belongs_to :exploration_type

  def showPhysical
    self.exploration_type_id == ExplorationType::PHYSICAL || self.exploration_type_id == ExplorationType::FULL
  end

  def showPelvic
    self.exploration_type_id == ExplorationType::PELVIC || self.exploration_type_id == ExplorationType::FULL
  end

end
