# La clase "Country" representa un registro de tipo "País"
class Country < ActiveRecord::Base

  # @!attribute states
  # @return [array(State)] Array de registros de tipo "Estado" los cuales pertenecen al país.
  has_many :states
end
