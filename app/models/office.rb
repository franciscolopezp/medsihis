class Office < ActiveRecord::Base
  belongs_to :city
  belongs_to :hospital
  has_many :office_times, dependent: :destroy
  has_many :medical_consultations, dependent: :destroy
  belongs_to :doctor
  has_and_belongs_to_many :activities
  has_and_belongs_to_many :users
  has_many :assistants
  belongs_to :template_prescription
  has_many :office_permissions
  has_and_belongs_to_many :accounts
  has_many :consultation_costs
  has_many :clinical_analysis_orders
  has_many :certificates

  mount_uploader :logo, OfficeUploader
  #accepts_nested_attributes_for :logo, :logo_cache, :remove_logo
  #attr_accessible :logo, :logo_cache, :remove_logo

  validates :folio, unique_folio_office: true, :on => :update
  validates :folio, numericality: { only_integer: true},  allow_blank: true, allow_nil: true

  scope :current_office, lambda {|doctor| includes(:office_times).where(:office_times => {day: DateTime.now.wday}).where('office_times.start <= ?',Time.now.to_formatted_s(:time)+":00").where('office_times.end >= ?',Time.now.to_formatted_s(:time)+":00").where(doctor_id: doctor.id)}
  scope :not_template, lambda {where(template_prescription: nil )}
  scope :has_template, lambda {where.not(template_prescription: nil )}
  scope :active_offices, lambda {where(active: true)}
end
