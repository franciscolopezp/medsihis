class AssortmentPharmacyItem < ActiveRecord::Base
  has_one :pharmacy_item_log, as: :traceable
  belongs_to :pharmacy_item
  belongs_to :medicament_assortment
end
