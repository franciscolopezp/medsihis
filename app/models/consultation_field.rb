class ConsultationField < ActiveRecord::Base
  def get_type
    Const::consultation_field_types[self.field_type]
  end
end
