class VitalSign < ActiveRecord::Base
  has_one :medical_consultation
  belongs_to :imc_adult
  belongs_to :imc_children_max19_year
  validates :glucose, :height,
            :imc, :temperature, :weight, :head_circumference, :systolic_pressure, :diastolic_pressure, numericality: { only_float: true, greater_than: 0 },  allow_blank: true, allow_nil: true

  validates :systolic_pressure, :diastolic_pressure, numericality: { only_integer: true, greater_than: 0 },  allow_blank: true, allow_nil: true

  validates :systolic_pressure, :presence => { if: :presence_diastolic_pressure?, :message => "no puede estar en blanco" }
  validates :diastolic_pressure, :presence => { if: :presence_systolic_pressure?, :message => "no puede estar en blanco" }

  def blood_pressure
    systolic_pressure.to_s + " / " + diastolic_pressure.to_s unless systolic_pressure.nil? and diastolic_pressure.nil?
  end

  def blood_pressure_graph
    if systolic_pressure.present? and diastolic_pressure.present?
      (systolic_pressure.to_f/diastolic_pressure.to_f).round(3)
    end
  end

  def presence_diastolic_pressure?
      true if diastolic_pressure.present?
  end

  def presence_systolic_pressure?
      true if systolic_pressure.present?
  end
end
