class MedicamentCategory < ActiveRecord::Base
  has_many :pharmacy_items
end
