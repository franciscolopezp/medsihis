class Pregnancy < ActiveRecord::Base
  belongs_to :medical_expedient
  has_and_belongs_to_many :medical_consultations
  has_one :birth_info, :dependent => :destroy
  has_one :abortion, :dependent => :destroy
  has_many :ultrasounds
  belongs_to :user
  scope :current_by_expedient, -> (medical_expedient_id){
     where('medical_expedient_id = ? AND active = ?',medical_expedient_id,true)
  }
end
