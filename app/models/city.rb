# La clase "City" representa un registro de tipo "Ciudad".
class City < ActiveRecord::Base

  # @!attribute state
  # @return [State] Registro de tipo "Estado" al cual pertenece la ciudad.
  belongs_to :state
  has_many :clinic_infos
  has_many :emisor_fiscal_informations
  # @!attribute patients
  # @return [array(Patients)] Array de registros de tipo "Paciente" a los cuales se les ha asignado la ciudad.
  has_many :patients
  has_many :general_fiscal_infos
  # @!attribute offices
  # @return [array(Office)] Array de registros de tipo "Consultorio" a los cuales se les ha asignado la ciudad.
  has_many :offices

  # @!attribute doctors
  # @return [array(Doctor)] Array de registros de tipo "Doctor" a los cuales se les ha asignado la ciudad.
  has_many :people

  # @!attribute fiscal_informations
  # @return [array(FiscalInformation)] Array de registros de tipo "Información Fiscal" a los cuales se les ha asignado la ciudad.
  has_many :fiscal_informations

  # @!attribute assistants
  # @return [array(Assistant)] Array de registros de tipo "Asistente" a los cuales se les ha asignado la ciudad.


  # @!attribute laboratories
  # @return [array(Laboratory)] Array de registros de tipo "Laboratorio" a los cuales se les ha asignado la ciudad.
  has_many :laboratories

  # @!attribute perinatal_informations
  # @return [array(PerinatalInformation)] Array de registros de tipo "Antecedentes Perinatales" a los cuales se les ha asignado la ciudad.
  has_many :perinatal_informations

  # @!attribute patient_fiscal_informations
  # @return [array(PatientFiscalInformation)] Array de registros de tipo "Información Fiscal de Paciente" a los cuales se les ha asignado la ciudad.
  has_many :patient_fiscal_informations

  # Método que filtra registros de tipo "Ciudad" por nombre, el método retornará los primeros 15 resultados que coincidan.
  #
  # @param word [string] String con un fragmento del nombre de la ciudad.
  scope :find_by_name, lambda {  |word| City.where("name like ?", "%#{word}%").limit(15) }

  # Método que formatea el nombre de la ciudad, concatenando el nombre del estado y del país unidos por una coma y un espacio.
  #
  # @return [string] nombre de la ciudad junto con el nombre del estado y país al que pertenece, ejemplo: "Mérida, Yucatán, México".
  def format_city_state_and_country
      if self.name.nil?
        ""
      else
        self.name+", "+self.state.name+", "+self.state.country.name
      end
  end
end
