class Inventory < ActiveRecord::Base
  belongs_to :warehouse
  belongs_to :medicament_category
  belongs_to :created, :class_name => 'User', :foreign_key => 'created_user_id'
  belongs_to :updated, :class_name => 'User', :foreign_key => 'updated_user_id'

  has_many :inventory_stocks, dependent: :destroy
  has_many :inventory_pharmacy_stocks
end
