class ExplorationType < ActiveRecord::Base
  has_many :explorations

  FULL = 1
  PHYSICAL = 2
  PELVIC = 3
end
