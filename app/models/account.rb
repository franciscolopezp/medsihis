# La clase "Account" representa un registro ode tipo "Cuenta de Médico" y es en donde se almacenan los datos de las cuentas bancarias y locales de un doctor.  
class Account < ActiveRecord::Base
  
  # @!attribute doctor
  # @return [Doctor] Registro de tipo "Doctor" al cual pertenece la cuenta.
  has_and_belongs_to_many :user

  # @!attribute bank
  # @return [Bank] Registro de tipo "Banco" al cual pertenece la cuenta.
  belongs_to :bank

  # @!attribute sources
  # @return [array(Transfer)] Array de registros de tipo "Transferencia" que hacen referencia a la cuenta como cuenta de origen.
  has_many :sources, :class_name => 'Transfer', :foreign_key => 'from_account_id'

  # @!attribute destinies
  # @return [array(Transfer)] Array de registros de tipo "Transferencia" que hacen referencia a la cuenta como cuenta de destino.
  has_many :destinies, :class_name => 'Transfer', :foreign_key => 'to_account_id'

  # @!attribute daily_cashes
  # @return [array(DailyCash)] Array de registros de tipo "Corte de caja" que hacen referencia a los cortes de caja que pertenecen a la cuenta.
  has_many :daily_cashes

  # @!attribute movements
  # @return [array(Transaction)] Array de registros de tipo "Transacción" que hacen referencia a las transacciones que le pertenecen a la cuenta.
  has_and_belongs_to_many :movements, :class_name => 'Transaction', :foreign_key => 'account_id'

  # @!attribute offices
  # @return [array(Office)] Array de 0 o 1 elemento que contiene la relación de una cuenta local a un registro de tipo "Consultorio".
  has_and_belongs_to_many :offices

  belongs_to :currency

  # Método que filtra registros de tipo "Cuenta" por consultorios, restringiendo a que las cuentas a sean de tipo local.
  #
  # @param offices [array(integer)] Array de enteros que representan los ids de los consultorios de los cuales se quieren extraer las cuentas.
  scope :local_accounts, -> (offices){
    joins(:bank)
        .joins(:accounts_offices)
        .where('accounts_offices.office_id IN ( '+offices.join(',')+' )')
        .where(banks: {is_local: true})
  }

  # @return [Office,nil] Registro de tipo "Consultorio" asociado a la cuenta, retorna nulo si la cuenta no tiene un registro asociado.
  def office
    if self.offices.active_offices.size > 0
      self.offices[0]
    else
      nil
    end
  end

  # @return [integer] Entero que representa el id del registro de tipo "Consultorio" asociado a la cuenta, retorna 0 si no existe algún registro asociado.
  def office_id
    if self.offices.active_offices.size > 0
      self.offices[0].id
    else
      0
    end
  end

  # @return [string] String que representa el nombre del registro de tipo "Consultorio" asociado a la cuenta, retorna una cadena vacía si no existe algún registro asociado.
  def office_name
    if self.offices.active_offices.size > 0
      self.offices[0].name
    else
      ''
    end
  end
end
