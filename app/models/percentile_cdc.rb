class PercentileCdc < ActiveRecord::Base
  belongs_to :gender
  belongs_to :type_percentile

  PERCENTILE_GRAPHIC = [3,5,10,25,50,75,85,90,95,97]
end
