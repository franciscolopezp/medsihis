class CustomTemplatePrescriptionField < ActiveRecord::Base
  belongs_to :custom_template_prescription
  belongs_to :fields_prescription

  mount_uploader :extra, CustomPrescriptionUploader
  validates :font, :size, :position_top_cm, :position_left_cm, presence: true
  validates_inclusion_of :is_bold, :show_field, in: [true, false]
  validates :size, numericality: {only_integer: true}, allow_blank: false, allow_nil: false
  validates :position_left_cm, :position_top_cm, numericality: {only_float: true}, allow_blank: false, allow_nil: false

  SECTIONS = [{"HEADER" => "ENCABEZADO"}, {"BODY" => "CUERPO"}, {"FOOTER" => "PIE DE PAGINA"}]
end
