class ConsultationType < ActiveRecord::Base
  belongs_to :doctor
  has_and_belongs_to_many :users
  has_many :consultation_costs
  has_many :offices, :through => :consultation_costs

  scope :by_doctor, -> (doctor_id){
    where({doctor_id: doctor_id})
  }
end
