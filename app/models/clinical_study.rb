# La clase "ClinicalStudy" representa un registro de tipo "Estudio Clínico".
class ClinicalStudy < ActiveRecord::Base

  # @!attribute indications
  # @return [array(Indication)] Array de registros de tipo "Indicación" asociados al estudio clínico.
  has_and_belongs_to_many :indications

  # @!attribute clinical_analysis_orders
  # @return [array(ClinicalAnalysisOrder)] Array de registros de tipo "Orden de Análisis Clínicos" a los cuales se le ha asociado el estudio clínico.
  has_and_belongs_to_many :clinical_analysis_orders

  # @!attribute laboratory
  # @return [Laboratory] Registro de tipo "Laboratorio" al cual pertenece el estudio clínico.
  belongs_to :laboratory

  # @!attribute clinical_study_type
  # @return [ClinicalStudyType] Registor de tipo "Tipo de Estudio Clínico" asociado al estudio clínico para clasificarlo.
  belongs_to :clinical_study_type

  has_one :study_report_template

  has_and_belongs_to_many :study_packages

end
