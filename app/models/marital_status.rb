class MaritalStatus < ActiveRecord::Base
  has_many :no_pathologicals
  has_many :default_no_pathologicals
  SINGLE = 1
  MARRIED = 2
end
