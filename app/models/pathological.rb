class Pathological < ActiveRecord::Base
  belongs_to :section, polymorphic: true
  belongs_to :subcategory_pathological
  belongs_to :medical_history

  # funcionan para la asociacion :through, de medical history con los demas modelos
  belongs_to :family_disease
  belongs_to :pathological_allergy
  belongs_to :surgery
  belongs_to :pathological_condition
  belongs_to :pathological_medicine

end
