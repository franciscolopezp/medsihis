class MedicamentPurchase < ActiveRecord::Base
  belongs_to :user
  has_one :medicament_log, as: :traceable
  belongs_to :warehouse
  belongs_to :supplier
  has_many :medicament_purchase_items
  has_many :medicament_purchase_farmacy_items
end
