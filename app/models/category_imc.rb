# La clase "CategoryImc" representa un registro de tipo "Categoría de IMC".
class CategoryImc < ActiveRecord::Base
	
  # @!attribute imc_adults
  # @return [array(ImcAdult)] Array de registros de tipo "Indi"
  has_many :imc_adults

  NORMAL = 1
  OVERWEIGHT = 2
  OBESITY_GRADE_ONE = 3
  OBESITY_GRADE_TWO = 4
  OBESITY_GRADE_THREE = 5
end
