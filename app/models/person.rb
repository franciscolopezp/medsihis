class Person < ActiveRecord::Base
  has_one :user
  has_one :doctor
  has_one :assistant
  belongs_to :city
  belongs_to :gender
  accepts_nested_attributes_for :user

  def full_name
    return self.name+' '+self.last_name
  end

end
