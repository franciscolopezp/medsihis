class EmisorFiscalInformation < ActiveRecord::Base
  belongs_to :city
  has_many :admission_invoices
  mount_uploader :certificate_stamp, ClinicInfoUploader
  mount_uploader :certificate_key, ClinicInfoUploader
  before_create :encrypt_password

  def encrypt_password
    if !self.certificate_password.nil? and !self.certificate_password.blank?
      self.certificate_password= AESCrypt.encrypt(self.certificate_password, ENV['TS_KEY'])
    end
    self.certificate_password
  end

  def decrypt_password
    self.certificate_password= AESCrypt.decrypt(self.certificate_password, ENV['TS_KEY'])
  end

end
