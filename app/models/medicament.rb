class Medicament < ActiveRecord::Base
  has_and_belongs_to_many :medical_histories
  has_and_belongs_to_many :active_substances
  has_many :lots
  has_many :medicament_assortment_items
  has_many :medicament_purchase_items
  belongs_to :doctor
  belongs_to :medicament_laboratory
  belongs_to :medicament_category
  scope :find_by_name, lambda {  |word| Medicament.where("comercial_name like ?", "%#{word}%").limit(15) }

  validates :comercial_name, presence: :true
  validates :presentation, presence: :true

  def prescription_format
    pres = self.comercial_name+" ("+self.presentation.to_s+"). Laboratorio: "+ self.brand.to_s
    pres
  end

end
