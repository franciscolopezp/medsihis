class HospitalService < ActiveRecord::Base
  include ActiveRecord::UnionScope
  belongs_to :user
  has_many :service_charges
  has_many :other_charge_items
  has_many :insurence_prices
  has_many :insurances, :through => :insurence_prices
  belongs_to :hospital_service_category
  has_many :package_service_costs
  has_many :service_packages, :through => :package_service_costs

  scope :in_other_charge,  ->(start_date, end_date) {
    joins(:other_charge_items)
        .where('other_charge_items.created_at >= ? AND other_charge_items.created_at < ?', start_date, end_date)
        .select('hospital_services.code, hospital_services.name as name, COUNT(*) as cont, sum(total) as total')
        .group(:hospital_service_id)
  }
  scope :in_hospital_admission,  ->(start_date, end_date) {
    joins(:service_charges)
        .where('service_charges.created_at >= ? AND service_charges.created_at < ?', start_date, end_date)
        .select('hospital_services.code, hospital_services.name as name, COUNT(*) as cont, sum(residue) as total')
        .group(:hospital_service_id)
  }

  # A union of the aforementioned scopes
  scope :all_services, -> (start_date, end_date){ union_scope_custom(in_other_charge(start_date, end_date), in_hospital_admission(start_date, end_date)) }
end
