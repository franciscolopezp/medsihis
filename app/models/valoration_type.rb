class ValorationType < ActiveRecord::Base
  has_and_belongs_to_many :valoration_fields, :join_table => 'valoration_types_fields'
end
