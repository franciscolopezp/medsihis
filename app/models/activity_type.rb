# La clase "ActivityType" representa un registro de tipo "Tipo de Actividad" el cual puede ser asignado a una actividad para su clasificación.
class ActivityType < ActiveRecord::Base

  # @!attribute activities
  # @return [array(Activity)] Array de registros de tipo "Actividad" que tienen asignado el tipo de actividad.
  has_many :activities

  # @!attribute activity_statuses
  # @return [array(ActivityStatus)] Array de registros de tipo "Estado de Actividad" que pertenecen al tipo de actividad.
  has_many :activity_statuses

  APPOINTMENT = 1
  SURGERY = 2
  HOLIDAYS = 3
  OTHERS = 4
end


