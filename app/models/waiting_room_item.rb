class WaitingRoomItem < ActiveRecord::Base
  belongs_to :patient
  has_many :waiting_room_item_statuses
  belongs_to :waiting_room_status
  has_and_belongs_to_many :patient_valorations, :join_table => 'wroom_items_valorations'

  def waiting_time
    now = Time.now
    last_date = self.created_at
    minutes = ((now - last_date) / 1.minutes).to_i
    resp = ""
    hours = minutes / 60
    residue_minutes = minutes % 60
    if hours > 0
      resp << hours.to_i.to_s + "h "
    end

    if residue_minutes > 0
      resp << residue_minutes.to_i.to_s + "m"
    end

    if resp == ""
      resp = "0m"
    end

    resp
  end
end
