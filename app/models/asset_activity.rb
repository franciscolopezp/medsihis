class AssetActivity < ActiveRecord::Base
  belongs_to :fixed_asset_item
  belongs_to :user
  belongs_to :fixed_asset_activity
end
