class SubcategoryPathological < ActiveRecord::Base
  has_many :pathologicals

  FAMILY_DISEASE = 1
  PATIENT_CONDITION = 2
  MEDICINES = 3
  CONDITIONS = 4
end
