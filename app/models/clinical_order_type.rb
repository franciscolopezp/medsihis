class ClinicalOrderType < ActiveRecord::Base
  has_many :laboratories
  has_many :clinical_analysis_orders

  STUDY_CLINICAL = 1
  STUDY_GABINETE = 2
end
