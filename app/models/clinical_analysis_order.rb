# La clase "ClinicalAnalysisOrder" representa un registro de tipo "Orden de Análisis Clínicos".
class ClinicalAnalysisOrder < ActiveRecord::Base

  validates :folio, :date_order, :clinical_studies, presence: :true
  validate :validate_clinical_studies

  # @!attribute medical_expedient
  # @return [MedicalExpediente] Registro de tipo "Expediente Médico" al cual pertenece la orden de análisis clínicos.
  belongs_to :medical_expedient
  belongs_to :user
  # @!attribute archives
  # @return [array(Archive)] Array de registros de tipo "Archivo" que le pertenecen a la orden de análisis clínicos.
  has_many :archives
  has_many :clinical_order_result_xmls
  # @!attribute clinical_studies
  # @return [array(ClinicalStudy)] Array de registros de tipo "Estudio Clínico" asociados a la orden.
  has_and_belongs_to_many :clinical_studies

  # @!attribute indications
  # @return [array(Indication)] Array de registros de tipo "Indicación" asociados a una orden, las indicaciones se pueden obtener a través de un estudio clínico.
  has_many :indications, through: :clinical_studies
  belongs_to :clinical_order_type

  has_one :cabinet_report

  belongs_to :office

  belongs_to :laboratory

  # Valida que una orden de análisis clínicos contenga al menos un estudio, de lo contrario agrega un error al modelo evitando que se guarde el registro.
  def validate_clinical_studies
    errors.add(:clinical_studies, " agregue un estudio clínico") if clinical_studies.size == 0
  end

  def get_cost
    cost = 0
    self.clinical_studies.each do |s|
      cost += s.price.present? ? s.price : 0
    end
    cost
  end

end
