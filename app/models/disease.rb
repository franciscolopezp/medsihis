# La clase "Disease" representa un registro de tipo "Enfermedad".
class Disease < ActiveRecord::Base

  # @!attribute disease_category
  # @return [DiseaseCategory] Registro de tipo "Categoría de Enfermedad" al cual pertenece la enfermedad.
  belongs_to :disease_category

  # @!attribute doctor
  # @return [Doctor] Registro de tipo "Doctor" al cual pertenece la enfermedad, si es nil entonces significa que es una enfermedad global.
  belongs_to :doctor

  # @!attribute medical_consultations
  # @return [array(MedicalConsultation)] Array de registros de tipo "Consulta Médica" los cuales tiene una asociación con la enfermedad.
  has_and_belongs_to_many :medical_consultations

  # @!attribute family_diseases
  # @return [array(FamilyDisease)] Array de registros de tipo "Antecedente Familiar" a los cuales se le ha asociado una enfermedad.
  has_and_belongs_to_many :family_diseases

  # Método que filtra registros de tipo "Enfermedad" por nombre, restringe los primeros 10 resultados.
  #
  # @param word [string] String con un fragmento del nombre de una enfermedad.
  scope :find_by_name, lambda {  |word| Disease.where("name like ?", "%#{word}%").limit(10) }

end
