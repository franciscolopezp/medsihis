class TypeCertificate < ActiveRecord::Base
  has_many :certificates
  belongs_to :doctor

  validates :name, :container_html, presence: true

end
