# La clase "Hospital" representa un registro de tipo "Hospital".
class Hospital < ActiveRecord::Base

  # @!attribute city
  # @return [City] Registro de tipo "Ciudad" que le fue asignado al hospital.
  belongs_to :city

  # @!attribute offices
  # @return [array(Office)] Array de registros de tipo "Consultorio" asociados al hospital.
  has_many :offices
end
