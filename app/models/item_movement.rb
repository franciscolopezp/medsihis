class ItemMovement < ActiveRecord::Base
  belongs_to :waste_type
  belongs_to :warehouse
  has_many :medicament_wastes
  has_many :pharmacy_item_wastes
  belongs_to :user
  has_one :medicament_log, as: :traceable
end
