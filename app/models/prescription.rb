class Prescription < ActiveRecord::Base
  belongs_to :medical_consultation
  has_many :medical_indications

  validates :recommendations, :presence => { if: :validate_medical_indications?, :message => "Agregue una recomendación o una lista de medicamentos" }

  def validate_medical_indications?
    true if self.medical_indications.size == 0
  end

end
