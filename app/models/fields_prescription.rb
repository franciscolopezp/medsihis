class FieldsPrescription < ActiveRecord::Base
  has_many :custom_template_prescription_fields
  belongs_to :field_prescription_type
  DOCTOR_NAME = 1
  OFFICE_NAME_DOCTOR = 2
  ADDRESS_OFFICE_DOCTOR = 3
  PHONE_OFFICE = 4
  CELL_DOCTOR = 5
  MEDICAL_SPECIALTIES = 6
  ACADEMIC_STUDIES = 7
  PROFESSIONAL_IDENTITY_CARD =  8

  PATIENT_NAME = 9
  AGE_PATIENT = 10
  DATE_CONSULTATION = 11
  DIAGNOSTIC_CONSULTATION = 12
  MEDICAL_TREATMENT = 13
  BINDING_CODE_EXPEDIENT = 14
  QR_CODE_EXPEDIENT = 15

  FOLIO_CONSULTATION = 16
  WEIGHT_PATIENT = 17
  HEIGHT_PATIENT = 18
  LOGO_OFFICE = 19
  EXTRA_DATA = 20

  LABEL = 21
  LINE = 22
  SQUARE = 23
  RECTANGLE = 24
  CIRCLE = 25
  IMAGE = 26
  CRANIAL_PERIMETER = 27

  # fuentes para el tipo de letra
  FONT_FAMILY = ["Arial","Times New Roman","cursive", "monospace", "fantasy"]
  # tamaño de letra, en px
  SIZE_STYLE = (4..30)
end
