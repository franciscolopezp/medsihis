class Patient < ActiveRecord::Base
  belongs_to :city
  has_one :medical_expedient
  has_many :clinical_analysis_orders, through: :medical_expedient
  has_and_belongs_to_many :vaccine_books
  has_and_belongs_to_many :allergies
  has_and_belongs_to_many :activities
  belongs_to :user
  belongs_to :gender
  belongs_to :blood_type
  belongs_to :insurance
  has_many :patient_fiscal_informations
  has_many :medical_consultations, :through => :medical_expedient
  has_many :vaccinations
  has_many :application_ages, :through => :vaccinations
  has_many :certificates
  has_many :hospital_admissions
  include ActionView::Helpers::DateHelper

  validates :name, :last_name, :birth_day,  presence: :true

  scope :by_doctor, lambda {|doctor_id| where('doctor_id = ?',doctor_id)}
  scope :by_match, lambda {  |word,limit| where("concat(name,' ',last_name) like ?", "%#{word}%").limit(limit) }

  BLOOD_TYPE = [
      ["A","A"],
      ["A+","A+"],
      ["B","B"],
      ["B+","B+"],
      ["AB","AB"],
      ["AB+","AB+"],
      ["O","O"],
      ["O+","O+"],
  ]

  def full_name
    self.name+" "+self.last_name
  end

  def age
    now = Time.now.utc
    now.year - self.birth_day.year - (self.birth_day.to_time.change(:year => now.year) > now ? 1 : 0)
  end

  def age_and_month
    age = distance_of_time_in_words_hash(Time.now, self.birth_day)
    age_string = ''
    if age[:years] != nil && age[:years].to_i > 0
      age_string << age[:years].to_s + ' año'
      if age[:years].to_i > 1
        age_string << 's'
      end
    end

    if age[:months] != nil && age[:months].to_i > 0
      age_string << ' ' + age[:months].to_s + ' mes'
      if age[:months].to_i > 1
        age_string << 'es'
      end
    end
    age_string
  end

  # este método se usa unicamente para calcular el IMC y el eso sugerido, tiene como regla si el paciente tiene mas de 9 años y 6 mese,
  # los años se redondea, por ejemplo el paciente tiene 9 años y 7 mese se redondea a 10 años, si tiene 10 años y 6 meses a 11 años, etc.
  def age_month
    age = distance_of_time_in_words_hash(Time.now, self.birth_day)
    months = 0
    if age[:years] != nil && age[:years].to_i > 0
      months = months + (age[:years].to_i * 12)
    end

    if age[:months] != nil && age[:months].to_i > 0
      months = months + age[:months].to_i
    end
    months
  end

  def max_date_to_percentile
      self.birth_day + 5.years
  end

  def max_date_to_percentile_cdc
      self.birth_day + 20.years
  end

  def format_age view
    age = view.distance_of_time_in_words_hash(Time.now, self.birth_day)
    age_string = ''
    if age[:years] != nil && age[:years] > 0
      age_string << age[:years].to_s + ' año'
      if age[:years] > 1
        age_string << 's'
      end
    end

    if age[:months] != nil && age[:months] > 0
      age_string << ' ' + age[:months].to_s + ' mes'
      if age[:months] > 1
        age_string << 'es'
      end
    end
    age_string
  end

end
