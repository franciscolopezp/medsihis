class User < ActiveRecord::Base
  #validates :user_name,:password,:email, presence: true
  belongs_to :role
  belongs_to :person
  has_many :user_tokens
  has_one :doctor
  has_many :item_transfers
  has_many :pharmacy_item_wastes
  has_many :medical_expedients
  has_many :general_fiscal_infos
  has_many :patients
  has_many :medicament_purchases
  has_many :medicament_inventories
  has_many :medicament_wastes
  has_many :hospital_admissions
  has_many :hospital_services
  has_many :hospital_admission_transactions
  has_many :medicament_logs
  has_many :medicament_assortments
  has_many :service_charges
  has_many :clinic_invoices
  has_many :pharmacy_item_inventories
  has_many :other_charges
  has_many :other_charge_transactions
  has_many :admission_invoices
  has_and_belongs_to_many :accounts
  has_and_belongs_to_many :offices
  has_and_belongs_to_many :general_consultation_types
  has_and_belongs_to_many :consultation_types
  has_and_belongs_to_many :warehouses
  has_and_belongs_to_many :doctors
  has_one :assistant, dependent: :destroy
  has_one :user_patient
  has_many :daily_cashes
  has_many :transactions
  has_many :abortions
  has_many :birth_infos
  has_many :pregnancies
  has_many :cabinet_reports
  has_many :medical_consultations
  has_many :clinical_analysis_orders
  has_many :closed_daily_cashes, :class_name => 'DailyCash', :foreign_key => 'close_user_id'
  has_many :patient_valorations

  #scope :all_doctor, lambda {joins(:type_user).where('type_users.id = ?',TypeUser::DOCTOR)}
  #scope :all_admin, lambda { joins(:type_user).where('type_users.id = ?',TypeUser::ADMIN)}
  #scope :all_assistant, lambda { joins(:type_user).where('type_users.id = ?',TypeUser::MEDICAL_ASSISTANT)}

  validate :unique_username
  validate :unique_email

  has_secure_password :validations => false


  validates :user_name, :email, presence: true

  def get_user(id)
    user = User.find(id)
    user
  end



  def self.search(search)
    if search
      where('user_name LIKE ?', "%#{search}%")
    else
      all
    end
  end

  def clearance_levels
    # Single role name
    self.role.name
  end


  def self.by_username username,id
    if id == nil
      where('user_name = ?',username).first
    else
      where('id != ?',id).where('user_name = ?',username).first
    end
  end

  def self.by_email email,id
    if id == nil
      where('email = ?',email).first
    else
      where('id != ?',id).where('email = ?',email).first
    end
  end

  def unique_username
    if self.user_name != '' and User.by_username(self.user_name,self.id) != nil
      errors.add(:user_name, "El usuario '"+self.user_name+"' ya existe en el sistema.")
    end
  end

  def unique_email
    if self.email != '' and User.by_email(self.email,self.id) != nil
      errors.add(:email, "El email '"+self.email+"' ya existe en el sistema.")
    end
  end

  def owner_name
   return self.person.full_name
  end



end
