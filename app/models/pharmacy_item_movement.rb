class PharmacyItemMovement < ActiveRecord::Base
  belongs_to :pharmacy_item
  belongs_to :item_transfer
end
