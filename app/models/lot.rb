class Lot < ActiveRecord::Base
  belongs_to :medicament
  has_many :medicament_purchase_items
  has_many :medicament_inventories
  has_many :medicament_wastes
  has_many :medicament_logs
  has_many :medicament_assortment_items
  has_many :medicament_movements
  has_many :medicament_transfer_temps
  has_many :pharmacy_item_transfer_temps
end
