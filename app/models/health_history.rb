class HealthHistory < ActiveRecord::Base
  belongs_to :user_patient
  has_many :expedient_requests, dependent: :destroy
  has_many :medical_expedients, through: :expedient_requests

  validates :name, presence: true
end
