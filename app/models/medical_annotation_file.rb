class MedicalAnnotationFile < ActiveRecord::Base
  belongs_to :medical_annotation
  belongs_to :user
  mount_uploader :document, MedicalAnnotationFileUploader
end
