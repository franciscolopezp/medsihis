# La clase "ChildNonPathologicalInfo" representa un registro de tipo "Antecedentes no Patológicos del menor".
# Éste registro existe cuando se ha contratado al especialidad de pediatría.
class ChildNonPathologicalInfo < ActiveRecord::Base

  # @!attribute parent_infos
  # @return [array(ParentInfo)] Array de registros de tipo "Información de Padre (o Madre)" puede tener de 0 a 2 registros como máximo.
  has_many :parent_infos

  # @!attribute medical_history
  # @return [MedicalHistory] Registro de tipo "Historial Médico" al cual está asociado el antecedente no patológico.
  belongs_to :medical_history
end
