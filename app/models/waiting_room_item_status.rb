class WaitingRoomItemStatus < ActiveRecord::Base
  belongs_to :waiting_room_item
  belongs_to :waiting_room_status
end
