class Transfer < ActiveRecord::Base
  belongs_to :from, :class_name => 'Account', :foreign_key => 'from_account_id'
  belongs_to :to, :class_name => 'Account', :foreign_key => 'to_account_id'
  has_many :transactions, as: :chargeable
end
