class MedicalAnnotationImage < ActiveRecord::Base
  belongs_to :medical_annotation_type

  mount_uploader :image, MedicalAnnotationImageUploader
end
