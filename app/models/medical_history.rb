class MedicalHistory < ActiveRecord::Base
  belongs_to :medical_expedient
  #pediatria
  has_one :perinatal_information
  has_one :child_non_pathological_info

  #generales
  has_one :no_pathological, dependent: :destroy
  belongs_to :family_disease, dependent: :destroy
  has_and_belongs_to_many :medicaments

  accepts_nested_attributes_for :no_pathological
  accepts_nested_attributes_for :family_disease

  # estas relaciones funcionan para llegar a un modelo atraves del modelo polimorfismo
=begin  has_one :pathological_family, lambda {where(section_type:'FamilyDisease')}, class_name: 'Pathological'
  has_one :family_disease, through: :pathological_family, :source => :section, :source_type => "FamilyDisease"

  has_one :pathology_allergy, lambda {where(section_type:'PathologicalAllergy')}, class_name: 'Pathological'
  has_one :pathological_allergy, through: :pathology_allergy, :source => :section, :source_type => "PathologicalAllergy"

  has_one :pathological_surgery, lambda {where(section_type:'Surgery')}, class_name: 'Pathological'
  has_one :surgery, through: :pathological_surgery, :source => :section, :source_type => "Surgery"

  has_one :pathology_medicine, lambda {where(section_type:'PathologicalMedicine')}, class_name: 'Pathological'
  has_one :pathological_medicine, through: :pathology_medicine, :source => :section, :source_type => "PathologicalMedicine"

  has_one :pathology_condition, lambda {where(section_type:'PathologicalCondition')}, class_name: 'Pathological'
  has_one :pathological_condition, through: :pathology_condition, :source => :section, :source_type => "PathologicalCondition"
=end
  has_one :patient, through: :medical_expedient


 # accepts_nested_attributes_for :pathologicals

  #ginecologia
  has_one :inherited_family_background
  has_one :gynecologist_obstetrical_background

  #antecedentes urología
  has_one :urological_background

  has_many :cardiovascular_aps
  has_many :respiratory_aps
  has_many :digestive_aps
  has_many :urinary_aps
  has_many :psychosomatic_sis
  has_many :nervous_sis
  has_many :genital_aps
  has_many :otolaryngology_sis
  has_many :metabolic_sis
  has_many :hematological_sis
  has_many :ophthalmological_sis
  has_many :infection_ots
  has_many :sense_organs
  has_many :locomotor_sis

end
