class ClinicInfo < ActiveRecord::Base
  has_many :currency_exchanges
  has_many :currencies, through: :currency_exchanges
  belongs_to :city
  mount_uploader :logo, ClinicInfoUploader
  mount_uploader :banner, ClinicInfoUploader
  mount_uploader :index_image, ClinicInfoUploader
end
