class MedicamentLog < ActiveRecord::Base
  belongs_to :traceable, polymorphic: true
  belongs_to :user
  belongs_to :lot
  belongs_to :warehouse
end
