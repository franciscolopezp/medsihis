class MovementType < ActiveRecord::Base

  has_many :other_movements

  def self.group_by_category
    data = []
    MovementCategory.all.each do |c|
      mt = {
          'category' => {'id'=>c.id, 'name'=>c.name, 'type' => c.m_type},
          'data' =>self.where('movement_category_id = '+c.id.to_s)
      }
      data.push(mt)
    end
    data
  end
end
