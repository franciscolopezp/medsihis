# La clase "ApplicationAge" representa un registro de tipo "Edad de Aplicación".
class ApplicationAge < ActiveRecord::Base
  
  # @!attribute vaccine
  # @return [Vaccine] Registro de tipo "Vacuna" al cual pertenece la edad de aplicación.
  belongs_to :vaccine
  
  # @!attribute vaccinations
  # @return [array(Vaccinations)] Array de registros de tipo "Aplicación de Vacuna" asociados a una edad de aplicación, un paciente es quien tiene la aplicación de vacuna.
  has_many :vaccinations

  # @!attribute patients
  # @return [array(Patient)] Array de registros de tipo "Paciente" que tienen asociado registros de tipo "Aplicación de Vacuna".
  has_many :patients, :through => :vaccinations
end
