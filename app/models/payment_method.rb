class PaymentMethod < ActiveRecord::Base
  has_many :transactions
  has_many :invoices
  CASH = 1
  TRANSFER = 3
end
