class OtherCharge < ActiveRecord::Base
  has_many :other_charge_items
  has_many :other_charge_transactions
  belongs_to :user
end
