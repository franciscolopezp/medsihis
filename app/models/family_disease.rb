# La clase "FamilyDisease" representa un registro de tipo "Antecedente Familiar".
class FamilyDisease < ActiveRecord::Base

  # @!attribute section
  # @return [Pathological] Registro de tipo "Padecimiento" al cual pertenece el antecedente.
  has_one :pathological, as: :section

  # @!attribute diseases
  # @return [array(Disease)] Array de registros de tipo "Enfermedad" asociados al antecedente.
  has_and_belongs_to_many :diseases
end
