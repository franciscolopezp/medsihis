# La clase "Assistant" representa un registro de tipo "Asistente" la cual esta asociada y pertenece a un doctor.
class Assistant < ActiveRecord::Base

  # @!attribute city
  # @return [City] Registro de tipo "Ciudad" que le fue asignado a la asistente.


  # @!attribute doctor
  # @return [Doctor] Registro de tipo "Doctor" que representa el doctor al que pertenece la asistente.
  has_and_belongs_to_many :doctors

  # @!attribute office_permissions
  # @return [array(OfficePermission)] Array de registros de tipo "Permiso de Consultorio" los cuales representan los permisos que una asistente tiene en un consultorio, estos permisos están ligados a agenda y cortes de caja.
  has_many :office_permissions, dependent: :destroy

  has_many :permissions, through: :office_permissions

  has_many :offices, -> { distinct }, through: :office_permissions

  belongs_to :gender

  # @!attribute user
  # @return [User] Registro de tipo "Usuario" que contiene las credenciales de acceso a la aplicación de la asistente.
  belongs_to :user, dependent: :delete

  # @!attribute daily_cashes
  # @return [array(DailyCash)] Array de registros de tipo "Corte de Caja" que una asistente es responsable de haber abierto.
  has_many :daily_cashes, through: :user

  has_and_belongs_to_many :expedient_permissions

  accepts_nested_attributes_for :user

  # @return [string] String que representa el nombre completo de la asistente se concatena las columnas nombre y apellido.
  def full_name
    return self.user.person.name+' '+self.user.person.last_name
  end
end
