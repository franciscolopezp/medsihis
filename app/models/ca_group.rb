# La clase "CaGroup" representa un registro de tipo "Grupo de Análisis Clínicos"
class CaGroup < ActiveRecord::Base

  # @!attribute indications
  # @return [array(Indication)] Array de registros de tipo "Indicación", representan las indicaciones para un grupo de análisis clínico.
  has_many :indications

  belongs_to :laboratory
end
