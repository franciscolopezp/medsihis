class VaccineBook < ActiveRecord::Base
  has_many :vaccines
  has_and_belongs_to_many :patients
end
