class ValorationField < ActiveRecord::Base
  VITAL_SIGN = 1
  EXPLORATION = 2

  has_and_belongs_to_many :valoration_types, :join_table => 'valoration_types_fields'
end
