# La clase "ImcChildrenMax19Year" representa un registro de tipo "IMC de niños menores de 19 años".
class ImcChildrenMax19Year < ActiveRecord::Base
  
  # Unidad de Peso - Kilogramos
  UNIT_WEIGHT_KG = "Kg"
  # Unidad de Peso - Índice de Masa Corporal
  UNIT_WEIGHT_IMC = "I.M.C."

  # Rango de Meses - Menos de un mes y maximo 5 años, se consideran los datos como kg
  START_TABLE_KG_CHILDREN_IN_MONTH = 0

  # Rango de Meses - Segunda tabla de los menores, de 1 año a 5 años
  START_TABLE_TWO_KG_CHILDREN_IN_MONTH = 12

  # Rango de Meses - 5 años y 6 meses
  START_TABLE_IMC_CHILDREN_IN_MONTH = 66

  # Rango de Meses - 9 años y 6 meses, <- inicio de la tabla imc para adolescentes
  END_TABLE_IMC_CHILDREN_IN_MONTH = 114

  # Rango de Meses - 19 años, despues de esta edad se considera como adulto
  END_TABLE_IMC_TO_CHILDREN = 228 


  # @!attribute gender
  # @return [Gender] Registro de tipo "Género" asignado al IMC.
  belongs_to :gender

  # @!attribute vital_signs
  # @return [array(VitalSign)] Array de registros de tipo "Signos Vitales" a los cuales fue asignado el IMC.
  has_many :vital_signs
  
end
