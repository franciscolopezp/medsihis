class MedicalIndication < ActiveRecord::Base
  belongs_to :medicament
  belongs_to :prescription

  def prescription_format
    pres = self.medicament.prescription_format + "<br>Dosis: "+ self.dose
    pres
  end
end
