class BirthInfo < ActiveRecord::Base
  belongs_to :birth_type
  belongs_to :delivery_type
  belongs_to :episiotomy_type
  belongs_to :tear_type
  belongs_to :duration_type
  belongs_to :user
  belongs_to :pregnancy
  has_many :neonate_infos , :dependent => :destroy
end
