class PharmacyItemWaste < ActiveRecord::Base
  has_one :pharmacy_item_log, as: :traceable
  belongs_to :user
  belongs_to :pharmacy_item
  belongs_to :warehouse
  belongs_to :item_movement
end
