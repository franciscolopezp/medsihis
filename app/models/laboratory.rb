class Laboratory < ActiveRecord::Base
  belongs_to :city
  has_many :clinical_studies
  has_many :indications, :through => :clinical_studies
  has_one :doctor
  belongs_to :clinical_order_type
  has_many :ca_groups
  has_many :clinical_study_types
  mount_uploader :logo, LaboratoryUploader

  has_and_belongs_to_many :doctors

  has_many :clinical_analysis_orders

  LABORATORY_DEFAULT = 3
end
