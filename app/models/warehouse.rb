class Warehouse < ActiveRecord::Base
  has_many :medicament_purchases
  has_and_belongs_to_many :users
  has_many :medicament_inventories
  has_many :medicament_wastes
  has_many :medicament_logs
  has_many :medicament_assortments
  has_many :from_movements, :class_name => 'MedicamentMovement', :foreign_key => 'from_warehouse'
  has_many :to_movements, :class_name => 'MedicamentMovement', :foreign_key => 'to_warehouse'
  has_many :from_movements, :class_name => 'PharmacyItemMovement', :foreign_key => 'from_warehouse'
  has_many :to_movements, :class_name => 'PharmacyItemMovement', :foreign_key => 'to_warehouse'
  has_many :from_movements, :class_name => 'ItemTransfer', :foreign_key => 'from_warehouse'
  has_many :to_movements, :class_name => 'ItemTransfer', :foreign_key => 'to_warehouse'
  has_many :pharmacy_item_inventories
  has_many :pharmacy_item_wastes
end
