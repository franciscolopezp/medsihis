# La clase "IdentityCard" representa un registro de tipo "Cédula Profesional".
class IdentityCard < ActiveRecord::Base

  # @!attribute doctor
  # @return [Doctor] Registro de tipo "Doctor" al cual pertenece la cédula profesional.
  belongs_to :doctor

  # @!attribute specialty
  # @return [Specialty] Registro de tipo "Especialidad" asociado a la cédula profesional.
  belongs_to :specialty
end
