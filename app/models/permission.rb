class Permission < ActiveRecord::Base
  has_many :office_permissions

  READ = 1
  CREATE = 2
  UPDATE = 3
  DELETE = 4
  DAILY_CASH = 5

end
