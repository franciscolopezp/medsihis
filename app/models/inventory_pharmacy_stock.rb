class InventoryPharmacyStock < ActiveRecord::Base
  has_one :pharmacy_item_log, as: :traceable
  belongs_to :inventory
  belongs_to :pharmacy_item
end
