class TypeUser < ActiveRecord::Base
  has_many :user

  ADMIN = 1
  DOCTOR = 2
  MEDICAL_ASSISTANT = 3
  PATIENT = 4
  SHARED_ASSISTANT = 5
  RECEPTIONIST = 6
  NURSE = 7
  PHARMACY_MANAGER = 8

  TOKEN_API = "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoibWVkc2lfbW92aWwifQ."

  def self.names
    {
        1 => 'Administrador',
        2 => 'Doctor',
        3 => 'Asistente',
        4 => 'Paciente',
        5 => 'Asistente compartida',
        6 => 'Recepción',
        7 => 'Enfermería',
        8 => 'Encargado de farmacia'
    }
  end

end
