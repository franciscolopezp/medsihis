class FixedAsset < ActiveRecord::Base
  belongs_to :fixed_asset_category
  has_many :fixed_asset_activities
  has_many :fixed_asset_items
end
