class TypePercentile < ActiveRecord::Base
  has_many :percentile_children
  has_many :percentile_cdcs

  SIZE_LENGTH = 1
  HEAD_CIRCUMFERENCE = 2
  WEIGHT = 3
  IMC = 4

  DATA_PERCENTILE_OMS = 1
  DATA_PERCENTILE_CDC = 2
end
