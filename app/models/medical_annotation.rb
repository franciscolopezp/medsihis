class MedicalAnnotation < ActiveRecord::Base
  belongs_to :medical_annotation_type
  belongs_to :user
  belongs_to :patient
  has_and_belongs_to_many :hospital_admissions, :join_table => 'hospitalizations_annotations'
  has_many :annotation_fields
  has_and_belongs_to_many :patient_valorations, :join_table => 'medical_annotations_valorations'
  has_many :medical_annotation_files


  has_many :annotation_linked_fields
  has_many :medical_annotation_linked_fields, :through => :annotation_linked_fields
end
