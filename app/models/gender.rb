# La clase "Gender" representa un registro de tipo "Género".
class Gender < ActiveRecord::Base
  
  # Género - Hombre
  MAN = 1
  # Género - Mujer
  WOMEN = 2

  # @!attribute imc_children_max19_years
  # @return [array(ImcChildrenMax19Years)] Array de registros de tipo "IMC de niños menores de 19 años" asociados con el género.
  has_many :imc_children_max19_years
  has_many :persons
end
