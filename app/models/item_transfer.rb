class ItemTransfer < ActiveRecord::Base
  has_one :medicament_log, as: :traceable
  has_one :pharmacy_item_log, as: :traceable
  belongs_to :from_warehouse, :class_name => 'Warehouse'
  belongs_to :to_warehouse, :class_name => 'Warehouse'
  belongs_to :modified_by, :class_name => 'User'
  belongs_to :user
  has_many :medicament_transfer_temps
  has_many :pharmacy_item_transfer_temps
  has_many :medicament_movements
  has_many :pharmacy_item_movements
end
