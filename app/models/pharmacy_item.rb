class PharmacyItem < ActiveRecord::Base
  belongs_to :measure_unit_item
  has_many :medicament_purchase_farmacy_items
  has_many :pharmacy_item_inventories
  has_many :pharmacy_item_wastes
  has_many :assortment_pharmacy_items
  belongs_to :medicament_category
  has_many :inventory_pharmacy_stocks
  has_many :pharmacy_item_movements
  has_many :pharmacy_item_transfer_temps
end
