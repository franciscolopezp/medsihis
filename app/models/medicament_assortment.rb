class MedicamentAssortment < ActiveRecord::Base
  has_one :medicament_log, as: :traceable
  belongs_to :warehouse
  belongs_to :user
  belongs_to :hospital_admission
  has_many :medicament_assortment_items
  has_many :assortment_pharmacy_items
end
