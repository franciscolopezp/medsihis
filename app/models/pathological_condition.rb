class PathologicalCondition < ActiveRecord::Base
  has_one :pathological, as: :section
end
