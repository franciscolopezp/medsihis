class ClinicInvoice < ActiveRecord::Base
  has_many :clinic_invoice_concepts
  has_one :clinic_invoice_receptor
  has_one :clinic_invoice_emisor
  belongs_to :patient_fiscal_information
  belongs_to :user
  belongs_to :payment_way
  has_many :admission_transactions
end
