class License < ActiveRecord::Base
  belongs_to :doctor
  has_many :license_features, dependent: :destroy
  has_many :features, :through => :license_features
  belongs_to :distribution_type

  accepts_nested_attributes_for :license_features

  validates :start_date, :end_date, :status, :distribution_type, presence: :true
  validates_inclusion_of :current, in:[true,false]
  #validates_associated :license_features
  #Doctor Licence
  ACTIVE = "Activa";
  CANCELED = "Cancelada";
  ENDED = "Vencida";
  #Patient License
  FREE = 0;
  PREMIUM = 1;

  def active_to_day?
    self.end_date >= Date.today
  end
end
