class AddressBook < ActiveRecord::Base
  belongs_to :hospital
  belongs_to :doctor
  belongs_to :office
end
