# La clase "Archive" representa un registro de tipo "Archivo" asociado a un registro de tipo "Orden de análisis clínicos" y contiene la ruta de donde está ubicado el archivo en disco.
class Archive < ActiveRecord::Base

  # @!attribute clinical_analysis_order
  # @return [ClinicalAnalysisOrder] Registro de tipo "Orden de análisis clínicos" al cual pertenece el archivo que se sube al servidor.
  belongs_to :clinical_analysis_order

  mount_uploader :file_name, ArchiveUploader
end
