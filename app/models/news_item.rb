class NewsItem < ActiveRecord::Base
  DRAFT = 0
  ACTIVE = 1
  ARCHIVED = -1

  belongs_to :author

  mount_uploader :banner, NewsUploader

  def self.statuses
    [
        {:id=>0, :name => "Borrador"},
        {:id=>1, :name => "Activo"},
        {:id=>-1, :name => "Archivado"}
    ]
  end

  def self.get_status_name status_id
    case status_id
      when DRAFT
        return 'Borrador'
      when ACTIVE
        return 'Activo'
      when ARCHIVED
        return 'Archivado'
    end
  end

end
