# La clase "Bank" representa un registro de tipo "Banco"
class Bank < ActiveRecord::Base

  # @!attribute accounts
  # @return [array(Account)] Array de registros de tipo "Cuenta de Médico", representan las cuentas que un doctor puede tener en un mismo banco.
  has_many :accounts
  has_many :general_accounts
end
