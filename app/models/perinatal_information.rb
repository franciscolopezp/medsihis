class PerinatalInformation < ActiveRecord::Base
  belongs_to :medical_history
  belongs_to :city
end
