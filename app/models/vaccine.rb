class Vaccine < ActiveRecord::Base
  belongs_to :vaccine_book
  has_many :application_ages
end
