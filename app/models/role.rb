class Role < ActiveRecord::Base
  has_many :user
  has_and_belongs_to_many :actions, :join_table => :roles_actions
end
