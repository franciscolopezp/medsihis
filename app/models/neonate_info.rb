class NeonateInfo < ActiveRecord::Base
  belongs_to :gender
  belongs_to :birth_info
  belongs_to :blood_type
end
