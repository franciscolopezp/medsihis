class ActiveSubstance < ActiveRecord::Base
  has_and_belongs_to_many :medicaments
end
