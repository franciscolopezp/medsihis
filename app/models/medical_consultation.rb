class MedicalConsultation < ActiveRecord::Base
  has_and_belongs_to_many :diseases
  has_one :prescription
  belongs_to :medical_expedient
  belongs_to :office
  belongs_to :invoice
  belongs_to :consultation_type
  has_one :patient, through: :medical_expedient
  has_one :doctor, through: :medical_expedient
  belongs_to :user
  has_many :charges
  has_one :exploration
  has_one :uro_exploration
  has_and_belongs_to_many :pregnancies
  has_many :consultation_infos

  has_and_belongs_to_many :patient_valorations, :join_table => 'consultations_valorations'

  accepts_nested_attributes_for :diseases
  accepts_nested_attributes_for :prescription
  accepts_nested_attributes_for :exploration

  validates :diagnostic, :presence => { if: :validate_diseases?, :message => "Agregar diagnostico o una lista de enfermedades" }
  validates :date_consultation, :current_condition, :office, presence: :true
  validates :folio, unique_folio_medical_consultation_by_office: true
  validates :folio, :presence => {:message => "Agregue un folio al consultorio, para generar la consulta"}
  validates :price, presence: :true, numericality: { only_float: true}

  def validate_diseases?
    true if self.diseases.size == 0
  end

  def is_paid
    charged = 0
    self.charges.each do |c|
      t = c.get_transaction
      val = 0
      if !t.amount.nil? && !t.currency_exchange.nil?
        val = t.amount * t.currency_exchange
      end
      charged = charged + val
    end

    if charged >= self.price
      return true
    end

    false
  end

  def debt
    charged = 0
    self.charges.each do |c|
      t = c.get_transaction
      val = 0
      if !t.amount.nil? && !t.currency_exchange.nil?
        val = t.amount * t.currency_exchange
      end
      charged = charged + val
    end

    self.price - charged
  end
  def get_total
    total_amount = 0
    self.charges.each do |c|
      t = c.get_transaction
      val = 0

      if !t.nil? && !t.amount.nil? && !t.currency_exchange.nil?
        val = t.amount * t.currency_exchange
      end
      total_amount = total_amount + val
    end

   total_amount
  end
  def age_in_days_patient_in_consultation
    distance = Time.parse(self.date_consultation.to_s) - Time.parse(self.patient.birth_day.to_s)
    days = 0
    [
        [60, :seconds],
        [60, :minutes],
        [24, :hours],
        [365, :days],
        [100, :years]
    ].map do |count, name|
      if distance > 0
        distance, n = distance.divmod(count)
        if name.to_s == "years"
          n = 365 * n
        end
        if name.to_s == "years" or name.to_s == "days"
          days = days + n.to_i
          #puts "#{n.to_i} #{name}"
        end
      end
    end
    days
  end

  def self.pending_consultations_by_offices offices
    where("is_charged is not true AND office_id IN ("+offices.join(",")+") ")

  end

  def self.pending_consultations_by_doctor doctor_id
  joins("join offices on medical_consultations.office_id = offices.id")
  .where("medical_consultations.is_charged is not true AND offices.doctor_id = ?",doctor_id.to_s).order("medical_consultations.date_consultation ASC")
  end

  def self.total_grouped_by_day(ini_date,end_date,doctor_id)
    consults = joins(:medical_expedient).where("date_consultation >= '#{ini_date}'").where("date_consultation <  '#{end_date}'")#where(date_consultation: start.beginning_of_day..Time.zone.now)
    consults = consults.where("medical_expedients.doctor_id = #{doctor_id}")
    consults = consults.group("date(date_consultation)")
    consults = consults.select("date_consultation, sum(price) as total_price,COUNT(DISTINCT date_consultation) AS consults")
    consults.group_by { |o| o.date_consultation.to_date}
  end
  def self.total_grouped_by_day_auto(start,doctor_id)
    consults = joins(:medical_expedient).where(date_consultation: start.beginning_of_day..Time.zone.now)
    consults = consults.where("medical_expedients.doctor_id = #{doctor_id}")
    consults = consults.group("date(date_consultation)")
    consults = consults.select("date_consultation, sum(price) as total_price,COUNT(DISTINCT date_consultation) AS consults")
    consults.group_by { |o| o.date_consultation.to_date}
  end
end
