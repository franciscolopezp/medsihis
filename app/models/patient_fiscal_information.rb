class PatientFiscalInformation < ActiveRecord::Base
  belongs_to :city
  belongs_to :patient
  has_many :invoices
  has_many :clinic_invoices
end
