# La clase "Charge" representa un registro de tipo "Cobro de Consulta".
class Charge < ActiveRecord::Base

  # @!attribute transaction
  # @return [Transaction] Registro de tipo "Transacción" en los cuales se refleja un cobro de consulta.
  has_many :transactions, as: :chargeable

  # @!attribute medical_consultation
  # @return [MedicalConsultation] Registro de tipo "Consulta Médica" al cual pertenece el cobro.
  belongs_to :medical_consultation

  def get_transaction
    self.transactions.first
  end
end
