class Insurance < ActiveRecord::Base
  has_many :patients
  has_many :insurence_prices
  has_many :hospital_services, :through => :insurence_prices
end
