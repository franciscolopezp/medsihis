class StudyPackage < ActiveRecord::Base
  belongs_to :laboratory
  has_and_belongs_to_many :clinical_studies
end
