# La clase "Activity" representa un registro de tipo "Actividad de Médico" y es en donde se almacenan los datos de las citas, cirujías, vacaciones y demás actividades de un doctor.
class Activity < ActiveRecord::Base
  # @!attribute doctor
  # @return [Doctor] Registro de tipo "Doctor" al cual pertenece la actividad.
  belongs_to :doctor

  # @!attribute offices
  # @return [array(Office)] Registros de tipo "Consultorio" a los cuales afecta una actividad.
  has_and_belongs_to_many :offices

  # @!attribute activity_type
  # @return [ActivityType] Registro de tipo "Tipo de Actividad" el cual representa la clasificación que puede tener un registro de tipo "Actividad" los valores pueden ser [Cita, Cirujía, Vacaciones, Otros].
  belongs_to :activity_type

  # @!attribute activity_status
  # @return [ActivityStatus] Registro de tipo "Estado de Actividad" el cual representa el estado actual de una actividad los valores cambian de acuerdo al tipo de actividad.
  belongs_to :activity_status

  # @!attribute patients
  # @return [array(Patients)] Array que contiene 0 o 1 registro de tipo "Paciente", la relación se genera cuando la actividad es de tipo Cita.
  has_and_belongs_to_many :patients

  # @!attribute personalsupports
  # @return [array(Personalsupport)] Array que contiene registro de tipo "Personal de Apoyo (Para cirujía)", la relación se genera cuando la actividad es de tipo Cirugía.
  has_and_belongs_to_many :personalsupports

  has_and_belongs_to_many :user_patients

  accepts_nested_attributes_for :personalsupports

  # @return [array] Array que contiene datos de tipo entero los cuales representan ids de registros de tipo "Personal de Apoyo" asociados a la actividad.  
  def personal_supports_ids
    self.personalsupports.map {|p| p.id.to_s}
  end
end
