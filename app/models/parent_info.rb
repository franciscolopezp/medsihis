class ParentInfo < ActiveRecord::Base
  belongs_to :child_non_pathological_info
  belongs_to :blood_type
  def full_name
    first_name+" "+last_name
  end

end
