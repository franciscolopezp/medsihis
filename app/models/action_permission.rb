class ActionPermission < ActiveRecord::Base
  belongs_to :module_permission
  belongs_to :action
end
