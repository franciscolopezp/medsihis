class ServiceCharge < ActiveRecord::Base
  before_destroy :set_cost_discount
  after_destroy :update_admission_status
  belongs_to :hospital_service
  belongs_to :user
  belongs_to :hospital_admission

  def set_cost_discount
    @cost = self.cost
    @discount = self.discount
  end

  def update_admission_status
    admission = self.hospital_admission
    admission.update(total: admission.total - @cost, debt: admission.debt - (@cost - @discount))
  end
end
