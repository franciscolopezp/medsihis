class CabinetReport < ActiveRecord::Base
  belongs_to :clinical_analysis_order
  belongs_to :invoice
  belongs_to :user
  has_many :cabinet_report_pictures
  has_many :charge_report_studies

  scope :pending_by_offices, -> (offices){
    joins(:clinical_analysis_order)
        .where(:is_charged => false)
        .where(:clinical_analysis_orders => {office_id: offices})
  }

  scope :pending_by_doctor, -> (doctor_id){
    joins(:clinical_analysis_order => :office)
        .where(:is_charged => false)
        .where(:offices => {doctor_id: doctor_id})
  }

  def get_total
    total_amount = 0
    self.charge_report_studies.each do |c|
      t = c.transaction_item
      val = 0
      if !t.amount.nil? && !t.currency_exchange.nil?
        val = t.amount * t.currency_exchange
      end
      total_amount = total_amount + val
    end

    total_amount
  end

  def is_paid
    return (get_total >= self.price)
  end

  def get_balance
    return (self.price - get_total)
  end
end
