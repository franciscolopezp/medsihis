class MedicamentPurchaseItem < ActiveRecord::Base
  has_one :medicament_log, as: :traceable
  belongs_to :medicament_purchase
  belongs_to :lot
  belongs_to :medicament
end
