class Transaction < ActiveRecord::Base
  belongs_to :chargeable, polymorphic: true
  has_and_belongs_to_many :accounts
  has_and_belongs_to_many :daily_cashes
  belongs_to :transaction_type
  belongs_to :payment_method
  belongs_to :user
  belongs_to :invoice
  belongs_to :doctor
  belongs_to :currency


  def get_account
    self.accounts.first
  end

  def self.total_grouped_by_day_auto(start,doctor_id)

    transacs = where(date: start.beginning_of_day..Time.zone.now)
    transacs = transacs.where("doctor_id = #{doctor_id}")
    transacs = transacs.group("date(date)")
    transacs = transacs.select("date, (sum(IF(t_type = 0,amount,0))* -1) as egreso,sum(IF(t_type = 1,amount,0)) as ingreso,(sum(IF(t_type = 0,amount,0))* -1) + sum(IF(t_type = 1,amount,0)) as total,COUNT(DISTINCT date) AS transacs")
    transacs.group_by { |o| o.date.to_date}

  end
  def self.total_grouped_by_day(ini_date,end_date,doctor_id)

    transacs = where("date >= '#{ini_date}'").where("date <  '#{end_date}'")
    transacs = transacs.where("doctor_id = #{doctor_id}")
    transacs = transacs.group("date(date)")
    transacs = transacs.select("date, (sum(IF(t_type = 0,amount,0))* -1) as egreso,sum(IF(t_type = 1,amount,0)) as ingreso,(sum(IF(t_type = 0,amount,0))* -1) + sum(IF(t_type = 1,amount,0)) as total,COUNT(DISTINCT date) AS transacs")
    transacs.group_by { |o| o.date.to_date}

  end
end
