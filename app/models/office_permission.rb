class OfficePermission < ActiveRecord::Base
  belongs_to :assistant
  belongs_to :office
  has_one :doctor, through: :office
  belongs_to :permission
end
