class OtherChargeTransaction < ActiveRecord::Base
  has_many :transactions, as: :chargeable
  belongs_to :other_charge
  belongs_to :user
end
