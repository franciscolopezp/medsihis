# La clase "ImcAdult" representa un registro de tipo "IMC de Adulto".
class ImcAdult < ActiveRecord::Base
  
  # @!attribute vital_signs
  # @return [array(VitalSign)] Array de registros de tipo "Signos Vitales" asociados al IMC de adulto.
  has_many :vital_signs

  # @!attribute associate
  # @return [ImcAdult] Registro de tipo "IMC de Adulto" que representa un máximo o un mínimo de una categoría.
  has_one :associate, class_name: 'ImcAdult', foreign_key: 'association_id'

  # @!attribute category_imc
  # @return [CategoryImc] Registro de tipo "Categoría de IMC" al cual pertenece el IMC de adulto.
  belongs_to :category_imc

  # @!attribute rank_imc_adult
  # @return [RankImcAdult] Registro de tipo "Tipo de IMC Adulto" asignado al IMC de adulto, indica si el imc es mínimo o máximo en la categoría.
  belongs_to :rank_imc_adult

end
