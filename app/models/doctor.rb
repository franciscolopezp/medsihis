# La clase "Doctor" representa un registro de tipo "Doctor".
class Doctor < ActiveRecord::Base

  # @!attribute user
  # @return [User] Registro de tipo "Usuario" que contiene las credenciales de acceso del doctor.  
  belongs_to :user, dependent: :delete

  # @!attribute laboratory
  # @return [Laboratory] Registro de tipo "Laboratorio" que representa el laboratorio con el cual el Dr tiene convenio, ésto agiliza el proceso de emitir órdenes de análisis clínicos.  
  belongs_to :laboratory
  has_and_belongs_to_many :users
  # @!attribute city
  # @return [City] Registro de tipo "Ciudad" que le fue asignado al doctor.


  # @!attribute fiscal_information
  # @return [FiscalInformation] Registro de tipo "Información" asociado a un doctor, este registro contiene los datos que un médico necesita para emitir una factura.
  belongs_to :fiscal_information

  # @!attribute identity_cards
  # @return [array(IdentityCard)] Array de registros de tipo "Cédula Profesional" asociados a un doctor, un doctor puede agregar sus especialidades y especificar el número de cédula profesional y la institución que la emitió.  
  has_many :identity_cards

  # @!attribute specialties
  # @return [array(Specialty)] Array de registros de tipo "Especialidad" asociados a un doctor a través de sus cédulas profesionales.
  has_many :specialties, :through => :identity_cards

  # @!attribute licenses
  # @return [array(License)] Array de registros de tipo "Licencia" asociados a un doctor.
  has_many :licenses

  # @!attribute licenses
  # @return [array(License)] Array de registros de tipo "Licencia" asociados a un doctor.


  has_many :clinical_analysis_orders, through: :medical_expedients

  has_many :medical_consultations, through: :medical_expedients

  has_many :consultation_types

  has_many :type_certificates


  has_many :hospital_admissions
  # @!attribute offices
  # @return [array(Office)] Array de registros de tipo "Consultorio" asociados a un doctor.
  has_many :offices

  # @!attribute activities
  # @return [array(Activity)] Array de registros de tipo "Activity" qasociados a un doctor.
  has_many :activities

  # @!attribute email_settings
  # @return [array(EmailSetting)] Array de registros de tipo "Configuración de Correo" asociados a un doctor.
  has_many :email_settings

  # @!attribute allergies
  # @return [array(Allergy)] Array de registros de tipo "Alergia" asociados a un doctor.
  has_many :allergies

  # @!attribute diseases
  # @return [array(Disease)] Array de registros de tipo "Enfermedad" asociados a un doctor.
  has_many :diseases

  # @!attribute patients
  # @return [array(Patient)] Array de registros de tipo "Paciente" asociados a un doctor.


  # @!attribute invoices
  # @return [array(Invoice)] Array de registros de tipo "Factura" asociados a un doctor.
  has_many :invoices

  # @!attribute medicaments
  # @return [array(Medicament)] Array de registros de tipo "Medicamento" asociados a un doctor.
  has_many :medicaments

  # @!attribute accounts
  # @return [array(Account)] Array de registros de tipo "Cuenta" asociados a un doctor.

  has_many :general_fiscal_infos
  # @!attribute assistants
  # @return [array(Assistant)] Array de registros de tipo "Asistente" asociados a un doctor.
  has_and_belongs_to_many :assistants

  # relacion de doctores favoritos de un paciente
  has_and_belongs_to_many :user_patients

  # @!attribute daily_cashes
  # @return [array(DailyCash)] Array de registros de tipo "Corte de Caja" asociados a un doctor.
  has_many :daily_cashes, through: :accounts

  # @!attribute personalsuppots
  # @return [array(Personalsupport)] Array de registros de tipo "Personal de Apoyo" asociados a un doctor.
  has_many :personalsupports

  belongs_to :time_zone

  has_many :currency_exchanges
  has_many :currencies, through: :currency_exchanges


  has_one :custom_template_report

  has_and_belongs_to_many :laboratories

  def full_name_title
    str = ''
    if self.user.person.gender_id == Gender::MAN
      str << 'Dr. '
    else
      str << 'Dra. '
    end
    str << self.user.person.name+' '+self.user.person.last_name
    return str
  end

  accepts_nested_attributes_for :fiscal_information
  mount_uploader :digital_sign, DoctorFilesUploader
end
