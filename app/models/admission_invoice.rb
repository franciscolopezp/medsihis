class AdmissionInvoice < ActiveRecord::Base
  belongs_to :user
  belongs_to :hospital_admission
  belongs_to :payment_way
  has_many :admission_invoice_concepts
  has_one :admission_invoice_emisor
  has_one :admission_invoice_receptor
  belongs_to :emisor_fiscal_information
end
