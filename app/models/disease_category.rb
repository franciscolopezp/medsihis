# La clase "DiseaseCategory" representa un registro de tipo "Categoría de Enfermedad".
class DiseaseCategory < ActiveRecord::Base
  
  # @!attribute diseases
  # @return [array(Disease)] Array de registros de tipo "Enfermedad" a las cuales se le ha asignado la categoría.
  has_many :diseases
end
