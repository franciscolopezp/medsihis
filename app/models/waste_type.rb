class WasteType < ActiveRecord::Base
  has_many :medicament_wastes
  has_many :pharmacy_item_wastes
end
