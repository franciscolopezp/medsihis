class Invoice < ActiveRecord::Base
  has_many :transactions
  has_many :medical_consultations
  has_many :cabinet_reports
  has_many :invoice_concepts
  has_one :open_invoice_receptor
  has_one :invoice_emisor
  belongs_to :patient_fiscal_information
  belongs_to :payment_method
  belongs_to :doctor
  belongs_to :payment_way
end
