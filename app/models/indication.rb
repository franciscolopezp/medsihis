class Indication < ActiveRecord::Base
  belongs_to :ca_group
  has_and_belongs_to_many :clinical_studies
end
