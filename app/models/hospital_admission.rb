class HospitalAdmission < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :user
  belongs_to :attention_type
  belongs_to :triage
  belongs_to :patient
  has_many :admission_transactions
  has_many :medicament_assortments
  has_and_belongs_to_many :patient_valorations, :join_table => 'hospitalizations_valorations'
  has_and_belongs_to_many :medical_annotations, :join_table => 'hospitalizations_annotations'
  has_many :service_charges
  has_many :hospital_admission_discounts
  has_many :admission_invoices
end
