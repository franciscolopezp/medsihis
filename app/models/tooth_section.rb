class ToothSection < ActiveRecord::Base
  belongs_to :tooth
  has_many :tooth_pathologies
  has_many :tooth_treatments
end
