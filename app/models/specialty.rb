class Specialty < ActiveRecord::Base
  has_many :identity_cards
  has_many :doctors, :through => :identity_cards

end
