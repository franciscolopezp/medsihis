class FixedAssetCategory < ActiveRecord::Base
  has_many :fixed_assets
end
