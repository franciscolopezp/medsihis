class WaitingRoomActivity < ActiveRecord::Base
  has_and_belongs_to_many :waiting_room_statuses, :join_table => 'wroom_statuses_wroom_activities'
end
