class ServicePackage < ActiveRecord::Base
  has_many :package_service_costs
  has_many :hospital_services, :through => :package_service_costs
end
