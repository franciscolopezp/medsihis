class MedicamentPurchaseFarmacyItem < ActiveRecord::Base
  has_one :pharmacy_item_log, as: :traceable
  belongs_to :medicament_purchase
  belongs_to :pharmacy_item
end
