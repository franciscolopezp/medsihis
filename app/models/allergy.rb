# La clase "Allergy" representa un registro de tipo "Alergia" global o que pertenece a un doctor.
class Allergy < ActiveRecord::Base

  # @!attribute doctor
  # @return [Doctor] Registro de tipo "Doctor" al cual pertenece la alergia, si es nil significa que es global en el sistema.
  belongs_to :doctor

  # @!attribute patients
  # @return [array(Patient)] Array de registros de tipo "Paciente" a los cuales se les ha asociado la alergia.
  has_and_belongs_to_many :patients
end
