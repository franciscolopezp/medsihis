# La clase "ActivityStatus" representa un registro de tipo "Estado de Actividad" el cual es asignado a una actividad según sea el estado en el que se encuentre.
class ActivityStatus < ActiveRecord::Base


  # @!attribute activities
  # @return [array(Activity)] Array de registros de tipo "Actividad" que tienen asignado el estado.
  has_many :activities

  # @!attribute activity_type
  # @return [ActivityType] Registro de tipo "Tipo de Actividad" al cual pertenece el estado de actividad.
  belongs_to :activity_type

  SCHEDULED = 1
  WAITING = 2
  CANCELED = 5
  CONFIRM_PENDING = 12
end
