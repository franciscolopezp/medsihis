class Personalsupport < ActiveRecord::Base
  belongs_to :operation_role
  belongs_to :doctor
  accepts_nested_attributes_for :operation_role
  has_and_belongs_to_many :activities

  def full_name
    self.name+" "+self.lastname
  end
end
