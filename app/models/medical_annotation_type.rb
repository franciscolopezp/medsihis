class MedicalAnnotationType < ActiveRecord::Base
  has_many :medical_annotation_fields
  has_many :medical_annotation_images
  has_and_belongs_to_many :medical_annotation_linked_fields, :join_table => "medical_annotations_linked_fields"

  mount_uploader :template_file, MedicalAnnotationUploader
end
