class Section < ActiveRecord::Base
  has_many :actions
  has_many :module_permissions
end
