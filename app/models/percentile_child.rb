class PercentileChild < ActiveRecord::Base
  belongs_to :gender
  belongs_to :type_percentile

  PERCENTILE_GRAPHIC = [3, 10, 25, 50, 75, 90, 97]
end
