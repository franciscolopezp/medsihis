class WaitingRoomStatus < ActiveRecord::Base
  belongs_to :waiting_room_item_status
  has_and_belongs_to_many :waiting_room_activities, :join_table => 'wroom_statuses_wroom_activities'
end
