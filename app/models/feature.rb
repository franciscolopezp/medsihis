# La clase "Feature" representa un registro de tipo "Especialidad del Sistema".
class Feature < ActiveRecord::Base

  # Especialidad del sistema identificada por un string único - General.
  GEN = 'gen'
  # Especialidad del sistema identificada por un string único - Pediatría.
  PED = 'ped'
  # Especialidad del sistema identificada por un string único - Ginecología.
  GIN = 'gin'
  # Especialidad del sistema identificada por un string único - Urología.
  URO = 'uro'

  # @!attribute license_features
  # @return [array(LicenseFeature)] Array de registros de tipo "Funcionalidad de Licencia" que representan la relación que una especialidad del sistema tiene con una licencia de un doctor.  
  has_many :license_features

  # @!attribute licenses
  # @return [array(Licenses)] Array de registros de tipo "Licencia" asociados con la especialidad del sistema a través de una funcionalidad de licencia.  
  has_many :licenses, :through => :license_features

end
