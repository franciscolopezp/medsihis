class PharmacyItemLog < ActiveRecord::Base
  belongs_to :traceable, polymorphic: true
  belongs_to :user
  belongs_to :pharmacy_item
  belongs_to :warehouse
end
