class FixedAssetActivity < ActiveRecord::Base
  belongs_to :fixed_asset
  has_many :asset_activities
end
