class CustomTemplatePrescription < ActiveRecord::Base
    has_many :custom_template_prescription_fields

  accepts_nested_attributes_for :custom_template_prescription_fields

  validates :header_cm, :footer_cm, presence: true
  validates :header_cm, :footer_cm, numericality: {only_float: true}, allow_blank: false, allow_nil: false


  ORIENTATIONS = [{"Landscape" => "Horizontal"}, {"Portrait" => "Vertical"}]

  PAGE_SIZE = [{"A5" => "Media Carta"}, {"Letter" => "Carta Completa"}]

end
