class BloodType < ActiveRecord::Base
  has_many :patients
  has_many :parent_infos
  has_many :neonate_infos
end
