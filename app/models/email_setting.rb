# La clase "EmailSetting" representa un registro de tipo "Configuración de correo".
class EmailSetting < ActiveRecord::Base

  # @!attribute doctor
  # @return [Doctor] Registro de tipo "Doctor" al cual pertenece la configuración de correo.  
  belongs_to :doctor
end
