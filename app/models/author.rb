class Author < ActiveRecord::Base
  has_many :news_items
  mount_uploader :profile_image, AuthorsUploader
  def full_name
    self.name+' '+self.last_name
  end
end
