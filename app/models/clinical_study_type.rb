# La clase "ClinicalStudyType" representa un registro de tipo "Tipo de Estudio Clínico" el cual sirve para clasificar estudios clínicos.
class ClinicalStudyType < ActiveRecord::Base

  # @!attribute clinical_studies
  # @return [array(ClinicalStudies)] Array de registros de tipo "Estudio Clínicos" a los cuales se le ha asociado el tipo de estudio clínico.
  has_many :clinical_studies

  belongs_to :laboratory

  # Tipo de Estudio Clínico -  Hematología
  HEMATOLOGIA_TYPE = 1

  # Tipo de Estudio Clínico -  Coagulación
  COAGULACION_TYPE = 2

  # Tipo de Estudio Clínico -  Química Clínica
  QUIMICA_CLINICA_TYPE = 3

  # Tipo de Estudio Clínico -  Estudios en Orina
  ESTUDIOS_EN_ORINA_TYPE = 4

  # Tipo de Estudio Clínico -  Serología
  SEROLOGIA_TYPE = 5

  # Tipo de Estudio Clínico -  Inmunología
  INMUNOLOGIA_TYPE = 6

  # Tipo de Estudio Clínico -  Perfil Torch
  PERFIL_TORCH_TYPE = 7

  # Tipo de Estudio Clínico -  Perfil de Alergenos
  PERFIL_DE_ALERGENOS_TYPE = 8

  # Tipo de Estudio Clínico -  Electroforesis
  ELECTROFORESIS_TYPE = 9

  # Tipo de Estudio Clínico -  Enfermedades Infecciosas
  ENFERMEDADES_INFECCIOSAS_TYPE = 10

  # Tipo de Estudio Clínico -  Infecciones Ginecológicas
  INFECCIONES_GINECOLOGICAS_TYPE = 11

  # Tipo de Estudio Clínico -  Enfermedad Celiaca
  ENFERMEDAD_CELIACA_TYPE = 12

  # Tipo de Estudio Clínico -  Estudios Tiroideos
  ESTUDIOS_TIROIDEOS_TYPE = 13

  # Tipo de Estudio Clínico -  Perfil de Diabetes
  PERFIL_DE_DIABETES_TYPE = 14

  # Tipo de Estudio Clínico -  Perfil Reproductivo
  PERFIL_REPRODUCTIVO_TYPE = 15

  # Tipo de Estudio Clínico -  Perfil Suprarenal
  PERFIL_SUPRARENAL_TYPE = 16

  # Tipo de Estudio Clínico -  Marcadores Tumorales
  MARCADORES_TUMORALES_TYPE = 17

  # Tipo de Estudio Clínico -  Marcadores Cardiacos
  MARCADORES_CARDIACOS_TYPE = 18

  # Tipo de Estudio Clínico -  Drogas de Abuso
  DROGAS_DE_ABUSO_TYPE = 19

  # Tipo de Estudio Clínico -  Drogas Terapéuticas
  DROGAS_TERAPEUTICAS_TYPE = 20

  # Tipo de Estudio Clínico -  Tamizaje Neonatal
  TAMIZAJE_NEONATAL_TYPE = 21

  # Tipo de Estudio Clínico -  Síndrome Antifosfolípidos
  SINDROME_ANTIFOSFOLIPIDOS_TYPE = 22

  # Tipo de Estudio Clínico -  Biología Molecular
  BIOLOGIA_MOLECULAR_TYPE = 23

  # Tipo de Estudio Clínico -  Peril de Hepatitis
  PERFIL_DE_HEPATITIS_TYPE = 24

  # Tipo de Estudio Clínico -  Perfil de Osteoporosis
  PERFIL_DE_OSTEOPOROSIS_TYPE = 25

  # Tipo de Estudio Clínico -  Bacteriología
  BACTERIOLOGIA_TYPE = 26

  # Tipo de Estudio Clínico -  Hematología
  PARASITOLOGIA_TYPE = 27

  # Tipo de Estudio Clínico -  Citoquímicos de Líquidos
  CITOQUIMICOS_DE_LIQUIDOS_CORPORALES_TYPE = 28

  # Tipo de Estudio Clínico -  Estudios de Espermatobioscopía
  ESTUDIOS_DE_ESPERMATOBIOSCOPIA_TYPE = 29

  # Tipo de Estudio Clínico (Marcadores Diversos) -  Sepsis
  SEPSIS_TYPE = 30

  # Tipo de Estudio Clínico (Marcadores Diversos) -  Crecimiento
  CRECIMIENTO_TYPE = 31

  # Tipo de Estudio Clínico (Marcadores Diversos) -  Hepáticos
  HEPATICOS_TYPE = 32

  # Tipo de Estudio Clínico (Marcadores Diversos) -  Inflamatorios
  INFLMATORIOS_TYPE = 33

  # =============================================================
  # Tipo de Estudio de Gabinete (Categoría general gabinete)
  GENERAL_GABINETE = 34

end
