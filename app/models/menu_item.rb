class MenuItem < ActiveRecord::Base
  belongs_to :action
  has_many :menu_item_children
end
