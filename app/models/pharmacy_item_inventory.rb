class PharmacyItemInventory < ActiveRecord::Base
  belongs_to :pharmacy_item
  belongs_to :warehouse
  belongs_to :user
end
