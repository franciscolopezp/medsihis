class Surgery < ActiveRecord::Base
  has_one :pathological, as: :section
end
