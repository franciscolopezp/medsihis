class InvoiceConcept < ActiveRecord::Base
  belongs_to :invoice
  belongs_to :measure_unit
end
