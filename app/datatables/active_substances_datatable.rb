class ActiveSubstancesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: active_substances.total_entries,
        rows: data,
        total: active_substances.count,
    }
  end

  private

  def data
    active_substances.map do |active_substance|
      {
          "id" => active_substance.id,
          "name" => active_substance.name,
          "url_show"=>admin_active_substance_path(active_substance),
          "url_edit"=>edit_admin_active_substance_path(active_substance),
          "url_destroy"=>admin_active_substance_path(active_substance,format: :json)
      }

    end
  end

  def active_substances
    @active_substance ||= fetch_active_substances
  end

  def fetch_active_substances
    active_substances = ActiveSubstance.order("#{sort_column} #{sort_direction}")
    active_substances = active_substances.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      active_substances = active_substances.where("id like :search or name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    active_substances
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end