class TransactionsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,doctor_id)
    @view = view
    @doctor_id = doctor_id
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: transactions.total_entries,
        rows: data,
        total: transactions.total_entries
    }
  end

  private



  def data
    serie = ""
    transactions.map do |consultation|
      #transaction = charge.transaction
      folio = 0

      if params[:type].to_i == 1

        {
            "id" => consultation.id,
            "amount" =>  number_to_currency(consultation.get_total, :unit => "$"),
            "description" => "Cobro de consulta",
            "patient" => "<a href='"+ edit_doctor_patient_path(consultation.patient.id) +"'>" + consultation.patient.full_name + "</a>",
            "date" => consultation.date_consultation.strftime('%d/%m/%Y'),
            "patient_name" => consultation.patient.full_name,
            "serie" => serie,
            "type" => "consult",
            "patient_id" => consultation.patient.id,
            "print"=>print_pdf_doctor_transactions_path(consultation,folio, format: :pdf),
            "download_xml"=>transaction_download_xml_doctor_transactions_path(consultation,folio, format: :xml)
        }

      else
        {
            "id" => consultation.id,
            "amount" =>  number_to_currency(consultation.get_total, :unit => "$"),
            "description" => "Cobro de estudio",
            "patient" => "<a href='"+ edit_doctor_patient_path(consultation.clinical_analysis_order.medical_expedient.patient.id) +"'>" + consultation.clinical_analysis_order.medical_expedient.patient.full_name + "</a>",
            "date" => consultation.created_at.strftime('%d/%m/%Y'),
            "patient_name" => consultation.clinical_analysis_order.medical_expedient.patient.full_name,
            "serie" => serie,
            "type" => "report",
            "patient_id" => consultation.clinical_analysis_order.medical_expedient.patient.id,
            "print"=>print_pdf_doctor_transactions_path(consultation,folio, format: :pdf),
            "download_xml"=>transaction_download_xml_doctor_transactions_path(consultation,folio, format: :xml)
        }

      end

    end
  end

  def transactions
    @specialty ||= fetch_transactions
  end

  def fetch_transactions

    if @doctor_id == nil
      @doctor_id = params[:doctor_id]
    end
    if params[:type].to_i == 1
    if params[:patient].to_s == "all"
      transactions = MedicalConsultation.joins(:medical_expedient => :doctor).where("medical_expedients.doctor_id = #{@doctor_id} AND medical_consultations.price > 0")
    else
      transactions = MedicalConsultation.joins(:medical_expedient => :doctor).where("medical_expedients.doctor_id = #{@doctor_id} AND medical_expedients.patient_id = #{params[:patient].to_i} AND medical_consultations.price > 0")
    end
    transactions = transactions.page(page).per_page(per_page)

    else
      if params[:patient].to_s == "all"
        transactions = CabinetReport.joins(:clinical_analysis_order => :medical_expedient).where("medical_expedients.doctor_id = #{@doctor_id}")
      else
        transactions = CabinetReport.joins(:clinical_analysis_order => :medical_expedient).where("medical_expedients.doctor_id = #{@doctor_id} AND medical_expedients.patient_id = #{params[:patient].to_i}")
      end
      transactions = transactions.page(page).per_page(per_page)

    end

    transactions.where("invoiced = 0 AND is_charged = 1").order("#{sort_column} #{sort_direction}",created_at: :desc)

  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
