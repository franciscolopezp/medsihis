class InventoriesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: inventories.total_entries,
        rows: data,
        total: fetch_inventories.count,
    }
  end

  private

  def data

    inventories.map do |inventory|
      {
          "id" => inventory.id,
          "is_closed" => inventory.is_closed,
          "inventory_date" => inventory.inventory_date.present? ? inventory.inventory_date.strftime("%d/%m/%Y") : '',
          "warehouse" => inventory.warehouse.present? ? inventory.warehouse.name: '',
          "category" => inventory.medicament_category.present? ? inventory.medicament_category.name: '',
          "created_by" => inventory.created.present? ? inventory.created.person.name: '',
          "updated_by" => inventory.updated.present? ? inventory.updated.person.name: '',
          "url_edit"=> edit_admin_inventory_path(inventory),
          "print_url"=>print_pdf_inventory_report_admin_inventories_path(inventory, format: :pdf),
      }
    end
  end

  def inventories
    @inventory ||= fetch_inventories
  end

  def fetch_inventories
    inventories = Inventory.joins(:created => :person)
                      .joins("LEFT JOIN warehouses ON warehouses.id = inventories.warehouse_id")
                      .joins("LEFT JOIN medicament_categories ON medicament_categories.id = inventories.medicament_category_id")
                      .joins("LEFT JOIN users users2 ON users2.id = inventories.created_user_id")
                      .joins("LEFT JOIN people people2 ON people2.id = users2.person_id")
                      .order("#{sort_column} #{sort_direction}")

    inventories = inventories.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      inventories = inventories.where("inventories.id like :search "+
                                      "OR inventory_date like :search "+
                                      "OR warehouses.name like :search "+
                                      "OR medicament_categories.name like :search "+
                                      "OR people.name like :search", search: "%#{params[:searchPhrase]}%")

    end
    inventories
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end
