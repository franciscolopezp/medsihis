class DoctorsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: doctors.total_entries,
        rows: data,
        total: Doctor.count,
    }
  end

  private

  def data
    doctors.map do |doctor|
      {
          "id" => doctor.id,
          "name" => doctor.user.person.name,
          "last_name" => doctor.user.person.last_name,
          "address" => doctor.user.person.address,
          "doctor.city" => doctor.user.person.city.name + "<br>" + doctor.user.person.city.state.name+ "," + doctor.user.person.city.state.country.name,
          "telephone" => doctor.user.person.telephone + "<br>" + doctor.user.person.cellphone,
          "url_show"=>admin_doctor_path(doctor),
          "url_edit"=>edit_admin_doctor_path(doctor),
          "url_destroy"=>admin_doctor_path(doctor, format: :json),
          "url_send_email"=>send_email_password_admin_doctors_path(doctor, format: :json),
          "email"=>doctor.user.email,
          "doctor.fiscal_information" => "RFC: " + doctor.fiscal_information.rfc + "<br>" + doctor.fiscal_information.bussiness_name

      }

    end
  end

  def doctors
    @doctor ||= fetch_doctors
  end

  def fetch_doctors
    doctors = Doctor.order("#{sort_column} #{sort_direction}")
    doctors = doctors.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      doctors = doctors.joins(:city).joins(:fiscal_information).where("doctors.id like :search or doctors.name like :search or last_name like :search or address like :search or telephone like :search or cellphone like :search or cities.name like :search or fiscal_informations.rfc like :search or fiscal_informations.bussiness_name like :search", search: "%#{params[:searchPhrase]}%")
    end
    doctors
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
