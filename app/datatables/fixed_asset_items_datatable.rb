class FixedAssetItemsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: fixed_asset_items.total_entries,
        rows: data,
        total: FixedAssetItem.count,
    }
  end

  private

  def data
    fixed_asset_items.map do |fixed_asset_item|
      if fixed_asset_item.state
        state = "Disponible"
      else
        state = "No disponible"
      end
      if fixed_asset_item.schedule
        schedule = "Calendarizado"
      else
        schedule = "No calendarizado"
      end
      {
          "id" => fixed_asset_item.id,
          "name" => fixed_asset_item.name,
          "description" => fixed_asset_item.description,
          "indetifier" => fixed_asset_item.identifier,
          "state" => state,
          "schedule" => schedule,
          "fixed_asset" => fixed_asset_item.fixed_asset.name,
          "url_show"=>admin_fixed_asset_items_path(fixed_asset_item),
          "url_edit"=>edit_admin_fixed_asset_item_path(fixed_asset_item),
          "url_destroy"=>admin_fixed_asset_item_path(fixed_asset_item,format: :json)
      }

    end
  end

  def fixed_asset_items
    @fixed_asset_item ||= fetch_fixed_asset_items
  end

  def fetch_fixed_asset_items
    fixed_asset_items = FixedAssetItem.joins(:fixed_asset).order("#{sort_column} #{sort_direction}")
    fixed_asset_items = fixed_asset_items.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      fixed_asset_items = fixed_asset_items.where("fixed_assets.name like :search or fixed_asset_items.name like :search or fixed_asset_activities.name like :search or identifier like :search", search: "%#{params[:searchPhrase]}%")
    end
    fixed_asset_items
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end