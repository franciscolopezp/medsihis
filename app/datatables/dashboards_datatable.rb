class DashboardsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: patient_books.total_entries,
        rows: data,
        total: Patient.count,
    }
  end

  private

  def data
    patient_books.map do |patient|
      {
          "id" => patient.id,
          "name" => patient.name,
          "vaccine_books" => (patient.vaccine_books.map do |book|
          book.name
          end).to_s,
          "url_show"=>doctor_patient_vaccine_book_path(patient)

      }

    end
  end

  def patient_books
    @doctor_patient ||= fetch_patient_books
  end

  def fetch_patient_books
    patient_books = Patient.order("#{sort_column} #{sort_direction}")
    patient_books = patient_books.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      patient_books = patient_books.where("id like :search or name", search: "%#{params[:searchPhrase]}%")
    end
    patient_books
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
