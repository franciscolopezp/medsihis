class AddressBooksDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,doctor_id,office_id)
    @view = view
    if doctor_id != nil
      @shared = false
      @doctor_id = doctor_id
    else
      @shared = true
      @doctor_id = params[:doctor_id]
    end
    @office_id = office_id
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: abooks.total_entries,
        rows: data,
        total: abooks.total_entries
    }
  end

  private

  def data
    abooks.map do |contact|
      if contact.office != nil
        doctor_office = contact.office.name
      else
        doctor_office = "Todos"
      end
      if contact.hospital != nil
        hospital_name = contact.hospital.name
      else
        hospital_name = "Ninguno"
      end
      if @shared
        {
            "id" => contact.id,
            "name" => contact.name,
            "telephone" => contact.telephone,
            "cellphone" => contact.cellphone,
            "email" => contact.email,
            "address" => contact.address,
            "hospital" => hospital_name,
            "office" => doctor_office,
            "url_show"=>shared_assistant_address_book_path(contact),
            "url_edit"=>edit_shared_assistant_address_book_path(contact),
            "url_destroy"=>shared_assistant_address_book_path(contact,format: :json)
        }
      else
        {
            "id" => contact.id,
            "name" => contact.name,
            "telephone" => contact.telephone,
            "cellphone" => contact.cellphone,
            "email" => contact.email,
            "address" => contact.address,
            "hospital" => hospital_name,
            "office" => doctor_office,
            "url_show"=>doctor_address_book_path(contact),
            "url_edit"=>edit_doctor_address_book_path(contact),
            "url_destroy"=>doctor_address_book_path(contact,format: :json)
        }
      end


    end
  end

  def abooks
    @personal ||= fetch_abooks
  end

  def fetch_abooks
    abooks = AddressBook.joins("LEFT JOIN hospitals ON address_books.hospital_id = hospitals.id").joins("LEFT JOIN offices ON address_books.office_id = offices.id").order("#{sort_column} #{sort_direction}")
    abooks = abooks.page(page).per_page(per_page)
    abooks = abooks.where("address_books.doctor_id = #{@doctor_id}")
    if params[:user_type].to_s != "1"
      abooks = abooks.where("address_books.office_id = #{@office_id} OR address_books.office_id is null")
    end
    if params[:searchPhrase].present?
      abooks = abooks.where("address_books.id like :search or address_books.name like :search or address_books.telephone like :search or address_books.cellphone like :search or address_books.email like :search or address_books.address like :search or hospitals.name like :search or offices.name like :search", search: "%#{params[:searchPhrase]}%")
    end
    abooks
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columns = {:id=>'address_books.id', :name=>'address_books.name', :telephone=> 'address_books.telephone', :email=>'address_books.email', :cellphone => 'address_books.cellphone', :hospital=> 'hospitals.name', :office => 'offices.name', :address => 'address_books.address'}
    #columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #columna = "#{key}"
        return columns[:"#{key}"]
      end
    end
  end
end
