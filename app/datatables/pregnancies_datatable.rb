class PregnanciesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,doctor)
    @view = view
    @doctor_id = doctor
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: pregnancies.total_entries,
        rows: data,
        total: pregnancies.total_entries
    }
  end

  private

  def data
    pregnancies.map do |pregnancie|
      {
          "patient" => pregnancie.medical_expedient.patient.name + " " +pregnancie.medical_expedient.patient.last_name,
          "age" => format_patient_age(pregnancie.medical_expedient.patient.birth_day),
          "last_consult" => pregnancie.medical_expedient.medical_consultations.last.date_consultation.strftime('%d/%m/%Y'),
          "start_date" => pregnancie.last_menstruation_date.strftime('%d/%m/%Y'),
          "end_date"=>pregnancie.probable_birth_date.strftime('%d/%m/%Y'),
      }

    end
  end

  def format_patient_age date
    age = @view.distance_of_time_in_words_hash(Time.now, date)
    age_string = ''
    if age[:years] != nil && age[:years] > 0
      age_string << age[:years].to_s + ' año'
      if age[:years] > 1
        age_string << 's'
      end
    end

    if age[:months] != nil && age[:months] > 0
      age_string << ' ' + age[:months].to_s + ' mes'
      if age[:months] > 1
        age_string << 'es'
      end
    end
    age_string
  end

  def pregnancies
    @specialty ||= fetch_pregnancies
  end

  def fetch_pregnancies
    pregnancies = Pregnancy.joins(:medical_expedient => :medical_consultations).joins(:medical_expedient => :patient).joins(:medical_expedient => :doctor).where("doctors.id = #{@doctor_id} and pregnancies.active = true")
    pregnancies = pregnancies.where("MONTH(pregnancies.probable_birth_date) >= #{params[:month_start]} and MONTH(pregnancies.probable_birth_date) <= #{params[:month_end]} and YEAR(pregnancies.probable_birth_date) = #{params[:year]}").distinct()
    pregnancies = pregnancies.order("#{sort_column} #{sort_direction}")
    pregnancies = pregnancies.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      pregnancies = pregnancies.where("patients.name like :search or patients.last_name like :search", search: "%#{params[:searchPhrase]}%")
    end
    pregnancies
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column

    columns = {:patient=>'patients.name', :age=> 'patients.birth_day', :last_consult=>'medical_consultations.date_consultation', :start_date => 'pregnancies.last_menstruation_date', :end_date=> 'pregnancies.probable_birth_date'}
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #columna = "#{key}"
        return columns[:"#{key}"]
      end
    end
=begin
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
=end
  end
end
