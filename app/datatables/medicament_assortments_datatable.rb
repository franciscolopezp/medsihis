class MedicamentAssortmentsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: medicament_assortments.total_entries,
        rows: data,
        total: MedicamentAssortment.count,
    }
  end

  private

  def data
    medicament_assortments.map do |assortment|
      {
          "id" => assortment.id,
          "warehouse" => assortment.warehouse.name,
          "patient" => assortment.hospital_admission.patient.full_name,
          "date" => assortment.created_at.strftime("%d/%m/%Y"),
          "url_show"=>cli_medicament_assortment_path(assortment),
          "url_edit"=>edit_cli_medicament_assortment_path(assortment),
          "url_destroy"=>cli_medicament_assortment_path(assortment,format: :json)
      }

    end
  end

  def medicament_assortments
    @medicament_assortment ||= fetch_medicament_assortments
  end

  def fetch_medicament_assortments
    medicament_assortments = MedicamentAssortment.joins(:hospital_admission => :patient).joins(:warehouse).order("#{sort_column} #{sort_direction}")
    medicament_assortments = medicament_assortments.order('created_at desc')
    medicament_assortments = medicament_assortments.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      medicament_assortments = medicament_assortments.where("concat(patients.name,' ',patients.last_name) like :search or warehouses.name like :search", search: "%#{params[:searchPhrase]}%")
    end
    medicament_assortments
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end