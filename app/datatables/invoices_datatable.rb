class InvoicesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,doctor_id,is_open)
    @view = view
    @doctor_id = doctor_id
    @is_open = is_open
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: invoices.total_entries,
        rows: data,
        total: invoices.total_entries
    }
  end

  private


  def data
    serie = ""
    patient_name = ""
    invoices.map do |invoice|
      total = invoice.subtotal - invoice.isr + invoice.iva
      if invoice.cancel != nil && invoice.cancel != 0
        status = "Cancelada"
      else
        status = "Facturada"
      end
      if invoice.invoice_type == 1
        if invoice.cabinet_reports.count > 0
          invoice_type = "Estudio de Gabinete"
        else
          invoice_type = "Consulta Médica"
        end
        if invoice.patient_fiscal_information!=nil
          patient_name = invoice.patient_fiscal_information.business_name.gsub(',,', ' ')
        else
          patient_name = ""
        end
      elsif invoice.invoice_type == 2
        invoice_type = "Público General"
        patient_name = "Público General"
      else
        invoice_type = "Factura Abierta"
        patient_name = invoice.open_invoice_receptor.business_name
      end
      if @doctor_id != nil
        doctor_id_invoice = @doctor_id
      else
        doctor_id_invoice = params[:doctor_id]
      end
      {
          "id" => invoice.id,
          "serie" => invoice.serie + "-" + invoice.folio.to_s,
          "amount" =>  number_to_currency(total, :unit => "$"),
          "status" => status,
          "patient" => patient_name,
          "type" => invoice_type,
          "folio" => invoice.folio,
          "date" => invoice.created_at.strftime('%d/%m/%Y'),
          "cancel"=>invoice.cancel,
          "doctor_id" =>doctor_id_invoice,
          "print"=>print_pdf_doctor_invoices_path(invoice, format: :pdf),
          "download_xml"=>invoice_download_xml_doctor_invoices_path(invoice, format: :xml)
      }

    end
  end

  def invoices
    @specialty ||= fetch_invoices
  end

  def fetch_invoices
    invoices = Invoice.joins("LEFT JOIN open_invoice_receptors ON open_invoice_receptors.invoice_id = invoices.id").order("#{sort_column} #{sort_direction}",created_at: :desc)
    invoices = invoices.page(page).per_page(per_page)
    if @doctor_id != nil
      invoices = invoices.all.where("invoices.doctor_id = #{@doctor_id}")
    else
      invoices = invoices.all.where("invoices.doctor_id = #{params[:doctor_id]}")
    end
      if params[:bussines_name].to_s != "all" && params[:bussines_name].to_s != ""
        invoices = invoices.where("open_invoice_receptors.business_name like  '#{params[:bussines_name].to_s}'")
      end
      if params[:invoice_status].to_s == "0"
        invoices = invoices.where("cancel is null")
      elsif params[:invoice_status].to_s == "1"
        invoices = invoices.where("cancel = true")
      end
      if params[:start_date].to_s != "all"
        invoices = invoices.where("invoices.created_at >= '#{params[:start_date].to_s}'")
      end
      if params[:end_date].to_s != "all"
        invoices = invoices.where("invoices.created_at < '#{params[:end_date].to_s}'")
      end
    if @is_open
      invoices = invoices.where("invoice_type = 3")
    end
    invoices
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
