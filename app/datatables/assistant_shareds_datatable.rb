class AssistantSharedsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: assistants.total_entries,
        rows: data,
        total: assistants.total_entries,
    }
  end

  private

  def data
    assistants.map do |assistant|
      {
          "id" => assistant.id,
          "name" => assistant.name,
          "last_name" => assistant.last_name,
          "telephone" => assistant.telephone,
          "cellphone" => assistant.cellphone,
          "url_show"=>admin_assistant_path(assistant),
          "url_edit"=>edit_admin_assistant_path(assistant),
          "url_destroy"=>admin_assistant_path(assistant,format: :json)
      }

    end
  end

  def assistants
    @assistant ||= fetch_assistants
  end

  def fetch_assistants
    doctor_query = "shared = true"
    assistants = Assistant.where(doctor_query).order("#{sort_column} #{sort_direction}")
    assistants = assistants.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      assistants = assistants.where("id like :search or name like :search or last_name like :search or telephone like :search or cellphone like :search or email like :search", search: "%#{params[:searchPhrase]}%")
    else
      assistants = assistants
    end
    assistants
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    params[:sort] == 'desc' ? 'desc' : 'asc'
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columns = {:id => 'id', :name=>'name', :last_name=> 'last_name', :telephone=> 'telephone', :cellphone=>'cellphone', :email => 'email'}
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #columna = "#{key}"
        return columns[:"#{key}"]
      end
    end
  end
end
