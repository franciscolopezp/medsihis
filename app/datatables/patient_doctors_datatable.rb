class PatientDoctorsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: doctors.count,
        rows: data,
        total: doctors.count,
    }
  end

  private
  def data
    doctors.map do |doctor|
      specialties_str = ''
      offices_str = ''

      doctor.specialties.each do |s|
        specialties_str << s.name + '<br>'
      end

      doctor.offices.active_offices.each do |o|
        offices_str << '<button class="btn btn-success btn-icon waves-effect waves-circle waves-float" onclick="selectDoctor('+doctor.id.to_s+','+o.id.to_s+',\''+doctor.full_name+'\',\''+o.name+'\','+doctor.gender.id.to_s+')"><i class="zmdi zmdi-calendar-check zmdi-hc-fw"></i></button> '
        offices_str << o.name + ' ('+o.hospital.name+')<br>'
      end

      {
          "id" => doctor.id,
          "name" => doctor.full_name,
          "city" => doctor.city.format_city_state_and_country,
          "specialty" => specialties_str,
          "office" => offices_str
      }
    end
  end

  def doctors
    @doctor ||= fetch_doctors
  end

  def fetch_doctors
    doctors = Doctor.joins(:specialties).distinct.order("#{sort_column} #{sort_direction}")
    doctors = doctors.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      doctors = doctors.where('CONCAT(doctors.name, " ", doctors.last_name) like :search OR specialties.name like :search', search: "%#{params[:searchPhrase]}%")
    end

    if params[:city_id].present?
      doctors = doctors.where('doctors.city_id = ?',params[:city_id])
    end

    if params[:specialty_id].present?
      doctors = doctors.where('specialties.id = ?',params[:specialty_id])
    end

    doctors
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    if params[:sort] != nil
      params[:sort].each do |key, v|
        return v
      end
    end
    return 'asc'
  end

  def sort_column
    columns = {
        :name=>'doctors.name',
    }
    if params[:sort] != nil
      params[:sort].each do |key, array|
        return columns[:"#{key}"]
      end
    end
    return columns[:name]
  end
end
