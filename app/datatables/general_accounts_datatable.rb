class GeneralAccountsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: general_accounts.total_entries,
        rows: data,
        total: GeneralAccount.count,
    }
  end

  private

  def data
    general_accounts.map do |info|
      {
          "id" => info.id,
          "account_number" => info.account_number,
          "clabe" => info.clabe,
          "details" => info.details,
          "balance" => number_to_currency(info.balance,:unit => "$"),
          "bank" => info.bank.name,
          "currency" => info.currency.name,
          "url_show"=>admin_general_account_path(info),
          "url_edit"=>edit_admin_general_account_path(info),
          "url_destroy"=>admin_general_account_path(info,format: :json)

      }

    end
  end

  def general_accounts
    @general_account ||= fetch_general_accounts
  end

  def fetch_general_accounts
    general_accounts = GeneralAccount.joins(:bank).joins(:currency).order("#{sort_column} #{sort_direction}")
    general_accounts = general_accounts.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      general_accounts = general_accounts.where("banks.name like :search or account_number like :search or clabe like :search or details like :search", search: "%#{params[:searchPhrase]}%")
    end
    general_accounts
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
