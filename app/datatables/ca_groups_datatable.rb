class CaGroupsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,lab_id,doctor_id)
    @view = view
    @lab_id = lab_id
    @doctor_id = doctor_id
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: groups.total_entries,
        rows: data,
        total: CaGroup.where('laboratory_id = ?',@lab_id).count,
    }
  end

  private

  def data
    groups.map do |group|
      {
          "id" => group.id,
          "name" => group.name,
          "url_show"=>@doctor_id.present? ? doctor_ca_group_path(group) : admin_ca_group_path(group),
          "url_edit"=>@doctor_id.present? ? edit_doctor_ca_group_path(group) : edit_admin_ca_group_path(group),
          "url_destroy"=>@doctor_id.present? ? doctor_ca_group_path(group,format: :json) : admin_ca_group_path(group,format: :json)

      }

    end
  end

  def groups
    @group ||= fetch_groups
  end

  def fetch_groups
    groups = CaGroup.where('laboratory_id = ?',@lab_id).order("#{sort_column} #{sort_direction}")
    groups = groups.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      groups = groups.where("id like :search or name like :search", search: "%#{params[:searchPhrase]}%")
    end
    groups
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
