class OfficesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,doctor_id)
    @view = view
    @doctor = doctor_id
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: offices.total_entries,
        rows: data,
        total: offices.total_entries,
    }
  end

  private

  def data
    offices.map do |office|
       if office.template_prescription != nil
         template_id = office.template_prescription.id
       else
         template_id = 0
       end

      {
          "id" => office.id,
          "name" => office.name,
          "address" => office.address,
          "telephone" => office.telephone,
          "template_id" => template_id,
          "time" => office.consult_duration + " minutos",
          "url_show"=>doctor_office_path(office),
          "url_edit"=>edit_doctor_office_path(office),
          "url_destroy"=>doctor_office_path(office,format: :json)
      }

    end
  end
1
  def offices
    @office ||= fetch_offices
  end

  def fetch_offices
    offices = Office.active_offices.order("#{sort_column} #{sort_direction}")
    offices = offices.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      offices = offices.where("id like :search or name like :search or address like :search or telephone like :search", search: "%#{params[:searchPhrase]}%")
    else
      offices = offices
    end
    offices
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
