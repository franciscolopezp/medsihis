class RolesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to,  to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: roles.total_entries,
        rows: data,
        total: Role.count,
    }
  end

  private

  def data
    roles.map do |role|
      {
          "id" => role.id,
          "name" => role.name,
          "url_show"=>admin_role_path(role),
          "url_edit"=>edit_admin_role_path(role),
          "url_destroy"=>admin_role_path(role,format: :json),
          "url_permission"=>permission_admin_roles_path(role)

      }

    end
  end

  def roles
    @specialty ||= fetch_roles
  end

  def fetch_roles
    roles = Role.order("#{sort_column} #{sort_direction}")
    roles = roles.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      roles = roles.where("id like :search or name like :search", search: "%#{params[:searchPhrase]}%")
    end
    roles
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end