class AdmissionInvoicesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: admission_invoices.total_entries,
        rows: data,
        total: admission_invoices.total_entries,
    }
  end

  private

  def data
    admission_invoices.map do |invoice|
      {
          "id" => invoice.id,
          "patient" => invoice.hospital_admission.present? ? invoice.hospital_admission.patient.full_name : "",
          "date" => invoice.created_at.strftime("%d/%m/%Y"),
          "folio" => invoice.serie + "-"+invoice.folio.to_s,
          "is_cancel" => invoice.cancel,
          "rfc_emisor" => invoice.admission_invoice_emisor.rfc,
          "rfc_receptor" => invoice.admission_invoice_receptor.rfc,
          "business_name" => invoice.admission_invoice_receptor.business_name,
          "url_print_pdf"=>print_admission_invoice_pdf_cli_admission_invoices_path(invoice.id, format: :pdf),
          "url_print_xml"=>download_admission_invoice_xml_cli_admission_invoices_path(invoice.id, format: :xml)
      }

    end
  end

  def admission_invoices
    @fixed_asset_category ||= fetch_admission_invoices
  end

  def fetch_admission_invoices
    admission_invoices = AdmissionInvoice.joins(:admission_invoice_emisor).joins(:admission_invoice_receptor).order("#{sort_column} #{sort_direction}")
    admission_invoices = admission_invoices.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      admission_invoices = admission_invoices.where("admission_invoice_emisors.rfc like :search or admission_invoice_receptors.rfc like :search or admission_invoice_receptors.business_name like :search or concat(admission_invoices.serie,'-',admission_invoices.folio) like :search", search: "%#{params[:searchPhrase]}%")
    end
    admission_invoices
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end

end