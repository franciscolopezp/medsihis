class VitalSignsDatatable
  include DatatableHelper
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,patient_id)
    @view = view
    @patient_id = patient_id
  end


  private
  def data
    fetch_data.map do |pv|
      {
        "id" => pv.pv_id,
        "date" => pv.created_at.strftime("%d/%m/%Y"),
        "weight" => pv.peso,
        "height" => pv.talla,
        "temp" => pv.temperatura,
        "ta" => pv.ta_sistolica.to_i.to_s + "/" + pv.ta_diastolica.to_i.to_s,

      }
    end
  end

  def fetch_records
    vital_signs = ViewPatientValoration.where("patient_id = #{@patient_id}")
    vital_signs = filter(vital_signs)
    vital_signs
  end
end
