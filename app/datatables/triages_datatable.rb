class TriagesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: triages.total_entries,
        rows: data,
        total: triages.total_entries,
    }
  end

  private

  def data
    triages.map do |triage|
      {
          "id" => triage.id,
          "level" => triage.level,
          "attention_time" => triage.attention_time,
          "description" => triage.description,
          "color" => "<div style='background: #"+triage.color+";width: 40px;height: 20px'></div>",
          "url_show"=>admin_triage_path(triage),
          "url_edit"=>edit_admin_triage_path(triage),
          "url_destroy"=>admin_triage_path(triage,format: :json)
      }

    end
  end

  def triages
    @triage ||= fetch_triages
  end

  def fetch_triages
    triages = Triage.order("#{sort_column} #{sort_direction}")
    triages = triages.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      triages = triages.where("id like :search or level like :search or description like :search", search: "%#{params[:searchPhrase]}%")
    end
    triages
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end