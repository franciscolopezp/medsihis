class ClinicalStudyTypesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, lab_id,doctor_id)
    @view = view
    @lab_id = lab_id
    @doctor_id = doctor_id
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: clinical_study_types.total_entries,
        rows: data,
        total: ClinicalStudyType.where('laboratory_id = '+@lab_id.to_s).count,
    }
  end

  private

  def data
    clinical_study_types.map do |clinical_study_type|
      {
          "id" => clinical_study_type.id,
          "name" => clinical_study_type.name,
          "url_show"=>@doctor_id.present? ? doctor_clinical_study_type_path(clinical_study_type) : admin_clinical_study_type_path(clinical_study_type),
          "url_edit"=>@doctor_id.present? ? edit_doctor_clinical_study_type_path(clinical_study_type) : edit_admin_clinical_study_type_path(clinical_study_type),
          "url_destroy"=>@doctor_id.present? ? doctor_clinical_study_type_path(clinical_study_type,format: :json) : admin_clinical_study_type_path(clinical_study_type,format: :json)
      }

    end
  end

  def clinical_study_types
    @clinical_study_type ||= fetch_clinical_study_types
  end

  def fetch_clinical_study_types
    clinical_study_types = ClinicalStudyType.where('laboratory_id = '+@lab_id.to_s).order("#{sort_column} #{sort_direction}")
    clinical_study_types = clinical_study_types.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      clinical_study_types = clinical_study_types.where("id like :search or name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    clinical_study_types
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end
