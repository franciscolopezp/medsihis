class FixedAssetActivitiesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: fixed_asset_activities.total_entries,
        rows: data,
        total: FixedAssetActivity.count,
    }
  end

  private

  def data
    fixed_asset_activities.map do |fixed_asset|
      {
          "id" => fixed_asset.id,
          "name" => fixed_asset.name,
          "color" => "<div style='background: "+fixed_asset.color+";width: 40px;height: 20px'></div>",
          "fixed_asset" => fixed_asset.fixed_asset.name,
          "url_show"=>admin_fixed_asset_activities_path(fixed_asset),
          "url_edit"=>edit_admin_fixed_asset_activity_path(fixed_asset),
          "url_destroy"=>admin_fixed_asset_activity_path(fixed_asset,format: :json)
      }

    end
  end

  def fixed_asset_activities
    @fixed_asset_activities ||= fetch_fixed_asset_activities
  end

  def fetch_fixed_asset_activities
    fixed_asset_activities = FixedAssetActivity.joins(:fixed_asset).order("#{sort_column} #{sort_direction}")
    fixed_asset_activities = fixed_asset_activities.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      fixed_asset_activities = fixed_asset_activities.where("fixed_assets.name like :search or fixed_asset_activities.name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    fixed_asset_activities
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end