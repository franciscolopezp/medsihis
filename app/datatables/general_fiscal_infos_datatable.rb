class GeneralFiscalInfosDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: general_fiscal_infos.total_entries,
        rows: data,
        total: general_fiscal_infos.total_entries
    }
  end

  private

  def data
      general_fiscal_infos.map do |info|
        {
            "id" => info.id,
            "business_name" => info.business_name,
            "rfc" => info.rfc,
            "city" => info.city.name + "," + info.city.state.name + "," + info.city.state.country.name,
            "zip" => info.zip,
            "url_show"=>doctor_general_fiscal_info_path(info),
            "url_edit"=>edit_doctor_general_fiscal_info_path(info),
            "url_destroy"=>doctor_general_fiscal_info_path(info,format: :json)
        }
      end


  end

  def general_fiscal_infos
    @info ||= fetch_fiscal_infos
  end

  def fetch_fiscal_infos
      fiscal_infos = GeneralFiscalInfo.joins(:city).order("#{sort_column} #{sort_direction}")
      fiscal_infos = fiscal_infos.page(page).per_page(per_page)
      if params[:searchPhrase].present?
        fiscal_infos = fiscal_infos.joins(:city).where("zip like :search or cities.name like :search or business_name like :search or rfc like :search or address like :search or suburb like :search", search: "%#{params[:searchPhrase]}%")
      end

    fiscal_infos
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columns = {:id=>'id', :name=>'name', :last_name=> 'lastname', :email=>'email', :city => 'cities.name', :doctor=> 'doctors.name'}
    #columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #columna = "#{key}"
        return columns[:"#{key}"]
      end
    end
  end
end
