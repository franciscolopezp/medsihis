class IndicationsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: clinical_studies.total_entries,
        rows: data,
        total: count_indication,
    }
  end

  private

  def count_indication
    if params[:clinical_study].to_s == "all"
      clinical_studies = Indication.order("#{sort_column} #{sort_direction}")
    else
      clinical_studies = Indication.joins(:clinical_studies).where("clinical_studies_indications.clinical_study_id": params[:clinical_study].to_i).order("#{sort_column} #{sort_direction}")
    end
    clinical_studies.count
  end

  def data
    clinical_studies.map do |clinical_studie|
      {
          "id" => clinical_studie.id,
          "description" => clinical_studie.description,
          "priority" => clinical_studie.priority,
          "grupo" => clinical_studie.ca_group.name,
          "url_show"=>admin_indication_path(clinical_studie),
          "url_edit"=>edit_admin_indication_path(clinical_studie),
          "url_destroy"=>admin_indication_path(clinical_studie,format: :json)
      }

    end
  end

  def clinical_studies
    @personal ||= fetch_clinical_studies
  end

  def fetch_clinical_studies
    if params[:clinical_study].to_s == "all"
      clinical_studies = Indication.order("#{sort_column} #{sort_direction}")
    else
      clinical_studies = Indication.joins(:clinical_studies).where("clinical_studies_indications.clinical_study_id": params[:clinical_study].to_i).order("#{sort_column} #{sort_direction}")
    end

    clinical_studies = clinical_studies.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      clinical_studies = clinical_studies.joins(:ca_group).where("indications.id like :search or indications.description like :search or priority like :search or ca_groups.name like :search", search: "%#{params[:searchPhrase]}%")
    end
    clinical_studies
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
