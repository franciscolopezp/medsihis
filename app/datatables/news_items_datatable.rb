class NewsItemsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: news_items.total_entries,
        rows: data,
        total: NewsItem.count,
    }
  end

  private

  def data
    news_items.map do |news_item|
      {
          "id" => news_item.id,
          "title" => news_item.title,
          "summary" => news_item.summary,
          "author" => news_item.author.full_name,
          "status" => NewsItem.get_status_name(news_item.status),
          "url_show"=>admin_news_item_path(news_item),
          "url_edit"=>edit_admin_news_item_path(news_item),
          "url_destroy"=>admin_news_item_path(news_item, format: :json),
      }

    end
  end

  def news_items
    @news_item ||= fetch_news_items
  end

  def fetch_news_items
    news_items = NewsItem.order("#{sort_column} #{sort_direction}")
    news_items = news_items.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      news_items = news_items.where("title like :search or summary like :search or content like :search author like :search", search: "%#{params[:searchPhrase]}%")
    end
    news_items
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
