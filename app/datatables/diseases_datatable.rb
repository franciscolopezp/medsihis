class DiseasesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: diseases.total_entries,
        rows: data,
        total: Disease.count,
    }
  end

  private

  def data
    diseases.map do |disease|
      {
          "id" => disease.id,
          "category" => disease.disease_category.name,
          "code" => disease.code,
          "name" => disease.name,
          "url_show"=>admin_disease_path(disease),
          "url_edit"=>edit_admin_disease_path(disease),
          "url_destroy"=>admin_disease_path(disease,format: :json)

      }

    end
  end

  def diseases
    @disease ||= fetch_diseases_diseases
  end

  def fetch_diseases_diseases
    diseases = Disease.order("#{sort_column} #{sort_direction}")
    diseases = diseases.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      diseases = diseases.joins(:disease_category).where("diseases.id like :search or diseases.name like :search or diseases.code like :search or disease_categories.code like :search or disease_categories.name like :search", search: "%#{params[:searchPhrase]}%")
    end
    diseases
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
