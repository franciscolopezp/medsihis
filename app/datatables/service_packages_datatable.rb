class ServicePackagesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: service_packages.total_entries,
        rows: data,
        total: ServicePackage.count,
    }
  end

  private

  def data
    service_packages.map do |service_package|
      {
          "id" => service_package.id,
          "name" => service_package.name,
          "url_show"=>cli_service_package_path(service_package),
          "url_edit"=>edit_cli_service_package_path(service_package),
          "url_manage"=>manage_services_cli_service_packages_path(service_package),
          "url_destroy"=>cli_service_package_path(service_package,format: :json)
      }

    end
  end

  def service_packages
    @service_packages ||= fetch_service_packages
  end

  def fetch_service_packages
    service_packages = ServicePackage.order("#{sort_column} #{sort_direction}")
    service_packages = service_packages.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      service_packages = service_packages.where("name like :search", search: "%#{params[:searchPhrase]}%")
    end
    service_packages
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end

end