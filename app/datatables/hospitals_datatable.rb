class HospitalsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: hospitals.total_entries,
        rows: data,
        total: Hospital.count,
    }
  end

  private

  def data
    hospitals.map do |hospital|
      {
          "id" => hospital.id,
          "name" => hospital.name,
          "description" => hospital.description,
          "city" => hospital.city.format_city_state_and_country,
          "url_show"=>admin_hospital_path(hospital),
          "url_edit"=>edit_admin_hospital_path(hospital),
          "url_destroy"=>admin_hospital_path(hospital,format: :json)
      }

    end
  end

  def hospitals
    @hospital ||= fetch_hospitals
  end

  def fetch_hospitals
    hospitals = Hospital.order("#{sort_column} #{sort_direction}")
    hospitals = hospitals.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      hospitals = hospitals.where("id like :search or name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    hospitals
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end
