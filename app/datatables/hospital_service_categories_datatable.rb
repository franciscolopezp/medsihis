class HospitalServiceCategoriesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: hospital_service_categories.total_entries,
        rows: data,
        total: hospital_service_categories.total_entries,
    }
  end

  private

  def data
    hospital_service_categories.map do |category|
      {
          "id" => category.id,
          "name" => category.name,
          "url_show"=>cli_hospital_service_category_path(category),
          "url_edit"=>edit_cli_hospital_service_category_path(category),
          "url_destroy"=>cli_hospital_service_category_path(category,format: :json)
      }

    end
  end

  def hospital_service_categories
    @hospital_service_category ||= fetch_hospital_service_categories
  end

  def fetch_hospital_service_categories
    hospital_service_categories = HospitalServiceCategory.order("#{sort_column} #{sort_direction}")
    hospital_service_categories = hospital_service_categories.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      hospital_service_categories = hospital_service_categories.where("name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    hospital_service_categories
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end

end