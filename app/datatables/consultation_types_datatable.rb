class ConsultationTypesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: general_consultation_types.total_entries,
        rows: data,
        total: ConsultationType.count,
    }
  end

  private

  def data
    general_consultation_types.map do |info|
      {
          "id" => info.id,
          "name" => info.name,
          "is_editable_price" => info.consultation_costs.first.is_editable_price,
          "just_cost" => info.consultation_costs.first.cost,
          "cost" => number_to_currency(info.consultation_costs.first.cost,:unit=>"$"),
          "url_destroy"=>doctor_consultation_type_path(info,format: :json)

      }

    end
  end

  def general_consultation_types
    @general_account ||= fetch_general_consultation_types
  end

  def fetch_general_consultation_types

    general_consultation_types = ConsultationType.joins(:consultation_costs).order("#{sort_column} #{sort_direction}")
    general_consultation_types = general_consultation_types.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      general_consultation_types = general_consultation_types.where("consultation_types.name like :search or consultation_costs.cost like :search", search: "%#{params[:searchPhrase]}%")
    end
    general_consultation_types
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end