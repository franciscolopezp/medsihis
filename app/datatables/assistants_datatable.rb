class AssistantsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,doctor_id)
    @view = view
    @doctor_id = doctor_id
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: assistants.total_entries,
        rows: data,
        total: assistants.total_entries,
    }
  end

  private

  def data
    assistants.map do |assistant|
      {
          "id" => assistant.id,
          "name" => assistant.name,
          "last_name" => assistant.last_name,
          "telephone" => assistant.telephone,
          "cellphone" => assistant.cellphone,
          "shared" => assistant.shared,
          "url_show"=>doctor_assistant_path(assistant),
          "url_edit"=>edit_doctor_assistant_path(assistant),
          "url_destroy"=>doctor_assistant_path(assistant,format: :json)
      }

    end
  end

  def assistants
    @assistant ||= fetch_assistants
  end

  def fetch_assistants
    assistants = Assistant.joins("JOIN assistants_doctors ON assistants_doctors.assistant_id = assistants.id").where("assistants_doctors.doctor_id = #{@doctor_id}").order("#{sort_column} #{sort_direction}")
    assistants = assistants.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      assistants = assistants.where("id like :search or name like :search or last_name like :search or telephone like :search or cellphone like :search or email like :search", search: "%#{params[:searchPhrase]}%")
    else
      assistants = assistants
    end
    assistants
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    params[:sort] == 'desc' ? 'desc' : 'asc'
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columns = {:id => 'id', :name=>'name', :last_name=> 'last_name', :telephone=> 'telephone', :cellphone=>'cellphone', :email => 'email'}
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #columna = "#{key}"
        return columns[:"#{key}"]
      end
    end
  end
end
