class ClinicalStudiesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,lab_id,doctor_id)
    @view = view
    @lab_id = lab_id
    @doctor_id = doctor_id
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: clinical_studies.total_entries,
        rows: data,
        total: count_clinical_study,
    }
  end

  private

  def count_clinical_study
    ClinicalStudy.joins(:clinical_study_type).where(:clinical_study_types => {laboratory_id:@lab_id}).count
  end

  def data
    clinical_studies.map do |clinical_study|
      {
          "id" => clinical_study.id,
          "name" => clinical_study.name,
          "category" => clinical_study.clinical_study_type.name,
          "url_show"=>@doctor_id.present? ? doctor_clinical_study_path(clinical_study) : admin_clinical_study_path(clinical_study),
          "url_edit"=>@doctor_id.present? ? edit_doctor_clinical_study_path(clinical_study) : edit_admin_clinical_study_path(clinical_study),
          "url_destroy"=>@doctor_id.present? ? doctor_clinical_study_path(clinical_study,format: :json) : admin_clinical_study_path(clinical_study,format: :json)

      }

    end
  end

  def clinical_studies
    @clinical_study ||= fetch_clinical_studies
  end

  def fetch_clinical_studies
    clinical_studies = ClinicalStudy.joins(:clinical_study_type).where(:clinical_study_types => {laboratory_id:@lab_id}).order("#{sort_column} #{sort_direction}")

    clinical_studies = clinical_studies.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      clinical_studies = clinical_studies.where("id like :search or name like :search", search: "%#{params[:searchPhrase]}%")
    end
    clinical_studies
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
