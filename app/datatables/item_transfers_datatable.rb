class ItemTransfersDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: item_transfers.total_entries,
        rows: data,
        total: Insurance.count,
    }
  end

  private

  def data
    item_transfers.map do |transfer|
      if transfer.status == 0
        state = "Pendiente"
      elsif transfer.status == 1
        state = "Aceptada"
      elsif transfer.status == 2
        state = "Rechazada"
      else
        state = "Cancelada"
      end
      {
          "id" => transfer.id,
          "from_warehouse" => transfer.from_warehouse.name,
          "state" => state,
          "to_warehouse" => transfer.to_warehouse.name,
          "date" => transfer.created_at.strftime("%d/%m/%Y"),
          "user" => transfer.user.person.full_name,
          "url_show"=>cli_item_transfer_path(transfer),
          "url_edit"=>edit_cli_item_transfer_path(transfer),
          "url_destroy"=>cli_item_transfer_path(transfer,format: :json)
      }

    end
  end

  def item_transfers
    @insurance ||= fetch_item_transfers
  end

  def fetch_item_transfers
    item_transfers = ItemTransfer.order("#{sort_column} #{sort_direction}")
    item_transfers = item_transfers.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      item_transfers = item_transfers.where("id like :search", search: "%#{params[:searchPhrase]}%")
    end
    item_transfers
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end
