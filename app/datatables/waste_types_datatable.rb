class WasteTypesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: waste_types.total_entries,
        rows: data,
        total: waste_types.total_entries,
    }
  end

  private

  def data
    waste_types.map do |waste|
      if(waste.is_entry)
        waste_type = "ENTRADA"
      else
        waste_type = "SALIDA"
      end
      {
          "id" => waste.id,
          "name" => waste.name,
          "waste_type" => waste_type,
          "url_show"=>cli_waste_type_path(waste),
          "url_edit"=>edit_cli_waste_type_path(waste),
          "url_destroy"=>cli_waste_type_path(waste,format: :json)
      }

    end
  end

  def waste_types
    @waste_type ||= fetch_waste_types
  end

  def fetch_waste_types
    waste_types = WasteType.order("#{sort_column} #{sort_direction}")
    waste_types = waste_types.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      waste_types = waste_types.where("name like :search", search: "%#{params[:searchPhrase]}%")
    end
    waste_types
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end

end