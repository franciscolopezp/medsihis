class InsurancesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: insurances.total_entries,
        rows: data,
        total: Insurance.count,
    }
  end

  private

  def data
    insurances.map do |insurance|
      if insurance.is_active
        state = "Activa"
      else
        state = "Desactivada"
      end
      {
          "id" => insurance.id,
          "name" => insurance.name,
          "state" => state,
          "telephone" => insurance.telephone,
          "address" => insurance.address,
          "contact" => insurance.contact,
          "url_show"=>cli_insurance_path(insurance),
          "url_edit"=>edit_cli_insurance_path(insurance),
          "url_destroy"=>cli_insurance_path(insurance,format: :json)
      }

    end
  end

  def insurances
    @insurance ||= fetch_insurances
  end

  def fetch_insurances
    insurances = Insurance.where("id != 1").order("#{sort_column} #{sort_direction}")
    insurances = insurances.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      insurances = insurances.where("telephone like :search or name like :search or address lile :search or contact like :search", search: "%#{params[:searchPhrase]}%")
    end
    insurances
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end

end