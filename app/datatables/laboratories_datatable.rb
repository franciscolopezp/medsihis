class LaboratoriesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,doctor_id)
    @view = view
    @doctor_id = doctor_id
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: laboratories.total_entries,
        rows: data,
        total: laboratories_count
    }
  end

  private

  def data
    laboratories.map do |laboratory|
      url = get_urls laboratory
      {
          "id" => laboratory.id,
          "name" => laboratory.name,
          "address" => laboratory.address,
          "telephone" => laboratory.telephone,
          "email" =>laboratory.email,
          "custom" =>laboratory.is_custom,
          "city" => laboratory.city.nil? ? '' : laboratory.city.format_city_state_and_country,
          "url_show"=>url['url_show'],
          "url_edit"=>url['url_edit'],
          "url_destroy"=>url['url_destroy'],
          "url_study_categories"=>url['url_study_categories'],
          "url_ca_groups"=>url['url_ca_groups'],
          "url_indications"=>url['url_indications'],
          "url_studies"=>url['url_studies'],
          "url_packages"=>url['url_packages']
      }

    end
  end

  def laboratories
    @laboratory ||= fetch_laboratories
  end

  def laboratories_count
    if @doctor_id.present?#se solicita desde UI de Doctor
      Laboratory.joins(:doctors).where(:doctors => {:id => @doctor_id}).count
    else
      Laboratory.count
    end
  end

  def fetch_laboratories
    if @doctor_id.present?#se solicita desde UI de Doctor
      laboratories = Laboratory.joins(:doctors).where(:doctors => {:id => @doctor_id}).order("#{sort_column} #{sort_direction}")
    else
      laboratories = Laboratory.order("#{sort_column} #{sort_direction}")
    end

    laboratories = laboratories.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      laboratories = laboratories.joins(:city).where("laboratories.id like :search or laboratories.name like :search or address like :search or email like :search or telephone like :search or cities.name like :search", search: "%#{params[:searchPhrase]}%")
    end
    laboratories
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end

  def get_urls laboratory
    data = Hash.new
    data['url_show'] = @doctor_id.present? ? doctor_laboratory_path(laboratory) : admin_laboratory_path(laboratory)
    data['url_edit'] = @doctor_id.present? ? edit_doctor_laboratory_path(laboratory) : edit_admin_laboratory_path(laboratory)
    data['url_destroy'] = @doctor_id.present? ? doctor_laboratory_path(laboratory,format: :json) : admin_laboratory_path(laboratory,format: :json)
    data['url_study_categories'] = @doctor_id.present? ? doctor_clinical_study_types_path+ "?lab_id="+laboratory.id.to_s : admin_clinical_study_types_path+ "?lab_id="+laboratory.id.to_s
    data['url_ca_groups'] = @doctor_id.present? ? doctor_ca_groups_path + "?lab_id="+laboratory.id.to_s : admin_ca_groups_path + "?lab_id="+laboratory.id.to_s
    data['url_indications'] = @doctor_id.present? ? doctor_indications_path + "?lab_id="+laboratory.id.to_s : admin_indications_path + "?lab_id="+laboratory.id.to_s
    data['url_studies'] = @doctor_id.present? ? doctor_clinical_studies_path + "?lab_id="+laboratory.id.to_s : admin_clinical_studies_path + "?lab_id="+laboratory.id.to_s
    data['url_packages'] = @doctor_id.present? ? doctor_study_packages_path + "?lab_id="+laboratory.id.to_s : admin_study_packages_path  + "?lab_id="+laboratory.id.to_s

    data
  end
end
