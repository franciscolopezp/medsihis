class ClinicalAnalysisOrderDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, current_user, expedient = nil)
    @view = view
    @current_user = current_user
    @expedient = expedient
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: clinical_analysis_order.count,
        rows: data,
        total: clinical_analysis_order.count,
    }
  end

  private

  def data
    clinical_analysis_order.map do |analysis|
      {
          "id" => analysis.id,
          "folio" => analysis.folio,
          "patient_name" => analysis.medical_expedient.patient.full_name,
          "date_analysis" => analysis.date_order,
          "laboratory" => analysis.laboratory.nil? ? '' : analysis.laboratory.name,
          "has_report" => analysis.cabinet_report.nil? ? false : true,
          "user_id" => analysis.user.id,
          "is_charged" => analysis.cabinet_report.nil? ? true : analysis.cabinet_report.is_charged,
          "report_id" => analysis.cabinet_report.nil? ? 0 : analysis.cabinet_report.id,
          "price" => analysis.cabinet_report.nil? ? 0 : analysis.cabinet_report.price,
          "debt" => analysis.cabinet_report.nil? ? 0 : analysis.cabinet_report.get_balance,
          "annotations" => (analysis.annotations.length > 200 )? analysis.annotations[0..200].to_s+"..." : analysis.annotations,
          "url_show_analysis"=>doctor_analysis_order_path(analysis),
          "clinical_order_type"=>analysis.clinical_order_type.name,
          "clinical_order_type_id"=>analysis.clinical_order_type.id
      }
    end
  end

  def clinical_analysis_order
    @analysis_order ||= fetch_clinical_analysis_order
  end

  def fetch_clinical_analysis_order
    if !@expedient.nil?
      #patient = MedicalExpedient.find()
      analysis = ClinicalAnalysisOrder.joins(:user => :person).joins(:medical_expedient => :patient).where(:patients => {:id => @expedient.patient.id}).order("#{sort_column} #{sort_direction}")
      if params[:searchPhrase].present?
        analysis = ClinicalAnalysisOrder.joins(:user => :person).joins(:medical_expedient => :patient).where(:patients => {:id => @expedient.patient.id}).where("DATE_FORMAT(date_order,'%d/%m/%Y') like :search or CONCAT_WS(' ',patients.name,patients.last_name) like :search or clinical_analysis_orders.folio like :search or people.name like :search or people.last_name like :search", search: "%#{params[:searchPhrase]}%")
      end
    else
      analysis = ClinicalAnalysisOrder.joins(:user => :person).joins(:medical_expedient => :patient).order("#{sort_column} #{sort_direction}")
      if params[:searchPhrase].present?
        analysis = ClinicalAnalysisOrder.joins(:user => :person).joins(:medical_expedient => :patient).where("DATE_FORMAT(date_order,'%d/%m/%Y') like :search or CONCAT_WS(' ',patients.name,patients.last_name) like :search or clinical_analysis_orders.folio like :search or people.name like :search or people.last_name like :search", search: "%#{params[:searchPhrase]}%")
      end
    end

    if params[:filter_clinical_order_type].present?
     analysis = analysis.where(:clinical_order_type_id => params[:filter_clinical_order_type].to_i )
    end
    analysis = analysis.page(page).per_page(per_page)
    analysis
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
