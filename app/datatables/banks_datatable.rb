class BanksDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: banks.total_entries,
        rows: data,
        total: Bank.count,
    }
  end

  private

  def data
    banks.map do |bank|
      {
          "id" => bank.id,
          "name" => bank.name,
          "is_local" => bank.is_local ? "Si" : "No",
          "url_show"=>admin_bank_path(bank),
          "url_edit"=>edit_admin_bank_path(bank),
          "url_destroy"=>admin_bank_path(bank,format: :json)
      }

    end
  end

  def banks
    @bank ||= fetch_banks
  end

  def fetch_banks
    banks = Bank.order("#{sort_column} #{sort_direction}")
    banks = banks.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      banks = banks.where("id like :search or name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    banks
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end
