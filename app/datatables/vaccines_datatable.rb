class VaccinesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: vaccines.total_entries,
        rows: data,
        total: Vaccine.count,
    }
  end

  private

  def data
    vaccines.map do |vaccine|
      {
          "id" => vaccine.id,
          "vaccine_book" => vaccine.vaccine_book.name,
          "name" => vaccine.name,
          "prevent_disease" => vaccine.prevent_disease,
          "url_show"=>admin_vaccine_path(vaccine),
          "url_edit"=>edit_admin_vaccine_path(vaccine),
          "url_destroy"=>admin_vaccine_path(vaccine,format: :json)
      }

    end
  end

  def vaccines
    @specialty ||= fetch_vaccines
  end

  def fetch_vaccines
    vaccines = Vaccine.order("#{sort_column} #{sort_direction}")
    vaccines = vaccines.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      vaccines = vaccines.where("id like :search or name like :search or prevent_disease like :search", search: "%#{params[:searchPhrase]}%")
    end
    vaccines
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
