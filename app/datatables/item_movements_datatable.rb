class ItemMovementsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: item_movements.total_entries,
        rows: data,
        total: item_movements.total_entries,
    }
  end

  private

  def data

      item_movements.map do |movement|
        {
            "id" => movement.id,
            "warehouse" => movement.warehouse.name,
            "cause" => movement.reason,
            "waste_type" => movement.waste_type.name,
            "date" => movement.created_at.strftime('%d/%m/%Y'),
            "url_show"=>cli_item_movement_path(movement),
            "url_edit"=>edit_cli_item_movement_path(movement),
            "url_destroy"=>cli_item_movement_path(movement,format: :json)
        }

      end
  end

  def item_movements
    @item_movement ||= fetch_item_movements
  end

  def fetch_item_movements
    item_movements = ItemMovement.joins(:warehouse).joins(:waste_type).order("#{sort_column} #{sort_direction}")
    item_movements = item_movements.page(page).per_page(per_page)
      if params[:searchPhrase].present?
        item_movements = item_movements.where("reason like :search or warehouses.name like :search or waste_types.name like :search", search: "%#{params[:searchPhrase]}%")
      end
    item_movements
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end