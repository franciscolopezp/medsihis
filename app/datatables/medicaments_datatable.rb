class MedicamentsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
end

def as_json(options = {})
  {
      current: params[:current].to_i,
      rowCount: medicaments.total_entries,
      rows: data,
      total: Medicament.count,
  }
end

private

def data
  medicaments.map do |medicament|
    active_susbtances = ""
    if medicament.active_substances.count > 0
      medicament.active_substances.each do |subs|
        active_susbtances += subs.name  + "|"
      end
      active_susbtances.chop!
    else
      active_susbtances = "Sin especificar"
    end
    {
        "id" => medicament.id,
        "comercial_name" => medicament.comercial_name,
        "category" => medicament.medicament_category.name,
        "presentation" => medicament.presentation,
        "active_substance" => active_susbtances,
        "contraindication" => medicament.contraindication,
        "brand" => medicament.medicament_laboratory.name,
        "url_show"=>admin_medicament_path(medicament),
        "url_edit"=>edit_admin_medicament_path(medicament),
        "url_destroy"=>admin_medicament_path(medicament,format: :json),
        "url_substance" => active_substance_admin_medicaments_path(medicament)
    }

  end
end

def medicaments
  @medicament ||= fetch_medicaments
end

def fetch_medicaments
  medicaments = Medicament.joins(:medicament_laboratory).joins(:medicament_category).joins(:active_substances).group('medicaments.id').order("#{sort_column} #{sort_direction}")
  medicaments = medicaments.page(page).per_page(per_page)
  if params[:searchPhrase].present?
    medicaments = medicaments.where("comercial_name like :search or presentation like :search or active_substances.name like :search or contraindication like :search or medicament_laboratories.name like :search", search: "%#{params[:searchPhrase]}%")
  end
  medicaments
end

def page
  params[:current].to_i
end

def per_page
  params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
end

def sort_direction
  orden = ""
  if params[:sort] != nil
    params[:sort].each do |key, array|
      #puts "#{key}"
      orden =  array
    end
    orden.to_s
  end
end

def sort_column
  columna = "desc"
  if params[:sort] != nil
    params[:sort].each do |key, array|
      columna = "#{key}"
      #columna =  array
    end
    columna.to_s
  end
end
end
