class AccountsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,current_user)
    @view = view
    @current_user = current_user
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: accounts.total_entries,
        rows: data,
        total: fetch_accounts.count,
    }
  end

  private

  def data
    accounts.map do |account|
      {
          "id" => account.id,
          "bank" => account.bank.name,
          "currency" => account.currency.nil? ? '' : account.currency.name,
          "account_number" => account.account_number,
          "clabe"=>account.clabe,
          "balance"=>@view.number_to_currency(account.balance),
          "details"=>account.details,
          "print_account_balance"=>!account.bank.is_local,
          "url_edit"=>edit_doctor_account_path(account),
          "url_destroy"=>doctor_account_path(account,format: :json)
      }

    end
  end

  def accounts
    @account ||= fetch_accounts
  end

  def fetch_accounts

      accounts = Account.order("#{sort_column} #{sort_direction}")


    accounts = accounts.page(page).per_page(per_page)
    #accounts = accounts.where("doctor_id = #{@doctor_id}")
    if params[:searchPhrase].present?
      accounts = accounts.where("id like :search or account_number like :search ", search: "%#{params[:searchPhrase]}%")
    #else
    #  accounts = accounts.where("doctor_id = #{@doctor_id}")
    end
    accounts
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end
