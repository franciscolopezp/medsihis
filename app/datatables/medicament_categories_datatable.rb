class MedicamentCategoriesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: medicament_categories.total_entries,
        rows: data,
        total: MedicamentCategory.count,
    }
  end

  private

  def data
    medicament_categories.map do |medicament_category|
      {
          "id" => medicament_category.id,
          "name" => medicament_category.name,
          "url_show"=>admin_medicament_categories_path(medicament_category),
          "url_edit"=>edit_admin_medicament_category_path(medicament_category),
          "url_destroy"=>admin_medicament_category_path(medicament_category,format: :json)
      }

    end
  end

  def medicament_categories
    @medicament_category ||= fetch_medicament_categories
  end

  def fetch_medicament_categories
    medicament_categories = MedicamentCategory.order("#{sort_column} #{sort_direction}")
    medicament_categories = medicament_categories.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      medicament_categories = medicament_categories.where("id like :search or name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    medicament_categories
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end