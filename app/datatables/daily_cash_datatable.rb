class DailyCashDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, current_user)
    @view = view
    @current_user = current_user
    @account_cashes = 0
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: daily_cashes.count,
        rows: data,
        total: fetch_daily_cashes.count,
    }
  end

  private

  def data
    daily_cashes.map do |cash|
      {
          "id" => cash.id,
          "date" => cash.start_date.strftime("%d/%m/%Y"),
          "account" => cash.account.account_number,
          "responsable" => cash.user.owner_name,
          "start_balance" => cash.get_start_balance,
          "end_balance" => cash.get_end_balance,
          "url_show" => doctor_daily_cash_path(cash),
          "url_print" => daily_cash_pdfs_path(cash)
      }
    end
  end

  def daily_cashes
    @daily_cashes ||= fetch_daily_cashes
  end

  def fetch_daily_cashes
    daily_cash = DailyCash.joins(:account)
    if params[:start_date].to_s != "all"
      daily_cash = daily_cash.where("daily_cashes.created_at >= '#{params[:start_date].to_s}'")
    end
    if params[:end_date].to_s != "all"
      daily_cash = daily_cash.where("daily_cashes.created_at < '#{params[:end_date].to_s}'")
    end
    if !daily_cash.nil?
      daily_cash = daily_cash.page(page).per_page(per_page)
      if params[:searchPhrase].present?
        daily_cash_doc = daily_cash.joins(user: :doctor).where("daily_cashes.id like :search or DATE_FORMAT(daily_cashes.start_date,'%d/%m/%Y') like :search or accounts.account_number like :search or CONCAT_WS(' ',doctors.name,doctors.last_name) like :search", search: "%#{params[:searchPhrase]}%")
        daily_cash_assis = daily_cash.joins(user: :assistant).where("daily_cashes.id like :search or DATE_FORMAT(daily_cashes.start_date,'%d/%m/%Y') like :search or accounts.account_number like :search or CONCAT_WS(' ',assistants.name,assistants.last_name) like :search", search: "%#{params[:searchPhrase]}%")
        daily_cash = daily_cash_doc + daily_cash_assis
        @account_cashes = daily_cash.size
        daily_cash
      else
        @account_cashes = daily_cash.size
        daily_cash
      end
    else
      []
    end
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    if params[:sort] != nil
      params[:sort].each do |key, v|
        return v
      end
    end
    return 'desc'
  end

  def sort_column
    columns = {
        :id => 'daily_cashes.id',
        :date=>'daily_cashes.start_date',
        :account=> 'accounts.account_number',
        :responsable=> 'daily_cashes.user_id',
        :start_balance => 'daily_cashes.start_balance',
        :end_balance => 'daily_cashes.end_balance'
    }
    if params[:sort] != nil
      params[:sort].each do |key, array|
        return columns[:"#{key}"]
      end
    end
    columns[:date]
  end
end
