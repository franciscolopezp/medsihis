class MedicamentInventoriesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: medicament_inventories.total_entries,
        rows: data,
        total: medicament_pharmacy_inventories.count + medicament_inventories.count,
    }
  end

  private

  def data
    datas = []
    medicament_inventories.map do |medicament_inventory|
      datas.push( {
                      "id" => medicament_inventory.id,
                      "comercial_name" => medicament_inventory.lot.medicament.comercial_name,
                      "presentation" => medicament_inventory.lot.medicament.presentation,
                      "lot" => medicament_inventory.lot.name,
                      "quantity" => medicament_inventory.quantity,
                      "warehouse" => medicament_inventory.warehouse.name,
                      "category" => medicament_inventory.lot.medicament.medicament_category.present? ? medicament_inventory.lot.medicament.medicament_category.name: '',
                      "url_show"=>admin_medicament_inventories_path(medicament_inventory),
                      "url_edit"=>edit_admin_medicament_inventory_path(medicament_inventory),
                      "url_destroy"=>admin_medicament_inventory_path(medicament_inventory,format: :json)
                  })
    end

    medicament_pharmacy_inventories.map do |item|
      datas.push({
                     "id" => item.id,
                     "comercial_name" => item.pharmacy_item.code + " (" + item.pharmacy_item.description+")",
                     "presentation" => item.pharmacy_item.measure_unit_item.name,
                     "lot" => "N/A",
                     "quantity" => item.quantity,
                     "warehouse" => item.warehouse.name,
                     "category" => item.pharmacy_item.medicament_category.name

                 })
    end

    datas
  end

  def medicament_inventories
    @medicament_inventory ||= fetch_medicament_inventories
  end

  def fetch_medicament_inventories
    medicament_inventories = MedicamentInventory.joins(:lot => :medicament).joins(:warehouse).order("#{sort_column} #{sort_direction}")
    medicament_inventories = medicament_inventories.page(page).per_page(per_page)
    if params[:warehouse_id_search].to_s != "TODO"
      medicament_inventories = medicament_inventories.where("warehouses.id = '#{params[:warehouse_id_search].to_s}'")
    end
    if params[:consigment].to_i == 1
      medicament_inventories = medicament_inventories.where("is_consigment = 1")
    end
    if params[:medicament_catagory_id_search].to_s != "TODO"
      medicament_inventories = medicament_inventories.where("medicaments.medicament_category_id = '#{params[:medicament_catagory_id_search].to_s}'")
    end
    medicament_inventories
  end
  def medicament_pharmacy_inventories
    @pharmacy_item_inventory ||= fetch_pharmacy_inventories
  end

  def fetch_pharmacy_inventories
    medicament_pharmacy_inventories = PharmacyItemInventory.joins(:pharmacy_item).joins(:warehouse).order("#{sort_column} #{sort_direction}")
    medicament_pharmacy_inventories = medicament_pharmacy_inventories.page(page).per_page(per_page)
    if params[:warehouse_id_search].to_s != "TODO"
      medicament_pharmacy_inventories = medicament_pharmacy_inventories.where("warehouses.id = '#{params[:warehouse_id_search].to_s}'")
    end
    if params[:consigment].to_i == 1
      medicament_pharmacy_inventories = medicament_pharmacy_inventories.where("is_consigment = 1")
    end
    if params[:medicament_catagory_id_search].to_s != "TODO"
      medicament_pharmacy_inventories = medicament_pharmacy_inventories.where("pharmacy_items.medicament_category_id = '#{params[:medicament_catagory_id_search].to_s}'")
    end
    medicament_pharmacy_inventories
  end
  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end