class PersonalsupportsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,doctor_id,assistant_id)
    @view = view
    @doctor_id = doctor_id
    @assistant_id = assistant_id
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: personals.total_entries,
        rows: data,
        total: personals.total_entries
    }
  end

  private

  def data
    if @doctor_id != nil
      personals.map do |personal|
        {
            "id" => personal.id,
            "name" => personal.name,
            "last_name" => personal.lastname,
            "email" => personal.email,
            "contact_number" => personal.contact_number,
            "role" => personal.operation_role.name,
            "doctor" => personal.doctor.name + " " +personal.doctor.last_name,
            "url_show"=>doctor_personalsupport_path(personal),
            "url_edit"=>edit_doctor_personalsupport_path(personal),
            "url_destroy"=>doctor_personalsupport_path(personal,format: :json)
        }
      end
      else
        personals.map do |personal|
          {
              "id" => personal.id,
              "name" => personal.name,
              "last_name" => personal.lastname,
              "email" => personal.email,
              "contact_number" => personal.contact_number,
              "role" => personal.operation_role.name,
              "doctor" => personal.doctor.name + " " +personal.doctor.last_name,
              "url_show"=>shared_assistant_personalsupport_path(personal),
              "url_edit"=>edit_shared_assistant_personalsupport_path(personal),
              "url_destroy"=>shared_assistant_personalsupport_path(personal,format: :json)
          }
        end

    end
  end

  def personals
    @personal ||= fetch_personals
  end

  def fetch_personals
    if @doctor_id != nil
    personals = Personalsupport.includes(:operation_role).order("#{sort_column} #{sort_direction}")
    personals = personals.page(page).per_page(per_page)
    personals = personals.where("doctor_id = #{@doctor_id}")
    if params[:searchPhrase].present?
      personals = personals.joins(:operation_role).where("personalsupports.id like :search or personalsupports.name like :search or lastname like :search or email like :search or contact_number like :search or operation_roles.name like :search", search: "%#{params[:searchPhrase]}%")
    end

    else
      doctors_id = ""
      assistant = Assistant.find(@assistant_id)
      assistant.doctors.each do |doc|
        doctors_id += doc.id.to_s + ","
      end
      doctors_id = doctors_id.chop
      personals = Personalsupport.joins(:operation_role).joins(:doctor).order("#{sort_column} #{sort_direction}")
      personals = personals.page(page).per_page(per_page)
      personals = personals.where("doctor_id in ( #{doctors_id})")
      if params[:searchPhrase].present?
        personals = personals.joins(:operation_role).joins(:doctor).where("personalsupports.id like :search or personalsupports.name like :search or lastname like :search or email like :search or contact_number like :search or operation_roles.name like :search or doctors.name like :search or doctors.last_name like :search", search: "%#{params[:searchPhrase]}%")
      end
    end

    personals
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columns = {:id=>'id', :name=>'name', :last_name=> 'lastname', :email=>'email', :contact_number => 'contact_number', :role=> 'operation_roles.name', :doctor => 'doctors.name'}
    #columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #columna = "#{key}"
        return columns[:"#{key}"]
      end
    end
  end
end
