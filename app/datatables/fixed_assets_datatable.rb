class FixedAssetsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: fixed_assets.total_entries,
        rows: data,
        total: FixedAsset.count,
    }
  end

  private

  def data
    fixed_assets.map do |fixed_asset|
      {
          "id" => fixed_asset.id,
          "name" => fixed_asset.name,
          "category" => fixed_asset.fixed_asset_category.name,
          "url_manage" => manage_items_admin_fixed_assets_path(fixed_asset),
          "url_show"=>admin_fixed_assets_path(fixed_asset),
          "url_edit"=>edit_admin_fixed_asset_path(fixed_asset),
          "url_destroy"=>admin_fixed_asset_path(fixed_asset,format: :json)
      }

    end
  end

  def fixed_assets
    @fixed_asset ||= fetch_fixed_assets
  end

  def fetch_fixed_assets
    fixed_assets = FixedAsset.joins(:fixed_asset_category).order("#{sort_column} #{sort_direction}")
    fixed_assets = fixed_assets.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      fixed_assets = fixed_assets.where("fixed_assets.name like :search or fixed_asset_categories.name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    fixed_assets
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end