class UsersDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: users.total_entries,
        rows: data,
        total: User.count,
    }
  end

  private

  def data
    users.map do |user|

        {
            "id" => user.id,
            "user_name" => user.user_name,
            "email" => user.email,
            "password" => user.password,
            "type_user" => user.role.name,
            "url_show"=>admin_user_path(user),
            "url_edit"=>edit_admin_user_path(user),
            "url_destroy"=>admin_user_path(user,format: :json)
        }

    end
  end

  def users
    @users ||= fetch_users
  end

  def fetch_users
    users = User.order("#{sort_column} #{sort_direction}")
    users = users.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      users = users.where("user_name like :search or email like :search", search: "%#{params[:searchPhrase]}%")
    end
    users
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
