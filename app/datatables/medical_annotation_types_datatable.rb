class MedicalAnnotationTypesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: medical_annotation_types.total_entries,
        rows: data,
        total: MedicalAnnotationType.count,
    }
  end

  private

  def data
    medical_annotation_types.map do |medical_annotation_type|
      {
          "id" => medical_annotation_type.id,
          "name" => medical_annotation_type.name,
          "url_show"=>admin_medical_annotation_type_path(medical_annotation_type),
          "url_edit"=>edit_admin_medical_annotation_type_path(medical_annotation_type),
          "url_destroy"=>admin_medical_annotation_type_path(medical_annotation_type,format: :json)
      }

    end
  end

  def medical_annotation_types
    @medical_annotation_type ||= fetch_medical_annotation_types
  end

  def fetch_medical_annotation_types
    medical_annotation_types = MedicalAnnotationType.order("#{sort_column} #{sort_direction}")
    medical_annotation_types = medical_annotation_types.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      medical_annotation_types = medical_annotation_types.where("id like :search or name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    medical_annotation_types
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end
