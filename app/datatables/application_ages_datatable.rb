class ApplicationAgesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: appages.total_entries,
        rows: data,
        total: ApplicationAge.count,
    }
  end

  private

  def data
    appages.map do |application_age|
      {
          "id" => application_age.id,
          "vaccine" => application_age.vaccine.name,
          "age" => application_age.age,
          "age_type" => application_age.age_type,
          "dose" => application_age.dose,
          "comments" => application_age.comments,
          "url_show"=>admin_application_age_path(application_age),
          "url_edit"=>edit_admin_application_age_path(application_age),
          "url_destroy"=>admin_application_age_path(application_age,format: :json)
      }

    end
  end

  def appages
    @applicationAges ||= fetch_appages
  end

  def fetch_appages
    appages = ApplicationAge.order("#{sort_column} #{sort_direction}")
    appages = appages.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      appages = appages.where("id like :search or age like :search or age_type like :search or dose like :search or comments like :search", search: "%#{params[:searchPhrase]}%")
    end
    appages
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
