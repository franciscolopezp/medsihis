class PharmacyItemsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: pharmacy_items.total_entries,
        rows: data,
        total: PharmacyItem.count,
    }
  end

  private

  def data
    pharmacy_items.map do |item|
      {
          "id" => item.id,
          "name" => item.code,
          "measure" => item.measure_unit_item.name,
          "description" => item.description,
          "cost" => number_to_currency(item.cost,:unit => "$"),
          "category"=> item.medicament_category.name,
          "url_show"=>cli_pharmacy_items_path(item),
          "url_edit"=>edit_cli_pharmacy_item_path(item),
          "url_destroy"=>cli_pharmacy_item_path(item,format: :json)
      }

    end
  end

  def pharmacy_items
    @pharmacy_item ||= fetch_pharmacy_items
  end

  def fetch_pharmacy_items
    pharmacy_items = PharmacyItem.joins(:measure_unit_item).order("#{sort_column} #{sort_direction}")
    pharmacy_items = pharmacy_items.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      pharmacy_items = pharmacy_items.where("measure_unit_items.name like :search or description like :search or pharmacy_items.code like :search ", search: "%#{params[:searchPhrase]}%")
    end
    pharmacy_items
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end

end