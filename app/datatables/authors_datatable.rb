class AuthorsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: authors.total_entries,
        rows: data,
        total: Author.count,
    }
  end

  private

  def data
    authors.map do |author|
      {
          "id" => author.id,
          "name" => author.full_name,
          "twitter" => author.twitter,
          "company_title" => author.company_title,
          "job_title" => author.job_title,
          "url_show"=>admin_author_path(author),
          "url_edit"=>edit_admin_author_path(author),
          "url_destroy"=>admin_author_path(author, format: :json),
      }

    end
  end

  def authors
    @author ||= fetch_authors
  end

  def fetch_authors
    authors = Author.order("#{sort_column} #{sort_direction}")
    authors = authors.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      authors = authors.where("title like :search or summary like :search or content like :search author like :search", search: "%#{params[:searchPhrase]}%")
    end
    authors
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
