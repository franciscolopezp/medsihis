class PersonsDatatable
include Rails.application.routes.url_helpers
delegate :params, :h, :link_to, :number_to_currency, to: :@view

def initialize(view)
  @view = view
end

def as_json(options = {})
  {
      current: params[:current].to_i,
      rowCount: people.total_entries,
      rows: data,
      total: people.total_entries,
  }
end

private

def data
  people.map do |person|
    {
        "id" => person.id,
        "name" => person.full_name,
        "type" => person.user.role.name,
        "telephone" => person.telephone,
        "url_show"=>admin_person_path(person),
        "url_edit"=>edit_admin_person_path(person),
        "url_destroy"=>admin_person_path(person,format: :json)
    }

  end
end

def people
  @people ||= fetch_people
end

def fetch_people
  people = Person.all.order("#{sort_column} #{sort_direction}")
  people = people.page(page).per_page(per_page)
  if params[:searchPhrase].present?
    people = people.where("id like :search or name like :search or last_name like :search or telephone like :search", search: "%#{params[:searchPhrase]}%")
  else
    people = people
  end
  people
end

def page
  params[:current].to_i
end

def per_page
  params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
end

def sort_direction
  orden = ""
  params[:sort] == 'desc' ? 'desc' : 'asc'
  if params[:sort] != nil
    params[:sort].each do |key, array|
      #puts "#{key}"
      orden =  array
    end
    orden.to_s
  end
end

def sort_column
  columns = {:id => 'id', :name=>'name', :last_name=> 'last_name', :telephone=> 'telephone', :cellphone=>'cellphone', :email => 'email'}
  if params[:sort] != nil
    params[:sort].each do |key, array|
      #columna = "#{key}"
      return columns[:"#{key}"]
    end
  end
end
end
