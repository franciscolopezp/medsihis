class FixedAssetCategoriesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: fixed_asset_categories.total_entries,
        rows: data,
        total: FixedAssetCategory.count,
    }
  end

  private

  def data
    fixed_asset_categories.map do |fixed_asset_category|
      {
          "id" => fixed_asset_category.id,
          "name" => fixed_asset_category.name,
          "url_show"=>admin_fixed_asset_categories_path(fixed_asset_category),
          "url_edit"=>edit_admin_fixed_asset_category_path(fixed_asset_category),
          "url_destroy"=>admin_fixed_asset_category_path(fixed_asset_category,format: :json)
      }

    end
  end

  def fixed_asset_categories
    @fixed_asset_category ||= fetch_fixed_asset_categories
  end

  def fetch_fixed_asset_categories
    fixed_asset_categories = FixedAssetCategory.order("#{sort_column} #{sort_direction}")
    fixed_asset_categories = fixed_asset_categories.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      fixed_asset_categories = fixed_asset_categories.where("id like :search or name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    fixed_asset_categories
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end