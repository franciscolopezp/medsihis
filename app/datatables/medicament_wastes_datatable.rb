class MedicamentWastesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: medicament_wastes.total_entries,
        rows: data,
        total: medicament_wastes.total_entries,
    }
  end

  private

  def data
    if  params[:waste_type].to_i == 0
      medicament_wastes.map do |waste|
        {
            "id" => waste.id,
            "warehouse" => waste.warehouse.name,
            "product" => waste.lot.name + " ("+waste.lot.medicament.comercial_name+")",
            "type" => "Medicamento",
            "quantity" => waste.quantity,
            "cause" => waste.cause,
            "waste_type" => waste.waste_type.name,
            "date" => waste.created_at.strftime('%d/%m/%Y'),
            "url_show"=>cli_medicament_waste_path(waste),
            "url_edit"=>edit_cli_medicament_waste_path(waste),
            "url_destroy"=>cli_medicament_waste_path(waste,format: :json)
        }

      end
    else
      medicament_wastes.map do |waste|
        {
            "id" => waste.id,
            "warehouse" => waste.warehouse.name,
            "product" => waste.pharmacy_item.code + " ("+waste.pharmacy_item.description+")",
            "type" => "Artículo de farmacia",
            "quantity" => waste.quantity,
            "cause" => waste.cause,
            "waste_type" => waste.waste_type.name,
            "date" => waste.created_at.strftime('%d/%m/%Y'),
            "url_show"=>cli_medicament_waste_path(waste),
            "url_edit"=>edit_cli_medicament_waste_path(waste),
            "url_destroy"=>cli_medicament_waste_path(waste,format: :json)
        }

      end
    end

  end

  def medicament_wastes
    @medicament_waste ||= fetch_medicament_wastes
  end

  def fetch_medicament_wastes
    if params[:waste_type].to_i == 0
      medicament_wastes = MedicamentWaste.joins(:warehouse).joins(:lot).order("#{sort_column} #{sort_direction}")
      medicament_wastes = medicament_wastes.page(page).per_page(per_page)
      if params[:searchPhrase].present?
        medicament_wastes = medicament_wastes.where("cause like :search or quantity like :search or warehouses.name like :search or lots.name like :search", search: "%#{params[:searchPhrase]}%")
      end
    else
      medicament_wastes = PharmacyItemWaste.joins(:warehouse).joins(:pharmacy_item).order("#{sort_column} #{sort_direction}")
      medicament_wastes = medicament_wastes.page(page).per_page(per_page)
      if params[:searchPhrase].present?
        medicament_wastes = medicament_wastes.where("cause like :search or quantity like :search or warehouses.name like :search or pharmacy_items.name like :search", search: "%#{params[:searchPhrase]}%")
      end
    end
    medicament_wastes
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end