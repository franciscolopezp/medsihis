class PatientsDatatable
  include DatatableHelper
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view
  include SessionsHelper


  def initialize(view)
    @view = view
  end

  private
  def data
    result = fetch_data.map do |patient|
      format_json_patient patient
    end
    result
  end

  def fetch_records
    patients = Patient
                   .joins(:gender).joins(:user => :person)
                   .includes( :gender, :medical_expedient).where(active: true)




    #patients = filter(patients) #filtro del campo buscar
    if params[:search][:value].present? && params[:columns].size > 0
      #patients = patients.where("CONCAT(patients.name, ' ', last_name) like :search", search: "%#{params[:search][:value]}%")
      filter_query = ''
      params[:columns].each do |key, value|
        if value[:name].present? && value[:searchable].present? && value[:searchable] == 'true'

          if value[:name].include? '|'
            composite_column = value[:name].to_s.split('|')
            if composite_column.count > 1
              composite_column.each do |v|
                filter_query << v.to_s + ' LIKE :search OR '
              end
            end
          else
            filter_query << value[:name].to_s + ' LIKE :search OR '
          end
        end
      end
      filter_name = ''
      params[:search][:value].split(' ').each do |text|
        filter_name << " CONCAT(patients.name,'',patients.last_name) LIKE "+"'%"+text.to_s+"%'" +' AND '
      end
      filter_name = filter_name.first(-4)
      filter_query = filter_query.first(-4)
      filter_query = filter_query +  " OR " + filter_name
      patients = patients.where(filter_query, search: "%#{params[:search][:value]}%")
    end
    if params['length'].to_i <= 0
      params['length'] = patients.count
    end

    patients = patients.order("#{sort_column} #{sort_direction}").page(page).per_page(per_page)
    patients
  end

  def format_json_patient patient
      {
          :id => patient.id,
          :name => patient.name,
          :last_name => patient.last_name,
          :birth_day => patient.birth_day.strftime('%d/%m/%Y'),
          :age => CalculateAge::calculate(@view, patient.birth_day),
          :gender => patient.gender.name,
          :email =>patient.email,
          :phone => patient.phone,
          :cellphone => patient.cell,
          :captured_by => !patient.user.nil? ? patient.user.person.full_name : '',
          :url_show =>doctor_patient_path(patient),
          :url_edit =>edit_doctor_patient_path(patient),
          :url_destroy =>disable_patient_doctor_patients_path(format: :json),
          :url_show_medical_expedient =>doctor_expedient_path(patient.medical_expedient),
          :url_show_assistant_shared =>shared_assistant_patient_path(patient),
          :url_edit_assistant_shared =>edit_shared_assistant_patient_path(patient)
      }
  end

end
