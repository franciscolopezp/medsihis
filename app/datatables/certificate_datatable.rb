class CertificateDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,doctor, filter_by_patient = nil)
    @view = view
    @doctor = doctor
    @filter_by_patient = filter_by_patient
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: certificates.total_entries,
        rows: data,
        total: certificates.total_entries
    }
  end

  private
  def data

    certificates.map.with_index(1) do |certificate,index|

      {
          "id" => index,
          "name" =>  certificate.name,
          "type_certificate_id" =>  certificate.type_certificate.name,
          "office_id" =>  certificate.office.name,
          "patient_id" => (certificate.patient.nil?)? "" : certificate.patient.full_name,
          "show_url"=>show_certificate_doctor_certificate_index_path(certificate),
          "edit_url"=>edit_certificate_doctor_certificate_index_path(certificate),
          "print_url"=>print_certificate_doctor_certificate_index_path(certificate, format: :pdf),
          "destroy_url"=>destroy_certificate_doctor_certificate_index_path(certificate)
      }

    end
  end

  def certificates
    @certificates ||= fetch_certificate
  end

  def fetch_certificate
    certificates = Certificate.joins(:type_certificate).where(type_certificates: {doctor_id: @doctor.id})
    unless @filter_by_patient.nil?
      certificates = certificates.where(patient_id: @filter_by_patient.id)
    end

    certificates = certificates.order("#{sort_column} #{sort_direction}")
    if params[:searchPhrase].present?
      certificates = certificates.where("certificates.id like :search or certificates.name like :search", search: "%#{params[:searchPhrase]}%")
    end
    certificates = certificates.page(page).per_page(per_page)

    certificates
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
