class WarehousesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: warehouses.total_entries,
        rows: data,
        total: Warehouse.count,
    }
  end

  private

  def data
    warehouses.map do |warehouse|
      {
          "id" => warehouse.id,
          "name" => warehouse.name,
          "url_show"=>admin_warehouses_path(warehouse),
          "url_edit"=>edit_admin_warehouse_path(warehouse),
          "url_destroy"=>admin_warehouse_path(warehouse,format: :json)
      }

    end
  end

  def warehouses
    @fixed_asset_category ||= fetch_warehouses
  end

  def fetch_warehouses
    warehouses = Warehouse.order("#{sort_column} #{sort_direction}")
    warehouses = warehouses.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      warehouses = warehouses.where("id like :search or name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    warehouses
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end