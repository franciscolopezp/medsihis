class AppointmentsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,patient)
    @view = view
    @patient = patient
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: activities.count,
        rows: data,
        total: activities.count,
    }
  end

  private

  def data
    activities.map do |activity|
      {
          "id" => activity.id,
          "date" => activity.start_date.strftime('%d/%m/%Y %l:%M %p'),
          "details" => activity.details,
          "doctor" => activity.doctor.full_name,
          "url_destroy"=>doctor_activity_path(activity,format: :json)
      }

    end
  end

  def activities
    @activity ||= fetch_activities
  end

  def fetch_activities
    activities = Activity.joins(:user_patients,:doctor).where('activities_user_patients.user_patient_id = ? ', @patient.id).order("#{sort_column} #{sort_direction}")
    activities = activities.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      activities = activities.where("id like :search or name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    activities
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    if params[:sort] != nil
      params[:sort].each do |key, v|
        return v
      end
    end
    return 'desc'
  end

  def sort_column
    columns = {
        :date=>'activities.start_date',
        :details=>'activities.details',
        :doctor=>'doctors.name'
    }
    if params[:sort] != nil
      params[:sort].each do |key, array|
        return columns[:"#{key}"]
      end
    end
    return columns[:date]
  end
end
