class OtherChargesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: other_charges.total_entries,
        rows: data,
        total: OtherCharge.count,
    }
  end

  private

  def data
    other_charges.map do |charge|
      total = 0;
      discount = 0;
      paid = 0;
      charge.other_charge_items.each do |item|
        total += item.service_amount
        discount += item.service_discount
      end
      charge.other_charge_transactions.where("is_cancel = 0").each do |item|
        paid += item.amount
      end
      residue = paid - (total-discount)
      if residue >= 0
       lable_residue =  "<label style='color: #0a68b4; font-weight: bold;'>"+number_to_currency(residue,:unit => "$")+"</label>"
      else
        lable_residue =  "<label style='color: red; font-weight: bold;'>"+number_to_currency(residue,:unit => "$")+"</label>"
      end
      {
          "id" => charge.id,
          "name" => charge.patient_name,
          "discount" => number_to_currency(discount,:unit => "$"),
          "total" => number_to_currency(total,:unit => "$"),
          "paid" => number_to_currency(paid,:unit => "$"),
          "residue" => lable_residue,
          "is_closed" => charge.is_closed,
          "url_show"=>cli_other_charge_path(charge),
          "url_edit"=>edit_cli_other_charge_path(charge),
          "url_destroy"=>cli_other_charge_path(charge,format: :json)
      }

    end
  end

  def other_charges
    @fixed_asset_category ||= fetch_other_charges
  end

  def fetch_other_charges
    if params[:show_all].to_i == 0
      other_charges = OtherCharge.where("is_closed = 0").order("#{sort_column} #{sort_direction}")
    else
      other_charges = OtherCharge.where("is_closed = 1").order("#{sort_column} #{sort_direction}")
    end
    other_charges = other_charges.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      filter_name = ''
      params[:searchPhrase].split(' ').each do |text|
        filter_name << " patient_name LIKE "+"'%"+text.to_s+"%'" +' AND '
      end
      filter_name = filter_name.first(-4)
      other_charges = other_charges.where(filter_name)
    end
    other_charges
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end