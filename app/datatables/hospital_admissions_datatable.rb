class HospitalAdmissionsDatatable
  include DatatableHelper
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view
  include SessionsHelper

  def initialize(view)
    @view = view
  end

  private
  def data
    result = fetch_data.map do |admission|
      {
          "id" => admission.id,
          "patient" => admission.patient.full_name,
          "doctor" => admission.doctor.user.person.full_name,
          "triage" => "<div style='background: #"+admission.triage.color+";width: 40px;height: 20px'></div>",
          "attention_type" => admission.attention_type.name,
          "admission_date" => admission.admission_date.strftime('%d/%m/%Y %I:%M %p'),
          "url_show"=>cli_hospital_admission_path(admission),
          "url_edit"=>edit_cli_hospital_admission_path(admission),
          "url_invoice"=> open_invoice_admission_cli_hospital_admissions_path(admission),
          "url_destroy"=>cli_hospital_admission_path(admission,format: :json),
          "print_pdf_admission"=>hospital_admission_pdfs_path(admission),
      }
    end
    result
  end

  def fetch_records
    hospital_admissions = HospitalAdmission.joins(:patient, {:doctor => [:user => :person]}, :triage, :attention_type)
                              .includes(:patient, {:doctor => [:user => :person]}, :triage, :attention_type)
    if params[:closed].present? && params[:closed] == "true"
      hospital_admissions = hospital_admissions.where(:is_closed => true)
    else
      hospital_admissions = hospital_admissions.where(:is_closed => false)
    end

    if params[:search][:value].present? && params[:columns].size > 0
      filter_query = ''
      params[:columns].each do |key, value|
        if value[:name].present? && value[:searchable].present? && value[:searchable] == 'true'

          if value[:name].include? '|'
            composite_column = value[:name].to_s.split('|')
            if composite_column.count > 1
              composite_column.each do |v|
                filter_query << v.to_s + ' LIKE :search OR '
              end
            end
          else
            filter_query << value[:name].to_s + ' LIKE :search OR '
          end
        end
      end
      filter_name = ''
      params[:search][:value].split(' ').each do |text|
        filter_name << " CONCAT(patients.name,'',patients.last_name) LIKE "+"'%"+text.to_s+"%'" +' AND '
      end
      filter_name = filter_name.first(-4)
      filter_query = filter_query.first(-4)
      filter_query = filter_query +  " OR " + filter_name
      hospital_admissions = hospital_admissions.where(filter_query, search: "%#{params[:search][:value]}%")
    end
    if params['length'].to_i <= 0
      params['length'] = hospital_admissions.count
    end

    hospital_admissions = hospital_admissions.order("#{sort_column} #{sort_direction}").page(page).per_page(per_page)
    hospital_admissions
  end

end