class ConsultsToInvoiceDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: transactions.total_entries,
        rows: data,
        total: count_transacs,
    }
  end

  private


  def count_transacs
    if params[:patient].to_s == "all"
      transacs = Charge.joins(:transactions).joins(:medical_consultation => :medical_expedient).where("transaction_type_id = 1 AND medical_expedients.doctor_id = #{params[:doctor_id]}")
    else
      transacs = Charge.joins(:transactions).joins(:medical_consultation => :medical_expedient).where("transaction_type_id = 1 AND medical_expedients.doctor_id = #{params[:doctor_id]} AND medical_expedients.patient_id = #{params[:patient].to_i}")
    end
    transacs.where("invoiced = 0").count
  end


  def data
    serie = ""
    transactions.map do |charge|
      transaction = charge.transaction
      folio = 0

      if transaction.invoice != nil
        serie = transaction.invoice.serie + "-" + transaction.invoice.folio.to_s
        folio = transaction.invoice.folio
      else
        serie = "";
        folio = 0
      end

      {
          "id" => transaction.id,
          "amount" =>  number_to_currency(transaction.amount, :unit => "$"),
          "description" => transaction.description,
          "patient" => transaction.chargeable.medical_consultation.patient.name + " " + transaction.chargeable.medical_consultation.patient.last_name,
          "date" => transaction.date.strftime('%d/%m/%Y'),
          "transaction_type" => transaction.transaction_type.name,
          "serie" => serie,
          "folio" => folio,
          "invoiced"=>transaction.invoiced,
          "print"=>print_pdf_doctor_transactions_path(transaction,folio, format: :pdf),
          "download_xml"=>transaction_download_xml_doctor_transactions_path(transaction,folio, format: :xml)
      }

    end
  end

  def transactions
    @specialty ||= fetch_transactions
  end

  def fetch_transactions
    if params[:patient].to_s == "all"
      transactions = Charge.joins(:transactions).joins(:medical_consultation => :medical_expedient).where("transaction_type_id = 1 AND medical_expedients.doctor_id = #{params[:doctor_id]}")
    else
      transactions = Charge.joins(:transactions).joins(:medical_consultation => :medical_expedient).where("transaction_type_id = 1 AND medical_expedients.doctor_id = #{params[:doctor_id]} AND medical_expedients.patient_id = #{params[:patient].to_i}")
    end
    transactions = transactions.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      transactions = transactions.joins(:transactions).joins(:medical_consultation => :medical_expedient).where("transactions.transaction_type_id = 1  AND medical_expedients.doctor_id = #{params[:doctor_id]} AND transactions.id like :search or transactions.description like :search or date like :search", search: "%#{params[:searchPhrase]}%")
    end
    transactions.where("invoiced = 0").order("#{sort_column} #{sort_direction}",created_at: :desc)
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
