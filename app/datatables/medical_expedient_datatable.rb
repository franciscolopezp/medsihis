class MedicalExpedientDatatable
  include DatatableHelper
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  private
  def data
    fetch_data.map do |expedient|
      {
          "folio" => expedient.folio,
          "name" => expedient.patient.full_name,
          "gender" => expedient.patient.gender.name,
          "age" => CalculateAge::calculate(@view, expedient.patient.birth_day),
          "doctor" => get_doctor(expedient)[0][:doctor],
          "last_attention" => get_doctor(expedient)[0][:last_date],
          "url_show_medical_expedient"=>doctor_expedient_path(expedient),
      }
    end
  end

  def fetch_records
    expedients = MedicalExpedient
                     .joins(:patient => :gender)
                     .includes(:medical_consultations, :patient => :gender)
                     .where("medical_expedients.active = 1")

    expedients = filter(expedients)
    expedients
  end

  def get_doctor(expedient)
    data = []
    last_consultation = expedient.medical_consultations.last
    medical_annotation = MedicalAnnotation.where(:patient_id => expedient.patient_id).last

    if last_consultation.present? && !medical_annotation.present?
      return data.push(:doctor => last_consultation.user.person.full_name, :last_date => last_consultation.created_at.strftime("%d/%m/%Y"))
    end

    if !last_consultation.present? && medical_annotation.present?
      if medical_annotation.user.is_doctor
        return data.push(:doctor => medical_annotation.user.person.full_name, :last_date => medical_annotation.created_at.strftime("%d/%m/%Y"))
      else
        return data.push(:doctor => "No disponible", :last_date => medical_annotation.created_at.strftime("%d/%m/%Y"))
      end
    end

    if !last_consultation.present? && !medical_annotation.present?
      return data.push(:doctor => "No disponible",:last_date => "No disponible")
    end

    if last_consultation.created_at > medical_annotation.created_at
      return data.push(:doctor => last_consultation.user.person.full_name, :last_date => last_consultation.strftime("%d/%m/%Y"))
    else
      if medical_annotation.user.is_doctor
        return data.push(:doctor => medical_annotation.user.person.full_name, :last_date => medical_annotation.created_at.strftime("%d/%m/%Y"))
      else
        return data.push(:doctor => "No disponible", :last_date => medical_annotation.created_at.strftime("%d/%m/%Y"))
      end
    end

  end


end
