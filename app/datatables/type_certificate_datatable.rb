class TypeCertificateDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view,doctor)
    @view = view
    @doctor = doctor
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: type_certificates.total_entries,
        rows: data,
        total: type_certificates.total_entries
    }
  end

  private
  def data
    type_certificates.map.with_index(1) do |type,index|
      {
          "id" => index,
          "name" =>  type.name,
          "show_url"=>show_type_certificate_doctor_certificate_index_path(type),
          "edit_url"=>edit_type_certificate_doctor_certificate_index_path(type),
          "destroy_url"=>destroy_type_certificate_doctor_certificate_index_path(type)
      }

    end
  end

  def type_certificates
    @type_certificates ||= fetch_type_certificate
  end

  def fetch_type_certificate

    type_certificates = TypeCertificate.all.where(doctor_id: @doctor.id).order("#{sort_column} #{sort_direction}")


    if params[:searchPhrase].present?
      type_certificates = type_certificates.where("type_certificates.id like :search or type_certificates.name like :search", search: "%#{params[:searchPhrase]}%")
    end
    type_certificates = type_certificates.page(page).per_page(per_page)

    type_certificates
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
