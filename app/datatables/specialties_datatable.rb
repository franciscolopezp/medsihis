class SpecialtiesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: specialties.total_entries,
        rows: data,
        total: Specialty.count,
    }
  end

  private

  def data
    specialties.map do |specialty|
      {
          "id" => specialty.id,
          "name" => specialty.name,
          "url_show"=>admin_specialty_path(specialty),
          "url_edit"=>edit_admin_specialty_path(specialty),
          "url_destroy"=>admin_specialty_path(specialty,format: :json)

      }

    end
  end

  def specialties
    @specialty ||= fetch_specialties
  end

  def fetch_specialties
    specialties = Specialty.order("#{sort_column} #{sort_direction}")
    specialties = specialties.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      specialties = specialties.where("id like :search or name like :search", search: "%#{params[:searchPhrase]}%")
    end
    specialties
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
