class LotsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: lots.total_entries,
        rows: data,
        total: Lot.count,
    }
  end

  private

  def data
    lots.map do |lot|
      quantity = 0
      wastes = 0;
      assortments = 0;
      lot.medicament_purchase_items.each do |item|
        quantity += item.quantity
      end
      lot.medicament_wastes.each do |item|
        wastes += item.quantity
      end
      lot.medicament_assortment_items.each do |item|
        assortments += item.quantity
      end
      {
          "id" => lot.id,
          "name" => lot.name,
          "expiration_date" => lot.expiration_date.strftime("%d/%m/%Y"),
          "medicament" => lot.medicament.comercial_name,
          "quantity" => quantity-(wastes+assortments),
          "url_show"=>admin_lot_path(lot),
          "url_edit"=>edit_admin_lot_path(lot)
      }

    end
  end

  def lots
    @lot ||= fetch_lots
  end

  def fetch_lots
    lots = Lot.joins(:medicament).order("#{sort_column} #{sort_direction}")
    lots = lots.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      lots = lots.where("lots.name like :search or medicaments.name like :search", search: "%#{params[:searchPhrase]}%")
    end
    lots
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end