class HospitalServicesDatatable
  include DatatableHelper
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view
  include SessionsHelper


  def initialize(view)
    @view = view
  end

  private
  def data
    result = fetch_data.map do |service|
      format_json_service service
    end
    result
  end

  def fetch_records
    hospital_services = HospitalService.joins(:hospital_service_category).includes(:hospital_service_category).where("is_user = 1")
    hospital_services = filter(hospital_services)
    hospital_services
  end

  def format_json_service service
    if service.is_fixed_cost
      fixed_cost = "Si"
    else
      fixed_cost = "No"
    end
    {
        "id" => service.id,
        "code" => service.code,
        "name" => service.name,
        "is_fixed_cost" => fixed_cost,
        "category" => service.hospital_service_category.name,
        "cost" => number_to_currency(service.cost,:unit => "$"),
        "url_show"=>cli_hospital_service_path(service),
        "url_edit"=>edit_cli_hospital_service_path(service),
        "url_destroy"=>cli_hospital_service_path(service,format: :json)
    }
  end




end