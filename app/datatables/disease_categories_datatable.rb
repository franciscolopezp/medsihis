class DiseaseCategoriesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: diseases_categories.total_entries,
        rows: data,
        total: diseases_categories.total_entries,
    }
  end

  private

  def data
    diseases_categories.map do |disease_category|
      {
          "id" => disease_category.id,
          "code" => disease_category.code,
          "name" => disease_category.name,
          "url_show"=>admin_disease_category_path(disease_category),
          "url_edit"=>edit_admin_disease_category_path(disease_category),
          "url_destroy"=>admin_disease_category_path(disease_category,format: :json)

      }

    end
  end

  def diseases_categories
    @disease_category ||= fetch_diseases_categories
  end

  def fetch_diseases_categories
    diseases_categories = DiseaseCategory.order("#{sort_column} #{sort_direction}")
    diseases_categories = diseases_categories.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      diseases_categories = diseases_categories.where("id like :search or name like :search or code like :search", search: "%#{params[:searchPhrase]}%")
    end
    diseases_categories
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        #puts "#{key}"
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    columna = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        columna = "#{key}"
        #columna =  array
      end
      columna.to_s
    end
  end
end
