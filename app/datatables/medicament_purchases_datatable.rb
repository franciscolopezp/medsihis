class MedicamentPurchasesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: medicament_purchases.total_entries,
        rows: data,
        total: MedicamentPurchase.count,
    }
  end

  private

  def data
    medicament_purchases.map do |med_purchase|
      {
          "id" => med_purchase.id,
          "cost" => number_to_currency(med_purchase.cost,:unit => "$"),
          "folio" => med_purchase.folio,
          "invoice_number" => med_purchase.invoice_number,
          "supplier" => med_purchase.supplier.name,
          "warehouse" => med_purchase.warehouse.name,
          "url_show"=>cli_medicament_purchase_path(med_purchase),
          "url_edit"=>edit_cli_medicament_purchase_path(med_purchase),
          "url_destroy"=>cli_medicament_purchase_path(med_purchase,format: :json)
      }

    end
  end

  def medicament_purchases
    @medicament_purchase ||= fetch_medicament_purchases
  end

  def fetch_medicament_purchases
    medicament_purchases = MedicamentPurchase.joins(:supplier).joins(:warehouse).order("#{sort_column} #{sort_direction}")
    medicament_purchases = medicament_purchases.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      medicament_purchases = medicament_purchases.where("folio like :search or invoice_number like :search or suppliers.name like :search or warehouses.name like :search", search: "%#{params[:searchPhrase]}%")
    end
    medicament_purchases
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end