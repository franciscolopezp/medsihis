class SuppliersDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: suppliers.total_entries,
        rows: data,
        total: Supplier.count,
    }
  end

  private

  def data
    suppliers.map do |supplier|
      {
          "id" => supplier.id,
          "name" => supplier.name,
          "telephone" => supplier.telephone,
          "address" => supplier.address,
          "contact" => supplier.contact,
          "url_show"=>cli_suppliers_path(supplier),
          "url_edit"=>edit_cli_supplier_path(supplier),
          "url_destroy"=>cli_supplier_path(supplier,format: :json)
      }

    end
  end

  def suppliers
    @fixed_asset_category ||= fetch_suppliers
  end

  def fetch_suppliers
    suppliers = Supplier.order("#{sort_column} #{sort_direction}")
    suppliers = suppliers.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      suppliers = suppliers.where("telephone like :search or name like :search or address lile :search or contact like :search", search: "%#{params[:searchPhrase]}%")
    end
    suppliers
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end

end