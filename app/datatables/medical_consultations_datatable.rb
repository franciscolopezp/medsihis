class MedicalConsultationsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, current_user,assistant_id, expedient = nil)
    @view = view
    @current_user = current_user
    @expedient = expedient
    @assistant_id = assistant_id

    @total_count = medical_consultations.count + 1
    @current = params[:current].to_i
    @current  = @current - 1
    @total_count = @total_count - (per_page.to_i * @current)

    @total_count = 0 if params[:sort].present? and sort_direction.to_s.downcase == "asc"
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: medical_consultations.count,
        rows: data,
        total: medical_consultations.count
    }
  end

  private
  def data
    medical_consultations.map do |consult|
      @total_count = (params[:sort].present? and sort_direction.to_s.downcase == "asc")? (@total_count + 1) : (@total_count - 1)
      {
          "id" => consult.id,
          "number" => @total_count,
          "patient_name" => consult.medical_expedient.patient.full_name,
          "expedient" => consult.medical_expedient.folio,
          "user" => consult.user.id,
          "type" => consult.consultation_type.name,
          "editable_cost" => consult.consultation_type.consultation_costs.first.is_editable_price,
          "author" => consult.user.person.full_name,
          "date_consultation" => consult.date_consultation.strftime('%d/%m/%Y %I:%M %p'),
          "price" => number_to_currency(consult.price),
          "office" => consult.office.name,
          "diagnostic" => (consult.diagnostic.length > 200 )? consult.diagnostic[0..200].to_s+"..." : consult.diagnostic,
          "is_charged" => consult.is_charged,
          "email" => consult.medical_expedient.patient.email,
          "print_diagnostic" => consult.print_diagnostic,
          "url_show_medical_consultation"=>doctor_medical_consultation_path(consult),
          "url_edit_medical_consultation"=>edit_doctor_medical_consultation_path(consult),
          "balance" => number_to_currency(consult.price - consult.get_total),
          "charged_amount" => consult.get_total
      }

    end
  end

  def medical_consultations
    @medical_consultations ||= fetch_medical_consultation
  end

  def fetch_medical_consultation



    if !@expedient.nil?
      expedients = MedicalConsultation.joins(:user => :person)
                       .joins(:medical_expedient => :patient)
                       .where(:patients => {:id => @expedient.patient.id})
                       .order("#{sort_column} #{sort_direction}")
      if params[:searchPhrase].present?
        expedients = MedicalConsultation.joins(:user => :person)
                         .joins(:medical_expedient => :patient)
                         .where(:patients => {:id => @expedient.patient.id})
                         .where("medical_consultations.id like :search or diagnostic like :search or DATE_FORMAT(date_consultation,'%d/%m/%Y') like :search or CONCAT_WS(' ',patients.name,patients.last_name) like :search or CONCAT_WS(' ',people.name,people.last_name) like :search", search: "%#{params[:searchPhrase]}%")
                         .order("medical_consultations.id DESC")
      end
      expedients = expedients.page(page).per_page(per_page)
    else


        expedients = MedicalConsultation.joins(:office).joins(:medical_expedient => :user)
                         .joins(:medical_expedient => :patient)
                         .order("#{sort_column} #{sort_direction}")


      if params[:searchPhrase].present?
        expedients = expedients.where("medical_consultations.id like :search or diagnostic like :search or DATE_FORMAT(date_consultation,'%d/%m/%Y') like :search or CONCAT_WS(' ',patients.name,patients.last_name) like :search", search: "%#{params[:searchPhrase]}%")
      end

      if params[:start_date] != nil && params[:start_date] != '' && params[:start_date] != "undefined"
        expedients = expedients.where('medical_consultations.date_consultation >= ?' , Date::strptime(params[:start_date], "%d/%m/%Y"))
      end

      if params[:end_date] != nil && params[:end_date] != '' && params[:end_date] != "undefined"
        expedients = expedients.where('medical_consultations.date_consultation < ?' , Date::strptime(params[:end_date], "%d/%m/%Y") + 1.day)
      end

      if params[:office_id] != nil && params[:office_id] != '' && params[:office_id] != "undefined"
        expedients = expedients.where('medical_consultations.office_id = ?' , params[:office_id] )
      end

      if params[:doctor] != nil && params[:doctor] != '' && params[:doctor] != "undefined"
        doctor = Doctor.find(params[:doctor])
        expedients = expedients.where('medical_consultations.user_id = ?' , doctor.user.id )
      end


      expedients = expedients.page(page).per_page(per_page)
    end
    expedients
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    if params[:sort] != nil
      params[:sort].each do |key, v|
        return v
      end
    end
    return 'desc'
  end

  def sort_column
    columns = {
        :date_consultation=>'medical_consultations.date_consultation',
        :expedient=> 'medical_expedients.folio',
        :patient_name => 'patients.name',
        :diagnostic => 'medical_consultations.diagnostic',
        :office => 'offices.name',
        :price => 'medical_consultations.price'
    }
    if params[:sort] != nil
      params[:sort].each do |key, array|
        return columns[:"#{key}"]
      end
    end
    return columns[:date_consultation]
  end
end
