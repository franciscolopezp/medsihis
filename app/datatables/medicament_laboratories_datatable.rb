class MedicamentLaboratoriesDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: medicament_laboratories.total_entries,
        rows: data,
        total: MedicamentLaboratory.count,
    }
  end

  private

  def data
    medicament_laboratories.map do |medicament_laboratory|
      {
          "id" => medicament_laboratory.id,
          "name" => medicament_laboratory.name,
          "url_show"=>admin_medicament_laboratories_path(medicament_laboratory),
          "url_edit"=>edit_admin_medicament_laboratory_path(medicament_laboratory),
          "url_destroy"=>admin_medicament_laboratory_path(medicament_laboratory,format: :json)
      }

    end
  end

  def medicament_laboratories
    @fixed_asset_category ||= fetch_medicament_laboratories
  end

  def fetch_medicament_laboratories
    medicament_laboratories = MedicamentLaboratory.order("#{sort_column} #{sort_direction}")
    medicament_laboratories = medicament_laboratories.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      medicament_laboratories = medicament_laboratories.where("name like :search ", search: "%#{params[:searchPhrase]}%")
    end
    medicament_laboratories
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end


end