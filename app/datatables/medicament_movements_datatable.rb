class MedicamentMovementsDatatable
  include Rails.application.routes.url_helpers
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        current: params[:current].to_i,
        rowCount: medicament_movements.total_entries,
        rows: data,
        total: medicament_movements.total_entries
    }
  end

  private

  def data
    if  params[:movement_type].to_i == 0
    medicament_movements.map do |movement|
      {
          "id" => movement.id,
          "from_warehouse" => movement.from_warehouse.name,
          "to_warehouse" => movement.to_warehouse.name,
          "lot" => movement.lot.name,
          "medicament" => movement.lot.medicament.comercial_name,
          "quantity" => movement.quantity,
          "date" => movement.created_at.strftime('%d/%m/%Y'),
          "url_show"=>cli_medicament_movement_path(movement),
          "url_edit"=>edit_cli_medicament_movement_path(movement),
          "url_destroy"=>cli_medicament_movement_path(movement,format: :json)
      }
      end
      else
      medicament_movements.map do |movement|
        {
            "id" => movement.id,
            "from_warehouse" => movement.from_warehouse.name,
            "to_warehouse" => movement.to_warehouse.name,
            "lot" => "N/A",
            "medicament" => movement.pharmacy_item.code + " (" + movement.pharmacy_item.description + ")",
            "quantity" => movement.quantity,
            "date" => movement.created_at.strftime('%d/%m/%Y'),
            "url_show"=>cli_medicament_movement_path(movement),
            "url_edit"=>edit_cli_medicament_movement_path(movement),
            "url_destroy"=>cli_medicament_movement_path(movement,format: :json)
        }
    end
    end
  end

  def medicament_movements
    @medicament_movement ||= fetch_medicament_movements
  end

  def fetch_medicament_movements
    if params[:movement_type].to_i == 0
    medicament_movements = MedicamentMovement.joins(:lot).order("#{sort_column} #{sort_direction}")
    medicament_movements = medicament_movements.page(page).per_page(per_page)
    if params[:searchPhrase].present?
      medicament_movements = medicament_movements.where("from_warehouse like :search or to_warehouse like :search or lots.name like :search", search: "%#{params[:searchPhrase]}%")
    end
    else
      medicament_movements = PharmacyItemMovement.joins(:pharmacy_item).order("#{sort_column} #{sort_direction}")
      medicament_movements = medicament_movements.page(page).per_page(per_page)
      if params[:searchPhrase].present?
        medicament_movements = medicament_movements.where("from_warehouse like :search or to_warehouse like :search or pharmacy_items.name like :search", search: "%#{params[:searchPhrase]}%")
      end
    end
    medicament_movements
  end

  def page
    params[:current].to_i
  end

  def per_page
    params[:rowCount].to_i > 0 ? params[:rowCount].to_i : 10
  end

  def sort_direction
    orden = ""
    if params[:sort] != nil
      params[:sort].each do |key, array|
        orden =  array
      end
      orden.to_s
    end
  end

  def sort_column
    column = "desc"
    if params[:sort] != nil
      params[:sort].each do |key, array|
        column = "#{key}"
      end
      column.to_s
    end
  end
end