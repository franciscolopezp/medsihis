class RegisterDoctorStaff < ApplicationMailer
  def register(data)
    @data = data
    send_to = ENV['TO_WHEN_NEW_DOCTOR'].split('|')
    mail to: send_to, subject: 'Registro de Médico'
  end
end
