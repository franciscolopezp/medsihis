class InfoPersonalSupports < ApplicationMailer
  def send_message(personal_support,suffix_doctor,doctor,activity)
    attachments.inline['logo.png'] = File.read(Rails.root.join('app','assets','images','medsi_logo.png'))
    @personal_support = personal_support
    @doctor_activity = doctor
    @suffix = suffix_doctor
    @activity = activity
    mail(to: personal_support.email, subject: 'Aviso de actividad programada',
        reply_to: activity.doctor.user.email)
  end
end
