class ApplicationMailer < ActionMailer::Base
  default from: "atencion@medsi.com.mx"
  layout 'mailer'
end
