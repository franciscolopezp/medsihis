class AnalysisOrder < ApplicationMailer

  def send_report(email,pdfself,pdf_name,report)
    @report = report
    attachments[pdf_name] = {:mime_type => 'application/pdf',
                             :content => pdfself }

    mail(to: email, subject: "Reporte Medsi",
        reply_to: report.office.doctor.user.email)

  end
end