class RegisterPatient < ApplicationMailer
  def register(user_patient)
    @patient = user_patient
    @url = login_url
    mail to: user_patient.user.email, subject: 'Bienvenido a Medsi, la plataforma que te ayudará a llevar el control de tu salud'
  end
end