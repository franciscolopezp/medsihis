class RegisterDoctor < ApplicationMailer
  def register(data)
    @data = data
    send_to = data['email']
    mail to: send_to, subject: 'Gracias por su registro'
  end
end
