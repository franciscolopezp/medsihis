class AppointmentMail < ApplicationMailer
  def scheduled(activity)
    @patient = activity.patients.first
    @activity = activity
    mail(to: activity.doctor.user.email, subject: 'Nueva cita',
          reply_to: activity.doctor.user.email)
  end

  def cancel(activity)
    @activity = activity
    @patient = activity.patients.first
    mail(to: activity.doctor.user.email, subject: 'Cita cancelada',
         reply_to: activity.doctor.user.email)
  end

  def reschedule(activity)
    @patient = activity.patients.first
    @activity = activity
    mail(to: activity.doctor.user.email, subject: 'Cita re agendada',
         reply_to: activity.doctor.user.email)
  end
end