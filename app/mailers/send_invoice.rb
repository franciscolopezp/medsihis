class SendInvoice < ApplicationMailer

  def send_invoice(email,pdfs,pdf_name,invoice)
    puts email
    @sent_invoice = invoice
      attachments[pdf_name+'.xml'] = {:mime_type => 'application/xml',
                                   :content => invoice.xml }

    attachments[pdf_name] = {:mime_type => 'application/pdf',
                                 :content => pdfs }

    mail(to: email, subject: "Factura Electrónica",
          reply_to: ClinicInfo.first.contact_email)

  end

end
