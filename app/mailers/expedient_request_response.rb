class ExpedientRequestResponse < ApplicationMailer
  def approve(user_patient,expedient)
    @user_patient = user_patient
    @expedient = expedient
    mail(to: user_patient.user.email, subject: 'Vinculación de expediente aprobada',
        reply_to: expedient.doctor.user.email)
  end
end