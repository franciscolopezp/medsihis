class SendPrescription < ApplicationMailer

  def send_email(email,pdf,pdf_name,consultation)
    attachments[pdf_name] = {:mime_type => 'application/pdf',
                                 :content => pdf }

    @patient = consultation.medical_expedient.patient
    @consultation = consultation
    mail(to: email, subject: "Receta de su consulta "+consultation.date_consultation.strftime("%d/%m/%Y"),
        reply_to: ClinicInfo.first.contact_email)
  end

end
