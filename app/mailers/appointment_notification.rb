class AppointmentNotification < ApplicationMailer
  def confirm(activity)
    @patient = activity.user_patients.first
    @activity = activity
    mail(to: @patient.user.email, subject: 'Su cita ha sido confirmada',
         reply_to: activity.doctor.user.email)
  end

  def cancel(activity,message)
    @activity = activity
    @patient = activity.user_patients.first
    @message = message
    mail(to: @patient.user.email, subject: 'Su cita ha sido cancelada',
         reply_to: activity.doctor.user.email)
  end

  def new_schedule(activity)
    @patient = activity.user_patients.first
    @activity = activity
    mail(to: @patient.user.email, subject: 'Su cita ha sido re agendada',
         reply_to: activity.doctor.user.email)
  end
end