class RecoverPassword < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.recover_password.new_password.subject
  #
  def new_password(user)
    attachments.inline['logo.png'] = File.read(Rails.root.join('app','assets','images','medsi_logo.png'))
    encrypted_user_info = AESCrypt.encrypt(user.id.to_s, ENV["KEY_PASSWORD_AESCRYPT"])
    @url = reset_doctor_password_url + "/?user="+ URI.encode_www_form_component(encrypted_user_info)
    @user_required_name = user.person.full_name
    mail to: user.email, subject: "Solicitud password"
  end

  def new_user_doctor(user, abbreviation_dr)
    attachments.inline['logo.png'] = File.read(Rails.root.join('app','assets','images','medsi_logo.png'))
    encrypted_user_info = AESCrypt.encrypt(user.id.to_s, ENV["KEY_PASSWORD_AESCRYPT"])
    @url = reset_doctor_password_url + "/?user="+ URI.encode_www_form_component(encrypted_user_info)
    @user_required_name = user.user_name
    @doctor_full_name = abbreviation_dr+" "+user.person.full_name
    mail to: user.email, subject: "Registro de contraseña"
  end

  def new_user_patient_password(user)
    attachments.inline['logo.png'] = File.read(Rails.root.join('app','assets','images','medsi_logo.png'))
    encrypted_user_info = AESCrypt.encrypt(user.id.to_s, ENV["KEY_PASSWORD_AESCRYPT"])
    @url = reset_doctor_password_url + "/?user="+ URI.encode_www_form_component(encrypted_user_info)
    @user_required_name = user.person.full_name
    mail to: user.email, subject: "Solicitud password"
  end
end
