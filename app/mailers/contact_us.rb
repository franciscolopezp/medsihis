class ContactUs < ApplicationMailer
  def contact(data)
    @data = data
    send_to = ENV['TO_CONTACT_US'].split('|')
    mail to: send_to, subject: 'Nuevo mensaje de '+data['name']
  end
end
