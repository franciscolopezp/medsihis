class NewsController < ApplicationController
  let :all, :all

  before_action :set_news_item, only: [:show]

  def show

  end

  def index
    @news = NewsItem.where('status = ?',NewsItem::ACTIVE).order('updated_at DESC')
  end
  private
  def set_news_item
    @news_item = NewsItem.find(params[:id])
  end

end