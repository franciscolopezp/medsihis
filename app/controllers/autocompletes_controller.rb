class AutocompletesController < ApplicationController
  include AutocompletesHelper
  def medicaments_pharmacy_items
    to_search = params[:term]
    render :json => search_medicaments_pharmacy_items(to_search)
  end

  def hospital_services_items
    to_search = params[:term]
    render :json => search_hospital_services(to_search)
  end

  def cie10
    to_search = params[:term]
    render json: search_diseases(to_search)
  end
end