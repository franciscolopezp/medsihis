class Patient::AppointmentsController < ApplicationController
  let :PATIENT, :all
  include Doctor::ActivitiesHelper
  include Patient::AppointmentsHelper

  def index
    user_patient = UserPatient.find_by(user_id: session[:user_id])
    respond_to do |format|
      format.html
      format.json { render json: AppointmentsDatatable.new(view_context,user_patient)}
    end
  end


  def doctors
    respond_to do |format|
      format.html
      format.json { render json: PatientDoctorsDatatable.new(view_context)}
    end
  end

  def new
    @user_patient = UserPatient.find_by(user_id: session[:user_id])
  end

  def activity_params
    params.require(:activity).permit(:name, :details, :start_date, :end_date, :activity_type_id, :all_day, :activity_status_id,:doctor_id)
  end

  def create

    patient = UserPatient.find_by(user_id: session[:user_id])
    doctor = Doctor.find(params[:activity][:doctor_id])
    @activity = Activity.new(activity_params)

    res = exist_rank_date(@activity.start_date,@activity.end_date,doctor,0)
    if res[:result]

      respond_to do |format|
        format.html { render json: {:done => false, :message => 'Por favor verifique la disponibilidad nuevamente.', :title => 'El horario ya no está disponible' , :type => 'error'}}
      end

    else

      if session[:role] == 'patient'
        @activity.owner_type = 'Patient'
        @activity.owner_id = patient.id
      end

      data = {}
      if @activity.save
        offices = get_offices params
        Activity.update(@activity.id, :offices => offices, :user_patients => [patient])
        data = json_from_model @activity
      end

      @data = { :type => 'new_activity', :activity => data, :in_w_room => false , :is_new => true}

      notification_private_web doctor.user.user_name, @data
      message_doctor_app = message_create_appointment_app @activity
      notification_app doctor.user, message_doctor_app

      render :json => {done: true}, layout: false
    end
  end

  def check_availability
    response = check_availability_office

    respond_to do |format|
      format.html { render json: response}
    end
  end

  def load_doctor_schedule
    doctor = Doctor.find(params[:doctor_id]);
    Time.zone = doctor.time_zone.rails_name

    data = availability_medical_consultations params

    respond_to do |format|
      format.html { render json: data}
    end


  end







end