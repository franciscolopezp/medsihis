class Patient::ExpedientsController < ApplicationController
  let :PATIENT, :all
  let :DOCTOR, :update

  def index

  end


  def link_medical_expedient(expedient,health_history_id)
    response = Hash.new
    expedient_request = ExpedientRequest.new
    expedient_request.medical_expedient_id = expedient.id
    expedient_request.status = ExpedientRequest::PENDING
    expedient_request.health_history_id = health_history_id
    expedient_request.save

    response['exist'] = false
    response['done'] = true
    response['title'] = 'Solicitud de vinculación enviada'
    response['message'] = 'La solicitud ha sido enviada. Al momento de ser aprobada por el(la) '+expedient.doctor.full_name_title+' le notificaremos vía correo electrónico y podrá visualzar el expediente en esta pantalla '
    response['type'] = 'success'
    response
  end

  def valid_expedient(expedient,code) #revisa que el registro no sea nulo
    response = Hash.new
    if expedient != nil
      response['exist'] = true
      response['patient_name'] = expedient.patient.full_name
    else
      response['exist'] = false
      response['done'] = false
      response['title'] = 'No se puede vincular el expediente'
      response['message'] = 'No existe el expediente con código de vinculación: ' + code
      response['type'] = 'error'
    end
    response
  end

  def linked_expedient(patient,expedient) #retorna un mensaje personalizado para cada estatus de la solicitud si esta existe
    response = Hash.new
    response['linked'] = false
    response['exist'] = false
    response['done'] = false
    response['title'] = 'No se puede vincular el expediente'
    response['message'] = 'El expediente de '+expedient.patient.full_name
    response['type'] = 'error'

    request = ExpedientRequest.by_patient(patient.id).where(:medical_expedient_id => expedient.id).first
    if request != nil
      case request.status
        when ExpedientRequest::PENDING
          response['linked'] = true
          response['message'] << ' está en espera de aprobación.'
        when ExpedientRequest::APPROVED
          response['linked'] = true
          response['message'] << ' ha sido vinculado previamente a su cuenta.'
        when ExpedientRequest::REJECTED
          response['linked'] = true
          response['message'] << ' no ha sido vinculado a su cuenta.'
      end
    end

    response
  end

  def create
    response = Hash.new
    code = params[:code]
    force = params[:force]
    health_history_id = params[:health_history_id]
    patient = current_user.user_patient
    expedient = MedicalExpedient.find_by(code: code)

    if force == 'true'
      prev_linked = linked_expedient(patient,expedient)
      if prev_linked['linked']
        response = prev_linked
      else
        response = link_medical_expedient(expedient,health_history_id)
      end
    else
      response = valid_expedient(expedient,code)
    end
    respond_to do |format|
      format.html { render json: response}
    end

  end

  def update
    response = Hash.new
    response['done'] = true
    response['title'] = 'Solicitud atendida'
    response['message'] = ''
    response['type'] = 'success'

    if params[:new_status].to_i == ExpedientRequest::APPROVED
      response['message'] = 'Se ha aprobado la vinculación del expediente.'
    else
      response['message'] = 'Se ha denegado la vinculación del expediente.'
    end

    request = ExpedientRequest.find(params[:id])
    request.update({
        status:params[:new_status]
                   })

    if params[:new_status].to_i == ExpedientRequest::APPROVED
      ExpedientRequestResponse.approve(request.health_history.user_patient,request.medical_expedient).deliver_now
    end

    respond_to do |format|
      format.html { render json: response}
    end
  end

  def health_history

    if params[:operation] == 'add'
      history = HealthHistory.new
      history.name = params[:name]
      history.user_patient = current_user.user_patient
      history.save
    elsif params[:operation] == 'update'
      history = HealthHistory.find(params[:id])
      history.update({:name => params[:name]})
    end


    respond_to do |format|
      format.html { render json: {:done => true}}
    end


  end


  def default_patient_data

    ExpedientRequest.where(:health_history_id =>params[:health_history_id]).update_all(:is_default => false)
    request = ExpedientRequest.find(params[:request_id])
    request.update({is_default: true})

    respond_to do |format|
      format.html { render json: {:done => true}}
    end

  end

end