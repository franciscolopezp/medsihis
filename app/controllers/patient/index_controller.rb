class Patient::IndexController < ApplicationController
  let :PATIENT, :all
  include Patient::IndexHelper
  before_action :validate_user_patient, except: [:update_terms_conditions, :accept_terms]

  def index

  end

  def update_terms_conditions
    patient = current_user.user_patient
    if patient.update(accept_term_conditions: true, is_active: true)
      render :json => {result: true}
    else
      render :json => {result: false}
    end
  end

  def accept_terms
    render 'patient/index/terms_and_conditions', layout: false
  end
end
