class ApplicationController < ActionController::Base
  include CanCan::ControllerAdditions
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_time_zone
  include SessionsHelper
  include NotificationHelper
  include DateHelper
  layout :get_layout

  def unauthorized_access_redirection_path
    login_path
  end

  def set_time_zone
    Time.zone = session[:time_zone]
  end

  def unauthorized_view_render
    render 'errors/401'
  end

  private
  def get_layout
=begin
    my_class_name = self.class.name
    if my_class_name.index("::").nil? then
      'guest'
    else
      my_class_name.split("::").first.downcase
    end
=end
    if current_user != nil
          return "admin"
    else
      return "guest"
    end


  end
end
