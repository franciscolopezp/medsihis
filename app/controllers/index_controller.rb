class IndexController < ApplicationController
  skip_before_filter :verify_authenticity_token
  let :all, :all
  def index
    @doctors = IndexComment.where('p_type = ? AND active = ?',IndexComment::DOCTOR,true)
    @patients = IndexComment.where('p_type = ? AND active = ?',IndexComment::PATIENT,true)
    @show_banner = true
    @view = params[:view].present? ? params[:view] : 'features_office'
    if !ClinicInfo.first.show_index
      redirect_to login_path
    end
  end

  def register_user

    data = Hash.new
    data['name'] = params[:name]
    data['last_name'] = params[:last_name]
    data['phone'] = params[:phone]
    data['email'] = params[:email]
    data['specialty'] = Specialty.find(params[:specialty]).name
    data['city'] = City.find(params[:city]).format_city_state_and_country
    data['comments'] = params[:comments]

    RegisterDoctor.register(data).deliver_now
    RegisterDoctorStaff.register(data).deliver_now

    respond_to do |format|
      format.html { render json: {:done => true, :message => ''}}
    end

  end

  def register_user_patient
    patient = UserPatient.new(user_patient_params)
    patient.premium_account = 0
    if patient.save
      RegisterPatient.register(patient).deliver_now
      render :json => {result: true}
    else
      render :json => {result: false, errors: patient.errors.full_messages}
    end
  end

  def check_user
    if User.exists?(user_name: params[:user_name])
      render :json => {result: true, message: "Ya existe el usuario #{params[:user_name]} registrado, le sugerimos intentar recuperar su usuario y contraseña"}
    elsif User.exists?(email: params[:email])
      render :json => {result: true, message: "Ya existe el correo #{params[:email]} registrado, le sugerimos intentar recuperar su usuario y contraseña"}
    else
      render :json => {result: false}
    end
  end

  def medical_directory
    @show_banner = false
    name = ''
    if !params[:name].nil?
      name = params[:name]
    end
    @name = name
    where_query = 'CONCAT(doctors.name, " ", doctors.last_name) like \'%'+name+'%\''


    if !params[:city].nil? && params[:city] != ''
      @city = City.find(params[:city])
      where_query << ' AND doctors.city_id = '+params[:city]
    end

    @specialties = []
    current_specialties =  IdentityCard.joins(:specialty).joins(:doctor).where(where_query).order('specialties.name ASC').group(:specialty_id).all

    current_specialties.each do |i|
      @specialties.push({
                            'specialty_id' => i.specialty_id,
                            'name'=>i.specialty.name,
                            'count' => IdentityCard.joins(:doctor).where(where_query + ' AND specialty_id = ?',i.specialty_id).count
                        })
    end
  end

  def search_results

    where_query = ''

    if !params[:specialty_id].nil? && params[:specialty_id] != ''
      where_query = 'identity_cards.specialty_id = ' + params[:specialty_id]
    end

    if !params[:name].nil? && params[:name] != ''
      where_query << ' AND CONCAT(doctors.name, " ", doctors.last_name) like \'%'+params[:name]+'%\' '
    end
    if !params[:city_id].nil? && params[:city_id] != ''
      where_query << ' AND doctors.city_id = ' + params[:city_id]
    end

    doctors = Doctor.joins(:identity_cards)
                  .where(where_query)
                  .where("searcher_visible = 1")
                  .order('doctors.name ASC')

    data = []
    doctors.each do |d|

      offices = Office.active_offices.where('doctor_id = ?',d.id)
      offices_data = []

      offices.each do |o|
        offices_data.push(
            {
                'id' => o.id,
                'hospital' => o.hospital.name,
                'name' => o.name,
                'address' => o.address,
                'city' => o.city.format_city_state_and_country,
                'telephone' => o.telephone,
                'lat' => o.lat,
                'lng' => o.lng
            }
        )
      end

      doctor_info = {
          'doctor' => d,
          'city' => d.city.format_city_state_and_country,
          'identity_card' => d.identity_cards.where('specialty_id = ?',params[:specialty_id]).all.first,
          'offices' =>offices_data
      }
      data.push(doctor_info)
    end

    respond_to do |format|
      format.html { render json: data}
    end

  end

  def get_doctor_info
    doctor  = Doctor.find(params[:id])

    specialties = []
    doctor.identity_cards.each do |ic|
      specialties.push({
                           'specialty' => ic.specialty.name,
                           'identity' => ic.identity,
                           'institute' => ic.institute
                       })
    end

    offices = []
    doctor.offices.active_offices.each do |o|

      times = []
      o.office_times.each do |ot|
        times.push({
                       'day' => Day.week_days[ot.day],
                       'start' => ot.start.strftime("%H:%M"),
                       'end' => ot.end.strftime("%H:%M")
                   })
      end

      offices.push({
                       'name' => o.name,
                       'hospital' => o.hospital.name,
                       'address' => o.address,
                       'city' => o.city.format_city_state_and_country,
                       'telephone' => o.telephone,
                       'times' => times
                   })
    end


    response = {
        'doctor' => doctor,
        'identity_cards' => specialties,
        'offices' => offices
    }

    respond_to do |format|
      format.html { render :json => response}
    end
  end

  def submit_contact

    data = Hash.new
    data['name'] = params[:name]
    data['email'] = params[:email]
    data['message'] = params[:message]
    data['phone'] = params[:phone]

    ContactUs.contact(data).deliver_now

    respond_to do |format|
      format.html { redirect_to index_path+'#support', notice: 'Hemos recibido su mensaje, pronto nos pondremos en contacto con usted. Gracias por comunicarse con nosotros.' }
    end

  end


  def medsi_pdf
    pdf_filename = File.join(Rails.root, 'pdf/medsi_info.pdf')
    send_file(pdf_filename, :filename => 'medsi_information.pdf', :disposition => 'inline', :type => 'application/pdf')
  end

  def aviso_de_privacidad
    pdf_filename = File.join(Rails.root, 'pdf/AvisoPrivacidad.pdf')
    send_file(pdf_filename, :filename => 'medsi_information.pdf', :disposition => 'inline', :type => 'application/pdf')
  end

  def picture_doctor
    user = User.find(params[:user_id])
    doctor = user.doctor
    if doctor.picture.nil?
      root = Rails.root.join("app","assets","images","profile_doctor.jpg")
    else
      root = doctor.picture
    end
    send_file root,  :type => 'image/jpeg', :disposition => 'inline'
  end

  def picture_patient
    patient = Patient.find(params[:patient_id])
    if patient.picture.nil?
      root = Rails.root.join("app","assets","images","profile_doctor.jpg")
    else
      root = patient.picture
    end
    send_file root,  :type => 'image/jpeg', :disposition => 'inline'
  end

  def new_chat
    @email = params[:email]
    @name = params[:name]
    @channel = params[:channel]
    PrivatePub.publish_to "/messages/public", :type => 'new_chat',  :channel => @channel, :email => @email, :name => @name, :role => params[:role]
    render layout: false
  end

  def load_features
    render params[:view], layout: false
  end

  def send_message
    channel = "/messages/private/#{params[:channel]}"
    PrivatePub.publish_to channel, :message => params[:message], :sender => params[:sender]
    render :json => {done: true}, layout: false
  end


  private
  def user_patient_params
    params.require(:patient).permit(:name,:last_name, :birth_date, :phone, :city_id, :gender_id,
                                    user_attributes:[:user_name, :password, :email])

  end

end
