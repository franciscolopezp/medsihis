class Doctor::ExpMedicalHistoriesController < ApplicationController


  def update
    mhi = MedicalHistory.find(params[:id])
    updated = mhi.update(medical_history_params)

    if params[:routine_medicaments].present?
      medicines = Medicament.where('id IN ('+params[:routine_medicaments]+')')
      updated = mhi.update(medicaments:medicines)
    else
      updated = mhi.update(medicaments:[])
    end

    render json: {:done => updated}
  end

  private
  def medical_history_params
    params.require(:medical_history).permit(
        :surgeries, :transfusions,
        :allergy_medicament,
        :allergy_medicament_description, :other_allergies,
        :recent_diseases,
        :other_medicines,
        no_pathological_attributes:[
            :id,
            :smoking,
            :smoking_description,
            :alcoholism,
            :alcoholism_description,
            :play_sport,
            :play_sport_description,
            :annotations,
            :marital_status_id,
            :religion,
            :scholarship,
            :drugs,
            :drugs_description
        ],
        family_disease_attributes:[
            :id,
            :diabetes,
            :diabetes_description,
            :overweight,
            :overweight_description,
            :hypertension,
            :hypertension_description,
            :asthma,
            :asthma_description,
            :cancer,
            :cancer_description,
            :annotations,
            :other_diseases,
            :psychiatric_diseases,
            :psychiatric_diseases_description,
            :neurological_diseases,
            :neurological_diseases_description,
            :cardiovascular_diseases,
            :cardiovascular_diseases_description,
            :bronchopulmonary_diseases,
            :bronchopulmonary_diseases_description,
            :thyroid_diseases,
            :thyroid_diseases_description,
            :kidney_diseases,
            :kidney_diseases_description
        ]
    )
  end
end