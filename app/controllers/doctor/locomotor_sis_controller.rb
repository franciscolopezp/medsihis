class Doctor::LocomotorSisController < ApplicationController

  before_action :set_locomotor_si, only: [:update]

  def index
  end

  def create
    @locomotor = LocomotorSi.new(locomotor_si_params)
    respond_to do |format|
      if @locomotor.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @locomotor.id}}
      else
        format.html { render json: {:done => false, :errors => @locomotor.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @locomotor.update(locomotor_si_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @locomotor.errors.full_messages}}
      end
    end
  end

  private
  def set_locomotor_si
    @locomotor = LocomotorSi.find(params[:id])
  end
  def locomotor_si_params
    params.require(:locomotor_si).permit(:medical_history_id, :mialgias, :artralgias, :articular_rigideces, :inflammations)
  end
end