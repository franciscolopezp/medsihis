class Doctor::CardiovascularApsController < ApplicationController

  before_action :set_cardiovascular_ap, only: [:update]

  def index
  end

  def create
    @cardio = CardiovascularAp.new(cardiovascular_ap_params)
    respond_to do |format|
      if @cardio.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @cardio.id}}
      else
        format.html { render json: {:done => false, :errors => @cardio.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @cardio.update(cardiovascular_ap_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @cardio.errors.full_messages}}
      end
    end
  end

  private
  def set_cardiovascular_ap
    @cardio = CardiovascularAp.find(params[:id])
  end
  def cardiovascular_ap_params
    params.require(:cardiovascular_ap).permit(:description, :chest_pain,:edema,:dyspnoea, :paroxysmal_nocturnal_dyspnea, :orthopnea, :palpitations, :syncope_presyncope, :peripheral_vascular, :medical_history_id)
  end
end