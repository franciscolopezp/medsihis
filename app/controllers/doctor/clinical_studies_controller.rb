class Doctor::ClinicalStudiesController < ApplicationController

  before_action :check_lab, except: [:get_by_category,:get_by_laboratory,:get_template, :add_indication, :delete_indication]
  before_action :set_clinical_study, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Análisis Clínico".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    @lab_id = params[:lab_id] if params[:lab_id].present?
    respond_to do |format|
      format.html
      format.json {
        render json: ClinicalStudiesDatatable.new(view_context,@lab_id,session[:doctor_id])
      }
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Análisis Clínico".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Análisis Clínico".
  def new
    @lab_id = params[:lab_id] if params[:lab_id].present?
    @lab = Laboratory.find(@lab_id)
    @clinical_study = ClinicalStudy.new
    @study_types = ClinicalStudyType.where('laboratory_id = ?',params[:lab_id]).all
    @text_button = "Guardar"
  end

  # Consulta un registro de tipo "Análisis Clínico" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Análisis Clínico" para poder ser editado.
  def edit
    @lab_id = @clinical_study.clinical_study_type.laboratory_id
    @lab = Laboratory.find(@lab_id)
    @study_types = ClinicalStudyType.where('laboratory_id = ?',@lab_id).all
    @text_button = "Guardar"
  end

  # Genera un registro de tipo "Análisis Clínico" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Análisis Clínico" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Análisis Clínico".
  def create
    @text_button = "Guardar"
    @clinical_study = ClinicalStudy.new(clinical_study_params)

    respond_to do |format|
      if @clinical_study.save
        if params[:has_template].present? && params[:has_template].to_i == 1
          StudyReportTemplate.create(clinical_study_id: @clinical_study.id, template: params[:template])
        end

        format.html { redirect_to doctor_clinical_studies_path+'?lab_id='+params[:lab_id].to_s, notice: 'El estudio clinico fue agregada con exito'}
        format.json { render show, status: :ok, location: @clinical_study}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  # Actualiza un registro de tipo "Análisis Clínico" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Análisis Clínico" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Análisis Clínico".
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @clinical_study.update(clinical_study_params)
        if params[:has_template].present? && params[:has_template].to_i == 1
          template = @clinical_study.study_report_template
          if template.nil?
            StudyReportTemplate.create(clinical_study_id: @clinical_study.id, template: params[:template])
          else
            template.update({template:params[:template]})
          end
        end
        format.html { redirect_to doctor_clinical_studies_path+'?lab_id='+params[:lab_id].to_s, notice: 'Estudio clinico actualizada'}
        format.json { render show, status: :ok,location: @clinical_study}
      else
        format.html { render :edit}
        format.json { render json: 'error estudio clinico', status: :unprocessable_entity}
      end
    end
  end

  # Elimina un registro de tipo "Análisis Clínico" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Análisis Clínico".
  def destroy
    begin
      @clinical_study.destroy
      result = {:done => true}
    rescue ActiveRecord::StatementInvalid
      result = {:done => false, :error => 'No se puede eliminar el registro',  :message => 'Hay referencias a él en el sistema.'}
    rescue
      #Only comes in here if nothing else catches the error
    end

    respond_to do |format|
      format.html { redirect_to admin_laboratories_url, notice: '' }
      format.json { render json: result}
    end
  end

  def get_by_category
    result = ClinicalStudy.where('clinical_study_type_id = ?',params[:category_id])
    respond_to do |format|
      format.html { render json: result}
    end
  end

  def get_by_laboratory
    result = ClinicalStudy.joins(:clinical_study_type => :laboratory)
                 .where(:clinical_study_types => {laboratory_id: params[:laboratory_id]})
    respond_to do |format|
      format.html { render json: result}
    end
  end

  def get_indications
    result = ClinicalStudy.find(params[:id]).indications
    respond_to do |format|
      format.html { render json: result}
    end
  end

  def get_template
    study = ClinicalStudy.find(params[:study_id])
    template = ''

    if !study.study_report_template.nil?
      template = study.study_report_template.template
    end
    respond_to do |format|
      format.html { render :text => template}
    end
  end

  def add_indication
    study = ClinicalStudy.find(params[:study_id])
    indications = study.indications
    resp = {:done => true}

    if indications.where('id = ?',params[:indication_id]).first.nil?
      indication = Indication.find(params[:indication_id])
      indications.push(indication)
      study.update({indications:indications})
    else
      resp = {
          :done => false,
          :error => 'No se pudo completar la operación',
          :message => 'Ya existe la asociación de la indicación con el estudio'
      }
    end

    respond_to do |format|
      format.html { render json: resp}
    end
  end

  def delete_indication
    study = ClinicalStudy.find(params[:study_id])
    indication = study.indications.find(params[:indication_id])

    study.indications.delete(indication)

    respond_to do |format|
      format.html { render json: {:done => true}}
    end

  end

  private
  # Inicializa un objeto de tipo "Análisis Clínico" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Análisis Clínico".
  # @return [ClinicalStudy] Objeto de tipo "Análisis Clínico"
  def set_clinical_study
    @clinical_study = ClinicalStudy.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Análisis Clínico" sean: nombre y un array de ids de indicaciones.
  def clinical_study_params
    params.require(:clinical_study).permit(:name,:clinical_study_type_id,:price)
  end

  def check_lab

    begin

      if params[:lab_id].present?
        lab = Laboratory.find(params[:lab_id])
      elsif params[:id].present?
        group = ClinicalStudy.find(params[:id]).clinical_study_type
        lab = group.laboratory
      end

      doctor = lab.doctors.find(session[:doctor_id])
      if !lab.is_custom && doctor != nil
        redirect_to doctor_laboratories_url
      end


    rescue ActiveRecord::RecordNotFound
      redirect_to doctor_laboratories_url
    end
  end
end