class Doctor::GeneralFiscalInfosController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_general_fiscal_info, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: GeneralFiscalInfosDatatable.new(view_context)}
    end
  end

  def show
  end

  def new
    @text_button = "Guardar"
    @info = GeneralFiscalInfo.new
    @info.build_city
  end

  def edit
    @text_button = "Guardar"
  end

  def create
    if params[:general_fiscal_info][:int_number] == ""
      params[:general_fiscal_info][:int_number] = "-"
    end
    @text_button = "Guardar"
    @info = GeneralFiscalInfo.new(general_fiscal_info_params)
    @info.user = current_user

    respond_to do |format|
      if @info.save
        format.html  { redirect_to doctor_general_fiscal_infos_path}
        format.json { render :show, status: :created, location: @info  }
      else
        format.html { render :new}
        format.json {render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def update
    if params[:general_fiscal_info][:int_number] == ""
      params[:general_fiscal_info][:int_number] = "-"
    end
    @text_button = "Guardar"
    respond_to do |format|
      if @info.update(general_fiscal_info_params)
        format.html { redirect_to doctor_general_fiscal_infos_path}
        format.json { render show, status: :ok, location: @info}
      else
        format.html { render :edit}
        format.json { render json: 'general fiscal error', status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @info.destroy
    respond_to do |format|
      format.html { redirect_to doctor_general_fiscal_infos_path}
      format.json { head :no_content}
    end
  end

  def get_fiscal_data
    data = GeneralFiscalInfo.find(params[:id])
    data_array = []
    data_array.push({
                        business_name: data.business_name,
                        rfc: data.rfc,
                        address: data.address,
                        locality: data.locality,
                        ext_number: data.ext_number,
                        int_number: data.int_number,
                        zip: data.zip,
                        suburb: data.suburb,
                        city: data.city.name + "," + data.city.state.name + "," +data.city.state.country.name
                    })
    render :json => {:data =>data_array}, layout: false
  end

  private
  def set_general_fiscal_info
    @info = GeneralFiscalInfo.find(params[:id])
  end

  def general_fiscal_info_params
    params.require(:general_fiscal_info).permit(:business_name, :rfc, :address, :locality, :ext_number,:int_number,:zip,:suburb,:city_id,:person_type)
  end

end