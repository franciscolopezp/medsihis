class Doctor::MetabolicSisController < ApplicationController

  before_action :set_metabolic_si, only: [:update]

  def index
  end

  def create
    @metabolic = MetabolicSi.new(metabolic_si_params)
    respond_to do |format|
      if @metabolic.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @metabolic.id}}
      else
        format.html { render json: {:done => false, :errors => @metabolic.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @metabolic.update(metabolic_si_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @metabolic.errors.full_messages}}
      end
    end
  end

  private
  def set_metabolic_si
    @metabolic = MetabolicSi.find(params[:id])
  end
  def metabolic_si_params
    params.require(:metabolic_si).permit(:description, :weight,:size_body_shape,:asthenia,:weakness,:appetite,:thirst,:hot_cold,:sex,:galactorrea,:age_puberty,:medical_history_id)
  end
end