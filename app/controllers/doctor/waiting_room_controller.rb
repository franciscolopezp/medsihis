class Doctor::WaitingRoomController < ApplicationController

  def index

    if session[:role] == 'shared_assistant'
      @assistant = Assistant.find(session[:assistant_id])
      render layout: 'sharedassistant'
    end

  end
end