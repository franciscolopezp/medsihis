class Doctor::UltrasoundsController < ApplicationController

  before_action :set_ultrasound, only: [:update, :destroy]

  def index

  end

  def create
    ultrasound = Ultrasound.create(pregnancy_id:params[:pregnancy_id], date:params[:date], weeks:params[:weeks])
    respond_to do |format|
      msg = {:done => true , :message => 'Se agregó el ultrasonido', :rec => ultrasound}
      format.html { render :json => msg  }
    end
  end

  def update
    @ultrasound.update(date:params[:date], weeks:params[:weeks])
    render :json => {:done => true, :message => 'Ultrasonido actualizado'}
  end

  def destroy
    @ultrasound.destroy
    render :json => {:done => true, :message => 'Ultrasonido eliminado'}
  end

  private
  def set_ultrasound
    @ultrasound = Ultrasound.find(params[:id]);
  end

end