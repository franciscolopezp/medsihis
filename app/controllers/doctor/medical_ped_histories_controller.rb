class Doctor::MedicalPedHistoriesController < ApplicationController

  include Doctor::MedicalPedHistoriesHelper

  def index
    @medical_history = MedicalHistory.find(params[:medical_history_id])

    generate_medical_ped_history @medical_history
    render :layout => false
  end

  def graph_percentile_weight
    @medical_expedient = MedicalExpedient.find(params[:expedient_id])
    #result = select_point_weight
    result = generate_date_graph TypePercentile::WEIGHT,'Peso: ', params[:type_data]
    render json: result, layout: false
  end

  def graph_percentile_head_circumference
    @medical_expedient = MedicalExpedient.find(params[:expedient_id])
    #result = select_point_weight
    result = generate_date_graph TypePercentile::HEAD_CIRCUMFERENCE,'Circu.: ', TypePercentile::DATA_PERCENTILE_OMS
    render json: result, layout: false
  end

  def graph_percentile_imc
    @medical_expedient = MedicalExpedient.find(params[:expedient_id])
    #result = select_point_weight
    result = generate_date_graph TypePercentile::IMC,'IMC.: ', TypePercentile::DATA_PERCENTILE_CDC
    render json: result, layout: false
  end

  def graph_percentile_size_length
    @medical_expedient = MedicalExpedient.find(params[:expedient_id])
    result = generate_date_graph TypePercentile::SIZE_LENGTH,'Altura: ', params[:type_data]
    render json: result, layout: false
  end

end
