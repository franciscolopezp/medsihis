class Doctor::ChildNonPathologicalInfosController < ApplicationController

  before_action :set_child_non_pathological_info, only: [:update]

  def create
    @child_non_pathological_info = ChildNonPathologicalInfo.new(child_non_pathological_info_params)
    if @child_non_pathological_info.save
      respond_to do |format|
        format.json { render json: {:done => true, :data =>  @child_non_pathological_info}}
      end
    else
      respond_to do |format|
        format.json { render json: {:errors => @child_non_pathological_info.errors}}
      end
    end
  end

  def update
    if @child_non_pathological_info.update(child_non_pathological_info_params)
      respond_to do |format|
        format.json { render json: {:done => true, :data =>  @child_non_pathological_info}}
      end
    else
      respond_to do |format|
        format.json { render json: {:errors => @child_non_pathological_info.errors}}
      end
    end
  end

  private
  def set_child_non_pathological_info
    @child_non_pathological_info = ChildNonPathologicalInfo.find(params[:id])
  end

  def child_non_pathological_info_params
    params.require(:child_non_pathological_info).permit(:child_academic_grade, :number_people, :house_type)
  end

end