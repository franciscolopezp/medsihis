class Doctor::ConsultationTypesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]

  def index
    respond_to do |format|
      format.html
      format.json { render json: ConsultationTypesDatatable.new(view_context)}
    end
  end

  def create
      consultation_type = ConsultationType.new
      consultation_type.name = params[:name]
      consultation_type.is_local = true
      consultation_type.save
      consultation_cost = ConsultationCost.new
      consultation_cost.is_editable_price = params[:is_editable].to_i
      consultation_cost.consultation_type_id = consultation_type.id
      consultation_cost.cost = params[:cost]
      consultation_cost.save


    respond_to do |format|
      format.html { render json: {:done => true, :message => 'Registro creado correctamente'}}
    end
  end

  def update
    consultation_type = ConsultationType.find(params[:id])
    consultation_type.update({name: params[:name]})
    consultation_type.consultation_costs.first.update({cost: params[:cost]})
    respond_to do |format|
      format.html { render json: {:done => true, :message => 'Registro actualizado correctamente'}}
    end
  end

  def update_consultation_cost
    data = params[:data]

    data.each do |key,d|
      cost = ConsultationCost.where('office_id = ? AND consultation_type_id = ?',d['office'].to_i,d['consult_type'].to_i).first
      if cost.nil?
        cost = ConsultationCost.new
        cost.consultation_type_id = d['consult_type']
        cost.office_id = d['office']
        cost.save
      end

      cost.update({cost:d['cost']})
    end

    respond_to do |format|
      format.html { render json: {:done => true, :message => 'Registros actualizados correctamente'}}
    end

  end

  def destroy
    consultation_type = ConsultationType.find(params[:id])
    consultation_type.consultation_costs.delete_all
    consultation_type.destroy
    respond_to do |format|
      format.html { render json: {:done => true, :message => 'Registro eliminado correctamente'}}
      format.json { head :no_content}
    end
  end

end
