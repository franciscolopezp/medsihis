class Doctor::ParentInfosController < ApplicationController

  before_action :set_parent_info, only: [:update,:destroy]


  def create
    @parent_info = ParentInfo.new(parent_info_params)
    if @parent_info.save
      respond_to do |format|
        format.json { render json: @parent_info}
      end
    else
      respond_to do |format|
        format.json { render json: {:errors => @parent_info.errors}}
      end
    end
  end

  def update
    if @parent_info.update(parent_info_params)
      respond_to do |format|
        format.json { render json: @parent_info}
      end
    else
      respond_to do |format|
        format.json { render json: {:errors => @parent_info.errors}}
      end
    end
  end

  def destroy
    @parent_info.destroy
    respond_to do |format|
      format.json { render json: {:success => true, :message => "Se eliminó el padre",:data => @parent_info}}
    end
  end

  private
  def set_parent_info
    @parent_info = ParentInfo.find(params[:id])
  end

  def parent_info_params
    params.require(:parent_info).permit(:first_name, :last_name, :birth_date,:blood_type_id,:academic_grade,:gender,:child_non_pathological_info_id)
  end

end