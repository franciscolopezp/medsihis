class Doctor::ClinicalStudyTypesController < ApplicationController

  before_action :check_lab, except: [:create, :get_by_lab]
  before_action :set_clinical_study_type, only: [:show, :edit, :update, :destroy]

  def index
    @lab_id = params[:lab_id]
    @lab = Laboratory.find(params[:lab_id]) if params[:lab_id].present?

    respond_to do |format|
      format.html
      format.json {
        render json: ClinicalStudyTypesDatatable.new(view_context,@lab_id,session[:doctor_id])
      }
    end
  end

  def new
    @clinical_study_type = ClinicalStudyType.new
    @clinical_study_type.laboratory_id = params[:lab_id]
    @lab_id = params[:lab_id]
    @text_button = "Guardar"
  end

  def edit
    @lab_id = @clinical_study_type.laboratory_id
    @text_button = "Guardar"
  end


  def create
    @text_button = "Guardar"
    @clinical_study_type = ClinicalStudyType.new(clinical_study_type_params)

    respond_to do |format|
      if @clinical_study_type.save
        format.html { redirect_to doctor_clinical_study_types_path+'?lab_id='+params[:clinical_study_type][:laboratory_id].to_s, notice: 'Categoría agregada correctamente'}
        format.json { render show, status: :ok, location: @clinical_study_type}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @clinical_study_type.update(clinical_study_type_params)
        format.html { redirect_to doctor_clinical_study_types_path+'?lab_id='+params[:clinical_study_type][:laboratory_id].to_s, notice: 'Categoría actualizada correctamente'}
        format.json { render show, status: :ok,location: @clinical_study_type}
      else
        format.html { render :edit}
        format.json { render json: 'error estudio clinico', status: :unprocessable_entity}
      end
    end
  end

  def destroy
    begin
      @clinical_study_type.destroy
      result = {:done => true}
    rescue ActiveRecord::StatementInvalid
      result = {:done => false, :error => 'No se puede eliminar el registro',  :message => 'Hay referencias a él en el sistema.'}
    rescue
      #Only comes in here if nothing else catches the error
    end

    respond_to do |format|
      format.html { redirect_to admin_laboratories_url, notice: '' }
      format.json { render json: result}
    end
  end

  def get_by_lab
    result = ClinicalStudyType.where('laboratory_id  = ?',params[:lab_id])
    respond_to do |format|
      format.html { render json: result }
    end
  end

  private

  def set_clinical_study_type
    @clinical_study_type = ClinicalStudyType.find(params[:id])
  end

  def clinical_study_type_params
    params.require(:clinical_study_type).permit(:name,:laboratory_id,:indication_ids => [])
  end

  def check_lab

    begin

      if params[:lab_id].present?
        lab = Laboratory.find(params[:lab_id])
      elsif params[:id].present?
        group = ClinicalStudyType.find(params[:id])
        lab = group.laboratory
      end

      doctor = lab.doctors.find(session[:doctor_id])
      if !lab.is_custom && doctor != nil
        redirect_to doctor_laboratories_url
      end


    rescue ActiveRecord::RecordNotFound
      redirect_to doctor_laboratories_url
    end
  end
end