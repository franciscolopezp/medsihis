class Doctor::GenitalApsController < ApplicationController

  before_action :set_genital_ap, only: [:update]

  def index
  end

  def create
    @genital = GenitalAp.new(genital_ap_params)
    respond_to do |format|
      if @genital.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @genital.id}}
      else
        format.html { render json: {:done => false, :errors => @genital.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @genital.update(genital_ap_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @genital.errors.full_messages}}
      end
    end
  end

  private
  def set_genital_ap
    @genital = GenitalAp.find(params[:id])
  end
  def genital_ap_params
    params.require(:genital_ap).permit(:description, :medical_history_id, :menarquia, :menstrual_rhythm, :hypermenorrea, :amenorrea, :metrorragia, :leucorrea, :dismenorrea, :dispareunia, :impotence, :libido)
  end
end

