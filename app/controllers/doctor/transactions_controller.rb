class Doctor::TransactionsController < ApplicationController

  include Doctor::ADD_FINKOK
  include Doctor::FINKOK

  def index
    respond_to do |format|
      format.html
      format.json { render json: TransactionsDatatable.new(view_context,session[:doctor_id])}
    end
  end
  def general_invoice

  end
  def getFiscalInfos
    fiscalinfos = []
    patient = Patient.find(params[:patient_id])
    patient.patient_fiscal_informations.each do |info|
      fiscalinfos.push({
                        :id => info.id,
                        :business_name => info.business_name,
                        :rfc => info.rfc,
                        :type => "patient"
                    })
    end



    GeneralFiscalInfo.all.each do |info|
      fiscalinfos.push({
                           :id => info.id,
                           :business_name => info.business_name,
                           :rfc => info.rfc,
                           :type => "doctor"
                       })
    end
    respond_to do |format|
      format.html { render json: fiscalinfos}
    end
  end
  def getDocFiscalInfo
    result = false
    docinfo = Doctor.find(session[:doctor_id])
    info = docinfo.fiscal_information
    doc_info = []
    if info.certificate_key.file != nil  && info.certificate_stamp.file != nil
      result = true
    end
    doc_info.push({
                        :business_name => info.bussiness_name,
                        :rfc => info.rfc,
                        :address => info.fiscal_address,
                        :suburb => info.suburb,
                        :locality => info.locality,
                        :ext_number => info.ext_number,
                        :int_number => info.int_number,
                        :zip => info.zip,
                        :city => info.city.name,
                        :state => info.city.state.name,
                        :country => info.city.state.country.name,
                        :serie => info.serie,
                        :folio => info.folio,
                        :iva => info.iva,
                        :isr => info.isr,
                        :iva_report => docinfo.iva_report,
                        :iva_report_included => docinfo.iva_report_included
                    })
    respond_to do |format|
      format.html { render json: {:done => result, :data => doc_info}}
    end
  end
  def setFiscalInfo
    fiscalinfo = []
    if params[:type] == "patient"
    info = PatientFiscalInformation.find(params[:fiscal_info_id])
      fiscalinfo.push({
                           :id => info.id,
                           :business_name => info.business_name,
                           :rfc => info.rfc,
                           :address => info.address,
                           :suburb => info.suburb,
                           :locality => info.locality,
                           :ext_number => info.ext_number,
                           :int_number => info.int_number,
                           :zip => info.zip,
                           :city => info.city.name,
                           :state => info.city.state.name,
                           :country => info.city.state.country.name,
                           :person_type => info.person_type
                       })
    else
      info = GeneralFiscalInfo.find(params[:fiscal_info_id])
      fiscalinfo.push({
                          :id => info.id,
                          :business_name => info.business_name,
                          :rfc => info.rfc,
                          :address => info.address,
                          :suburb => info.suburb,
                          :locality => info.locality,
                          :ext_number => info.ext_number,
                          :int_number => info.int_number,
                          :zip => info.zip,
                          :city => info.city.name,
                          :state => info.city.state.name,
                          :country => info.city.state.country.name,
                          :person_type => info.person_type
                      })
    end

    respond_to do |format|
      format.html { render json: fiscalinfo}
    end
  end
  def cancel_invoice
    transac = Transaction.find(params[:transac_id])
    invoice = transac.invoice
    doc = Doctor.find(session[:doctor_id])
    proveedor = Comprobante.new()
    pem = Dir.pwd + "/uploads/fiscal_information_" + doc.fiscal_information.id.to_s + "/certificate_key/key.pem"
    certificado =  %x'openssl x509 -inform DER -in #{doc.fiscal_information.certificate_stamp.current_path.to_s}'
    resultado =  proveedor.cancela invoice.UUID, doc.fiscal_information.rfc,pem, certificado.to_s
    if resultado[:message] == "1"
      invoice.update(xml: resultado[:data],cancel:1)
      if invoice.invoice_type != 3
        invoice.transactions.each { |x|
          transac = Transaction.find(x)
          transac.update(invoiced: 0)
        }
      end
      result_message = "Factura cancelada con exito"
      result_code = "1"
    else
      result_message = get_cancel_invoice_error resultado[:message]
      result_code = "0"
    end

    respond_to do |format|
      format.html { render json: {:message => result_message, :result => result_code} }
    end
  end
  def timbrar
    if session[:doctor_id] == nil
      doctor_id = params[:doctor_id]
    else
      doctor_id = session[:doctor_id]
    end
    doc = Doctor.find(doctor_id)
    if doc.fiscal_information.certificate_base64 != nil || doc.fiscal_information.certificate_base64 != ""
      certificadobase64 = doc.fiscal_information.certificate_base64
    else
      certificado =  %x'openssl x509 -inform DER -in #{doc.fiscal_information.certificate_stamp.current_path.to_s}'
      arrayc =  certificado.split("\n")
      arrayc.shift
      arrayc.pop
      certificadobase64 = arrayc.join.to_s #aqui ya esta chamaquiado el certificado :) voy por menos
      doc.fiscal_information.update(certificate_base64: certificadobase64)
    end

    if doc.fiscal_information.certificate_number == nil || doc.fiscal_information.certificate_number == ""
      #Obtiene el numero de serie del certificado y lo convierte al numero.
      certificado =  %x'openssl x509 -serial -noout -inform DER -in #{doc.fiscal_information.certificate_stamp.current_path.to_s}'
      arreglonumero = certificado.to_s.split('=')
      arreglosolonumero = arreglonumero[1].to_s.split(//)
      serialNumberCer = []
      (1..39).step(2) do |n|
        serialNumberCer.push(arreglosolonumero[n])
      end
      noCertificado = serialNumberCer.join.to_s
      doc.fiscal_information.update(certificate_number: noCertificado)
    else
      noCertificado = doc.fiscal_information.certificate_number
    end

    payment_way = PaymentWay.find(params[:payment_type])
    actual_time = Time.now
    next_folio = doc.fiscal_information.folio + 1
    b = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.send(:'cfdi:Comprobante',
               'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
               'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
               'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
               'TipoCambio'=>"0.00",
               'Moneda'=>"PESOS",
               'version'=>"3.2",
               'serie'=>doc.fiscal_information.serie,
               'folio'=>next_folio.to_s,
               'fecha'=>actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
               'sello'=>"",
               'formaDePago'=>payment_way.name,
               'noCertificado'=>noCertificado,
               'certificado'=>certificadobase64,
               'condicionesDePago'=>"Contado",
               'subTotal'=>params[:sub_total],
               'descuento'=>"0",
               'total'=>params[:amount],
               'metodoDePago'=>params[:payment_method],
               'tipoDeComprobante'=>"ingreso",
               'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
               'LugarExpedicion'=>params[:city] + ", " + params[:state],
               'NumCtaPago'=>params[:account_number]){
        xml.send(:'cfdi:Emisor','rfc'=>doc.fiscal_information.rfc, 'nombre'=>doc.fiscal_information.bussiness_name){
          xml.send(:'cfdi:DomicilioFiscal','calle'=>doc.fiscal_information.fiscal_address, 'noExterior'=>doc.fiscal_information.ext_number, 'noInterior'=>doc.fiscal_information.int_number, 'colonia'=>doc.fiscal_information.suburb, 'municipio'=>doc.fiscal_information.locality, 'estado'=>doc.fiscal_information.city.state.name, 'pais'=>doc.fiscal_information.city.state.country.name, 'codigoPostal'=>doc.fiscal_information.zip){
            xml.text('')
          }
          xml.send(:'cfdi:RegimenFiscal','Regimen'=>doc.fiscal_information.tax_regime)
        }
        xml.send(:'cfdi:Receptor','rfc'=>params[:rfc], 'nombre'=>params[:business_name] ){
          xml.send('cfdi:Domicilio','calle'=>params[:address],'codigoPostal'=>params[:zip], 'noExterior'=>params[:ext_number], 'noInterior'=>params[:int_number], 'colonia'=>params[:suburb],'estado'=>params[:state], 'municipio'=>params[:locality],'pais'=>params[:country] )
        }
        xml.send(:'cfdi:Conceptos'){
          xml.send(:'cfdi:Concepto','cantidad'=>"1.00", 'unidad'=>"No Aplica", 'noIdentificacion'=>"1", 'descripcion'=>params[:descripcion_concepto], 'valorUnitario'=>params[:sub_total], 'importe'=>params[:sub_total])
        }
        xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>params[:isr], 'totalImpuestosTrasladados'=>params[:importeisr]){
          xml.send(:'cfdi:Traslados'){
            xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>params[:iva],'importe'=>params[:importeiva])
          }
        }
        xml.send(:'cfdi:Complemento')


      }
    end

    dir = Rails.root.join('uploads',doc.fiscal_information.rfc)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    xml_name = 'xml_base.xml'
    File.open(dir.join(xml_name), 'wb') do |file|
      file.write(b.to_xml)
    end
    if params[:type] == "report"
      check_transac = CabinetReport.find(params[:transaction_id])
    else
      check_transac = MedicalConsultation.find(params[:transaction_id])
    end

    doctor = Doctor.find(doctor_id)
    xml_base_root = dir.join(xml_name)


    ruta_cadena_original = Dir.pwd + "/uploads/" + doc.fiscal_information.rfc + "/cadenaoriginal.txt"
    file_pem_root = Dir.pwd + "/uploads/fiscal_information_" + doc.fiscal_information.id.to_s + "/certificate_key/key.pem"
    %x' xsltproc cadenaoriginal_3_2.xslt #{xml_base_root} > #{ruta_cadena_original}'


    sello =  %x' openssl dgst -sha1 -sign #{file_pem_root} #{ruta_cadena_original} | openssl enc -base64'
    selloarreglo = sello.split("\n")
    sello = selloarreglo.join.to_s
    a = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.send(:'cfdi:Comprobante',
               'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
               'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
               'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
               'TipoCambio'=>"0.00",
               'Moneda'=>"PESOS",
               'version'=>"3.2",
               'serie'=>doc.fiscal_information.serie,
               'folio'=>next_folio.to_s,
               'fecha'=>actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
               'sello'=>sello,
               'formaDePago'=>payment_way.name,
               'noCertificado'=>noCertificado,
               'certificado'=>certificadobase64,
               'condicionesDePago'=>"Contado",
               'subTotal'=>params[:sub_total],
               'descuento'=>"0",
               'total'=>params[:amount],
               'metodoDePago'=>params[:payment_method],
               'tipoDeComprobante'=>"ingreso",
               'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
               'LugarExpedicion'=>params[:city] + ", " + params[:state],
               'NumCtaPago'=>params[:account_number]){
        xml.send(:'cfdi:Emisor','rfc'=>doc.fiscal_information.rfc, 'nombre'=>doc.fiscal_information.bussiness_name){
          xml.send(:'cfdi:DomicilioFiscal','calle'=>doc.fiscal_information.fiscal_address, 'noExterior'=>doc.fiscal_information.ext_number, 'noInterior'=>doc.fiscal_information.int_number, 'colonia'=>doc.fiscal_information.suburb, 'municipio'=>doc.fiscal_information.locality, 'estado'=>doc.fiscal_information.city.state.name, 'pais'=>doc.fiscal_information.city.state.country.name, 'codigoPostal'=>doc.fiscal_information.zip){
            xml.text('')
          }
          xml.send(:'cfdi:RegimenFiscal','Regimen'=>doc.fiscal_information.tax_regime)
        }
        xml.send(:'cfdi:Receptor','rfc'=>params[:rfc], 'nombre'=>params[:business_name] ){
          xml.send('cfdi:Domicilio','calle'=>params[:address],'codigoPostal'=>params[:zip],'noExterior'=>params[:ext_number], 'noInterior'=>params[:int_number], 'colonia'=>params[:suburb],'estado'=>params[:state], 'municipio'=>params[:locality],'pais'=>params[:country])
        }
        xml.send(:'cfdi:Conceptos'){
          xml.send(:'cfdi:Concepto','cantidad'=>"1.00", 'unidad'=>"No Aplica", 'noIdentificacion'=>"1", 'descripcion'=>params[:descripcion_concepto], 'valorUnitario'=>params[:sub_total], 'importe'=>params[:sub_total])
        }
        xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>params[:isr], 'totalImpuestosTrasladados'=>params[:importeisr]){
          xml.send(:'cfdi:Traslados'){
            xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>params[:iva],'importe'=>params[:importeiva])
          }
        }
        xml.send(:'cfdi:Complemento')


      }
    end
    dir = Rails.root.join('uploads',doc.fiscal_information.rfc)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    xml_name = 'xml_base.xml'
    File.open(dir.join(xml_name), 'wb') do |file|
      file.write(a.to_xml)
    end



       #timbra el xml

      # definir el entorno para decidir que webservice utilizar pruebas/produccion
      proveedor = Comprobante.new()
      xml = File.read(xml_base_root)
      timbrada = proveedor.timbra(xml)
      resultado = timbrada
      qr_code_local_route = "/uploads/" + doc.fiscal_information.rfc + "/qr"+actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S')+".png"

    if resultado[:message] == "1"
      if check_transac.invoice != nil && check_transac.invoiced == true
        @new_invoice = check_transac.invoice
        @new_invoice.update(xml_base: xml_base_root ,serie: doc.fiscal_information.serie, folio: next_folio,doctor_id:doctor.id,
                            certification_date: actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'), payment_condition: payment_way.name,
                            account_number: params[:account_number], subtotal: params[:sub_total], iva: params[:importeiva], isr: params[:importeisr], patient_fiscal_information_id: params[:patient_fiscal],payment_methods_name: params[:payment_method_name],invoice_type: 1,payment_way_id:payment_way.id)
        @concept = @new_invoice.invoice_concepts.last
        @concept.update(invoice_id: @new_invoice.id, quantity: 1,unit:"No aplica", description: params[:descripcion_concepto], unit_price: params[:sub_total], cost: params[:sub_total] )
      else
        @new_invoice = Invoice.new(xml_base: xml_base_root ,serie: doc.fiscal_information.serie, folio: next_folio,doctor_id: doctor.id,
                                   certification_date: actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'), payment_condition: payment_way.name,
                                   account_number: params[:account_number], subtotal: params[:sub_total], iva: params[:importeiva], isr: params[:importeisr], patient_fiscal_information_id: params[:patient_fiscal],payment_methods_name: params[:payment_method_name],invoice_type: 1,payment_way_id:payment_way.id)
        @new_invoice.save
        @concept = InvoiceConcept.new(invoice_id: @new_invoice.id, quantity: 1,unit:"No aplica", description: params[:descripcion_concepto], unit_price: params[:sub_total], cost: params[:sub_total] )
        @concept.save
        check_transac.update(invoice_id: @new_invoice.id)

        OpenInvoiceReceptor.new(business_name: params[:business_name], rfc:params[:rfc],address:params[:address], suburb:params[:suburb], no_int:params[:int_number], no_ext:params[:ext_number], zip: params[:zip],
                                                             locality: params[:locality], city:params[:city], country:params[:country], state: params[:state], invoice_id: @new_invoice.id ).save

        InvoiceEmisor.new(business_name: doc.fiscal_information.bussiness_name, rfc:doc.fiscal_information.rfc,address:doc.fiscal_information.fiscal_address, suburb:doc.fiscal_information.suburb, no_int:doc.fiscal_information.int_number, no_ext:doc.fiscal_information.ext_number, zip: doc.fiscal_information.zip,
                                locality: doc.fiscal_information.locality, city:doc.fiscal_information.city.name, country:doc.fiscal_information.city.state.country.name, state: doc.fiscal_information.city.state.name, invoice_id: @new_invoice.id,tax_regime:doc.fiscal_information.tax_regime ).save

      end
      cadena_original_text = ""
      File.open(ruta_cadena_original, "r") do |f|
        f.each_line do |line|
          cadena_original_text += line
        end
      end
      @new_invoice.update(original_string: ruta_cadena_original,original_string_text:cadena_original_text)
      @new_invoice.update(UUID: resultado[:uuid], sat_seal: resultado[:timbre], no_cert_sat: resultado[:no_certificado_sat],xml: resultado[:xml],transmitter_seal: sello,qr_code: qr_code_local_route)

      doc.fiscal_information.update(folio: next_folio)
      total_qr = params[:amount]
      total_qr = "%.6f" %total_qr
      total_qr = total_qr.rjust(17,'0')
      cadena_qr =  "?re="+doc.fiscal_information.rfc+"&rr="+params[:rfc]+"&tt="+total_qr.to_s+"&id="+resultado[:uuid]
      create_qr(cadena_qr,dir,"qr"+actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S')+".png")
      check_transac.update(invoiced: 1)
      #send_invoice_after_stamp(@new_invoice)
      respond_to do |format|
        format.html { render json:{:done => true, :invoice => @new_invoice.id}}
      end

    else
      error_response = get_error_message resultado[:message]
      respond_to do |format|
        format.html { render json: error_response }
      end

    end


  end
  def send_invoice_after_stamp(invoice)
    inv = invoice
    if inv.medical_consultations.count > 0
      transac = inv.medical_consultations.first
    else
      transac = inv.cabinet_reports.first
    end
    pdf_name = "Factura_"+transac.invoice.patient_fiscal_information.rfc+"_"+transac.invoice.certification_date
    pdfs = render_to_string pdf: pdf_name , locals: { :@med_consultation => transac, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
                            margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => transac}}, center: 'TEXT',
                                                                                                               spacing:           1,
                                                                                                               line:              true }
    patientmail = ""
    if invoice.patient_fiscal_information.email.present?
      patientmail = invoice.patient_fiscal_information.email
    elsif invoice.patient_fiscal_information.email2.present?
      patientmail = invoice.patient_fiscal_information.email2
    elsif invoice.patient_fiscal_information.patient.email.present?
      patientmail = invoice.patient_fiscal_information.patient.email
    end
    SendInvoice.send_invoice(patientmail,pdfs,pdf_name,inv).deliver_now
  end
  def search_invoices
    amount = 0
    if params[:type].to_i == 1
    consultas = MedicalConsultation.joins(:medical_expedient).where("medical_expedients.doctor_id = #{session[:doctor_id]}").where("date_consultation between STR_TO_DATE('#{params[:fecha_ini]}', '%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('#{params[:fecha_f]}', '%d/%m/%Y'), INTERVAL 1 day) ")
    else
    consultas = CabinetReport.joins(:clinical_analysis_order => :medical_expedient).where("medical_expedients.doctor_id = #{session[:doctor_id]}").where("cabinet_reports.created_at >= '#{params[:fecha_ini]}'").where("cabinet_reports.created_at <  '#{params[:fecha_f]}'")
    end
    invoices_info = []
    consultas.each do |info|
      if params[:type].to_i == 1
      if info.charges.count > 0
      amount = info.get_total
        if !info.invoiced
        invoices_info.push({
                               :id => info.id,
                               :folio =>info.id,
                               :patient => (info.medical_expedient.patient.name + " " +info.medical_expedient.patient.last_name),
                               :date => info.date_consultation.strftime('%d/%m/%Y'),
                               :amount => ActionController::Base.helpers.number_to_currency(amount, :unit => "$"),
                               :payment_method => info.charges.collect{|c| c.get_transaction.payment_method.name}.join(', ')
                           })
        end
      end
      else
        if info.charge_report_studies.count > 0
          amount = info.get_total
          if !info.invoiced
            invoices_info.push({
                                   :id => info.id,
                                   :folio =>info.id,
                                   :patient => info.clinical_analysis_order.medical_expedient.patient.full_name,
                                   :date => info.created_at.strftime('%d/%m/%Y'),
                                   :amount => ActionController::Base.helpers.number_to_currency(amount, :unit => "$"),
                                   :payment_method => info.charge_report_studies.collect{|c| c.transaction_item.payment_method.name}.join(', ')
                               })
          end
        end
      end


    end
    a = invoices_info.count
    b = a
    respond_to do |format|
      format.html { render json: invoices_info}
    end
  end

  def stamp_general_invoice
    if session[:doctor_id] == nil
      doctor_id = params[:doctor_id]
    else
      doctor_id = session[:doctor_id]
    end
    doc = Doctor.find(doctor_id)
    cadena_tabla = params[:conceptos].split(",")
    transactions_id = params[:transactions_id].split(",")
    q = 0;


    if doc.fiscal_information.certificate_base64 != nil || doc.fiscal_information.certificate_base64 != ""
      certificadobase64 = doc.fiscal_information.certificate_base64
    else
      certificado =  %x'openssl x509 -inform DER -in #{doc.fiscal_information.certificate_stamp.current_path.to_s}'
      arrayc =  certificado.split("\n")
      arrayc.shift
      arrayc.pop
      certificadobase64 = arrayc.join.to_s
      doc.fiscal_information.update(certificate_base64: certificadobase64)
    end



    if doc.fiscal_information.certificate_number == nil || doc.fiscal_information.certificate_number == ""
      #Obtiene el numero de serie del certificado y lo convierte al numero.
      certificado =  %x'openssl x509 -serial -noout -inform DER -in #{doc.fiscal_information.certificate_stamp.current_path.to_s}'
      arreglonumero = certificado.to_s.split('=')
      arreglosolonumero = arreglonumero[1].to_s.split(//)
      serialNumberCer = []
      (1..39).step(2) do |n|
        serialNumberCer.push(arreglosolonumero[n])
      end
      noCertificado = serialNumberCer.join.to_s
      doc.fiscal_information.update(certificate_number: noCertificado)
    else
      noCertificado = doc.fiscal_information.certificate_number
    end

    actual_time = Time.now
    payment_way = PaymentWay.find(params[:payment_type])
    next_folio = doc.fiscal_information.folio + 1
    b = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.send(:'cfdi:Comprobante',
               'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
               'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
               'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
               'TipoCambio'=>"0.00",
               'Moneda'=>"PESOS",
               'version'=>"3.2",
               'serie'=>doc.fiscal_information.serie,
               'folio'=>next_folio.to_s,
               'fecha'=>actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
               'sello'=>"",
               'formaDePago'=>payment_way.name,
               'noCertificado'=>noCertificado,
               'certificado'=>certificadobase64,
               'condicionesDePago'=>"Contado",
               'subTotal'=>params[:sub_total],
               'descuento'=>"0",
               'total'=>params[:total],
               'metodoDePago'=>params[:payment_method],
               'tipoDeComprobante'=>"ingreso",
               'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
               'LugarExpedicion'=>doc.city.state.country.name.upcase,
               'NumCtaPago'=>params[:account_number]){
        xml.send(:'cfdi:Emisor','rfc'=>doc.fiscal_information.rfc, 'nombre'=>doc.fiscal_information.bussiness_name){
          xml.send(:'cfdi:DomicilioFiscal','calle'=>doc.fiscal_information.fiscal_address, 'noExterior'=>doc.fiscal_information.ext_number, 'noInterior'=>doc.fiscal_information.int_number, 'colonia'=>doc.fiscal_information.suburb, 'municipio'=>doc.fiscal_information.locality, 'estado'=>doc.fiscal_information.city.state.name, 'pais'=>doc.fiscal_information.city.state.country.name, 'codigoPostal'=>doc.fiscal_information.zip){
            xml.text('')
          }
          xml.send(:'cfdi:RegimenFiscal','Regimen'=>doc.fiscal_information.tax_regime)
        }
        xml.send(:'cfdi:Receptor','rfc'=>"XAXX010101000", 'nombre'=>"PUBLICO EN GENERAL" ){
          xml.send('cfdi:Domicilio','calle'=>doc.fiscal_information.fiscal_address, 'noExterior'=>doc.fiscal_information.ext_number, 'noInterior'=>doc.fiscal_information.int_number,'colonia'=>doc.fiscal_information.suburb, 'municipio'=>doc.fiscal_information.locality, 'pais'=>doc.fiscal_information.city.state.country.name, 'codigoPostal'=>doc.fiscal_information.zip)
        }
        xml.send(:'cfdi:Conceptos'){
          (0..(cadena_tabla.length/3)-1).step(1) do |n|
            xml.send(:'cfdi:Concepto','cantidad'=>cadena_tabla[q], 'unidad'=>"No Aplica", 'noIdentificacion'=>"2", 'descripcion'=>"Honorarios médicos", 'valorUnitario'=>cadena_tabla[q+1], 'importe'=>cadena_tabla[q+2])
            q = q +3
          end
        }
        xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>"0", 'totalImpuestosTrasladados'=>"0"){
          xml.send(:'cfdi:Traslados'){
            xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>params[:iva],'importe'=>params[:importe_iva])
          }
        }
        xml.send(:'cfdi:Complemento')


      }
    end

    dir = Rails.root.join('uploads',doc.fiscal_information.rfc)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    xml_name = 'xml_base_general.xml'
    File.open(dir.join(xml_name), 'wb') do |file|
      file.write(b.to_xml)
    end
    doctor = Doctor.find(doctor_id)
    if doctor == nil
      assistant = Assistant.find(session[:assistant_id])
      doctor = assistant.doctor
    end

    xml_base_general_invoice = dir.join(xml_name)
    ruta_cadena_original = Dir.pwd + "/uploads/" + doc.fiscal_information.rfc + "/cadenaoriginal.txt"
    file_pem_root = Dir.pwd + "/uploads/fiscal_information_" + doc.fiscal_information.id.to_s + "/certificate_key/key.pem"
    %x' xsltproc cadenaoriginal_3_2.xslt #{xml_base_general_invoice} > #{ruta_cadena_original}'


    q = 0
    sello =  %x' openssl dgst -sha1 -sign #{file_pem_root} #{ruta_cadena_original} | openssl enc -base64'
    selloarreglo = sello.split("\n")
    sello = selloarreglo.join.to_s
    a = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.send(:'cfdi:Comprobante',
               'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
               'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
               'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
               'TipoCambio'=>"0.00",
               'Moneda'=>"PESOS",
               'version'=>"3.2",
               'serie'=>doc.fiscal_information.serie,
               'folio'=>next_folio.to_s,
               'fecha'=>actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
               'sello'=>sello,
               'formaDePago'=>payment_way.name,
               'noCertificado'=>noCertificado,
               'certificado'=>certificadobase64,
               'condicionesDePago'=>"Contado",
               'subTotal'=>params[:sub_total],
               'descuento'=>"0",
               'total'=>params[:total],
               'metodoDePago'=>params[:payment_method],
               'tipoDeComprobante'=>"ingreso",
               'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
               'LugarExpedicion'=>doc.city.state.country.name.upcase,
               'NumCtaPago'=>params[:account_number]){
        xml.send(:'cfdi:Emisor','rfc'=>doc.fiscal_information.rfc, 'nombre'=>doc.fiscal_information.bussiness_name){
          xml.send(:'cfdi:DomicilioFiscal','calle'=>doc.fiscal_information.fiscal_address, 'noExterior'=>doc.fiscal_information.ext_number, 'noInterior'=>doc.fiscal_information.int_number, 'colonia'=>doc.fiscal_information.suburb, 'municipio'=>doc.fiscal_information.locality, 'estado'=>doc.fiscal_information.city.state.name, 'pais'=>doc.fiscal_information.city.state.country.name, 'codigoPostal'=>doc.fiscal_information.zip){
            xml.text('')
          }
          xml.send(:'cfdi:RegimenFiscal','Regimen'=>doc.fiscal_information.tax_regime)
        }
        xml.send(:'cfdi:Receptor','rfc'=>"XAXX010101000", 'nombre'=>"PUBLICO EN GENERAL" ){
          xml.send('cfdi:Domicilio','calle'=>doc.fiscal_information.fiscal_address, 'noExterior'=>doc.fiscal_information.ext_number, 'noInterior'=>doc.fiscal_information.int_number, 'colonia'=>doc.fiscal_information.suburb, 'municipio'=>doc.fiscal_information.locality, 'pais'=>doc.fiscal_information.city.state.country.name, 'codigoPostal'=>doc.fiscal_information.zip)
        }
        xml.send(:'cfdi:Conceptos'){
          (0..(cadena_tabla.length/3)-1).step(1) do |n|
            xml.send(:'cfdi:Concepto','cantidad'=>cadena_tabla[q], 'unidad'=>"No Aplica", 'noIdentificacion'=>"2", 'descripcion'=>"Honorarios médicos", 'valorUnitario'=>cadena_tabla[q+1], 'importe'=>cadena_tabla[q+2])
            q = q +3
          end
        }
        xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>"0", 'totalImpuestosTrasladados'=>"0"){
          xml.send(:'cfdi:Traslados'){
            xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>params[:iva],'importe'=>params[:importe_iva])
          }
        }
        xml.send(:'cfdi:Complemento')


      }
    end
    dir = Rails.root.join('uploads',doc.fiscal_information.rfc)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    xml_name = 'xml_base_general.xml'
    File.open(dir.join(xml_name), 'wb') do |file|
      file.write(a.to_xml)
    end



    #timbra el xml

    # definir el entorno para decidir que webservice utilizar pruebas/produccion
    proveedor = Comprobante.new()
    xml = File.read(xml_base_general_invoice)
    timbrada = proveedor.timbra(xml)
    resultado = timbrada


    qr_code_local_route = "/uploads/" + doc.fiscal_information.rfc + "/qr"+actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S')+".png"
    if resultado[:message] == "1"
      paymeth = PaymentMethod.find_by_code(params[:payment_method])
      @new_general_invoice = Invoice.new(xml_base: dir.join(xml_name) , serie: doc.fiscal_information.serie, folio: next_folio,doctor_id:doctor.id,
                                         certification_date: actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
                                         payment_condition: payment_way.name, account_number: params[:account_number], subtotal: params[:sub_total], iva: params[:importe_iva],
                                         isr: 0, patient_fiscal_information_id: 0,payment_method_id: paymeth.id,payment_methods_name: params[:payment_method_name],invoice_type:2,payment_way_id:payment_way.id)
      @new_general_invoice.save
      OpenInvoiceReceptor.new(business_name: doctor.fiscal_information.bussiness_name, rfc:doctor.fiscal_information.rfc,address:doctor.fiscal_information.fiscal_address, suburb:doctor.fiscal_information.suburb, no_int:doctor.fiscal_information.int_number, no_ext:doctor.fiscal_information.ext_number, zip: doctor.fiscal_information.zip,
                                                      locality: doctor.fiscal_information.locality, city:doctor.fiscal_information.city.name, country:doctor.fiscal_information.city.state.country.name, state: doctor.fiscal_information.city.state.name, invoice_id: @new_general_invoice.id ).save

      InvoiceEmisor.new(business_name: doctor.fiscal_information.bussiness_name, rfc:doctor.fiscal_information.rfc,address:doctor.fiscal_information.fiscal_address, suburb:doctor.fiscal_information.suburb, no_int:doctor.fiscal_information.int_number, no_ext:doctor.fiscal_information.ext_number, zip: doctor.fiscal_information.zip,
                              locality: doctor.fiscal_information.locality, city:doctor.fiscal_information.city.name, country:doctor.fiscal_information.city.state.country.name, state: doctor.fiscal_information.city.state.name, invoice_id: @new_general_invoice.id, tax_regime: doctor.fiscal_information.tax_regime ).save

      q = 0
      (0..(cadena_tabla.length/3)-1).step(1) do |n|
        InvoiceConcept.new(invoice_id: @new_general_invoice.id,unit:"No aplica", quantity: cadena_tabla[q], description: "Honorarios médicos", unit_price: cadena_tabla[q+1], cost: cadena_tabla[q+2] ).save
        q = q +3
      end
      q = 0
      cadena_original_text = "";
      File.open(ruta_cadena_original, "r") do |f|
        f.each_line do |line|
          cadena_original_text += line
        end
      end
      @new_general_invoice.update(original_string: ruta_cadena_original,original_string_text:cadena_original_text)
      @new_general_invoice.update(UUID: resultado[:uuid], sat_seal: resultado[:timbre], no_cert_sat: resultado[:no_certificado_sat],xml: resultado[:xml],transmitter_seal: sello,qr_code: qr_code_local_route)

      transactions_id.each { |x|
        if params[:type].to_i == 1
        transac = MedicalConsultation.find(x)
        else
          transac = CabinetReport.find(x)
        end
        transac.update(invoiced: 1,invoice_id:@new_general_invoice.id)
      }
      doc.fiscal_information.update(folio: next_folio)
      total_qr = params[:total]
      total_qr = "%.6f" %total_qr
      total_qr = total_qr.rjust(17,'0')
      cadena_qr =  "?re="+doc.fiscal_information.rfc+"&rr="+doc.fiscal_information.rfc+"&tt="+total_qr.to_s+"&id="+resultado[:uuid]
      create_qr(cadena_qr,dir,"qr"+actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S')+".png")

      respond_to do |format|
        format.html { render json: resultado[:message]}
      end

    else
      error_response = get_error_message resultado[:message]
      respond_to do |format|
        format.html { render json: error_response }
      end

    end

  end
  def transaction_download_xml
    transac = MedicalConsultation.find(params[:id])
    inv = transac.invoice
    if inv != nil
      xml_data = inv.xml
      folio = inv.folio
    else
      xml_data = transac.invoice.xml
      folio = transac.invoice.folio
    end
    respond_to do |format|
      format.xml { send_data render_to_string(xml: xml_data) ,:filename => 'Factura'+folio.to_s+'.xml', :type=>"application/xml", :disposition => 'attachment' }
    end
  end
  def print_pdf

    transac = MedicalConsultation.find(params[:id])
    inv = transac.invoice
    respond_to do |format|
      format.pdf do
        if inv.invoice_type == 2
          render pdf: "Factura_"+transac.created_at.strftime('%Y-%m-%d')+"T"+transac.created_at.strftime('%H:%M:%S'),  locals: { :@med_consultation => transac, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
                 margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => transac}}, center: 'TEXT',
                                                                                                    spacing:           1,
                                                                                                    line:              true
              }
        else
            render pdf: "Factura_"+transac.invoice.patient_fiscal_information.rfc+"_"+transac.invoice.certification_date,  locals: { :@med_consultation => transac, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
                 margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => transac}}, center: 'TEXT',
                                                                                                    spacing:           1,
                                                                                                    line:              true
              }
        end

      end
    end
  end

  def numero_a_palabras(numero)
    de_tres_en_tres = numero.to_i.to_s.reverse.scan(/\d{1,3}/).map{|n| n.reverse.to_i}

    millones = [
        {true => nil, false => nil},
        {true => 'millón', false => 'millones'},
        {true => "billón", false => "billones"},
        {true => "trillón", false => "trillones"}
    ]

    centena_anterior = 0
    contador = -1
    palabras = de_tres_en_tres.map do |numeros|
      contador += 1
      if contador%2 == 0
        centena_anterior = numeros
        [centena_a_palabras(numeros), millones[contador/2][numeros==1]].compact if numeros > 0
      elsif centena_anterior == 0
        [centena_a_palabras(numeros), "mil", millones[contador/2][false]].compact if numeros > 0
      else
        [centena_a_palabras(numeros), "mil"] if numeros > 0
      end
    end

    palabras.compact.reverse.join(' ')
  end

  def centena_a_palabras(numero)
    especiales = {
        11 => 'once', 12 => 'doce', 13 => 'trece', 14 => 'catorce', 15 => 'quince',
        10 => 'diez', 20 => 'veinte', 100 => 'cien'
    }
    if especiales.has_key?(numero)
      return especiales[numero]
    end

    centenas = [nil, 'ciento', 'doscientos', 'trescientos', 'cuatrocientos', 'quinientos', 'seiscientos', 'setecientos', 'ochocientos', 'novecientos']
    decenas = [nil, 'dieci', 'veinti', 'treinta', 'cuarenta', 'cincuenta', 'sesenta', 'setenta', 'ochenta', 'noventa']
    unidades = [nil, 'un', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve']

    centena, decena, unidad = numero.to_s.rjust(3,'0').scan(/\d/).map{|i| i.to_i}

    palabras = []
    palabras << centenas[centena]

    if especiales.has_key?(decena*10 + unidad)
      palabras << especiales[decena*10 + unidad]
    else
      tmp = "#{decenas[decena]}#{' y ' if decena > 2 && unidad > 0}#{unidades[unidad]}"
      palabras << (tmp.blank? ? nil : tmp)
    end

    palabras.compact.join(' ')
  end
  helper_method :numero_a_palabras

  def create_qr(cadena,ruta,nombre)
    qrcode = RQRCode::QRCode.new(cadena)
# With default options specified explicitly
    png = qrcode.as_png(
        resize_gte_to: false,
        resize_exactly_to: false,
        fill: 'white',
        color: 'black',
        size: 180,
        border_modules: 1,
        module_px_size: 6,
        file: nil # path to write
    )
    FileUtils.mkdir_p(ruta) unless File.directory?(ruta)

    File.open(ruta.join(nombre), 'wb') do |file|
      file.write(png.to_s)
    end
  end
  def open_invoice

  end
  def stamp_open_invoice
    if session[:doctor_id] == nil
      doctor_id = params[:doctor_id]
    else
      doctor_id = session[:doctor_id]
    end
    doc = Doctor.find(doctor_id)
    cadena_tabla = params[:conceptos].split("|")
    q = 0
    if doc.fiscal_information.certificate_base64 != nil || doc.fiscal_information.certificate_base64 != ""
      certificadobase64 = doc.fiscal_information.certificate_base64
    else
      certificado =  %x'openssl x509 -inform DER -in #{doc.fiscal_information.certificate_stamp.current_path.to_s}'
      arrayc =  certificado.split("\n")
      arrayc.shift
      arrayc.pop
      certificadobase64 = arrayc.join.to_s #aqui ya esta chamaquiado el certificado :) voy por menos
      doc.fiscal_information.update(certificate_base64: certificadobase64)
    end



    if doc.fiscal_information.certificate_number == nil || doc.fiscal_information.certificate_number == ""
      #Obtiene el numero de serie del certificado y lo convierte al numero.
      certificado =  %x'openssl x509 -serial -noout -inform DER -in #{doc.fiscal_information.certificate_stamp.current_path.to_s}'
      arreglonumero = certificado.to_s.split('=')
      arreglosolonumero = arreglonumero[1].to_s.split(//)
      serialNumberCer = []
      (1..39).step(2) do |n|
        serialNumberCer.push(arreglosolonumero[n])
      end
      noCertificado = serialNumberCer.join.to_i
      doc.fiscal_information.update(certificate_number: noCertificado)
    else
      noCertificado = doc.fiscal_information.certificate_number
    end

    payment_way = PaymentWay.find(params[:payment_type])
    actual_time = Time.now
    next_folio = doc.fiscal_information.folio + 1
    b = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.send(:'cfdi:Comprobante',
               'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
               'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
               'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
               'TipoCambio'=>"0.00",
               'Moneda'=>"PESOS",
               'version'=>"3.2",
               'serie'=>doc.fiscal_information.serie,
               'folio'=>next_folio.to_s,
               'fecha'=>actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
               'sello'=>"",
               'formaDePago'=>payment_way.name,
               'noCertificado'=>noCertificado,
               'certificado'=>certificadobase64,
               'condicionesDePago'=>"Contado",
               'subTotal'=>params[:sub_total],
               'descuento'=>"0",
               'total'=>params[:amount],
               'metodoDePago'=>params[:payment_method],
               'tipoDeComprobante'=>"ingreso",
               'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
               'LugarExpedicion'=>params[:country].upcase,
               'NumCtaPago'=>params[:account_number]){
        xml.send(:'cfdi:Emisor','rfc'=>doc.fiscal_information.rfc, 'nombre'=>doc.fiscal_information.bussiness_name){
          xml.send(:'cfdi:DomicilioFiscal','calle'=>doc.fiscal_information.fiscal_address, 'noExterior'=>doc.fiscal_information.ext_number, 'noInterior'=>doc.fiscal_information.int_number, 'colonia'=>doc.fiscal_information.suburb, 'municipio'=>doc.fiscal_information.locality, 'estado'=>doc.fiscal_information.city.state.name, 'pais'=>doc.fiscal_information.city.state.country.name, 'codigoPostal'=>doc.fiscal_information.zip){
            xml.text('')
          }
          xml.send(:'cfdi:RegimenFiscal','Regimen'=>doc.fiscal_information.tax_regime)
        }
        xml.send(:'cfdi:Receptor','rfc'=>params[:rfc], 'nombre'=>params[:business_name] ){
          xml.send('cfdi:Domicilio','calle'=>params[:address], 'noExterior'=>params[:ext_number], 'noInterior'=>params[:int_number], 'colonia'=>params[:suburb], 'municipio'=>params[:locality], 'pais'=>params[:country], 'codigoPostal'=>params[:zip])
        }
        xml.send(:'cfdi:Conceptos'){
          (0..(cadena_tabla.length/5)-1).step(1) do |n|
            xml.send(:'cfdi:Concepto','cantidad'=>cadena_tabla[q], 'unidad'=>cadena_tabla[q+1], 'noIdentificacion'=>"3", 'descripcion'=>cadena_tabla[q+2], 'valorUnitario'=>cadena_tabla[q+3], 'importe'=>cadena_tabla[q+4])
            q = q +5
          end
        }
        xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>params[:isr], 'totalImpuestosTrasladados'=>params[:importeisr]){
          xml.send(:'cfdi:Traslados'){
            xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>params[:iva],'importe'=>params[:importeiva])
          }
        }
        xml.send(:'cfdi:Complemento')


      }
    end
    q = 0
    dir = Rails.root.join('uploads',doc.fiscal_information.rfc)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    xml_name = 'xml_base_open.xml'
    File.open(dir.join(xml_name), 'wb') do |file|
      file.write(b.to_xml)
    end

    doctor = Doctor.find(doctor_id)
    if doctor == nil
      assistant = Assistant.find(session[:assistant_id])
      doctor = assistant.doctor
    end

    xml_base_open_invoice = dir.join(xml_name)


    ruta_cadena_original = Dir.pwd + "/uploads/" + doc.fiscal_information.rfc + "/cadenaoriginal.txt"
    file_pem_root = Dir.pwd + "/uploads/fiscal_information_" + doc.fiscal_information.id.to_s + "/certificate_key/key.pem"
    %x' xsltproc cadenaoriginal_3_2.xslt #{xml_base_open_invoice} > #{ruta_cadena_original}'


    sello =  %x' openssl dgst -sha1 -sign #{file_pem_root} #{ruta_cadena_original} | openssl enc -base64'
    selloarreglo = sello.split("\n")
    sello = selloarreglo.join.to_s
    a = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.send(:'cfdi:Comprobante',
               'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
               'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
               'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
               'TipoCambio'=>"0.00",
               'Moneda'=>"PESOS",
               'version'=>"3.2",
               'serie'=>doc.fiscal_information.serie,
               'folio'=>next_folio.to_s,
               'fecha'=>actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
               'sello'=>sello,
               'formaDePago'=>payment_way.name,
               'noCertificado'=>noCertificado,
               'certificado'=>certificadobase64,
               'condicionesDePago'=>"Contado",
               'subTotal'=>params[:sub_total],
               'descuento'=>"0",
               'total'=>params[:amount],
               'metodoDePago'=>params[:payment_method],
               'tipoDeComprobante'=>"ingreso",
               'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
               'LugarExpedicion'=>params[:country].upcase,
               'NumCtaPago'=>params[:account_number]){
        xml.send(:'cfdi:Emisor','rfc'=>doc.fiscal_information.rfc, 'nombre'=>doc.fiscal_information.bussiness_name){
          xml.send(:'cfdi:DomicilioFiscal','calle'=>doc.fiscal_information.fiscal_address, 'noExterior'=>doc.fiscal_information.ext_number, 'noInterior'=>doc.fiscal_information.int_number, 'colonia'=>doc.fiscal_information.suburb, 'municipio'=>doc.fiscal_information.locality, 'estado'=>doc.fiscal_information.city.state.name, 'pais'=>doc.fiscal_information.city.state.country.name, 'codigoPostal'=>doc.fiscal_information.zip){
            xml.text('')
          }
          xml.send(:'cfdi:RegimenFiscal','Regimen'=>doc.fiscal_information.tax_regime)
        }
        xml.send(:'cfdi:Receptor','rfc'=>params[:rfc], 'nombre'=>params[:business_name] ){
          xml.send('cfdi:Domicilio','calle'=>params[:address], 'noExterior'=>params[:ext_number], 'noInterior'=>params[:int_number],'colonia'=>params[:suburb], 'municipio'=>params[:locality], 'pais'=>params[:country], 'codigoPostal'=>params[:zip])
        }
        xml.send(:'cfdi:Conceptos'){
          (0..(cadena_tabla.length/5)-1).step(1) do |n|
            xml.send(:'cfdi:Concepto','cantidad'=>cadena_tabla[q], 'unidad'=>cadena_tabla[q+1], 'noIdentificacion'=>"3", 'descripcion'=>cadena_tabla[q+2], 'valorUnitario'=>cadena_tabla[q+3], 'importe'=>cadena_tabla[q+4])
            q = q +5
          end
        }
        xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>params[:isr], 'totalImpuestosTrasladados'=>params[:importeisr]){
          xml.send(:'cfdi:Traslados'){
            xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>params[:iva],'importe'=>params[:importeiva])
          }
        }
        xml.send(:'cfdi:Complemento')


      }
    end
    dir = Rails.root.join('uploads',doc.fiscal_information.rfc)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    xml_name = 'xml_base_open.xml'
    File.open(dir.join(xml_name), 'wb') do |file|
      file.write(a.to_xml)
    end



    #timbra el xml

    # definir el entorno para decidir que webservice utilizar pruebas/produccion
    proveedor = Comprobante.new()
    xml = File.read(xml_base_open_invoice)
    timbrada = proveedor.timbra(xml)
    resultado = timbrada
    qr_code_local_route = "/uploads/" + doc.fiscal_information.rfc + "/qr"+actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S')+".png"
    if resultado[:message] == "1"
      @new_open_invoice = Invoice.new(xml_base: dir.join(xml_name) , serie: doc.fiscal_information.serie, folio: next_folio,doctor_id:doctor.id,
                                      certification_date: actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
                                      payment_condition: payment_way.name, account_number: params[:account_number], subtotal: params[:sub_total].to_f, iva: params[:importeiva].to_f,
                                      isr: params[:importeisr], patient_fiscal_information_id: params[:patient_fiscal_id],payment_methods_name: params[:payment_method_name],invoice_type:3,payment_way_id:payment_way.id)
      @new_open_invoice.save
      @new_open_invoice_receptor = OpenInvoiceReceptor.new(business_name: params[:business_name], rfc:params[:rfc],address:params[:address], suburb:params[:suburb], no_int:params[:int_number], no_ext:params[:ext_number], zip: params[:zip],
                                                           locality: params[:locality], city:params[:city], country:params[:country], state: params[:state], invoice_id: @new_open_invoice.id )
      @new_open_invoice_receptor.save

      InvoiceEmisor.new(business_name: doc.fiscal_information.bussiness_name, rfc:doc.fiscal_information.rfc,address:doc.fiscal_information.fiscal_address, suburb:doc.fiscal_information.suburb, no_int:doc.fiscal_information.int_number, no_ext:doc.fiscal_information.ext_number, zip: doc.fiscal_information.zip,
                        locality: doc.fiscal_information.locality, city:doc.fiscal_information.city.name, country:doc.fiscal_information.city.state.country.name, state: doc.fiscal_information.city.state.name, invoice_id: @new_open_invoice.id,tax_regime: doc.fiscal_information.tax_regime ).save

      q = 0
      puts cadena_tabla
      (0..(cadena_tabla.length/5)-1).step(1) do |n|
        measure_unit = MeasureUnit.find_by_name(cadena_tabla[q+1])
        InvoiceConcept.new(invoice_id: @new_open_invoice.id, quantity: cadena_tabla[q], description: cadena_tabla[q+2],unit:cadena_tabla[q+1], unit_price: cadena_tabla[q+3], cost: cadena_tabla[q+4],measure_unit_id:measure_unit.id ).save
        q = q + 5
      end
      q = 0
      cadena_original_text = "";
      File.open(ruta_cadena_original, "r") do |f|
        f.each_line do |line|
          cadena_original_text += line
        end
      end
      @new_open_invoice.update(original_string: ruta_cadena_original,original_string_text:cadena_original_text)
      @new_open_invoice.update(UUID: resultado[:uuid], sat_seal: resultado[:timbre], no_cert_sat: resultado[:no_certificado_sat],xml: resultado[:xml],transmitter_seal: sello,qr_code: qr_code_local_route)

      doc.fiscal_information.update(folio: next_folio)
      total_qr = params[:amount]
      total_qr = "%.6f" %total_qr
      total_qr = total_qr.rjust(17,'0')
      cadena_qr =  "?re="+doc.fiscal_information.rfc+"&rr="+params[:rfc]+"&tt="+total_qr+"&id="+resultado[:uuid]
      create_qr(cadena_qr,dir,"qr"+actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S')+".png")
      respond_to do |format|
        format.html { render json:{:done => true, :invoice => @new_open_invoice.id}}
      end

    else
      error_response = get_error_message resultado[:message]
       respond_to do |format|
        format.html { render json:{:done => false, :error_response => error_response} }
      end

    end

  end

  def get_error_message error_code_stamp

    case error_code_stamp
      when  '300'
        error_message = 'Usuario y contraseña inválidos'
      when '301'
        error_message = 'XML mal formado '
      when '302'
        error_message = 'Sello mal formado'
      when '303'
        error_message = 'Sello no corresponde al emisor'
      when '305'
        error_message ='Fecha de emisión no está dentro de la vigencia del Certificado'
      when '304'
        error_message = 'Certificado revocado o caduco'
      when '401'
        error_message = 'Fecha y hora de generación fuera de rango'
      when '702'
        error_message = 'El prefijo del namespace no es el especificado por el SAT'
      when '705'
        error_message = 'Estructura de XML mal formada (Error de Sintaxis)'
      when '708'
        error_message = 'Error de conexión con el SAT'
      when '712'
        error_message = 'El Certificado de timbre no se pudo recuperar del SAT.'
      else
        error_message =  error_code_stamp + ' Error no especificado'
    end

    return error_message
  end
 def get_cancel_invoice_error cancel_code
   case cancel_code
     when  '202'
       error_message = 'Cancelado Previamente'
     when '203'
       error_message = 'No corresponde el RFC del emisor y de quien solicita la cancelación '
     when '205'
       error_message = 'No existente'
     when '900'
       error_message = 'Error de PAC'
     when '708'
       error_message ='Error de conexión con el SAT'
     else
       error_message =  cancel_code + ' Error no especificado'
   end

   return error_message
 end
  def get_receptor_email
    email = ""
    t = MedicalConsultation.find(params[:trasac_id])
    if t != nil
    if t.invoice != nil
      if t.invoice.patient_fiscal_information != nil
        if t.invoice.patient_fiscal_information.email != ""
            email = t.invoice.patient_fiscal_information.email
        end
      end
    end
    end

    respond_to do |format|
      format.html { render json: {:email => email} }
    end

  end
  def send_invoice_email
    transac = MedicalConsultation.find(params[:transac_id])
    inv = Invoice.find_by_folio(params[:folio])
    pdf_name = ""
    if inv != nil
      pdf_name = "Factura_global_"+transac.created_at.strftime('%Y-%m-%d')+"T"+transac.created_at.strftime('%H:%M:%S')
      pdfs = render_to_string pdf: pdf_name,  locals: { :@med_consultation => transac, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
             margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => consultation}}, center: 'TEXT',
                                                                                                spacing:           1,
                                                                                                line:              true
          }
    else
      pdf_name = "Factura_"+transac.invoice.patient_fiscal_information.rfc+"_"+transac.invoice.certification_date
      pdfs = render_to_string pdf: pdf_name , locals: { :@med_consultation => transac, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
                              margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => consultation}}, center: 'TEXT',
                                                                                                                 spacing:           1,
                                                                                                                 line:              true }
    end

    SendInvoice.send_invoice(params[:email],pdfs,pdf_name,inv).deliver_now
    render :json => true, layout: false


  end
  def get_doctor_patients
    doctor = Doctor.find(params[:doctor_id])
    patients = []
    doctor.patients.each do |patient|
      patients.push(
          {
              id: patient.id,
              name: patient.name + " " + patient.last_name,
          }
      )
    end
    render :json => {:patients => patients}, layout: false
  end
  def get_patient_fiscal
    fiscals = []
    if params[:patient_id] != nil && params[:patient_id] != ""
      patient = Patient.find(params[:patient_id])
      patient.patient_fiscal_informations.each do |fiscal|
        fiscals.push(
            {
                id: fiscal.id,
                business_name: fiscal.business_name.gsub(',,', ' '),
                type:"patient"
            }
        )
      end
    else
      if session[:doctor_id] != nil && session[:doctor_id] != ""
        doctor = Doctor.find(session[:doctor_id])
      else
        doctor = Doctor.find(params[:doctor_id])
      end
      doctor.general_fiscal_infos.each do |fiscal|
        fiscals.push(
            {
                id: fiscal.id,
                business_name: fiscal.business_name.gsub(',,', ' '),
                type:"doctor"
            }
        )
      end
    end
    render :json => {:fiscals => fiscals}, layout: false
  end
  def get_patient_fiscal_to_set
    fiscals = []
    if params[:type] == "patient"
    fiscal = PatientFiscalInformation.find(params[:fiscal_id])
      fiscals.push(
          {
              id: fiscal.id,
              business_name: fiscal.business_name.gsub(',,', ' '),
              rfc: fiscal.rfc,
              address: fiscal.address,
              locality: fiscal.locality,
              ext_number: fiscal.ext_number,
              int_number: fiscal.int_number,
              zip: fiscal.zip,
              suburb: fiscal.suburb,
              city: fiscal.city.name,
              state: fiscal.city.state.name,
              country: fiscal.city.state.country.name
          }
      )
    else
      fiscal = GeneralFiscalInfo.find(params[:fiscal_id])
      fiscals.push(
          {
              id: fiscal.id,
              business_name: fiscal.business_name.gsub(',,', ' '),
              rfc: fiscal.rfc,
              address: fiscal.address,
              locality: fiscal.locality,
              ext_number: fiscal.ext_number,
              int_number: fiscal.int_number,
              zip: fiscal.zip,
              suburb: fiscal.suburb,
              city: fiscal.city.name,
              state: fiscal.city.state.name,
              country: fiscal.city.state.country.name
          }
      )
    end

    render :json => {:fiscals => fiscals}, layout: false
  end
  def show
  end


  private

  # Use callbacks to share common setup or constraints between actions.

end

