class Doctor::OtolaryngologySisController < ApplicationController

  before_action :set_otolaryngology_si, only: [:update]

  def index
  end

  def create
    @otolaryngology = OtolaryngologySi.new(otolaryngology_si_params)
    respond_to do |format|
      if @otolaryngology.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @otolaryngology.id}}
      else
        format.html { render json: {:done => false, :errors => @otolaryngology.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @otolaryngology.update(otolaryngology_si_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @otolaryngology.errors.full_messages}}
      end
    end
  end

  private
  def set_otolaryngology_si
    @otolaryngology = OtolaryngologySi.find(params[:id])
  end
  def otolaryngology_si_params
    params.require(:otolaryngology_si).permit(:otalgia,:otorrea,:hypoacusia,:epistaxis,:rinorrea,:odinofagia,:phonation,:medical_history_id)
  end
end