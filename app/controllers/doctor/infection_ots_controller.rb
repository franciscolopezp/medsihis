class Doctor::InfectionOtsController < ApplicationController

  before_action :set_infection_ot, only: [:update]

  def index
  end

  def create
    @infection = InfectionOt.new(infection_ot_params)
    respond_to do |format|
      if @infection.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @infection.id}}
      else
        format.html { render json: {:done => false, :errors => @infection.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @infection.update(infection_ot_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @infection.errors.full_messages}}
      end
    end
  end

  private
  def set_infection_ot
    @infection = InfectionOt.find(params[:id])
  end
  def infection_ot_params
    params.require(:infection_ot).permit(:description, :contacts,:stings,:environment,:foods,:travels,:internal_surgery_prosthesis,:intravenous_drugs,:tattoos,:transfusions,:sexual_activity,:medical_history_id)
  end
end