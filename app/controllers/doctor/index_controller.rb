class Doctor::IndexController < ApplicationController

  include Doctor::ActivitiesHelper
  include Doctor::IndexHelper

  def index
    @doctor_username = session[:doctor_username]
    if session[:doctor_id] != nil
      @doctor = Doctor.find(session[:doctor_id])
    end

    @activity_types = ActivityType.all
    @activity_statuses = ActivityStatus.all
=begin
    @activate_notification = session[:active_notification]

    if @activate_notification
      generate_message_notification_license
    end
    @activate_notification = false if @title_notification.nil?
    session[:active_notification] = false
=end
  end

  def update_terms_conditions
    doctor = get_doctor_session
    if doctor.update(accept_term_conditions: true)
      render :json => {result: true}
    else
      render :json => {result: false}
    end
  end

  def accept_terms
    render 'doctor/index/terms_and_conditions', layout: false
  end
end