class Doctor::InheritedFamilyBackgroundsController < ApplicationController

  def create
    ifb = InheritedFamilyBackground.new(get_params)
    ifb.save
    render json: {:done => true}
  end
  def update
    inherited_family_background = InheritedFamilyBackground.find(params[:id])
    inherited_family_background.update(get_params)
    render json: {:done => true}
  end

  private
  def get_params
    params.require(:inherited_family_background).permit(
        :medical_history_id,
        :gynecological_diseases,:gynecological_diseases_description,
        :mammary_dysplasia,
    :mammary_dysplasia_description,:twinhood,
        :twinhood_description,:congenital_anomalies,:congenital_anomalies_description,
    :infertility,:infertility_description,:others,:others_description,:anotations)
  end
end