class Doctor::MedicalExpedientsController < ApplicationController
  #load_and_authorize_resource
  #skip_authorize_resource :only => [:create,:update]
  load_and_authorize_resource :only => [:index, :show]
  include Doctor::MedicalExpedientsHelper
  include Doctor::DashboardHelper
  include Doctor::MedicalPedHistoriesHelper
  include Doctor::FilterRequestHelper
  include Doctor::ExpedientPermissionsHelper

  before_action :filter_medical_expedient, only: []
  before_action :set_medical_expedient, only: [:show, :edit, :update, :destroy, :search_patient, :general_observations, :medical_consultation, :medical_history,:form_medical_consultation,:form_edit_medical_consultation,:vital_sign_section, :vital_sign_table]


  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicalExpedientDatatable.new(view_context)}
    end
  end

  def show
    puts "SESSION"
    puts session[:role]
    #if session[:role] == 'doctor'
      @all_accounts = current_user.accounts
    #end
    @has_gin = true #doctor_has_feature Feature::GIN
  end

  def get_vital_sign
    vital_sign_requested = VitalSign.find(params[:id])
    respond_to do |format|
      format.json { render json: vital_sign_requested}
    end
  end

  def get_data_chart_vital_sign
    expedient = MedicalExpedient.find(params[:medical_expedient_id])
    data = []
    expedient.medical_consultations.order('date_consultation ASC').all.each do |mc|
      item = Hash.new
      item['date'] = mc.date_consultation
      case params[:type]
        when "weight"
          item['val'] = mc.vital_sign.weight
        when "height"
          item['val'] = mc.vital_sign.height
        when "temperature"
          item['val'] = mc.vital_sign.temperature
        when "blood_pressure"
          value_blood_pressure = Hash.new
          if !mc.vital_sign.systolic_pressure.nil? and !mc.vital_sign.diastolic_pressure.nil?
            value_blood_pressure['systolic_pressure'] = mc.vital_sign.systolic_pressure
            value_blood_pressure['diastolic_pressure'] = mc.vital_sign.diastolic_pressure
            item['val'] = value_blood_pressure
          else
            item['val'] = nil
          end
        else
      end

      data.push(item)
    end
    render json: data
  end

  def vital_sign_section
    @has_gin = doctor_has_feature Feature::GIN
    render :layout => false
  end

  def vital_sign_table
    respond_to do |format|
      format.html
      format.json { render json: VitalSignsDatatable.new(view_context,@medical_expedient.id)}
    end
  end

  def medical_history

    @has_ped = doctor_has_feature Feature::PED
    @has_gin = doctor_has_feature Feature::GIN
    @has_uro = doctor_has_feature Feature::URO

    if @medical_expedient.medical_history != nil
      @medical_history = @medical_expedient.medical_history
    else
      @medical_history = MedicalHistory.new
      @medical_history.build_medical_expedient
    end

    # if record exist and doctor have the "Ginecología" feature pass record to view
    if @medical_history.inherited_family_background != nil && @has_gin
      @inherited_family_background = @medical_history.inherited_family_background
    elsif @has_gin # if not exist but doctor have the "Ginecología" feature initialize record in memory and pass to view
      @inherited_family_background = InheritedFamilyBackground.new
    end

    # if record exist and doctor have the "Ginecología" feature pass record to view
    if @medical_history.gynecologist_obstetrical_background != nil && @has_gin
      @gynecologist_obstetrical_background = @medical_history.gynecologist_obstetrical_background
    elsif @has_gin # if not exist but doctor have the "Ginecología" feature initialize record in memory and pass to view
      @gynecologist_obstetrical_background = GynecologistObstetricalBackground.new
    end

    if @has_uro && @medical_history.urological_background.nil?
      @uro_background = UrologicalBackground.new
    elsif @has_uro
      @uro_background = @medical_history.urological_background
    end
    @gender_patient = @medical_expedient.patient.gender_id

    render :layout => false
  end
  def view_expedient_patient_graph_valoration
    valoration_name = ""
    expedient = MedicalExpedient.find(params[:medical_expedient_id])
    data = []
    expedient.medical_consultations.order('date_consultation ASC').all.each do |mc|
      item = Hash.new
      item['date'] = mc.date_consultation
      aaa = params[:type]
      case params[:type]
        when "weight"
          item['val'] = mc.vital_sign.weight
          valoration_name = "Peso"
        when "height"
          item['val'] = mc.vital_sign.height
          valoration_name = "Altura"
        when "temperature"
          item['val'] = mc.vital_sign.temperature
          valoration_name = "Temperatura"
        when "blood_pressure"
          valoration_name = "Presion_sanguinea"
          value_blood_pressure = Hash.new
          if !mc.vital_sign.systolic_pressure.nil? and !mc.vital_sign.diastolic_pressure.nil?
            value_blood_pressure['systolic_pressure'] = mc.vital_sign.systolic_pressure
            value_blood_pressure['diastolic_pressure'] = mc.vital_sign.diastolic_pressure
            item['val'] = value_blood_pressure
          else
            item['val'] = nil
          end
        else
      end

      data.push(item)
    end
    render pdf: "Expediente "+expedient.patient.full_name+"_"+valoration_name, locals: { :@medical_expedient => expedient,
                                                                      :@type => params[:type],
                                                                      :@data => data},
           template: 'doctor/print_medical_expedients/body_valoration_graphs.html.erb', layout: false,
           margin: { top: 11, bottom: 17},
           encoding: 'UTF-8',

           footer:  {
               html: {
                   template:'doctor/print_medical_expedients/footer.html.erb',
                   locals: {:@medical_expedient => expedient}}
           }
  end

  def update_general_observations
    medical_exp = MedicalExpedient.find(params[:medical_expedient])
    if medical_exp.update(general_observations: params[:general_observations])
      respond_to do |format|
        format.html { render json: {:result => true} }
      end
    else
      respond_to do |format|
        format.html { render json: {:result => false} }
      end
    end

  end
  def search_patient
    @doctor_patient = @medical_expedient.patient
    render :layout => false
    #render :layout => false, :partial => "information_panoramic", :locals => {:asset => Asset.find(params[:id])}
  end
  def general_observations
    @medical_expedient
    render :layout => false
  end

  def medical_consultation
    @daily_cash = DailyCash.current session[:user_id]
    consultations = MedicalConsultationsDatatable.new(view_context, @current_user,nil,@medical_expedient)
    @medical_consultations = consultations.as_json
    render :layout => false
  end

  def form_edit_medical_consultation
    @patients = []
    @patients << @medical_expedient.patient

    @offices = current_user.offices
    @consultation_types = current_user.consultation_types
    c_types_id = []
    @consultation_types.each do |ct|
      c_types_id.push(ct.id)
    end
    @consultation_costs = ConsultationCost.where('consultation_type_id IN (?)',c_types_id)

    @last_consult = @patients[0].medical_consultations.order('date_consultation DESC').limit(3).first


    @doctor_medical_consultation = MedicalConsultation.find(params[:medical_consultation_id])

    @has_gin = true
    @has_ped = true
    @current_pregnancy = Pregnancy.current_by_expedient(@medical_expedient.id).first

    render partial: "doctor/medical_consultations/form_edit", :layout => false
  end
  def form_medical_consultation
    @patients = []
    @patients << @medical_expedient.patient

    @offices = current_user.offices
    @consultation_types = current_user.consultation_types
    c_types_id = []
    @consultation_types.each do |ct|
      c_types_id.push(ct.id)
    end
    @consultation_costs = ConsultationCost.where('consultation_type_id IN (?)',c_types_id)

    @last_consult = @patients[0].medical_consultations.order('date_consultation DESC').limit(3).first
    if @last_consult != nil
      @last_weight = @last_consult.vital_sign.weight
      @last_height = @last_consult.vital_sign.height
    else
      @last_weight = @patients[0].weight
      @last_height = @patients[0].height
    end

    @last_sp = @patients[0].systolic_pressure
    @last_dp = @patients[0].diastolic_pressure

    @doctor_medical_consultation = MedicalConsultation.new
    @doctor_medical_consultation.build_vital_sign
    @doctor_medical_consultation.build_prescription
    @doctor_medical_consultation.build_exploration

    @has_gin = doctor_has_feature Feature::GIN
    @has_ped = doctor_has_feature Feature::PED
    @has_uro = doctor_has_feature Feature::URO

    @current_pregnancy = Pregnancy.current_by_expedient(@medical_expedient.id).first

    render partial: "doctor/medical_consultations/form", :layout => false
  end

  def update_medical_consultation
    consult = MedicalConsultation.find(params[:medical_consultation_id])
    consult.update(current_condition:params[:medical_consultation][:current_condition], diagnostic:params[:medical_consultation][:diagnostic])
    consult.prescription.update(recommendations:params[:medical_consultation][:prescription_attributes][:recommendations])
    render :json => {ok: "true"}
  end
  def create_medical_consultation
    result = create_medical_consultation_record

    if result[:result]

      render :json => {ok: "true"}

    else
      render :json => {ok: "false", errors: result[:errors]}
    end
  end

  def show_medical_consultation
    @doctor_medical_consultation = MedicalConsultation.find(params[:id])
    render 'doctor/medical_consultations/show', layout: false
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medical_expedient
      @medical_expedient = MedicalExpedient.includes(:patient).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medical_expedient_params
      params.fetch(:doctor_medical_expedient, {})
    end
end
