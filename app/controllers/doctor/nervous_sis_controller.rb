class Doctor::NervousSisController < ApplicationController

  before_action :set_nervous_si, only: [:update]

  def index
  end

  def create
    @nervous = NervousSi.new(nervous_si_params)
    respond_to do |format|
      if @nervous.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @nervous.id}}
      else
        format.html { render json: {:done => false, :errors => @nervous.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @nervous.update(nervous_si_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @nervous.errors.full_messages}}
      end
    end
  end

  private
  def set_nervous_si
    @nervous = NervousSi.find(params[:id])
  end
  def nervous_si_params
    params.require(:nervous_si).permit(:description, :headache,:convulsions,:transitory_deficit,:confusion,:obnubilation,:march,:balance,:language,:sleep_vigil,:medical_history_id)
  end
end