class Doctor::LaboratoriesController < ApplicationController

  include ApplicationHelper
  before_action :set_laboratory, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: LaboratoriesDatatable.new(view_context, session[:doctor_id])}
    end
  end

  def show
    @groups = CaGroup.where('laboratory_id = ?',@laboratory.id).order('name ASC')
    @categories = ClinicalStudyType.where('laboratory_id = ?',@laboratory.id).order('name ASC')
  end

  def download_image
    lab = Laboratory.find(params[:id])
      send_file lab.logo.current_path, :type => 'image/jpeg', :disposition => 'inline'
  end

  def new
    @laboratory = Laboratory.new
    @laboratory.build_city
    @text_button = "Agregar"
  end

  def edit
    @text_button = "Actualizar"
  end

  def create
    @laboratory = Laboratory.new(laboratory_params)
    set_city_model params, @laboratory
    @city_name = params[:city_name]
    @text_button = "Agregar"

    respond_to do |format|
      if @laboratory.save
        doctors = [Doctor.find(session[:doctor_id])]
        @laboratory.update({doctors:doctors})

        format.html { redirect_to doctor_laboratories_path, notice: 'Laboratorio creado correctamente' }
        format.json { render :show, status: :created, location: @laboratory }
      else
        @laboratory.build_city
        format.html { render :new }
        format.json { render json: @laboratory.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @city_name = params[:city_name]
      @text_button = "Actualizar"
      if @laboratory.update(laboratory_params)
        set_city_model params, @laboratory
        @laboratory.update(city_id: @laboratory.city_id)
        format.html { redirect_to doctor_laboratories_path, notice: 'Laboratorio actualizado correctamente' }
        format.json { render :show, status: :ok, location: @laboratory }
      else
        @laboratory.build_city
        format.html { render :edit }
        format.json { render json: @laboratory.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy

    begin
      @laboratory.destroy
    rescue ActiveRecord::StatementInvalid
      result = {:done => false, :error => 'No se puede eliminar el registro',  :message => 'Hay referencias a él en el sistema.'}
    rescue
      #Only comes in here if nothing else catches the error
    end

    respond_to do |format|
      format.html { redirect_to doctor_laboratories_url, notice: '' }
      format.json { render json: result}
    end
  end

  private
  def set_laboratory
    @laboratory = Laboratory.find(params[:id])

    if !valid_lab
      redirect_to doctor_laboratories_url
    end

  end

  def laboratory_params
    params.require(:laboratory).permit(:address, :email, :telephone, :name, :city_id, :observations, :logo, :contact, :clinical_order_type_id, :is_custom)
  end

  def valid_lab
    #validar si el laboratorio le pertenece al doctor
    begin
      doctor = @laboratory.doctors.find(session[:doctor_id])
      if !@laboratory.is_custom && doctor != nil
        return false
      end
    rescue ActiveRecord::RecordNotFound
      return false
    rescue
      #Only comes in here if nothing else catches the error
    end

    true
  end

end
