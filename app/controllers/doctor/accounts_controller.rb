class Doctor::AccountsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:account_manage]
  include Doctor::FilterRequestHelper
  before_action :filter_account_bank, only: [:show, :edit]
  before_action :set_account, only: [:show, :edit, :update, :destroy]
  before_action :load_banks, only: [:new,:edit]

  def index
    @current_user = current_user
    respond_to do |format|
      format.html
      format.json { render json: AccountsDatatable.new(view_context,current_user)}
    end
  end

  def new
    @account = Account.new
    @is_doctor = current_user.is_doctor
  end

  def create
    @account = Account.new(account_params)
    respond_to do |format|
      if @account.save
        if current_user.is_doctor
          accounts = current_user.accounts
          accounts.push(@account)
          current_user.update(accounts:accounts);
        end
        format.html { redirect_to doctor_accounts_url, notice: 'Cuenta creada correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  def edit

  end

  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to doctor_accounts_url, notice: 'Cuenta actualizada correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  def destroy
    if @account.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Cuenta eliminada correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  def show

  end
  def save_accounts
      user = User.find(params[:user_id])
      user.accounts.delete_all
      params[:accounts].each do |idx,acc|
          account = Account.find(acc["account_id"].to_i)
          user.accounts << account
      end
      respond_to do |format|
        format.html {render json: true}
      end
  end
  def get_accounts
    accounts = []
    user = User.find(params[:user_id])
    user.accounts.each do |account|
      accounts.push({
                           :name=>"account_" + account.id.to_s
                       })
    end
    respond_to do |format|
      format.html { render json: accounts}
    end
  end
  def account_manage
    @current_user = current_user
    respond_to do |format|
      format.html
      format.json { render layout: false}
    end
  end
  def account_balance

    @month = params[:month]
    @year = params[:year]

    @init_date = Date.parse('1-'+@month+'-'+@year)
    @end_date = @init_date + 1.month

    @account = Account.find(params[:id])
    @transactions = @account.movements.where('transactions.date >= ? AND transactions.date < ?',@init_date,@end_date)

    puts @transactions.size
    puts '------------------'

    respond_to do |format|
      format.pdf do
        render pdf: 'estado_cuenta',
               encoding: 'utf8',
               :margin => { :bottom => 25 },
               :footer => {
                   :content => render_to_string(:template => 'doctor/accounts/account_balance_footer.pdf.erb')
               }
      end
    end
  end

  private
  def set_account
    @account = Account.find(params[:id])
  end

  def account_params
    data = params.require(:account).permit(:account_number,:clabe,:balance,:details, :bank_id, :currency_id,:is_global)
    data["balance"] = (data["balance"].gsub!(',','')).to_f if data["balance"].include? ','
    data
  end

  def load_banks
    @banks = Bank.where("is_local = 0")
    @local_banks = Bank.where("is_local = 1")
    @offices = Office.active_offices.where('doctor_id = '+session[:doctor_id].to_s)
  end

end