class Doctor::DigestiveApsController < ApplicationController

  before_action :set_digestive_ap, only: [:update]

  def index
  end

  def create
    @record = DigestiveAp.new(digestive_ap_params)
    respond_to do |format|
      if @record.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @record.id}}
      else
        format.html { render json: {:done => false, :errors => @record.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @record.update(digestive_ap_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @record.errors.full_messages}}
      end
    end
  end

  private
  def set_digestive_ap
    @record = DigestiveAp.find(params[:id])
  end
  def digestive_ap_params
    params.require(:digestive_ap).permit(:description, :medical_history_id, :anorexia, :polidipsia, :nauseas, :vomitos, :dispepsia, :disfagia, :odinofagia, :rectorragia, :melenas, :abdominalgia, :pirosis, :hematemesis, :acolia, :meteorismo, :tenesmo)
  end
end

