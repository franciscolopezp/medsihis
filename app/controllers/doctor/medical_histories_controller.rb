class Doctor::MedicalHistoriesController < ApplicationController

  include Doctor::MedicalExpedientsHelper
  include Doctor::MedicalHistoryHelper
  before_action :set_medical_history, only: [:show, :edit, :update, :destroy]

  # GET /doctor/medical_expedients
  # GET /doctor/medical_expedients.json
  @medicaments = Medicament.all
  @diseases = Disease.all
  def index
    @medical_history = MedicalHistory.all
  end

  def new
    @text_button = "Guardar"
    @medical_history = MedicalHistory.new
    @medical_history.build_medical_expedient
  end

  def add_disease

   disease_new = Disease.new(disease_category_id: params[:disease_category_id], code: params[:code], name: params[:name], doctor_id: current_user.doctor.id)
    if disease_new.save
      render :json => {result:true, disease: disease_new.to_json(only:[:id, :name])}
    else
      render :json => {result:false}
    end
  end

  def find_disease
    to_search = params[:term]
    render :json => search_disease(to_search)
  end

  def exist_disease
    disease = Disease.find_by(name: params[:name_element], doctor_id: current_user.doctor.id)
    if disease.nil?
      render :json => {result: false}
    else
      render :json => {result:true, model: disease.to_json(only:[:id, :name])}
    end
  end

  def add_medicament
    #check if active_substance exist

    add_medicament = true

    if params['force'] == 'no'
      medicament = Medicament.where('brand = ? AND active_substance like ?','Genéricos Intercambiables','%'+params[:active_substance]+'%').first
      if !medicament.nil?
        add_medicament = false
      end
    end

    if add_medicament
      medicament_new = Medicament.new(active_substance: params[:active_substance],
                                      brand: params[:brand], comercial_name: params[:comercial_name],
                                      contraindication: params[:contraindication],
                                      presentation: params[:presentation], doctor_id: current_user.doctor.id)

      medicament_new.save

      resp = {result:true, medicament: medicament_new.to_json(only:[:id, :comercial_name,:presentation])}
    else
      resp = {result:false, message:'El medicamento ya exsite como', medicament: medicament }
    end

    render :json => resp

  end

  def find_medicament
    to_search = params[:term]
    render :json => search_medicament(to_search).to_a
  end

  def exist_medicament
    medicament = Medicament.find_by(comercial_name: params[:name_element], doctor_id: get_doctor_session.id)
    if medicament.nil?
      render :json => {result: false}
    else
      render :json => {result:true, model: medicament.to_json(only:[:id, :comercial_name,:presentation])}
    end
  end

  def load_vaccine_books
    @patient_vaccine_book = Patient.find(params[:patient])

    render partial: "doctor/dashboard/patient_vaccine_book", layout: false
  end
  def delete_vaccinebook_patient
    patient = Patient.find(params[:patient])
    vaccinebook = VaccineBook.find(params[:vaccinebook])
    patient.vaccine_books.delete(vaccinebook)
      render :json => {result:true, message: "Cartilla eliminada."}
  end
  def add_vaccinebook_patient
    duplicate_book = 0
    patient = Patient.find(params[:patient])
    vaccinebook = VaccineBook.find(params[:vaccinebook])
    patient.vaccine_books.each do |book|
      if book.id.to_s == params[:vaccinebook]
        duplicate_book = 1
      end
    end
    if duplicate_book == 0
      patient.vaccine_books << vaccinebook
      render :json => {result:true}
    else
      render :json => {result:false, message: "La cartilla de vacunacion ya se encuentra agregada"}
    end
  end
  def edit
    @text_button = "Guardar"
  end


  def create
=begin
    @text_button = "Guardar"
    @medical_history = MedicalHistory.new(medical_history_params)

    respond_to do |format|
      if @medical_history.save
        format.html { redirect_to doctor_medical_histories_path, notice: 'La Enfermedad fue agregada con exito'}
        format.json { render json: 'true'}
      else
        format.html { render :new}
        format.json {render json: 'error', status: :unprocessable_entity}
      end
    end
=end
  end

  def update
   # @text_button = "Guardar"
    if update_medical_history
      render :json => true
    else
      render :json => false
    end

=begin
    respond_to do |format|

      if @medical_history.update(medical_history_params)
        format.json { render json: 'true'}
      else
        format.html { render :edit}
        format.json { render json: 'Personal error', status: :unprocessable_entity}
      end
    end
=end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_medical_history
    @medical_history = MedicalHistory.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def medical_history_params
    #params.fetch(:medical_history).permit(:nonpathological, :pathological, :overweight, :hypertension, :diabetes, :relative, :surgeries, :other_medicaments, :other_diseases, :medical_expedient_id)
  end


end
