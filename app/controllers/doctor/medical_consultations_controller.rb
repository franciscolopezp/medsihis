class Doctor::MedicalConsultationsController < ApplicationController
  load_and_authorize_resource :only => [:index]
  include Doctor::MedicalConsultationsHelper
  include Doctor::FilterRequestHelper
  include Doctor::MedicalExpedientsHelper
  before_action :filter_medical_consultations, only: [:generate_prescription]
  before_action :set_doctor_medical_consultation, only: [:show, :edit, :update, :destroy, :update_flag_print_diagnostic]


  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicalConsultationsDatatable.new(view_context,current_user,session[:assistant_id])}
    end
  end

  def show
  end

  def new
    @doctor_medical_consultation = MedicalConsultation.new
  end

  def edit
  end

  def create
    @doctor_medical_consultation = MedicalConsultation.new(doctor_medical_consultation_params)

    respond_to do |format|
      if @doctor_medical_consultation.save
        format.html { redirect_to @doctor_medical_consultation, notice: 'Medical consultation was successfully created.' }
        format.json { render :show, status: :created, location: @doctor_medical_consultation }
      else
        format.html { render :new }
        format.json { render json: @doctor_medical_consultation.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @doctor_medical_consultation.update(doctor_medical_consultation_params)
        format.html { redirect_to @doctor_medical_consultation, notice: 'Medical consultation was successfully updated.' }
        format.json { render :show, status: :ok, location: @doctor_medical_consultation }
      else
        format.html { render :edit }
        format.json { render json: @doctor_medical_consultation.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_cost
    med_consultation = MedicalConsultation.find(params[:consultation_id])
    is_charged = med_consultation.is_charged
    if med_consultation.get_total >= params[:new_price].to_f || params[:new_price].to_f == 0
      is_charged = true
    end

    if med_consultation.update(price: params[:new_price], is_charged: is_charged)
      render json: true, layout: false
    else
      render json: false, layout: false
    end
  end
  def update_flag_print_diagnostic
    if @doctor_medical_consultation.update(print_diagnostic: params[:flag])
      render json: true, layout: false
    else
      render json: false, layout: false
    end
  end

  def destroy
    @doctor_medical_consultation.destroy
    respond_to do |format|
      format.html { redirect_to doctor_medical_consultations_url, notice: 'Medical consultation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def send_prescription
    email = params[:email]

    medical_consultation = MedicalConsultation.find(params[:medical_consultation_id])
    pdf_name = "Receta médica "+medical_consultation.date_consultation.strftime('%d_%m_%Y')+".pdf";
    office = medical_consultation.office

    if get_doctor_session.is_active_custom_prescription
      pdf = generate_pdf_template_custom(consultation,true)
    else
      if office.template_prescription.nil?
        render :json => {:done => false, :message => "Por favor seleccione una plantilla para la receta"}, layout: false
      else
        if  office.template_prescription.id == TemplatePrescription::TEMPLATE_HORIZONTAL_A
          pdf = generate_pdf_template_a(medical_consultation,true)
        elsif office.template_prescription.id == TemplatePrescription::TEMPLATE_VERTICAL_B
          pdf = generate_pdf_template_b(medical_consultation,true)
        else
          if office.template_prescription.id == TemplatePrescription::TEMPLATE_VERTICAL_SIZE_CARTA_C
            pdf = generate_pdf_template_c(medical_consultation,true)
          end
        end

        SendPrescription.send_email(
            email,pdf,
            pdf_name,medical_consultation).deliver_now
        render :json => {:done => true, :message => "La receta fue enviada satisfactoriamente a "+email}, layout: false

      end
    end
  end



  def imc
    height = params[:height]
    height = height.to_f/100 if height.to_f > 1.84 #altura maxima en la tabla

    result = generate_imc Patient.find(params[:id_patient]), height, params[:weight].to_f.round(2)

      if result[:result]
        @table = result[:table]
        @flag_table_children = result[:flag_children]
        @flag_gender = result[:gender] if !result[:gender].nil?
        render json: {template: render_to_string("doctor/medical_consultations/imc_table_generate", :layout => false), value_suggested_weight: result[:suggested_weight], imc_real: result[:imc_real]}
      else
        render json: {error: "Error en los calculos de la tabla IMC"}
      end
  end


  def generate_pdf_template_a(consultation, attachment)
    if attachment
      pdf = render_to_string pdf: "Consulta "+consultation.medical_expedient.patient.full_name,
                   locals: { :@consultation => consultation },
                   orientation: 'Landscape', page_size: 'A5',
                   template: 'doctor/medical_consultations/template_prescription_a.html.erb', layout: false,
                   margin: { top: 6, bottom: 51, left: 10, right: 10 },
                   encoding: 'UTF-8',
                   footer:  {
                       html: {
                           template:'doctor/medical_consultations/footer_template_a.html.erb',
                           locals: {:@consultation => consultation}},
                       center: 'TEXT',
                       spacing: 1,
                       line: false}

      return pdf
    else
      render pdf: "Consulta "+consultation.medical_expedient.patient.full_name,
             locals: { :@consultation => consultation },
             orientation: 'Landscape', page_size: 'A5',
             template: 'doctor/medical_consultations/template_prescription_a.html.erb', layout: false,
             margin: { top: 6, bottom: 51, left: 10, right: 10 },
             encoding: 'UTF-8',
             footer:  {
                 html: {
                     template:'doctor/medical_consultations/footer_template_a.html.erb',
                     locals: {:@consultation => consultation}},
                 center: 'TEXT',
                 spacing: 1,
                 line: false}
    end
  end

  def generate_pdf_template_b(consultation,attachment)
    if attachment
      pdf = render_to_string pdf: "Consulta "+consultation.medical_expedient.patient.full_name,
             page_size: 'A5', locals: { :@consultation => consultation },
             template: 'doctor/medical_consultations/template_prescription_b.html.erb',
             layout: false,
             margin: { top: 6, bottom: 28, left: 10, right: 10 },
             encoding: 'UTF-8',
             footer:  {
                 html: {
                     template:'doctor/medical_consultations/footer_template_b.html.erb',
                     locals: {:@consultation => consultation}},
                 center: 'TEXT',
                 spacing: 1,
                 line: true}
      return pdf
    else
      render pdf: "Consulta "+consultation.medical_expedient.patient.full_name,
             page_size: 'A5', locals: { :@consultation => consultation },
             template: 'doctor/medical_consultations/template_prescription_b.html.erb',
             layout: false,
             margin: { top: 6, bottom: 28, left: 10, right: 10 },
             encoding: 'UTF-8',
             footer:  {
                 html: {
                     template:'doctor/medical_consultations/footer_template_b.html.erb',
                     locals: {:@consultation => consultation}},
                 center: 'TEXT',
                 spacing: 1,
                 line: true}
    end

  end

  def generate_pdf_template_c(consultation,attachment)
    if attachment
      pdf = render_to_string pdf: "Consulta "+consultation.medical_expedient.patient.full_name,
                             page_size: 'Letter',
                             locals: { :@consultation => consultation },
                             template: 'doctor/medical_consultations/template_prescription_c.html.erb',
                             layout: false,
                             margin: { top: 6, bottom: 48, left: 10, right: 10 },
                             encoding: 'UTF-8',
                             footer:  {
                                 html: {
                                     template:'doctor/medical_consultations/footer_template_c.html.erb',
                                     locals: {:@consultation => consultation}}}
      return pdf
    else
      render pdf: "Consulta "+consultation.medical_expedient.patient.full_name,
             page_size: 'Letter',
             locals: { :@consultation => consultation },
             template: 'doctor/medical_consultations/template_prescription_c.html.erb',
             layout: false,
             margin: { top: 6, bottom: 48, left: 10, right: 10 },
             encoding: 'UTF-8',
             footer:  {
                 html: {
                     template:'doctor/medical_consultations/footer_template_c.html.erb',
                     locals: {:@consultation => consultation}}}
    end
  end

  def generate_pdf_template_custom(consultation,attachment)
    template_custom = CustomTemplatePrescription.first
    top = (template_custom.header_cm * 10).to_i
    spacing = top + 1
    bottom = (template_custom.footer_cm * 10).to_i

    if attachment
      #mandar por correo
      pdf = render_to_string pdf: "Consulta "+consultation.medical_expedient.patient.full_name,
                             locals: { :@consultation => consultation, :@template_custom => template_custom },
                             orientation: template_custom.orientation, page_size: template_custom.page_size,
                             template: 'doctor/medical_consultations/custom_prescription/body.html.erb',
                             layout: false,
                             margin: { top: top, bottom: bottom, left: 0, right: 0 },
                             encoding: 'UTF-8',
                             header:  {
                                 html: {
                                     template:'doctor/medical_consultations/custom_prescription/header.html.erb',
                                     locals: {:@consultation => consultation, :@template_custom => template_custom}},
                                 spacing: spacing},
                             footer:  {
                                 html: {
                                     template:'doctor/medical_consultations/custom_prescription/footer.html.erb',
                                     locals: {:@consultation => consultation, :@template_custom => template_custom}}}
      return pdf
    else
      render pdf: "Consulta "+consultation.medical_expedient.patient.full_name,
             locals: { :@consultation => consultation, :@template_custom => template_custom },
             orientation: template_custom.orientation, page_size: template_custom.page_size,
             template: 'doctor/medical_consultations/custom_prescription/body.html.erb',
             layout: false,
             margin: { top: top, bottom: bottom, left: 0, right: 0 },
             encoding: 'UTF-8',
             header:  {
                 html: {
                     template:'doctor/medical_consultations/custom_prescription/header.html.erb',
                     locals: {:@consultation => consultation, :@template_custom => template_custom}},
                 spacing: spacing},
             footer:  {
                 html: {
                     template:'doctor/medical_consultations/custom_prescription/footer.html.erb',
                     locals: {:@consultation => consultation, :@template_custom => template_custom}}}
    end

  end

  def generate_prescription
    consultation = MedicalConsultation.find(params[:id])
    office = consultation.office
    if ClinicInfo.first.is_active_custom_prescription
      # generar receta personalizada
      respond_to do |format|
        format.pdf do
          generate_pdf_template_custom(consultation,false)
        end
      end
    else
      if office.template_prescription.nil?
        redirect_to url_for(:controller => 'doctor/offices', :action => 'index_template_prescription')
      else

        case office.template_prescription.id
          when TemplatePrescription::TEMPLATE_HORIZONTAL_A
            respond_to do |format|
              format.pdf do
                generate_pdf_template_a(consultation,false)
              end
            end

          when TemplatePrescription::TEMPLATE_VERTICAL_B
            respond_to do |format|
              format.pdf do
                generate_pdf_template_b(consultation,false)
              end
            end

          when  TemplatePrescription::TEMPLATE_VERTICAL_SIZE_CARTA_C
            respond_to do |format|
              format.pdf do
                generate_pdf_template_c(consultation,false)
              end
            end
        end
      end
    end
  end

  def has_medical_prescription_office
    result = has_template_office
    render :json => {result: result}, layout: false
  end

  def load_consultation
    @consult = MedicalConsultation.find(params[:id])
    @has_ped = doctor_has_feature Feature::PED
    render layout: false
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_doctor_medical_consultation
      @doctor_medical_consultation = MedicalConsultation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def doctor_medical_consultation_params
      params.fetch(:doctor_medical_consultation, {})
    end
end
