class Doctor::AssistantsController < ApplicationController

  include ApplicationHelper
  include Doctor::FilterRequestHelper
  include Doctor::MedicalExpedientsHelper
  before_action :filter_assistant, only: [:show, :edit]
  before_action :set_assistant, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: AssistantsDatatable.new(view_context,session[:doctor_id])}
    end
  end

  def assistant_shared_index
    respond_to do |format|
      format.html
      format.json { render json: AssistantSharedsDatatable.new(view_context)}
    end
  end

  def add_doctor_toAssistant
    assistant = Assistant.find(params[:assistant_id]);
    doctor = Doctor.find(params[:doctor_id])
    assistant.doctors << doctor
    respond_to do |format|
      format.json { render json: 'true'}
    end
  end


  def show
  end
  def check_new_user
    exist = false
    edit_value = params[:edit_value]
    user_name_tocheck = params[:user_name]
    email_to_check = params[:email]

    if edit_value != "0"
        assistant = Assistant.find(edit_value)
        if assistant.user.user_name != user_name_tocheck
          if User.exists?(user_name: user_name_tocheck)
            exist = true
          end
        end
        if assistant.user.email != email_to_check
          if User.exists?(email: email_to_check)
            exist = true
          end
        end
    else
      if User.exists?(user_name: user_name_tocheck) || User.exists?(email: email_to_check)
        exist = true
      end
    end
    respond_to do |format|
      format.html { render json: {:result => exist}}
    end
  end
  def new
    @permissions = Hash.new
    @text_button = "Guardar"
    @doctor = Doctor.find(current_user.doctor.id)
    @assistant = Assistant.new
    @assistant.build_user
    @assistant.build_city
  end

  def edit
    @doctor = Doctor.find(current_user.doctor.id)
    @text_button = "Guardar"

    initialize_assistant_permisions
  end
  def initialize_assistant_permisions
    @permissions = Hash.new
    @assistant.office_permissions.each do |op|
      @permissions[op.office_id.to_s+'-'+op.permission_id.to_s] = true
    end
  end
  def create
    @text_button = "Guardar"
    @assistant = Assistant.new(assistant_params)
    @doctor = Doctor.find(current_user.doctor.id)
    set_city_model params, @assistant
    respond_to do |format|
      if @assistant.save
        @assistant.doctors << Doctor.find(params[:doctor_id])
        Assistant.update(@assistant.id,{office_permissions: get_office_permissions})

        if params[:expedient_p].present?
          expedient_permissions = ExpedientPermission.find(params[:expedient_p])
          @assistant.update(expedient_permissions: expedient_permissions )
        end

        format.html { redirect_to doctor_assistants_url, notice: 'Se a creado exitosamente un asistente.' }
        format.json { render :show, status: :created, location: @assistant }
      else
        initialize_assistant_permisions
        @assistant.build_city
        format.html { render :new }
        format.json { render json: @assistant.errors, status: :unprocessable_entity }
      end
    end
  end

  def get_office_permissions
    a_permissions = []
    offices = Office.active_offices.where('doctor_id = '+session[:doctor_id].to_s)
    offices.each do |o|
      if params['office_'+o.id.to_s] != nil
        permissions = params['office_'+o.id.to_s];
        permissions.each do |p|
          office_permission = OfficePermission.new
          #office_permission.assistant_id = @assistant.id
          office_permission.office_id = o.id
          office_permission.permission_id = p
          a_permissions.push(office_permission)
        end
      end
    end
    a_permissions
  end

  def update
    @text_button = "Guardar"
    @city_name = params[:city_name]
    @doctor = Doctor.find(current_user.doctor.id)
    respond_to do |format|
      if @assistant.update(assistant_params)
        set_city_model params, @assistant
        @assistant.update(city_id: @assistant.city_id)
        if params[:change_permissions] != nil
          OfficePermission.where(:assistant_id => @assistant.id).delete_all
          Assistant.update(@assistant.id,{office_permissions: get_office_permissions})
        end

        if params[:change_permissions_expedient].present?
          @assistant.expedient_permissions.delete_all
          if params[:expedient_p].present?
            expedient_permissions = ExpedientPermission.find(params[:expedient_p])
            @assistant.update(expedient_permissions: expedient_permissions )
          end
        end

        format.html { redirect_to doctor_assistants_path, notice: 'Asistente acuatializado correctamente' }
        format.json { render :show, status: :ok, location: @assistant }
      else
        initialize_assistant_permisions
        @assistant.build_city
        format.html { render :edit }
        format.json { render json: @assistant.errors, status: :unprocessable_entity }
      end
    end
  end

  def get_assistant_data
    permision = []
    assist = []
    assistant = Assistant.find(params[:assistant_id])
    assistant.office_permissions.each do |per|
      permision.push(
                   {
                       office: per.office.name,
                       permision: per.permission.label,
                       code: per.permission.id
                   }
      )
    end
    bith_day_asssis = ""
    if assistant.birth_day != nil
      bith_day_asssis = assistant.birth_day.strftime('%d/%m/%Y')
    end
    assist.push(
              {
                  name:assistant.name,
                  last_name:assistant.last_name,
                  birth_day: bith_day_asssis,
                  telephone: assistant.telephone,
                  cellphone: assistant.cellphone,
                  email: assistant.user.email,
                  address: assistant.address,
                  gender: assistant.gender.name,
                  city: assistant.city.name+","+assistant.city.state.name+","+assistant.city.state.country.name
              }
    )
    render :json => {:assist => assist, :permis => permision, :expedient_permissions => assistant.expedient_permissions.map{|ep| {id: ep.id, name: ep.name}}
    }, layout: false
  end

  def destroy
    @assistant.user.destroy
    @assistant.destroy
    respond_to do |format|
      format.html { redirect_to doctor_assistants_url, notice: 'Se a eliminado correctamente el asistente' }
      format.json { head :no_content }
    end
  end

  private
  def set_assistant
    @assistant = Assistant.find(params[:id])
  end

  def assistant_params
    params.require(:assistant).permit(:address, :birth_day, :cellphone, :gender, :name, :gender_id,
                                      :last_name,:shared, :telephone, :city_id, user_attributes:[:id, :user_name, :password, :email])
  end
end
