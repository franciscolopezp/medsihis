class Doctor::RespiratoryApsController < ApplicationController

  before_action :set_respiratory_ap, only: [:update]

  def index
  end

  def create
    @respiratory = RespiratoryAp.new(respiratory_ap_params)
    respond_to do |format|
      if @respiratory.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @respiratory.id}}
      else
        format.html { render json: {:done => false, :errors => @respiratory.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @respiratory.update(respiratory_ap_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @respiratory.errors.full_messages}}
      end
    end
  end

  private
  def set_respiratory_ap
    @respiratory = RespiratoryAp.find(params[:id])
  end
  def respiratory_ap_params
    params.require(:respiratory_ap).permit(:description, :medical_history_id, :cough, :chest_pain, :dyspnoea, :hemoptisis, :epistaxis, :cyanosis)
  end
end

