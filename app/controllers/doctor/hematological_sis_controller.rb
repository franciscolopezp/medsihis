class Doctor::HematologicalSisController < ApplicationController

  before_action :set_hematological_si, only: [:update]

  def index
  end

  def create
    @hematological = HematologicalSi.new(hematological_si_params)
    respond_to do |format|
      if @hematological.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @hematological.id}}
      else
        format.html { render json: {:done => false, :errors => @hematological.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @hematological.update(hematological_si_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @hematological.errors.full_messages}}
      end
    end
  end

  private
  def set_hematological_si
    @hematological = HematologicalSi.find(params[:id])
  end
  def hematological_si_params
    params.require(:hematological_si).permit(:description, :astenia,:palidez,:hemorrhages,:adenopathies,:medical_history_id)
  end
end