class Doctor::UrologicalBackgroundsController < ApplicationController
  before_action :set_urological_background, only: [:update]

  def create
    urological_bk = UrologicalBackground.new(urological_background_params)
    if urological_bk.save
      render json: {:done => true}
    else
      render json: {:done => false}
    end
  end

  def update
    respond_to do |format|
      if @uro_bk.update(urological_background_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @uro_bk.errors.full_messages}}
      end
    end
  end
  
  private
  def urological_background_params
    params.require(:urological_background).permit(
        :medical_history_id, :testicular_pain,
    :impotence,:erection_dificulty,:premature_ejaculation,
    :vaginal_discharge,:anormal_hair_growth,:back_pain,:kidneys_pain,:pelvic_pain,:low_abdomen_pain,:kidney_bk,:kidney_stones,:venereal_diseases,
    :self_examination,:prostate, :last_prostate_examination,
    :fum,:gestations,:last_pap_date,:contraceptive_method,:sexually_active,
    :sex_life_start,:sexual_partners,:relationship_type,:satisfying_sex,
    :intercourse_pain)
  end

  def set_urological_background
    @uro_bk = UrologicalBackground.find(params[:id])
  end

end