class Doctor::GynecologistObstetricalBackgroundsController < ApplicationController

  def create
    ginecologist_obstetrical_bk = GynecologistObstetricalBackground.new(get_params)
    ginecologist_obstetrical_bk.save

    respond_to do |format|
      msg = {:done => true , :message => "Datos guardados correctamente"}
      format.html { render :json => msg  }
    end

  end

  def update
    ginecologist_obstetrical_bk = GynecologistObstetricalBackground.find(params[:id])
    ginecologist_obstetrical_bk.update(get_params)
    respond_to do |format|
      msg = {:done => true , :message => "Datos actualizado correctamente"}
      format.html { render :json => msg  }
    end

  end

  private
  def get_params
    params.require(:gynecologist_obstetrical_background).permit(
        :medical_history_id,
        :menstruation_start_age,:ivs,:fup,:married,:spouse_name,
        :gestations,:natural_birth,
        :caesarean_surgery,:abortion_number,:abortion_causes,
        :sexually_active,:sexual_partners,:relationship_type,
        :std,:contraceptives_method,:menopause,:anotations,
        :information_spouse,:menses_last_date,
        :papanicolau_last_date,:cycles)
  end
end