class Doctor::PatientFiscalInformationsController < ApplicationController

  before_action :set_fiscal_information, only: [:show, :edit, :update, :destroy]

  # GET /doctors/vaccines
  # GET /doctors/vaccines.json
  def index
    #respond_to do |format|
     # format.html
      #format.json { render json: ApplicationAgesDatatable.new(view_context)}
    #end
  end


  def show
  end

  # GET /doctors/vaccines/new
  def new
    @text_button = "Guardar"
    @p_fiscal_information = PatientFiscalInformation.new
    @p_fiscal_information.build_patient
  end

  # GET /doctors/vaccines/1/edit
  def edit
    @text_button = "Guardar"
  end

  # POST /doctors/vaccines
  # POST /doctors/vaccines.json
  def create
    @text_button = "Guardar"
    @p_fiscal_information = PatientFiscalInformation.new(fiscal_info_params)

    respond_to do |format|
      if @p_fiscal_information.save
        msg = {:status => "success" , :message => "Se agregó la informacion fiscal"}
        format.json { render :json => msg  }
      else
        msg = {:status => "error" , :message => "Hubo un error"}
        format.json { render :json => msg  }
      end
    end
  end

  # PATCH/PUT /doctors/vaccines/1
  # PATCH/PUT /doctors/vaccines/1.json
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @p_fiscal_information.update(fiscal_info_params)
        msg = {:status => "success" , :message => "Se actualizó la informacion fiscal"}
        format.json { render :json => msg  }
      else
        msg = {:status => "error" , :message => "Hubo un error"}
        format.json { render :json => msg  }
      end
    end
  end

  # DELETE /doctors/vaccines/1
  # DELETE /doctors/vaccines/1.json
  def destroy
    @p_fiscal_information.destroy
    respond_to do |format|
      format.html { redirect_to doctor_patient_fiscal_informations_url, notice: 'fiscal info was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_fiscal_information
    @p_fiscal_information = PatientFiscalInformation.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fiscal_info_params
    params.require(:patient_fiscal_information).permit(:person_type, :rfc,:business_name, :address, :ext_number, :int_number,:suburb ,:locality, :zip, :patient_id, :city_id,:email,:email2)
  end
end
