class Doctor::CabinetReportsController < ApplicationController

  include Doctor::CabinetReportHelper
  before_action :set_report, only: [:show, :edit, :update, :destroy]


  def new
    @cabinetReport = CabinetReport.new
  end

  def edit
  end

  def save_cabinet_report
    last_folio = MedicalExpedient.joins(:clinical_analysis_orders).where(doctor_id: session[:doctor_id]).maximum("clinical_analysis_orders.folio")
    if last_folio.nil?
      next_folio = 1
    else
      next_folio = last_folio.to_i + 1
    end
    clinical_order = ClinicalAnalysisOrder.new(folio:next_folio,annotations:"",result:"",date_order:Time.now.to_date, office_id: params[:office_id], laboratory_id: params[:laboratory_id],user_id: current_user.id )
    clinical_order.medical_expedient = MedicalExpedient.find(params[:expedient_id])
    clinical_order.clinical_order_type = ClinicalOrderType.find(2)
    clinical_studies = []
    clinical_studies << ClinicalStudy.find(params[:clinical_study_cabinet])
    clinical_order.clinical_studies = clinical_studies
    annos = JSON.parse(params[:annotations])
    images = JSON.parse(params[:images])


    if clinical_order.save!
      price_order = params[:price]
      clinical_order_report = CabinetReport.new(clinical_analysis_order_id: clinical_order.id,conclusions: params[:report_sumary], is_charged: false, price: price_order,invoiced:0,user_id: current_user.id)
      @clinical_order = ClinicalAnalysisOrder.find(clinical_order.id)
      if clinical_order_report.save
        #guardar imagenes adjuntas
        annos.each.with_index do |val,index|
          images_report = CabinetReportPicture.new(cabinet_report_id:clinical_order_report.id,annotations:val)
          if images_report.save!
                save_picture_report(images[index],clinical_order.id,index.to_s,images_report)
          end
        end
        render :json => {ok: "true", url_print: download_analysis_doctor_cabinet_reports_url(@clinical_order, format: :pdf)}
      else
        render :json => {ok: "false"}
      end
    else
      render :json => {ok: "false"}
    end

    #url_print: download_analysis_doctor_analysis_order_index_url(result[:analysis], format: :pdf)

  end

  def print_report_analysis_order

    if get_doctor_session.is_active_custom_report
    template_custom = get_doctor_session.custom_template_report
    top = (template_custom.header_cm * 10).to_i
    spacing = top + 1
    bottom = (template_custom.footer_cm * 10).to_i

    @clinical_order = ClinicalAnalysisOrder.find(params[:id])
    respond_to do |format|
      format.pdf do
        render pdf: "Reporte_gabinete "+@clinical_order.medical_expedient.patient.full_name,
               page_size: 'Letter',
               locals: { :@analysis_order => @clinical_order },
               template: 'doctor/analysis_order/report_body_pdf.html.erb',
               layout: false,
               margin: { top: top, bottom: bottom, left: 15, right: 15  },
               encoding: 'UTF-8',
               header: {
                   spacing: spacing,
                   html: {
                       template: 'doctor/analysis_order/custom_report/header.html.erb',
                       locals: { :@analysis_order => @clinical_order, :@template_custom => template_custom}
                   }},
               footer:  {
                   html: {
                       template:'doctor/analysis_order/custom_report/footer.html.erb',
                       locals: {:@analysis_order => @clinical_order, :@template_custom => template_custom}}
               }
      end
    end
    else
      @clinical_order = ClinicalAnalysisOrder.find(params[:id])
      respond_to do |format|
        format.pdf do
          render pdf: "Reporte_gabinete "+@clinical_order.medical_expedient.patient.full_name,
                 page_size: 'Letter',
                 locals: { :@analysis_order => @clinical_order },
                 template: 'doctor/analysis_order/report_body_pdf.html.erb',
                 layout: false,
                 margin: { top: 45, bottom: 21, left: 15, right: 15 },
                 encoding: 'UTF-8',
                 header: {
                     :spacing => 35,
                     html: {
                         template: 'doctor/analysis_order/report_header_pdf.html.erb',
                         locals: { :@analysis_order => @clinical_order}
                     }},
                 footer:  {
                     html: {
                         template:'doctor/analysis_order/report_footer_pdf.html.erb',
                         locals: {:@analysis_order => @clinical_order}}
                 }
        end
      end
    end
  end

  def create
    #create_office params
    respond_to do |format|
      msg = {:status => "ok" , :message => "Se agregó el reporte"}
      format.json { render :json => msg  }
    end
  end

  def update
    #update_office params
    respond_to do |format|
      msg = {:status => "ok" , :message => "Se actualizó el reporte"}
      format.json { render :json => msg  }
    end
  end

  def update_price

    report = CabinetReport.find(params[:report_id])
    report.update({:price => params[:price]})

    if report.get_balance == 0
      sql = "update cabinet_reports set is_charged = true where id = "+params[:report_id]
      ActiveRecord::Base.connection.execute(sql)
    end

    respond_to do |format|
      msg = {:status => "ok" , :message => "Se actualizó el costo del reporte"}
      format.html { render :json => msg  }
    end


  end

  def destroy
    #@cabinetReport.update({active:false})
=begin
    respond_to do |format|
      format.html { redirect_to doctor_offices_path, notice: 'Consultorio eliminado satisfactoriamente'}
      format.json { head :no_content}
    end
=end
  end





  private
  def set_report
    @cabinetReport = CabinetReport.find(params[:id])
  end


end