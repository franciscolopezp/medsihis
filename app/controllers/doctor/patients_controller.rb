class Doctor::PatientsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  #load_and_authorize_resource
  #skip_authorize_resource :only => [:create,:update]
  include Doctor::PatientsHelper
  include Doctor::FilterRequestHelper
  include Doctor::MedicalExpedientsHelper
  before_action :filter_patient, only: []
  before_action :set_doctor_patient, only: [:show, :edit, :update, :destroy]

  skip_before_filter :verify_authenticity_token

  def index
    has_ped_doctor = true #doctor_has_feature Feature::PED
    has_gin_doctor = true #doctor_has_feature Feature::GIN
    respond_to do |format|
      format.html
      format.json { render json: PatientsDatatable.new(view_context)}
    end
  end

  def show
  end
  def check_patient_exist
    exist = false
    patients_found = []
    name = params[:name]
    last_name = params[:last_name]
    patient_id = params[:patient_id]
    if !patient_id.present? && name.present?
      filter_name = ''
      name.split(' ').each do |text|
        filter_name << " name LIKE "+"'%"+text.to_s+"%'" +' OR '
      end
      filter_name = filter_name.first(-4)
      patients_founded = Patient.where(filter_name + " AND last_name like '%#{last_name}%'")
      if patients_founded.count > 0
        patients_founded.each do |patient|
          patients_found.push(patient)
        end
        exist = true
      else
        exist = false
      end
    end
    respond_to do |format|
      format.html { render json: {:done => exist, :patients_founded => patients_found}}
    end
  end
  def create
    @doctor_patient = Patient.new(doctor_patient_params)
    set_city_patient params
    save_patient_folio(current_user,params[:patient][:gender_id])
    respond_to do |format|
      if @doctor_patient.save
        save_default_backgrounds @doctor_patient
        if !params[:picture64].nil? && params[:picture64]!=""
          save_picture_cam params[:picture64]
        end
        if !params[:crop_image_64].nil? && params[:crop_image_64]!=""
          save_picture_cam params[:crop_image_64]
        end
        format.html {
          if current_user.is_doctor
            redirect_to doctor_expedient_path(@doctor_patient.medical_expedient.id), notice: t('controller.record_created')
          else
            redirect_to doctor_patients_path, notice: t('controller.record_created')
          end
        }
        format.json { render :show, status: :created, location: @doctor_patient }
      else
        @doctor_patient.build_city
        format.html { render :new }
        format.json { render json: @doctor_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @city_name = params[:city_name]
      @text_button = "Actualizar"
      if @doctor_patient.update(doctor_patient_params)
        set_city_patient params
        @doctor_patient.update(city_id: @doctor_patient.city_id)

        if !params[:picture64].nil? && params[:picture64]!=""
          save_picture_cam params[:picture64]
        end

        if !params[:crop_image_64].nil? && params[:crop_image_64]!=""
          save_picture_cam params[:crop_image_64]
        end
        if params[:update_from_expediente]!=nil
          format.html { redirect_to doctor_search_expedient_path(@doctor_patient.medical_expedient), notice: t('controller.record_updated') }
        else
          format.html { redirect_to doctor_patients_path, notice: t('controller.record_updated') }
          format.json { render :show, status: :ok, location: @doctor_patient }
        end
      else
        @doctor_patient.build_city
        format.html { render :edit }
        format.json { render json: @doctor_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /doctors/patients/1
  # DELETE /doctors/patients/1.json
  def destroy
    @doctor_patient.destroy
    delete_picture @doctor_patient.picture
    respond_to do |format|
      format.html { redirect_to doctor_patients_url, notice: t('controller.record_archived') }
      format.json { head :no_content }
    end
  end



  def find_patients
    str = params[:term]
    render :json => search_patients(str,10).to_a
  end
  @vaccines_books = VaccineBook.all
  @allergies = Allergy.all



  def inactive_patients_table
    respond_to do |format|
      format.html
      format.json { render json: PatientsDatatable.new(view_context)}
    end
  end

  # GET /doctors/patients/new
  def new
    @doctor_patient = Patient.new
    @doctor_patient.build_city
    @text_button = "Agregar"
    @doctor_id = session[:doctor_id]
    @has_gin = doctor_has_feature Feature::GIN
    @is_add = true
  end

  def doctor_has_feature feature_key
=begin
    session[:doctor_features].each do |f|
      if f == feature_key
        return true
      end
    end
    false
=end
 true
  end

  def calculate_age
    respond_to do |format|
      format.html { render json: {:age => CalculateAge::calculate(view_context,Time.parse(params[:date]))}}
    end
  end

  # GET /doctors/patients/1/edit
  def edit
    @text_button = "Actualizar"
    @is_add = false
  end

  def picture
    @doctor_patient = Patient.find(params[:patient_id])
    save_picture_cam params[:patient_picture]
    respond_to do |format|
      msg = {:status => "ok" , :message => "Se agregó la foto"}
      format.json { render :json => msg  }
    end
  end
  def inactive_patients
  end
  def disable_patient
    patient_to_inactive = Patient.find(params[:patient_id])
    me_to_inactive = patient_to_inactive.medical_expedient

    patient_to_inactive.update(active: params[:active])
    me_to_inactive.update(active: params[:active])

    render json: {:done => true}
  end


  def download_image
    patient = Patient.find(params[:id])
    send_file patient.picture,  :type => 'image/jpeg', :disposition => 'inline'
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_doctor_patient
      @doctor_patient = Patient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def doctor_patient_params
      params.require(:patient).permit(:address, :birth_day, :blood_type_id, :cell, :email, :gender, :is_foreign,:city_unknown, :name, :last_name,
                                      :phone,  :city_id, :vaccinations, :birthplace,:insurance_id,
                                      :user_id, :gender_id, :occupation, :active, :allergy_ids => [])
    end
end
