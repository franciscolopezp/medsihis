class Doctor::OphthalmologicalSisController < ApplicationController

  before_action :set_ophthalmological_si, only: [:update]

  def index
  end

  def create
    @ophthalmological = OphthalmologicalSi.new(ophthalmological_si_params)
    respond_to do |format|
      if @ophthalmological.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @ophthalmological.id}}
      else
        format.html { render json: {:done => false, :errors => @ophthalmological.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @ophthalmological.update(ophthalmological_si_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @ophthalmological.errors.full_messages}}
      end
    end
  end

  private
  def set_ophthalmological_si
    @ophthalmological = OphthalmologicalSi.find(params[:id])
  end
  def ophthalmological_si_params
    params.require(:ophthalmological_si).permit(:agudeza,:diplopia,:eye_pain,:photofobia,:amaurosis,:fotopsias,:miodesopsias,:picor,:pitanas,:medical_history_id)
  end
end