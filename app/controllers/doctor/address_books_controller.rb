class Doctor::AddressBooksController < ApplicationController

  before_action :set_address_book, only: [:update,:destroy,:edit]

  def index
    respond_to do |format|
      format.html
      format.json { render json: AddressBooksDatatable.new(view_context,session[:doctor_id],session[:current_office_id])}
    end
  end
  def create
    @address_book = AddressBook.new(address_book_params)
    respond_to do |format|
    if @address_book.save
      @address_book.update(doctor_id: session[:doctor_id])
      if params[:office_id] != "0"
        @address_book.update(office_id: params[:office_id])
      end
      if params[:hospital_id] != "0"
        @address_book.update(hospital_id: params[:hospital_id])
      end
      format.html { redirect_to doctor_address_books_path, notice: 'El contacto fue agregado con exito'}
      format.json { render show, status: :ok, location: @address_book}
    else
      format.html { render :new}
      format.json { render json: 'error', status: :unprocessable_entity}
    end
    end
  end
  def new
    @address_book = AddressBook.new
  end
  def edit
    @text_button = "Guardar"
  end
  def update
    if @address_book.update(address_book_params)
      if params[:office_id] != "0"
        @address_book.update(office_id: params[:office_id])
      end
      if params[:hospital_id] != "0"
        @address_book.update(hospital_id: params[:hospital_id])
      end
      respond_to do |format|
        format.html { redirect_to doctor_address_books_path, notice: 'El contacto fue agregado con exito'}
        format.json { render show, status: :ok, location: @address_book}
      end
    else
      respond_to do |format|
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @address_book.destroy
    respond_to do |format|
      format.json { render json: {:success => true, :message => "Se eliminó el dato",:data => @address_book}}
    end
  end

  def get_contact_data
    data = AddressBook.find(params[:data_id])
    data_array = []
    if data.office != nil
      office_name = data.office.name
    else
      office_name = "Todos"
    end
    if data.hospital != nil
      hospital_name = data.hospital.name
    else
      hospital_name = "Ninguno"
    end
    data_array.push({
                          name: data.name,
                          hospital: hospital_name,
                          telephone: data.telephone,
                          office: office_name,
                          cellphone: data.cellphone,
                          address: data.address,
                          email: data.email
                      })
    render :json => {:data =>data_array}, layout: false
  end
  private
  def set_address_book
    @address_book = AddressBook.find(params[:id])
  end

  def address_book_params
    params.require(:address_book).permit(:name, :telephone, :cellphone,:email,:address)
  end
end