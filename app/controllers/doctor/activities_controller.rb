class Doctor::ActivitiesController < ApplicationController

  before_action :set_activity, only: [:update, :destroy,:show]
  include Doctor::ActivitiesHelper
  include Doctor::DashboardHelper
  include ApplicationHelper

  def index
    result = get_activities_response
    respond_to do |format|
      format.html { render json: result}
    end
  end

  def show
    act = json_from_model @activity
    respond_to do |format|
      format.json { render json: act}
    end

  end

  def new
    @activity = Activity.new
  end

  def create
    @activity = Activity.new(activity_params)

    start_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:activity][:start_date])
    end_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:activity][:end_date])

    @activity.start_date = start_date
    @activity.end_date = end_date

    if session[:role] == 'doctor'
      @activity.owner_type = 'Doctor'
      @activity.owner_id = Doctor.find_by(user_id: session[:user_id]).id
    elsif session[:role] == 'assistant'
      @activity.owner_type = 'Assistant'
      @activity.owner_id = Assistant.find_by(user_id: session[:user_id]).id
    end

    @activity.doctor_id = session[:doctor_id].nil? ? params[:doctor_id] : session[:doctor_id]

    info_personal_support = false
    if @activity.activity_type.id == ActivityType::SURGERY
      if !params[:activity][:personal_supports].blank?
        info_personal_support = true
        params[:activity][:personal_supports].each do |value|
          @activity.personalsupports << Personalsupport.find(value.to_i)
        end
      end
    end
    data = {}
    if @activity.save
      offices = get_offices params
      patients = get_patients params
      Activity.update(@activity.id, :offices => offices, :patients => patients)
      @activity = Activity.find(@activity.id)
      doctor = Doctor.find(params[:doctor_id])
      abbreviation = session[:doctor_id].nil? ?  abbreviation_dr_by_doctor(doctor) : abbreviation_dr
      if patients.count > 0
        if patients.first.email != "" &&  patients.first.email != nil
          AppointmentMail.scheduled(@activity).deliver_now
        end
      end
      @activity.personalsupports.each{ |personal| InfoPersonalSupports.send_message(personal,abbreviation,doctor,@activity).deliver } if info_personal_support
      data = json_from_model @activity
    end

    @data = { :type => 'new_activity', :activity => data, :in_w_room => false , :is_new => true}


    if @activity.activity_status.id == ActivityStatus::WAITING && @activity.start_date.today?
      @data["in_w_room"] = true;
      @data["w_room_data"] = format_activity_wroom(@activity);
    end

=begin
    key = session[:doctor_username].nil? ? Doctor.find(params[:doctor_id]).user.user_name : session[:doctor_username]
    notification_private_web key, @data
=end



    render :json => {done: true}, layout: false
  end

  def resend_surgery_email
    activity = Activity.find(params[:activity_id])
    persons = Personalsupport.where('id IN (?)',params[:data])
    persons.each{ |personal| InfoPersonalSupports.send_message(personal,abbreviation_dr,get_doctor_session,activity).deliver }

    render :json => {:done => true, :message => 'Se reenviaron los correos'}, layout: false
  end

  def update
    # la actividad puede cambiar de doctor cuando se encuentre en el asistente compartido, en caso
    # de que cambie se toma el id del doctor en params, en caso contrario en session
    @activity.doctor_id = session[:doctor_id].nil? ? params[:doctor_id] : session[:doctor_id]

    start_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:activity][:start_date])
    end_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:activity][:end_date])
    last_start_date = @activity.start_date
    last_end_date = @activity.end_date
    if @activity.update(activity_params)
      @activity.update({:start_date => start_date , :end_date => end_date})
      patients = get_patients params
      Activity.update(@activity.id,:patients => patients)
      if params[:activity][:activity_status_id].to_i == ActivityStatus::CANCELED
        if patients.count > 0
          if patients.first.email != "" &&  patients.first.email != nil
            AppointmentMail.cancel(@activity).deliver_now
          end
        end
      elsif params[:activity][:activity_status_id].to_i == ActivityStatus::SCHEDULED
        if patients.count > 0
          if patients.first.email != "" &&  patients.first.email != nil
            if last_start_date != @activity.start_date || last_end_date != @activity.end_date
            AppointmentMail.reschedule(@activity).deliver_now
            end
          end
        end
      end
      if params[:is_owner] == 'true'
        offices = get_offices params
        Activity.update(@activity.id, :offices => offices)
      end

      @activity.personalsupports.destroy_all
      if @activity.activity_type.id == ActivityType::SURGERY
        if !params[:activity][:personal_supports].blank?
          params[:activity][:personal_supports].each do |value|
            @activity.personalsupports << Personalsupport.find(value.to_i)
          end
        end
      end

      @activity = Activity.find(@activity.id)
      act = json_from_model @activity
    end

    @data = { :type => 'new_activity', :activity => act, :in_w_room => false, :is_new => false}

    if @activity.activity_status.id == ActivityStatus::WAITING  && @activity.start_date.today?
      @data["in_w_room"] = true;
      @data["w_room_data"] = format_activity_wroom(@activity);
    end

=begin
    key = session[:doctor_username].nil? ? Doctor.find(params[:doctor_id]).user.user_name : session[:doctor_username]
    notification_private_web key, @data
=end

    render :json => {done: true}, layout: false
  end

  def update_status

    @activity = Activity.find(params[:activity_id])
    @activity.update({:activity_status_id => params[:activity_status_id] })

    old_act = nil
    if params[:old_in_consult].present?
      Activity.update(params[:old_in_consult],{:activity_status_id => ActivityStatus::WAITING})
      aux = Activity.find(params[:old_in_consult])
      old_act = json_from_model aux
    end

    flag_schedule = false
    date_before = nil
    if params[:activity_status_id].to_i == ActivityStatus::SCHEDULED && params[:new_schedule] == 'true'
      flag_schedule = true
      date_before = @activity.start_date

      start_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:start_date])
      end_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:end_date])

      @activity.update({:start_date => start_date, :end_date => end_date})
    end
    #if assistant or doctor approve or cancel a requested appointment send an email
    user_patient = @activity.user_patients.first
    if params[:send_email] != nil && params[:send_email] == 'true' && user_patient != nil
      if params[:activity_status_id].to_i == ActivityStatus::SCHEDULED && params[:new_schedule] == 'true' #la cita ha sido reagendada
        AppointmentNotification.new_schedule(@activity).deliver_now
        @type_message = 3
      elsif params[:activity_status_id].to_i == ActivityStatus::SCHEDULED && params[:new_schedule] != 'true' # la cita fue confirmada
        AppointmentNotification.confirm(@activity).deliver_now
        @type_message = 1
      elsif params[:activity_status_id].to_i == ActivityStatus::CANCELED
        @type_message = 5
        AppointmentNotification.cancel(@activity,params[:message]).deliver_now
      end
    end

    doctor_username = @activity.doctor.user.user_name

    act = json_from_model @activity

    @data = {
        :type => params[:dragged] == 'true' ? 'new_wroom_status' : 'new_activity',
        :activity => act,
        :in_w_room => false,
        :is_new => false,
        :dragged => params[:dragged] == 'true' ? true : false,
        :prev_in_cosult => old_act
    }

    if @activity.activity_status_id == ActivityStatus::WAITING  && @activity.start_date.today?
      @data["in_w_room"] = true
      @data["w_room_data"] = format_activity_wroom(@activity)
    end

    # notificar al doctor de la nueva cita en web y la app
    if @activity.user_patients.size > 0
      # se notifica al paciente de la app y al doctor
      notification_doctor_and_patient doctor_username, @data, @activity.user_patients.first.user, @activity, flag_schedule, date_before
    else
      # se notifica solo al doctor
      notification_private_web doctor_username, @data
    end

    if @type_message.nil?
      render :json => {done: true}, layout: false
    else
      render :json => {done: true, type_message_appointment: @type_message}, layout: false
    end

  end

  def update_dates

    start_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:start_date])
    end_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:end_date])



    if params[:calendar] == "doctor"
      @activity = Activity.find(params[:activity_id])
      @activity.update({:start_date => start_date, :end_date => end_date })
    elsif params[:calendar] == "asset"
      @activity = AssetActivity.find(params[:activity_id])
      @activity.update({:start_date => start_date, :end_date => end_date })
    end

    render :json => {done: true}, layout: false
  end

  def destroy
    act = @activity
    @activity.destroy

=begin
    @data2 = nil

    @data = { :type => 'delete_activity', :id => params[:id], :activity => act }

    channel = session[:doctor_username].nil? ? Doctor.find(params[:doctor_id]).user.user_name : session[:doctor_username]

    notification_private_web channel, @data
=end

    render :json => {done: true}, layout: false
  end

  def validate_rank_date
    start_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:start_date])
    end_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:end_date])

    activity_id = params[:activity_id]


    exist_date = {}
    doctor = params[:doctor_id].nil? ? get_doctor_session : Doctor.find(params[:doctor_id])

    result = exist_rank_date start_date,end_date, doctor,activity_id
    if result[:result]
      exist_date[:result] = true
      exist_date[:message] = "Existe una actividad ya programada en el mismo horario."
      exist_date[:activity] = result[:activity]
    else
      exist_date[:result] = false
    end

    render :json => exist_date, layout: false
  end

  def get_day_activities
    y = params[:year].to_i
    m = params[:month].to_i
    d = params[:date].to_i
    m = m+1
    date = Date.new(y,m,d)

    day_of_week = date.strftime('%w')
    offices_times = OfficeTime.where('day = ? and office_id = ?',day_of_week,session[:current_office_id])

    stime = nil
    etime = nil

    offices_times.each do |ot|
      if stime.nil? || ot.start < stime
        stime = ot.start
      end
      if etime.nil? || ot.end > etime
        etime = ot.end
      end
    end

    if stime.nil?
      stime = Time.new(2000, 1, 1, 0, 0, 0)
    end

    if etime.nil?
      etime = Time.new(2000, 1, 1, 23, 30, 0)
    end

    start_time = stime.strftime('%H:%M:%S')
    end_time = etime.strftime('%H:%M:%S')

    records = get_activities_response

    if records.length > 0

      first = records[0]
      last = records[records.length - 1]

      if first[:start].strftime('%H%M%S').to_i < stime.strftime('%H%M%S').to_i
        start_time = first[:start].strftime('%H:%M:%S')
      end

      end_time = last[:end].strftime('%H%M').to_i < 2300 ? (last[:end] + 1.hour).strftime('%H:%M:%S') : last[:end].strftime('%H:%M:%S')

    end

    data = Hash.new
    data['activities'] = records
    data['start_h'] = start_time
    data['end_h'] = end_time
    render :json => data, layout: false
  end

  private
  def get_activities_response
    is_agenda = params[:is_agenda] == 'true' ? true : false

    activities = Activity.where(get_query_activities params)

    activities = activities.order('activities.start_date ASC')

    result = []
    activities.each do |a|
      if is_agenda
        act = fullcalendar_activity a
      else # maybe is requested from waiting room page
        act = json_from_model a
      end
      result.push(act)
    end

    result
  end


  def set_activity
    @activity = Activity.find(params[:id])
  end

  def activity_params
    params.require(:activity).permit(:name, :details, :start_date, :end_date, :activity_type_id, :all_day, :activity_status_id)
  end
end
