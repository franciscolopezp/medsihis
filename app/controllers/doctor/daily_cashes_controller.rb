class Doctor::DailyCashesController < ApplicationController
  load_and_authorize_resource :only => [:index,:history]
  include Doctor::DailyCashesHelper

  def index
    @show_add_daily_cash_btn = true
    local_accounts = current_user.accounts.joins(:bank).where('banks.is_local = 1')

    @daily_cashes = DailyCash.where('account_id IN (?) and status = ?', local_accounts.map {|a| a.id}, DailyCash::OPEN)
    @accounts = local_accounts
    @all_accounts = current_user.accounts

    @daily_cashes.each do |dc|
      if dc.user_id == session[:user_id]
        @show_add_daily_cash_btn = false
      end
    end

    @transaction_types = TransactionType.all
    @movement_types = MovementType::group_by_category
    @payment_methods = PaymentMethod.all

  end

  def create
    done = true
    error = ''
    message = ''
    daily_cash  = DailyCash.open_by_account(params[:account_id]).first
    if daily_cash == nil
      open_daily_cash params[:account_id]
    else
      done = false
      error = 'Existe un corte activo'
      message = 'No es posible tener dos cortes activos en la misma cuenta.'
    end

    respond_to do |format|
      format.html { render json: {:done => done, :message => message, :error => error}}
    end

  end

  def update
    resp = {
        'done' => true,
        'url_print' => daily_cash_pdfs_path(params[:id]),
        'error' => '',
        'message' => ''
    }

    e_balance = {
        'enough' => true
    }

    if params[:option].to_i != DailyCash::CLOSE_ONLY
      e_balance = check_enough_balance
    end

    if e_balance['enough']
      close_dcash params
    else
      resp['done'] = false
      resp['error'] = e_balance['error']
      resp['message'] = e_balance['message']
    end
    respond_to do |format|
      format.html { render json: resp}
    end
  end


  def create_charge
    resp = check_for_previous_daily_cash
    if resp['exist']
      respond_to do |format|
        format.html { render json: {:done => false, :error => resp['error'], :message => resp['message']}}
      end
    else
      resp = generate_charge params

      if !resp[:done]
        respond_to do |format|
          format.html { render json: resp}
        end
      else
        chan = ''
        if session[:doctor_username].nil?
          account = Account.find(params[:account_id])
          chan = account.doctor.user.user_name
        else
          chan = session[:doctor_username]
        end
        render :json => {done: true}, layout: false
      end


    end
  end


  def show
    @daily_cash = DailyCash.find(params[:id])

    @prev = params[:prev]

    if session[:role] == 'shared_assistant'
      render layout: 'sharedassistant'
    end

  end

  def add_transaction
    done = true
    error = ''
    message = ''
    transaction = nil
    resp = check_for_previous_daily_cash
    enough_balance = check_enough_balance
    if !resp['exist'] && enough_balance['enough']
     transaction = generate_movement(params)
    else
      done = false
      error = resp['error'] + enough_balance['error']
      message = resp['message'] + enough_balance['message']
    end

    respond_to do |format|
      format.html { render json: {
          :done => done,
          :error => error,
          :message => message,
          :url => movement_ticket_cli_tickets_path(transaction)
      }}
    end

  end

  def history
    respond_to do |format|
      format.html {
        if session[:role] == 'shared_assistant'
          render layout: 'sharedassistant'
        end
      }
      format.json { render json: DailyCashDatatable.new(view_context, current_user)}
    end
  end

  private

  def check_for_previous_daily_cash
    resp = Hash.new
    resp['exist'] = false
    resp['error'] = ''
    resp['message'] = ''

    source_dc = DailyCash.previous_open(params[:account_id]).first
    if source_dc != nil
      resp['exist'] = true
      resp['error'] = 'Hay cortes de días anteriores'
      resp['message'] = 'Por favor cierre el corte de la cuenta "' + source_dc.account.account_number + '" para continuar.'
      return resp
    end

    if params[:transaction_type_id].to_i == TransactionType::TRANSFER
      target_dc =  DailyCash.previous_open(params[:to_account_id]).first
      if target_dc != nil
        resp['exist'] = true
        resp['error'] = 'Hay cortes de días anteriores'
        resp['message'] = 'Por favor cierre el corte de la cuenta "' + target_dc.account.account_number + '" para continuar.'
        return resp
      end
    end
    resp
  end

  def check_enough_balance
    resp = {
        'enough'=>true,
        'error' => '',
        'message' => ''
    }

    account = Account.find(params[:account_id])

    if account.bank.is_local #checar saldos suficientes en caja
      current_dc = DailyCash.open_by_account(account.id).first

      if current_dc.nil?
        return {
            'enough'=>false,
            'error' => 'No hay un corte de caja abierto',
            'message' => 'Por favor abra un corte de caja para continuar.'
        }
      end

      if params[:transaction_type_id].to_i == TransactionType::TRANSFER || params[:t_type].to_i == 0
        params[:amounts].each do |idx, val|
          amount = val[:amount].to_f
          if amount > current_dc.get_balance(val[:currency].to_i)
            currency = Currency.find(val[:currency])
            resp['enough'] = false
            resp['error'] = 'Saldo insuficiente'
            resp['message'] = 'El saldo en '+currency.iso_code+' es insuficiente para llevar a cabo la operación.'
            return resp
          end
        end
      end
    else #checar saldo suficiente en bancos
      if params[:transaction_type_id].to_i == TransactionType::TRANSFER || params[:t_type].to_i == 0
        params[:amounts].each do |idx, val|
          amount = val[:amount].to_f
          if amount > account.balance
            resp['enough'] = false
            resp['error'] = 'Saldo insuficiente'
            resp['message'] = 'El saldo es insuficiente para llevar a cabo la operación.'
            return resp
          elsif val[:currency].to_i != account.currency_id
            resp['enough'] = false
            resp['error'] = 'Tipos de cambio incorrectos'
            resp['message'] = 'Tipos de cambio incorrectos.'
            return resp
          end
        end
      end


    end

    resp
  end

end
