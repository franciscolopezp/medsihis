class Doctor::AllergiesController < ApplicationController

  before_action :set_allergy, only: [:show, :edit, :update, :destroy]

  @patients = Patient.all
  def index
    Allergy.all
  end

  def show
  end

  def new
    @allergy = Allergy.new
    @allergy.build_doctor
  end

  def edit

  end

  def create
    @allergy = Allergy.new(allergy_params)

    respond_to do |format|
      if @allergy.save
        format.html  { redirect_to doctor_allergies_path, notice: 'La alergia fue agregado exitosamente'}
        format.json { render :show, status: :created, location: @allergy  }
      else
        format.html { render :new}
        format.json {render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def update
    respond_to do |format|
      if @allergy.update(allergy_params)
        format.html { redirect_to doctor_allergies_path, notice: 'Alergia actualizado con exito'}
        format.json { render show, status: :ok, location: @allergy}
      else
        format.html { render :edit}
        format.json { render json: 'Alergy error', status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @allergy.destroy
    respond_to do |format|
      format.html { redirect_to doctor_allergies_path, notice: 'Alergia eliminado satisfactoriamente'}
      format.json { head :no_content}
    end
  end

  private
  def set_allergy
    @allergy = Allergy.find(params[:id])
  end

  def allergy_params
    params.require(:allergy).permit(:name, :doctor_id, :patient_ids => [])
  end

end