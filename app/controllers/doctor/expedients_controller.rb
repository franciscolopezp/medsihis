class Doctor::ExpedientsController < ApplicationController

  def index

  end

  def show
    @expedient = MedicalExpedient.find(params[:id])
  end

  def medical_history

    @has_ped = true
    @has_gin = true
    @has_uro = true

    @medical_expedient = MedicalExpedient.find(params[:expedient_id])
    if @medical_expedient.medical_history != nil
      @medical_history = @medical_expedient.medical_history
    else
      @medical_history = MedicalHistory.new
    end

    if !@medical_history.family_disease.present?
      @medical_history.build_family_disease
    end

    # if record exist and doctor have the "Ginecología" feature pass record to view
    if @medical_history.inherited_family_background != nil && @has_gin
      @inherited_family_background = @medical_history.inherited_family_background
    elsif @has_gin # if not exist but doctor have the "Ginecología" feature initialize record in memory and pass to view
      @inherited_family_background = InheritedFamilyBackground.new
    end

    # if record exist and doctor have the "Ginecología" feature pass record to view
    if @medical_history.gynecologist_obstetrical_background != nil && @has_gin
      @gynecologist_obstetrical_background = @medical_history.gynecologist_obstetrical_background
    elsif @has_gin # if not exist but doctor have the "Ginecología" feature initialize record in memory and pass to view
      @gynecologist_obstetrical_background = GynecologistObstetricalBackground.new
    end

    if @has_uro && @medical_history.urological_background.nil?
      @uro_background = UrologicalBackground.new
    elsif @has_uro
      @uro_background = @medical_history.urological_background
    end


    if @has_ped && @medical_history.perinatal_information.nil?
      @perinatal_information = PerinatalInformation.new
    elsif @has_ped
      @perinatal_information = @medical_history.perinatal_information
    end

    if @has_ped && @medical_history.child_non_pathological_info.nil?
      @child_non_pathological_info = ChildNonPathologicalInfo.new
    elsif @has_ped
      @child_non_pathological_info = @medical_history.child_non_pathological_info
    end

    @gender_patient = @medical_expedient.patient.gender_id

    render :layout => false
  end

  def systems
    @medical_expedient = MedicalExpedient.find(params[:expedient_id])
    @medical_history = @medical_expedient.medical_history
    render :layout => false
  end

  def consultations
    @medical_expedient = MedicalExpedient.includes(:patient).find(params[:expedient_id])
    render :layout => false
  end

  def consultations_list
    @medical_expedient = MedicalExpedient.includes(:patient).find(params[:expedient_id])
    @daily_cash = DailyCash.current session[:user_id]
    consultations = MedicalConsultationsDatatable.new(view_context, @current_user,nil,@medical_expedient)
    @medical_consultations = consultations.as_json
    render :layout => false
  end

  def consultations_new
    @consultation_fields = ConsultationField.where(:active => true).order('field_order ASC')

    @medical_expedient = MedicalExpedient.includes(:patient).find(params[:expedient_id])
    @patients = []
    @patients << @medical_expedient.patient

    @offices = current_user.offices
    @consultation_types = current_user.consultation_types
    c_types_id = []
    @consultation_types.each do |ct|
      c_types_id.push(ct.id)
    end
    @consultation_costs = ConsultationCost.where('consultation_type_id IN (?)',c_types_id)

    @last_consult = @patients[0].medical_consultations.order('date_consultation DESC').limit(3).first

    @doctor_medical_consultation = MedicalConsultation.new
    @doctor_medical_consultation.build_prescription
    @doctor_medical_consultation.build_exploration

    @has_gin = true
    @has_ped = true
    @has_uro = true

    @current_pregnancy = Pregnancy.current_by_expedient(@medical_expedient.id).first

    render :layout => false
  end

  def hospital_admissions
    @medical_expedient = MedicalExpedient.find(params[:expedient_id])
    render layout: false
  end

  def clinical_studies
    @medical_expedient = MedicalExpedient.find(params[:expedient_id])
    render layout: false
  end

  def vaccine_books
    patient_id = MedicalExpedient.find(params[:expedient_id]).patient_id
    @patient_vaccine_book = Patient.find(patient_id)

    render :layout => false
  end

  def update_vaccination_date
    vaccination = Vaccination.where(:patient_id => params[:patient_id], :application_age_id => params[:application_age_id]).first
    if vaccination.present?
      vaccination.update(vaccinate_date: params[:date])
    else
      Vaccination.create(vaccinate_date: params[:date], patient_id: params[:patient_id], application_age_id: params[:application_age_id])
    end
    render json: {:done => true}
  end

  def consultations_valoration_fields
    @valoration_type = ValorationType.find(params[:valoration_type_id])
    render :layout => false
  end

  def vital_signs
    @medical_expedient = MedicalExpedient.find(params[:expedient_id])
    render :layout => false
  end
  def save_vital_sign
    expedient = MedicalExpedient.find(params[:expedient_id])
    patient_valoration = PatientValoration.create(
        valoration_type_id:params[:valoration_type],
        patient_id: expedient.patient_id,
        user:current_user
    )
    params[:fields].each do |idx, field|
      PatientValorationField.create(
          patient_valoration_id:patient_valoration.id,
          valoration_field_id:field['id'],
          data: field['value']
      )
    end
    patient_valoration.update(medical_expedients:[expedient])
    render json: {:done => true}
  end
  def vital_sign_table
    respond_to do |format|
      format.html
      format.json { render json: VitalSignsDatatable.new(view_context,params[:patient_id])}
    end
  end
  def get_data_chart
    expedient = MedicalExpedient.find(params[:medical_expedient_id])
    data = []
    ViewPatientValoration.where("patient_id = #{expedient.patient.id}").all.each do |mc|
      item = Hash.new
      item['date'] = mc.created_at
      case params[:type]
        when "weight"
          item['val'] = mc.peso
        when "height"
          item['val'] = mc.talla
        when "temperature"
          item['val'] = mc.temperatura
        when "blood_pressure"
          value_blood_pressure = Hash.new
          if !mc.ta_sistolica.nil? and !mc.ta_diastolica.nil?
            value_blood_pressure['systolic_pressure'] = mc.ta_sistolica
            value_blood_pressure['diastolic_pressure'] = mc.ta_diastolica
            item['val'] = value_blood_pressure
          else
            item['val'] = nil
          end
        else
      end
      puts item
      data.push(item)
    end
    puts data
    render json: data
  end
  def print_graph_valoration
    valoration_name = ""
    expedient = MedicalExpedient.find(params[:medical_expedient_id])
    data = []
    ViewPatientValoration.where("patient_id = #{expedient.patient.id}").order('created_at ASC').all.each do |mc|
      item = Hash.new
      item['date'] = mc.created_at
      case params[:type]
        when "weight"
          item['val'] = mc.peso
          valoration_name = "Peso"
        when "height"
          item['val'] = mc.talla
          valoration_name = "Altura"
        when "temperature"
          item['val'] = mc.temperatura
          valoration_name = "Temperatura"
        when "blood_pressure"
          valoration_name = "Tensión arterial"
          value_blood_pressure = Hash.new
          if !mc.ta_sistolica.nil? and !mc.ta_diastolica.nil?
            value_blood_pressure['systolic_pressure'] = mc.ta_sistolica
            value_blood_pressure['diastolic_pressure'] = mc.ta_diastolica
            item['val'] = value_blood_pressure
          else
            item['val'] = nil
          end
        else
      end

      data.push(item)
    end
    render pdf: "Expediente "+expedient.patient.full_name+"_"+valoration_name, locals: { :@medical_expedient => expedient,
                                                                                         :@type => params[:type],
                                                                                         :@data => data},
           template: 'doctor/print_medical_expedients/body_valoration_graphs.html.erb', layout: false,
           margin: { top: 11, bottom: 17},
           encoding: 'UTF-8',

           footer:  {
               html: {
                   template:'doctor/print_medical_expedients/footer.html.erb',
                   locals: {:@medical_expedient => expedient}}
           }
  end
  def get_all_vital_signs
    pv = PatientValoration.find(params[:pv_id])
    field_data = ''
     pv.patient_valoration_fields.each do |field|
         field_data +=  '<div class="col-md-4">'+
    '<strong>'+field.valoration_field.name+ '</strong><br>'+
                '<label class="valoration_data_label">'+field.data+'</label></div>'
     end
    render json: {:done => true, :data => field_data}
  end
  def get_current_pvs
    pvs = []
    response = false
    patient_valorations = PatientValoration.where("patient_id = #{params[:patient_id]} AND DATE(created_at) = CURDATE()").last
    if patient_valorations != nil
      response = true
      patient_valorations.patient_valoration_fields.each do |field|
        pvs.push({
            :field_id => field.valoration_field_id,
            :data => field.data
                 })
      end
    end

    render json: {:done => response, :pvs => pvs}
  end
end

