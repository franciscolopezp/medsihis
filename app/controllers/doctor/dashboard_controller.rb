class Doctor::DashboardController < ApplicationController

  include Doctor::DashboardHelper
  include Doctor::DailyCashesHelper
  include Doctor::FilterRequestHelper
  include Doctor::IndexHelper

  before_action :filter_daily_cash, only: [:daily_cash]

  def doctor
  end

  def assistant
    init_assistant_available_info

    @doctor_username = session[:doctor_username]
    @payment_methods = PaymentMethod.all
    @daily_cash = DailyCash.current(session[:user_id]).first
    @assistant = Assistant.find(session[:assistant_id])
    @patients = Patient.by_doctor session[:doctor_id]
    @doctor = Doctor.find(session[:doctor_id])

    @activate_notification = session[:active_notification]

    if @activate_notification
      generate_message_notification_license
    end
    @activate_notification = false if @title_notification.nil?
    session[:active_notification] = false
  end

  def assistant_waiting_room
    activities = get_activities_waiting_room params
    respond_to do |format|
      format.html { render json: activities}
    end
  end

  def pending_consults
    consults = get_pending_consults(current_user)
    respond_to do |format|
      format.html { render json: consults}
    end
  end

  def pending_reports
    consults = get_pending_reports params[:office_id]
    respond_to do |format|
      format.html { render json: consults}
    end
  end


  def vaccineBook
    respond_to do |format|
      format.html
      format.json { render json: DashboardsDatatable.new(view_context)}
    end
  end

  def patient_vaccine_book
    @patient_vaccine_book = Patient.find(params[:id])
  end


  private
  def init_assistant_available_info
    @accounts = []
    @daily_cashes = []

    #arreglo de ids de consultorios a los que tiene permiso de corte de caja
    daily_cash_permission = []

    show_daily_cashes = false
    assistant = Assistant.find(session[:assistant_id])
    assistant.office_permissions.each do |p|
      if p.permission.name == 'daily_cash'
        daily_cash_permission.push(p.office_id)
        if session[:current_office_id].to_i == p.office_id
          show_daily_cashes = true
        end
      end
    end

    if show_daily_cashes
      @daily_cashes = DailyCash.open_by_office session[:current_office_id]
      # se consultan las cuentas locales que tiene el consultorio
      local_accounts = Account.local_accounts [session[:current_office_id]]
      # de las cuentas obtenidas se filtran aquellas que no tengan corte de caja abierto
      # esta es la variable que se usa para pintar el select al momento de abrir cortes de caja
      @accounts = get_no_dc_accounts(local_accounts)
    end

    @all_accounts = []
    doctor_accounts = Account.by_doctor session[:doctor_id]
    doctor_accounts.each do |a|
      if !a.bank.is_local || (daily_cash_permission.include?(a.office_id) && session[:current_office_id].to_i == a.office_id)
        @all_accounts.push(a)
      end
    end
  end

end