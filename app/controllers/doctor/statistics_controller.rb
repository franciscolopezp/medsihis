class Doctor::StatisticsController < ApplicationController
  load_and_authorize_resource :only => [:graph_diseases]
  include Doctor::MedicalExpedientsHelper

  def doctor_charts

  end
  def comparative_charts

  end

  def get_genres
    ini_date = params[:ini_date]
    end_date = params[:end_date]
    mens = Patient.where("doctor_id = #{session[:doctor_id]} AND gender_id = #{Gender::MAN} ").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    women = Patient.where("doctor_id = #{session[:doctor_id]} AND gender_id = #{Gender::WOMEN} ").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    childrens = Patient.where("doctor_id = #{session[:doctor_id]} AND (YEAR(CURDATE())-YEAR(patients.birth_day)) < 10").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    teenagers = Patient.where("doctor_id = #{session[:doctor_id]} AND (YEAR(CURDATE())-YEAR(patients.birth_day)) > 9 AND (YEAR(CURDATE())-YEAR(patients.birth_day)) < 20").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    adults = Patient.where("doctor_id = #{session[:doctor_id]} AND (YEAR(CURDATE())-YEAR(patients.birth_day)) > 19 AND (YEAR(CURDATE())-YEAR(patients.birth_day)) < 60").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    oldpeople = Patient.where("doctor_id = #{session[:doctor_id]} AND (YEAR(CURDATE())-YEAR(patients.birth_day)) > 60").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    patients = Patient.where("doctor_id = #{session[:doctor_id]} AND active = 1").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    patients_total = Patient.where("doctor_id = #{session[:doctor_id]} AND active = 1").count
    consultations = MedicalConsultation.joins(:medical_expedient).where("doctor_id = #{session[:doctor_id]}").where("date_consultation >= '#{ini_date}'").where("date_consultation <  '#{end_date}'").count
    consultations_total = MedicalConsultation.joins(:medical_expedient).where("doctor_id = #{session[:doctor_id]}").count
    laboratory_orders = ClinicalAnalysisOrder.joins(:medical_expedient).where("doctor_id = #{session[:doctor_id]}").where("clinical_analysis_orders.created_at >= '#{ini_date}'").where("clinical_analysis_orders.created_at <  '#{end_date}'").count
    laboratory_orders_total = ClinicalAnalysisOrder.joins(:medical_expedient).where("doctor_id = #{session[:doctor_id]}").count
    invoices_stamp = Invoice.where("doctor_id = #{session[:doctor_id]}").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    invoices_cancel = Invoice.where("doctor_id = #{session[:doctor_id]} AND cancel = 1").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    invoices_stamp_total = Invoice.where("doctor_id = #{session[:doctor_id]}").count
    invoices_cancel_total = Invoice.where("doctor_id = #{session[:doctor_id]} AND cancel = 1").count
    surgeries = Activity.where("doctor_id = #{session[:doctor_id]} AND activity_type_id = 2 AND activity_status_id = 7").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    surgeries_total = Activity.where("doctor_id = #{session[:doctor_id]} AND activity_type_id = 2 AND activity_status_id = 7").count
    entry_data = Transaction.where("doctor_id = #{session[:doctor_id]} AND t_type = 1").where("date >= '#{ini_date}'").where("date <  '#{end_date}'")
    entry_data_total = Transaction.where("doctor_id = #{session[:doctor_id]} AND t_type = 1")
    entry = 0.0
    egress= 0.0
    entry_total = 0.0
    egress_total = 0.0
    entry_data.each do |ent|
      entry += ent.amount
    end
    entry_data_total.each do |ent|
      entry_total += ent.amount
    end
    egress_data = Transaction.where("doctor_id = #{session[:doctor_id]} AND t_type = 0").where("date >= '#{ini_date}'").where("date <  '#{end_date}'")
    egress_data_total = Transaction.where("doctor_id = #{session[:doctor_id]} AND t_type = 0")
    egress_data.each do |egr|
      egress += egr.amount
    end
    egress_data_total.each do |egr|
      egress_total += egr.amount
    end
    total_amount = entry - egress
    total_amount_total = entry_total - egress_total

    is_ped = 0;
    newborn = 0
    oneyears = 0
    lactantings = 0
    preschool = 0
    school = 0
    teenagers = 0
    adults = 0
    if doctor_has_feature Feature::PED
    is_ped = true;
    newborn = Patient.where("doctor_id = #{session[:doctor_id]} AND (datediff(CURDATE(),patients.birth_day)) <= 28").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    oneyears = Patient.where("doctor_id = #{session[:doctor_id]} AND (datediff(CURDATE(),patients.birth_day)) > 28 AND (YEAR(CURDATE())-YEAR(patients.birth_day)) = 0").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    lactantings = Patient.where("doctor_id = #{session[:doctor_id]} AND (YEAR(CURDATE())-YEAR(patients.birth_day)) > 0 AND (YEAR(CURDATE())-YEAR(patients.birth_day)) < 2").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    preschool = Patient.where("doctor_id = #{session[:doctor_id]} AND (YEAR(CURDATE())-YEAR(patients.birth_day)) > 1 AND (YEAR(CURDATE())-YEAR(patients.birth_day)) < 5").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    school = Patient.where("doctor_id = #{session[:doctor_id]} AND (YEAR(CURDATE())-YEAR(patients.birth_day)) > 4 AND (YEAR(CURDATE())-YEAR(patients.birth_day)) < 10").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    teenagers = Patient.where("doctor_id = #{session[:doctor_id]} AND (YEAR(CURDATE())-YEAR(patients.birth_day)) > 9 AND (YEAR(CURDATE())-YEAR(patients.birth_day)) < 20").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    adults = Patient.where("doctor_id = #{session[:doctor_id]} AND (YEAR(CURDATE())-YEAR(patients.birth_day)) > 19").where("created_at >= '#{ini_date}'").where("created_at <  '#{end_date}'").count
    end
    respond_to do |format|
      format.html { render json: {:result => true,:mens => mens, :women => women,:childrens => childrens,
                                  :teenagers => teenagers ,:adults => adults, :oldpeople =>oldpeople,:patients => patients,
                                  :is_ped => is_ped,:newborn=>newborn,:oneyears=>oneyears,:lactantings=>lactantings,:preschool=>preschool,:school=>school,
                                  :consultations => consultations, :laboratory_orders => laboratory_orders, :invoices_stamp => invoices_stamp,
                                  :invoices_cancel => invoices_cancel, :surgeries => surgeries,:entry => entry,:egress => egress, :total_amount => total_amount,
                                  :consultations_total => consultations_total,:patients_total => patients_total,:laboratory_order_total => laboratory_orders_total,
                                  :surgeries_total => surgeries_total,:invoices_cancel_total => invoices_cancel_total,
                                  :invoice_stamp_total => invoices_stamp_total,:entry_total => entry_total,:egress_total => egress_total,:total_amount_total => total_amount_total}}
    end
  end
  def get_cities
    cities = []
    allcitites = Patient.joins(:city).where("doctor_id = #{session[:doctor_id]}").where("patients.created_at >= '#{params[:ini_date]}'").where("patients.created_at <  '#{params[:end_date]}'")
    puts allcitites.to_s
    allcitites = allcitites.group("cities.name").distinct.count("patients.name")
    puts allcitites.to_s
    #allcitites = allcitites.select(:city_id).distinct
    allcitites.each do |city|
      puts "jum" + city.to_s
      cities.push({
                      :name => city[0].to_s,
                      :value =>  city[1].to_s
                  })
    end
    respond_to do |format|
      format.html { render json: cities }
    end
  end
  def consults_chart_data
    ini_date = Date.parse params[:ini_date]
    end_date = Date.parse params[:end_date]
    data = []
    consults_a_day = MedicalConsultation.total_grouped_by_day(params[:ini_date],params[:end_date],session[:doctor_id])
    (ini_date..end_date).map do |date|
      if consults_a_day[date] != nil
        price = consults_a_day[date].first.try(:total_price) || 0
        consults = consults_a_day[date].first.try(:consults) || 0
      else
        price = 0
        consults = 0
      end
      data.push({
          date_consultation: date,
          price: price,
          consults: consults,
      })
    end
    render :json => data, layout: false
  end
  def transacs_chart_data
    data = []
    ini_date = Date.parse params[:ini_date]
    end_date = Date.parse params[:end_date]
    data = []
    transactions_a_day = Transaction.total_grouped_by_day(params[:ini_date],params[:end_date],session[:doctor_id])
    (ini_date..end_date).map do |date|
      if transactions_a_day[date] != nil
        egreso = transactions_a_day[date].first.try(:egreso) || 0
        total = transactions_a_day[date].first.try(:total) || 0
        entry = transactions_a_day[date].first.try(:ingreso) || 0
        transacs = transactions_a_day[date].first.try(:transacs) || 0
      else
        egreso = 0
        entry = 0
        total = 0
        transacs = 0
      end
      data.push({
          date: date,
          egreso: egreso,
          entry: entry,
          total: total,
          transacs: transacs,
      })
    end
    render :json => data, layout: false
  end

  def comparative_chart
        mes_ini_actual = params[:mes_ini_actual].to_i
        mes_fin_actual = params[:mes_fin_actual].to_i + 1
        mes_ini_comp = params[:mes_ini_comp].to_i
        mes_fin_comp = params[:mes_fin_comp].to_i + 1
        anio_actual = params[:anio_actual]
        anio_comp = params[:anio_comp]
    patients = Patient.where("doctor_id = #{session[:doctor_id]} AND active = 1").where("month(created_at) >= '#{mes_ini_actual}'").where("month(created_at) <  '#{mes_fin_actual}'").where("year(created_at) = #{anio_actual}").count
    patients_comp = Patient.where("doctor_id = #{session[:doctor_id]} AND active = 1").where("month(created_at) >= '#{mes_ini_comp}'").where("month(created_at) <  '#{mes_fin_comp}'").where("year(created_at) = #{anio_comp}").count
    consultations = MedicalConsultation.joins(:medical_expedient).where("doctor_id = #{session[:doctor_id]}").where("month(date_consultation) >= '#{mes_ini_actual}'").where("month(date_consultation) <  '#{mes_fin_actual}'").where("year(date_consultation) = #{anio_actual}").count
    consultations_comp = MedicalConsultation.joins(:medical_expedient).where("doctor_id = #{session[:doctor_id]}").where("month(date_consultation) >= '#{mes_ini_comp}'").where("month(date_consultation) <  '#{mes_fin_comp}'").where("year(date_consultation) = #{anio_comp}").count
    laboratory_orders = ClinicalAnalysisOrder.joins(:medical_expedient).where("doctor_id = #{session[:doctor_id]}").where("month(clinical_analysis_orders.created_at) >= '#{mes_ini_actual}'").where("month(clinical_analysis_orders.created_at) <  '#{mes_fin_actual}'").where("year(clinical_analysis_orders.created_at) = #{anio_actual}").count
    laboratory_orders_comp = ClinicalAnalysisOrder.joins(:medical_expedient).where("doctor_id = #{session[:doctor_id]}").where("month(clinical_analysis_orders.created_at) >= '#{mes_ini_comp}'").where("month(clinical_analysis_orders.created_at) <  '#{mes_fin_comp}'").where("year(clinical_analysis_orders.created_at) = #{anio_comp}").count
    invoices_stamp = Invoice.where("doctor_id = #{session[:doctor_id]} ").where("month(created_at) >= '#{mes_ini_actual}'").where("month(created_at) <  '#{mes_fin_actual}'").where("year(created_at) = #{anio_actual}").count
    invoices_cancel = Invoice.where("doctor_id = #{session[:doctor_id]} AND cancel = 1").where("month(created_at) >= '#{mes_ini_actual}'").where("month(created_at) <  '#{mes_fin_actual}'").where("year(created_at) = #{anio_actual}").count
    invoices_stamp_comp = Invoice.where("doctor_id = #{session[:doctor_id]} ").where("month(created_at) >= '#{mes_ini_comp}'").where("month(created_at) <  '#{mes_fin_comp}'").where("year(created_at) = #{anio_comp}").count
    invoices_cancel_comp = Invoice.where("doctor_id = #{session[:doctor_id]} AND cancel = 1").where("month(created_at) >= '#{mes_ini_comp}'").where("month(created_at) <  '#{mes_fin_comp}'").where("year(created_at) = #{anio_comp}").count
    surgeries = Activity.where("doctor_id = #{session[:doctor_id]} AND activity_type_id = 2 AND activity_status_id = 7").where("month(created_at) >= '#{mes_ini_actual}'").where("month(created_at) <  '#{mes_fin_actual}'").where("year(created_at) = #{anio_actual}").count
    surgeries_comp = Activity.where("doctor_id = #{session[:doctor_id]} AND activity_type_id = 2 AND activity_status_id = 7").where("month(created_at) >= '#{mes_ini_comp}'").where("month(created_at) <  '#{mes_fin_comp}'").where("year(created_at) = #{anio_comp}").count
    entry_data = Transaction.where("doctor_id = #{session[:doctor_id]} AND t_type = 1").where("month(created_at) >= '#{mes_ini_actual}'").where("month(created_at) <  '#{mes_fin_actual}'").where("year(created_at) = #{anio_actual}")
    entry_data_total = Transaction.where("doctor_id = #{session[:doctor_id]} AND t_type = 1").where("month(created_at) >= '#{mes_ini_comp}'").where("month(created_at) <  '#{mes_fin_comp}'").where("year(created_at) = #{anio_comp}")
    entry = 0.0
    egress= 0.0
    entry_total = 0.0
    egress_total = 0.0
    entry_data.each do |ent|
      entry += ent.amount
    end
    entry_data_total.each do |ent|
      entry_total += ent.amount
    end
    egress_data = Transaction.where("doctor_id = #{session[:doctor_id]} AND t_type = 0").where("month(created_at) >= '#{mes_ini_actual}'").where("month(created_at) <  '#{mes_fin_actual}'").where("year(created_at) = #{anio_actual}")
    egress_data_total = Transaction.where("doctor_id = #{session[:doctor_id]} AND t_type = 0").where("month(created_at) >= '#{mes_ini_comp}'").where("month(created_at) <  '#{mes_fin_comp}'").where("year(created_at) = #{anio_comp}")
    egress_data.each do |egr|
      egress += egr.amount
    end
    egress_data_total.each do |egr|
      egress_total += egr.amount
    end
    total_amount = entry - egress
    total_amount_total = entry_total - egress_total


    data_chart = []
        sql_qry = " select (select sum(amount) from transactions where  t_type = 1 and month(date) = #{mes_ini_actual} and year(date) = #{anio_actual} and doctor_id = #{session[:doctor_id]}) as actual,
        (select sum(amount) from transactions where t_type = 1 and month(date) = #{mes_ini_actual} and year(date) = #{anio_actual.to_i - 1} and doctor_id = #{session[:doctor_id]}) as pasado, #{mes_ini_actual} as month"
      (1..(mes_ini_actual-1)).step(1).each do |month|
        sql_qry += " UNION ALL select (select sum(amount) from transactions where t_type = 1 and month(date) = #{month} and year(date) = #{anio_actual} and doctor_id = #{session[:doctor_id]}) as actual,
        (select sum(amount) from transactions where t_type = 1 and month(date) = #{month} and year(date) = #{anio_actual.to_i - 1} and doctor_id = #{session[:doctor_id]}) as pasado, #{month} as month"
      end
        sql_qry += " order by month"
        records = ActiveRecord::Base.connection.exec_query(sql_qry)
        records.each do |t|
          month_index = get_month_name t["month"]
          actual = 0
          past = 0
          if t["actual"]!= nil
            actual = t["actual"]
          end
          if t["pasado"]!= nil
            past = t["pasado"]
          end
          data_chart.push({
                        date: month_index,
                        actual: actual,
                        past: past
                    })
        end
        puts data_chart
    respond_to do |format|
      format.html { render json: {:result => true,:patients => patients,:patients_comp => patients_comp,:consultations => consultations, :laboratory_orders => laboratory_orders, :invoices_stamp => invoices_stamp,
                                  :invoices_cancel => invoices_cancel, :surgeries => surgeries,:entry => entry,:egress => egress, :total_amount => total_amount,
                                  :consultations_comp => consultations_comp,:laboratory_order_comp => laboratory_orders_comp,
                                  :surgeries_comp => surgeries_comp,:invoices_cancel_comp => invoices_cancel_comp,
                                  :invoice_stamp_comp => invoices_stamp_comp,:entry_comp => entry_total,:egress_comp => egress_total,:total_amount_comp => total_amount_total,:data_chart => data_chart }}
    end
  end

  def comparative_chart_egress
    mes_ini_actual = params[:mes_ini_actual].to_i
    anio_actual = params[:anio_actual]
    data_chart = []
    sql_qry = " select (select sum(amount) from transactions where  t_type = 0 and month(date) = #{mes_ini_actual} and year(date) = #{anio_actual} and doctor_id = #{session[:doctor_id]}) as actual,
        (select sum(amount) from transactions where t_type = 0 and month(date) = #{mes_ini_actual} and year(date) = #{anio_actual.to_i - 1} and doctor_id = #{session[:doctor_id]}) as pasado, #{mes_ini_actual} as month"
    (1..(mes_ini_actual-1)).step(1).each do |month|
      sql_qry += " UNION ALL select (select sum(amount) from transactions where t_type = 0 and month(date) = #{month} and year(date) = #{anio_actual} and doctor_id = #{session[:doctor_id]}) as actual,
        (select sum(amount) from transactions where t_type = 0 and month(date) = #{month} and year(date) = #{anio_actual.to_i - 1} and doctor_id = #{session[:doctor_id]}) as pasado, #{month} as month"
    end
    sql_qry += " order by month"
    records = ActiveRecord::Base.connection.exec_query(sql_qry)
    records.each do |t|
      month_index = get_month_name t["month"]
      actual = 0
      past = 0
      if t["actual"]!= nil
        actual = t["actual"]
      end
      if t["pasado"]!= nil
        past = t["pasado"]
      end
      data_chart.push({
                          date: month_index,
                          actual: actual,
                          past: past
                      })
    end
    puts data_chart
    respond_to do |format|
      format.html { render json: {:result => true,:data_chart => data_chart }}
    end
  end
  def comparative_chart_ingress
    mes_ini_actual = params[:mes_ini_actual].to_i
    anio_actual = params[:anio_actual]
    data_chart = []
    sql_qry = " select (select sum(amount) from transactions where  t_type = 1 and month(date) = #{mes_ini_actual} and year(date) = #{anio_actual} and doctor_id = #{session[:doctor_id]}) as actual,
        (select sum(amount) from transactions where t_type = 1 and month(date) = #{mes_ini_actual} and year(date) = #{anio_actual.to_i - 1} and doctor_id = #{session[:doctor_id]})  as pasado, #{mes_ini_actual} as month"
    (1..(mes_ini_actual-1)).step(1).each do |month|
      sql_qry += " UNION ALL select (select sum(amount) from transactions where t_type = 1 and month(date) = #{month} and year(date) = #{anio_actual} and doctor_id = #{session[:doctor_id]}) as actual,
        (select sum(amount) from transactions where t_type = 1 and month(date) = #{month} and year(date) = #{anio_actual.to_i - 1} and doctor_id = #{session[:doctor_id]}) as pasado, #{month} as month"
    end
    sql_qry += " order by month"
    records = ActiveRecord::Base.connection.exec_query(sql_qry)
    records.each do |t|
      month_index = get_month_name t["month"]
      actual = 0
      past = 0
      if t["actual"]!= nil
        actual = t["actual"]
      end
      if t["pasado"]!= nil
        past = t["pasado"]
      end
      data_chart.push({
                          date: month_index,
                          actual: actual,
                          past: past
                      })
    end
    puts data_chart
    respond_to do |format|
      format.html { render json: {:result => true,:data_chart => data_chart }}
    end
  end
  def comparative_chart_consults
    mes_ini_actual = params[:mes_ini_actual].to_i
    anio_actual = params[:anio_actual]
    data_chart = []
    sql_qry = " select (select count(*) from medical_consultations mc join medical_expedients me
on mc.medical_expedient_id = me.id where me.doctor_id = #{session[:doctor_id]} and month(date_consultation) = #{mes_ini_actual} and year(date_consultation) = #{anio_actual} ) as actual,
        (select count(*) from medical_consultations mc join medical_expedients me
on mc.medical_expedient_id = me.id where me.doctor_id = #{session[:doctor_id]} and month(date_consultation) = #{mes_ini_actual} and year(date_consultation) = #{anio_actual.to_i - 1} ) as pasado, #{mes_ini_actual} as month"
    (1..(mes_ini_actual-1)).step(1).each do |month|
      sql_qry += " UNION ALL select (select count(*) from medical_consultations mc join medical_expedients me
on mc.medical_expedient_id = me.id where me.doctor_id = #{session[:doctor_id]} and month(date_consultation) = #{month} and year(date_consultation) = #{anio_actual}) as actual,
        (select count(*) from medical_consultations mc join medical_expedients me
on mc.medical_expedient_id = me.id where me.doctor_id = #{session[:doctor_id]} and month(date_consultation) = #{month} and year(date_consultation) = #{anio_actual.to_i - 1}) as pasado, #{month} as month"
    end
    sql_qry += " order by month"
    records = ActiveRecord::Base.connection.exec_query(sql_qry)
    records.each do |t|
      month_index = get_month_name t["month"]
      actual = 0
      past = 0
      if t["actual"]!= nil
        actual = t["actual"]
      end
      if t["pasado"]!= nil
        past = t["pasado"]
      end
      data_chart.push({
                          date: month_index,
                          actual: actual,
                          past: past
                      })
    end
    puts data_chart
    respond_to do |format|
      format.html { render json: {:result => true,:data_chart => data_chart }}
    end
  end
  def get_month_name number
    month_index = ""
    case number
      when 1
        month_index  = "Enero"
      when 2
        month_index  = "Febrero"
      when 3
        month_index  = "Marzo"
      when 4
        month_index  = "Abril"
      when 5
        month_index  = "Mayo"
      when 6
        month_index  = "Junio"
      when 7
        month_index  = "Julio"
      when 8
        month_index  = "Agosto"
      when 9
        month_index  = "Septiembre"
      when 10
        month_index  = "Octubre"
      when 11
        month_index  = "Noviembre"
      when 12
        month_index  = "Diciembre"
      else
        month_index  = "ninguno"
    end
    month_index
  end

  def statistics_graph_diseases

    render 'diseases'
  end

  def graph_diseases
    type_graph_person = 1
    type_graph_incidence = 2
    type = params[:type_graph]
    query = nil
    start_date = Date.parse(params[:start_date])
    end_date = Date.parse(params[:end_date])
    more_than = params[:more_than].to_i
    title_graph = ""
    if type.to_i == type_graph_person
      title_graph = "PERSONAS"
     query = query_graph_diseases type,start_date,end_date,more_than

    else
      if type.to_i == type_graph_incidence.to_i
        title_graph = "INCIDENCIAS"
        query = query_graph_diseases type,start_date,end_date,more_than
      end
    end

    records = ActiveRecord::Base.connection.exec_query(query) if !query.nil?

    if !records.nil?

      categories = []
      series = []
      data_series = []
      records.each do |row|
        categories << row["disease"].mb_chars.upcase
        data_series << row["total"].to_i
      end
      series << {
          name: title_graph,
          data: data_series
      }
      render json: {categories: categories, series: series}, layout: false
    else
      render json: {categories: [], series: []}, layout: false
    end

  end


  private

end
