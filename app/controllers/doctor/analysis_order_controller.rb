class Doctor::AnalysisOrderController < ApplicationController

  include Doctor::AnalysisOrderHelper
  include Doctor::FilterRequestHelper
  include Doctor::ExpedientPermissionsHelper
  before_action :filter_print_analysis_order, only: [:print_analysis_order]
  before_action :set_analysis_order, only: [:show, :print_analysis_order, :add_archive]
  before_action :set_medical_expedient, only: [:index_analysis, :create_analysis]


  def index_analysis
    consultations = ClinicalAnalysisOrderDatatable.new(view_context, @current_user,@medical_expedient)
    @analysis_order_rows = consultations.as_json

    @payment_methods = PaymentMethod.all
    @all_accounts = current_user.accounts

    render :layout => false
  end

  def new
    @analysis_order = ClinicalAnalysisOrder.new
    expedient = MedicalExpedient.find(params[:id])
    @expedient_id = expedient.id

    if current_user.is_doctor
      @laboratories = Laboratory.joins(:doctors).where(:doctors => {:id => session[:doctor_id]})
    else
      @laboratories = Laboratory.where('is_custom  = 0')
    end

    render :layout => false
  end

  def create_analysis
    result = create_clinical_analysis_order
    if result[:result]
      render :json => {ok: "true", url_print: download_analysis_doctor_analysis_order_index_url(result[:analysis], format: :pdf)}
    else
      render :json => {ok: "false", errors: result[:errors]}
    end
  end
  def send_report_email
    @clinical_order = ClinicalAnalysisOrder.find(params[:id])
    pdf_name = "Reporte "+@clinical_order.medical_expedient.patient.full_name
    pdf_self = render_to_string pdf: "Reporte "+@clinical_order.medical_expedient.patient.full_name,
        page_size: 'Letter',
        locals: { :@analysis_order => @clinical_order },
        template: 'doctor/analysis_order/report_body_pdf.html.erb',
        layout: false,
        margin: { top: 45, bottom: 21, left: 15, right: 15 },
        encoding: 'UTF-8',
        header: {
        :spacing => 35,
        html: {
            template: 'doctor/analysis_order/report_header_pdf.html.erb',
            locals: { :@analysis_order => @clinical_order}
        }},
        footer:  {
        html: {
            template:'doctor/analysis_order/report_footer_pdf.html.erb',
            locals: {:@analysis_order => @clinical_order}}
    }
    AnalysisOrder.send_report(params[:email],pdf_self,pdf_name,@clinical_order).deliver_now
    render :json => true, layout: false
  end
  def add_archive
    archive = Archive.new(file_name: params[:file_0], original_name: params[:file_0].original_filename, clinical_analysis_order_id:@analysis_order.id)
    if archive.save
      @analysis_order_rows = generate_analysis_as_json @analysis_order
      @flag_div_container = true
      render json: @flag_div_container, layout: false
    else
      render json: false, layout: false
    end
  end

  def download_file
    file = Archive.find(params[:id])
    root = file.file_name.current_path
    send_file(
        "#{root}",
        filename: "#{file.original_name}",
        type: "#{file.file_name.content_type}"
    )
  end

  def viewer_pdf
    file = Archive.find(params[:id])
    respond_to do |format|
      format.pdf do
        root_pdf = file.file_name.current_path
        send_file(
            "#{root_pdf}",
            filename: file.original_name,
            type: file.file_name.content_type
        )
      end
    end
  end

  def viewer_img
    file = Archive.find(params[:id])
    respond_to do |format|
      format.html do
        root_pdf = file.file_name.current_path
        send_file(
            "#{root_pdf}",
            filename: file.original_name,
            type: file.file_name.content_type
        )
      end
    end
  end

  def delete_archive
    archive = Archive.find(params[:id])
    analysis = archive.clinical_analysis_order
    archive.destroy
    render json: true
  end

  def show

  end

  def print_analysis_order

    respond_to do |format|
      format.pdf do
        render pdf: "Análisis clínico "+@analysis_order.medical_expedient.patient.full_name,
               page_size: 'Letter',
               locals: { :@analysis_order => @analysis_order },
               template: 'doctor/analysis_order/body_pdf.html.erb', layout: false,
               margin: { top: 60, bottom: 30, left: 15, right: 15 },
               encoding: 'utf-8',
               header: {
                   :spacing => 50,
                   html: {
                       template: 'doctor/analysis_order/header_pdf.html.erb',
                       locals: { :@analysis_order => @analysis_order}
                   }},
               footer:  {
                   html: {
                       template:'doctor/analysis_order/footer_pdf.html.erb',
                       locals: {:@analysis_order => @analysis_order}}
            }
      end
    end
  end

  def find_clinical_studies
    if !params[:study_selected].nil?
      selected = ClinicalStudy.find(params[:study_selected])
    else
      selected = []
    end
    all_study = ClinicalStudy.where(clinical_study_type_id: params[:study_type]).where.not(id: selected).order(name: :asc)
    render :json => {
        result: all_study,
        selected: selected
    }, :layout => false
  end

  def find_all_indications
    json_analysis = JSON.parse params[:value]

    studies = json_analysis.map{|analysis| analysis["id"]}

    indications = Indication.joins(:clinical_studies).where(:clinical_studies => {id: studies})

    filters = filter_indications_by_analysis indications
    render :json => {indications: filters }, layout: false
  end


  private
    def set_analysis_order
      @analysis_order = ClinicalAnalysisOrder.find(params[:id])
    end

    def set_medical_expedient
      @medical_expedient = MedicalExpedient.includes(:patient).find(params[:expedient_id])
    end
end
