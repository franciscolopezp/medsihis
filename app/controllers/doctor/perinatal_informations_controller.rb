class Doctor::PerinatalInformationsController < ApplicationController

  before_action :set_perinatal_information, only: [:update]

  def create
    @perinatal_information = PerinatalInformation.new(perinatal_information_params)
    if @perinatal_information.save
      respond_to do |format|
        format.json { render json: {:done => true, :data => @perinatal_information}}
      end
    else
      respond_to do |format|
        format.json { render json: {:errors => @perinatal_information.errors}}
      end
    end
  end

  def update
    if @perinatal_information.update(perinatal_information_params)
      respond_to do |format|
        format.json { render json: {:done => true, :data => @perinatal_information}}
      end
    else
      respond_to do |format|
        format.json { render json: {:errors => @perinatal_information.errors}}
      end
    end
  end

  private
  def set_perinatal_information
    @perinatal_information = PerinatalInformation.find(params[:id])
  end

  def perinatal_information_params
    params.require(:perinatal_information).permit(:hospital, :city_id, :weight, :height, :pregnat_risks, :other_info, :medical_history_id)
  end

end