class Doctor::SenseOrgansController < ApplicationController

  before_action :set_sense_organ, only: [:update]

  def index
  end

  def create
    @locomotor = SenseOrgan.new(sense_organ_params)
    respond_to do |format|
      if @locomotor.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @locomotor.id}}
      else
        format.html { render json: {:done => false, :errors => @locomotor.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @locomotor.update(sense_organ_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @locomotor.errors.full_messages}}
      end
    end
  end

  private
  def set_sense_organ
    @locomotor = SenseOrgan.find(params[:id])
  end
  def sense_organ_params
    params.require(:sense_organ).permit(:medical_history_id, :hair, :head, :ears, :nose, :mouth, :throat, :neck, :breasts)
  end
end