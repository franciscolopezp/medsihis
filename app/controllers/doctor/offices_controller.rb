class Doctor::OfficesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:office_manage]
  include Doctor::OfficesHelper
  include Admin::CitiesHelper
  include Doctor::FilterRequestHelper
  before_action :filter_office, only: [:show, :edit]
  before_action :set_office, only: [:show, :edit, :update, :destroy, :deleted_template_office, :selected_template_prescription]

  def index
      respond_to do |format|
        format.html
        format.json { render json: OfficesDatatable.new(view_context,session[:doctor_id])}
      end
  end

  def show
  end
  def download_image
    office = Office.find(params[:id])
    send_file office.logo.current_path, :type => 'image/jpeg', :disposition => 'inline'
  end
  def new
    @office = Office.new
  end

  def edit
  end
  def office_manage
    @current_user = current_user
    respond_to do |format|
      format.html
      format.json { render layout: false}
    end
  end
  def get_offices
    offices = []
    user = User.find(params[:user_id])
    user.offices.each do |office|
      offices.push({
                        :name=>"office_" + office.id.to_s
                    })
    end
    respond_to do |format|
      format.html { render json: offices}
    end
  end
  def save_offices
    user = User.find(params[:user_id])
    user.offices.delete_all
    params[:offices].each do |idx,acc|
      office = Office.find(acc["office_id"].to_i)
      user.offices << office
    end
    respond_to do |format|
      format.html {render json: true}
    end
  end
  def create
    create_office params
    respond_to do |format|
      msg = {:status => "ok" , :message => "Se agregó el consultorio"}
      format.json { render :json => msg  }
    end
  end

  def update
    response = update_office params

    if response[:result]
      status = "ok"
      message = "Se actualizó el consultorio"
      errors = []
    else
      status = "error"
      message = "Error! Ocurrio un error"
      errors = response[:errors]
    end

    respond_to do |format|
      msg = {:status => status , :message => message, :errors => errors }
      format.json { render :json => msg  }
    end
  end

  def destroy
    @office.update({active:false})
    respond_to do |format|
      format.html { redirect_to doctor_offices_path, notice: 'Consultorio eliminado satisfactoriamente'}
      format.json { head :no_content}
    end
  end

  def set_current_office
    session[:current_office_id] = params[:office_id]
    respond_to do |format|
      msg = {:done => true , :message => "Se actualizó el consultorio actual"}
      format.html { render :json => msg  }
    end
  end
  def get_office_data
    office = Office.find(params[:office_id])
    office_array = []
    office_array.push({
                        name: office.name,
                        city: office.city.name + ","+ office.city.state.name + ","+office.city.state.country.name,
                        address: office.address,
                        hospital: office.hospital.name,
                        telephone: office.telephone,
                        duration: office.consult_duration,
                        folio: office.folio
                      })
    times = []
    office.office_times.each do |time|
      times.push({
                day: time.day,
                hour: time.start.strftime("%H:%M") + "-" + time.end.strftime("%H:%M")
                 })
    end
      render :json => {:office =>office_array, :time => times}, layout: false
  end
  def index_template_prescription
    @offices_not_template = current_user.offices.active_offices.not_template
    @offices_has_template = current_user.offices.active_offices.has_template

    #render plain: @offices_not_template.to_json
  end

  def selected_template_prescription
    @office.update(template_prescription_id: params[:template])
    render :json => {result: "ok"}, layout: false
  end

  def deleted_template_office
    @office.update(template_prescription_id: nil)
    render :json => {result: "ok"}, layout: false
  end

  private
  def set_office
    @office = Office.find(params[:id])
  end

  def office_params
    params.require(:office).permit(:name, :address, :telephone,:lat,:lng)
  end

end