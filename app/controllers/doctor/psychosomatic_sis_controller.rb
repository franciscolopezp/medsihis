class Doctor::PsychosomaticSisController < ApplicationController

  before_action :set_psychosomatic_si, only: [:update]

  def index
  end

  def create
    @record = PsychosomaticSi.new(psychosomatic_si_params)
    respond_to do |format|
      if @record.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @record.id}}
      else
        format.html { render json: {:done => false, :errors => @record.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @record.update(psychosomatic_si_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @record.errors.full_messages}}
      end
    end
  end

  private
  def set_psychosomatic_si
    @record = PsychosomaticSi.find(params[:id])
  end
  def psychosomatic_si_params
    params.require(:psychosomatic_si).permit(:description, :medical_history_id, :personalidad, :ansiedad, :animo, :amnesia)
  end
end

