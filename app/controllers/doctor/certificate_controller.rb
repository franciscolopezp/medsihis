class Doctor::CertificateController < ApplicationController

  include Doctor::CertificateHelper

  # tipo de certificados
  def index_type_certificate
    respond_to do |format|
      format.html
      format.json { render json: TypeCertificateDatatable.new(view_context,get_doctor_session)}
    end
  end

  def new_type_certificate
    @type_certificate = TypeCertificate.new
  end

  def show_type_certificate
    @type_certificate = TypeCertificate.find(params[:id])
  end

  def edit_type_certificate
    @type_certificate = TypeCertificate.find(params[:id])
  end

  def create_type_certificate
    @type_certificate = TypeCertificate.new params_type_certificate
    @type_certificate.doctor = get_doctor_session

    if @type_certificate.save
      respond_to do |format|
        format.html {redirect_to show_type_certificate_doctor_certificate_index_path(@type_certificate) }
      end
    else
      respond_to do |format|
        format.html { render :new_type_certificate }
      end
    end
    #render :plain => "ok"
  end

  def update_type_certificate
    @type_certificate = TypeCertificate.find(params[:id])

    if @type_certificate.update params_type_certificate
      respond_to do |format|
        format.html {redirect_to show_type_certificate_doctor_certificate_index_path(@type_certificate) }
      end
    else
      respond_to do |format|
        format.html { render :edit_type_certificate }
      end
    end
  end

  def destroy_type_certificate
    @type_certificate = TypeCertificate.find(params[:id])

    if @type_certificate.certificates.size > 0
      render :json => {result: false, message: "No se puede eliminar porque se encuentra usado por un certificado"}
    else
      @type_certificate.destroy
      render :json => {result: true}
    end
  end

  def type_certificate_container
    type_certificate = TypeCertificate.find(params[:id])
    render :json => {container_html: type_certificate.container_html}, layout: false
  end

  # certificados
  def index_certificate
    respond_to do |format|
      format.html
      format.json { render json: CertificateDatatable.new(view_context,get_doctor_session)}
    end
  end

  def new_certificate
    @certificate = Certificate.new
    @type_certificate = get_doctor_session.type_certificates
    @offices = Office.where('doctor_id = ?',get_doctor_session.id).active_offices.all
    @patients = get_doctor_session.patients
  end

  def show_certificate
    @certificate = Certificate.find(params[:id])

  end

  def edit_certificate
    @certificate = Certificate.find(params[:id])
    @type_certificate = get_doctor_session.type_certificates
    @offices = Office.where('doctor_id = ?',get_doctor_session.id).active_offices.all
    @patients = get_doctor_session.patients
  end

  def create_certificate
    @certificate = Certificate.new params_certificate
    if @certificate.save
      respond_to do |format|
        format.html {redirect_to show_certificate_doctor_certificate_index_path(@certificate) }
      end
    else
      @type_certificate = get_doctor_session.type_certificates
      @offices = Office.where('doctor_id = ?',get_doctor_session.id).active_offices.all
      @patients = get_doctor_session.patients
      respond_to do |format|
        format.html { render :new_certificate }
      end
    end
  end

  def update_certificate
    @certificate = Certificate.find(params[:id])
    if @certificate.update params_certificate

      respond_to do |format|
        format.html {redirect_to show_certificate_doctor_certificate_index_path(@certificate) }
      end
    else
      @type_certificate = get_doctor_session.type_certificates
      @offices = Office.where('doctor_id = ?',get_doctor_session.id).active_offices.all
      @patients = get_doctor_session.patients
      respond_to do |format|
        format.html { render :edit_certificate }
      end
    end
  end

  def destroy_certificate
    certificate = Certificate.find(params[:id])
    certificate.destroy

    render :json => {result: true}
  end

  def print_certificate
    certificate = Certificate.find(params[:id])
    respond_to do |format|
      format.pdf do
        render pdf: "Certificado "+certificate.name,
               page_size: 'Letter',
               locals: { :@certificate => certificate },
               template: 'doctor/certificate/certificate_pdf/body.html.erb',
               layout: false,
               margin: { top: 25, bottom: 25, left: 15, right: 15 },
               encoding: 'UTF-8',
               header: {
                   html: {
                       template: 'doctor/certificate/certificate_pdf/header.html.erb',
                       locals: { :@certificate => certificate}
                   },
                   center: 'TEXT',
                   spacing: 25
                   },
               footer:  {
                   html: {
                       template:'doctor/certificate/certificate_pdf/footer.html.erb',
                       locals: {:@certificate => certificate}}
               }
      end
    end
  end

  def certificate_patient
    patient = Patient.find(params[:patient_id])

    respond_to do |format|
      format.html
      format.json { render json: CertificateDatatable.new(view_context,get_doctor_session, patient)}
    end
  end

  private
    def params_type_certificate
      params.require(:type_certificate).permit(:name, :container_html)
    end

    def params_certificate
      params.require(:certificate).permit(:name, :office_id, :type_certificate_id, :container_html,:patient_id)
    end
end
