class Doctor::UrinaryApsController < ApplicationController

  before_action :set_urinary_ap, only: [:update]

  def index
  end

  def create
    @record = UrinaryAp.new(urinary_ap_params)
    respond_to do |format|
      if @record.save
        format.html { render json: {:done => true, :message => 'Se creó el registro satisfactoriamente.',:id => @record.id}}
      else
        format.html { render json: {:done => false, :errors => @record.errors.full_messages}}
      end
    end
  end

  def update
    respond_to do |format|
      if @record.update(urinary_ap_params)
        format.html { render json: {:done => true, :message => 'Se actualizó el registro satisfactoriamente.'}}
      else
        format.html { render json: {:done => false, :errors => @record.errors.full_messages}}
      end
    end
  end

  private
  def set_urinary_ap
    @record = UrinaryAp.find(params[:id])
  end
  def urinary_ap_params
    params.require(:urinary_ap).permit(:description, :medical_history_id, :disuria, :polaquiuria, :incontinencia, :poliuria, :nicturia, :hematuria, :prostatismo, :tenesmo)
  end
end

