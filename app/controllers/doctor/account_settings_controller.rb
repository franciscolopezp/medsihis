class Doctor::AccountSettingsController < ApplicationController

  include Doctor::AccountSettingsHelper

  def settings

    #create_custom_template_doctor
  end
  def delete_currency_exchange
    CurrencyExchange.find(params[:current_exchange_id]).destroy
    respond_to do |format|
      format.html { render json: {:true => true, :message => 'Registro eliminado correctamente'}}
    end
  end
  def create_custom_prescription
    reponse = false
    custom_template = CustomTemplatePrescription.new(header_cm: 0, footer_cm: 0, doctor_id: session[:doctor_id])
    if custom_template.save
      response = true
    end
    respond_to do |format|
      format.html {render json:{:done => response}}
    end
  end
  def create_custom_report
    reponse = false
    custom_template = CustomTemplateReport.new(header_cm: 0, footer_cm: 0, doctor_id: session[:doctor_id])
    if custom_template.save
      response = true
    end
    respond_to do |format|
      format.html {render json:{:done => response}}
    end
  end

  def add_currency_exchange
    doc = Doctor.find(session[:doctor_id])
    is_default = params[:is_default].to_i
    response = true
    exists_default = false
    old_default = nil
    if params[:edit_id] != ""
      edit_currency_exchange = CurrencyExchange.find(params[:edit_id])
      doc.currency_exchanges.each do |cur|
        if cur.currency_id != edit_currency_exchange.currency.id
          if cur.currency_id == params[:currency_id].to_i
            response = false
          end
        end
        if cur.is_default
          exists_default = true
          old_default = cur
        end
      end
      if response
        if exists_default && is_default == 1
          old_default.update(is_default: 0)
        end
        edit_currency_exchange.currency = Currency.find(params[:currency_id])
        edit_currency_exchange.update(value:params[:value],is_default:is_default)
      end

    else
      doc.currency_exchanges.each do |cur|
        if cur.currency_id == params[:currency_id].to_i
          response = false
        end
        if cur.is_default
          exists_default = true
          old_default = cur
        end
      end
      if response
        if exists_default && is_default == 1
          old_default.update(is_default: 0)
        end
        new_exchange = CurrencyExchange.new(value:params[:value],is_default:is_default)
        new_exchange.doctor = doc
        new_exchange.currency = Currency.find(params[:currency_id])
        new_exchange.save
      end
    end


    respond_to do |format|
      format.html { render json: {:done => response, :message => 'Registro creado correctamente'}}
    end
  end
  def delete_report_field
    response = false
    if CustomTemplateReportField.destroy(params[:idfield])
      response = true
    end
    respond_to do |format|
      format.html {render json: {:done => response}}
    end
  end
  def delete_prescription_field
    response = false
    if CustomTemplatePrescriptionField.destroy(params[:idfield])
      response = true
    end
    respond_to do |format|
      format.html {render json: {:done => response}}
    end
  end
  def download_image_report
    field = CustomTemplateReportField.find(params[:id])
    send_file field.extra.current_path,  :type => 'image/jpeg', :disposition => 'inline'
  end
  def download_image
    field = CustomTemplatePrescriptionField.find(params[:id])
    send_file field.extra.current_path,  :type => 'image/jpeg', :disposition => 'inline'
  end
  def add_report_field
    response = false
    ftype = params[:type].to_i
    doctor = Doctor.find(session[:doctor_id])
    if ftype == 1
      new_field = CustomTemplateReportField.new(font:"Arial",is_bold:0,size:12,position_top_cm:0,position_left_cm:0,show_field:0,
                                                      custom_template_report_id:doctor.custom_template_report.id,fields_prescription_id:params[:newfield],
                                                      section:"HEADER",width_box:0)
      if new_field.save
        response = true
      end
    elsif ftype == 2
      new_field = CustomTemplateReportField.new(font:"Arial",is_bold:0,size:12,position_top_cm:0,position_left_cm:0,show_field:0,
                                                custom_template_report_id:doctor.custom_template_report.id,fields_prescription_id:21,
                                                      section:"HEADER",width_box:0,label:params[:label])
      if new_field.save
        response = true
      end
    elsif ftype == 3
      new_field = CustomTemplateReportField.new(font:"Arial",is_bold:0,size:0,position_top_cm:0,position_left_cm:0,show_field:0,
                                                custom_template_report_id:doctor.custom_template_report.id,fields_prescription_id:params[:newfield],
                                                      section:"HEADER",width_box:0)
      if new_field.save
        response = true
      end
    else
      #aqui debe ir la magia
    end
    respond_to do |format|
      format.html {render json:{:done => response}}
    end
  end

  def add_prescription_field
    response = false
    ftype = params[:type].to_i
    if ftype == 1
      new_field = CustomTemplatePrescriptionField.new(font:"Arial",is_bold:0,size:12,position_top_cm:0,position_left_cm:0,show_field:0,
                                                      custom_template_prescription_id:CustomTemplatePrescription.first.id,fields_prescription_id:params[:newfield],
                                                      section:"HEADER",width_box:0)
      if new_field.save
        response = true
      end
    elsif ftype == 2
      new_field = CustomTemplatePrescriptionField.new(font:"Arial",is_bold:0,size:12,position_top_cm:0,position_left_cm:0,show_field:0,
                                                      custom_template_prescription_id:CustomTemplatePrescription.first.id,fields_prescription_id:21,
                                                      section:"HEADER",width_box:0,label:params[:label])
      if new_field.save
        response = true
      end
    elsif ftype == 3
      new_field = CustomTemplatePrescriptionField.new(font:"Arial",is_bold:0,size:0,position_top_cm:0,position_left_cm:0,show_field:0,
                                                      custom_template_prescription_id:CustomTemplatePrescription.first.id,fields_prescription_id:params[:newfield],
                                                      section:"HEADER",width_box:0)
      if new_field.save
        response = true
      end
    else
      #aqui debe ir la magia
    end
    respond_to do |format|
      format.html {render json:{:done => response}}
    end
  end
  def add_file_report
    response = false
    doctor = Doctor.find(session[:doctor_id])
    new_field = CustomTemplateReportField.new(font:"Arial",is_bold:0,size:0,position_top_cm:0,position_left_cm:0,show_field:0,
                                              custom_template_report_id:doctor.custom_template_report.id,fields_prescription_id:26,
                                              section:"HEADER",width_box:0,extra:params[:file_0])
    if new_field.save
      response = true
    end
    respond_to do |format|
      format.html {render json:{:done => response}}
    end
  end
  def add_file_prescription
    response = false
    doctor = Doctor.find(session[:doctor_id])
    new_field = CustomTemplatePrescriptionField.new(font:"Arial",is_bold:0,size:0,position_top_cm:0,position_left_cm:0,show_field:0,
                                                    custom_template_prescription_id:CustomTemplatePrescription.first.id,fields_prescription_id:26,
                                                    section:"HEADER",width_box:0,extra:params[:file_0])
    if new_field.save
      response = true
    end
    respond_to do |format|
      format.html {render json:{:done => response}}
    end
  end
  def get_fields_toadd
    fields = []
    listfields = FieldsPrescription.where("field_prescription_type_id = #{params[:field_id]}")
    listfields.each do |fielddata|
      fields.push(
          {
              id: fielddata.id,
              name: fielddata.name
          }
      )
    end
    render :json => {:fields => fields}, layout: false
  end
  def update_settings
    result = update_settings_doctor(params)
    if result[:result]
      flash[:notice] = 'Se actualizo correctamente las configuraciones'
    else
      flash[:notice] = "Ocurrio un problema, #{result[:error]}"
    end

    redirect_to action: :settings
  end

  def update_charge_report
    charge = params[:charge]
    doctor = Doctor.find(session[:doctor_id])
    doctor.update({:charge_reports => charge})
    respond_to do |format|
      format.html { render json: {:done => true}}
    end
  end
  def update_iva_report
    iva = params[:iva]
    doctor = Doctor.find(session[:doctor_id])
    doctor.update({:iva_report => iva})
    respond_to do |format|
      format.html { render json: {:done => true}}
    end
  end
  def update_iva_report_included
    ivaincluded = params[:ivaincluded]
    doctor = Doctor.find(session[:doctor_id])
    doctor.update({:iva_report_included => ivaincluded})
    respond_to do |format|
      format.html { render json: {:done => true}}
    end
  end

  def doctor_profile
    @doctor_profile = Doctor.find(current_user.doctor.id)
  end
end