class Doctor::PregnanciesController < ApplicationController

  include Doctor::ExpedientPermissionsHelper
  before_action :set_pregnancy, only: [:update, :destroy, :finish]

  def index
    @medical_expedient = MedicalExpedient.find(params[:medical_expedient_id])
    patient = @medical_expedient.patient


    last_consult = patient.medical_consultations.order('date_consultation DESC').limit(3).first
    if last_consult != nil
      @last_weight = last_consult.vital_sign.weight
    else
      @last_weight = patient.weight
    end

    render :layout => false

  end

  def create
    @pregnancy = Pregnancy.new(pregnancy_params)
    @pregnancy.medical_expedient_id = params[:medical_expedient_id]
    @pregnancy.active = true
    @pregnancy.user = current_user
    @pregnancy.save
    respond_to do |format|
      msg = {:done => true , :message => 'Se generó el embarazo'}
      format.json { render :json => msg  }
    end
  end

  def update
    @pregnancy.update(pregnancy_params)
    respond_to do |format|
      msg = {:done => true , :message => 'Se actualizó el embarazo'}
      format.json { render :json => msg  }
    end
  end

  def finish
    data = {
        :active => false,
        :pregnancy_end_success => params[:success],
        :pregnancy_end_date => params[:pregnancy_end_date]
    }

    data_go = Hash.new
    if @pregnancy.medical_expedient.medical_history.gynecologist_obstetrical_background == nil
      gynecologist_obstetrical_background = GynecologistObstetricalBackground.new
      gynecologist_obstetrical_background.medical_history_id = @pregnancy.medical_expedient.medical_history.id
      gynecologist_obstetrical_background.save
      go_background = gynecologist_obstetrical_background
    else
      go_background = @pregnancy.medical_expedient.medical_history.gynecologist_obstetrical_background
    end


    if go_background.gestations.nil?
      data_go['gestations'] = 1
    else
      data_go['gestations'] = go_background.gestations + 1
    end

    if go_background.caesarean_surgery.nil?
      data_go['caesarean_surgery'] = 0
    else
      data_go['caesarean_surgery'] = go_background.caesarean_surgery.to_i
    end

    if go_background.natural_birth.nil?
      data_go['natural_birth'] = 0
    else
      data_go['natural_birth'] = go_background.natural_birth
    end

    if go_background.abortion_number.nil?
      data_go['abortion_number'] = 0
    else
      data_go['abortion_number'] = go_background.abortion_number
    end

    if params[:success] == 'true'
      #create birth info

      if params[:info][:birth_type].to_i == BirthType::CAESAREAN
        data_go['caesarean_surgery'] = data_go['caesarean_surgery'] +1
      else
        data_go['natural_birth'] = data_go['natural_birth'] + 1
      end

      birth_info = BirthInfo.new
      date = ActiveSupport::TimeZone["Mexico City"].parse(params[:info][:date])

      birth_info.date = date
      birth_info.birth_type_id = params[:info][:birth_type]
      birth_info.observations = params[:info][:observations]
      birth_info.rpm = params[:info][:rpm]
      birth_info.aditional_observations = params[:info][:aditional_observations]
      birth_info.pregnancy_id = @pregnancy.id
      birth_info.hospital = params[:info][:hospital]
      if !params[:info][:delivery_type].nil?
        birth_info.delivery_type_id = params[:info][:delivery_type]
      end
      if !params[:info][:episiotomy].nil?
        birth_info.episiotomy_type_id = params[:info][:episiotomy]
      end
      if !params[:info][:tear].nil?
        birth_info.tear_type_id = params[:info][:tear]
      end
      if !params[:info][:duration].nil?
        birth_info.duration_type_id = params[:info][:duration]
      end
      birth_info.user = current_user
      birth_info.save

      params[:info][:neonateInfos].each do |idx, item|
        date = ActiveSupport::TimeZone["Mexico City"].parse(item['birthdate'])
        neonate_info = NeonateInfo.new
        neonate_info.child_name = item['name']
        neonate_info.gender_id = item['gender']
        neonate_info.birthdate = date
        neonate_info.weight = item['weight']
        neonate_info.height = item['height']
        neonate_info.apgar_appearance = item['apgar_appearance']
        neonate_info.apgar_pulse = item['apgar_pulse']
        neonate_info.apgar_gesture = item['apgar_gesture']
        neonate_info.apgar_activity = item['apgar_activity']
        neonate_info.apgar_breathing = item['apgar_breathing']
        neonate_info.blood_type_id = item['blood_type']
        neonate_info.abnormalities = item['abnormalities']
        neonate_info.observations = item['observations']
        neonate_info.attended = item['attended']
        neonate_info.pediatrician = item['pediatrician']
        neonate_info.birth_info_id = birth_info.id
        neonate_info.save

      end

    else
      data_go['abortion_number'] = data_go['abortion_number'] + 1
      abortion = Abortion.new
      abortion.date = ActiveSupport::TimeZone["Mexico City"].parse(params[:info][:date])
      abortion.hospital = params[:info][:hospital]
      abortion.abortion_type_id = params[:info][:type]
      abortion.observations = params[:info][:observations]
      abortion.pregnancy_id = @pregnancy.id
      abortion.user = current_user
      abortion.save

    end

    @pregnancy.update(data)
    go_background.update(data_go)
    respond_to do |format|
      msg = {:done => true , :message => 'Se actualizó el embarazo'}
      format.json { render :json => msg  }
    end
  end


  def get_birth_info
    birth_info = BirthInfo.find(params[:id])
    respond_to do |format|
      format.html { render :json => birth_info  }
    end
  end

  def get_neonate_info
    neonate_info = NeonateInfo.find(params[:id])
    respond_to do |format|
      format.html { render :json => neonate_info  }
    end
  end

  def update_birth_info
    birth_info = BirthInfo.find(params[:id])
    birth_info.update({
      date:params[:date],
      hospital:params[:hospital],
      birth_type_id:params[:birth_type],
      observations:params[:observations],
      rpm:params[:rpm],
      delivery_type_id:params[:delivery_type],
      episiotomy_type_id:params[:episiotomy],
      tear_type_id:params[:tear],
      duration_type_id:params[:duration],
      aditional_observations:params[:aditional_observations]
    })
    respond_to do |format|
      format.html { render :json => birth_info  }
    end
  end

  def update_neonate_info

    neonate_info = NeonateInfo.find(params[:id])
    neonate_info.update({
        child_name:params[:name],
        gender_id:params[:gender],
        birthdate:params[:birthdate],
        weight:params[:weight],
        height:params[:height],
        apgar_appearance:params[:apgar_appearance],
        apgar_pulse:params[:apgar_pulse],
        apgar_gesture:params[:apgar_gesture],
        apgar_activity:params[:apgar_activity],
        apgar_breathing:params[:apgar_breathing],
        blood_type_id:params[:blood_type],
        abnormalities:params[:abnormalities],
        attended:params[:attended],
        observations:params[:observations],
        pediatrician:params[:pediatrician]
    })
    respond_to do |format|
      format.html { render :json => neonate_info  }
    end
  end

  def get_abortion_info
    abortion_info = Abortion.find(params[:id])
    respond_to do |format|
      format.html { render :json => abortion_info  }
    end
  end

  def update_abortion_info
    abortion_info = Abortion.find(params[:id])
    abortion_info.update({
        observations:params[:observations],
        abortion_type_id:params[:type],
        date:params[:date],
        hospital:params[:hospital],
                      })
    respond_to do |format|
      format.html { render :json => abortion_info  }
    end
  end

  def pregnancy_report
    respond_to do |format|
      format.html
      format.json { render json: PregnanciesDatatable.new(view_context,session[:doctor_id])}
    end
  end
  def print_report
    @view = view_context
    @month_start = params[:month_start]
    @month_end = params[:month_end]
    @year = params[:year]
    @doctor = Doctor.find(session[:doctor_id])
    @pregnancies = Pregnancy.joins(:medical_expedient).joins(:medical_expedient => :patient).joins(:medical_expedient => :doctor).where("doctors.id = #{session[:doctor_id]} and pregnancies.active = 1")
    @pregnancies = @pregnancies.where("MONTH(pregnancies.probable_birth_date) >= #{@month_start} and MONTH(pregnancies.probable_birth_date) <= #{@month_end} and YEAR(pregnancies.probable_birth_date) = #{params[:year]}")
    respond_to do |format|
      format.pdf do
        render pdf: 'reporte_embarazos_' + Date.today.strftime('%Y%m%d'),
               encoding: 'utf8',
               margin: { top: 10, bottom: 15, left: 15, right: 15 },
               :footer => {   html: {   template:'doctor/pregnancies/print_report_footer.pdf.erb'  } }
      end
    end
  end
  def destroy
    @pregnancy.destroy
    respond_to do |format|
      msg = {:done => true , :message => 'Se eliminó el embarazo'}
      format.json { render :json => msg  }
    end
  end

  def load_ultrasounds
    preg = Pregnancy.find(params[:pregnancy_id])
    rec = Ultrasound.where('pregnancy_id = ?',params[:pregnancy_id])
    respond_to do |format|
      format.html { render :json => {
          :fum => preg.last_menstruation_date.nil? ? '' : preg.last_menstruation_date.strftime('%d/%m/%Y') ,
          :fpp => preg.probable_birth_date.nil? ? '' : preg.probable_birth_date.strftime('%d/%m/%Y') ,
          :data => rec}
      }
    end
  end


  private
  def pregnancy_params
    params.require(:pregnancy).permit(:last_menstruation_date, :probable_birth_date, :initial_weight, :baby_name)
  end

  def set_pregnancy
    @pregnancy = Pregnancy.find(params[:id])
  end


end