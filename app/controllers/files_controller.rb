class FilesController < ApplicationController
  let :all, :all

  def download
    send_file get_url, :disposition => 'inline'
  end

  private
  def get_url
    case params[:model]
      when Const::DOWNLOAD_SETTING_IMAGE
        image = SettingImage.find(params[:id])
        return image.image.path
    end

  end

end