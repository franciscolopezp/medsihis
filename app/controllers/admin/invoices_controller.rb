class Admin::InvoicesController < ApplicationController

  include InvoiceHelper
  skip_before_filter :verify_authenticity_token, :only => [:create]


  def index
  end


  def new
  end

  def getFiscalInfos
    fiscals = []
   GeneralFiscalInfo.all.each do |info|
     fiscals.push({
                        :id => info.id,
                        :rfc => info.rfc,
                        :business_name => info.business_name,
                        :type => 'general'
                    })
   end
    PatientFiscalInformation.all.each do |info|
      fiscals.push({
                       :id => info.id,
                       :rfc => info.rfc,
                       :business_name => info.business_name.gsub(',,', ' '),
                       :type => 'patient'
                   })
    end
    render :json => {:fiscalInfos => fiscals}, layout: false
  end

  def create
    response = create_invoice params
    if response[:status] == "ok"
      redirect_to cli_admission_invoices_path
    else
      flash[:danger] = "No se ha podido generar la factura. " + response[:message].to_s
      redirect_to new_admin_invoice_path
    end
  end


  def edit

  end


  def update
  end


  def destroy

  end


end