class Admin::StudyPackagesController < ApplicationController
  before_action :set_study_package, only: [:show, :edit, :update, :destroy]

  def index
    @lab_id = params[:lab_id] if params[:lab_id].present?
    @packages = StudyPackage.where(:laboratory_id => @lab_id)
    @categories = ClinicalStudyType.where('laboratory_id = ?',@lab_id)
  end

  def create
    @study_package = StudyPackage.new(study_package_params).save
    respond_to do |format|
      format.html { render json: {:done => true, :message => ''}}
    end
  end

  def update
    @study_package.update(study_package_params)
    respond_to do |format|
      format.html { render json: {:done => true, :message => ''}}
    end
  end

  def add_study
    done = true
    message = ''
    package = StudyPackage.find(params[:package_id])
    studies = package.clinical_studies
    params[:studies_id].each do |sid|
      if !studies.where('id = ?',sid).first.nil?
        message += "El estudio '"+studies.find(sid).name+"' ha sido asociado previamente. "
        done = false
      end
    end

    if done
      params[:studies_id].each do |sid|
        studies.push(ClinicalStudy.find(sid))
      end
      package.update(clinical_studies:studies)
    end

    respond_to do |format|
      format.html { render json: {:done => done, :message => message}}
    end
  end

  def remove_study
    done = true
    message = ''
    package = StudyPackage.find(params[:package_id])
    study = ClinicalStudy.find(params[:study_id])
    package.clinical_studies.delete(study)
    respond_to do |format|
      format.html { render json: {:done => done, :message => message}}
    end
  end

  def by_laboratory
    respond_to do |format|
      format.html { render json: StudyPackage.where(:laboratory_id => params[:laboratory_id])}
    end
  end


  def show
    respond_to do |format|
      format.html { render json: @study_package}
    end
  end

  def destroy
    @study_package.destroy
    respond_to do |format|
      format.html { render json: {:done => true, :message => ''}}
    end
  end

  def get_studies
    package = StudyPackage.find(params[:package_id])
    if !params[:studies_selected].nil?
      selected = ClinicalStudy.find(params[:studies_selected])
    else
      selected = []
    end
    other_studies = package.clinical_studies.where.not(id: selected).order(name: :asc)
    render :json => {
        result: [],
        selected: (selected + other_studies)
    }, :layout => false
  end


  private
  def set_study_package
    @study_package = StudyPackage.find(params[:id])
  end

  def study_package_params
    params.require(:study_package).permit(:name,:laboratory_id)
  end

end