class Admin::InventoriesController < ApplicationController
  load_and_authorize_resource :only => [:report,:index, :show,:new,:print_pdf_inventory_report,:edit]
  include ApplicationHelper
  include Cli::MedicamentHelper
  before_action :set_inventory, only: [:show, :edit, :update, :destroy, :search_medicament_inventories]

  def index
    respond_to do |format|
      format.html
      format.json { render json: InventoriesDatatable.new(view_context)}
    end
  end

  def new
    @inventory = Inventory.new
  end

  def report

  end
  def export_excel
    @category = "Todos"
    @warehouse = "Todos"
    medicament_inventories = MedicamentInventory.joins(:lot => :medicament).joins(:warehouse)
    medicament_pharmacy_inventories = PharmacyItemInventory.joins(:pharmacy_item).joins(:warehouse)
    if params[:warehouse_id_search].to_s != "TODO"
      @warehouse = Warehouse.find(params[:warehouse_id_search]).name
      medicament_inventories = medicament_inventories.where("warehouses.id = '#{params[:warehouse_id_search].to_s}'")
      medicament_pharmacy_inventories = medicament_pharmacy_inventories.where("warehouses.id = '#{params[:warehouse_id_search].to_s}'")
    end
    if params[:medicament_catagory_id_search].to_s != "TODO"
      @category = MedicamentCategory.find(params[:medicament_catagory_id_search]).name
      medicament_inventories = medicament_inventories.where("medicaments.medicament_category_id = '#{params[:medicament_catagory_id_search].to_s}'")
      medicament_pharmacy_inventories = medicament_pharmacy_inventories.where("pharmacy_items.medicament_category_id = '#{params[:medicament_catagory_id_search].to_s}'")
    end

    @export_info = medicament_inventories
    @export_info_2 = medicament_pharmacy_inventories
    filename = "Reporte_inventario.xls"
    respond_to do |format|
      format.xlsx {render xlsx: 'export_excel',filename: filename}
    end
  end
  def create
    @inventory = Inventory.new(inventory_params)
    @inventory.created_user_id=current_user.id
    @inventory.inventory_date=Date.today.to_s
    @inventory.is_closed=false

    if params[:inventory][:medicament_category_id] == "TODO"
      @inventory.medicament_category_id = nil
    end

    respond_to do |format|
      if @inventory.save
        save_items_inventory(@inventory.id,@inventory.warehouse_id, @inventory.medicament_category_id)
        format.html { redirect_to admin_inventories_url, notice: 'Inventario Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  def save_items_inventory(id ,warehouse_id, medicament_category_id)
    medicament_inventories = MedicamentInventory.joins(:lot => :medicament)
                                 .joins(:warehouse)
                                 .where("warehouses.id = '#{warehouse_id}'")
    medicament_pharmacy_inventories = PharmacyItemInventory.joins(:pharmacy_item)
                                 .joins(:warehouse)
                                 .where("warehouses.id = '#{warehouse_id}'")

    if medicament_category_id != nil
      medicament_inventories = medicament_inventories.where("medicaments.medicament_category_id = '#{medicament_category_id}'")
      medicament_pharmacy_inventories = medicament_pharmacy_inventories.where("pharmacy_items.medicament_category_id = '#{medicament_category_id}'")
    end
    medicament_inventories.each do |mi|
        InventoryStock.create(inventory_id:id, quantity:mi.quantity, lot_id:mi.lot_id)
    end
    medicament_pharmacy_inventories.each do |ph|
      InventoryPharmacyStock.create(inventory_id:id, quantity:ph.quantity, pharmacy_item_id:ph.pharmacy_item_id)
    end
  end

  def edit

  end

  def update
    respond_to do |format|
      if @inventory.update(inventory_params)
        format.html { redirect_to admin_inventories_url, notice: 'Inventario Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  def destroy
    if @inventory.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Inventario Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end
  end

  def print_pdf_inventory_report
    inventory = Inventory.find(params[:id])
    respond_to do |format|
      format.pdf do
        render pdf: "Inventario "+inventory.inventory_date.strftime('%Y%m%d'),
               page_size: 'Letter',
               locals: { :@inventory => inventory },
               template: 'admin/inventories/inventory_pdf/body.html.erb',
               layout: false,
               margin: { top: 25, bottom: 25, left: 15, right: 15 },
               encoding: 'UTF-8',
               header: {
                   html: {
                       template: 'admin/inventories/inventory_pdf/header.html.erb',
                       locals: { :@inventory => inventory}
                   },
                   center: 'TEXT',
                   spacing: 25
               },
               footer:  {
                   html: {
                       template:'admin/inventories/inventory_pdf/footer.html.erb',
                       locals: {:@inventory => inventory}
                   }
               }
      end
    end
  end

  def update_medicament_inventories

    inventory = Inventory.find(params[:inventory_id])
    inventory.update(updated_user_id:current_user.id, is_closed:true)

    params[:stocks].each do |idx, stk|
      quantity_old=stk["quantity_old"].to_i
      quantity_new=stk["quantity"].to_i
      is_income = 0
      is_update = true
      if(quantity_old > quantity_new)
        is_income = 0
      elsif(quantity_old < quantity_new)
        is_income = 1
      else
        is_update = false
      end

      if(is_update)
        if stk["is_medicament"].to_i == 1
          stock = MedicamentInventory.where('lot_id = ? AND warehouse_id = ?', stk["lot_id"].to_i, params[:warehouse_id]).first
          stock.update(quantity:stk["quantity"].to_i)
          update_log(stk["lot_id"].to_i,params[:warehouse_id],is_income,stk["quantity"].to_i,current_user.id,InventoryStock.find(stk["stock_id"]))
        else
          stock = PharmacyItemInventory.where("warehouse_id = #{params[:warehouse_id]} and pharmacy_item_id = #{stk["pharmacy_item_id"]}").first
          stock.update(quantity:stk["quantity"].to_i)
          update_pharmacy_log(stk["pharmacy_item_id"].to_i,params[:warehouse_id],is_income,stk["quantity"].to_i,current_user.id,InventoryPharmacyStock.find(stk["stock_id"]))
        end
      end
    end

    respond_to do |format|
      format.html {render json: true}
    end
  end

  private
  def set_inventory
    @inventory = Inventory.find(params[:id])
  end

  def inventory_params
    params.require(:inventory).permit(:warehouse_id, :medicament_category_id)
  end

  def inventory_stock_params(info)
    info.permit(:quantity, :lot_id, :inventory_id)
  end




end