class Admin::ApplicationAgesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_application_age, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Edad de Aplicación".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: ApplicationAgesDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Edad de Aplicación".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Edad de Aplicación".
  def new
    @text_button = "Guardar"
    @application_age = ApplicationAge.new
    @application_age.build_vaccine
  end

  # Consulta un registro de tipo "Edad de Aplicación" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] Un string que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Edad de Aplicación" para poder ser editado.
  def edit
    @text_button = "Guardar"
  end

  # Genera un registro de tipo "Edad de Aplicación" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Edad de Aplicación" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Edad de Aplicación".
  def create
    @text_button = "Guardar"
    @application_age = ApplicationAge.new(application_age_params)

    respond_to do |format|
      if @application_age.save
        format.html { redirect_to admin_application_ages_path, notice: 'vaccination was successfully created.' }
        format.json { render :show, status: :created, location: @application_age }
      else
        format.html { render :new }
        format.json { render json: @application_age.errors, status: :unprocessable_entity }
      end
    end
  end

  # Actualiza un registro de tipo "Edad de Aplicación" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Edad de Aplicación" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Edad de Aplicación".
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @application_age.update(application_age_params)
        format.html { redirect_to admin_application_ages_path, notice: 'vaccination was successfully updated.' }
        format.json { render :show, status: :ok, location: @application_age }
      else
        format.html { render :edit }
        format.json { render json: @application_age.errors, status: :unprocessable_entity }
      end
    end
  end

  # Elimina un registro de tipo "Edad de Aplicación" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Edad de Aplicación".
  def destroy
    @application_age.destroy
    respond_to do |format|
      format.html { redirect_to admin_application_ages_url, notice: 'vaccination was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Inicializa un objeto de tipo "Edad de Aplicación" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Edad de Aplicación".
  # @return [ApplicationAge] Objeto de tipo "Edad de Aplicación"
  def set_application_age
    @application_age = ApplicationAge.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Edad de Aplicación" sean: edad, tipo de edad, dosis, comentarios, id de vacuna.
  def application_age_params
    params.require(:application_age).permit(:age, :age_type, :dose, :comments, :vaccine_id)
  end
end
