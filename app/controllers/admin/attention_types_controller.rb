class Admin::AttentionTypesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_attention, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: AttentionTypesDatatable.new(view_context)}
    end
  end


  def new
    @attention_type = AttentionType.new
  end



  def create
    @attention_type = AttentionType.new(attention_type_params)
    respond_to do |format|
      if @attention_type.save
        format.html { redirect_to admin_attention_types_url, notice: 'Tipo de atencion Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @attention_type.update(attention_type_params)
        format.html { redirect_to admin_attention_types_url, notice: 'Tipo de atención Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @attention_type.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Tipo de atencion Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_attention
    @attention_type = AttentionType.find(params[:id])
  end


  def attention_type_params
    params.require(:attention_type).permit(:name)
  end

end