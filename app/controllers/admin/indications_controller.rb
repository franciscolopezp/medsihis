class Admin::IndicationsController < ApplicationController
  before_action :set_indication, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Indicación".
  #
  # @param params [hash] Hash con la llave "searchPhrase" y "clinical_study" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    @lab_id = params[:lab_id] if params[:lab_id].present?

    respond_to do |format|
      format.html{
        @indication_groups = CaGroup.where('laboratory_id = ?', @lab_id).all
      }
      format.json { render json: IndicationsDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Indicación".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Indicación".
  def new
    @text_button = "Guardar"
    @indication = Indication.new
    @indication.build_ca_group
  end

  # Consulta un registro de tipo "Indicación" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Indicación" para poder ser editado.
  def edit
    @text_button = "Guardar"
  end

  # Genera un registro de tipo "Indicación" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Indicación" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Indicación".
  def create
    @indication = Indication.new(indication_params).save
    respond_to do |format|
      format.html { render json: {:done => true, :message => ''}}
    end
  end

  # Actualiza un registro de tipo "Indicación" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Indicación" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Indicación".
  def update
    @indication.update(indication_params)
    respond_to do |format|
      format.html { render json: {:done => true, :message => ''}}
    end
  end


  def show
    respond_to do |format|
      format.html { render json: @indication}
    end
  end

  # Elimina un registro de tipo "Indicación" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Indicación".
  def destroy
    @indication.destroy
    respond_to do |format|
      format.html { render json: {:done => true, :message => ''}}
    end
  end

  def get_by_group
    result = Indication.where('ca_group_id = ?',params[:group_id])
    respond_to do |format|
      format.html { render json: result}
    end
  end

  private
  # Inicializa un objeto de tipo "Indicación" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Indicación".
  # @return [Indication] Objeto de tipo "Indicación"
  def set_indication
    @indication = Indication.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Indicación" sean: 
  # descripción, prioridad, id de grupo de análisis, ids de estudios clínicos.
  def indication_params
    params.require(:indication).permit(:description, :priority, :ca_group_id)
  end

end