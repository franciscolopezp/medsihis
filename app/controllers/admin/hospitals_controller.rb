class Admin::HospitalsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_hospital, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Hospital".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: HospitalsDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Hospital".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Hospital".
  def new
    @hospital = Hospital.new
  end

  # Genera un registro de tipo "Hospital" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Hospital" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Hospital".
  def create
    @hospital = Hospital.new(hospital_params)
    respond_to do |format|
      if @hospital.save
        format.html { redirect_to admin_hospitals_url, notice: 'Hospital Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  # Consulta un registro de tipo "Hospital" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Hospital" para poder ser editado.
  def edit

  end

  # Actualiza un registro de tipo "Hospital" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Hospital" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Hospital".
  def update
    respond_to do |format|
      if @hospital.update(hospital_params)
        format.html { redirect_to admin_hospitals_url, notice: 'Hospital Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  # Elimina un registro de tipo "Hospital" de la base de datos y despliega un mensaje en formato json.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [string] String en formato json que contiene un mensaje con el estado de la operación.
  def destroy
    if @hospital.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Hospital Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private
  # Inicializa un objeto de tipo "Hospital" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Hospital".
  # @return [Hospital] Objeto de tipo "Hospital"
  def set_hospital
    @hospital = Hospital.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Hospital" sean: nombre y descripción, id de la ciudad.
  def hospital_params
    params.require(:hospital).permit(:name,:description,:city_id)
  end

end