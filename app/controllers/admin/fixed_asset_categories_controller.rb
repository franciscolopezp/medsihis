class Admin::FixedAssetCategoriesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_fixed_asset_category, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: FixedAssetCategoriesDatatable.new(view_context)}
    end
  end


  def new
    @fixed_asset_category= FixedAssetCategory.new
  end



  def create
    @fixed_asset_category = FixedAssetCategory.new(fixed_asset_category_params)
    respond_to do |format|
      if @fixed_asset_category.save
        format.html { redirect_to admin_fixed_asset_categories_url, notice: 'Categoría de activo fijo Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @fixed_asset_category.update(fixed_asset_category_params)
        format.html { redirect_to admin_fixed_asset_categories_url, notice: 'Categoría de activo fijo Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @fixed_asset_category.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Categoría de activo fijo Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_fixed_asset_category
    @fixed_asset_category = FixedAssetCategory.find(params[:id])
  end


  def fixed_asset_category_params
    params.require(:fixed_asset_category).permit(:name)
  end

end