class Admin::CitiesController < ApplicationController
  include Admin::CitiesHelper

  # Retorna una lista paginada de registros de tipo "Ciudad" para un campo de texto que se autocompleta.
  #
  # @param params [hash] Hash con la llave "query" que se usará para filtrar los registros.
  # @return [string] String con formato json que contiene los primeros 15 registros que coincidan con el parámetro de búsqueda.
  def find_cities
    str = params[:term]
    render :json => find_city(str)
  end
end