class Admin::ImportationsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:import_user,:create,:import_medical_history,:import_medical_consultation,:import_pregnancy]
  include Doctor::PatientsHelper
  def index

  end
  def cli_importations

  end
  def import_user
    users_created = 0
    users_update = 0
    result = "true"
    json = get_data_csv params[:upload_file]
    begin
      json.each do |patient|
        patient_found = Person.where("name like '#{patient["Nombres"]}' AND last_name like '#{patient["Apellidos"]}'")
        if patient_found.count > 0
          patient_to_update = patient_found.first
          patient_to_update.update(name: patient["nombres"],last_name: patient["apellidos"],birth_day: !patient["fechanacimiento"].nil? ? patient["fechanacimiento"].to_date : Time.now,
                                    telephone: patient["telefono"], cellphone: patient["celular"],address: patient["direccion"])
          users_update += 1
        else
          new_patient = Person.new(name: patient["nombres"],last_name: patient["apellidos"],birth_day: !patient["fechanacimiento"].nil? ? patient["fechanacimiento"].to_date : Time.now,
                                   telephone: patient["telefono"], cellphone: patient["celular"],address: patient["direccion"])
          new_patient.gender = Gender.find_by_name(patient["genero"])
          cities = City.where("name like '#{patient["ciudad"]}'")
          if cities.count > 0
            new_patient.city = cities.first
          else
            new_patient.city = City.find(1801)
          end

          if new_patient.save
            new_user = User.new(user_name:patient["nombre_usuario"],password:"12345",email:patient["email"])
            if patient["rol"] == "Doctor"
                  new_user.is_doctor = true
                  new_user.person = new_patient
                  if new_user.save
                    new_doctor = Doctor.new(time_zone_id:18,curp:"")
                    new_doctor.user = new_user
                    new_fiscal_information = FiscalInformation.new
                    if new_fiscal_information.save
                      new_doctor.fiscal_information = new_fiscal_information
                      new_doctor.save
                    end
                  else
                    puts new_user.errors.full_messages
                  end
            elsif patient["rol"] == "administrador"
              new_user.type_user = TypeUser.find(TypeUser::ADMIN)
              new_user.person = new_patient
              if new_user.save
              else
                puts new_user.errors.full_messages
              end
            elsif patient["rol"] == "recepcion"
              new_user.person = new_patient
              if new_user.save
              else
                puts new_user.errors.full_messages
              end
            elsif patient["rol"] == "encargadofarmacia"
              new_user.person = new_patient
              if new_user.save
              else
                puts new_user.errors.full_messages
              end
            elsif patient["rol"] == "enfermera"
              new_user.person = new_patient
              if new_user.save
              else
                puts new_user.errors.full_messages
              end
            end
            users_created+=1
          else
            result = "false"
          end
        end

      end
    end


    respond_to do |format|
      format.html { redirect_to cli_importations_admin_importations_path, notice: 'Se agregaron '+ users_created.to_s + ' usuarios y se actualizaron ' + users_update.to_s}
    end
  end
  def get_data_csv csv_file
    uploaded_io = csv_file
    dir = Rails.root.join('uploads',uploaded_io.original_filename)
    File.open(dir, 'wb') do |file|
      file.write(uploaded_io.read)
    end
    data = CSV.read(dir, { :col_sep => "|" , encoding: "ISO-8859-1", headers: true, header_converters: :symbol, converters: :all})
    hashed_data = data.map { |d| d.to_hash }
    son = hashed_data.to_json
    json = JSON.parse(son)
    json
  end
  def create
    total_patient_created = 0
    patients_update = 0
    result = "true"
    expedients_folios = []
    json = get_data_csv params[:upload_file]
    doctor = Doctor.find(params[:doctor_for_migrate])
    begin
      json.each do |patient|
        patient_found = Patient.where("name like '#{patient["nombre"]}' AND last_name like '#{patient["apellido"]}' AND doctor_id = #{params[:doctor_for_migrate]}")
        if patient_found.count > 0
          patient_to_update = patient_found.first
          patient_to_update.update(name: patient["nombre"],last_name: patient["apellido"],birth_day: !patient["fechanacimiento"].nil? ? patient["fechanacimiento"].to_date : Time.now,
                                   weight: patient["peso"],height: patient["altura"],phone: patient["tel"], cell: patient["cel"], email: patient["mail"],
                                   address: patient["direccion"], blood_type: patient["tiposangre"],birthplace: patient["lugarnacimiento"])
          patients_update += 1
          if patient["folio"] != nil && patient["folio"] != ""
            expedient_to_update = patient_to_update.medical_expedient
            expedient_to_update.update(folio: patient["folio"].to_i)
          end
        else
          new_patient = Patient.new(name: patient["nombre"],last_name: patient["apellido"],birth_day: !patient["fechanacimiento"].nil? ? patient["fechanacimiento"].to_date : Time.now,
                                    weight: patient["peso"],height: patient["altura"],phone: patient["tel"], cell: patient["cel"], email: patient["mail"],
                                    address: patient["direccion"], blood_type: patient["tiposangre"],birthplace: patient["lugarnacimiento"],active:1)
          new_patient.gender = Gender.find(patient["genero"])
          cities = City.where("name like '#{patient["ciudad_paciente"]}'")
          if cities.count > 0
            new_patient.city = cities.first
          else
            new_patient.city = doctor.city
          end

          new_patient.doctor = doctor
          if patient["folio"] != nil && patient["folio"] != ""
            folios = MedicalExpedient.all.where("folio = #{patient["folio"]} AND doctor_id = #{doctor.id}")
            if folios.count > 0
               expedients_folios.push({
                              id:folios.first.id,
                              folio:folios.first.folio
                                      })
            end
            new_expedient = MedicalExpedient.new(folio: patient["folio"].to_i, doctor: doctor,active:1)
          else
            new_expedient = MedicalExpedient.new(folio: MedicalExpedient.generate_folio, doctor: doctor,active:1)
          end
          new_expedient = generate_medical_history new_expedient
          new_patient.medical_expedient = new_expedient
          if new_patient.save
            total_patient_created+=1
          else
            result = "false"
          end
        end

      end
      expedients_folios.each do |exp|
        exp_folio = MedicalExpedient.find(exp[:id])
        exp_folio.update(folio:MedicalExpedient.generate_folio)
      end
    end


    respond_to do |format|
      format.html { redirect_to admin_importations_path, notice: 'Se agregaron '+ total_patient_created.to_s + ' pacientes y se actualizaron ' + patients_update.to_s}
    end
  end
  def import_patients

  end
  def delete_file file
    if File.exist? file
      FileUtils.rm_rf(file)
    end
  end
  def import_medical_history
      names_no_founded = ""
      patient_updated = 0
      history_info = get_data_csv params[:upload_file]
      doctor = Doctor.find(params[:doctor_for_migrate])
      begin
        history_info.each do |history|
          patient_found = Patient.where("name like '#{history["nombre"]}' AND last_name like '#{history["apellido"]}' AND doctor_id = #{params[:doctor_for_migrate]}")
          if patient_found.count >0
              medical_history = patient_found.first.medical_expedient.medical_history
              medical_history.no_pathological.update(smoking: history["nop_fuma"],smoking_description: history["nop_fuma_descrip"],
              alcoholism: history["nop_alcoholismo"],alcoholism_description: history["nop_alcoholismo_descrip"], play_sport:history["nop_deporte"],
              play_sport_description:history["nop_deporte_descrip"],annotations:history["nop_anotaciones"],marital_status_id:history["nop_marital_status_id"],
              religion:history["nop_religion"],scholarship:history["nop_escolaridad"],drugs:history["nop_drogas"],drugs_description:history["nop_drogas_descrip"])

              medical_history.family_disease.update(diabetes: history["fd_diabetes"],diabetes_description:history["fd_diabetes_descrip"],overweight:history["fd_sobrepeso"],
              overweight_description:history["fd_sobrepeso_descrip"],hypertension:history["fd_hipertension"],hypertension_description:history["fd_hipertension_descrip"],asthma:history["fd_asma"],
              asthma_description:history["fd_asma_descrip"],cancer:history["fd_cancer"],cancer_description:history["fd_cancer_descrip"],annotations:history["fd_anotaciones"],
              other_diseases:history["fd_otras_enfermedades"],psychiatric_diseases:history["fd_psiquiatrica"],psychiatric_diseases_description:history["fd_psiquiatrica_descrip"],
              neurological_diseases:history["fd_neurologica"],neurological_diseases_description:history["fd_neurologica_descrip"],cardiovascular_diseases:history["fd_cardiovascular"],
              cardiovascular_diseases_description:history["fd_cardiovascular_descrip"],bronchopulmonary_diseases:history["fd_broncopulmonar"],bronchopulmonary_diseases_description:history["fd_broncopulmonar_descrip"],
              thyroid_diseases:history["fd_tiroide"],thyroid_diseases_description:history["fd_tiroide_descrip"],kidney_diseases:history["fd_renal"],kidney_diseases_description:history["fd_renal_descrip"])

              medical_history.pathological_allergy.update(allergy_medications:history["pat_alergia"],allergy_medications_description:history["pat_alergia_descrip"],
              other_allergies:history["pat_otras_alergias"])

              medical_history.surgery.update(description:history["ciru_descripcion"],hospitalizations:history["ciru_hospitalizaciones"],transfusions:history["ciru_transfuciones"])

              medical_history.pathological_condition.update(description:history["pac_descripcion"])

              medical_history.pathological_medicine.update(other_medicines:history["pame_otras_medicinas"])

              if history["peri_existe"]==1
                cities = City.where("name like '#{history["peri_ciudad"]}'")
                new_pediatry_history = PerinatalInformation.new(hospital: history["peri_hospital"],weight:history["peri_peso"],height:history["peri_altura"],
                pregnat_risks:history["peri_riesgo_embarazo"],other_info:history["peri_otra_info"])
                new_pediatry_history.medical_history = medical_history
                if cities.count > 0
                  new_pediatry_history.city = cities.first
                else
                  new_pediatry_history.city = doctor.city
                end
                  new_pediatry_history.save
                new_child_non_pathological =ChildNonPathologicalInfo.new(house_type:history["childn_tipo_casa"],
                number_people:history["childn_numero_personas"],child_academic_grade:history["childn_escolaridad"])
                new_child_non_pathological.medical_history = medical_history
                  new_child_non_pathological.save

                if history["padr_tienepadre"] == 1
                new_father = ParentInfo.new(first_name:history["padr_nombre_padre"],last_name:history["padr_apellidos_padre"],gender:history["padr_genero_padre"],birth_date:!history["padr_fechanacimiento_padre"].nil? ? history["padr_fechanacimiento_padre"].to_date : Time.now,
                blood_type:history["padr_tiposangre_padre"],academic_grade:history["padr_escolaridad_padre"])
                new_father.child_non_pathological_info = new_child_non_pathological
                new_father.save
                end
                if history["padr_tienemadre"] == 1
                new_mother = ParentInfo.new(first_name:history["padr_nombre_madre"],last_name:history["padr_apellidos_madre"],gender:history["padr_genero_madre"],birth_date:!history["padr_fechanacimiento_madre"].nil? ? history["padr_fechanacimiento_madre"].to_date : Time.now,
                                            blood_type:history["padr_tiposangre_madre"],academic_grade:history["padr_escolaridad_madre"])
                new_mother.child_non_pathological_info = new_child_non_pathological
                new_mother.save
                end

              end

            if history["gine_existe"] == 1
              new_inherited = InheritedFamilyBackground.new(gynecological_diseases:history["inh_ginecopatia"],gynecological_diseases_description:history["inh_ginecopatia_descrip"],mammary_dysplasia:history["inh_mastopatia"],
              mammary_dysplasia_description:history["inh_mastopatia_descrip"],twinhood:history["inh_gemelaridad"],twinhood_description:history["inh_gemelaridad_descrip"],congenital_anomalies:history["inh_defecto_congenito"],
              congenital_anomalies_description:history["inh_defecto_congenito_descrip"],infertility:history["inh_infertilidad"],infertility_description:history["inh_infertilidad_descrip"],others:history["inh_otros"],others_description:history["inh_otros_descrip"],
              anotations:history["inh_anotaciones"])
              new_inherited.medical_history = medical_history
              new_inherited.save

              new_gyne_obs = GynecologistObstetricalBackground.new(menstruation_start_age:history["gyn_edad_primera_menstruacion"],married:history["gyn_casada"],spouse_name:history["gyn_nombre_conyuge"],gestations:history["gyn_gestaciones"],
              natural_birth:history["gyn_partos_naturales"],caesarean_surgery:history["gyn_cesarias"],abortion_number:history["gyn_abortos"],abortion_causes:history["gyn_abortos_causas"],
              menstruation_type:history["gyn_tipo_menstruacion"],sexually_active:history["gyn_sexualmente_activa"],contraceptives_method:history["gyn_metodo_anticonceptivo"],
              menopause:history["gyn_menopausia"],anotations:history["gyn_anotaciones"],information_spouse:history["gyn_informacion_conyuge"],menses_last_date:!history["gyn_FUM"].nil? ? history["gyn_FUM"].to_date : Time.now,
              papanicolau_last_date:!history["gyn_ultimo_papanicolao"].nil? ? history["gyn_ultimo_papanicolao"].to_date : Time.now,cycles:history["gyn_ciclos"],ivs:history["gyn_ivs"],fup:history["gyn_fup"],sexual_partners:history["gyn_parejas_sexuales"],
              relationship_type:history["gyn_tipo_relacion"],std:history["gyn_std"])
              new_gyne_obs.medical_history = medical_history
              new_gyne_obs.save


            end
            patient_updated+=1
          else
            names_no_founded = names_no_founded + " - " + history["nombre"] + " " +history["apellido"]
          end


        end
      end
      respond_to do |format|
        format.html { redirect_to admin_importations_path, notice: 'Se actualizaron '+ patient_updated.to_s + ' pacientes y no se encontraros los siguientes nombres ' + names_no_founded }
      end
  end

  def import_medical_consultation
    names_no_founded = ""
    patient_updated = 0;
    data_not_saved = ""
    not_saved = 0;
    doctor = Doctor.find(params[:doctor_for_migrate])
    office = doctor.offices.first
    next_folio = office.folio
    consult_info = get_data_csv params[:upload_file]
    begin
      MedicalExpedient.transaction do
      consult_info.each do |history|
        patient_found = Patient.where("name like '#{history["nombre"]}' AND last_name like '#{history["apellido"]}' AND doctor_id = #{params[:doctor_for_migrate]}")
        if patient_found.count >0
            medical_expedient_found = patient_found.first.medical_expedient
            

            new_vital = VitalSign.new(weight:history["vi_peso"],height:history["vi_altura"],temperature:history["vi_temperatura"],blood_pressure:history["vi_presion_sanguinea"],
            glucose:history["vi_glucosa"],imc:history["vi_imc"],suggested_weight:history["vi_peso_sugerido"],head_circumference:history["vi_circunferencia_cabeza"],
            heart_rate:history["vi_ritmo_cardiaco"],breathing_frequency:history["vi_frecuencia_respiratoria"],eco_rctg:history["vi_eco_rctg"],
            fu:history["vi_fu"],pres:history["vi_pres"],ff:history["vi_ff"],vdrl:history["vi_vdrl"],ego:history["vi_ego"],other:history["vi_otro"],
            systolic_pressure:history["vi_presion_sistolica"],diastolic_pressure:history["vi_presion_diastolica"],oxygen_saturation:0)



            new_prescription = Prescription.new(recommendations:history["pr_recomendaciones"])
            
            new_consultation = MedicalConsultation.new(folio:next_folio,notes:history["mc_notas"],current_condition:history["mc_condicionactual"],date_consultation:!history["mc_fechahora"].nil? ? history["mc_fechahora"].to_datetime : Time.now,
            diagnostic:history["mc_diagnostico"],is_charged:true,invoiced:true,price:history["mc_precio"],print_diagnostic:history["mc_imprime_diagnostico"])
            new_consultation.medical_expedient = medical_expedient_found
            new_consultation.office = office
            new_consultation.consultation_type = medical_expedient_found.doctor.consultation_types.first
            new_consultation.vital_sign = new_vital
            new_consultation.prescription = new_prescription


            if new_consultation.save
              next_folio += 1
              office.update(folio:next_folio)
              new_charge = Charge.new(details:history["ch_detalles"])
              new_charge.medical_consultation = new_consultation
              new_charge.save

              if history["ex_existe"] == 1
                new_exploration = Exploration.new(breasts:history["ex_pechos"],vulva:history["ex_vulva"],abdomen:history["ex_abdomen"],cervix:history["ex_cervix"],ovaries:history["ex_ovarios"],
                                                  annexed:history["ex_anexo"],edemas:history["ex_edemas"],uterine_height:history["ex_altura_utero"],
                                                  exploration_type_id:history["ex_tipo_exploracion"],head:history["ex_cabeza"],lungs:history["ex_pulmones"],heart:history["ex_corazon"],
                                                  extremities:history["ex_extremidades"],other:history["ex_otro"],vagina:history["ex_vagina"],pelvis:history["ex_pelvis"],
                                                  rectum:history["ex_recto"])
                new_exploration.medical_consultation = new_consultation
                new_exploration.save
              end

              patient_updated+=1
            else
              puts new_consultation.errors.full_messages
              not_saved += 1
              data_not_saved += history["nombre"] + " " + history["apellido"] + " fecha consulta " + history["mc_fechahora"] + " Folio: " + next_folio.to_s + " | "
              txt_root = Rails.root.join('uploads','bitConsultsMigra.txt')
              File.open(txt_root, 'wb') do |file|
                file.write(data_not_saved)
              end
              raise "Fallo insertar consultas medicas ver bitacora de en la carpeta de uploads"
            end
        else
          names_no_founded = names_no_founded + " - " + history["nombre"] + " " +history["apellido"]
        end
      end
      end#
    end
    respond_to do |format|
      format.html { redirect_to admin_importations_path, notice: 'Se actualizaron '+ patient_updated.to_s + ' consultas de pacientes y no se encontraros los siguientes nombres ' + names_no_founded  + ' No guardados ' + not_saved.to_s}
    end
  end

  def import_pregnancy
    names_no_founded = ""
    patient_updated = 0
    consult_info = get_data_csv params[:upload_file]
    begin
      consult_info.each do |history|
        patient_found = Patient.where("name like '#{history["nombre"]}' AND last_name like '#{history["apellido"]}' AND doctor_id = #{params[:doctor_for_migrate]}")
        if patient_found.count >0
          medical_expedient_found = patient_found.first.medical_expedient
          new_pregancy = Pregnancy.new(active:history["pg_activo"],last_menstruation_date:history["pg_fechaultmens"].nil? ? history["pg_fechaultmens"].to_date : Time.now,
          probable_birth_date:history["pg_fpp"].nil? ? history["pg_fpp"].to_date : Time.now,initial_weight:history["pg_pesoinicial"],baby_name:history["pg_nombrebb"],pregnancy_end_success:history["pg_finalizobien"],
          pregnancy_end_date:history["pg_fechafinalizacion"].nil? ? history["pg_fechafinalizacion"].to_date : Time.now)
          new_pregancy.medical_expedient = medical_expedient_found

          if new_pregancy.save!
            if history["pg_finalizobien"] == 1
             new_birth = BirthInfo.new(birth_type_id:history["b_tipo"],delivery_type_id:history["b_delivery_type"],
             episiotomy_type_id:history["b_episiotomy_type"],tear_type_id:history["b_tear_type"],
             duration_type_id:history["b_duration_type"],date:history["b_fecha"].nil? ? history["b_fecha"].to_datetime : Time.now,
             observations:history["b_observaciones"],rpm:history["b_rpm"],hospital:history["b_hospital"],
             aditional_observations:history["b_observacionesadicionales"])
              new_birth.pregnancy = new_pregancy
              new_birth.save
            else
              new_abortion = Abortion.new(abortion_type_id:history["ab_tipo"],
                                          date:history["ab_fecha"].nil? ? history["ab_fecha"].to_datetime : Time.now,
                                          hospital:history["ab_hospital"],observations:history["ab_observaciones"])
              new_abortion.pregnancy = new_pregancy
              new_abortion.save
            end
            patient_updated+=1
          else
            names_no_founded = names_no_founded + " - " + history["nombre"] + " " +history["apellido"]
          end

        end
      end
    end


    respond_to do |format|
      format.html { redirect_to admin_importations_path, notice: 'Se actualizaron '+ patient_updated.to_s + ' pacientes y no se encontraros los siguientes nombres ' + names_no_founded }
    end
  end
end