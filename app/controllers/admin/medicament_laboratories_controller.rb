class Admin::MedicamentLaboratoriesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_medicament_laboratory, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicamentLaboratoriesDatatable.new(view_context)}
    end
  end


  def new
    @medicament_laboratory= MedicamentLaboratory.new
  end



  def create
    @medicament_laboratory = MedicamentLaboratory.new(medicament_laboratory_params)
    respond_to do |format|
      if @medicament_laboratory.save
        format.html { redirect_to admin_medicament_laboratories_url, notice: 'Laboratorio Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @medicament_laboratory.update(medicament_laboratory_params)
        format.html { redirect_to admin_medicament_laboratories_url, notice: 'laboratorio  Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @medicament_laboratory.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Laboratorio Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_medicament_laboratory
    @medicament_laboratory = MedicamentLaboratory.find(params[:id])
  end


  def medicament_laboratory_params
    params.require(:medicament_laboratory).permit(:name)
  end

end