class Admin::AssistantsController < ApplicationController
  include ApplicationHelper
  before_action :set_assistant_shared, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Banco".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: AssistantSharedsDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Banco".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Banco".
  def new
    @assistant = Assistant.new
    @assistant.build_user
    @assistant.build_city
  end


  def create
    @text_button = "Guardar"
    @assistant = Assistant.new(assistant_shared_params)
    set_city_model params, @assistant
    respond_to do |format|
      if @assistant.save
        format.html { redirect_to edit_admin_assistant_path(@assistant), notice: 'Se a creado exitosamente un asistente.' }
        format.json { render :show, status: :created, location: @assistant }
      else
        @assistant.build_city
        format.html { render :new }
        format.json { render json: @assistant.errors, status: :unprocessable_entity }
      end
    end
  end

  # Consulta un registro de tipo "Banco" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Banco", para poder ser editado.
  def edit

  end

  # Actualiza un registro de tipo "Banco" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Banco" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Banco".
  def update
    @city_name = params[:city_name]
    respond_to do |format|
      if @assistant.update(assistant_shared_params)
        set_city_model params, @assistant
        @assistant.update(city_id: @assistant.city_id)
        format.html { redirect_to admin_assistants_path, notice: 'Asistente acuatializado correctamente' }
        format.json { render :show, status: :ok, location: @assistant }
      else
        @assistant.build_city
        format.html { render :edit }
        format.json { render json: @assistant.errors, status: :unprocessable_entity }
      end
    end
  end

  # Elimina un registro de tipo "Banco" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Banco".
  def destroy
    @assistant.user.destroy
    @assistant.destroy
    respond_to do |format|
      format.html { redirect_to admin_assistants_url, notice: 'Se a eliminado correctamente el asistente' }
      format.json { head :no_content }
    end

  end
  def get_assistant_data
    doctors = []
    assist = []
    assistant = Assistant.find(params[:assistant_id])
    assistant.doctors.each do |doc|
      doctors.push(
          {
              name: doc.name,
              last_name:doc.last_name
          }
      )
    end
    bith_day_asssis = ""
    if assistant.birth_day != nil
      bith_day_asssis = assistant.birth_day.strftime('%d/%m/%Y')
    end
    assist.push(
        {
            name:assistant.name,
            last_name:assistant.last_name,
            birth_day: bith_day_asssis,
            telephone: assistant.telephone,
            cellphone: assistant.cellphone,
            email: assistant.user.email,
            address: assistant.address,
            gender: assistant.gender.name,
            city: assistant.city.name+","+assistant.city.state.name+","+assistant.city.state.country.name
        }
    )
    render :json => {:assist => assist, :docs => doctors
    }, layout: false
  end
  def get_doctor_offices
    doctor = Doctor.find(params[:doctor_id])
    offices = []
    doctor.offices.each do |office|
      offices.push(
                 {
                  id: office.id,
                  name: office.name
                 }
      )
    end
    render :json => {:offices => offices}, layout: false
  end
  def add_doctor_assistant
    assistant = Assistant.find(params[:assistant_id])
    doctor = Doctor.find(params[:doctor_id])
    respond = true;
    assistant.doctors.each do |doc|
    if doc.id == doctor.id
      respond = false;
    end
    end
    if respond
      assistant.doctors << doctor
      OfficePermission.new(assistant_id: assistant.id, office_id: params[:office_id].to_i,permission_id:1).save
      OfficePermission.new(assistant_id: assistant.id, office_id: params[:office_id].to_i,permission_id:2).save
      OfficePermission.new(assistant_id: assistant.id, office_id: params[:office_id].to_i,permission_id:3).save
      OfficePermission.new(assistant_id: assistant.id, office_id: params[:office_id].to_i,permission_id:4).save
      OfficePermission.new(assistant_id: assistant.id, office_id: params[:office_id].to_i,permission_id:5).save
    end


    respond_to do |format|
      format.json { render json: {:respond => respond}}
    end
  end

  def delete_doctor_assistant
    assistant = Assistant.find(params[:assistant_id])
    doctor = Doctor.find(params[:doctor_id])
    assistant.doctors.delete(doctor)
    assis_per = []
    doc_offi = []
    doctor.offices.each do |office|
      doc_offi.push(office.id)
    end
    assistant.office_permissions.each do |per|
      assis_per.push(per.office.id)
    end
    comoon_ids = (assis_per & doc_offi)
    OfficePermission.delete_all("office_id = #{comoon_ids.first} and assistant_id = #{assistant.id}")
    respond_to do |format|
      format.json { render json: 'true'}
    end
  end

  private
  # Inicializa un objeto de tipo "Banco" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Banco".
  # @return [Bank] Objeto de tipo "Banco"
  def set_assistant_shared
    @assistant = Assistant.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Banco" sean: nombre, es local.
  def assistant_shared_params
    params.require(:assistant).permit(:address, :birth_day, :cellphone, :gender, :name, :gender_id,
                                      :last_name,:shared, :telephone, :doctor_id,:city_id, user_attributes:[:id, :user_name, :password, :email])
  end

end