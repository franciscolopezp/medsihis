class Admin::IdentityCardsController < ApplicationController
  before_action :set_identity_card, only: [:show, :edit, :update, :destroy]

  # Genera un registro de tipo "Enfermedad" en la base de datos y despliega un mensaje en formato json.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Enfermedad" en la base de datos.
  # @return [string] String en formato json que contiene un mensaje con el estado de la operación.
  def create
    @identity_card = IdentityCard.new(identity_card_params)
    respond_to do |format|
      if @identity_card.save
        msg = {:status => "ok" , :message => "Se agregó la especialidad"}
        format.json { render :json => msg  }
      else
        msg = {:status => "fail" , :message => "Hubo un error"}
        format.json { render :json => msg  }
      end
    end
  end

  # Actualiza un registro de tipo "Enfermedad" de la base de datos y despliega un mensaje en formato json.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Enfermedad" de la base de datos.
  # @return [string] String en formato json que contiene un mensaje con el estado de la operación.
  def update
    respond_to do |format|
      if @identity_card.update(identity_card_params)
        msg = {:status => "ok" , :message => "Se actualizó la especialidad"}
        format.json { render :json => msg  }
      else
        msg = {:status => "fail" , :message => "Hubo un error"}
        format.json { render :json => msg  }
      end
    end
  end

  # Elimina un registro de tipo "Enfermedad" de la base de datos y despliega un mensaje en formato json.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [string] String en formato json que contiene un mensaje con el estado de la operación.
  def destroy
    @identity_card.destroy
    respond_to do |format|
      format.html { redirect_to admin_identity_cards_path , notice: 'Se eliminó la Especialidad'}
      format.json { head :no_content}
    end
  end

  private
  # Inicializa un objeto de tipo "Enfermedad" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Enfermedad".
  # @return [IdentityCard] Objeto de tipo "Enfermedad"
  def set_identity_card
    @identity_card = IdentityCard.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Enfermedad" sean: cédula profesional, id del doctor, 
  # id de la especialidad, instituto o universidad.
  def identity_card_params
    params.require(:identity_card).permit(:identity, :doctor_id, :specialty_id,:institute)
  end

end