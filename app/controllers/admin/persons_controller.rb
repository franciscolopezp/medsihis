class Admin::PersonsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  include ApplicationHelper
  include Admin::PersonsHelper
  before_action :set_person, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: PersonsDatatable.new(view_context)}
    end
  end


  def new
    @person = Person.new
    @person.build_user
    @person.build_city
  end

  def save_doctors_agendas
    user = User.find(params[:user_id])
    user.doctors.destroy_all
    if params[:doctors] != nil
      params[:doctors].each do |idx, item|
        doctor = Doctor.find(item['doctor_id'].to_i)
        user.doctors << doctor
      end
    end
    render :json => {:done => true}, layout: false
  end
  def get_doctors_agendas
    user = User.find(params[:user_id])
    doctors = []
    user.doctors.each do |doctor|
      doctors.push({
          :name => doctor.user.person.full_name,
          :id => doctor.id
                   })
    end
    render :json => {:done => true,:doctors => doctors}, layout: false
  end
  def find_person
      to_search = params[:term]
      render :json => search_person(to_search)
  end
  def create
    @text_button = "Guardar"
    @person = Person.new(person_params)
    set_city_model params, @person
    respond_to do |format|
      if @person.save
        if @person.user.is_doctor
          format.html { redirect_to new_admin_doctors_path(@person.user.id), notice: '' }
          format.json { render :show, status: :created, location: @person }
        else
          format.html { redirect_to admin_persons_path, notice: '' }
          format.json { render :show, status: :created, location: @person }
        end
      else
        @person.build_city
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end


  def edit

  end


  def update
    @city_name = params[:city_name]
    respond_to do |format|
      if @person.update(person_params)
        if @person.user.is_doctor
          doctor = @person.user.doctor
          if doctor != nil
            format.html { redirect_to edit_admin_doctor_path(doctor.id,false), notice: '' }
            format.json { render :show, status: :created, location: @person }
          else
            format.html { redirect_to new_admin_doctors_path(@person.user.id), notice: '' }
            format.json { render :show, status: :created, location: @person }
          end
        else
          format.html { redirect_to admin_persons_path, notice: '' }
          format.json { render :show, status: :created, location: @person }
        end
        format.html { redirect_to admin_persons_path, notice: '' }
        format.json { render :show, status: :ok, location: @person }
      else
        @person.build_city
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @person.user.destroy
    @person.destroy
    respond_to do |format|
      format.html { redirect_to admin_persons_url, notice: '' }
      format.json { head :no_content }
    end

  end

  private

  def set_person
    @person = Person.find(params[:id])
  end


  def person_params
    params.require(:person).permit(:address, :birth_day, :cellphone, :name, :gender_id,
                                      :last_name,:telephone, :doctor_id,:city_id, user_attributes:[:id,:can_cancel_payment,:can_close_charge,:is_doctor,:is_active, :user_name, :password, :email, :role_id])
  end

end