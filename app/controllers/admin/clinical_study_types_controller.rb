class Admin::ClinicalStudyTypesController < ApplicationController
  before_action :set_clinical_study_type, only: [:show, :edit, :update, :destroy]

  def index
    @lab_id = params[:lab_id]
    @lab = Laboratory.find(params[:lab_id]) if params[:lab_id].present?

    respond_to do |format|
      format.html
      format.json {
        laboratory = params[:laboratory]
        render json: ClinicalStudyTypesDatatable.new(view_context,laboratory,nil)
      }
    end
  end

  def new
    @clinical_study_type = ClinicalStudyType.new
    @clinical_study_type.laboratory_id = params[:lab_id]
    @lab_id = params[:lab_id]
    @text_button = "Guardar"
  end

  def edit
    @lab_id = @clinical_study_type.laboratory_id
    @text_button = "Guardar"
  end


  def create
    @text_button = "Guardar"
    @clinical_study_type = ClinicalStudyType.new(clinical_study_type_params)

    respond_to do |format|
      if @clinical_study_type.save
        format.html { redirect_to admin_clinical_study_types_path+'?lab_id='+params[:clinical_study_type][:laboratory_id].to_s, notice: 'El estudio clinico fue agregada con exito'}
        format.json { render show, status: :ok, location: @clinical_study_type}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @clinical_study_type.update(clinical_study_type_params)
        format.html { redirect_to admin_clinical_study_types_path+'?lab_id='+params[:clinical_study_type][:laboratory_id].to_s, notice: 'Estudio clinico actualizada'}
        format.json { render show, status: :ok,location: @clinical_study_type}
      else
        format.html { render :edit}
        format.json { render json: 'error estudio clinico', status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @clinical_study_type.destroy
    respond_to do |format|
      format.html { redirect_to admin_clinical_studies_path, notice: 'Estudio clinico eliminado'}
      format.json { head :no_content}
    end
  end

  private

  def set_clinical_study_type
    @clinical_study_type = ClinicalStudyType.find(params[:id])
  end

  def clinical_study_type_params
    params.require(:clinical_study_type).permit(:name,:laboratory_id,:indication_ids => [])
  end
end