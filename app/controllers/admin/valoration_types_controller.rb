class Admin::ValorationTypesController < ApplicationController
  load_and_authorize_resource :only => [:index, :new, :destroy, :edit]
  before_action :set_valoration_type, only: [:show, :edit, :update, :destroy]

  def index
  end

  def show
    fields = @valoration_type.valoration_fields.map{|vf| vf.id}
    render json: {:data => @valoration_type, :fields => fields}
  end

  def new
  end

  def edit
  end

  def create
    @valoration_type = ValorationType.new(valoration_type_params)
    fields = []
    if params[:fields].present?
      params[:fields].each do |f|
        fields.push(ValorationField.find(f))
      end
    end
    @valoration_type.valoration_fields = fields
    render json: {:done => @valoration_type.save}
  end

  def update
    fields = []
    if params[:fields].present?
      params[:fields].each do |f|
        fields.push(ValorationField.find(f))
      end
    end

    @valoration_type.update(valoration_type_params)
    @valoration_type.update(valoration_fields: fields)
    render json: {:done => true }
  end

  def destroy
    render json: {:done => @valoration_type.destroy }
  end

  private
  def set_valoration_type
    @valoration_type = ValorationType.find(params[:id])
  end

  def valoration_type_params
    params.require(:valoration_type).permit(:name, :valoration_type)
  end
end
