class Admin::RolesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:permission]
  before_action :set_role, only: [:show, :edit, :update, :destroy,:permission]

  def index
    respond_to do |format|
      format.html
      format.json { render json: RolesDatatable.new(view_context)}
    end
  end


  def new
    @text_button = "Guardar"
    @role = Role.new
  end


  def permission

  end
  def edit
    @text_button = "Guardar"
  end

  def create
    @text_button = "Guardar"
    @role = Role.new(role_params)

    respond_to do |format|
      if @role.save
        format.html { redirect_to admin_roles_path, notice: 'El Rol fue agregado con exito'}
        format.json { render show, status: :ok, location: @role}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @role.update(role_params)
        format.html { redirect_to admin_roles_path, notice: 'Rol actualizado'}
        format.json { render show, status: :ok,location: @role}
      else
        format.html { render :edit}
        format.json { render json: 'error rol', status: :unprocessable_entity}
      end
    end
  end


  def destroy
    @role.destroy
    respond_to do |format|
      format.html { redirect_to admin_roles_path, notice: 'rol eliminado'}
      format.json { head :no_content}
    end
  end
  def save_permissions
    rol = Role.find(params[:role_id])


    rol.actions.delete_all

    params[:permissions].each do |idx,perm|
      if perm["actions"]!=nil
        perm["actions"].each do |idx,act|
          rol.actions << Action.find(act["action_id"].to_i)
        end
      end
    end

    respond_to do |format|
      format.html {render json: true}
    end
  end
  def get_permissions
    permissions = []
    rol = Role.find(params[:role_id])
    rol.actions.each do |modperm|
      permissions.push({
                           :name=>"section_" + modperm.section.id.to_s
                       })
        permissions.push({
                             :name=>"action_" + modperm.id.to_s
                         })
    end
    respond_to do |format|
      format.html { render json: permissions}
    end
  end

  private

  def set_role
    @role = Role.find(params[:id])
  end


  def role_params
    params.require(:role).permit(:name)
  end
end