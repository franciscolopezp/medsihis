class Admin::LicensesController < ApplicationController
  include Admin::LicensesHelper
  before_action :set_license, only: [:edit, :update, :destroy]

  # Hace un llamado al método "create_licence" de la clase "LicensesHelper" para generar un registro de tipo "Licencia" en la base de datos 
  # y despliega un mensaje en formato json.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Licencia" en la base de datos.
  # @return [string] String en formato json que contiene un mensaje con el estado de la operación.
  def create

    result = create_license params
    respond_to do |format|
      if result[:result]
        msg = {:status => true , :message => "Se agregó la licencia"}
        format.json { render :json => msg  }
      else
        msg = {:status => false , :message => "No se agregó la licencia", :errors => result[:errors]}
        format.json { render :json => msg  }
      end
    end
  end

  # Despliega un registro de tipo "Licencia" y un arreglo de las "Especialidades" contenidas por ella en formato json.
  #
  # @param params [hash] Hash con la llave "id" que representa el id del registro de tipo "Licencia" a ser desplegado.
  # @return [string] String en formato json que contiene los datos de un registro de tipo "Licencia" y un arreglo de registros de tipo "Especialidad".
  def show
    @license = License.includes(:license_features).find(params[:id])
    features = [];
    @license.license_features.each do |lf|
      features.push({licence:lf.feature.id,price:lf.cost})
    end
    msg = {:license => @license , :features => features}
    respond_to do |format|
      format.json { render :json =>  msg}
    end
  end

  # Hace un llamado al método "update_license" de la clase "LicensesHelper" para actualizar un registro de tipo "Licencia" de la base de datos y despliega un mensaje en formato json.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Licencia" de la base de datos.
  # @return [string] String en formato json que contiene un mensaje con el estado de la operación.
  def update
    result = update_license params
    respond_to do |format|
      if result[:result]
        msg = {:status => true , :message => "Se actualizó la licencia"}
        format.json { render :json => msg  }
      else
        msg = {:status => false , :message => "No se actualizó la licencia", :errors => result[:errors]}
        format.json { render :json => msg  }
      end

    end
  end

  # Elimina un registro de tipo "Licencia" de la base de datos y despliega un mensaje en formato json.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [string] String en formato json que contiene un mensaje el estado de la operación.
  def destroy
    @license.destroy
    respond_to do |format|
      format.html { redirect_to admin_identity_cards_path , notice: 'Se eliminó la Licencia'}
      format.json { head :no_content}
    end
  end

  private
  # Inicializa un objeto de tipo "Licencia" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Licencia".
  # @return [License] Objeto de tipo "Licencia"
  def set_license
    @license = License.find(params[:id])
  end

end