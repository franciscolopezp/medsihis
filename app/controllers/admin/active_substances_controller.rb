class Admin::ActiveSubstancesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_active_substance, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: ActiveSubstancesDatatable.new(view_context)}
    end
  end


  def new
    @active_substance= ActiveSubstance.new
  end



  def create
    @active_substance = ActiveSubstance.new(active_substance_params)
    respond_to do |format|
      if @active_substance.save
        format.html { redirect_to admin_active_substances_url, notice: 'Sustancia activa Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @active_substance.update(active_substance_params)
        format.html { redirect_to admin_active_substances_url, notice: 'Sustancia activa Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy
    if @active_substance.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Almacen eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_active_substance
    @active_substance = ActiveSubstance.find(params[:id])
  end


  def active_substance_params
    params.require(:active_substance).permit(:name)
  end

end