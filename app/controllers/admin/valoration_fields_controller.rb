class Admin::ValorationFieldsController < ApplicationController
  load_and_authorize_resource :only => [:index, :new, :destroy, :edit]
  before_action :set_valoration_field, only: [:show, :edit, :update, :destroy]

  def index
  end

  def show
    render json: @valoration_field.to_json
  end

  def new
  end

  def edit
  end

  def create
    @valoration_field = ValorationField.new(valoration_field_params)
    render json: {:done => @valoration_field.save}
  end

  def update
    render json: {:done => @valoration_field.update(valoration_field_params)}
  end

  def destroy
    render json: {:done => @valoration_field.destroy }
  end

  def add_to_valoration
    resp = true
    valoration_type = ValorationType.find(params[:valoration_type_id])
    valoration_field = valoration_type.valoration_fields.where(:id => params[:valoration_field_id]).first
    if valoration_field.present?
      resp = false
    else
      valoration_type.valoration_fields.push(ValorationField.find(params[:valoration_field_id]))
    end
    render json: {:done => resp}
  end

  private
  def set_valoration_field
    @valoration_field = ValorationField.find(params[:id])
  end

  def valoration_field_params
    params.require(:valoration_field).permit(:name, :label, :unit, :code, :f_type, :f_html_type,:collection_type)
  end
end
