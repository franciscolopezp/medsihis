class Admin::TriagesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_triage, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: TriagesDatatable.new(view_context)}
    end
  end


  def new
    @triage = Triage.new
  end



  def create
    @triage = Triage.new(triage_params)
    respond_to do |format|
      if @triage.save
        format.html { redirect_to admin_triages_url, notice: 'Triage Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @triage.update(triage_params)
        format.html { redirect_to admin_triages_url, notice: 'Triage Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @triage.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Triage Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_triage
    @triage = Triage.find(params[:id])
  end


  def triage_params
    params.require(:triage).permit(:level,:description,:attention_time,:color, :priority)
  end

end