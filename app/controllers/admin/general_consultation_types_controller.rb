class Admin::GeneralConsultationTypesController < ApplicationController
  before_action :set_general_consultation_type, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: ConsultationTypesDatatable.new(view_context)}
    end
  end


  def new
    @text_button = "Guardar"
    @general_consultation_type = GeneralConsultationType.new
  end

  def save_consultation_types
    user = User.find(params[:user_id])
    user.consultation_types.delete_all
    params[:consultation_types].each do |idx,acc|
      consultation_type = ConsultationType.find(acc["consultation_type_id"].to_i)
      user.consultation_types << consultation_type
    end
    respond_to do |format|
      format.html {render json: true}
    end
  end
  def get_consultation_types
    consultation_types = []
    user = User.find(params[:user_id])
    user.consultation_types.each do |consultation_type|
      consultation_types.push({
                        :name=>"consultation_type_" + consultation_type.id.to_s
                    })
    end
    respond_to do |format|
      format.html { render json: consultation_types}
    end
  end
  def consultation_type_manage

  end
  def edit
    @text_button = "Guardar"
  end


  def create
    @text_button = "Guardar"
    @general_consultation_type = GeneralConsultationType.new(general_consultation_type_params)

    respond_to do |format|
      if @general_consultation_type.save
        format.html { redirect_to admin_general_consultation_types_path, notice: 'El tipo de consulta se agrego con exito'}
        format.json { render show, status: :ok, location: @general_consultation_type}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end


  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @general_consultation_type.update(general_consultation_type_params)
        format.html { redirect_to admin_general_consultation_types_path, notice: 'Tipo de consulta actualizada'}
        format.json { render show, status: :ok,location: @general_consultation_type}
      else
        format.html { render :edit}
        format.json { render json: 'error cuenta', status: :unprocessable_entity}
      end
    end
  end


  def destroy
    @general_consultation_type.destroy
    respond_to do |format|
      format.html { redirect_to admin_general_consultation_types_path, notice: 'Tipo de consulta eliminada'}
      format.json { head :no_content}
    end
  end

  private

  def set_general_consultation_type
    @general_consultation_type = GeneralConsultationType.find(params[:id])
  end


  def general_consultation_type_params
    params.require(:general_consultation_type).permit(:name,:cost)
  end
end