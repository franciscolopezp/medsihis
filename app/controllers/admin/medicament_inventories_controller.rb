class Admin::MedicamentInventoriesController < ApplicationController

  include ApplicationHelper
  before_action :set_medicament_inventory, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicamentInventoriesDatatable.new(view_context)}
    end
  end

  def new
    @medicament_inventory = MedicamentInventory.new
  end


  def create
    @medicament_inventory = MedicamentInventory.new(medicament_inventory_params)
    respond_to do |format|
      if @medicament_inventory.save
        format.html { redirect_to admin_inventories_url, notice: 'Inventario Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  def edit

  end

  def update
    respond_to do |format|
      if @medicament_inventory.update(medicament_inventory_params)
        format.html { redirect_to admin_inventories_url, notice: 'Inventario Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  def destroy
    if @medicament_inventory.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Inventario Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end
  end

  private
  def set_medicament_inventory
    @medicament_inventory = MedicamentInventory.find(params[:id])
  end

  def medicament_inventory_params
    params.require(:medicament_inventory).permit(:medicament_inventory_date, :warehouse_id, :medicament_category_id, :created_user_id, :updated_user_id)
  end




end