class Admin::CaGroupsController < ApplicationController
  before_action :set_group, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Grupo de Análisis Clínico".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string, view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    @lab_id = params[:lab_id]
    @lab = Laboratory.find(params[:lab_id]) if params[:lab_id].present?
    respond_to do |format|
      format.html
      format.json {
        laboratory = params[:laboratory]
        render json: CaGroupsDatatable.new(view_context,laboratory,nil)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Grupo de Análisis Clínico".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Grupo de Análisis Clínico".
  def new
    @text_button = "Guardar"
    @group = CaGroup.new
    @group.laboratory_id = params[:lab_id]
    @lab_id = params[:lab_id]
  end

  # Consulta un registro de tipo "Grupo de Análisis Clínico" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Grupo de Análisis Clínico" para poder ser editado.
  def edit
    @lab_id = @group.laboratory_id
    @text_button = "Guardar"
  end

  # Genera un registro de tipo "Grupo de Análisis Clínico" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Grupo de Análisis Clínico" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Grupo de Análisis Clínico".
  def create
    @text_button = "Guardar"
    @group = CaGroup.new(group_params)

    respond_to do |format|
      if @group.save
        format.html { redirect_to admin_ca_groups_path+'?lab_id='+params[:ca_group][:laboratory_id].to_s, notice: 'El grupo fue agregada con exito'}
        format.json { render show, status: :ok, location: @group}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  # Actualiza un registro de tipo "Grupo de Análisis Clínico" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Grupo de Análisis Clínico" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Grupo de Análisis Clínico".
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @group.update(group_params)
        format.html { redirect_to admin_ca_groups_path+'?lab_id='+params[:ca_group][:laboratory_id].to_s, notice: 'Grupo actualizado'}
        format.json { render show, status: :ok,location: @group}
      else
        format.html { render :edit}
        format.json { render json: 'error grupo', status: :unprocessable_entity}
      end
    end
  end

  # Elimina un registro de tipo "Grupo de Análisis Clínico" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Grupo de Análisis Clínico".
  def destroy
    @group.destroy
    respond_to do |format|
      format.html { redirect_to admin_ca_groups_path, notice: 'Gropo eliminado'}
      format.json { head :no_content}
    end
  end

  private
  # Inicializa un objeto de tipo "Grupo de Análisis Clínico" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Grupo de Análisis Clínico".
  # @return [CaGroup] Objeto de tipo "Grupo de Análisis Clínico"
  def set_group
    @group = CaGroup.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Grupo de Análisis Clínico" sean: nombre.
  def group_params
    params.require(:ca_group).permit(:name,:laboratory_id)
  end
end