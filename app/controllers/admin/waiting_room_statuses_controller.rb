class Admin::WaitingRoomStatusesController < ApplicationController
  load_and_authorize_resource :only => [:index,:new,:destroy,:edit]
  before_action :set_waiting_room_status, only: [:show, :edit, :update, :destroy]


  def index

  end

  def create
    @waiting_room_status = WaitingRoomStatus.new(waiting_room_status_params)
    @waiting_room_status.s_order = WaitingRoomStatus.count
    activities = []
    if params[:activities].present?
      activities = WaitingRoomActivity.where('id IN (?)', params[:activities])
    end
    @waiting_room_status.waiting_room_activities = activities
    render json: {:done => @waiting_room_status.save}
  end

  def update
    @waiting_room_status.update(waiting_room_status_params)
    activities = []
    if params[:activities].present?
      activities = WaitingRoomActivity.where('id IN (?)', params[:activities])
    end
    @waiting_room_status.update(waiting_room_activities: activities)
    render json: {:done => true}
  end

  def show
    render json: {:data => @waiting_room_status, :activities => @waiting_room_status.waiting_room_activities.map{|a| a.id}}
  end

  def destroy

  end

  def save_order
    order = 0
    params[:ids].each do |id|
      WaitingRoomStatus.update(id, {s_order: order})
      order += 1
    end
    render json: {:done => true}
  end

  private
  def waiting_room_status_params
    params.require(:waiting_room_status).permit(:name, :description)
  end

  def set_waiting_room_status
    @waiting_room_status = WaitingRoomStatus.find(params[:id])
  end
end