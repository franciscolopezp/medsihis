class Admin::IndexCommentsController < ApplicationController

  before_action :set_index_comment, only: [:show, :edit, :update, :destroy]

  # Despliega al usuario una página en formato html que contiene dos listas con registros de tipo "Comentario de Usuario".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene dos listas de registros de tipo "Comentario de Usuario" agrupados por tipo: Doctor y Paciente.
  def index
    @doctors = IndexComment.where('p_type = ?',IndexComment::DOCTOR)
    @patients = IndexComment.where('p_type = ?',IndexComment::PATIENT)
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Comentario de Usuario".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Comentario de Usuario".
  def new
    @index_comment = IndexComment.new
  end

  # Genera un registro de tipo "Comentario de Usuario" en la base de datos y redirecciona a la página que contiene la lista.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Comentario de Usuario" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista de registros de tipo "Comentario de Usuario".
  def create
    @index_comment = IndexComment.new(index_comment_params)
    respond_to do |format|
      if @index_comment.save
        format.html { redirect_to admin_index_comments_url, notice: 'Comentario agregado satisfactoriamente' }
      else
        format.html { render :new }
      end
    end

  end

  # Consulta un registro de tipo "Comentario de Usuario" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Comentario de Usuario" para poder ser editado.
  def edit

  end

  # Actualiza un registro de tipo "Comentario de Usuario" de la base de datos y redirecciona a la página que contiene la lista.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Comentario de Usuario" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista de registros de tipo "Comentario de Usuario".
  def update
    respond_to do |format|
      if @index_comment.update(index_comment_params)
        format.html { redirect_to admin_index_comments_url, notice: 'Comentario actualizado correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  # Elimina un registro de tipo "Comentario de Usuario" de la base de datos y redirecciona a la página que contiene la lista.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista de registros de tipo "Comentario de Usuario".
  def destroy
    @index_comment.destroy
    respond_to do |format|
      msg = {:done => true , :message => 'Eliminado'}
      format.html { render :json => msg  }
    end

  end

  # Permite la descarga de la foto de una persona que ha hecho un comentario de la aplicación.
  #
  # @param params [hash] Hash que contiene la llave "id" que representa el id del Comentario.
  # @return [file] Recurso de tipo imagen que puede ser descargado o desplegado desde un navegador.
  def profile_image
    index_comment = IndexComment.find(params[:id])
    send_file index_comment.photo.current_path, :type => 'image/jpeg', :disposition => 'inline'
  end

  private
  # Inicializa un objeto de tipo "Comentario de Usuario" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Comentario de Usuario".
  # @return [IndexComment] Objeto de tipo "Comentario de Usuario"
  def set_index_comment
    @index_comment = IndexComment.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Comentario de Usuario" sean: 
  # tipo, prefijo, nombre, apellido,fecha de nacimiento,título, universidad, género, comentario, foto, estado (activo/inactivo).
  def index_comment_params
    params.require(:index_comment).permit(:p_type,:prefix,:first_name,:last_name,:birth_date,:job_title,:university,:gender,:comments,:photo,:active)
  end

end
