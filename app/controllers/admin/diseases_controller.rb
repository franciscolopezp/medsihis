class Admin::DiseasesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_disease, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Enfermedad".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: DiseasesDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Enfermedad".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Enfermedad".
  def new
    @disease = Disease.new
    @disease.build_disease_category
    @disease.build_doctor
    @textd_button = "Guardar"
  end

  # Consulta un registro de tipo "Enfermedad" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Enfermedad" para poder ser editado.
  def edit
    @textd_button = "Guardar"
  end

  # Genera un registro de tipo "Enfermedad" en la base de datos y despliega un mensaje en formato json.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Enfermedad" en la base de datos.
  # @return [string] String que contiene los datos de un registro de tipo "Enfermedad".
  def disease_json
    new_disease = Disease.new(name: params[:name], code: params[:code], disease_category_id: params[:disease_category_id], doctor_id:session[:doctor_id])
    if new_disease.save
      respond_to do |format|
        format.json { render json: new_disease}
      end
    end
  end

  # Genera un registro de tipo "Enfermedad" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Enfermedad" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Enfermedad".
  def create
    @textd_button = "Guardar"
    @disease = Disease.new(disease_params)
    #@disease.doctor = Doctor.find(session[:doctor_id]) if session[:doctor_id] != nil
    respond_to do |format|
      if @disease.save
        format.html { redirect_to admin_diseases_path, notice: 'La Enfermedad fue agregada con exito'}
        format.json { render json: 'true'}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  # Actualiza un registro de tipo "Enfermedad" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Enfermedad" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Enfermedad".
  def update
    @textd_button = "Guardar"
    respond_to do |format|
      if @disease.update(disease_params)
        format.html { redirect_to admin_diseases_path, notice: 'Enfermedad actualizada'}
        format.json { render show, status: :ok,location: @disease}
      else
        format.html { render :edit}
        format.json { render json: 'error enfermedad', status: :unprocessable_entity}
      end
    end
  end

  # Elimina un registro de tipo "Enfermedad" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Enfermedad".
  def destroy
    @disease.destroy
    respond_to do |format|
      format.html { redirect_to admin_diseases_path, notice: 'Enfermedad eliminada'}
      format.json { head :no_content}
    end
  end

  private
  # Inicializa un objeto de tipo "Enfermedad" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Enfermedad".
  # @return [Disease] Objeto de tipo "Enfermedad"
  def set_disease
    @disease = Disease.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Enfermedad" sean: nombre y código.
  def disease_params
    params.require(:disease).permit(:code,:name,:disease_category_id, :doctor_id)
  end
end