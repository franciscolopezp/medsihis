class Admin::FinkokConfigsController < ApplicationController
  before_action :set_config, only: [:show, :edit, :update, :destroy]

  # Consulta un registro de tipo "Configuración de Finkok" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Configuración de Finkok" para poder ser editado.
  def edit
    @text_button = "Guardar"
  end

  # Actualiza un registro de tipo "Configuración de Finkok" de la base de datos y redirecciona a una página para editar el único registro que debe existir en base de datos.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Configuración de Finkok" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene el único registro de tipo "Configuración de Finkok" para poder ser editado.
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @finkok_config.update(config_params)
        format.html { redirect_to edit_admin_finkok_config_path(@finkok_config), notice: 'Configuración actualizada'}
        format.json { render show, status: :ok,location: @finkok_config}
      else
        format.html { render :edit}
        format.json { render json: 'error al actualizar', status: :unprocessable_entity}
      end
    end
  end

  private
  # Inicializa un objeto de tipo "Configuración de Finkok" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Configuración de Finkok".
  # @return [Disease] Objeto de tipo "Configuración de Finkok"
  def set_config
    @finkok_config = FinkokConfig.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de actualizar un registro de tipo "Configuración de Finkok" sean: 
  # usuario de prueba, contraseña de prueba, usuario de producción, contraseña de producción, url de prueba, url de producción, entorno actual.
  def config_params
    params.require(:finkok_config).permit(:test_username,:test_password,:prod_username,:prod_password,:test_wsdl,:prod_wsdl,:environment)
  end
end