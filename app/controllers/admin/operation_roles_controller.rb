class Admin::OperationRolesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_role, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Rol en Cirujía".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: OperationRolesDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Rol en Cirujía".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Rol en Cirujía".
  def new
    @text_button = "Guardar"
    @rol = OperationRole.new
  end

  # Consulta un registro de tipo "Rol en Cirujía" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Rol en Cirujía" para poder ser editado.
  def edit
    @text_button = "Guardar"
  end

  # Genera un registro de tipo "Rol en Cirujía" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Rol en Cirujía" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Rol en Cirujía".
  def create
    @text_button = "Guardar"
    @rol = OperationRole.new(role_params)

    respond_to do |format|
      if @rol.save
        format.html { redirect_to admin_operation_roles_path, notice: 'El rol fue agregado con exito'}
        format.json { render show, status: :ok, location: @rol}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  # Actualiza un registro de tipo "Rol en Cirujía" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Rol en Cirujía" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Rol en Cirujía".
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @rol.update(role_params)
        format.html { redirect_to admin_operation_roles_path, notice: 'rol actualizado'}
        format.json { render show, status: :ok,location: @rol}
      else
        format.html { render :edit}
        format.json { render json: 'error rol', status: :unprocessable_entity}
      end
    end
  end

  # Elimina un registro de tipo "Rol en Cirujía" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Rol en Cirujía".
  def destroy
    @rol.destroy
    respond_to do |format|
      format.html { redirect_to admin_operation_roles_path, notice: 'rol eliminado'}
      format.json { head :no_content}
    end
  end

  private
  # Inicializa un objeto de tipo "Rol en Cirujía" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Rol en Cirujía".
  # @return [Medicament] Objeto de tipo "Rol en Cirujía".
  def set_role
    @rol = OperationRole.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Rol en Cirujía" sean: nombre.
  def role_params
    params.require(:operation_role).permit(:name)
  end
end