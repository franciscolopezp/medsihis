  class Admin::FixedAssetsController < ApplicationController
    load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:manage_items]
  include Admin::FixedAssetsHelper
  before_action :set_fixed_asset, only: [:show, :edit, :update, :destroy,:manage_items]


  def index
    respond_to do |format|
      format.html
      format.json { render json: FixedAssetsDatatable.new(view_context)}
    end
  end


  def new
    @fixed_asset= FixedAsset.new
  end

  def manage_items

  end
  def get_fixed_asset_activities
    result = get_fixed_activities_response(params)
    respond_to do |format|
      format.html { render json: result}
    end
  end
  def create
    @fixed_asset = FixedAsset.new(fixed_asset_params)
    respond_to do |format|
      if @fixed_asset.save
        format.html { redirect_to admin_fixed_assets_url, notice: 'activo fijo Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @fixed_asset.update(fixed_asset_params)
        format.html { redirect_to admin_fixed_assets_url, notice: 'activo fijo Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @fixed_asset.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'activo fijo Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  def dashboard

  end

  def load_dashboard_items
    @items = FixedAssetItem.joins(:fixed_asset => :fixed_asset_category)
    if params[:category_id].present?
      @items = @items.where('fixed_assets.fixed_asset_category_id = ?',params[:category_id])
    end
    @items = @items.order('fixed_asset_categories.name ASC')
    render layout: false
  end

  def schedule
    @item = FixedAssetItem.find(params[:item_id])
    if params[:start_date].present?
      @start_date = DateTime.strptime(params[:start_date], '%d/%m/%Y')
    else
      @start_date = DateTime.now
    end

    if params[:end_date].present?
      @end_date = DateTime.strptime(params[:end_date], '%d/%m/%Y')
    else
      @end_date = DateTime.now
    end
  end

  private

  def set_fixed_asset
    @fixed_asset = FixedAsset.find(params[:id])
  end


  def fixed_asset_params
    params.require(:fixed_asset).permit(:name,:fixed_asset_category_id, :color)
  end

end