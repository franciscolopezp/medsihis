class Admin::ClinicInfosController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:default_backgrounds]
  include Doctor::ADD_FINKOK
  before_action :set_config, only: [:edit, :update]
  #skip_before_action :verify_authenticity_token, only: [:save_no_pathologicals]

  def edit
    @text_button = "Guardar"
  end
  def download_image
    info = ClinicInfo.find(params[:id])
    send_file info.logo.current_path, :type => 'image/jpeg', :disposition => 'inline'
  end

  def download_banner
    info = ClinicInfo.find(params[:id])
    send_file info.banner.current_path, :type => 'image/jpeg', :disposition => 'inline'
  end
  def download_index_image
    info = ClinicInfo.find(params[:id])
    send_file info.index_image.current_path, :type => 'image/jpeg', :disposition => 'inline'
  end
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @clinic_info.update(config_params)
        if params[:charge_reports] != nil
          @clinic_info.update(charge_reports:1)
        else
          @clinic_info.update(charge_reports:0)
        end
        if params[:is_active_custom_prescription] != nil
          @clinic_info.update(is_active_custom_prescription:1)
        else
          @clinic_info.update(is_active_custom_prescription:0)
        end
        if params[:change_files_fiscal].present?
          if !params[:clinic_info][:emisor_fiscal_information_attributes][:certificate_password].blank?
            # encriptar la nueva contraseña
            @clinic_info.emisor_fiscal_information.certificate_password = params[:clinic_info][:emisor_fiscal_information_attributes][:certificate_password]
            @clinic_info.emisor_fiscal_information.update!(certificate_password: @clinic_info.emisor_fiscal_information.encrypt_password)
          end
          respond_message = ""
          if !params[:clinic_info][:emisor_fiscal_information_attributes][:certificate_password].blank? &&
              !params[:clinic_info][:emisor_fiscal_information_attributes][:certificate_stamp].blank? &&
              !params[:clinic_info][:emisor_fiscal_information_attributes][:certificate_key].blank?

            exp_date_req =  %x'openssl x509 -enddate -noout -inform DER -in #{@clinic_info.emisor_fiscal_information.certificate_stamp.current_path.to_s}'
            exp_date = Date.parse exp_date_req
            today_date = Date.today
            if exp_date > today_date
              pem_op = create_pem_file @clinic_info
              if(!pem_op)
                respond_message += "Contraseña de documentos fiscales incorrecta, documentos no actualizados"
                @clinic_info.emisor_fiscal_information.remove_certificate_stamp = true
                @clinic_info.emisor_fiscal_information.remove_certificate_key = true
                @clinic_info.emisor_fiscal_information.update!(certificate_password: "")
                @clinic_info.errors.add(:base, :not_implemented, message: respond_message)

              end
            else
              @clinic_info.emisor_fiscal_information.remove_certificate_stamp = true
              @clinic_info.emisor_fiscal_information.remove_certificate_key = true
              @clinic_info.emisor_fiscal_information.update!(certificate_password: "")
              respond_message += "El certificado ha caducado. Fecha de caducidad #{exp_date.to_s}, documentos no actualizados"
            end
          end
        end
        @clinic_info.update(user_id:current_user.id)
        format.html { redirect_to edit_admin_clinic_info_path(@clinic_info), notice: 'Configuración actualizada'}
        format.json { render show, status: :ok,location: @clinic_info}
      else
        format.html { render :edit}
        format.json { render json: 'error al actualizar', status: :unprocessable_entity}
      end
    end
  end
  def get_emisor_info
    data = EmisorFiscalInformation.find(params[:id])
    respond_to do |format|
      format.html { render json: {:done => true, :data => data,:city => data.city.name}}
    end
  end
  def delete_emisor_info
    EmisorFiscalInformation.find(params[:id]).delete
    respond_to do |format|
      format.html { render json: {:done => true}}
    end
  end
  def getClinicInfo
    result = false
    clinic = ClinicInfo.first
    info = clinic.emisor_fiscal_information
    doc_info = []
    if info.certificate_key.file != nil  && info.certificate_stamp.file != nil
      result = true
    end
    doc_info.push({
                      :business_name => info.bussiness_name,
                      :rfc => info.rfc,
                      :address => info.fiscal_address,
                      :suburb => info.suburb,
                      :locality => info.locality,
                      :ext_number => info.ext_number,
                      :int_number => info.int_number,
                      :zip => info.zip,
                      :city => info.city.name,
                      :state => info.city.state.name,
                      :country => info.city.state.country.name,
                      :serie => info.serie,
                      :folio => info.folio,
                      :iva => info.iva,
                      :isr => info.isr,
                  })
    respond_to do |format|
      format.html { render json: {:done => result, :data => doc_info}}
    end
  end
  def create_pem_file clinic
    create_pem_output = true
    ruta_pem_file = Dir.pwd + "/uploads/clinic_" + clinic.emisor_fiscal_information.id.to_s + "/certificate_key/key.pem"
    fiscal_info = clinic.emisor_fiscal_information
    cert_pass = fiscal_info.decrypt_password
    console_output = %x'openssl pkcs8 -inform DER -in #{fiscal_info.certificate_key.current_path.to_s} -passin pass:#{cert_pass}'
    #valida que la contraseña sea la correcta si es correcta regresa algo cool
    if console_output.length > 3
      %x'openssl pkcs8 -inform DER -in #{fiscal_info.certificate_key.current_path.to_s} -passin pass:#{cert_pass} > #{ruta_pem_file}'
      #puts "pem correcto"
      #Obtiene el numero de serie del certificado y lo convierte al numero.
      certificado =  %x'openssl x509 -serial -noout -inform DER -in #{clinic.emisor_fiscal_information.certificate_stamp.current_path.to_s}'
      arreglonumero = certificado.to_s.split('=')
      arreglosolonumero = arreglonumero[1].to_s.split(//)
      serialNumberCer = []
      (1..39).step(2) do |n|
        serialNumberCer.push(arreglosolonumero[n])
        puts arreglosolonumero[n]
      end
      noCertificado = serialNumberCer.join.to_s
      clinic.emisor_fiscal_information.update(certificate_number: noCertificado)
      certificado =  %x'openssl x509 -inform DER -in #{clinic.emisor_fiscal_information.certificate_stamp.current_path.to_s}'
      arrayc =  certificado.split("\n")
      arrayc.shift
      arrayc.pop
      certificadobase64 = arrayc.join.to_s
      clinic.emisor_fiscal_information.update(certificate_base64: certificadobase64)
      clinic.emisor_fiscal_information.certificate_password = cert_pass
      clinic.emisor_fiscal_information.update(certificate_password: @clinic_info.emisor_fiscal_information.encrypt_password)
    else
      create_pem_output = false
      #puts "pem no correcto"
    end
    #registra un nuevo rfc
    cliente = Clientes2.new
    cliente.register_in_finkok clinic.emisor_fiscal_information.rfc
    create_pem_output
  end
  def delete_currency_exchange
    CurrencyExchange.find(params[:current_exchange_id]).destroy
    respond_to do |format|
      format.html { render json: {:true => true, :message => 'Registro eliminado correctamente'}}
    end
  end
  def add_currency_exchange
    clinic = ClinicInfo.first
    is_default = params[:is_default].to_i
    response = true
    exists_default = false
    old_default = nil
    if params[:edit_id] != ""
      edit_currency_exchange = CurrencyExchange.find(params[:edit_id])
      clinic.currency_exchanges.each do |cur|
        if cur.currency_id != edit_currency_exchange.currency.id
          if cur.currency_id == params[:currency_id].to_i
            response = false
          end
        end
        if cur.is_default
          exists_default = true
          old_default = cur
        end
      end
      if response
        if exists_default && is_default == 1
          old_default.update(is_default: 0)
        end
        edit_currency_exchange.currency = Currency.find(params[:currency_id])
        edit_currency_exchange.update(value:params[:value],is_default:is_default)
      end

    else
      clinic.currency_exchanges.each do |cur|
        if cur.currency_id == params[:currency_id].to_i
          response = false
        end
        if cur.is_default
          exists_default = true
          old_default = cur
        end
      end
      if response
        if exists_default && is_default == 1
          old_default.update(is_default: 0)
        end
        new_exchange = CurrencyExchange.new(value:params[:value],is_default:is_default)
        new_exchange.clinic_info = clinic
        new_exchange.currency = Currency.find(params[:currency_id])
        new_exchange.save
      end
    end


    respond_to do |format|
      format.html { render json: {:done => response, :message => 'Registro creado correctamente'}}
    end
  end

  def default_backgrounds
    default_orological_background_record = DefaultUrologicalBackground.first
    if default_orological_background_record != nil
      @default_urological_background = default_orological_background_record
    else
      @default_urological_background = DefaultUrologicalBackground.new
    end
    default_perinatal_background_record = DefaultPerinatalBackground.first
    if default_perinatal_background_record != nil
      @default_perinatal_background = default_perinatal_background_record
    else
      @default_perinatal_background = DefaultPerinatalBackground.new
    end
    default_ginecology_background_record = DefaultGinecologyBackground.first
      if default_ginecology_background_record != nil
        @default_ginecology_background = default_ginecology_background_record
      else
        @default_ginecology_background = DefaultGinecologyBackground.new
      end
    default_no_pathological_record = DefaultNoPathological.first
      if default_no_pathological_record != nil
        @default_no_pathological = default_no_pathological_record
      else
        @default_no_pathological = DefaultNoPathological.new
      end
    default_pathological_record = DefaultPathological.first
      if default_pathological_record != nil
        @default_pathological = default_pathological_record
      else
        @default_pathological = DefaultPathological.new
      end

  end
  def save_no_pathologicals
    respond_to do |format|
    no_pathological = DefaultNoPathological.first
    if no_pathological != nil
      if no_pathological.update(no_pathological_infos)
        format.html { redirect_to default_backgrounds_admin_clinic_infos_path, notice: 'Datos actualizados.' }
      end
    else
      no_pathological = DefaultNoPathological.new(no_pathological_infos)
      if no_pathological.save
        format.html { redirect_to default_backgrounds_admin_clinic_infos_path, notice: 'Datos actualizados.' }
      end
    end
    end
  end
  def save_ginecological_background
    respond_to do |format|
      ginecologycal_background = DefaultGinecologyBackground.first
      if ginecologycal_background != nil
        if ginecologycal_background.update(default_ginecology_backgroud_params)
          format.html { redirect_to default_backgrounds_admin_clinic_infos_path, notice: 'Datos actualizados.' }
        end
      else
        ginecologycal_background = DefaultGinecologyBackground.new(default_ginecology_backgroud_params)
        if ginecologycal_background.save
          format.html { redirect_to default_backgrounds_admin_clinic_infos_path, notice: 'Datos actualizados.' }
        end
      end
    end
  end
  def save_perinatal_background
    respond_to do |format|
      perinatal_background = DefaultPerinatalBackground.first
      if perinatal_background != nil
        if perinatal_background.update(default_perinatal_background_params)
          format.html { redirect_to default_backgrounds_admin_clinic_infos_path, notice: 'Datos actualizados.' }
        end
      else
        perinatal_background = DefaultPerinatalBackground.new(default_perinatal_background_params)
        if perinatal_background.save
          format.html { redirect_to default_backgrounds_admin_clinic_infos_path, notice: 'Datos actualizados.' }
        end
      end
    end
  end
  def save_urological_background
    respond_to do |format|
      urological_background = DefaultUrologicalBackground.first
      if urological_background != nil
        if urological_background.update(default_urological_background_params)
          format.html { redirect_to default_backgrounds_admin_clinic_infos_path, notice: 'Datos actualizados.' }
        end
      else
        urological_background = DefaultUrologicalBackground.new(default_urological_background_params)
        if urological_background.save
          format.html { redirect_to default_backgrounds_admin_clinic_infos_path, notice: 'Datos actualizados.' }
        end
      end
    end
  end
  def save_pathologicals
    respond_to do |format|
      pathological = DefaultPathological.first
      if pathological != nil
        if pathological.update(pathological_params)
          format.html { redirect_to default_backgrounds_admin_clinic_infos_path, notice: 'Datos actualizados.' }
        end
      else
        pathological = DefaultPathological.new(pathological_params)
        if pathological.save
          format.html { redirect_to default_backgrounds_admin_clinic_infos_path, notice: 'Datos actualizados.' }
        end
      end
    end
  end
  private

  def set_config
    @clinic_info = ClinicInfo.find(params[:id])
  end

  def no_pathological_infos
    params.require(:default_no_pathological).permit(:smoking,:smoking_description,:alcoholism,:alcoholism_description,:drugs,:drugs_description,:play_sport, :play_sport_description,
                                        :scolarship, :marital_status_id, :religion,:annotations)
  end
  def pathological_params
    params.require(:default_pathological).permit( :diabetes,:diabetes_description,:overweight,
                                                  :overweight_description,:hypertension,:hypertension_description,
                                                  :asthma,:asthma_description,:cancer,:cancer_description,:annotations,
                                                  :other_diseases,
                                                  :psychiatric_diaseases,:psychiatric_diaseases_description,
                                                  :neurological_diseases,
                                                  :neurological_diseases_description,:cardiovascular_diseases,
                                                  :cardiovascular_diseases_description,
                                                  :bronchopulmonary_diseases,
                                                  :bronchopulmonary_diseases_description,:thyroid_diseases,
                                                  :thyroid_diseases_description,:kidney_diseases,
                                                  :kidney_diseases_description,:surgeries,:transfusions,
                                                  :allergy,:allergy_description,:recent_diseases,:other_allergies,:other_medicaments)
  end
  def config_params
    params.require(:clinic_info).permit(:name,:address,:telephone,:contact,:logo,:banner,:city_id,:contact_email, :set_custom_ticket_description,
                                        :ticket_description, :ticket_series, :ticket_next_folio, :latitude, :longitude,:index_image,:show_index,
                                        emisor_fiscal_information_attributes:[:id, :rfc, :bussiness_name, :fiscal_address, :ext_number , :city_id , :certificate_stamp ,:certificate_key, :tax_regime,:zip,:suburb,:locality,:folio,:serie,:int_number,:iva])
  end
  def default_perinatal_background_params
    params.require(:default_perinatal_background).permit(:hospital,:weight,:height,:pregnancy_risks,:other_info,:scholarship,:house_type,
                    :hounse_inmates)
  end
  def default_ginecology_backgroud_params
    params.require(:default_ginecology_background).permit(:ginecopatia,
    :ginecopatia_description,
    :mastopatia,
    :mastopatia_description,
    :gemelaridad,
    :gemelaridad_description,
    :congenital_anomalies,
    :congenital_anomalies_description,
    :infertility,
    :infertility_description,
    :others,
    :others_diseases,
    :annotations,
    :menstruation_start_age,
    :married,
    :spouse_name,
    :gestations,
    :natural_births,
    :cesareans,
    :abortions,
    :abortion_causes,
    :menstruation_type,
    :sexually_active,
    :contraceptives_method,
    :menopause,
    :obstetrical_annotations,
    :spouse_information,
    :cycles,
    :ivs,
    :sexual_partners,
    :std)
  end
  def default_urological_background_params
    params.require(:default_urological_background).permit(:testicular_pain,
    :impotence,
    :erection_dificulty,
    :premature_ejaculation,
    :vaginal_discharge,
    :anormal_hair_growth,
    :back_pain,
    :kidneys_pain,
    :pelvic_pain,
    :low_abdomen_pain,
    :kidney_bk,
    :kidney_stones,
    :venereal_diseases,
    :self_examination,
    :prostate,
    :last_prostate_examination,
    :fum,
    :gestations,
    :last_pap_date,
    :contraceptive_method,
    :sexually_active,
    :sex_life_start,
    :sexual_partners,
    :relationship_type,
    :satisfying_sex,
    :intercourse_pain)
  end

end