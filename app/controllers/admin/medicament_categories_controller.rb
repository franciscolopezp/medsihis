class Admin::MedicamentCategoriesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_medicament_category, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicamentCategoriesDatatable.new(view_context)}
    end
  end


  def new
    @medicament_category= MedicamentCategory.new
  end



  def create
    @medicament_category = MedicamentCategory.new(medicament_category_params)
    respond_to do |format|
      if @medicament_category.save
        format.html { redirect_to admin_medicament_categories_url, notice: 'Categoría de medicamento creado correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @medicament_category.update(medicament_category_params)
        format.html { redirect_to admin_medicament_categories_url, notice: 'Categoría de medicamento actualizado correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @medicament_category.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Categoría de medicamento eliminado correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_medicament_category
    @medicament_category = MedicamentCategory.find(params[:id])
  end


  def medicament_category_params
    params.require(:medicament_category).permit(:name)
  end

end