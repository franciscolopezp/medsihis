class Admin::DoctorsController < ApplicationController
  include ApplicationHelper
  include Admin::DoctorsHelper
  include Doctor::ADD_FINKOK
  before_action :set_doctor, only: [:show, :edit, :update, :destroy, :send_email_reset_password]

  # Retorna una lista paginada de registros de tipo "Doctor".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: DoctorsDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con los datos de un Doctor.
  #
  # @param params [hash] Hash con la llave "id" que representa el id del Doctor que debe ser desplegado.
  # @return [view] Página en formato html con los datos de un Doctor.
  def show
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Doctor".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Doctor".
  def new
    @text_button = "Guardar"
    @doctor = Doctor.new
    @doctor.build_fiscal_information
    @doctor.fiscal_information.build_city
    @new_doctor = true
    @user_doctor = User.find(params[:user_id])
  end

  # Consulta un registro de tipo "Doctor" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Doctor" para poder ser editado.
  def edit
    @text_button = "Guardar"
    @new_doctor = false
  end

  # Permite la descarga de la imagen de la Firma Digital de un Doctor.
  #
  # @param params [hash] Hash que contiene la llave "id" que representa el id del Doctor.
  # @return [file] Recurso de tipo imagen que puede ser descargado o desplegado desde un navegador.
  def download_image
    doctor = Doctor.find(params[:id])
    if doctor.digital_sign != nil && doctor.digital_sign != ''
    send_file doctor.digital_sign.current_path, :type => 'image/jpeg', :disposition => 'inline'
    end
  end

  # Permite la descarga del archivo ".cer" de un Doctor.
  #
  # @param params [hash] Hash que contiene la llave "id" que representa el id del Doctor.
  # @return [file] Recurso de tipo texto para ser descargado desde un navegador.
  def download_certificate
    doctor = Doctor.find(params[:id])
    if doctor.fiscal_information.certificate_stamp.current_path != nil && doctor.fiscal_information.certificate_stamp.current_path != ''
    send_file doctor.fiscal_information.certificate_stamp.current_path
    end
  end

  # Permite la descarga del archivo ".key" de un Doctor.
  #
  # @param params [hash] Hash que contiene la llave "id" que representa el id del Doctor.
  # @return [file] Recurso de tipo texto para ser descargado desde un navegador.
  def download_key
    doctor = Doctor.find(params[:id])
    if doctor.fiscal_information.certificate_key.current_path != nil && doctor.fiscal_information.certificate_key.current_path != ''
    send_file doctor.fiscal_information.certificate_key.current_path
    end
  end
  def get_doctor_offices
    offices = []
    doctor = Doctor.find(params[:doctor_id])
    doctor.user.offices.each do |office|
      offices.push({
          :id => office.id,
          :name => office.name,
          :consult_duration => office.consult_duration
                   })
    end
    render :json => {:done => true, :offices =>offices}, layout: false
  end
  # Genera un registro de tipo "Doctor" en la base de datos y redirecciona a la página que contiene la lista paginada.
  # De igual manera crea un registro de tipo "Información Fiscal" y crea la relación entre ambos registros. 
  # Este método genera una contraseña encriptada para el médico y le notifica sobre la necesidad de cambiarla mediante un correo electrónico.
  # Si la información fiscal fue ingresada correctamente genera un archivo con extensión ".pem" el cual se utilizará al momento de que el doctor emita 
  # facturas electrónicas.
  #
  # @param params [hash] Hash con los datos necesarios para generar registros de tipo "Doctor" e "Información Fiscal" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Doctor".
  def create
   # enc_cert_pass =  encrypt(params[:doctor]['fiscal_information_attributes']['certificate_password'])
   # puts "Certificate Password: " + enc_cert_pass
   # puts "DEcrypted: " + decrypt(enc_cert_pass)

    @text_button = "Guardar"
    @doctor = Doctor.new(doctor_params)
    if params[:doctor][:searcher_visible] != nil
      @doctor.searcher_visible = 1
    else
      @doctor.searcher_visible = 0
    end
    @doctor.accept_term_conditions = true
    respond_message = 'El doctor fue actualizado exitosamente'
    respond_to do |format|
      begin
        Doctor.transaction do
          @doctor.save!
          @doctor.update(user_id:params[:doctor_user_id])
            if !params[:doctor][:fiscal_information_attributes][:certificate_password].blank?
              # encriptar la nueva contraseña
              @doctor.fiscal_information.certificate_password = params[:doctor][:fiscal_information_attributes][:certificate_password]
              @doctor.fiscal_information.update!(certificate_password: @doctor.fiscal_information.encrypt_password)
            end
            if @doctor.fiscal_information.certificate_password !="" && @doctor.fiscal_information.certificate_key!="" && @doctor.fiscal_information.certificate_password !=nil && @doctor.fiscal_information.certificate_key!=nil
              exp_date_req =  %x'openssl x509 -enddate -noout -inform DER -in #{@doctor.fiscal_information.certificate_stamp.current_path.to_s}'
              exp_date = Date.parse exp_date_req
              today_date = Date.today
              if exp_date > today_date
                pem_op = create_pem_file @doctor
                if(!pem_op)
                  respond_message += " \n Contraseña de documentos fiscales incorrecta, documentos no agregados"
                  @doctor.fiscal_information.remove_certificate_stamp = true
                  @doctor.fiscal_information.remove_certificate_key = true
                  @doctor.fiscal_information.update!(certificate_password: "")
                  @doctor.errors.add(:base, :not_implemented, message: respond_message)
                  #raise ActiveRecord::RecordInvalid.new(@doctor)
                end
              else
                @doctor.fiscal_information.remove_certificate_stamp = true
                @doctor.fiscal_information.remove_certificate_key = true
                @doctor.fiscal_information.update!(certificate_password: "")
                respond_message += " \n El certificado fiscal ha caducado. Fecha de caducidad #{exp_date.to_s}, documentos no agregados"
              end
            end
            #RecoverPassword.new_user_doctor(@doctor.user,abbreviation_dr_by_doctor(@doctor)).deliver_now
            format.html  { redirect_to admin_persons_path, notice: respond_message}
        end
      rescue ActiveRecord::RecordInvalid => invalid
        @new_doctor = true
        @doctor.build_fiscal_information
        @doctor.fiscal_information.build_city
        format.html { render :new, notice: respond_message}
      end

    end
  end

  # Envía un correo con instrucciones a un Doctor cuando éste solicita un cambio de contraseña.
  #
  # @param params [hash] Hash que contiene la llave "id" que representa el id del Doctor.
  # @return [string] String con valor "true" si el proceso se realizó correctamente
  def send_email_reset_password
    RecoverPassword.new_user_doctor(@doctor.user,abbreviation_dr_by_doctor(@doctor)).deliver_now
    render :json => true, layout: false
  end
  def check_cert_date doctor
    date_valid = false
    exp_date_req =  %x'openssl x509 -enddate -noout -inform DER -in #{doctor.fiscal_information.certificate_stamp.current_path.to_s}'
    exp_date = Date.parse exp_date_req
    today_date = Date.today
    if exp_date > today_date
      date_valid = true
    end
    date_valid
  end
  # Genera un archivo ".pem" y lo guarda en disco en la ruta "/uploads/fiscal_information_{id de la información fiscal del doctor}/certificate_key/key.pem"
  # Registra el rfc del doctor con el proveedor de timbres de facturación.
  #
  # @param doctor [Doctor] Objeto de tipo "Doctor".
  # @return [nil] No se retorna ningún valor.
  def create_pem_file doctor
    create_pem_output = true
    ruta_pem_file = Dir.pwd + "/uploads/fiscal_information_" + doctor.fiscal_information.id.to_s + "/certificate_key/key.pem"
    fiscal_info = doctor.fiscal_information
    cert_pass = fiscal_info.decrypt_password
    console_output = %x'openssl pkcs8 -inform DER -in #{fiscal_info.certificate_key.current_path.to_s} -passin pass:#{cert_pass}'
    #valida que la contraseña sea la correcta si es correcta regresa algo cool
    if console_output.length > 3
      %x'openssl pkcs8 -inform DER -in #{fiscal_info.certificate_key.current_path.to_s} -passin pass:#{cert_pass} > #{ruta_pem_file}'
      #puts "pem correcto"
      #Obtiene el numero de serie del certificado y lo convierte al numero.
      certificado =  %x'openssl x509 -serial -noout -inform DER -in #{doctor.fiscal_information.certificate_stamp.current_path.to_s}'
      arreglonumero = certificado.to_s.split('=')
      arreglosolonumero = arreglonumero[1].to_s.split(//)
      serialNumberCer = []
      (1..39).step(2) do |n|
        serialNumberCer.push(arreglosolonumero[n])
        puts arreglosolonumero[n]
      end
      noCertificado = serialNumberCer.join.to_s
      doctor.fiscal_information.update(certificate_number: noCertificado)
      certificado =  %x'openssl x509 -inform DER -in #{doctor.fiscal_information.certificate_stamp.current_path.to_s}'
      arrayc =  certificado.split("\n")
      arrayc.shift
      arrayc.pop
      certificadobase64 = arrayc.join.to_s
      doctor.fiscal_information.update(certificate_base64: certificadobase64)
      doctor.fiscal_information.certificate_password = cert_pass
      doctor.fiscal_information.update(certificate_password: @doctor.fiscal_information.encrypt_password)
    else
      create_pem_output = false
      #puts "pem no correcto"
    end
    #registra un nuevo rfc
    cliente = Clientes2.new
    cliente.register_in_finkok doctor.fiscal_information.rfc
    create_pem_output
  end
  def find_doctors
    str = params[:term]
    render :json => find_doctor(str)
  end
  # Actualiza los registros de tipo "Doctor" e "Información Fiscal" de la base de datos y redirecciona a la página que contiene la lista paginada.
  # Si la información fiscal fue ingresada correctamente genera un archivo con extensión ".pem" el cual se utilizará al momento de que el doctor emita 
  # facturas electrónicas.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Doctor" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Doctor".
  def update
    @text_button = "Guardar"
    respond_message = 'El doctor fue actualizado exitosamente'
    #respond_to do |format|
      begin
        Doctor.transaction do
          if params[:doctor][:searcher_visible] != nil
            @doctor.searcher_visible = 1
          else
            @doctor.searcher_visible = 0
          end
              if @doctor.update!(doctor_params)
            

            if !params[:picture64].nil? && params[:picture64]!=""
              save_picture_cam params[:picture64]
            end

            if !params[:crop_image_64].nil? && params[:crop_image_64]!=""
              save_picture_cam params[:crop_image_64]
            end

            if params[:change_files_fiscal].present?
              if !params[:doctor][:fiscal_information_attributes][:certificate_password].blank?
                # encriptar la nueva contraseña
                @doctor.fiscal_information.certificate_password = params[:doctor][:fiscal_information_attributes][:certificate_password]
                @doctor.fiscal_information.update!(certificate_password: @doctor.fiscal_information.encrypt_password)
              end
              respond_message = ""
              if !params[:doctor][:fiscal_information_attributes][:certificate_password].blank? &&
                  !params[:doctor][:fiscal_information_attributes][:certificate_stamp].blank? &&
                  !params[:doctor][:fiscal_information_attributes][:certificate_key].blank?
                #cert_exp_date = check_cert_date @doctor
                exp_date_req =  %x'openssl x509 -enddate -noout -inform DER -in #{@doctor.fiscal_information.certificate_stamp.current_path.to_s}'
                exp_date = Date.parse exp_date_req
                today_date = Date.today
                if exp_date > today_date
                  pem_op = create_pem_file @doctor
                  if(!pem_op)
                    respond_message += "Contraseña de documentos fiscales incorrecta, documentos no actualizados"
                    @doctor.fiscal_information.remove_certificate_stamp = true
                    @doctor.fiscal_information.remove_certificate_key = true
                    @doctor.fiscal_information.update!(certificate_password: "")
                    @doctor.errors.add(:base, :not_implemented, message: respond_message)
                    #raise ActiveRecord::RecordInvalid.new(@doctor)
                  end
                else
                  @doctor.fiscal_information.remove_certificate_stamp = true
                  @doctor.fiscal_information.remove_certificate_key = true
                  @doctor.fiscal_information.update!(certificate_password: "")
                  respond_message += "El certificado ha caducado. Fecha de caducidad #{exp_date.to_s}, documentos no actualizados"
                end
              end
            end
            if params[:from_doctor]=="1"
              redirect_to :back, notice: respond_message
            else
                redirect_to admin_persons_path, notice: respond_message
            end
          end
        end
      rescue ActiveRecord::RecordInvalid => invalid
        render :edit
        #render json: 'Doctor error', status: :unprocessable_entity
      end
    #end
  end

  # Elimina un registro de tipo "Doctor" junto con su dependencia "Información Fiscal" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Doctor".
  def destroy
    @doctor.destroy
    respond_to do |format|
      format.html { redirect_to admin_doctors_path, notice: 'El doctor fue eliminado exitosamente'}
      format.json { head :no_content}
    end
  end

  def print_certificate
    id = params[:license_id]
    @license = License.find(id)
    @version = "General"

    @license.features.each do |ft|
      if ft.key == 'gin'
        @version = 'Ginecología'
      end
    end

    respond_to do |format|
      format.pdf do
        render :pdf => "certificado_licencia",
               encoding: "UTF-8",
               :page_height => '11in', :page_width => '8.5in',
               :margin => { :top => 0, :bottom => 0, :left => 0 , :right => 0 }
      end

    end

  end

  def download_profile_image
    doctor = Doctor.find(params[:id])
    send_file doctor.picture,  :type => 'image/jpeg', :disposition => 'inline'
  end
  def check_new_user
    exist = false
    edit_value = params[:edit_value]
    user_name_tocheck = params[:user_name]
    mail = params[:mail]

    if edit_value != "0"
      doc = Doctor.find(edit_value)
      if doc.user.user_name != user_name_tocheck
        if User.exists?(user_name: user_name_tocheck)
          exist = true
        end
      end
      if doc.user.email != mail
        if User.exists?(email: mail)
          exist = true
        end
      end
    else
      if User.exists?(user_name: user_name_tocheck)
        exist = true
      end
      if User.exists?(email: mail)
        exist = true
      end
    end
    respond_to do |format|
      format.html { render json: {:result => exist}}
    end
  end
  private
  # Inicializa un objeto de tipo "Doctor" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Doctor".
  # @return [Doctor] Objeto de tipo "Doctor"
  def set_doctor
    @doctor = Doctor.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Doctor" sean: nombre, apellido, cédula profesional, 
  # curp, firma digital, costo de consulta, género, fecha de nacimiento, teléfono, celular, dirección, ubicación, ciudad.
  # Para generar sus credenciales de acceso se recibe: id de usuario (si ya existe en la base de datos), nombre de usuario, email, tipo de usuario.
  # Para generar los datos fiscales se reciben: id (si existe en base de datos), rfc, razón social, dirección fiscal, número exterior, archivo .cer, archivo .key, régimen, código postal, colonia, localidad, folio de facturación, serie de facturación, número en que inicia el foliaje, tasa de IVA.
  def doctor_params
    params.require(:doctor).permit(:time_zone_id, :identity_card,
                                   :curp, :digital_sign, :consult_cost,
                                   fiscal_information_attributes:[:id, :rfc, :bussiness_name, :fiscal_address, :ext_number , :city_id , :certificate_stamp ,:certificate_key, :tax_regime,:zip,:suburb,:locality,:folio,:serie,:int_number,:iva])
  end

end