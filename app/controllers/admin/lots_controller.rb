class Admin::LotsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]

  before_action :set_lot, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: LotsDatatable.new(view_context)}
    end
  end


  def new
    @lot= Lot.new
  end

  def edit_expiration_date
    lot = Lot.find(params[:lot_id])
    lot.update(expiration_date:params[:expiration_date])
    respond_to do |format|
      format.json { render :json => {:done=> true, :message => 'Se completó la operación'} }
    end
  end

  def create
    @lot = Lot.new(lot_params)
    respond_to do |format|
      if @lot.save
        format.html { redirect_to admin_lots_url, notice: 'Lote Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @lot.update(lot_params)
        format.html { redirect_to admin_lots_url, notice: 'Lote Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end




  private

  def set_lot
    @lot = Lot.find(params[:id])
  end


  def lot_params
    params.require(:lot).permit(:name,:expiration_date,:medicament_id)
  end

end