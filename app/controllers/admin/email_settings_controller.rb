class Admin::EmailSettingsController < ApplicationController
  before_action :set_email_setting, only: [:show, :update, :destroy]

  # Despliega información de un registro de tipo "Configuración de Correo" en formato json.
  #
  # @param params [hash] Hash con la llave "id" que representa el id del registro "Configuración de Correo" que debe ser desplegado.
  # @return [string] String en formato json con la información del registro de tipo "Configuración de Correo" que fue solicitado.
  def show
    respond_to do |format|
        format.json { render :json =>  @email_setting}
    end
  end

  # Genera un registro de tipo "Configuración de Correo" en la base de datos y despliega el registro recién creado en formato json.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Configuración de Correo" en la base de datos.
  # @return [string] String con formato json que contiene información del registro "Configuración de Correo" recién creado.
  def create
    @email_setting = EmailSetting.new(email_setting_params)
    respond_to do |format|
      if @email_setting.save
        format.json { render :json =>  @email_setting}
      else
      end
    end
  end

  # Actualiza un registro de tipo "Configuración de Correo" de la base de datos y despliega el registro recién creado en formato json.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Configuración de Correo" de la base de datos.
  # @return [string] String con formato json que contiene información del registro "Configuración de Correo" recién creado.
  def update
    respond_to do |format|
      if @email_setting.update(email_setting_params)
        format.json { render :json =>  @email_setting}
      end
    end
  end

  # Elimina un registro de tipo "Configuración de Correo" de la base de datos y despliega información en formato json.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [string] String en formato json con un mensaje con el resultado de la operación.
  def destroy
    @email_setting.destroy
    respond_to do |format|
      msg = {:status => "ok" , :message => "Se eliminó la configuración"}
      format.json { render :json => msg  }
    end
  end

  private
  # Inicializa un objeto de tipo "Configuración de Correo" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Configuración de Correo".
  # @return [EmailSetting] Objeto de tipo "Configuración de Correo"
  def set_email_setting
    @email_setting = EmailSetting.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Configuración de Correo" sean: host, usuario, contraseña, puerto, id del doctor.
  def email_setting_params
    params.require(:email_setting).permit(:host, :username, :password, :port, :doctor_id)
  end
end
