class Admin::FixedAssetItemsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_fixed_asset_item, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: FixedAssetItemsDatatable.new(view_context)}
    end
  end


  def new
    @fixed_asset_item= FixedAssetItem.new
  end



  def create
    @fixed_asset_item = FixedAssetItem.new(fixed_asset_item_params)
    respond_to do |format|
      if @fixed_asset_item.save
        if params[:fixed_asset_item][:grid] != nil
          if params[:fixed_asset_item][:state].to_i == 1
            @fixed_asset_item.update(state:1)
          else
            @fixed_asset_item.update(state:0)
          end
          if params[:fixed_asset_item][:schedule].to_i == 1
            @fixed_asset_item.update(schedule:1)
          else
            @fixed_asset_item.update(schedule:0)
          end
          msg = {:status => "success" , :message => ""}
          format.json { render :json => msg  }
        else
          if params[:state] != nil
            @fixed_asset_item.update(state:1)
          else
            @fixed_asset_item.update(state:0)
          end
          if params[:schedule] != nil
            @fixed_asset_item.update(schedule:1)
          else
            @fixed_asset_item.update(schedule:0)
          end
          format.html { redirect_to admin_fixed_asset_items_url, notice: 'elemnto activo fijo Creado Correctamente.' }
          end
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @fixed_asset_item.update(fixed_asset_item_params)

        if params[:fixed_asset_item][:grid] != nil
          if params[:fixed_asset_item][:state].to_i == 1
            @fixed_asset_item.update(state:1)
          else
            @fixed_asset_item.update(state:0)
          end
          if params[:fixed_asset_item][:schedule].to_i == 1
            @fixed_asset_item.update(schedule:1)
          else
            @fixed_asset_item.update(schedule:0)
          end
          msg = {:status => "success" , :message => ""}
          format.json { render :json => msg  }
        else
          if params[:state] != nil
            @fixed_asset_item.update(state:1)
          else
            @fixed_asset_item.update(state:0)
          end
          if params[:schedule] != nil
            @fixed_asset_item.update(schedule:1)
          else
            @fixed_asset_item.update(schedule:0)
          end
          format.html { redirect_to admin_fixed_asset_items_url, notice: 'elemento activo fijo Actualizado Correctamente.' }
        end

      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @fixed_asset_item.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'activo fijo Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_fixed_asset_item
    @fixed_asset_item = FixedAssetItem.find(params[:id])
  end


  def fixed_asset_item_params
    params.require(:fixed_asset_item).permit(:name,:description,:identifier,:fixed_asset_id)
  end

end