class Admin::NewsItemsController < ApplicationController

  before_action :set_news_item, only: [:show, :edit, :update, :destroy, :get_banner]

  def index
    respond_to do |format|
      format.html
      format.json { render json: NewsItemsDatatable.new(view_context)}
    end
  end

  def new
    @news_item = NewsItem.new
  end

  def create
    @news_item = NewsItem.new(news_params)
    respond_to do |format|
      if @news_item.save
        format.html { redirect_to admin_news_items_path, notice: 'La noticia fue agregada con exito'}
        format.json { render show, status: :ok, location: @news_item}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def edit

  end

  def update
    respond_to do |format|
      if @news_item.update(news_params)
        format.html { redirect_to admin_news_items_path, notice: 'La noticia fue actualizada con exito'}
        format.json { render show, status: :ok, location: @news_item}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def get_banner
    send_file @news_item.banner.path,  :type => 'image/jpeg', :disposition => 'inline'
  end

  private
  def set_news_item
    @news_item = NewsItem.find(params[:id])
  end

  def news_params
    params.require(:news_item).permit(:title,:summary,:content,:author_id,:status,:banner)
  end
end