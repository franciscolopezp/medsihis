class Admin::WarehousesController < ApplicationController
  load_and_authorize_resource :only => [:index, :manage_warehouse,:new,:destroy,:edit]
  before_action :set_warehouse, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: WarehousesDatatable.new(view_context)}
    end
  end


  def new
    @warehouse= Warehouse.new
  end



  def create
    @warehouse = Warehouse.new(warehouse_params)
    respond_to do |format|
      if @warehouse.save
        format.html { redirect_to admin_warehouses_url, notice: 'Almacen Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end

  def get_warehouses
    warehouses = []
    user = User.find(params[:user_id])
    user.warehouses.each do |warehouse|
      warehouses.push({
                       :name=>"warehouse_" + warehouse.id.to_s
                   })
    end
    respond_to do |format|
      format.html { render json: warehouses}
    end
  end

  def save_warehouses
    user = User.find(params[:user_id])
    user.warehouses.delete_all
    params[:warehouses].each do |idx,acc|
      warehouse = Warehouse.find(acc["warehouse_id"].to_i)
      user.warehouses << warehouse
    end
    respond_to do |format|
      format.html {render json: true}
    end
  end
  def manage_warehouse

  end

  def update
    respond_to do |format|
      if @warehouse.update(warehouse_params)
        format.html { redirect_to admin_warehouses_url, notice: 'Almacen Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy
    if @warehouse.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Almacen eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_warehouse
    @warehouse = Warehouse.find(params[:id])
  end


  def warehouse_params
    params.require(:warehouse).permit(:name)
  end

end