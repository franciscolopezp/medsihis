class Admin::UsersController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Usuario".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: UsersDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Usuario".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Usuario".
  def new
    @text_button = "Guardar"
    @user = User.new
  end

  # Consulta un registro de tipo "Usuario" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Usuario" para poder ser editado.
  def edit
    @text_button = "Guardar"
  end

  # Genera un registro de tipo "Usuario" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Usuario" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Usuario".
  def create
    @text_button = "Guardar"
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html  { redirect_to admin_users_path, notice: 'El usuario fue agregado exitosamente'}
        format.json { render :show, status: :created, location: @user  }
      else
        format.html { render :new}
        format.json {render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  # Actualiza un registro de tipo "Usuario" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Usuario" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Usuario".
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to admin_users_path, notice: 'Usuario actualizado con exito'}
        format.json { render show, status: :ok, location: @user}
      else
        format.html { render :edit}
        format.json { render json: 'User error', status: :unprocessable_entity}
      end
    end
  end

  # Elimina un registro de tipo "Usuario" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Usuario".
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_users_path, notice: 'Usuario eliminado satisfactoriamente'}
      format.json { head :no_content}
    end
  end

  private
  # Inicializa un objeto de tipo "Usuario" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Usuario".
  # @return [Medicament] Objeto de tipo "Usuario".
  def set_user
    @user = User.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Usuario" sean: nombre de usuario, correo, contraseña, tipo de usuario.
  def user_params
    params.require(:user).permit(:user_name, :email, :password, :role_id)
  end
  
end