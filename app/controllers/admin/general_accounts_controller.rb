class Admin::GeneralAccountsController < ApplicationController
  include Doctor::PatientsHelper
  before_action :set_general_account, only: [:show, :edit, :update, :destroy]
  before_action :load_banks, only: [:new,:edit]


  def index
    respond_to do |format|
      format.html
      format.json { render json: GeneralAccountsDatatable.new(view_context)}
    end
  end


  def new
    @text_button = "Guardar"
    @general_account = GeneralAccount.new
  end


  def edit
    @text_button = "Guardar"
  end


  def create
    @text_button = "Guardar"
    @general_account = GeneralAccount.new(general_account_params)

    respond_to do |format|
      if @general_account.save
        format.html { redirect_to admin_general_accounts_path, notice: 'La cuenta fue agregada con exito'}
        format.json { render show, status: :ok, location: @general_account}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end


  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @general_account.update(general_account_params)
        format.html { redirect_to admin_general_accounts_path, notice: 'Cuenta actualizada'}
        format.json { render show, status: :ok,location: @general_account}
      else
        format.html { render :edit}
        format.json { render json: 'error cuenta', status: :unprocessable_entity}
      end
    end
  end


  def destroy
    @general_account.destroy
    respond_to do |format|
      format.html { redirect_to admin_general_accounts_path, notice: 'Cuenta eliminada'}
      format.json { head :no_content}
    end
  end

  private

  def set_general_account
    @general_account = GeneralAccount.find(params[:id])
  end

  def load_banks
    @banks = Bank.where("is_local = 0")
    @local_banks = Bank.where("is_local = 1")
  end

  def general_account_params
    params.require(:general_account).permit(:account_number,:clabe,:balance,:details,:bank_id,:currency_id)
  end
end