class Admin::AssetActivitiesController < ApplicationController
  before_action :set_asset_activity, only: [:show, :edit, :update, :destroy]

  def index

  end

  def new
    @asset_activity = AssetActivity.new
  end


  def show
    respond_to do |format|
      format.html {}
      format.json {
        render json: {
            title: @asset_activity.title,
            details: @asset_activity.details,
            start_date: @asset_activity.start_date,
            end_date: @asset_activity.end_date,
            s_date: @asset_activity.start_date.strftime('%Y-%m-%d %H:%M'),
            e_date: @asset_activity.end_date.strftime('%Y-%m-%d %H:%M'),
            fixed_asset_activity_id: @asset_activity.fixed_asset_activity_id
        }
      }
    end
  end

  def create
    Time.zone = "Mexico City"
    @asset_activity = AssetActivity.new
    if available_schedule_asset_item || params[:force].to_s == "true"
      set_activity_data
      @asset_activity.save
      render json: {:done => true}
    else
      render json: {:done => false}
    end
  end

  def edit

  end

  def update
    set_activity_data
    @asset_activity.save
    render json: {:done => true}
  end

  def destroy
    if @asset_activity.destroy
      render json: {:done => true}
    else
      render json: {:done => false}
    end
  end

  def set_activity_data
    @asset_activity.fixed_asset_item_id = params[:asset_activity][:fixed_asset_item_id]
    @asset_activity.title = params[:asset_activity][:title]
    @asset_activity.details =params[:asset_activity][:details]
    @asset_activity.start_date = params[:asset_activity][:start_date]
    @asset_activity.end_date = params[:asset_activity][:end_date]
    @asset_activity.fixed_asset_activity = FixedAssetActivity.find(params[:asset_activity][:activity])
    @asset_activity.user = current_user
  end

  def available_schedule_asset_item
    start_date = DateTime.parse(params[:asset_activity][:start_date], '%d/%m/%Y %I:M %P')
    end_date = DateTime.parse(params[:asset_activity][:end_date], '%d/%m/%Y %I:M %P')

    activities = AssetActivity.where('fixed_asset_item_id = ?',params[:asset_activity][:fixed_asset_item_id])
    activities = activities.where('start_date <= ? AND end_date > ?',start_date,start_date)
    if activities.size > 0
      return false  #fecha de inicio entre fechas de inio y fin de alguna actividad
    end


    activities = AssetActivity.where('fixed_asset_item_id = ?',params[:asset_activity][:fixed_asset_item_id])
    activities = activities.where('start_date < ? AND end_date >= ?',end_date,end_date)
    if activities.size > 0
      return false  #fecha de fin entre fechas de inio y fin de alguna actividad
    end


    activities = AssetActivity.where('fixed_asset_item_id = ?',params[:asset_activity][:fixed_asset_item_id])
    activities = activities.where('start_date >= ? AND end_date <= ?',start_date,end_date)
    if activities.size > 0
      return false  #actividad engloba a alguna
    end
    return true
  end

  private
  def set_asset_activity
    @asset_activity = AssetActivity.find(params[:id])
  end

  def asset_activity_params
    params.require(:asset_activity).permit(:title,:details)
  end

end