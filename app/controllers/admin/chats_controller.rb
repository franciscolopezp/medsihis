class Admin::ChatsController < ApplicationController

  def index

  end

  def chat
    @name = params[:name]
    @channel = params[:channel]
    @user_type = params[:client_role] == 'patient' ? 'Paciente':'Doctor'
    @user_name = params[:client_name]
    @user_email = params[:client_email]
    render layout: false
  end

end