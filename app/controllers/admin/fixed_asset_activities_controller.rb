class Admin::FixedAssetActivitiesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_fixed_asset_activity, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: FixedAssetActivitiesDatatable.new(view_context)}
    end
  end

  def dashboard

  end


  def new
    @fixed_asset_activity= FixedAssetActivity.new
  end



  def create
    @fixed_asset_activity = FixedAssetActivity.new(fixed_asset_activity_params)
    respond_to do |format|
      if @fixed_asset_activity.save
        format.html { redirect_to admin_fixed_asset_activities_url, notice: 'actividad activo fijo Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end

  def delete_fixed_activity
    response = false
    if AssetActivity.find(params[:id]).delete
      response = true
    end
    render :json => {:done => response}, layout: false
  end
  def update
    respond_to do |format|
      if @fixed_asset_activity.update(fixed_asset_activity_params)
        format.html { redirect_to admin_fixed_asset_activities_url, notice: 'activo fijo Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end
  def load_asset_activities
    activities = []
    item = FixedAssetItem.find(params[:id])
    FixedAssetActivity.where("fixed_asset_id = #{item.fixed_asset.id}").each do |at|
      activities.push({
          :color => at.color,
          :id => at.id,
          :name => at.name
                      })
    end
    render :json => {:done => true,:activities => activities}, layout: false
  end
  def check_color
    response = false
    message = ""
    id = params[:id].to_i
    if id != 0
      existed_item = FixedAssetActivity.find(id)
      if existed_item.color != params[:color ]
        founded_color = FixedAssetActivity.find_by_color(params[:color])
        if founded_color != nil
          message = "El color seleccionado ya se encuentra en uso, favor de elegir otro."
        else
          response = true
        end
      else
        response = true
      end
    else
      founded_color = FixedAssetActivity.find_by_color(params[:color])
      if founded_color != nil
        message = "El color seleccionado ya se encuentra en uso, favor de elegir otro."
      else
        response = true
      end
    end

    render :json => {:done => response,msg:message}, layout: false
  end
  def destroy

    if @fixed_asset_activity.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'activo fijo Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_fixed_asset_activity
    @fixed_asset_activity = FixedAssetActivity.find(params[:id])
  end


  def fixed_asset_activity_params
    params.require(:fixed_asset_activity).permit(:name,:fixed_asset_id,:color)
  end

end