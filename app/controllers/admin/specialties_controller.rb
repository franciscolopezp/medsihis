class Admin::SpecialtiesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  include Doctor::PatientsHelper
  before_action :set_specialty, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Especialidad".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: SpecialtiesDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Especialidad".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Especialidad".
  def new
    @text_button = "Guardar"
    @specialty = Specialty.new
  end

  # Consulta un registro de tipo "Especialidad" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Especialidad" para poder ser editado.
  def edit
    @text_button = "Guardar"
  end

  # Genera un registro de tipo "Especialidad" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Especialidad" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Especialidad".
  def create
    @text_button = "Guardar"
    @specialty = Specialty.new(specialty_params)

    respond_to do |format|
      if @specialty.save
        format.html { redirect_to admin_specialties_path, notice: 'La especialidad fue agregada con exito'}
        format.json { render show, status: :ok, location: @specialty}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  # Actualiza un registro de tipo "Especialidad" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Especialidad" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Especialidad".
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @specialty.update(specialty_params)
        format.html { redirect_to admin_specialties_path, notice: 'Especialidad actualizada'}
        format.json { render show, status: :ok,location: @specialty}
      else
        format.html { render :edit}
        format.json { render json: 'error especialidad', status: :unprocessable_entity}
      end
    end
  end

  # Elimina un registro de tipo "Especialidad" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Especialidad".
  def destroy
    @specialty.destroy
    respond_to do |format|
      format.html { redirect_to admin_specialties_path, notice: 'Epecialidad eliminada'}
      format.json { head :no_content}
    end
  end

  private
  # Inicializa un objeto de tipo "Especialidad" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Especialidad".
  # @return [Specialty] Objeto de tipo "Especialidad".
  def set_specialty
    @specialty = Specialty.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Especialidad" sean:  nombre.
  def specialty_params
    params.require(:specialty).permit(:name)
  end
end