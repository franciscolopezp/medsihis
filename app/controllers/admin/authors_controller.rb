class Admin::AuthorsController < ApplicationController

  before_action :set_author, only: [:show, :edit, :update, :destroy, :get_picture]

  def index
    respond_to do |format|
      format.html
      format.json { render json: AuthorsDatatable.new(view_context)}
    end
  end

  def new
    @author = Author.new
  end

  def create
    @author = Author.new(news_params)
    respond_to do |format|
      if @author.save
        format.html { redirect_to admin_authors_path, notice: 'El autor fue agregada con exito'}
        format.json { render show, status: :ok, location: @author}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def edit

  end

  def update
    respond_to do |format|
      if @author.update(news_params)
        format.html { redirect_to admin_authors_path, notice: 'El autor fue actualizada con exito'}
        format.json { render show, status: :ok, location: @author}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def get_picture
    send_file @author.profile_image.path,  :type => 'image/jpeg', :disposition => 'inline'
  end

  private
  def set_author
    @author = Author.find(params[:id])
  end

  def news_params
    params.require(:author).permit(:profile_image,:name,:last_name,:twitter,:company_title,:job_title,:email)
  end
end