class Admin::LaboratoriesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  include ApplicationHelper
  before_action :set_laboratory, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Laboratorio".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: LaboratoriesDatatable.new(view_context,nil)}
    end
  end

  def show
    @groups = CaGroup.where('laboratory_id = ?',@laboratory.id).order('name ASC')
    @categories = ClinicalStudyType.where('laboratory_id = ?',@laboratory.id).order('name ASC')
  end

  # Permite la descarga del logo de un Laboratorio.
  #
  # @param params [hash] Hash que contiene la llave "id" que representa el id del Laboratorio.
  # @return [file] Recurso de tipo imagen que puede ser descargado o desplegado desde un navegador.
  def download_image
    lab = Laboratory.find(params[:id])
      send_file lab.logo.current_path, :type => 'image/jpeg', :disposition => 'inline'
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Laboratorio".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Laboratorio".
  def new
    @laboratory = Laboratory.new
    @laboratory.build_city
    @text_button = "Agregar"
  end

  # Consulta un registro de tipo "Laboratorio" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Laboratorio" para poder ser editado.
  def edit
    @text_button = "Actualizar"
  end

  # Genera un registro de tipo "Laboratorio" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Laboratorio" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Laboratorio".
  def create
    @laboratory = Laboratory.new(laboratory_params)
    set_city_model params, @laboratory
    @city_name = params[:city_name]
    @text_button = "Agregar"

    respond_to do |format|
      if @laboratory.save
        format.html { redirect_to admin_laboratories_path, notice: 'El laboratorio fue creado satisfactoriamente.' }
        format.json { render :show, status: :created, location: @laboratory }
      else
        @laboratory.build_city
        format.html { render :new }
        format.json { render json: @laboratory.errors, status: :unprocessable_entity }
      end
    end
  end

  # Actualiza un registro de tipo "Laboratorio" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Laboratorio" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Laboratorio".
  def update
    respond_to do |format|
      @city_name = params[:city_name]
      @text_button = "Actualizar"
      if @laboratory.update(laboratory_params)
        set_city_model params, @laboratory
        @laboratory.update(city_id: @laboratory.city_id)
        format.html { redirect_to admin_laboratories_path, notice: 'El laboratorio fue actualizado satisfactoriamente.' }
        format.json { render :show, status: :ok, location: @laboratory }
      else
        @laboratory.build_city
        format.html { render :edit }
        format.json { render json: @laboratory.errors, status: :unprocessable_entity }
      end
    end
  end

  # Elimina un registro de tipo "Laboratorio" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Laboratorio".
  def destroy
    @laboratory.destroy
    respond_to do |format|
      format.html { redirect_to admin_laboratories_url, notice: 'El laboratorio fue eliminado satisfactoriamente.' }
      format.json { head :no_content }
    end
  end

  private
  # Inicializa un objeto de tipo "Laboratorio" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Laboratorio".
  # @return [Laboratory] Objeto de tipo "Laboratorio"
  def set_laboratory
    @laboratory = Laboratory.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Laboratorio" sean: 
  # dirección, email, teléfono, nombre, id de la ciudad, observaciones, imagen del logo, nombre del contacto.
  def laboratory_params
    params.require(:laboratory).permit(:address, :email, :telephone, :name, :city_id, :observations, :logo, :contact, :clinical_order_type_id, :is_custom)
  end
end
