class Admin::VaccineBooksController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_vaccine_book, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Cartilla de Vacunación".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: VaccineBooksDatatable.new(view_context)}
    end
  end

  # Crea una relación entre un registro de tipo "Paciente" y uno de tipo "Cartilla de Vacunación".
  #
  # @param params[hash] Hash con las llaves "patient_id" y "vaccine_book_id" que representan el id del registro Paciente y el id del registro "Cartilla de Vacunación" respectivamente.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Cartilla de Vacunación".
  def add_book_patient
    patient = Patient.find(params[:patient_id]);
    vaccine_book = VaccineBook.find(params[:vaccine_book_id])
    patient.vaccine_books << vaccine_book
    respond_to do |format|
      format.json { render json: 'true'}
    end
  end


  # Despliega una página en formato html con un formulario para generar un registro de tipo "Cartilla de Vacunación".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Cartilla de Vacunación".
  def new
    @text_button = "Guardar"
    @vaccine_book = VaccineBook.new
  end

  # Consulta un registro de tipo "Cartilla de Vacunación" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Cartilla de Vacunación" para poder ser editado.
  def edit
    @text_button = "Guardar"
  end

  # Genera un registro de tipo "Cartilla de Vacunación" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Cartilla de Vacunación" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Cartilla de Vacunación".
  def create
    @text_button = "Guardar"
    @vaccine_book = VaccineBook.new(vaccine_book_params)

    respond_to do |format|
      if @vaccine_book.save
        format.html { redirect_to admin_vaccine_books_path, notice: 'vaccine book was successfully created.' }
        format.json { render :show, status: :created, location: @vaccine_book }
      else
        format.html { render :new }
        format.json { render json: @vaccine_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # Actualiza un registro de tipo "Cartilla de Vacunación" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Cartilla de Vacunación" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Cartilla de Vacunación".
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @vaccine_book.update(vaccine_book_params)
        format.html { redirect_to admin_vaccine_books_path, notice: 'vaccine book was successfully updated.' }
        format.json { render :show, status: :ok, location: @vaccine_book }
      else
        format.html { render :edit }
        format.json { render json: @vaccine_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # Elimina un registro de tipo "Cartilla de Vacunación" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Cartilla de Vacunación".
  def destroy
    @vaccine_book.destroy
    respond_to do |format|
      format.html { redirect_to admin_vaccine_books_url, notice: 'vaccine book was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Inicializa un objeto de tipo "Cartilla de Vacunación" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Cartilla de Vacunación".
  # @return [VaccineBook] Objeto de tipo "Cartilla de Vacunación".
  def set_vaccine_book
    @vaccine_book = VaccineBook.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Cartilla de Vacunación" sean:  nombre, género, edad inicial, edad final.
  def vaccine_book_params
    params.require(:vaccine_book).permit(:name, :gender, :start_age, :end_age)
  end
end
