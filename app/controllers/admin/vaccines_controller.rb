class Admin::VaccinesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  include Doctor::VaccinesHelper
  before_action :set_doctor_vaccine, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Vacuna".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: VaccinesDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Vacuna".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Vacuna".
  def new
    @text_button = "Guardar"
    @doctor_vaccine = Vaccine.new
    @doctor_vaccine.build_vaccine_book
  end

  # Consulta un registro de tipo "Vacuna" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Vacuna" para poder ser editado.
  def edit
    @text_button = "Guardar"
  end

  # Genera un registro de tipo "Vacuna" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Vacuna" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Vacuna".
  def create
    @text_button = "Guardar"
    @doctor_vaccine = Vaccine.new(doctor_vaccine_params)

    respond_to do |format|
      if @doctor_vaccine.save
        format.html { redirect_to admin_vaccines_path, notice: 'Vaccine was successfully created.' }
        format.json { render :show, status: :created, location: @doctor_vaccine }
      else
        format.html { render :new }
        format.json { render json: @doctor_vaccine.errors, status: :unprocessable_entity }
      end
    end
  end

  # Actualiza un registro de tipo "Vacuna" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Vacuna" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Vacuna".
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @doctor_vaccine.update(doctor_vaccine_params)
        format.html { redirect_to admin_vaccines_path, notice: 'Vaccine was successfully updated.' }
        format.json { render :show, status: :ok, location: @doctor_vaccine }
      else
        format.html { render :edit }
        format.json { render json: @doctor_vaccine.errors, status: :unprocessable_entity }
      end
    end
  end

  # Elimina un registro de tipo "Vacuna" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Vacuna".
  def destroy
    @doctor_vaccine.destroy
    respond_to do |format|
      format.html { redirect_to admin_vaccines_url, notice: 'Vaccine was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Inicializa un objeto de tipo "Vacuna" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Vacuna".
  # @return [Vaccine] Objeto de tipo "Vacuna".
  def set_doctor_vaccine
    @doctor_vaccine = Vaccine.find(params[:id])
  end

    # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Vacuna" sean:  nombre, enfermedad que previene, cartilla a la que pertenece.
  def doctor_vaccine_params
    params.require(:vaccine).permit(:name, :prevent_disease,:vaccine_book_id)
  end
end
