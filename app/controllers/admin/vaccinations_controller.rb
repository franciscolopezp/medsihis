class Admin::VaccinationsController < ApplicationController

  before_action :set_vaccination, only: [:show, :edit, :update, :destroy]

  # Genera o actualiza un registro de tipo "Aplicación de Vacuna" en la base de datos y despleiga un mensaje en formato json.
  #
  # @param params [hash] Hash con los datos necesarios para generar o actualizar un registro de tipo "Aplicación de Vacuna" en la base de datos.
  # @return [string] String en formato json que contiene información del estado de la operación.
  def add_vaccinate_date
    vaccination  = Vaccination.find_by(patient_id: params[:patient_id],application_age_id:params[:application_age_id])
    if vaccination !=nil
      vaccination.update(vaccinate_date: params[:vaccinate_date])
      respond_to do |format|
        format.json { render :json => {:done=> true, :message => 'Se completó la operación'} }
      end
    else
      new_vaccination = Vaccination.new(vaccinate_date: params[:vaccinate_date], patient_id: params[:patient_id], application_age_id: params[:application_age_id])
      if new_vaccination.save
        respond_to do |format|
          format.json { render :json => {:done=> true, :message => 'Se completó la operación'} }
        end
      else
        respond_to do |format|
          format.json { render :json => {:done=> false, :message => 'No se pudo guardar.'}}
        end
      end
    end
  end

end