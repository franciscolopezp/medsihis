class Admin::ConsultationFieldsController < ApplicationController
  before_action :set_consultation_field, only: [:update, :show]
  load_and_authorize_resource :only => [:index]

  def index
    @clinic_info = ClinicInfo.find(1)
    @html_content = Setting.find_by_code(Const::SETTING_TEMPLATE_CONSULTATION).data
  end

  def show
    render :json => @consultation_field
  end

  def create
    field = ConsultationField.new(consultation_field_params)
    field.is_custom = true
    field.active = false
    render json: {:done => field.save}
  end

  def update
    updated = @consultation_field.update(consultation_field_params)
    render json: {:done => updated}
  end

  def save_fields_config
    if params[:active].present?
      params[:active].each do |idx, a|
        ConsultationField.update(a[:id], active:true, field_order: a[:order])
      end
    end
    if params[:no_active].present?
      params[:no_active].each do |idx, a|
        ConsultationField.update(a[:id], active:false, field_order: nil)
      end
    end
    render :json => {:done => true}
  end

  private
  def consultation_field_params
    params.require(:consultation_field).permit(:name, :code, :field_type, :field_class)
  end

  def set_consultation_field
    @consultation_field = ConsultationField.find(params[:id])
  end
end