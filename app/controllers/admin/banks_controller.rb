class Admin::BanksController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_bank, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Banco".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: BanksDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Banco".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Banco".
  def new
    @bank = Bank.new
  end


  # Genera un registro de tipo "Banco" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Banco" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Banco".
  def create
    @bank = Bank.new(bank_params)
    respond_to do |format|
      if @bank.save
        format.html { redirect_to admin_banks_url, notice: 'Banco Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  # Consulta un registro de tipo "Banco" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Banco", para poder ser editado.
  def edit

  end

  # Actualiza un registro de tipo "Banco" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Banco" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Banco".
  def update
    respond_to do |format|
      if @bank.update(bank_params)
        format.html { redirect_to admin_banks_url, notice: 'Banco Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  # Elimina un registro de tipo "Banco" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Banco".
  def destroy

    if @bank.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Banco Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private
  # Inicializa un objeto de tipo "Banco" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Banco".
  # @return [Bank] Objeto de tipo "Banco"
  def set_bank
    @bank = Bank.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Banco" sean: nombre, es local.
  def bank_params
    params.require(:bank).permit(:name,:is_local)
  end

end