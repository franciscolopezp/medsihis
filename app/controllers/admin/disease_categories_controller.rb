class Admin::DiseaseCategoriesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_disease_category, only: [:show, :edit, :update, :destroy]

  # Retorna una lista paginada de registros de tipo "Categoría de Enfermedad".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string.view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: DiseaseCategoriesDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Categoría de Enfermedad".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Categoría de Enfermedad".
  def new
    @disease_category = DiseaseCategory.new
    @textd_button = "Guardar"
  end

  # Consulta un registro de tipo "Categoría de Enfermedad" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Categoría de Enfermedad" para poder ser editado.
  def edit
    @textd_button = "Guardar"
  end

  # Elimina un registro de tipo "Enfermedad" en la base de datos y despliega un mensaje en formato json.
  #
  # @param params [hash] Hash con la llave "disease_id" que representa el id del registro de tipo "Enfermedad" a eliminar de la base de datos.
  # @return [string] String en formato json que contiene un mensaje con el resultado de la operación.
  def diseaseDelete
    Disease.find(params[:disease_id]).destroy
    respond_to do |format|
      msg = {:status => "ok" , :message => "Se elimino la enfermedad"}
      format.json { render :json => msg  }
      end
  end

  # Genera un registro de tipo "Categoría de Enfermedad" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Categoría de Enfermedad" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Categoría de Enfermedad".
  def create
    @textd_button = "Guardar"
    @disease_category = DiseaseCategory.new(disease_category_params)

    respond_to do |format|
      if @disease_category.save
        format.html { redirect_to admin_disease_categories_path, notice: 'La Categoría de enfermedad fue agregada con exito'}
        format.json { render show, status: :ok, location: @disease_category}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  # Actualiza un registro de tipo "Categoría de Enfermedad" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Categoría de Enfermedad" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Categoría de Enfermedad".
  def update
    @textd_button = "Guardar"
    respond_to do |format|
      if @disease_category.update(disease_category_params)
        format.html { redirect_to admin_disease_categories_path, notice: 'Categoría enfermedad actualizada'}
        format.json { render show, status: :ok,location: @disease_category}
      else
        format.html { render :edit}
        format.json { render json: 'error Categoría enfermedad', status: :unprocessable_entity}
      end
    end
  end

  # Elimina un registro de tipo "Categoría de Enfermedad" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Categoría de Enfermedad".
  def destroy
    @disease_category.destroy
    respond_to do |format|
      format.html { redirect_to admin_disease_categories_path, notice: 'Categoría enfermedad eliminada'}
      format.json { head :no_content}
    end
  end

  private
  # Inicializa un objeto de tipo "Categoría de Enfermedad" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Categoría de Enfermedad".
  # @return [DiseaseCategory] Objeto de tipo "Categoría de Enfermedad"
  def set_disease_category
    @disease_category = DiseaseCategory.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Categoría de Enfermedad" sean: nombre y código.
  def disease_category_params
    params.require(:disease_category).permit(:code,:name)
  end
end