class Admin::MedicalAnnotationTypesController < ApplicationController
  include Cli::MedicalAnnotationHelper
  include DateHelper
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_medical_annotation_type, only: [:show, :edit, :update, :destroy, :add_linked_field]

  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicalAnnotationTypesDatatable.new(view_context)}
    end
  end

  def new
    @medical_annotation_type = MedicalAnnotationType.new
    @linkable_fields = Const::linkable_annotation_fields
  end


  def create
    @medical_annotation_type = MedicalAnnotationType.new(medical_annotation_type_params)
    if @medical_annotation_type.save
      save_fields
      update_template
      render json: {:done => true}
    else
      render json: {:done => false}
    end
  end

  def edit
    @linkable_fields = Const::linkable_annotation_fields
    @medical_annotation_image = MedicalAnnotationImage.new
  end

  def update
      if @medical_annotation_type.update(medical_annotation_type_params)
      save_fields
      update_template
      render json: {:done => true}
    else
      render json: {:done => false}
    end
  end

  def destroy
    if @medical_annotation_type.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Banco Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end
  end

  def save_fields
    if params[:fields].present?
      fields = JSON.parse(params[:fields])
      fields.each do |field|
        config = JSON.generate(field)
        puts config.class
        puts config
        f = MedicalAnnotationField.create(medical_annotation_type_id:@medical_annotation_type.id, config:config)
      end
    end
  end

  def delete_field
    field = MedicalAnnotationField.find(params[:id])
    field.destroy
    render json: {:done => true}
  end


  def add_linked_field
    if params[:linked_fields].present?
      fields = MedicalAnnotationLinkedField.where('id IN (?)', params[:linked_fields])
      @medical_annotation_type.update(medical_annotation_linked_fields: fields)
    end
    render json: {:done => true}
  end

  private
  def update_template
    if params[:medical_annotation_type][:template_file].present?
      content = @medical_annotation_type.template_file.read
      @medical_annotation_type.update(template: content)
    end
  end

  def set_medical_annotation_type
    @medical_annotation_type = MedicalAnnotationType.find(params[:id])
  end

  def medical_annotation_type_params
    params.require(:medical_annotation_type).permit(:name, :template, :template_file, :require_upload, :add_valoration)
  end

end