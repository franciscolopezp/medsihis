class Admin::MedicamentsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:active_substance]
  include Doctor::MedicalExpedientsHelper
  before_action :set_medicament, only: [:show, :edit, :update, :destroy,:active_substance]

  # Retorna una lista paginada de registros de tipo "Medicamento".
  #
  # @param params [hash] Hash con la llave "searchPhrase" que se usará para filtrar los registros.
  # @return [string,view] String con formato json que contiene el número de registros y los records paginados o página en formato html en donde se desplegarán los registros.
  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicamentsDatatable.new(view_context)}
    end
  end

  # Despliega una página en formato html con un formulario para generar un registro de tipo "Medicamento".
  #
  # @param nil[nil] No se reciben parámetros.
  # @return [view] Página en formato html que contiene un formulario para dar de alta un registro de tipo "Medicamento".
  def new
    @text_button = "Guardar"
    @medicament = Medicament.new
  end

  # Consulta un registro de tipo "Medicamento" y despliega la información en un formulario para ser editado.
  #
  # @param id [string] String que representa el id del registro a editar.
  # @return [view] Página en formato html que contiene un formulario con los datos de un registro de tipo "Medicamento" para poder ser editado.
  def edit
    @text_button = "Guardar"
  end

  # Genera un registro de tipo "Medicamento" en la base de datos y despliega un mensaje en formato json.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Medicamento" en la base de datos.
  # @return [string] String que contiene los datos de un registro de tipo "Medicamento".
  def medicament_json
    new_medicament = Medicament.new(comercial_name: params[:comercial_name], presentation: params[:presentation], active_substance: params[:active_substance],contraindication: params[:contraindication],brand: params[:brand], doctor_id:session[:doctor_id])
    if new_medicament.save
      respond_to do |format|
        format.json { render json: new_medicament}
      end
    else
      puts new_medicament.errors.full_messages
    end
  end
  def find_medicament
    to_search = params[:term]
    render :json => search_medicament(to_search)
  end
  # Genera un registro de tipo "Medicamento" en la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para generar un registro de tipo "Medicamento" en la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Medicamento".
  def create
    @text_button = "Guardar"
    @medicament = Medicament.new(medicament_params)

    respond_to do |format|
      if @medicament.save
        format.html  { redirect_to active_substance_admin_medicaments_path(@medicament), notice: 'El medicamento fue agregado exitosamente'}
        format.json { render :show, status: :created, location: @medicament  }
      else
        format.html { render :new}
        format.json {render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  # Actualiza un registro de tipo "Medicamento" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param params [hash] Hash con los datos necesarios para actualizar un registro de tipo "Medicamento" de la base de datos.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Medicamento".
  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @medicament.update(medicament_params)
        if params[:active_substance] != nil
          active_substance = ActiveSubstance.find(params[:active_substance])
          @medicament.active_substances.delete_all
          active_substance.medicaments << @medicament
        end
        format.html { redirect_to admin_medicaments_path, notice: 'Medicamento actualizado con exito'}
        format.json { render show, status: :ok, location: @medicament}
      else
        format.html { render :edit}
        format.json { render json: 'Medicamento error', status: :unprocessable_entity}
      end
    end
  end

  # Elimina un registro de tipo "Medicamento" de la base de datos y redirecciona a la página que contiene la lista paginada.
  #
  # @param id [string] String que representa el id del registro a eliminar.
  # @return [redirect] Redirecciona a la página que contiene la lista paginada de los registros de tipo "Medicamento".
  def destroy
    @medicament.destroy
    respond_to do |format|
      format.html { redirect_to admin_medicaments_path, notice: 'Medicamento eliminado satisfactoriamente'}
      format.json { head :no_content}
    end
  end
  def active_substance

  end
  def delete_substance
    medicament = Medicament.find(params[:medicament_id])
    active_substance = ActiveSubstance.find(params[:active_substance_id])
    medicament.active_substances.delete(active_substance)
    respond_to do |format|
      format.html {render json: true}
    end
  end
  def add_substance
    response = true
    medicament = Medicament.find(params[:medicament_id])
    active_substance = ActiveSubstance.find(params[:active_substance_id])
    medicament.active_substances.each do |subs|
      if subs.id == active_substance.id
        response = false
      end
    end
    if response
      medicament.active_substances << active_substance
    end
    respond_to do |format|
      format.html {render json: response}
    end
  end
  private
  # Inicializa un objeto de tipo "Medicamento" con el parámetro "id" que se recibe en la petición.
  #
  # @param id[string] String que representa el id del registro de tipo "Medicamento".
  # @return [Medicament] Objeto de tipo "Medicamento".
  def set_medicament
    @medicament = Medicament.find(params[:id])
  end

  # Restringe a que los datos a recibir de una petición al momento de crear o actualizar un registro de tipo "Medicamento" sean: 
  # nombre comercial,presentación,sustancia activa,contraindicación,marca, id del doctor (cuando un doctor lo da de alta).
  def medicament_params
    params.require(:medicament).permit(:tags,:code,:is_consigment,:medicament_category_id, :comercial_name, :presentation, :contraindication, :medicament_laboratory_id,:cost,:sale_cost)
  end

end