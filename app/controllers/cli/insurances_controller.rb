class Cli::InsurancesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:active_insurence,:prices]
  before_action :set_insurance, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: InsurancesDatatable.new(view_context)}
    end
  end


  def new
    @insurance= Insurance.new
  end



  def create
    @insurance = Insurance.new(insurance_params)
    respond_to do |format|
      if @insurance.save
        @insurance.update(is_active:1)
        format.html { redirect_to cli_insurances_url, notice: 'Aseguradora Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @insurance.update(insurance_params)
        format.html { redirect_to cli_insurances_url, notice: 'Aseguradora Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end
  def save_price
    response = false;
    price = params[:price].to_f
    exist_record = InsurencePrice.where("insurance_id = #{params[:insurance_id]} and hospital_service_id = #{params[:service_id]}").first
    if exist_record != nil
      exist_record.update(price:price)
      response = true;
    else
      new_prince = InsurencePrice.new
      new_prince.insurance = Insurance.find(params[:insurance_id])
      new_prince.hospital_service = HospitalService.find(params[:service_id])
      new_prince.price = price
      if new_prince.save
        response = true
      end
    end
    render :json => {:done => response}, layout: false
  end
  def get_services
    services = []
    insurences = []
    if params[:service_id] != ""
      HospitalService.where("id = #{params[:service_id]}").each do |service|
        services.push({
                          :id => service.id,
                          :name => service.name
                      })
      end
    else
      HospitalService.all.each do |service|
        services.push({
                          :id => service.id,
                          :name => service.name
                      })
      end
    end

    Insurance.where("is_active = 1 AND id != 1").each do |insurence|
      insurences.push({
          :id => insurence.id,
          :name => insurence.name
                      })
    end
    render :json => {:done => true, :services =>services,:insurences => insurences}, layout: false
  end
  def active_insurence
    insurence = Insurance.find(params[:id])
    insurence.update(is_active:1)
    render :json => {:done => true}, layout: false
  end
  def set_prices
    prices = []
    InsurencePrice.all.each do |price|
        prices.push({
            :field => "asurance_price_"+price.insurance.id.to_s+"_"+price.hospital_service.id.to_s,
            :price => price.price
                    })
    end
    render :json => {:done => true, :prices =>prices}, layout: false
  end
  def prices
    @insurences_amount = Insurance.all.count
  end
  def destroy

    if  @insurance.update(is_active:0)
      respond_to do |format|
        msg = {:done => true , :message => 'Aseguradora Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_insurance
    @insurance = Insurance.find(params[:id])
  end


  def insurance_params
    params.require(:insurance).permit(:name,:telephone,:address,:contact)
  end

end