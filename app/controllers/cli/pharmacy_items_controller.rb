class Cli::PharmacyItemsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_pharmacy_item, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: PharmacyItemsDatatable.new(view_context)}
    end
  end


  def new
    @pharmacy_item= PharmacyItem.new
  end



  def create
    @pharmacy_item = PharmacyItem.new(pharmacy_item_params)
    respond_to do |format|
      if @pharmacy_item.save
        format.html { redirect_to cli_pharmacy_items_url, notice: 'Articulo Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @pharmacy_item.update(pharmacy_item_params)
        format.html { redirect_to cli_pharmacy_items_url, notice: 'Articulo Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end

  def find_pharmacy_item
    to_search = params[:term]
    items = []
    PharmacyItem.where("code like '%#{to_search}%' or description like '%#{to_search}%' or tags like '%#{to_search}%'").limit(20).each do |c|
      items.push({id: c.id, value: c.code+' ('+c.description+')'})
    end
    items
    render :json => items.to_a
  end

  def destroy

    if @pharmacy_item.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Articulo Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_pharmacy_item
    @pharmacy_item = PharmacyItem.find(params[:id])
  end


  def pharmacy_item_params
    params.require(:pharmacy_item).permit(:tags,:is_consigment,:code,:cost,:description,:measure_unit_item_id,:medicament_category_id,:sale_cost)
  end

end