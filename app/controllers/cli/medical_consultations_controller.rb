class Cli::MedicalConsultationsController < ApplicationController
  #se llama así solo por conveniencia del Can Can este va a ser el contenedor de las gráficas
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  include Cli::IndicatorsHelper

  def general

  end


  def load_data
    start_date = Date.parse params[:start_date]
    end_date = (Date.parse params[:end_date]) + 1.day
    render json: IndicatorReport.new(view_context, start_date, end_date).to_json
  end

end