class Cli::WaitingRoomItemsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_waiting_room_item, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render :json => get_waiting_room_items }
    end
  end

  def load_menu_item
    @waiting_room_item_id = params[:item_id]
    @activities = WaitingRoomStatus.find(params[:status_id]).waiting_room_activities
    @patient_id = params[:patient_id]
    @patient = Patient.find(@patient_id)
    render layout: false
  end

  def load_previous_triage
    waiting_room_item = WaitingRoomItem.find(params[:waiting_room_item_id])
    triage_id = ""
    valoration = waiting_room_item.patient_valorations.first
    if valoration.present?
      triage_data = valoration.patient_valoration_fields.joins(:valoration_field).where(:valoration_fields => {:collection_type => Const::VALORATION_COLLECTION_TYPE_TRIAGE} ).first
      if triage_data.present?
        triage_id = triage_data.data
      end
    end
    render json: {:triage_id => triage_id}
  end

  def go_to
    patient_id = params[:patient_id]
    go_to = params[:go_to]
    patient = Patient.find(patient_id)

    case go_to
      when Const::WAITING_ROOM_GO_TO_MEDICAL_EXPEDIENT
        redirect_to doctor_expedient_path(patient.medical_expedient.id)
      when Const::WAITING_ROOM_GO_TO_ADMISSION_CLINICAL
        admission = patient.hospital_admissions.last
        if admission.present?
          redirect_to cli_hospitalization_path(admission.id)
        else
          render :text => "No se encuentra la última admisión generada. Por favor contacte al administrador del sistema.", :layout => false
        end
      when Const::WAITING_ROOM_GO_TO_ADMISSION_ACCOUNT
        admission = patient.hospital_admissions.last
        if admission.present?
          redirect_to cli_hospital_admission_path(admission.id)
        else
          render :text => "No se encuentra la última admisión generada. Por favor contacte al administrador del sistema.", :layout => false
        end
    end

  end

  def create_admission
    waiting_room_item = WaitingRoomItem.find(params[:waiting_room_item_id])
    valoration = waiting_room_item.patient_valorations.first
    valorations = []
    if valoration.present?
      valorations = [valoration]
    end
    admission = HospitalAdmission.new(
       total:0, payment:0 , debt:0, is_closed: false,
       admission_date: Time.now,
        patient_id: params[:patient_id],
        doctor_id:params[:doctor_id],
        user_id: current_user.id,
        triage_id: params[:triage_id],
        attention_type_id: params[:attention_type_id],
       patient_valorations: valorations
    )
    render json: {:done => admission.save}
  end

  def save_valoration
    waiting_room_item = WaitingRoomItem.find(params[:waiting_room_item_id])
    patient_valoration = PatientValoration.create(user_id: current_user.id, valoration_type_id: params[:valoration_type_id], patient_id: params[:patient_id])

    params[:fields].each do |idx, f|
      PatientValorationField.create(patient_valoration_id: patient_valoration.id, valoration_field_id: f['id'], data:f['val'])
    end

    waiting_room_item.update(patient_valorations: [patient_valoration])

    render json: {:done => true}
  end

  def create
    wroom_status = WaitingRoomStatus.find_by_s_order(0)
    @waiting_room_item = WaitingRoomItem.new
    @waiting_room_item.patient_id = params[:patient_id]
    @waiting_room_item.waiting_room_status_id = wroom_status.id
    @waiting_room_item.time = 0
    @waiting_room_item.i_order = WaitingRoomItem.where(:waiting_room_status_id => wroom_status.id).count


    @waiting_room_item.save

    wroom_item_status = WaitingRoomItemStatus.new(
        waiting_room_item_id: @waiting_room_item.id,
        waiting_room_status_id: wroom_status.id,
        time:0
    )
    wroom_item_status.save

    render json: {:done => true}
  end

  def update
    # cambio de columna
    if params[:new_status_id].present?
      now = Time.now
      last_date = @waiting_room_item.created_at
      minutes = ((now - last_date) / 1.minutes).to_i
      wroom_item_status = WaitingRoomItemStatus.new(
          waiting_room_item_id: params[:id],
          waiting_room_status_id: params[:new_status_id],
          time:minutes
      )
      @waiting_room_item.update(time: minutes, waiting_room_status_id: params[:new_status_id])
      resp = wroom_item_status.save
    end

    if params[:from_ids].present?
      order = 0
      params[:from_ids].each do |from_id|
        WaitingRoomItem.update(from_id, i_order: order)
        order += 1
      end
    end

    if params[:to_ids].present?
      order = 0
      params[:to_ids].each do |to_id|
        WaitingRoomItem.update(to_id, i_order: order)
        order += 1
      end
    end

    render json: {:done => resp}
  end

  private
  def get_waiting_room_items
    items = WaitingRoomItem.joins(:waiting_room_status)
                .joins(:patient)
                .includes(:patient, :waiting_room_status)


    if params[:date].present?
      date = Date.parse params[:date]
      date2 = date + 1.day
      items = items.where('waiting_room_items.created_at >= ? and waiting_room_items.created_at < ?', date, date2)
    end

    items = items.order('waiting_room_items.i_order ASC')


    resp = []
    items.each do |i|
      color = ""
      valoration = i.patient_valorations.first
      if valoration.present?
        triage_data = valoration.patient_valoration_fields.joins(:valoration_field).where(:valoration_fields => {:collection_type => Const::VALORATION_COLLECTION_TYPE_TRIAGE} ).first
        if triage_data.present?
          triage_record = Triage.find(triage_data.data)
          color = "#"+triage_record.color
        end
      end
      resp.push({
          :id => i.id,
          :patient => i.patient.full_name,
          :patient_id => i.patient_id,
          :date => i.created_at,
          :date_str => date_hour(i.created_at),
          :status_id => i.waiting_room_status.id,
          :status_order => i.waiting_room_status.s_order,
          :waiting_time => i.waiting_time,
          :triage_color => color
                })
    end
    resp
  end

  def set_waiting_room_item
    @waiting_room_item = WaitingRoomItem.find(params[:id])
  end
end