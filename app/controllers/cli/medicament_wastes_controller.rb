class Cli::MedicamentWastesController < ApplicationController
  include Cli::MedicamentHelper
  before_action :set_medicament_waste, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicamentWastesDatatable.new(view_context)}
    end
  end

  def pharmacy_wastes
    @pharmacy_waste= PharmacyItemWaste.new
  end

  def get_waste_type
    waste_type = WasteType.find(params[:waste_id])
    render :json => {:done => true,:is_entry => waste_type.is_entry}, layout: false
  end
  def check_quantity_pharmacy
    response = true
    message = ""
    current_quantity = 0
    search_inventory = PharmacyItemInventory.where("warehouse_id = #{params[:warehouse]} and pharmacy_item_id = #{params[:item]}")
    if search_inventory.count > 0
      founded_inventory = search_inventory.first
      updated_quantity = founded_inventory.quantity - params[:quantity].to_i
      if updated_quantity < 0
        response = false
        current_quantity = founded_inventory.quantity
        message = "El almacen seleccionado no cuenta con la cantidad solicitada, solo se tiene: " + current_quantity.to_s + " de cantidad"
      end
    else
      response = false
      message = "El artículo no existe en el almacen seleccionado"
    end
    render :json => {:done => response,:current_cuantity => current_quantity,msg:message}, layout: false
  end
  def new
    @medicament_waste= MedicamentWaste.new
  end




  def check_quantity
    message = ""
    response = true
    current_quantity = 0
    search_inventory = MedicamentInventory.where("warehouse_id = #{params[:warehouse]} and lot_id = #{params[:lot]}")
    if search_inventory.count > 0
      founded_inventory = search_inventory.first
      updated_quantity = founded_inventory.quantity - params[:quantity].to_i
      if updated_quantity < 0
        response = false
        current_quantity = founded_inventory.quantity
        message = "El almacen seleccionado no cuenta con la cantidad solicitada, solo se tiene: " + current_quantity.to_s + " de cantidad"
      end
    else
      response = false
      message = "El artículo no existe en el almacen seleccionado"
    end
    render :json => {:done => response,:current_cuantity => current_quantity, :msg => message}, layout: false
  end
  private

  def set_medicament_waste
    @medicament_waste = MedicamentWaste.find(params[:id])
  end

  def pharmacy_waste_params
    params.require(:pharmacy_item_waste).permit(:cause,:warehouse_id,:pharmacy_item_id,:quantity,:waste_type_id)
  end
  def medicament_waste_params
    params.require(:medicament_waste).permit(:cause,:warehouse_id,:lot_id,:quantity,:waste_type_id)
  end

end