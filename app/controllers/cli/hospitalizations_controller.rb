class Cli::HospitalizationsController < ApplicationController

  before_action :set_hospitalization, only: [:show, :edit, :update, :destroy]

  def show
    @annotation_file = MedicalAnnotationFile.new
  end

  def save_medical_annotation
    hospitalization = HospitalAdmission.find(params[:hospitalization_id])
    annotation = MedicalAnnotation.create(
        user:current_user,
        medical_annotation_type_id:params[:annotation_type],
        patient_id: hospitalization.patient_id
    )
    params[:fields].each do |idx, field|
      AnnotationField.create(
          medical_annotation_id:annotation.id,
          medical_annotation_field_id:field['id'],
          data: field['value']
      )
    end
    annotation.update(hospital_admissions:[hospitalization])

    if params[:linked_fields].present?
      params[:linked_fields].each do |idx, lf|
        AnnotationLinkedField.create(
            medical_annotation_id: annotation.id,
            medical_annotation_linked_field_id: lf['id'],
            data:lf['value'])
      end
    end

    if params[:valorations].present?
      params[:valorations].each do |index,v|
        patient_valoration = PatientValoration.create(
            valoration_type_id:v[:valoration_type_id],
            patient_id: hospitalization.patient_id,
            user:current_user
        )
        v[:fields].each do |idx, field|
          PatientValorationField.create(
              patient_valoration_id:patient_valoration.id,
              valoration_field_id:field['id'],
              data: field['value']
          )
        end
        patient_valoration.update(hospital_admissions:[hospitalization],medical_annotations:[annotation])
      end
    end

    render json: {:done => true}
  end

  def save_medical_valoration
    hospitalization = HospitalAdmission.find(params[:hospitalization_id])
    patient_valoration = PatientValoration.create(
        valoration_type_id:params[:valoration_type],
        patient_id: hospitalization.patient_id,
        user:current_user
    )
    params[:fields].each do |idx, field|
      PatientValorationField.create(
          patient_valoration_id:patient_valoration.id,
          valoration_field_id:field['id'],
          data: field['value']
      )
    end
    patient_valoration.update(hospital_admissions:[hospitalization])
    render json: {:done => true}
  end

  def load_annotation_fields
    @annotation_type = MedicalAnnotationType.find(params[:annotation_type_id])
    render layout: false
  end

  private
  def set_hospitalization
    @hospitalization = HospitalAdmission.find(params[:id])
  end
end
