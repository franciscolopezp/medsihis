class Cli::WasteTypesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_waste_type, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: WasteTypesDatatable.new(view_context)}
    end
  end


  def new
    @waste_type= WasteType.new
  end



  def create
    @waste_type = WasteType.new(waste_type_params)
    respond_to do |format|
      if @waste_type.save
        format.html { redirect_to cli_waste_types_url, notice: 'Tipo de salida Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @waste_type.update(waste_type_params)
        format.html { redirect_to cli_waste_types_url, notice: 'Tipo de salida Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @waste_type.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Tipo de salida Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_waste_type
    @waste_type = WasteType.find(params[:id])
  end


  def waste_type_params
    params.require(:waste_type).permit(:name,:is_entry)
  end

end