class Cli::ItemTransfersController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  include Cli::MedicamentHelper
  before_action :set_item_transfer, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: ItemTransfersDatatable.new(view_context)}
    end
  end

  def item_transfer_accepted
    response = false
    item_transfer = ItemTransfer.find(params[:transfer_id])
    item_transfer.medicament_transfer_temps.each do |med|
      transfer = MedicamentMovement.new(quantity:med.quantity,lot_id:med.lot.id,item_transfer_id:item_transfer.id,user_id:current_user.id)
      if transfer.save
        search_inventory = MedicamentInventory.where("warehouse_id = #{item_transfer.to_warehouse.id} and lot_id = #{med.lot.id}")
        if search_inventory.count > 0
          founded_inventory = search_inventory.first
          updated_quantity = founded_inventory.quantity + med.quantity
          founded_inventory.update(quantity:updated_quantity)
          update_log(founded_inventory.lot.id,founded_inventory.warehouse.id,1,med.quantity,current_user.id,item_transfer)
        else
          medicament_inventory = MedicamentInventory.new(warehouse_id:item_transfer.to_warehouse.id,lot_id:med.lot.id,quantity:med.quantity,user_id:current_user.id)
          if medicament_inventory.save
            update_log(medicament_inventory.lot.id,medicament_inventory.warehouse.id,1,med.quantity,current_user.id,item_transfer)
          end
        end
      end
    end
    item_transfer.medicament_transfer_temps.delete_all
    item_transfer.pharmacy_item_transfer_temps.each do |phar|
        transfer = PharmacyItemMovement.new(quantity:phar.quantity,pharmacy_item_id:phar.pharmacy_item.id,user_id:current_user.id,item_transfer_id:item_transfer.id)
      if transfer.save
        search_inventory = PharmacyItemInventory.where("warehouse_id = #{item_transfer.to_warehouse.id} and pharmacy_item_id = #{phar.pharmacy_item.id}")
        if search_inventory.count > 0
          founded_inventory = search_inventory.first
          updated_quantity = founded_inventory.quantity + phar.quantity
          founded_inventory.update(quantity:updated_quantity)
          update_pharmacy_log(founded_inventory.pharmacy_item.id,founded_inventory.warehouse.id,1,phar.quantity,current_user.id,item_transfer)
        else
          new_pharmacy_inventory = PharmacyItemInventory.new(warehouse_id:item_transfer.to_warehouse.id,pharmacy_item_id:phar.pharmacy_item.id,quantity:phar.quantity,user_id:current_user.id)
          if new_pharmacy_inventory.save
            update_pharmacy_log(new_pharmacy_inventory.pharmacy_item.id,new_pharmacy_inventory.warehouse.id,1,phar.quantity,current_user.id,item_transfer)
          end
        end
      end
    end
    item_transfer.pharmacy_item_transfer_temps.delete_all
    item_transfer.update(status:1,modified_by_id:current_user.id,reject_reason:params[:reject_reason])
    render :json => {:done => true,:url => cli_item_transfers_path}, layout: false
  end

  def item_transfer_cancel
    response = false
    item_transfer = ItemTransfer.find(params[:transfer_id])
    item_transfer.medicament_transfer_temps.each do |med|
      transfer = MedicamentMovement.new(quantity:med.quantity,lot_id:med.lot.id,item_transfer_id:item_transfer.id,user_id:current_user.id)
      if transfer.save
        search_inventory = MedicamentInventory.where("warehouse_id = #{item_transfer.from_warehouse.id} and lot_id = #{med.lot.id}")
        if search_inventory.count > 0
          founded_inventory = search_inventory.first
          updated_quantity = founded_inventory.quantity + med.quantity
          founded_inventory.update(quantity:updated_quantity)
          update_log(founded_inventory.lot.id,founded_inventory.warehouse.id,1,med.quantity,current_user.id,item_transfer)
        else
          medicament_inventory = MedicamentInventory.new(warehouse_id:item_transfer.from_warehouse.id,lot_id:med.lot.id,quantity:med.quantity,user_id:current_user.id)
          if medicament_inventory.save
            update_log(medicament_inventory.lot.id,medicament_inventory.warehouse.id,1,med.quantity,current_user.id,item_transfer)
          end
        end
      end
    end
    item_transfer.medicament_transfer_temps.delete_all
    item_transfer.pharmacy_item_transfer_temps.each do |phar|
      transfer = PharmacyItemMovement.new(quantity:phar.quantity,pharmacy_item_id:phar.pharmacy_item.id,user_id:current_user.id,item_transfer_id:item_transfer.id)
      if transfer.save
        search_inventory = PharmacyItemInventory.where("warehouse_id = #{item_transfer.from_warehouse.id} and pharmacy_item_id = #{phar.pharmacy_item.id}")
        if search_inventory.count > 0
          founded_inventory = search_inventory.first
          updated_quantity = founded_inventory.quantity + phar.quantity
          founded_inventory.update(quantity:updated_quantity)
          update_pharmacy_log(founded_inventory.pharmacy_item.id,founded_inventory.warehouse.id,1,phar.quantity,current_user.id,item_transfer)
        else
          new_pharmacy_inventory = PharmacyItemInventory.new(warehouse_id:item_transfer.from_warehouse.id,pharmacy_item_id:phar.pharmacy_item.id,quantity:phar.quantity,user_id:current_user.id)
          if new_pharmacy_inventory.save
            update_pharmacy_log(new_pharmacy_inventory.pharmacy_item.id,new_pharmacy_inventory.warehouse.id,1,phar.quantity,current_user.id,item_transfer)
          end
        end
      end
    end
    item_transfer.pharmacy_item_transfer_temps.delete_all
    item_transfer.update(status:3,modified_by_id:current_user.id,reject_reason:params[:reject_reason])
    render :json => {:done => true,:url => cli_item_transfers_path}, layout: false
  end

  def item_transfer_rejected
    item_transfer = ItemTransfer.find(params[:transfer_id])
    item_transfer.medicament_transfer_temps.each do |med|
      transfer = MedicamentMovement.new(quantity:med.quantity,lot_id:med.lot.id,item_transfer_id:item_transfer.id,user_id:current_user.id)
      if transfer.save
        search_inventory = MedicamentInventory.where("warehouse_id = #{item_transfer.from_warehouse.id} and lot_id = #{med.lot.id}")
        if search_inventory.count > 0
          founded_inventory = search_inventory.first
          updated_quantity = founded_inventory.quantity + med.quantity
          founded_inventory.update(quantity:updated_quantity)
          update_log(founded_inventory.lot.id,founded_inventory.warehouse.id,1,med.quantity,current_user.id,item_transfer)
        else
          medicament_inventory = MedicamentInventory.new(warehouse_id:item_transfer.from_warehouse.id,lot_id:med.lot.id,quantity:med.quantity,user_id:current_user.id)
          if medicament_inventory.save
            update_log(medicament_inventory.lot.id,medicament_inventory.warehouse.id,1,med.quantity,current_user.id,item_transfer)
          end
        end
      end
    end
    item_transfer.medicament_transfer_temps.delete_all
    item_transfer.pharmacy_item_transfer_temps.each do |phar|
      if transfer.save
        search_inventory = PharmacyItemInventory.where("warehouse_id = #{item_transfer.from_warehouse.id} and pharmacy_item_id = #{phar.pharmacy_item.id}")
        if search_inventory.count > 0
          founded_inventory = search_inventory.first
          updated_quantity = founded_inventory.quantity + phar.quantity
          founded_inventory.update(quantity:updated_quantity)
          update_pharmacy_log(founded_inventory.pharmacy_item.id,founded_inventory.warehouse.id,1,phar.quantity,current_user.id,item_transfer)
        else
          new_pharmacy_inventory = PharmacyItemInventory.new(warehouse_id:item_transfer.from_warehouse.id,pharmacy_item_id:phar.pharmacy_item.id,quantity:phar.quantity,user_id:current_user.id)
          if new_pharmacy_inventory.save
            update_pharmacy_log(new_pharmacy_inventory.pharmacy_item.id,new_pharmacy_inventory.warehouse.id,1,phar.quantity,current_user.id,item_transfer)
          end
        end
      end
    end
    item_transfer.pharmacy_item_transfer_temps.delete_all

    item_transfer.update(status:2,reject_reason:params[:reject_reason],modified_by_id:current_user.id)
    render :json => {:done => true,:url => cli_item_transfers_path}, layout: false
  end
  def check_user
    user_id = 0
    response = false
    msg = ""
    user = User.find_by(user_name: params[:user_name].downcase)
    user = User.find_by(email: params[:user_name].downcase) if user.nil?
    if user
      if !user.password_digest.nil?
        if user.authenticate(params[:password])
            user_id = user.id
            response = true
        else
          msg = "La contraseña es incorrecta"
        end
      else
        msg = "El usuario no cuenta con contraseña valida. Contacte a soporte tecnico"
      end
    else
      msg = "Usuario no encontrado"
    end
    render :json => {:done => response,:user_id =>user_id, :msg => msg}, layout: false

  end
  def save_transfer
    response = false
    item_transfer = ItemTransfer.new(status:0,from_warehouse_id:params[:from_warehouse],to_warehouse_id:params[:to_warehouse],
                                     user_id:current_user.id)
    if item_transfer.save
      if params[:items] != nil
        params[:items].each do |idx, item|
          #entradas
          if item['type'] == "med"
              med_temp = MedicamentTransferTemp.new(quantity:item['quantity'].to_i,lot_id:item['lot'].to_i,item_transfer_id:item_transfer.id)
              if med_temp.save
                search_inventory = MedicamentInventory.where("warehouse_id = #{item_transfer.from_warehouse.id} and lot_id = #{item['lot']}")
                if search_inventory.count > 0
                  inventory_found = search_inventory.first
                    update_quantity = inventory_found.quantity - item['quantity'].to_i
                    inventory_found.update(quantity:update_quantity)
                    update_log(item['lot'].to_i,item_transfer.from_warehouse.id,0,item['quantity'].to_i,current_user.id,item_transfer)
                end

              end
          else
            #entrada de articulos farmacia
            pharmacy_item_waste = PharmacyItemTransferTemp.new(pharmacy_item_id:item['item_id'],item_transfer_id:item_transfer.id,quantity:item['quantity'].to_i)
            if pharmacy_item_waste.save
              item_inventory_founded = PharmacyItemInventory.where("warehouse_id = #{item_transfer.from_warehouse.id} and pharmacy_item_id = #{pharmacy_item_waste.pharmacy_item.id}" )
              if item_inventory_founded.count > 0
                inventory_phar_founded = item_inventory_founded.first
                  updated_quantity_phar_item = inventory_phar_founded.quantity - pharmacy_item_waste.quantity
                  inventory_phar_founded.update(quantity:updated_quantity_phar_item)
                  update_pharmacy_log(pharmacy_item_waste.pharmacy_item.id,item_transfer.from_warehouse.id,0,pharmacy_item_waste.quantity,current_user.id,pharmacy_item_waste)
=begin
              else
                PharmacyItemInventory.new(quantity:pharmacy_item_waste.quantity,warehouse_id:item_transfer.warehouse.id,
                                          pharmacy_item_id:pharmacy_item_waste.pharmacy_item.id,user_id:current_user.id).save
                update_pharmacy_log(pharmacy_item_waste.pharmacy_item.id,item_transfer.from_warehouse.id,1,pharmacy_item_waste.quantity,current_user.id,pharmacy_item_waste)
=end
              end
            end
          end

        end
      end

      response = true
    end


    render :json => {:done => response}, layout: false

  end

  def show

  end
  def new
    @item_transfer = ItemTransfer.new
  end

  private

  def set_item_transfer
    @item_transfer = ItemTransfer.find(params[:id])
  end


end