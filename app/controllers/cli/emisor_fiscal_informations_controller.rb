class Cli::EmisorFiscalInformationsController < ApplicationController
  include Doctor::ADD_FINKOK
  skip_before_action :verify_authenticity_token, only: [:create]
  before_action :set_supplier, only: [:show, :edit, :update, :destroy]





  def new
    @emisor_fiscal_info= EmisorFiscalInformation.new
  end



  def create
    response = true
    if !params[:emisor_fiscal_info][:certificate_password].blank? &&
        !params[:emisor_fiscal_info][:certificate_stamp].blank? &&
        !params[:emisor_fiscal_info][:certificate_key].blank? && params[:is_edit].to_i == 0
      @emisor_fiscal_info = EmisorFiscalInformation.new(emisor_fiscal_info_params)
      respond_to do |format|
        if @emisor_fiscal_info.save
          if !params[:emisor_fiscal_info][:certificate_password].blank?
            # encriptar la nueva contraseña
            @emisor_fiscal_info.certificate_password = params[:emisor_fiscal_info][:certificate_password]
            @emisor_fiscal_info.update!(certificate_password: @emisor_fiscal_info.encrypt_password)
          end
          respond_message = ""
          exp_date_req =  %x'openssl x509 -enddate -noout -inform DER -in #{@emisor_fiscal_info.certificate_stamp.current_path.to_s}'
          exp_date = Date.parse exp_date_req
          today_date = Date.today
          if exp_date > today_date
            pem_op = create_pem_file @emisor_fiscal_info
            response = true
            if(!pem_op)
              response = false
              respond_message += "Contraseña de documentos fiscales incorrecta, documentos no actualizados"
              @emisor_fiscal_info.remove_certificate_stamp = true
              @emisor_fiscal_info.remove_certificate_key = true
              @emisor_fiscal_info.update!(certificate_password: "")
              @emisor_fiscal_info.errors.add(:base, :not_implemented, message: respond_message)
              EmisorFiscalInformation.find(@emisor_fiscal_info.id).delete
            end
          else
            response = false
            @emisor_fiscal_info.remove_certificate_stamp = true
            @emisor_fiscal_info.remove_certificate_key = true
            @emisor_fiscal_info.update!(certificate_password: "")
            respond_message += "El certificado ha caducado. Fecha de caducidad #{exp_date.to_s}, documentos no actualizados"
            EmisorFiscalInformation.find(@emisor_fiscal_info.id).delete
          end
          format.html { redirect_to admin_clinic_info_path(1), notice: 'Dato Creado Correctamente.' }
          format.json { render json: {:done => response,:msg => respond_message, :url => edit_admin_clinic_info_path(1)} }
        else
          format.html { render :new }
          format.json { render json: {:done => false,:msg => "occurrio un error al guardar la información"} }
        end
      end
    elsif params[:is_edit].to_i == 1
      respond_to do |format|
        emisor_fiscal_info_edit = EmisorFiscalInformation.find(params[:emisor_id])
       if emisor_fiscal_info_edit.update(emisor_fiscal_info_params)
         if !params[:emisor_fiscal_info][:certificate_password].blank? &&
             !params[:emisor_fiscal_info][:certificate_stamp].blank? &&
             !params[:emisor_fiscal_info][:certificate_key].blank?

           if !params[:emisor_fiscal_info][:certificate_password].blank?
             # encriptar la nueva contraseña
             emisor_fiscal_info_edit.certificate_password = params[:emisor_fiscal_info][:certificate_password]
             emisor_fiscal_info_edit.update!(certificate_password: emisor_fiscal_info_edit.encrypt_password)
           end
           respond_message = ""
           exp_date_req =  %x'openssl x509 -enddate -noout -inform DER -in #{emisor_fiscal_info_edit.certificate_stamp.current_path.to_s}'
           exp_date = Date.parse exp_date_req
           today_date = Date.today
           if exp_date > today_date
             pem_op = create_pem_file emisor_fiscal_info_edit
             response = true
             if(!pem_op)
               response = false
               respond_message += "Contraseña de documentos fiscales incorrecta, documentos no actualizados"
               emisor_fiscal_info_edit.remove_certificate_stamp = true
               emisor_fiscal_info_edit.remove_certificate_key = true
               emisor_fiscal_info_edit.update!(certificate_password: "")
               emisor_fiscal_info_edit.errors.add(:base, :not_implemented, message: respond_message)
             end
           else
             response = false
             emisor_fiscal_info_edit.remove_certificate_stamp = true
             emisor_fiscal_info_edit.remove_certificate_key = true
             emisor_fiscal_info_edit.update!(certificate_password: "")
             respond_message += "El certificado ha caducado. Fecha de caducidad #{exp_date.to_s}, documentos no actualizados"
           end
         end

         format.html { redirect_to admin_clinic_info_path(1), notice: 'Dato Creado Correctamente.' }
         format.json { render json: {:done => response,:msg => respond_message, :url => edit_admin_clinic_info_path(1)} }
       else
         format.html { render :new }
         format.json { render json: {:done => false,:msg => "occurrio un error al guardar la información"} }
       end
      end
    else
      respond_to do |format|
        format.html { redirect_to admin_clinic_info_path(1), notice: 'Dato Creado Correctamente.' }
        format.json { render json: {:done => false,:msg => "Debe ingresar sus archivos fiscales y contraseña", :url => admin_clinic_info_path(1)} }
      end
    end

  end


  def create_pem_file emisor_fiscal_information
    create_pem_output = true
    ruta_pem_file = Dir.pwd + "/uploads/clinic_info/" + emisor_fiscal_information.id.to_s + "/certificate_key/key.pem"
    fiscal_info = emisor_fiscal_information
    cert_pass = fiscal_info.decrypt_password
    console_output = %x'openssl pkcs8 -inform DER -in #{fiscal_info.certificate_key.current_path.to_s} -passin pass:#{cert_pass}'
    #valida que la contraseña sea la correcta si es correcta regresa algo cool
    if console_output.length > 3
      %x'openssl pkcs8 -inform DER -in #{fiscal_info.certificate_key.current_path.to_s} -passin pass:#{cert_pass} > #{ruta_pem_file}'
      #puts "pem correcto"
      #Obtiene el numero de serie del certificado y lo convierte al numero.
      certificado =  %x'openssl x509 -serial -noout -inform DER -in #{emisor_fiscal_information.certificate_stamp.current_path.to_s}'
      arreglonumero = certificado.to_s.split('=')
      arreglosolonumero = arreglonumero[1].to_s.split(//)
      serialNumberCer = []
      (1..39).step(2) do |n|
        serialNumberCer.push(arreglosolonumero[n])
        puts arreglosolonumero[n]
      end
      noCertificado = serialNumberCer.join.to_s
      emisor_fiscal_information.update(certificate_number: noCertificado)
      certificado =  %x'openssl x509 -inform DER -in #{emisor_fiscal_information.certificate_stamp.current_path.to_s}'
      arrayc =  certificado.split("\n")
      arrayc.shift
      arrayc.pop
      certificadobase64 = arrayc.join.to_s
      emisor_fiscal_information.update(certificate_base64: certificadobase64)
      emisor_fiscal_information.certificate_password = cert_pass
      emisor_fiscal_information.update(certificate_password: emisor_fiscal_information.encrypt_password)
    else
      create_pem_output = false
      #puts "pem no correcto"
    end
    #registra un nuevo rfc
    cliente = Clientes2.new
    cliente.register_in_finkok emisor_fiscal_information.rfc
    create_pem_output
  end

  def destroy

    if @emisor_fiscal_info.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Data Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private




  def emisor_fiscal_info_params
    params.require(:emisor_fiscal_info).permit(:rfc, :bussiness_name, :fiscal_address, :ext_number , :city_id , :certificate_stamp ,:certificate_key, :tax_regime,:zip,:suburb,:locality,:folio,:serie,:int_number,:iva)
  end

end