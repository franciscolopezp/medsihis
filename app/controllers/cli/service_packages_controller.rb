class Cli::ServicePackagesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]

  before_action :set_service_package, only: [:show, :edit, :update, :destroy,:manage_services]


  def index
    respond_to do |format|
      format.html
      format.json { render json: ServicePackagesDatatable.new(view_context)}
    end
  end


  def new
    @service_package= ServicePackage.new
  end



  def create
    @service_package = ServicePackage.new(service_package_params)
    respond_to do |format|
      if @service_package.save
        format.html { redirect_to manage_services_cli_service_packages_path(@service_package), notice: 'paquete Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @service_package.update(service_package_params)
        format.html { redirect_to cli_service_packages_url, notice: 'paquete Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end
  def delete_service
    response = false
    service_package_cost = PackageServiceCost.find(params[:id])
    if service_package_cost != nil
      if PackageServiceCost.find(service_package_cost.id).delete
        response = true
      end
    end
    respond_to do |format|
      format.html {render json: response}
    end
  end
  def add_service
    response = true
    service_package = ServicePackage.find(params[:service_package_id])
    service_package.hospital_services.each do |subs|
      if subs.id == params[:service_id].to_i
        response = false
      end
    end
    if response
     PackageServiceCost.create(hospital_service_id:params[:service_id].to_i,service_package_id:service_package.id,cost:params[:cost].to_f)
    end
    respond_to do |format|
      format.html {render json: response}
    end
  end
  def manage_services
    @service_package = ServicePackage.find(params[:id])
  end
  def destroy

    if @service_package.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'paquete Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_service_package
    @service_package = ServicePackage.find(params[:id])
  end


  def service_package_params
    params.require(:service_package).permit(:name)
  end

end