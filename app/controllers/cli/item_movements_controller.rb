class Cli::ItemMovementsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new]
  include Cli::MedicamentHelper
  before_action :set_medicament_purchase, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: ItemMovementsDatatable.new(view_context)}
    end
  end

  def save_movement
    response = false
    item_movement = ItemMovement.new(reason:params[:reason],warehouse_id:params[:warehouse],waste_type_id:params[:waste_type],
                                                     user_id:current_user.id)
    if item_movement.save
      if params[:items] != nil
        params[:items].each do |idx, item|
            #entradas
            if item['type'] == "med"
                if item['lot'].to_i == 0
                  new_lot = Lot.new(name:item['lot_name'],expiration_date:item['exp_date'],medicament_id:item['item_id'].to_i,user_id:current_user.id)
                  if new_lot.save
                    medicament_waste = MedicamentWaste.new(quantity:item['quantity'],lot_id:new_lot.id,warehouse_id:item_movement.warehouse.id,item_movement_id:item_movement.id)
                    if medicament_waste.save
                      MedicamentInventory.new(warehouse_id:item_movement.warehouse.id,lot_id:new_lot.id,quantity:item['quantity'],user_id:current_user.id).save
                      update_log(new_lot.id,item_movement.warehouse.id,1,item['quantity'].to_i,current_user.id,item_movement)
                    end
                  end
                else
                  medicament_waste = MedicamentWaste.new(quantity:item['quantity'].to_i,warehouse_id:item_movement.warehouse.id,user_id:current_user.id,lot_id:item['lot'].to_i,item_movement_id:item_movement.id)
                  if medicament_waste.save
                    search_inventory = MedicamentInventory.where("warehouse_id = #{item_movement.warehouse.id} and lot_id = #{item['lot']}")
                    if search_inventory.count > 0
                      inventory_found = search_inventory.first
                      if params[:is_entry] == "true"
                        update_quantity = inventory_found.quantity + item['quantity'].to_i
                        inventory_found.update(quantity:update_quantity)
                        update_log(item['lot'].to_i,item_movement.warehouse.id,1,item['quantity'].to_i,current_user.id,item_movement)
                      else
                        update_quantity = inventory_found.quantity - item['quantity'].to_i
                        inventory_found.update(quantity:update_quantity)
                        update_log(item['lot'].to_i,item_movement.warehouse.id,0,item['quantity'].to_i,current_user.id,item_movement)
                      end

                    end
                  end
                end
            else
              #entrada de articulos farmacia
              pharmacy_item_waste = PharmacyItemWaste.new(pharmacy_item_id:item['item_id'].to_i,warehouse_id:item_movement.warehouse.id,
                                                                         quantity:item['quantity'].to_i,user_id:current_user.id,
                                                                         item_movement_id:item_movement.id)
              if pharmacy_item_waste.save
                item_inventory_founded = PharmacyItemInventory.where("warehouse_id = #{item_movement.warehouse.id} and pharmacy_item_id = #{pharmacy_item_waste.pharmacy_item.id}" )
                if item_inventory_founded.count > 0
                  inventory_phar_founded = item_inventory_founded.first
                  if params[:is_entry] == "true"
                    updated_quantity_phar_item = inventory_phar_founded.quantity + pharmacy_item_waste.quantity
                    inventory_phar_founded.update(quantity:updated_quantity_phar_item)
                    update_pharmacy_log(pharmacy_item_waste.pharmacy_item.id,item_movement.warehouse.id,1,pharmacy_item_waste.quantity,current_user.id,pharmacy_item_waste)
                  else
                    updated_quantity_phar_item = inventory_phar_founded.quantity - pharmacy_item_waste.quantity
                    inventory_phar_founded.update(quantity:updated_quantity_phar_item)
                    update_pharmacy_log(pharmacy_item_waste.pharmacy_item.id,item_movement.warehouse.id,0,pharmacy_item_waste.quantity,current_user.id,pharmacy_item_waste)
                  end
                else
                  PharmacyItemInventory.new(quantity:pharmacy_item_waste.quantity,warehouse_id:item_movement.warehouse.id,
                                            pharmacy_item_id:pharmacy_item_waste.pharmacy_item.id,user_id:current_user.id).save
                  update_pharmacy_log(pharmacy_item_waste.pharmacy_item.id,item_movement.warehouse.id,1,pharmacy_item_waste.quantity,current_user.id,pharmacy_item_waste)
                end
              end
            end

        end
      end

      response = true
    end


    render :json => {:done => response}, layout: false

  end
end
