class Cli::HospitalServiceCategoriesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:category_services_report]
  before_action :set_hospital_service_category, only: [:show, :edit, :update, :destroy]
  include Cli::HospitalServicesHelper


  def index
    respond_to do |format|
      format.html
      format.json { render json: HospitalServiceCategoriesDatatable.new(view_context)}
    end
  end

  def category_services_report

  end
  def export_services
    @initial_date = params[:fecha_ini].to_date
    @end_date = params[:fecha_f].to_date
    puts @initial_date
    @export_info = get_all_services_by_category
    filename = "Reporte_servicios_"+params[:fecha_ini]+"_a_"+params[:fecha_f]+".xls"
    respond_to do |format|
      format.html
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end
  end
  def search_services
    all_services = get_all_services_by_category
    respond_to do |format|
      format.html { render json: all_services}
    end
  end

  def new
    @hospital_service_category= HospitalServiceCategory.new
  end



  def create
    @hospital_service_category = HospitalServiceCategory.new(hospital_service_category_params)
    respond_to do |format|
      if @hospital_service_category.save
        format.html { redirect_to cli_hospital_service_categories_url, notice: 'categoría Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @hospital_service_category.update(hospital_service_category_params)
        format.html { redirect_to cli_hospital_service_categories_url, notice: 'categoría Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @hospital_service_category.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Categoría Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_hospital_service_category
    @hospital_service_category = HospitalServiceCategory.find(params[:id])
  end


  def hospital_service_category_params
    params.require(:hospital_service_category).permit(:name)
  end

end