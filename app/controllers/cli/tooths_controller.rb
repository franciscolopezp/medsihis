class Cli::ToothsController < ApplicationController




def get_last_data
  pathologies = []
  treatments = []
  history = []
=begin
  lasts_pathologicals = ToothPathology.where("medical_history_id = #{params[:medical_history_id]}").group('created_at').order('created_at desc')
  last_treatmens = ToothTreatment.where("medical_history_id = #{params[:medical_history_id]}").group('created_at').order('created_at desc')
  if lasts_pathologicals.count.length > 0
      ToothPathology.where("created_at = ?",lasts_pathologicals.first.created_at).each do |pathology|
        pathologies.push({
                             :section => pathology.tooth_section.id,
                             :description => pathology.description
                         })
      end
  end
  if last_treatmens.count.length > 0
    ToothTreatment.where("created_at = ?",last_treatmens.first.created_at).each do |treatment|
      treatments.push({
                          :section => treatment.tooth_section.id,
                          :description => treatment.description
                      })

    end
  end
=end
  ToothPathology.where("medical_history_id = #{params[:medical_history_id]}").each do |info|
    history.push({
                     :section => info.tooth_section.id,
                     :description => info.description,
                     :type => 0,
                     :date => info.created_at
                 })
  end
  ToothTreatment.where("medical_history_id = #{params[:medical_history_id]}").each do |info|
    history.push({
                     :section => info.tooth_section.id,
                     :description => info.description,
                     :type => 1,
                     :date => info.created_at
                 })
  end

  render :json => {:done => true,:pathologies => pathologies, :treatments => treatments,:history => history}, layout: false
end
def get_teeth_historial
  pathologies = []
  treatments = []
  ToothPathology.where("medical_history_id = #{params[:medical_history_id]} AND tooth_section_id = #{params[:section_id]}").each do |pathology|
    pathologies.push({
              :date => pathology.created_at.strftime("%d/%m/%Y"),
              :teeth => pathology.tooth_section.tooth.name,
              :section => pathology.tooth_section.name,
              :description => pathology.description
                     })
  end
  ToothTreatment.where("medical_history_id = #{params[:medical_history_id]} AND tooth_section_id = #{params[:section_id]}").each do |treatment|
      treatments.push({
                          :date => treatment.created_at.strftime("%d/%m/%Y"),
                          :teeth => treatment.tooth_section.tooth.name,
                          :section => treatment.tooth_section.name,
                          :description => treatment.description
                      })
  end
  render :json => {:done => true,:pathologies => pathologies, :treatments => treatments}, layout: false
end
  def save_odontogram

    if params[:pathologies] != nil
      original_query = ToothPathology.where("medical_history_id = #{params[:medical_history_id]}")
      original_query_treat = ToothTreatment.where("medical_history_id = #{params[:medical_history_id]}")
      if original_query.count > 0 || original_query_treat.count > 0
        is_original = 0
      else
        is_original = 1
      end
      params[:pathologies].each do |idx, item|
        ToothPathology.new(description:item['description'],
                                                             tooth_section_id:item['section_id'].to_i,
                                                             medical_history_id:params[:medical_history_id],
                                                             is_original:is_original).save
      end
    end
    if params[:treatments] != nil
      params[:treatments].each do |idx, item|
        ToothTreatment.new(description:item['description'],
                           tooth_section_id:item['section_id'].to_i,
                           medical_history_id:params[:medical_history_id],
                           is_original:is_original).save
      end
    end
    render :json => {:done => true}, layout: false
  end

  def get_original_odontogram
    pathologies = []
    treatments = []
   ToothTreatment.where("medical_history_id = #{params[:medical_history_id]} AND is_original = 1").each do |treatment|
     treatments.push({
         :section => treatment.tooth_section.id,
         :description => treatment.description
                    })
   end
   ToothPathology.where("medical_history_id = #{params[:medical_history_id]} AND is_original = 1").each do |pathology|
     pathologies.push({
                          :section => pathology.tooth_section.id,
                          :description => pathology.description
                      })
   end
    render :json => {:done => true,:pathologies => pathologies, :treatments => treatments}, layout: false
  end


  def create

  end







  private



end