class Cli::MedicalAnnotationFilesController < ApplicationController

  def create
    annotation_file = MedicalAnnotationFile.new(medical_annotation_file_params)
    annotation_file.user = current_user
    if annotation_file.save
      case params[:source]
        when "hospitalization"
          redirect_to cli_hospitalization_path(params[:hospitalization_id])
      end
    end
  end

  def download
    annotation_file = MedicalAnnotationFile.find(params[:id])
    if is_image(annotation_file.document.url)
      send_file annotation_file.document.path, :disposition => 'inline'
    else
      send_file annotation_file.document.path
    end
  end

  def is_image(file)
    ext = File.extname(file)
    ['.png','.jpg','.jpeg','.bmp'].include? ext
  end

  private
  def medical_annotation_file_params
    params.require(:medical_annotation_file).permit(:medical_annotation_id,:notes, :document)
  end
end
