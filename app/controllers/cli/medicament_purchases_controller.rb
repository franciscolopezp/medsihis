class Cli::MedicamentPurchasesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new]
  include Cli::MedicamentHelper
  before_action :set_medicament_purchase, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicamentPurchasesDatatable.new(view_context)}
    end
  end

  def show

  end
  def new
    @medicament_purchase= MedicamentPurchase.new
  end


  def save_purchase
    response = false
    medicament_purchase = MedicamentPurchase.new(cost:params[:cost].to_f,supplier_id:params[:supplier],folio:params[:folio],
    invoice_number:params[:invoice_number],user_id:current_user.id,warehouse_id:params[:warehouse])
    if medicament_purchase.save
      if params[:pharmacy_items] != nil
      params[:pharmacy_items].each do |idx, item|
        if item['type'] == "med"
            if item['lot'].to_i == 0
              new_lot = Lot.new(name:item['lot_name'],expiration_date:item['exp_date'],medicament_id:item['item_id'].to_i,user_id:current_user.id)
              if new_lot.save
                medicament_purchase_item = MedicamentPurchaseItem.new(quantity:item['quantity'],medicament_id:item['item_id'].to_i,lot_id:new_lot.id,cost:item['cost'].to_f,medicament_purchase_id:medicament_purchase.id)
                if medicament_purchase_item.save
                  medicament_updated = Medicament.find(item['item_id'])
                  medicament_updated.update(sale_cost:item['buy_cost'].to_f)
                  MedicamentInventory.new(warehouse_id:medicament_purchase.warehouse.id,lot_id:new_lot.id,quantity:item['quantity'],user_id:current_user.id).save
                  update_log(new_lot.id,medicament_purchase.warehouse.id,1,item['quantity'].to_i,current_user.id,medicament_purchase_item)
                end
              end
            else
              medicament_purchase_item = MedicamentPurchaseItem.new(quantity:item['quantity'].to_i,medicament_id:item['item_id'].to_i,lot_id:item['lot'].to_i,cost:item['cost'].to_f,medicament_purchase_id:medicament_purchase.id)
              if medicament_purchase_item.save
                medicament_updated = Medicament.find(item['item_id'].to_i)
                medicament_updated.update(sale_cost:item['buy_cost'].to_f)
                search_inventory = MedicamentInventory.where("warehouse_id = #{medicament_purchase.warehouse.id} and lot_id = #{item['lot']}")
                if search_inventory.count > 0
                  inventory_found = search_inventory.first
                  update_quantity = inventory_found.quantity + item['quantity'].to_i
                  inventory_found.update(quantity:update_quantity)
                  update_log(item['lot'].to_i,medicament_purchase.warehouse.id,1,item['quantity'].to_i,current_user.id,medicament_purchase_item)
                end
              end
            end

        else
          pharmacy_item_purchase = MedicamentPurchaseFarmacyItem.new(pharmacy_item_id:item['item_id'].to_i,
                                                                     quantity:item['quantity'].to_i,cost:item['cost'].to_f,
                                                                     medicament_purchase_id:medicament_purchase.id)
          if pharmacy_item_purchase.save
            item_inventory_founded = PharmacyItemInventory.where("warehouse_id = #{medicament_purchase.warehouse.id} and pharmacy_item_id = #{pharmacy_item_purchase.pharmacy_item.id}" )
            item_updated = PharmacyItem.find(item['item_id'].to_i)
            item_updated.update(sale_cost:item['buy_cost'].to_f)
            if item_inventory_founded.count > 0
              inventory_phar_founded = item_inventory_founded.first
              updated_quantity_phar_item = inventory_phar_founded.quantity + pharmacy_item_purchase.quantity
              inventory_phar_founded.update(quantity:updated_quantity_phar_item)
            else
              PharmacyItemInventory.new(quantity:pharmacy_item_purchase.quantity,warehouse_id:medicament_purchase.warehouse.id,
                                        pharmacy_item_id:pharmacy_item_purchase.pharmacy_item.id,user_id:current_user.id).save
            end
            update_pharmacy_log(pharmacy_item_purchase.pharmacy_item.id,medicament_purchase.warehouse.id,1,pharmacy_item_purchase.quantity,current_user.id,pharmacy_item_purchase)
          end
        end
      end

      end


      response = true
    end


    render :json => {:done => response}, layout: false
  end
  def get_last_folio
    last_folio = 1
    last_medicament_purchase = MedicamentPurchase.last
    if last_medicament_purchase != nil
      last_folio = last_medicament_purchase.folio + 1
    end
    render :json => {:folio => last_folio}, layout: false
  end
  def get_lot_medicament
    lots_a = []
    lots = Lot.where("medicament_id = #{params[:medicament_id]}")
    lots.each do |lot|
        lots_a.push({
                        :id => lot.id,
                        :name => lot.name,
                        :exp_date => lot.expiration_date.strftime("%d/%m/%Y")
                    })
    end
    render :json => {:lots => lots_a}, layout: false
  end
  def get_lot
    lot = Lot.find(params[:lot_id])
    render :json => {:medicament_id => lot.medicament_id,:lot_name => lot.name,:expiration_date => lot.expiration_date.strftime("%d/%m/%Y")}, layout: false
  end
  def create
    @medicament_purchase = MedicamentPurchase.new(medicament_purchase_params)
    respond_to do |format|
      if @medicament_purchase.save
        format.html { redirect_to cli_medicament_purchases_url, notice: 'Compra de medicamento Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @medicament_purchase.update(medicament_purchase_params)
        format.html { redirect_to cli_medicament_purchases_url, notice: 'Compra de medicamento Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy
    @medicament_purchase.medicament_purchase_items.delete_all
    if @medicament_purchase.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Compra de medicamento Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_medicament_purchase
    @medicament_purchase = MedicamentPurchase.find(params[:id])
  end


  def medicament_purchase_params
    params.require(:medicament_purchase).permit(:cost,:invoice_number,:supplier_id,:warehouse_id)
  end

end