class Cli::SettingsController < ApplicationController
  load_and_authorize_resource :only => [:index]
  skip_before_filter :verify_authenticity_token

  def index

  end


  def create
    params[:settings].each do |idx, s|
      Setting.where('code = ?', s[:code]).update_all(data: s[:data])
    end
    render json: {:done => true}
  end

  def save_image
    image = SettingImage.new(name:params[:name], code: "["+params[:code]+"]", image: params[:image])
    image.save
    redirect_to cli_settings_path
  end
end