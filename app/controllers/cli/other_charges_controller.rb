class Cli::OtherChargesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:charges_report]
  include Doctor::DailyCashesHelper
  before_action :set_other_charge, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: OtherChargesDatatable.new(view_context)}
    end
  end

  def charges_report

  end

  def export_charges
    charges_info = []
    transactions = Transaction.where("date between STR_TO_DATE('#{params[:fecha_ini]}', '%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('#{params[:fecha_f]}', '%d/%m/%Y'), INTERVAL 1 day) ").order('created_at DESC')
    transactions.each do |t|
      folio = ''
      details = ''
      services = ''
      lable_amount =  t.amount.to_s
      case t.chargeable_type
        when 'OtherMovement'
          services = 'Otros movimientos'
          details << 'Tipo de Movimiento: '+t.chargeable.movement_type.name + '; '
          details << 'Detalles: '+t.chargeable.details
          if t.chargeable.movement_type.id == 22
            lable_amount =  "-"+t.amount.to_s
          end
        when 'Charge'
          services = 'Cobro de consulta'
          details << 'Paciente: '+t.chargeable.medical_consultation.patient.full_name + '; '
          details << 'Fecha Consulta: '+t.chargeable.medical_consultation.date_consultation.strftime('%d/%m/%Y %I:%M %P') + '; '
          details << 'Detalles: '+t.chargeable.details
        when 'Transfer'
          services = 'Transferencia'
          details << 'Transferencia de ' + t.chargeable.from.account_number+' ('+t.chargeable.from.bank.name+') a '+ t.chargeable.to.account_number+' ('+t.chargeable.to.bank.name+'); '
          details << 'Detalles: '+t.description
        when 'ChargeReportStudy'
          services ='Cobro de Estudio'
          details << 'Estudio: ' + t.chargeable.get_study+'; '
          details << 'Paciente: ' + t.chargeable.get_patient+'; '
          details << 'Detalles: '+t.description
        when 'AdmissionTransaction'
          t.chargeable.hospital_admission.service_charges.each_with_index do |service,idx|
            services += (idx+1).to_s + ".- " + service.hospital_service.name + '; '
          end
          details << 'Admisión hospitalaria' +';'
          details << 'Paciente: ' + t.chargeable.hospital_admission.patient.full_name+'; '
          folio = t.chargeable.folio
        when 'OtherChargeTransaction'
          t.chargeable.other_charge.other_charge_items.each_with_index do |service,idx|
            services += (idx+1).to_s + ".- " +service.hospital_service.name + '; '
          end
          details << 'Otros cargos' +'; '
          details << 'Paciente: ' + t.chargeable.other_charge.patient_name+'; '
          folio = t.chargeable.folio
      end

      charges_info.push({
          :folio => folio,
          :date => t.date.strftime("%d/%m/%Y"),
          :service => services,
          :details => details,
          :responsible => t.user.person.full_name,
          :amount => lable_amount
                        })
    end
    @export_info = charges_info
    filename = "Reporte_cobros_"+params[:fecha_ini]+"_a_"+params[:fecha_f]+".xls"
    respond_to do |format|
      format.html
      format.xls { headers["Content-Disposition"] = "attachment; filename=\"#{filename}\"" }
    end

  end
  def search_charges
    charges_info = []
    transactions = Transaction.where("date between STR_TO_DATE('#{params[:fecha_ini]}', '%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('#{params[:fecha_f]}', '%d/%m/%Y'), INTERVAL 1 day) ").order('created_at DESC')
    transactions.each do |t|
      details = ''
      services = ''
      folio = ''
      lable_amount =  "<label style='color: #0a68b4; font-weight: bold;'>$ "+t.amount.to_s+"</label>"
      case t.chargeable_type
        when 'OtherMovement'
          services = 'Otros movimientos'
          details << 'Tipo de Movimiento: '+t.chargeable.movement_type.name + '<br>'
          details << 'Detalles: '+t.chargeable.details
          if t.chargeable.movement_type.id == 22
            lable_amount =  "<label style='color: red; font-weight: bold;'>-$ "+t.amount.to_s+"</label>"
          end
        when 'Charge'
          services = 'Cobro de consulta'
              details << 'Paciente: '+t.chargeable.medical_consultation.patient.full_name + ' <br> '
          details << 'Fecha Consulta: '+t.chargeable.medical_consultation.date_consultation.strftime('%d/%m/%Y %I:%M %P') + ' <br> '
          details << 'Detalles: '+t.chargeable.details
        when 'Transfer'
          services = 'Transferencia'
              details << 'Transferencia de ' + t.chargeable.from.account_number+' ('+t.chargeable.from.bank.name+') a '+ t.chargeable.to.account_number+' ('+t.chargeable.to.bank.name+')<br>'
          details << 'Detalles: '+t.description
        when 'ChargeReportStudy'
          services ='Cobro de Estudio'
              details << 'Estudio: ' + t.chargeable.get_study+'<br>'
          details << 'Paciente: ' + t.chargeable.get_patient+'<br>'
          details << 'Detalles: '+t.description
        when 'AdmissionTransaction'
          t.chargeable.hospital_admission.service_charges.each_with_index do |service,idx|
            services += (idx+1).to_s + ".- " + service.hospital_service.name + '<br>'
          end
          details << 'Admisión hospitalaria' +'<br>'
          details << 'Paciente: ' + t.chargeable.hospital_admission.patient.full_name+'<br>'
          folio = t.chargeable.folio
        when 'OtherChargeTransaction'
          t.chargeable.other_charge.other_charge_items.each_with_index do |service,idx|
            services += (idx+1).to_s + ".- " +service.hospital_service.name + '<br>'
          end
          details << 'Otros cargos ' +'<br>'
          details << 'Paciente: ' + t.chargeable.other_charge.patient_name+'<br>'
          folio = t.chargeable.folio
      end

      charges_info.push({
          :folio => folio,
                  :responsible => t.user.person.full_name,
                 :date => date_hour(t.date),
                 :service => services,
                 :amount => lable_amount,
                 :details => details,
                        })
    end
    respond_to do |format|
      format.html { render json: charges_info}
    end
  end
  def show

  end
  def new
    @other_charge= OtherCharge.new
  end

  def cancel_payment
    response = false
    transaction_to_cancel = OtherChargeTransaction.find(params[:other_charge_transaction_id])
    other_movement = OtherMovement.new
    other_movement.movement_type = MovementType.find(22)
    other_movement.details = "Cancelación de pago. Folio: " + transaction_to_cancel.folio
    #Transacion 22
    if other_movement.save
      transaction_to_cancel.update(is_cancel:1)
      response = true
      amounts = params[:amounts]
      amounts.each do |idx, val|
        data = Hash.new
        data['chargeable'] = other_movement
        data['description'] = "Cancelación de pago. Folio: " + transaction_to_cancel.folio
        data['t_type'] = params[:t_type]
        data['account_id'] = transaction_to_cancel.transactions.first.accounts.first.id
        data['transaction_type'] = params[:transaction_type_id]
        data['payment_method'] = transaction_to_cancel.transactions.first.payment_method.id
        amount = val[:amount].to_f
        currency_id = transaction_to_cancel.transactions.first.currency.id
        ex = CurrencyExchange.find(transaction_to_cancel.transactions.first.currency.id)
        data['currency_id'] = transaction_to_cancel.transactions.first.currency.id
        data['currency_exchange'] = ex.value
        data['amount'] = amount
        generate_transaction data
    end

    end
    respond_to do |format|
      format.json { render :json => {:done => response}  }
    end
  end
  def pay_other_charge
    response = false
    other_charge = OtherCharge.find(params[:other_charge_id])
    other_charge_transaction = OtherChargeTransaction.new
    other_charge_transaction.folio = get_nex_folio_ticket
    other_charge_transaction.amount = params[:amount].to_f
    other_charge_transaction.user = current_user
    other_charge_transaction.other_charge = other_charge
    other_charge_transaction.is_cancel = false

    if other_charge_transaction.save
      response = true
      #Transacion
      amounts = params[:amounts]
      amounts.each do |idx, val|
        data = Hash.new
        data['chargeable'] = other_charge_transaction
        data['description'] = params[:details]
        data['t_type'] = params[:t_type]
        data['account_id'] = params[:account_id]
        data['transaction_type'] = params[:transaction_type_id]
        data['payment_method'] = params[:payment_method_id]

        amount = val[:amount].to_f
        currency_id = val[:currency].to_i
        ex = CurrencyExchange.find(val[:currency].to_i)

        data['currency_id'] = currency_id
        data['currency_exchange'] = ex.value
        data['amount'] = amount
        generate_transaction data

      end
    end
    respond_to do |format|
      format.json { render :json => {:done => response}  }
    end
  end
  def get_services_from_package
    services = []
    package = ServicePackage.find(params[:id])
    if package.package_service_costs.count > 0
      response = true
      package.package_service_costs.each do |service|
        services.push({
                          :service_id => service.hospital_service.id,
                          :service_name => service.hospital_service.name,
                          :service_cost => service.cost
                      })

      end
    else
      response = false
    end
    render :json => {:done => response, :services => services}, layout: false
  end
  def get_nex_folio_ticket
    clinic = ClinicInfo.find(1)
    series = clinic.ticket_series
    next_folio = clinic.ticket_next_folio
    new_folio = series+"-"+next_folio.to_s
    clinic.update(ticket_next_folio: next_folio + 1)
    return new_folio
  end

  def save_other_charge
    if params[:is_update].to_i == 0
      other_charge = OtherCharge.new
      other_charge.patient_name = params[:patient_name]
      other_charge.total_amount = params[:total].to_f
      other_charge.total_discount = params[:discount].to_f
      other_charge.user = current_user
      other_charge.is_closed = false
      if other_charge.save
        params[:services].each do |idx, item|
          OtherChargeItem.new(service_amount:item['cost'].to_f,
                              service_discount:item['discount'].to_f,total:item['total'].to_f,
                              other_charge_id:other_charge.id,
                              hospital_service_id:item['service_id'].to_i).save

        end
      end
    else
      other_charge = OtherCharge.find(params[:is_update])
      if other_charge.update(patient_name:params[:patient_name],total_amount:params[:total].to_f,total_discount:params[:discount].to_f,user_id:current_user.id)
        other_charge.other_charge_items.destroy_all
        params[:services].each do |idx, item|
          OtherChargeItem.new(service_amount:item['cost'].to_f,
                              service_discount:item['discount'].to_f,total:item['total'].to_f,
                              other_charge_id:other_charge.id,
                              hospital_service_id:item['service_id'].to_i).save

        end
      end
    end

    render :json => {:done => true, :root => edit_cli_other_charge_path(other_charge)}, layout: false
  end
  def get_charge_items
    items = []
    other_charge = OtherCharge.find(params[:id])
    other_charge.other_charge_items.each do |info|
      items.push({
                     id:info.id,
                     service_id:info.hospital_service.id,
          cost: info.service_amount,
          discount:info.service_discount,
          total: info.total
                 })
    end
    total_paid = 0
    other_charge.other_charge_transactions.where("is_cancel = 0").each do |info|
      total_paid += info.amount
    end
    render :json => {:done => true, :items =>items,:total_paid => total_paid}, layout: false
  end
  def close_other_charge
    other_charge_to_close = OtherCharge.find(params[:id])
    other_charge_to_close.update(is_closed:1)
    render :json => {:done => true,:root =>cli_other_charge_path(other_charge_to_close)}, layout: false
  end
  def check_close_permission
    user_id = 0
    response = false
    msg = ""
    user = User.find_by(user_name: params[:user_name].downcase)
    user = User.find_by(email: params[:user_name].downcase) if user.nil?
    if user
      if !user.password_digest.nil?
        if user.authenticate(params[:password])
          if user.can_close_charge
            user_id = user.id
            response = true
          else
            msg = "El usuario no tiene permiso para cerrar"
          end
        else
          msg = "La contraseña es incorrecta"
        end
      else
        msg = "El usuario no cuenta con contraseña valida. Contacte a soporte tecnico"
      end
    else
      msg = "Usuario no encontrado"
    end
    render :json => {:done => response,:user_id =>user_id, :msg => msg}, layout: false

  end
  def check_cancel_permission
    user_id = 0
    response = false
    msg = ""
    user = User.find_by(user_name: params[:user_name].downcase)
    user = User.find_by(email: params[:user_name].downcase) if user.nil?
    if user
      if !user.password_digest.nil?
        if user.authenticate(params[:password])
          if user.can_cancel_payment
            user_id = user.id
            response = true
          else
            msg = "El usuario no tiene permiso para cancelar pagos"
          end
        else
          msg = "La contraseña es incorrecta"
        end
      else
        msg = "El usuario no cuenta con contraseña valida. Contacte a soporte tecnico"
      end
    else
      msg = "Usuario no encontrado"
    end
    render :json => {:done => response,:user_id =>user_id, :msg => msg}, layout: false

  end
  def create
    @other_charge = OtherCharge.new(other_charge_params)
    respond_to do |format|
      if @other_charge.save
        format.html { redirect_to cli_other_charges_url, notice: 'Cobro Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @other_charge.update(other_charge_params)
        format.html { redirect_to cli_other_charges_url, notice: 'Cobro Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @other_charge.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'cobro Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_other_charge
    @other_charge = OtherCharge.find(params[:id])
  end


  def other_charge_params
    params.require(:other_charge).permit(:patient_name,:total_amount,:total_discount)
  end

end