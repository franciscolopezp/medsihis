class Cli::AdmissionInvoicesController < ApplicationController
  load_and_authorize_resource :only => [:index]
  include Doctor::DailyCashesHelper
  include Doctor::FINKOK

  def index
    respond_to do |format|
      format.html
      format.json { render json: AdmissionInvoicesDatatable.new(view_context)}
    end
  end
  def get_receptor_email
    emails = []
    t = AdmissionInvoice.find(params[:invoice_id])
    if t != nil
        if t.hospital_admission.patient.email.present?
          emails.push({
                          number:0,
                          email:t.hospital_admission.patient.email
                      })
        end
        if t.hospital_admission.patient.patient_fiscal_informations.count > 0
          t.hospital_admission.patient.patient_fiscal_informations.each do |fiscal|
            if fiscal.email.present?
              emails.push({
                              number:1,
                              email:fiscal.email
                          })

            end
            if fiscal.email2.present?
              emails.push({
                              number:2,
                              email:fiscal.email2
                          })

            end
          end

        end

    end

    respond_to do |format|
      format.html { render json: {:emails => emails} }
    end
  end
  def print_admission_invoice_pdf
    inv = AdmissionInvoice.find(params[:id])
    respond_to do |format|
      format.pdf do
        render pdf: "Factura "+inv.serie+"-"+inv.folio.to_s,  locals: { :@invoice_data => inv }, template: 'cli/hospital_admissions/template_invoice.html.erb', layout: false,
               margin: { top: 5, bottom: 20, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'cli/hospital_admissions/template_invoice_footer.html.erb', locals: {}}, center: 'TEXT',
                                                                                                  spacing:           1,
                                                                                                  line:              true
            }
      end
    end
  end
  def send_invoice_email
      inv = AdmissionInvoice.find(params[:invoice_id])

        pdf_name = "Factura "+inv.serie+"-"+inv.folio.to_s
        pdfs = render_to_string  pdf: "Factura "+inv.serie+"-"+inv.folio.to_s,  locals: { :@invoice_data => inv }, template: 'cli/hospital_admissions/template_invoice.html.erb', layout: false,
                                 margin: { top: 5, bottom: 20, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'cli/hospital_admissions/template_invoice_footer.html.erb', locals: {}}, center: 'TEXT',
                                                                                                                    spacing:           1,
                                                                                                                    line:              true
            }

      emaillist = params[:email]
      if params[:emailp] != ""
        emaillist += ";" + params[:emailp]
      end
      if params[:emailf1] != ""
        emaillist += ";" + params[:emailf1]
      end
      if params[:emailf2] != ""
        emaillist += ";" + params[:emailf2]
      end
      if params[:extramails] != ""
        emaillist += ";" +params[:extramails]
      end
      SendInvoice.send_invoice(emaillist,pdfs,pdf_name,inv).deliver_now

      render :json => true, layout: false


    end
  def cancel_admission_invoice
    invoice = AdmissionInvoice.find(params[:invoice_id])
    doc = invoice.emisor_fiscal_information
    proveedor = Comprobante.new()
    pem = Dir.pwd + "/uploads/clinic_info/" + doc.id.to_s + "/certificate_key/key.pem"
    certificado =  %x'openssl x509 -inform DER -in #{doc.certificate_stamp.current_path.to_s}'
    resultado =  proveedor.cancela invoice.UUID, doc.rfc,pem, certificado.to_s
    if resultado != nil
      if resultado[:message] == "1"
        invoice.update(xml: resultado[:data],cancel:1)

        result_message = "Factura cancelada con exito"
        result_code = "1"
      else
        result_message = get_cancel_invoice_error resultado[:message]
        result_code = "0"
      end
    else
      result_message = "Ocurrio un error al intentar cancelar la factura."
      result_code = "0"
    end
    respond_to do |format|
      format.html { render json: {:message => result_message, :result => result_code} }
    end
  end


  def numero_a_palabras(numero)
    de_tres_en_tres = numero.to_i.to_s.reverse.scan(/\d{1,3}/).map{|n| n.reverse.to_i}

    millones = [
        {true => nil, false => nil},
        {true => 'millón', false => 'millones'},
        {true => "billón", false => "billones"},
        {true => "trillón", false => "trillones"}
    ]

    centena_anterior = 0
    contador = -1
    palabras = de_tres_en_tres.map do |numeros|
      contador += 1
      if contador%2 == 0
        centena_anterior = numeros
        [centena_a_palabras(numeros), millones[contador/2][numeros==1]].compact if numeros > 0
      elsif centena_anterior == 0
        [centena_a_palabras(numeros), "mil", millones[contador/2][false]].compact if numeros > 0
      else
        [centena_a_palabras(numeros), "mil"] if numeros > 0
      end
    end

    palabras.compact.reverse.join(' ')
  end
  def centena_a_palabras(numero)
    especiales = {
        11 => 'once', 12 => 'doce', 13 => 'trece', 14 => 'catorce', 15 => 'quince',
        10 => 'diez', 20 => 'veinte', 100 => 'cien'
    }
    if especiales.has_key?(numero)
      return especiales[numero]
    end

    centenas = [nil, 'ciento', 'doscientos', 'trescientos', 'cuatrocientos', 'quinientos', 'seiscientos', 'setecientos', 'ochocientos', 'novecientos']
    decenas = [nil, 'dieci', 'veinti', 'treinta', 'cuarenta', 'cincuenta', 'sesenta', 'setenta', 'ochenta', 'noventa']
    unidades = [nil, 'un', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve']

    centena, decena, unidad = numero.to_s.rjust(3,'0').scan(/\d/).map{|i| i.to_i}

    palabras = []
    palabras << centenas[centena]

    if especiales.has_key?(decena*10 + unidad)
      palabras << especiales[decena*10 + unidad]
    else
      tmp = "#{decenas[decena]}#{' y ' if decena > 2 && unidad > 0}#{unidades[unidad]}"
      palabras << (tmp.blank? ? nil : tmp)
    end

    palabras.compact.join(' ')
  end
  helper_method :numero_a_palabras
  def download_admission_invoice_xml
    inv = AdmissionInvoice.find(params[:id])
    xml_data = inv.xml
    respond_to do |format|
      format.xml { send_data render_to_string(xml: xml_data) ,:filename => 'Factura '+inv.serie+'-'+inv.folio.to_s+'.xml', :type=>"application/xml", :disposition => 'attachment' }
    end
  end


  private



end