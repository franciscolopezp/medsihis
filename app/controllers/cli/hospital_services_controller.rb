class Cli::HospitalServicesController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:services_report]
  before_action :set_hospital_service, only: [:show, :edit, :update, :destroy]
  include Cli::HospitalServicesHelper

  def index
    respond_to do |format|
      format.html
      format.json { render json: HospitalServicesDatatable.new(view_context)}
    end
  end

  def new
    @hospital_service= HospitalService.new
  end

  def create
    @hospital_service = HospitalService.new(hospital_service_params)
    respond_to do |format|
      if @hospital_service.save
        if params[:is_fixed_cost] != nil
          @hospital_service.update(is_fixed_cost:1)
        else
          @hospital_service.update(is_fixed_cost:0)
        end
        @hospital_service.update(user_id:current_user.id,is_user:1)
        format.html { redirect_to cli_hospital_services_url, notice: t('controller.record_created') }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end

  def services_report

  end

  def export_services
    @initial_date = params[:fecha_ini].to_date
    @end_date = params[:fecha_f].to_date
    @export_info = get_all_services
    filename = "Reporte_servicios_"+params[:fecha_ini]+"_a_"+params[:fecha_f]+".xls"
    respond_to do |format|
      format.xlsx {render xlsx: 'export_services',filename: filename}
    end
  end

  def search_services
    all_services = get_all_services
    respond_to do |format|
      format.html { render json: all_services}
    end
  end

  def update
    respond_to do |format|
      if @hospital_service.update(hospital_service_params)
        if params[:is_fixed_cost] != nil
          @hospital_service.update(is_fixed_cost:1,user_id:current_user.id)
        else
          @hospital_service.update(is_fixed_cost:0,user_id:current_user.id)
        end
        format.html { redirect_to cli_hospital_services_url, notice: t('controller.record_updated') }
      else
        format.html { render :new }
      end
    end
  end

  def destroy

    if @hospital_service.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'servicio Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_hospital_service
    @hospital_service = HospitalService.find(params[:id])
  end


  def hospital_service_params
    params.require(:hospital_service).permit(:code, :name,:cost,:hospital_service_category_id)
  end

end