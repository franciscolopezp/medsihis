class Cli::MedicalAnnotationImagesController < ApplicationController

  def create
    annotation_image = MedicalAnnotationImage.new(medical_annotation_image_params)
    if annotation_image.save
      redirect_to edit_admin_medical_annotation_type_path(params[:medical_annotation_image][:medical_annotation_type_id])
    end
  end

  def download
    annotation_image = MedicalAnnotationImage.find(params[:id])
    if is_image(annotation_image.image.url)
      send_file annotation_image.image.path, :disposition => 'inline'
    else
      send_file annotation_image.image.path
    end
  end

  def is_image(image)
    ext = File.extname(image)
    ['.png','.jpg','.jpeg','.bmp'].include? ext
  end

  private
  def medical_annotation_image_params
    params.require(:medical_annotation_image).permit(:medical_annotation_type_id,:f_key, :image)
  end
end
