class Cli::HospitalAdmissionsController < ApplicationController
  #load_and_authorize_resource
  #skip_authorize_resource :only => [:create,:update]
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit,:open_invoice_admission]
  include Doctor::DailyCashesHelper
  include Doctor::FINKOK
  before_action :set_hospital_admission, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: HospitalAdmissionsDatatable.new(view_context)}
    end
  end

  def closed_admission
    respond_to do |format|
      format.html
      format.json { render json: HospitalAdmissionsDatatable.new(view_context)}
    end
  end

  def new
    @hospital_admission= HospitalAdmission.new
  end


   def delete_service
     service_charge = ServiceCharge.find(params[:id])
     service_charge.destroy
     flash[:success] = "Se eliminó el servicio de manera exitosa"
     render json: {:done => true}
   end

  def get_payments
    admission = HospitalAdmission.find(params[:id])
    payments = []
    admission.medicament_assortments.each do |payment|
      payments.push({
          :id => payment.id,
          :amount => payment.residue,
          :details => "Surtido de medicamentos"
                    })
    end
    admission.service_charges.each do |payment|
      payments.push({
                        :id => payment.id,
                        :amount => payment.residue,
                        :details => payment.hospital_service.name
                    })
    end
    respond_to do |format|
      format.html { render json: {:payments => payments,:done => true} }
    end
  end
  def close_admissions
    total_count = 0
    HospitalAdmission.where("is_closed = 0").each do |adm|
      total = 0
      discount_balance = 0
      total_paid = 0
      adm.medicament_assortments.each do |ic|
        total += ic.cost
        discount_balance += ic.discount
      end
      adm.service_charges.each do |ic|
        total += ic.cost
        discount_balance += ic.discount
      end
      adm.admission_transactions.where("is_cancel = 0").each do |ic|
        total_paid += ic.amount
      end
      total_debt = (total-discount_balance) - total_paid
      adm.update(total:total,payment:total_paid,debt:total_debt)
      
      if adm.debt <= 0 && adm.total > 0
        adm.update(is_closed:1)
        total_count += 1
      end
    end
    respond_to do |format|
      format.html { render json: {:done => true, :count => total_count} }
    end
  end
  def open_admission
    response = false
    admission = HospitalAdmission.find(params[:id])
    if admission != nil
      admission.update(is_closed:0)
      response = true
    end
    respond_to do |format|
      format.html { render json: {:done => response} }
    end
  end
  def get_services_select
    services = []
    HospitalService.all.each do |info|
      services.push({
                           :id => info.id,
                           :name => info.name,
                           :type => "service"
                       })
    end
    ServicePackage.all.each do |info|
      services.push({
                        :id => info.id,
                        :name => info.name,
                        :type => "package"
                       })
    end
    respond_to do |format|
      format.html { render json: services}
    end
  end
  def open_invoice_admission
    @admission = HospitalAdmission.find(params[:id])
  end
  def stamp_admission_invoice
    fiscal_information = EmisorFiscalInformation.find(params[:emisor_id])
    certificadobase64 = fiscal_information.certificate_base64
    noCertificado = fiscal_information.certificate_number

    payment_way = PaymentWay.find(params[:payment_type])
    actual_time = Time.now
    next_folio = fiscal_information.folio + 1
    b = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.send(:'cfdi:Comprobante',
               'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
               'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
               'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
               'TipoCambio'=>"0.00",
               'Moneda'=>"PESOS",
               'version'=>"3.2",
               'serie'=>fiscal_information.serie,
               'folio'=>next_folio.to_s,
               'fecha'=>actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
               'sello'=>"",
               'formaDePago'=>payment_way.name,
               'noCertificado'=>noCertificado,
               'certificado'=>certificadobase64,
               'condicionesDePago'=>"Contado",
               'subTotal'=>params[:sub_total],
               'descuento'=>"0",
               'total'=>params[:amount],
               'metodoDePago'=>params[:payment_method],
               'tipoDeComprobante'=>"ingreso",
               'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
               'LugarExpedicion'=>params[:country].upcase,
               'NumCtaPago'=>params[:account_number]){
        xml.send(:'cfdi:Emisor','rfc'=>fiscal_information.rfc, 'nombre'=>fiscal_information.bussiness_name){
          xml.send(:'cfdi:DomicilioFiscal','calle'=>fiscal_information.fiscal_address, 'noExterior'=>fiscal_information.ext_number, 'noInterior'=>fiscal_information.int_number, 'colonia'=>fiscal_information.suburb, 'municipio'=>fiscal_information.locality, 'estado'=>fiscal_information.city.state.name, 'pais'=>fiscal_information.city.state.country.name, 'codigoPostal'=>fiscal_information.zip){
            xml.text('')
          }
          xml.send(:'cfdi:RegimenFiscal','Regimen'=>fiscal_information.tax_regime)
        }
        xml.send(:'cfdi:Receptor','rfc'=>params[:rfc], 'nombre'=>params[:business_name] ){
          xml.send('cfdi:Domicilio','calle'=>params[:address], 'noExterior'=>params[:ext_number], 'noInterior'=>params[:int_number], 'colonia'=>params[:suburb], 'municipio'=>params[:locality], 'pais'=>params[:country], 'codigoPostal'=>params[:zip])
        }
        xml.send(:'cfdi:Conceptos'){
          params[:conceptos].each do |idx, item|
            xml.send(:'cfdi:Concepto','cantidad'=>1, 'unidad'=>"No aplica", 'noIdentificacion'=>"1", 'descripcion'=>item['details'], 'valorUnitario'=>item['amount'], 'importe'=>item['amount'])
          end
        }
        xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>params[:isr], 'totalImpuestosTrasladados'=>params[:importeisr]){
          xml.send(:'cfdi:Traslados'){
            xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>params[:iva],'importe'=>params[:importeiva])
          }
        }
        xml.send(:'cfdi:Complemento')


      }
    end
    q = 0
    dir = Rails.root.join('uploads',fiscal_information.rfc)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    xml_name = 'xml_base_open.xml'
    File.open(dir.join(xml_name), 'wb') do |file|
      file.write(b.to_xml)
    end



    xml_base_open_invoice = dir.join(xml_name)


    ruta_cadena_original = Dir.pwd + "/uploads/" + fiscal_information.rfc + "/cadenaoriginal.txt"
    file_pem_root = Dir.pwd + "/uploads/clinic_info/" + fiscal_information.id.to_s + "/certificate_key/key.pem"
    %x' xsltproc cadenaoriginal_3_2.xslt #{xml_base_open_invoice} > #{ruta_cadena_original}'


    sello =  %x' openssl dgst -sha1 -sign #{file_pem_root} #{ruta_cadena_original} | openssl enc -base64'
    selloarreglo = sello.split("\n")
    sello = selloarreglo.join.to_s
    a = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.send(:'cfdi:Comprobante',
               'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
               'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
               'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
               'TipoCambio'=>"0.00",
               'Moneda'=>"PESOS",
               'version'=>"3.2",
               'serie'=>fiscal_information.serie,
               'folio'=>next_folio.to_s,
               'fecha'=>actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
               'sello'=>sello,
               'formaDePago'=>payment_way.name,
               'noCertificado'=>noCertificado,
               'certificado'=>certificadobase64,
               'condicionesDePago'=>"Contado",
               'subTotal'=>params[:sub_total],
               'descuento'=>"0",
               'total'=>params[:amount],
               'metodoDePago'=>params[:payment_method],
               'tipoDeComprobante'=>"ingreso",
               'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
               'LugarExpedicion'=>params[:country].upcase,
               'NumCtaPago'=>params[:account_number]){
        xml.send(:'cfdi:Emisor','rfc'=>fiscal_information.rfc, 'nombre'=>fiscal_information.bussiness_name){
          xml.send(:'cfdi:DomicilioFiscal','calle'=>fiscal_information.fiscal_address, 'noExterior'=>fiscal_information.ext_number, 'noInterior'=>fiscal_information.int_number, 'colonia'=>fiscal_information.suburb, 'municipio'=>fiscal_information.locality, 'estado'=>fiscal_information.city.state.name, 'pais'=>fiscal_information.city.state.country.name, 'codigoPostal'=>fiscal_information.zip){
            xml.text('')
          }
          xml.send(:'cfdi:RegimenFiscal','Regimen'=>fiscal_information.tax_regime)
        }
        xml.send(:'cfdi:Receptor','rfc'=>params[:rfc], 'nombre'=>params[:business_name] ){
          xml.send('cfdi:Domicilio','calle'=>params[:address], 'noExterior'=>params[:ext_number], 'noInterior'=>params[:int_number],'colonia'=>params[:suburb], 'municipio'=>params[:locality], 'pais'=>params[:country], 'codigoPostal'=>params[:zip])
        }
        xml.send(:'cfdi:Conceptos'){
          params[:conceptos].each do |idx, item|
            xml.send(:'cfdi:Concepto','cantidad'=>1, 'unidad'=>"No aplica", 'noIdentificacion'=>"1", 'descripcion'=>item['details'], 'valorUnitario'=>item['amount'], 'importe'=>item['amount'])
          end
        }
        xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>params[:isr], 'totalImpuestosTrasladados'=>params[:importeisr]){
          xml.send(:'cfdi:Traslados'){
            xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>params[:iva],'importe'=>params[:importeiva])
          }
        }
        xml.send(:'cfdi:Complemento')


      }
    end
    dir = Rails.root.join('uploads',fiscal_information.rfc)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    xml_name = 'xml_base_open.xml'
    File.open(dir.join(xml_name), 'wb') do |file|
      file.write(a.to_xml)
    end



    #timbra el xml

    # definir el entorno para decidir que webservice utilizar pruebas/produccion
    proveedor = Comprobante.new()
    xml = File.read(xml_base_open_invoice)
    timbrada = proveedor.timbra(xml)
    resultado = timbrada
    qr_code_local_route = "/uploads/" + fiscal_information.rfc + "/qr"+actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S')+".png"
    if resultado[:message] == "1"
      @new_open_invoice = AdmissionInvoice.new(hospital_admission_id:params[:hospital_admission_id],user_id:current_user.id,emisor_fiscal_information_id:fiscal_information.id,xml_base: dir.join(xml_name) , serie: fiscal_information.serie, folio: next_folio,
                                      certification_date: actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
                                      payment_condition: payment_way.name, account_number: params[:account_number], subtotal: params[:sub_total].to_f, iva: params[:importeiva].to_f,
                                      isr: params[:importeisr], invoice_type:3,payment_way_id:payment_way.id)
      @new_open_invoice.save
      @new_open_invoice_receptor = AdmissionInvoiceReceptor.new(business_name: params[:business_name], rfc:params[:rfc],address:params[:address], suburb:params[:suburb], no_int:params[:int_number], no_ext:params[:ext_number], zip: params[:zip],
                                                           locality: params[:locality], city:params[:city], country:params[:country], state: params[:state], admission_invoice_id: @new_open_invoice.id )
      @new_open_invoice_receptor.save

      AdmissionInvoiceEmisor.new(business_name: fiscal_information.bussiness_name, rfc:fiscal_information.rfc,address:fiscal_information.fiscal_address, suburb:fiscal_information.suburb, no_int:fiscal_information.int_number, no_ext:fiscal_information.ext_number, zip: fiscal_information.zip,
                        locality: fiscal_information.locality, city:fiscal_information.city.name, country:fiscal_information.city.state.country.name, state: fiscal_information.city.state.name, admission_invoice_id: @new_open_invoice.id,tax_regime: fiscal_information.tax_regime ).save

      params[:conceptos].each do |idx, item|
        AdmissionInvoiceConcept.new(admission_invoice_id: @new_open_invoice.id, quantity: 1, description: item['details'],unit:"No aplica", unit_price: item['amount'], cost: item['amount'],measure_unit_id:1 ).save
      end

      cadena_original_text = ""
      File.open(ruta_cadena_original, "r") do |f|
        f.each_line do |line|
          cadena_original_text += line
        end
      end
      @new_open_invoice.update(original_string: ruta_cadena_original,original_string_text:cadena_original_text)
      @new_open_invoice.update(UUID: resultado[:uuid], sat_seal: resultado[:timbre], no_cert_sat: resultado[:no_certificado_sat],xml: resultado[:xml],transmitter_seal: sello,qr_code: qr_code_local_route)

      fiscal_information.update(folio: next_folio)
      total_qr = params[:amount]
      total_qr = "%.6f" %total_qr
      total_qr = total_qr.rjust(17,'0')
      cadena_qr =  "?re="+fiscal_information.rfc+"&rr="+params[:rfc]+"&tt="+total_qr+"&id="+resultado[:uuid]
      create_qr(cadena_qr,dir,"qr"+actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S')+".png")
      respond_to do |format|
        format.html { render json:{:done => true, :invoice => @new_open_invoice.id}}
      end

    else
      error_response = get_error_message resultado[:message]
      respond_to do |format|
        format.html { render json:{:done => false, :error_response => error_response} }
      end

    end

  end
  def print
    @clinic = ClinicInfo.find(1)
    if params[:type] == "global"
      transactions = AdmissionTransaction.where('hospital_admission_id = ?', params[:item_id])
    else
      transactions = [AdmissionTransaction.find(params[:item_id])]
    end
    @total = 0
    transactions.each do |t|
      @total += t.amount
    end
    @concept = @clinic.set_custom_ticket_description ? @clinic.ticket_description : transactions.map {|t| t.get_concept}.join(", ")
    render layout: false
  end

  def send_invoice_email
    inv = AdmissionTransaction.find(params[:invoice_id]).clinic_invoice
    pdf_name = "Factura_"+inv.patient_fiscal_information.rfc+"_"+inv.certification_date
      pdfs = render_to_string pdf: pdf_name,  locals: { :@invoice_data => inv }, template: 'cli/hospital_admissions/template_invoice.html.erb', layout: false,
                              margin: { top: 5, bottom: 20, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'cli/hospital_admissions/template_invoice_footer.html.erb', locals: {}}, center: 'TEXT',
                                                                                                                 spacing:           1,
                                                                                                                 line:              true
          }

    emaillist = params[:email]
    if params[:emailp] != ""
      emaillist += ";" + params[:emailp]
    end
    if params[:emailf1] != ""
      emaillist += ";" + params[:emailf1]
    end
    if params[:emailf2] != ""
      emaillist += ";" + params[:emailf2]
    end
    if params[:extramails] != ""
      emaillist += ";" +params[:extramails]
    end
    SendInvoice.send_invoice(emaillist,pdfs,pdf_name,inv).deliver_now

    render :json => true, layout: false


  end
  def get_receptor_email
    emails = []
    email = ""
    t = AdmissionTransaction.find(params[:invoice_id]).clinic_invoice
    if t != nil
      if t.patient_fiscal_information != nil
        if t.patient_fiscal_information.patient.email.present?
          email = t.patient_fiscal_information.patient.email
          emails.push({
                          number:0,
                          email:t.patient_fiscal_information.patient.email
                      })
        end
        if t.patient_fiscal_information.email.present?
          emails.push({
                          number:1,
                          email:t.patient_fiscal_information.email
                      })

        end
        if t.patient_fiscal_information.email2.present?
          emails.push({
                          number:2,
                          email:t.patient_fiscal_information.email2
                      })

        end
      end
    end

    respond_to do |format|
      format.html { render json: {:emails => emails} }
    end

  end
  def get_emisor_info
    result = false
    if params[:id].to_i == 0
      info = EmisorFiscalInformation.first
    else
      info = EmisorFiscalInformation.find(params[:id])
    end
    if info != nil
      doc_info = []
      if info.certificate_key.file != nil  && info.certificate_stamp.file != nil
        result = true
      else
        response_message =   "No se encontraron documentos fiscales: .cer y .key. Es necesario agregarlos en su información fiscal para poder emitir facturas."
      end
      doc_info.push({
                        :business_name => info.bussiness_name,
                        :rfc => info.rfc,
                        :address => info.fiscal_address,
                        :suburb => info.suburb,
                        :locality => info.locality,
                        :ext_number => info.ext_number,
                        :int_number => info.int_number,
                        :zip => info.zip,
                        :city => info.city.name,
                        :state => info.city.state.name,
                        :country => info.city.state.country.name,
                        :serie => info.serie,
                        :folio => info.folio,
                        :iva => info.iva,
                        :isr => info.isr,
                        :id => info.id
                    })
    else
      response_message = "No se tiene información fiscal registradas.";
    end

    respond_to do |format|
      format.html { render json: {:done => result, :data => doc_info,:msg => response_message}}
    end
  end
  def cancel_invoice
    invoice = AdmissionTransaction.find(params[:invoice_id]).clinic_invoice
    doc = EmisorFiscalInformation.first
    proveedor = Comprobante.new()
    pem = Dir.pwd + "/uploads/clinic_" + doc.id.to_s + "/certificate_key/key.pem"
    certificado =  %x'openssl x509 -inform DER -in #{doc.certificate_stamp.current_path.to_s}'
    resultado =  proveedor.cancela invoice.UUID, doc.rfc,pem, certificado.to_s
    if resultado != nil
      if resultado[:message] == "1"
        invoice.update(xml: resultado[:data],cancel:1)
          invoice.admission_transactions.each { |x|
            transac = AdmissionTransaction.find(x)
            transac.update(is_invoiced: 0)
          }
        result_message = "Factura cancelada con exito"
        result_code = "1"
      else
        result_message = get_cancel_invoice_error resultado[:message]
        result_code = "0"
      end
    else
      result_message = "Ocurrio un error al intentar cancelar la factura."
      result_code = "0"
    end
    respond_to do |format|
      format.html { render json: {:message => result_message, :result => result_code} }
    end
  end

  def get_residue
    residue = 0
    if params[:type] == "assortment"
      medicament_assorment = MedicamentAssortment.find(params[:id])
      residue = medicament_assorment.residue
    elsif params[:type] == "service"
      service_charge = ServiceCharge.find(params[:id])
      residue = service_charge.residue
    else

    end
    respond_to do |format|
      format.json { render :json => {:residue => residue}  }
    end
  end
  def update_service_discount
    if params[:type] == "service"
      service_charge = ServiceCharge.find(params[:service_id])
    else
      service_charge = MedicamentAssortment.find(params[:service_id])
    end
    hospital_admission = service_charge.hospital_admission
    current_debt = hospital_admission.debt
    current_residue = service_charge.residue

    updated_residue = service_charge.cost - params[:discount].to_f
    service_charge.update(discount:params[:discount].to_f,residue:updated_residue)
    hospital_admission.update(debt: current_debt + updated_residue - current_residue)
    respond_to do |format|
      format.json { render :json => {:done => true}  }
    end
  end

  def generate_cost
    hospital_admission = HospitalAdmission.find(params[:admission])
    response = false
    if params[:type] == 'service'
      service_charge  = ServiceCharge.new
      service_charge.cost = params[:cost].to_f
      service_charge.observations = params[:observations]
      service_charge.hospital_service = HospitalService.find(params[:service])
      service_charge.hospital_admission = hospital_admission
      service_charge.user = current_user
      service_charge.is_paid = false
      service_charge.discount = 0
      service_charge.residue = params[:cost].to_f
      if service_charge.save
        updated_total = hospital_admission.total + service_charge.cost
        updated_debt = hospital_admission.debt + service_charge.cost
        hospital_admission.update(total:updated_total, debt:updated_debt)
        response = true
      end
    else
      ServicePackage.find(params[:service]).package_service_costs.each do |service|
        service_charge  = ServiceCharge.new
        service_charge.cost = service.cost
        service_charge.observations = params[:observations]
        service_charge.hospital_service = service.hospital_service
        service_charge.hospital_admission = hospital_admission
        service_charge.user = current_user
        service_charge.is_paid = false
        service_charge.discount = 0
        service_charge.residue = service.cost
        if service_charge.save
          updated_total = hospital_admission.total + service_charge.cost
          updated_debt = hospital_admission.debt + service_charge.cost
          hospital_admission.update(total:updated_total, debt:updated_debt)
          response = true
        end
      end
    end

    respond_to do |format|
      format.json { render :json => {:done => response}  }
    end
  end

  def get_service_cost
    cost = 0
    if params[:type] == 'service'
      hospital_service = HospitalService.find(params[:service])
      cost = hospital_service.cost
      if params[:admission].to_i == 1
        patient = Patient.find(params[:patient])
        if patient.insurance != nil
          if patient.insurance.id != 1
            exist_record = InsurencePrice.where("insurance_id = #{patient.insurance.id} AND hospital_service_id = #{hospital_service.id}").first
            if exist_record != nil
              cost = exist_record.price
            end
          end
        end
      end
      is_fixed_cost = hospital_service.is_fixed_cost
    else
      ServicePackage.find(params[:service]).package_service_costs.each do |service|
        cost += service.cost
        is_fixed_cost = false
      end
    end



    respond_to do |format|
      format.json { render :json => {:cost => cost,:is_fixed_cost => is_fixed_cost}  }
    end
  end

  def generate_discount
    HospitalAdmissionDiscount.create(hospital_admission_id:params[:hospital_admission_id], amount:params[:amount], user:current_user)
    admission = HospitalAdmission.find(params[:hospital_admission_id])
    admission.update(debt: admission.debt - params[:amount].to_f)
    render json: {:done => true}
  end


  def get_nex_folio_ticket
    clinic = ClinicInfo.find(1)
    series = clinic.ticket_series
    next_folio = clinic.ticket_next_folio
    new_folio = series+"-"+next_folio.to_s
    clinic.update(ticket_next_folio: next_folio + 1)
    return new_folio
  end
  def close_admission
    hospital_admission = HospitalAdmission.find(params[:id])
    hospital_admission.update(is_closed:1)
    respond_to do |format|
      format.json { render :json => {:done => true}  }
    end
  end
  def cancel_payment_admission
    response = false
    transaction_to_cancel = AdmissionTransaction.find(params[:admission_transaction_id])
    other_movement = OtherMovement.new
    other_movement.movement_type = MovementType.find(22)
    other_movement.details = "Cancelación de pago. Folio: " + transaction_to_cancel.folio
    #Transacion 22
    if other_movement.save
      transaction_to_cancel.update(is_cancel:1)
      response = true
      amounts = params[:amounts]
      amounts.each do |idx, val|
        data = Hash.new
        data['chargeable'] = other_movement
        data['description'] = "Cancelación de pago. Folio: " + transaction_to_cancel.folio
        data['t_type'] = params[:t_type]
        data['account_id'] = transaction_to_cancel.transactions.first.accounts.first.id
        data['transaction_type'] = params[:transaction_type_id]
        data['payment_method'] = transaction_to_cancel.transactions.first.payment_method.id
        amount = val[:amount].to_f
        currency_id = transaction_to_cancel.transactions.first.currency.id
        ex = CurrencyExchange.find(transaction_to_cancel.transactions.first.currency.id)
        data['currency_id'] = transaction_to_cancel.transactions.first.currency.id
        data['currency_exchange'] = ex.value
        data['amount'] = amount
        generate_transaction data
      end

    end
    respond_to do |format|
      format.json { render :json => {:done => response}  }
    end
  end
  def generate_payment
    response = false
    hospital_admission = HospitalAdmission.find(params[:hospital_admission_id])
    admission_transaction = AdmissionTransaction.new
    admission_transaction.folio = get_nex_folio_ticket
    admission_transaction.amount = params[:amount].to_f
    admission_transaction.user = current_user
    admission_transaction.is_cancel = false
    admission_transaction.hospital_admission = hospital_admission
    if admission_transaction.save
      response = true
      update_debt = hospital_admission.debt - admission_transaction.amount
      updated_payment = hospital_admission.payment + admission_transaction.amount
      hospital_admission.update(debt:update_debt,payment:updated_payment)

      #Transacion
      amounts = params[:amounts]
      amounts.each do |idx, val|
        data = Hash.new
        data['chargeable'] = admission_transaction
        data['description'] = params[:details]
        data['t_type'] = params[:t_type]
        data['account_id'] = params[:account_id]
        data['transaction_type'] = params[:transaction_type_id]
        data['payment_method'] = params[:payment_method_id]

        amount = val[:amount].to_f
        currency_id = val[:currency].to_i
        ex = CurrencyExchange.find(val[:currency].to_i)

        data['currency_id'] = currency_id
        data['currency_exchange'] = ex.value
        data['amount'] = amount
        generate_transaction data

      end
    end
    respond_to do |format|
      format.json { render :json => {:done => response}  }
    end
  end

  def create
    @hospital_admission = HospitalAdmission.new(hospital_admission_params)
    respond_to do |format|
      if @hospital_admission.save
        @hospital_admission.update(user_id:current_user.id, is_closed:0, total:0,debt:0, payment:0)
        format.html { redirect_to cli_hospital_admissions_url,notice: t('controller.record_created') }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @hospital_admission.update(hospital_admission_params)
        format.html { redirect_to cli_hospital_admissions_url, notice: t('controller.record_updated') }
      else
        format.html { render :new }
      end
    end
  end

  def show

  end

  def destroy

    if @hospital_admission.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end
  def stamp
    doc = ClinicInfo.first
    if doc.emisor_fiscal_information.certificate_base64 != nil || doc.emisor_fiscal_information.certificate_base64 != ""
      certificadobase64 = doc.emisor_fiscal_information.certificate_base64
    else
      certificado =  %x'openssl x509 -inform DER -in #{doc.emisor_fiscal_information.certificate_stamp.current_path.to_s}'
      arrayc =  certificado.split("\n")
      arrayc.shift
      arrayc.pop
      certificadobase64 = arrayc.join.to_s
      doc.emisor_fiscal_information.update(certificate_base64: certificadobase64)
    end

    if doc.emisor_fiscal_information.certificate_number == nil || doc.emisor_fiscal_information.certificate_number == ""
      #Obtiene el numero de serie del certificado y lo convierte al numero.
      certificado =  %x'openssl x509 -serial -noout -inform DER -in #{doc.emisor_fiscal_information.certificate_stamp.current_path.to_s}'
      arreglonumero = certificado.to_s.split('=')
      arreglosolonumero = arreglonumero[1].to_s.split(//)
      serialNumberCer = []
      (1..39).step(2) do |n|
        serialNumberCer.push(arreglosolonumero[n])
      end
      noCertificado = serialNumberCer.join.to_s
      doc.emisor_fiscal_information.update(certificate_number: noCertificado)
    else
      noCertificado = doc.emisor_fiscal_information.certificate_number
    end

    payment_way = PaymentWay.find(params[:payment_type])
    actual_time = Time.now
    next_folio = doc.emisor_fiscal_information.folio + 1
    b = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.send(:'cfdi:Comprobante',
               'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
               'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
               'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
               'TipoCambio'=>"0.00",
               'Moneda'=>"PESOS",
               'version'=>"3.2",
               'serie'=>doc.emisor_fiscal_information.serie,
               'folio'=>next_folio.to_s,
               'fecha'=>actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
               'sello'=>"",
               'formaDePago'=>payment_way.name,
               'noCertificado'=>noCertificado,
               'certificado'=>certificadobase64,
               'condicionesDePago'=>"Contado",
               'subTotal'=>params[:sub_total],
               'descuento'=>"0",
               'total'=>params[:amount],
               'metodoDePago'=>params[:payment_method],
               'tipoDeComprobante'=>"ingreso",
               'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
               'LugarExpedicion'=>params[:city] + ", " + params[:state],
               'NumCtaPago'=>params[:account_number]){
        xml.send(:'cfdi:Emisor','rfc'=>doc.emisor_fiscal_information.rfc, 'nombre'=>doc.emisor_fiscal_information.bussiness_name){
          xml.send(:'cfdi:DomicilioFiscal','calle'=>doc.emisor_fiscal_information.fiscal_address, 'noExterior'=>doc.emisor_fiscal_information.ext_number, 'noInterior'=>doc.emisor_fiscal_information.int_number, 'colonia'=>doc.emisor_fiscal_information.suburb, 'municipio'=>doc.emisor_fiscal_information.locality, 'estado'=>doc.emisor_fiscal_information.city.state.name, 'pais'=>doc.emisor_fiscal_information.city.state.country.name, 'codigoPostal'=>doc.emisor_fiscal_information.zip){
            xml.text('')
          }
          xml.send(:'cfdi:RegimenFiscal','Regimen'=>doc.emisor_fiscal_information.tax_regime)
        }
        xml.send(:'cfdi:Receptor','rfc'=>params[:rfc], 'nombre'=>params[:business_name] ){
          xml.send('cfdi:Domicilio','calle'=>params[:address],'codigoPostal'=>params[:zip], 'noExterior'=>params[:ext_number], 'noInterior'=>params[:int_number], 'colonia'=>params[:suburb],'estado'=>params[:state], 'municipio'=>params[:locality],'pais'=>params[:country] )
        }
        xml.send(:'cfdi:Conceptos'){
          xml.send(:'cfdi:Concepto','cantidad'=>"1.00", 'unidad'=>"No Aplica", 'noIdentificacion'=>"1", 'descripcion'=>params[:descripcion_concepto], 'valorUnitario'=>params[:sub_total], 'importe'=>params[:sub_total])
        }
        xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>params[:isr], 'totalImpuestosTrasladados'=>params[:importeisr]){
          xml.send(:'cfdi:Traslados'){
            xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>params[:iva],'importe'=>params[:importeiva])
          }
        }
        xml.send(:'cfdi:Complemento')


      }
    end

    dir = Rails.root.join('uploads',doc.emisor_fiscal_information.rfc)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    xml_name = 'xml_base.xml'
    File.open(dir.join(xml_name), 'wb') do |file|
      file.write(b.to_xml)
    end

      check_transac = AdmissionTransaction.find(params[:transaction_id])


    xml_base_root = dir.join(xml_name)


    ruta_cadena_original = Dir.pwd + "/uploads/" + doc.emisor_fiscal_information.rfc + "/cadenaoriginal.txt"
    file_pem_root = Dir.pwd + "/uploads/clinic_1" + "/certificate_key/key.pem"
    %x' xsltproc cadenaoriginal_3_2.xslt #{xml_base_root} > #{ruta_cadena_original}'


    sello =  %x' openssl dgst -sha1 -sign #{file_pem_root} #{ruta_cadena_original} | openssl enc -base64'
    selloarreglo = sello.split("\n")
    sello = selloarreglo.join.to_s
    a = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
      xml.send(:'cfdi:Comprobante',
               'xmlns:xsi'=>"http://www.w3.org/2001/XMLSchema-instance",
               'xmlns:cfdi' => "http://www.sat.gob.mx/cfd/3",
               'xmlns:terceros'=>"http://www.sat.gob.mx/terceros",
               'TipoCambio'=>"0.00",
               'Moneda'=>"PESOS",
               'version'=>"3.2",
               'serie'=>doc.emisor_fiscal_information.serie,
               'folio'=>next_folio.to_s,
               'fecha'=>actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'),
               'sello'=>sello,
               'formaDePago'=>payment_way.name,
               'noCertificado'=>noCertificado,
               'certificado'=>certificadobase64,
               'condicionesDePago'=>"Contado",
               'subTotal'=>params[:sub_total],
               'descuento'=>"0",
               'total'=>params[:amount],
               'metodoDePago'=>params[:payment_method],
               'tipoDeComprobante'=>"ingreso",
               'xsi:schemaLocation'=>"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/terceros http://www.sat.gob.mx/sitio_internet/cfd/terceros/terceros11.xsd",
               'LugarExpedicion'=>params[:city] + ", " + params[:state],
               'NumCtaPago'=>params[:account_number]){
        xml.send(:'cfdi:Emisor','rfc'=>doc.emisor_fiscal_information.rfc, 'nombre'=>doc.emisor_fiscal_information.bussiness_name){
          xml.send(:'cfdi:DomicilioFiscal','calle'=>doc.emisor_fiscal_information.fiscal_address, 'noExterior'=>doc.emisor_fiscal_information.ext_number, 'noInterior'=>doc.emisor_fiscal_information.int_number, 'colonia'=>doc.emisor_fiscal_information.suburb, 'municipio'=>doc.emisor_fiscal_information.locality, 'estado'=>doc.emisor_fiscal_information.city.state.name, 'pais'=>doc.emisor_fiscal_information.city.state.country.name, 'codigoPostal'=>doc.emisor_fiscal_information.zip){
            xml.text('')
          }
          xml.send(:'cfdi:RegimenFiscal','Regimen'=>doc.emisor_fiscal_information.tax_regime)
        }
        xml.send(:'cfdi:Receptor','rfc'=>params[:rfc], 'nombre'=>params[:business_name] ){
          xml.send('cfdi:Domicilio','calle'=>params[:address],'codigoPostal'=>params[:zip],'noExterior'=>params[:ext_number], 'noInterior'=>params[:int_number], 'colonia'=>params[:suburb],'estado'=>params[:state], 'municipio'=>params[:locality],'pais'=>params[:country])
        }
        xml.send(:'cfdi:Conceptos'){
          xml.send(:'cfdi:Concepto','cantidad'=>"1.00", 'unidad'=>"No Aplica", 'noIdentificacion'=>"1", 'descripcion'=>params[:descripcion_concepto], 'valorUnitario'=>params[:sub_total], 'importe'=>params[:sub_total])
        }
        xml.send(:'cfdi:Impuestos','totalImpuestosRetenidos'=>params[:isr], 'totalImpuestosTrasladados'=>params[:importeisr]){
          xml.send(:'cfdi:Traslados'){
            xml.send(:'cfdi:Traslado', 'impuesto'=>"IVA",'tasa'=>params[:iva],'importe'=>params[:importeiva])
          }
        }
        xml.send(:'cfdi:Complemento')


      }
    end
    dir = Rails.root.join('uploads',doc.emisor_fiscal_information.rfc)
    FileUtils.mkdir_p(dir) unless File.directory?(dir)

    xml_name = 'xml_base.xml'
    File.open(dir.join(xml_name), 'wb') do |file|
      file.write(a.to_xml)
    end



    #timbra el xml

    proveedor = Comprobante.new()
    xml = File.read(xml_base_root)
    timbrada = proveedor.timbra(xml)
    resultado = timbrada
    qr_code_local_route = "/uploads/" + doc.emisor_fiscal_information.rfc + "/qr"+actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S')+".png"

    if resultado[:message] == "1"
        @new_invoice = ClinicInvoice.new(xml_base: xml_base_root ,serie: doc.emisor_fiscal_information.serie, folio: next_folio,
                                   certification_date: actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S'), payment_condition: payment_way.name,
                                   account_number: params[:account_number], subtotal: params[:sub_total], iva: params[:importeiva], isr: params[:importeisr], patient_fiscal_information_id: params[:patient_fiscal],invoice_type: 1,payment_way_id:payment_way.id)
        @new_invoice.save
        @concept = ClinicInvoiceConcept.new(clinic_invoice_id: @new_invoice.id, quantity: 1,unit:"No aplica", description: params[:descripcion_concepto], unit_price: params[:sub_total], cost: params[:sub_total] )
        @concept.save
        check_transac.update(clinic_invoice_id: @new_invoice.id)

        ClinicInvoiceReceptor.new(business_name: params[:business_name], rfc:params[:rfc],address:params[:address], suburb:params[:suburb], no_int:params[:int_number], no_ext:params[:ext_number], zip: params[:zip],
                                locality: params[:locality], city:params[:city], country:params[:country], state: params[:state], clinic_invoice_id: @new_invoice.id ).save

        ClinicInvoiceEmisor.new(business_name: doc.emisor_fiscal_information.bussiness_name, rfc:doc.emisor_fiscal_information.rfc,address:doc.emisor_fiscal_information.fiscal_address, suburb:doc.emisor_fiscal_information.suburb, no_int:doc.emisor_fiscal_information.int_number, no_ext:doc.emisor_fiscal_information.ext_number, zip: doc.emisor_fiscal_information.zip,
                          locality: doc.emisor_fiscal_information.locality, city:doc.emisor_fiscal_information.city.name, country:doc.emisor_fiscal_information.city.state.country.name, state: doc.emisor_fiscal_information.city.state.name, clinic_invoice_id: @new_invoice.id,tax_regime:doc.emisor_fiscal_information.tax_regime ).save

      cadena_original_text = ""
      File.open(ruta_cadena_original, "r") do |f|
        f.each_line do |line|
          cadena_original_text += line
        end
      end
      @new_invoice.update(original_string: ruta_cadena_original,original_string_text:cadena_original_text)
      @new_invoice.update(UUID: resultado[:uuid], sat_seal: resultado[:timbre], no_cert_sat: resultado[:no_certificado_sat],xml: resultado[:xml],transmitter_seal: sello,qr_code: qr_code_local_route)

      doc.emisor_fiscal_information.update(folio: next_folio)
      total_qr = params[:amount]
      total_qr = "%.6f" %total_qr
      total_qr = total_qr.rjust(17,'0')
      cadena_qr =  "?re="+doc.emisor_fiscal_information.rfc+"&rr="+params[:rfc]+"&tt="+total_qr.to_s+"&id="+resultado[:uuid]
      create_qr(cadena_qr,dir,"qr"+actual_time.strftime('%Y-%m-%d')+"T"+actual_time.strftime('%H:%M:%S')+".png")
      check_transac.update(is_invoiced: 1)
      #send_invoice_after_stamp(@new_invoice)
      respond_to do |format|
        format.html { render json:{:done => true, :invoice => @new_invoice.id}}
      end

    else
      error_response = get_error_message resultado[:message]
      respond_to do |format|
        format.html { render json: error_response }
      end

    end


  end
  def numero_a_palabras(numero)
    de_tres_en_tres = numero.to_i.to_s.reverse.scan(/\d{1,3}/).map{|n| n.reverse.to_i}

    millones = [
        {true => nil, false => nil},
        {true => 'millón', false => 'millones'},
        {true => "billón", false => "billones"},
        {true => "trillón", false => "trillones"}
    ]

    centena_anterior = 0
    contador = -1
    palabras = de_tres_en_tres.map do |numeros|
      contador += 1
      if contador%2 == 0
        centena_anterior = numeros
        [centena_a_palabras(numeros), millones[contador/2][numeros==1]].compact if numeros > 0
      elsif centena_anterior == 0
        [centena_a_palabras(numeros), "mil", millones[contador/2][false]].compact if numeros > 0
      else
        [centena_a_palabras(numeros), "mil"] if numeros > 0
      end
    end

    palabras.compact.reverse.join(' ')
  end

  def centena_a_palabras(numero)
    especiales = {
        11 => 'once', 12 => 'doce', 13 => 'trece', 14 => 'catorce', 15 => 'quince',
        10 => 'diez', 20 => 'veinte', 100 => 'cien'
    }
    if especiales.has_key?(numero)
      return especiales[numero]
    end

    centenas = [nil, 'ciento', 'doscientos', 'trescientos', 'cuatrocientos', 'quinientos', 'seiscientos', 'setecientos', 'ochocientos', 'novecientos']
    decenas = [nil, 'dieci', 'veinti', 'treinta', 'cuarenta', 'cincuenta', 'sesenta', 'setenta', 'ochenta', 'noventa']
    unidades = [nil, 'un', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve']

    centena, decena, unidad = numero.to_s.rjust(3,'0').scan(/\d/).map{|i| i.to_i}

    palabras = []
    palabras << centenas[centena]

    if especiales.has_key?(decena*10 + unidad)
      palabras << especiales[decena*10 + unidad]
    else
      tmp = "#{decenas[decena]}#{' y ' if decena > 2 && unidad > 0}#{unidades[unidad]}"
      palabras << (tmp.blank? ? nil : tmp)
    end

    palabras.compact.join(' ')
  end
  helper_method :numero_a_palabras
  def print_pdf
    inv = AdmissionTransaction.find(params[:id]).clinic_invoice
    respond_to do |format|
      format.pdf do
          render pdf: "Factura_"+inv.patient_fiscal_information.rfc+"_"+inv.certification_date,  locals: { :@invoice_data => inv }, template: 'cli/hospital_admissions/template_invoice.html.erb', layout: false,
                 margin: { top: 5, bottom: 20, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'cli/hospital_admissions/template_invoice_footer.html.erb', locals: {}}, center: 'TEXT',
                                                                                                    spacing:           1,
                                                                                                    line:              true
              }
      end
    end
  end

  def download_xml
    inv = AdmissionTransaction.find(params[:id]).clinic_invoice
      xml_data = inv.xml
      folio = inv.folio
    respond_to do |format|
      format.xml { send_data render_to_string(xml: xml_data) ,:filename => 'Factura'+folio.to_s+'.xml', :type=>"application/xml", :disposition => 'attachment' }
    end
  end
  def create_qr(cadena,ruta,nombre)
    qrcode = RQRCode::QRCode.new(cadena)
# With default options specified explicitly
    png = qrcode.as_png(
        resize_gte_to: false,
        resize_exactly_to: false,
        fill: 'white',
        color: 'black',
        size: 180,
        border_modules: 1,
        module_px_size: 6,
        file: nil # path to write
    )
    FileUtils.mkdir_p(ruta) unless File.directory?(ruta)

    File.open(ruta.join(nombre), 'wb') do |file|
      file.write(png.to_s)
    end
  end
  def get_error_message error_code_stamp

    case error_code_stamp
      when  '300'
        error_message = 'Usuario y contraseña inválidos'
      when '301'
        error_message = 'XML mal formado '
      when '302'
        error_message = 'Sello mal formado'
      when '303'
        error_message = 'Sello no corresponde al emisor'
      when '305'
        error_message ='Fecha de emisión no está dentro de la vigencia del Certificado'
      when '304'
        error_message = 'Certificado revocado o caduco'
      when '401'
        error_message = 'Fecha y hora de generación fuera de rango'
      when '702'
        error_message = 'El prefijo del namespace no es el especificado por el SAT'
      when '705'
        error_message = 'Estructura de XML mal formada (Error de Sintaxis)'
      when '708'
        error_message = 'Error de conexión con el SAT'
      when '712'
        error_message = 'El Certificado de timbre no se pudo recuperar del SAT.'
      else
        error_message =  error_code_stamp + ' Error no especificado'
    end

    return error_message
  end
  def get_cancel_invoice_error cancel_code
    case cancel_code
      when  '202'
        error_message = 'Cancelado Previamente'
      when '203'
        error_message = 'No corresponde el RFC del emisor y de quien solicita la cancelación '
      when '205'
        error_message = 'No existente'
      when '900'
        error_message = 'Error de PAC'
      when '708'
        error_message ='Error de conexión con el SAT'
      else
        error_message =  cancel_code + ' Error no especificado'
    end

    return error_message
  end

  private

  def set_hospital_admission
    @hospital_admission = HospitalAdmission.find(params[:id])
  end


  def hospital_admission_params
    params.require(:hospital_admission).permit(:doctor_id,:patient_id,:triage_id,:attention_type_id,:admission_date)
  end

end