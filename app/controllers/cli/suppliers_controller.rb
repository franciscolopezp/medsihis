class Cli::SuppliersController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  before_action :set_supplier, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: SuppliersDatatable.new(view_context)}
    end
  end


  def new
    @supplier= Supplier.new
  end



  def create
    @supplier = Supplier.new(supplier_params)
    respond_to do |format|
      if @supplier.save
        format.html { redirect_to cli_suppliers_url, notice: 'Proveedor Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @supplier.update(supplier_params)
        format.html { redirect_to cli_suppliers_url, notice: 'Proveedor Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @supplier.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Proveedor Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_supplier
    @supplier = Supplier.find(params[:id])
  end


  def supplier_params
    params.require(:supplier).permit(:name,:telephone,:address,:contact)
  end

end