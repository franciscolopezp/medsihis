class Cli::MedicamentAssortmentsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new]
  include Cli::MedicamentHelper
  before_action :set_medicament_assortment, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicamentAssortmentsDatatable.new(view_context)}
    end
  end

  def show

  end
  def new
    @medicament_assortment = MedicamentAssortment.new
  end

  def save_assortment
    response = false
    medicament_assortment = MedicamentAssortment.new(cost:0,hospital_admission_id:params[:admission],folio:params[:folio],discount:0,
                                                 user_id:current_user.id,warehouse_id:params[:warehouse],residue:0,is_paid:0)
    total_cost = 0
    if medicament_assortment.save
      if params[:pharmacy_items] != nil
      params[:pharmacy_items].each do |idx, item|
        if item['type'] == 'med'
          cost = Medicament.find(item['item_id'].to_i).cost
          medicament_assorment_item = MedicamentAssortmentItem.new(quantity:item['quantity'].to_i,medicament_id:item['item_id'].to_i,lot_id:item['lot'].to_i,cost:cost,medicament_assortment_id:medicament_assortment.id)
          if medicament_assorment_item.save
            total_cost += cost * item['quantity'].to_i
            search_inventory = MedicamentInventory.where("warehouse_id = #{medicament_assortment.warehouse.id} and lot_id = #{item['lot']}")
            inventory_found = search_inventory.first
            update_quantity = inventory_found.quantity - item['quantity'].to_i
            inventory_found.update(quantity:update_quantity)
            update_log(item['lot'].to_i,medicament_assortment.warehouse.id,0,item['quantity'].to_i,current_user.id,medicament_assorment_item)
          end
        else
          cost = PharmacyItem.find(item['item_id'].to_i).cost
          pharmacy_item_assorment = AssortmentPharmacyItem.new(pharmacy_item_id:item['item_id'].to_i,
                                                               quantity:item['quantity'].to_i,cost:cost,
                                                               medicament_assortment_id:medicament_assortment.id,
                                                               warehouse_id:medicament_assortment.warehouse.id)
          if pharmacy_item_assorment.save
            total_cost += cost * item['quantity'].to_i
            item_inventory_founded = PharmacyItemInventory.where("warehouse_id = #{medicament_assortment.warehouse.id} and pharmacy_item_id = #{pharmacy_item_assorment.pharmacy_item.id}" )
            if item_inventory_founded.count > 0
              inventory_phar_founded = item_inventory_founded.first
              updated_quantity_phar_item = inventory_phar_founded.quantity - pharmacy_item_assorment.quantity
              inventory_phar_founded.update(quantity:updated_quantity_phar_item)
            end
            update_pharmacy_log(pharmacy_item_assorment.pharmacy_item.id,medicament_assortment.warehouse.id,0,pharmacy_item_assorment.quantity,current_user.id,pharmacy_item_assorment)
          end
        end
      end
    end
      medicament_assortment.update(cost:total_cost,residue:total_cost)
      update_hospital_admission = medicament_assortment.hospital_admission
      updated_debt = update_hospital_admission.debt + medicament_assortment.cost
      updated_total = update_hospital_admission.total + medicament_assortment.cost
      update_hospital_admission.update(debt:updated_debt,total:updated_total)
      response = true
    end


    render :json => {:done => response}, layout: false
  end
  def get_last_folio
    last_folio = 1
    last_medicament_assortment = MedicamentAssortment.last
    if last_medicament_assortment != nil
      last_folio = last_medicament_assortment.folio + 1
    end
    render :json => {:folio => last_folio}, layout: false
  end
  def get_item_cost
    item = PharmacyItem.find(params[:pharmacy_item_id])
    render :json => {:pharmacy_cost => item.cost}, layout: false
  end
  def get_lot
    lots_a = []
    medicament = Medicament.find(params[:medicament_id])
    lots = Lot.where("medicament_id = #{params[:medicament_id]}")
    lots.each do |lot|
      warehouse_selected = MedicamentInventory.where("lot_id = #{lot.id} and warehouse_id = #{params[:warehouse]}")
      warehouse_selected.each do |war|
        lots_a.push({
                        :id => war.lot.id,
                        :name => war.lot.name,
                        :quantity => war.quantity,
                        :exp_date => war.lot.expiration_date.strftime("%d/%m/%Y")
                    })
      end
    end
    render :json => {:lots => lots_a,:medicament_cost => medicament.cost}, layout: false
  end

  def create
    @medicament_assortment = MedicamentAssortment.new(supplier_params)
    respond_to do |format|
      if @medicament_assortment.save
        format.html { redirect_to cli_medicament_assortments_url, notice: 'surtido Creado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit

  end


  def update
    respond_to do |format|
      if @medicament_assortment.update(medicament_assortment_params)
        format.html { redirect_to cli_medicament_assortments_url, notice: 'Proveedor Actualizado Correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def destroy

    if @medicament_assortment.destroy
      respond_to do |format|
        msg = {:done => true , :message => 'Proveedor Eliminado Correctamente'}
        format.json { render :json => msg  }
      end
    else
      respond_to do |format|
        msg = {:done => false , :message => 'Errores!!!!!!'}
        format.json { render :json => msg  }
      end
    end

  end

  private

  def set_medicament_assortment
    @medicament_assortment = MedicamentAssortment.find(params[:id])
  end


  def medicament_assortment_params
    params.require(:medicament_assortment).permit(:quantity,:cost,:admission_id,:lot_id,:warehouse_id)
  end

end