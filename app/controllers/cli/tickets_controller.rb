class Cli::TicketsController < ApplicationController


  def admission_transaction
    @admission_transaction = AdmissionTransaction.find(params[:id])
    render pdf: "ticket",
           margin: { top: 5, bottom: 0, left: 3, right: 3 },
           page_size: 'Letter',
           :page_height => '3.3in',
           :page_width => '2.28in',
           layout: false,
           encoding: 'UTF-8'
  end

  def other_payments
    @admission_transaction = OtherChargeTransaction.find(params[:id])
    render pdf: "ticket",
           margin: { top: 5, bottom: 1, left: 1, right: 1 },
           page_size: 'Letter',
           :page_height => '4.3in',
           :page_width => '2.28in',
           layout: false,
           encoding: 'UTF-8'
  end
  def movement_ticket
    @transaction = Transaction.find(params[:id])
    render pdf: "ticket",
           margin: { top: 5, bottom: 1, left: 1, right: 1 },
           page_size: 'Letter',
           :page_height => '3.5in',
           :page_width => '2.28in',
           layout: false,
           encoding: 'UTF-8'
  end
end