class Cli::MedicamentMovementsController < ApplicationController
  load_and_authorize_resource :only => [:index, :show,:new,:destroy,:edit]
  include Cli::MedicamentHelper
  before_action :set_medicament_movement, only: [:show, :edit, :update, :destroy]


  def index
    respond_to do |format|
      format.html
      format.json { render json: MedicamentMovementsDatatable.new(view_context)}
    end
  end


  def new
    @medicament_movement= MedicamentMovement.new
  end


  def pharmacy_movements
    @pharmacy_movement= PharmacyItemMovement.new
  end

  def pharmacy_movement_save
    @pharmacy_movement = PharmacyItemMovement.new(pharmacy_movement_params)
    respond_to do |format|
      if @pharmacy_movement.save
        @pharmacy_movement.update(user_id:current_user.id)
        search_inventory_rest = PharmacyItemInventory.where("warehouse_id = #{@pharmacy_movement.from_warehouse.id} and pharmacy_item_id = #{@pharmacy_movement.pharmacy_item.id}")
        founded_inventory_rest = search_inventory_rest.first
        update_quantity_rest = founded_inventory_rest.quantity - @pharmacy_movement.quantity
        founded_inventory_rest.update(quantity:update_quantity_rest)
        update_pharmacy_log(founded_inventory_rest.pharmacy_item.id,founded_inventory_rest.warehouse.id,0,@pharmacy_movement.quantity,current_user.id,@pharmacy_movement)
        search_inventory = PharmacyItemInventory.where("warehouse_id = #{@pharmacy_movement.to_warehouse.id} and pharmacy_item_id = #{@pharmacy_movement.pharmacy_item.id}")
        if search_inventory.count > 0
          founded_inventory = search_inventory.first
          updated_quantity = founded_inventory.quantity + @pharmacy_movement.quantity
          founded_inventory.update(quantity:updated_quantity)
          update_pharmacy_log(founded_inventory.pharmacy_item.id,founded_inventory.warehouse.id,1,@pharmacy_movement.quantity,current_user.id,@pharmacy_movement)
        else
          new_pharmacy_inventory = PharmacyItemInventory.new(warehouse_id:@pharmacy_movement.to_warehouse.id,pharmacy_item_id:@pharmacy_movement.pharmacy_item.id,quantity:@pharmacy_movement.quantity,user_id:current_user.id)
          if new_pharmacy_inventory.save
            update_pharmacy_log(new_pharmacy_inventory.pharmacy_item.id,new_pharmacy_inventory.warehouse.id,1,@pharmacy_movement.quantity,current_user.id,@pharmacy_movement)
          end
        end
        format.html { redirect_to cli_medicament_movements_url, notice: '.' }
      else
        format.html { render :new }
      end
    end

  end
  def create
    @medicament_movement = MedicamentMovement.new(medicament_movement_params)
    respond_to do |format|
      if @medicament_movement.save
        @medicament_movement.update(user_id:current_user.id)
        search_inventory_rest = MedicamentInventory.where("warehouse_id = #{@medicament_movement.from_warehouse.id} and lot_id = #{@medicament_movement.lot.id}")
        founded_inventory_rest = search_inventory_rest.first
        update_quantity_rest = founded_inventory_rest.quantity - @medicament_movement.quantity
        founded_inventory_rest.update(quantity:update_quantity_rest)
        update_log(founded_inventory_rest.lot.id,founded_inventory_rest.warehouse.id,0,@medicament_movement.quantity,current_user.id,@medicament_movement)
        search_inventory = MedicamentInventory.where("warehouse_id = #{@medicament_movement.to_warehouse.id} and lot_id = #{@medicament_movement.lot.id}")
        if search_inventory.count > 0
          founded_inventory = search_inventory.first
          updated_quantity = founded_inventory.quantity + @medicament_movement.quantity
          founded_inventory.update(quantity:updated_quantity)
          update_log(founded_inventory.lot.id,founded_inventory.warehouse.id,1,@medicament_movement.quantity,current_user.id,@medicament_movement)
        else
          medicament_inventory = MedicamentInventory.new(warehouse_id:@medicament_movement.to_warehouse.id,lot_id:@medicament_movement.lot.id,quantity:@medicament_movement.quantity,user_id:current_user.id)
          if medicament_inventory.save
            update_log(medicament_inventory.lot.id,medicament_inventory.warehouse.id,1,@medicament_movement.quantity,current_user.id,@medicament_movement)
          end
        end
        format.html { redirect_to cli_medicament_movements_url, notice: '.' }
      else
        format.html { render :new }
      end
    end
  end

  def check_quantity
    response = true
    current_quantity = 0
    search_inventory = MedicamentInventory.where("warehouse_id = #{params[:warehouse]} and lot_id = #{params[:lot]}")
    if search_inventory.count > 0
      founded_inventory = search_inventory.first
      updated_quantity = founded_inventory.quantity - params[:quantity].to_i
      if updated_quantity < 0
        response = false
        current_quantity = founded_inventory.quantity
      end
    end
    render :json => {:done => response,:current_cuantity => current_quantity}, layout: false
  end
  private

  def set_medicament_movement
    @medicament_movement = MedicamentMovement.find(params[:id])
  end

  def pharmacy_movement_params
    params.require(:pharmacy_item_movement).permit(:from_warehouse_id,:to_warehouse_id,:pharmacy_item_id,:quantity)
  end
  def medicament_movement_params
    params.require(:medicament_movement).permit(:from_warehouse_id,:to_warehouse_id,:lot_id,:quantity)
  end

end