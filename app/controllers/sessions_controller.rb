class SessionsController < ApplicationController
  def new
    if logged_in?
      enter
    else
      @username = ''
      if params[:user] != nil
        @username = params[:user]
      end
      render layout: false
    end
  end

  def create
    user = User.find_by(user_name: params[:session][:email].downcase)
    user = User.find_by(email: params[:session][:email].downcase) if user.nil?

    if user
      if !user.password_digest.nil?
        if user.authenticate(params[:session][:password])
          if user.is_active
            log_in user
            enter
          else
            flash.now[:danger] = 'Su usuario no se encuentra activo, contacte al administrador'
            render 'sessions/new', layout: false
          end
        else
          flash.now[:danger] = 'Combinación incorrecta de Usuario/Contraseña'
          render 'sessions/new', layout: false
          #flash.now[:danger] = 'Contraseña incorrecta' if user && user.password != params[:session][:password]
        end
      else
        #error no se a agregado el password de activación
        flash.now[:danger] = 'Complete el registro para acceder a MEDSI, se a enviado un correo con las indicaciones'
        render 'sessions/new', layout: false
        #flash.now[:danger] = 'Contraseña incorrecta' if user && user.password != params[:session][:password]
      end
    else
      #usuario y contraseña mal escritos
      flash.now[:danger] = 'Combinación incorrecta de Usuario/Contraseña'
      render 'sessions/new', layout: false
      #flash.now[:danger] = 'Contraseña incorrecta' if user && user.password != params[:session][:password]
    end

  end

  def reset_doctor_password_request
    message = "top secret message"
    password = ENV["KEY_PASSWORD_AESCRYPT"]
    encrypted_data = AESCrypt.encrypt(message, password)
    message = AESCrypt.decrypt(encrypted_data, password)
    redirect_to reset_doctor_password_url(:id => encrypted_data)
  end

  def reset_doctor_password
    @user_info_required = AESCrypt.decrypt(params[:user], ENV["KEY_PASSWORD_AESCRYPT"])
    render layout: false
  end

  def reset_password
    user = User.find(params[:user_id])
    if user.update(password: params[:password])
      respond_to do |format|
        format.json { render json: 'true'}
      end
    else
      respond_to do |format|
        format.json { render json: 'error'}
      end
    end

  end
  def send_password_mail
    user_applicant = User.find_by_email(params[:email])
    if user_applicant!=nil
      if user_applicant.doctor != nil
        RecoverPassword.new_password(user_applicant).deliver_now
      else
        RecoverPassword.new_user_patient_password(user_applicant).deliver_now
      end
      respond_to do |format|
        format.json { render json: 'true'}
      end
    else
      respond_to do |format|
        format.json { render json: 'error'}
      end
    end

  end
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end