class PartialsController < ApplicationController
  let :all, :all



  def medical_consultations_preview
    @consultation_fields = ConsultationField.where(:active => true).order('field_order ASC')
    render layout: false
  end
end