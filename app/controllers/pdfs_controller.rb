class PdfsController < ApplicationController
  let :all, :all
  include Cli::HospitalServicesHelper
  include Doctor::MedicalPedHistoriesHelper

  def daily_cash
    @title = "Reporte de corte de caja"
    @daily_cash = DailyCash.find(params[:daily_cash_id])
    transactions = @daily_cash.movements.order('date DESC')
    @transaction_types = [
        [TransactionType::CONSULTATION,'Cobro de consultas','Consultas'],
        [TransactionType::REPORT,'Cobro de Estudios','Estudios'],
        [TransactionType::OTHER,'Otros Ingresos/Egresos','Otros'],
        [TransactionType::TRANSFER,'Transferencias','Transferencias'],
        [TransactionType::HOSPITAL_ADMISSION,'Cargos de admisiones','Cargos de admisiones'],
        [TransactionType::OTHER_CHARGES,'Otros cargos','Otros cargos']

    ]

    @data = Hash.new
    @transaction_types.each do |tt|
      @data[tt[0]] = get_items_dc tt[0], transactions
    end
    render_pdf("corte_de_caja")
  end

  def get_items_dc transaction_type_id , transactions
    result = []
    transactions.each do |t|
      if t.transaction_type_id == transaction_type_id
        result.push t
      end
    end
    result
  end

  def report_daily_cashes

  end

  def report_invoices

  end
  def report_inventories
    @category = "Todos"
    @warehouse = "Todos"
    medicament_inventories = MedicamentInventory.joins(:lot => :medicament).joins(:warehouse)
    medicament_pharmacy_inventories = PharmacyItemInventory.joins(:pharmacy_item).joins(:warehouse)
    if params[:warehouse_id_search].to_s != "TODO"
      @warehouse = Warehouse.find(params[:warehouse_id_search]).name
      medicament_inventories = medicament_inventories.where("warehouses.id = '#{params[:warehouse_id_search].to_s}'")
      medicament_pharmacy_inventories = medicament_pharmacy_inventories.where("warehouses.id = '#{params[:warehouse_id_search].to_s}'")
    end
    if params[:medicament_catagory_id_search].to_s != "TODO"
      @category = MedicamentCategory.find(params[:medicament_catagory_id_search]).name
      medicament_inventories = medicament_inventories.where("medicaments.medicament_category_id = '#{params[:medicament_catagory_id_search].to_s}'")
      medicament_pharmacy_inventories = medicament_pharmacy_inventories.where("pharmacy_items.medicament_category_id = '#{params[:medicament_catagory_id_search].to_s}'")
    end

    @export_info = medicament_inventories
    @export_info_2 = medicament_pharmacy_inventories
    @title = "Inventario de medicamentos y articulos de farmacia"
    render_pdf("reporte_de_inventario")
  end
  def report_charges
    @title = "Reporte de cobros"
    transactions = Transaction.where("date between STR_TO_DATE('#{params[:fecha_ini]}', '%d/%m/%Y') AND DATE_ADD(STR_TO_DATE('#{params[:fecha_f]}', '%d/%m/%Y'), INTERVAL 1 day) ").order('date DESC')
    @export_info = transactions
    @start_date = params[:fecha_ini]
    @end_date = params[:fecha_f]
    render_pdf("reporte_de_cobros")
  end

  def report_services
    @title = "Reporte de servicios"
    @export_info = get_all_services
    @start_date = params[:fecha_ini]
    @end_date = params[:fecha_f]
    render_pdf("reporte_de_servicios")
  end

  def report_consultations
    @title = "Reporte de consultas"
    @start_date = 'No proporcionado'
    @end_date = 'No proporcionado'
    @doctor_name = 'Todos los doctores'
    @office_name = 'Todos los consultorios'

    @consultations = MedicalConsultation.joins(:office).joins(:medical_expedient => :user)
                         .joins(:medical_expedient => :patient)
                         .order("medical_consultations.date_consultation DESC")


    if params[:start_date] != nil && params[:start_date] != ''
      @start_date = params[:start_date]
      @consultations = @consultations.where('medical_consultations.date_consultation >= ?' , Date::strptime(params[:start_date], "%d/%m/%Y"))
    end

    if params[:end_date] != nil && params[:end_date] != ''
      @end_date = params[:end_date]
      @consultations = @consultations.where('medical_consultations.date_consultation < ?' , Date::strptime(params[:end_date], "%d/%m/%Y") + 1.day)
    end

    if params[:office_id] != nil && params[:office_id] != ''
      @office_name = Office.find(params[:office_id]).name
      @consultations = @consultations.where('medical_consultations.office_id = ?' , params[:office_id] )
    end

    if params[:doctor] != nil && params[:doctor] != ''
      doctor = Doctor.find(params[:doctor])
      @doctor_name = doctor.user.person.full_name
      @consultations = @consultations.where('medical_consultations.user_id = ?' , doctor.user.id )
    end

    render_pdf("reporte_de_consultas")
  end

  def report_service_category
    @title = "Reporte de categorías de servicio"
    @export_info = get_all_services_by_category
    @start_date = params[:fecha_ini]
    @end_date = params[:fecha_f]
    render_pdf("reporte_categorías_servicio")
  end

  def expedient_percentile
  end

  def medical_expedient
    @title = "Expediente clínico"
    @medical_expedient = MedicalExpedient.find(params[:id])

    @has_ped = true
    @has_gin = true

    result_weight = nil
    result_head = nil
    result_size = nil
    result_imc = nil
    title_graph_percentile = ""
    subtitle_graph_percentile = ""
    generate_medical_ped_history @medical_expedient.medical_history
    if @medical_expedient.patient.age.to_i <= 2
      # datos de la OMS
      type_data = TypePercentile::DATA_PERCENTILE_OMS
      title_graph_percentile = "Datos de la OMS"
      subtitle_graph_percentile = "El rango de edad es de 1 a 5 años"
      result_weight = generate_date_graph TypePercentile::WEIGHT,'Peso: ', type_data, false
      result_head = generate_date_graph TypePercentile::HEAD_CIRCUMFERENCE,'Circu.: ', type_data, false
      result_size = generate_date_graph TypePercentile::SIZE_LENGTH,'Altura: ', type_data, false
    else
      # datos de la CDC
      type_data = TypePercentile::DATA_PERCENTILE_CDC
      title_graph_percentile = "Datos de la CDC"
      subtitle_graph_percentile = "El rango de edad es de 2 a 20 años"
      result_weight = generate_date_graph TypePercentile::WEIGHT,'Peso: ', type_data, false
      result_imc = generate_date_graph TypePercentile::IMC,'IMC: ', type_data, false
      result_size = generate_date_graph TypePercentile::SIZE_LENGTH,'Altura: ', type_data, false
    end

    if @medical_expedient.patient.age.to_i <= 20
      @show_percentile_chart = true
    else
      @show_percentile_chart = false
    end


    @result_weight = result_weight
    @result_head = result_head
    @result_imc = result_imc
    @result_size = result_size
    @title_type_data_percentile = title_graph_percentile
    @subtitle_graph_percentile = subtitle_graph_percentile

    render_pdf("expediente_clinico")
  end

  def medical_annotation
    @title = "Nota médica"
    @view = view_context
    @medical_annotation = MedicalAnnotation.find(params[:medical_annotation_id])
    render_pdf("nota_medica")
  end

  def hospital_admission
    @title = "Admisión hospitalaria"
    @admission = HospitalAdmission.find(params[:id])
    render_pdf("admision_hospitalaria")
  end

  def hospital_admission_medical
    @title = "Admisión hospitalaria"
    @hospitalization  = HospitalAdmission.find(params[:id])
    render_pdf("admision_hospitalaria")
  end

  def consultation_template
    @title = "Nota médica de consulta"
    consultation = MedicalConsultation.find(params[:consultation_id])
    consultation_template = Setting.find_by_code(Const::SETTING_TEMPLATE_CONSULTATION)
    @html_content = HtmlParser::medical_consultation_annotation(consultation, consultation_template.data)
    render_pdf("nota_medica_consulta_#{consultation.folio}")
  end

  def chart_diseases
    @title = "Gráfica de enfermedades"
    type_graph_person = 1
    type_graph_incidence = 2
    type = params[:type_graph]
    query = nil
    start_date = Date.parse(params[:start_date])
    end_date = Date.parse(params[:end_date])
    more_than = params[:more_than].to_i
    title_graph = ""
    if type.to_i == type_graph_person
      title_graph = "PERSONAS"
      query = query_graph_diseases type,start_date,end_date,more_than

    else
      if type.to_i == type_graph_incidence.to_i
        title_graph = "INCIDENCIAS"
        query = query_graph_diseases type,start_date,end_date,more_than
      end
    end

    records = ActiveRecord::Base.connection.exec_query(query) if !query.nil?

    if !records.nil?

      categories = []
      series = []
      data_series = []
      records.each do |row|
        categories << row["disease"].mb_chars.upcase
        data_series << row["total"].to_i
      end
      series << {
          name: title_graph,
          data: data_series
      }

      @categories_graph = categories
      @series = series
      @start_date = start_date
      @end_date = end_date
      @more_than = more_than

      render_pdf("grafica_enfermedades")
    else
      render json: {result: "Error"},   layout: false
    end
  end

  def chart_percentile
    @title = "Gráfica de percentiles"
    @medical_expedient = MedicalExpedient.find(params[:id])
    @graph_type = params[:type]
    @data_type = params[:data].to_i
    @has_ped = true
    @has_gin = true

      generate_medical_ped_history @medical_expedient.medical_history
      if @data_type == 1
        # datos de la OMS
        type_data = TypePercentile::DATA_PERCENTILE_OMS
        @title_type_data_percentile = "Datos de la OMS"
        @subtitle_graph_percentile = "El rango de edad es de 1 a 5 años"
        @result_weight = generate_date_graph TypePercentile::WEIGHT,'Peso: ', type_data, false
        @result_head = generate_date_graph TypePercentile::HEAD_CIRCUMFERENCE,'Circu.: ', type_data, false
        @result_size = generate_date_graph TypePercentile::SIZE_LENGTH,'Altura: ', type_data, false
      else
        # datos de la CDC
        type_data = TypePercentile::DATA_PERCENTILE_CDC
        @title_type_data_percentile = "Datos de la CDC"
        @subtitle_graph_percentile = "El rango de edad es de 2 a 20 años"
        @result_weight = generate_date_graph_combine TypePercentile::WEIGHT,'Peso: ', type_data, false
        @result_imc = generate_date_graph TypePercentile::IMC,'IMC: ', type_data, false
        @result_size = generate_date_graph_combine TypePercentile::SIZE_LENGTH,'Altura: ', type_data, false
      end

    render_pdf("grafica_percentiles")
  end

  def ticket
    @title = "Ticket"
    @amount = 0
    @concepts = []
    @header = []

    clinic_info = ClinicInfo.find(1)


    case params[:type]
      when Const::TICKET_ADMISSION
        admission_transaction = AdmissionTransaction.find(params[:id])
        comps = admission_transaction.folio.split("-")


        @concepts.push({
                           :name => clinic_info.ticket_description,
                           :amount => admission_transaction.amount
                       })

        @header = [
            {:label => "Fecha", :val => date_hour(admission_transaction.created_at)},
            {:label => "Serie", :val => comps[0]},
            {:label => "Folio", :val => comps[1]},
            {:label => "Agente", :val => admission_transaction.user.person.full_name},
            {:label => "Paciente", :val => admission_transaction.hospital_admission.patient.full_name.upcase},
        ]
      when Const::TICKET_OTHER_CHARGES
        other_charge = OtherChargeTransaction.find(params[:id])
        comps = other_charge.folio.split("-")
        @header = [
            {:label => "Fecha", :val => date_hour(other_charge.created_at)},
            {:label => "Serie", :val => comps[0]},
            {:label => "Folio", :val => comps[1]},
            {:label => "Agente", :val => other_charge.user.person.full_name},
            {:label => "Paciente", :val => other_charge.other_charge.patient_name},
        ]

        @concepts.push({
                           :name => clinic_info.ticket_description,
                           :amount => other_charge.amount
                       })

      when Const::TICKET_TRANSACTION
        transaction = Transaction.find(params[:id])

        @header = [
            {:label => "Fecha", :val => date_hour(transaction.created_at)},
            {:label => "Tipo", :val => transaction.transaction_type.name},
            {:label => "", :val => transaction.payment_method.name},
        ]

        @concepts.push({
                           :name => clinic_info.ticket_description,
                           :amount => transaction.amount
                       })

    end

    ticket_width = Setting.find_by_code(Const::SETTING_TICKET_WIDTH).data.to_f
    ticket_height = Setting.find_by_code(Const::SETTING_TICKET_HEIGHT).data.to_f

    w = ticket_width / 24
    h = ticket_height / 24

    h = h + (@concepts.size * 0.16)
    render_ticket("ticket",w,h)
  end

  private

  def render_pdf(name)
    render pdf: name, page_size: 'Letter',
           :header => {
               :html => {
                   :template => '/pdfs/pdf_header.html.erb'
               }
           },
           :footer => {
               :html => {
                   :template => '/pdfs/pdf_footer.html.erb'
               }
           }
  end

  def render_ticket(name, width, height)
    render pdf: name,
           page_height:"#{height}in",
           page_width: "#{width}in",
           :margin => { :top => 5, :bottom => 5, :left => 2, :right => 2 }
  end

  def query_graph_diseases(type,start_date,end_date,more_than)
    type_graph_person = 1
    type_graph_incidence = 2
    if type.to_i == type_graph_person
      query = "SELECT DIS.name as disease, Count(distinct ME.id) as total
            FROM diseases_medical_consultations ENF
                inner join diseases DIS on ENF.disease_id = DIS.id
                inner join medical_consultations MC on ENF.medical_consultation_id = MC.id
                inner join medical_expedients ME on MC.medical_expedient_id = ME.id
            Where  date(MC.date_consultation) between '#{start_date.strftime("%Y-%m-%d")}' and '#{end_date.strftime("%Y-%m-%d")}'
            GROUP BY ENF.disease_id
            HAVING count(distinct ME.id) > #{more_than}
            ORDER BY count(distinct ME.id) DESC "

    else
      if type.to_i == type_graph_incidence
        query = "SELECT DIS.name as disease, Count(*) as total
                  FROM diseases_medical_consultations ENF
                      inner join diseases DIS on ENF.disease_id = DIS.id
                      inner join medical_consultations MC on ENF.medical_consultation_id = MC.id
                      inner join medical_expedients ME on MC.medical_expedient_id = ME.id
                  Where  date(MC.date_consultation) between '#{start_date.strftime("%Y-%m-%d")}' and '#{end_date.strftime("%Y-%m-%d")}'
                  GROUP BY ENF.disease_id
                  HAVING count(*) > #{more_than}
                  ORDER BY count(*) DESC  "
      end
    end
    query
  end

  def create_pdf

  end
end