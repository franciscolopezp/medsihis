class SharedAssistant::TransactionsController < ApplicationController
  let :DOCTOR, :all
  let :SHARED_ASSISTANT, :all
  include Doctor::ADD_FINKOK
  include Doctor::FINKOK

  def index
    respond_to do |format|
      format.html
      format.json { render json: TransactionsDatatable.new(view_context,session[:doctor_id])}
    end
  end
  def general_invoice

  end

  def getDocFiscalInfo
    docinfo = Doctor.find(params[:doctor_id])
    info = docinfo.fiscal_information
    doc_info = []
    doc_info.push({
                      :business_name => info.bussiness_name,
                      :rfc => info.rfc,
                      :address => info.fiscal_address,
                      :suburb => info.suburb,
                      :locality => info.locality,
                      :ext_number => info.ext_number,
                      :int_number => info.int_number,
                      :zip => info.zip,
                      :city => info.city.name,
                      :state => info.city.state.name,
                      :country => info.city.state.country.name,
                      :serie => info.serie,
                      :folio => info.folio,
                      :iva => info.iva,
                      :isr => info.isr,
                      :iva_report => docinfo.iva_report,
                      :iva_report_included => docinfo.iva_report_included
                  })
    respond_to do |format|
      format.html { render json: doc_info}
    end
  end
  def open_invoice

  end


  def search_invoices
    amount = 0
    transaction_id = 0
    if params[:type].to_i == 1
    consultas = MedicalConsultation.joins(:medical_expedient).where("medical_expedients.doctor_id = #{params[:doctor_id]}").where("date_consultation >= '#{params[:fecha_ini]}'").where("date_consultation <  '#{params[:fecha_f]}'")
    else
      consultas = CabinetReport.joins(:clinical_analysis_order => :medical_expedient).where("medical_expedients.doctor_id = #{params[:doctor_id]}").where("cabinet_reports.created_at >= '#{params[:fecha_ini]}'").where("cabinet_reports.created_at <  '#{params[:fecha_f]}'")
    end
    invoices_info = []
    consultas.each do |info|
      if params[:type].to_i == 1
        if info.charges.count > 0
          amount = info.get_total
          if !info.invoiced
            invoices_info.push({
                                   :id => info.id,
                                   :folio =>info.id,
                                   :patient => (info.medical_expedient.patient.name + " " +info.medical_expedient.patient.last_name),
                                   :date => info.date_consultation.strftime('%d/%m/%Y'),
                                   :amount => ActionController::Base.helpers.number_to_currency(amount, :unit => "$"),
                                   :payment_method => info.charges.collect{|c| c.get_transaction.payment_method.name}.join(', ')
                               })
          end
        end
      else
        if info.charge_report_studies.count > 0
          amount = info.get_total
          if !info.invoiced
            invoices_info.push({
                                   :id => info.id,
                                   :folio =>info.id,
                                   :patient => info.clinical_analysis_order.medical_expedient.patient.full_name,
                                   :date => info.created_at.strftime('%d/%m/%Y'),
                                   :amount => ActionController::Base.helpers.number_to_currency(amount, :unit => "$"),
                                   :payment_method => info.charge_report_studies.collect{|c| c.transaction_item.payment_method.name}.join(', ')
                               })
          end
        end
      end
    end
    respond_to do |format|
      format.html { render json: invoices_info}
    end
  end



  private

  # Use callbacks to share common setup or constraints between actions.

end

