class SharedAssistant::InvoicesController < ApplicationController
  let :SHARED_ASSISTANT, :all
  include Doctor::ADD_FINKOK
  include Doctor::FINKOK
  before_action :set_invoice, only: [:show, :edit, :update, :destroy]

  def index

    respond_to do |format|
      format.html
      format.json { render json: InvoicesDatatable.new(view_context,session[:doctor_id],false)}
    end

  end
  def open_invoices
    respond_to do |format|
      format.html
      format.json { render json: InvoicesDatatable.new(view_context,session[:doctor_id],true)}
    end
  end
  def get_receptor_email
    email = ""
    t = Invoice.find(params[:invoice_id])
    if t != nil
        if t.patient_fiscal_information != nil
          if t.patient_fiscal_information.email != ""
            email = t.patient_fiscal_information.email
          end
        end
    end

    respond_to do |format|
      format.html { render json: {:email => email} }
    end

  end

  def send_invoice_email
    inv = Invoice.find(params[:invoice_id])
    if inv.transactions.count > 0
      transac = inv.transactions.first
      consultation = transac.chargeable.medical_consultation
    end
    if  inv.invoice_type == 2
      pdf_name = "Factura_"+inv.created_at.strftime('%Y-%m-%d')+"T"+inv.created_at.strftime('%H:%M:%S')
      pdfs = render_to_string pdf: pdf_name,  locals: { :@med_consultation => consultation, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
                              margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => consultation}}, center: 'TEXT',
                                                                                                                 spacing:           1,
                                                                                                                 line:              true
          }
    elsif inv.invoice_type == 1
      pdf_name = "Factura_"+transac.invoice.patient_fiscal_information.rfc+"_"+transac.invoice.certification_date
      pdfs = render_to_string pdf: pdf_name , locals: { :@med_consultation => consultation, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
                              margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => consultation}}, center: 'TEXT',
                                                                                                                 spacing:           1,
                                                                                                                 line:              true }
    else
      pdf_name = "Factura_"+inv.open_invoice_receptor.rfc+"_"+inv.created_at.strftime('%Y-%m-%d')+"T"+inv.created_at.strftime('%H:%M:%S')
      pdfs = render_to_string pdf: pdf_name,  locals: { :@med_consultation => consultation, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
             margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => consultation}}, center: 'TEXT',
                                                                                                spacing:           1,
                                                                                                line:              true
          }
    end

    SendInvoice.send_invoice(params[:email],pdfs,pdf_name,inv).deliver_now
    render :json => true, layout: false


  end
  def print_pdf_report
    @start_date = 'No proporcionado'
    @end_date = 'No proporcionado'
    @business_name = 'Todos'
    @invoice_status = 'Todos'
    @doctor = Doctor.find(session[:doctor_id])
    invoices = Invoice.all.where("invoices.doctor_id = #{@doctor.id}")
    if params[:patient_fi].to_s != "all"
      invoices = invoices.where("patient_fiscal_information_id = #{params[:patient_fi].to_s}")
      @business_name = PatientFiscalInformation.find(params[:patient_fi]).business_name
    end
    if params[:invoice_status].to_s == "0"
      invoices = invoices.where("cancel is null")
      @invoice_status = "Activos"
    elsif params[:invoice_status].to_s == "1"
      invoices = invoices.where("cancel = true")
      @invoice_status = "Cancelados"
    end
    if params[:start_date].to_s != "all"
      invoices = invoices.where("created_at >= '#{params[:start_date].to_s}'")
      @start_date = params[:start_date]
    end
    if params[:end_date].to_s != "all"
      invoices = invoices.where("created_at < '#{params[:end_date].to_s}'")
      @end_date = params[:end_date]
    end
    @invoices = invoices
    render pdf: "Reporte_facturas_"+ Date.today.strftime('%Y%m%d'), locals: { :@invoices_processed => invoices}, template: 'doctor/invoices/invoice_report.html.erb', layout: false,
           margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/invoices/invoice_report_footer.html.erb', locals: {:@invoices_processed => invoices}}, center: 'TEXT',
                                                                                              spacing:           1,
                                                                                              line:              true
        }
  end
  def print_pdf
    inv = Invoice.find(params[:id])
    if inv.transactions.count > 0
      transac = inv.transactions.first
      consultation = transac.chargeable.medical_consultation
    end
    respond_to do |format|
      format.pdf do
        if inv.invoice_type == 2
          render pdf: "Factura_"+inv.doctor.fiscal_information.rfc+"_"+inv.created_at.strftime('%Y-%m-%d')+"T"+inv.created_at.strftime('%H:%M:%S'),  locals: { :@med_consultation => consultation, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
                 margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => consultation}}, center: 'TEXT',
                                                                                                    spacing:           1,
                                                                                                    line:              true
              }
        elsif inv.invoice_type == 1
          render pdf: "Factura_"+transac.invoice.patient_fiscal_information.rfc+"_"+transac.invoice.certification_date,  locals: { :@med_consultation => consultation, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
                 margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => consultation}}, center: 'TEXT',
                                                                                                    spacing:           1,
                                                                                                    line:              true
              }
        else
          render pdf: "Factura_"+inv.open_invoice_receptor.rfc+"_"+inv.created_at.strftime('%Y-%m-%d')+"T"+inv.created_at.strftime('%H:%M:%S'),  locals: { :@med_consultation => consultation, :@invoice_data => inv }, template: 'doctor/transactions/template_invoice.html.erb', layout: false,
                 margin: { top: 2, bottom: 28, left: 5, right: 5 }, encoding: 'UTF-8', footer:  {   html: {   template:'doctor/transactions/template_invoice_footer.html.erb', locals: {:@med_consultation => consultation}}, center: 'TEXT',
                                                                                                    spacing:           1,
                                                                                                    line:              true
              }
        end

      end
    end
  end
  def numero_a_palabras(numero)
    de_tres_en_tres = numero.to_i.to_s.reverse.scan(/\d{1,3}/).map{|n| n.reverse.to_i}

    millones = [
        {true => nil, false => nil},
        {true => 'millón', false => 'millones'},
        {true => "billón", false => "billones"},
        {true => "trillón", false => "trillones"}
    ]

    centena_anterior = 0
    contador = -1
    palabras = de_tres_en_tres.map do |numeros|
      contador += 1
      if contador%2 == 0
        centena_anterior = numeros
        [centena_a_palabras(numeros), millones[contador/2][numeros==1]].compact if numeros > 0
      elsif centena_anterior == 0
        [centena_a_palabras(numeros), "mil", millones[contador/2][false]].compact if numeros > 0
      else
        [centena_a_palabras(numeros), "mil"] if numeros > 0
      end
    end

    palabras.compact.reverse.join(' ')
  end

  def centena_a_palabras(numero)
    especiales = {
        11 => 'once', 12 => 'doce', 13 => 'trece', 14 => 'catorce', 15 => 'quince',
        10 => 'diez', 20 => 'veinte', 100 => 'cien'
    }
    if especiales.has_key?(numero)
      return especiales[numero]
    end

    centenas = [nil, 'ciento', 'doscientos', 'trescientos', 'cuatrocientos', 'quinientos', 'seiscientos', 'setecientos', 'ochocientos', 'novecientos']
    decenas = [nil, 'dieci', 'veinti', 'treinta', 'cuarenta', 'cincuenta', 'sesenta', 'setenta', 'ochenta', 'noventa']
    unidades = [nil, 'un', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve']

    centena, decena, unidad = numero.to_s.rjust(3,'0').scan(/\d/).map{|i| i.to_i}

    palabras = []
    palabras << centenas[centena]

    if especiales.has_key?(decena*10 + unidad)
      palabras << especiales[decena*10 + unidad]
    else
      tmp = "#{decenas[decena]}#{' y ' if decena > 2 && unidad > 0}#{unidades[unidad]}"
      palabras << (tmp.blank? ? nil : tmp)
    end

    palabras.compact.join(' ')
  end
  helper_method :numero_a_palabras
  def download_xml
    inv = Invoice.find(params[:id])
    transac = nil
    if inv != nil
      xml_data = inv.xml
      folio = inv.folio
    else
      xml_data = transac.invoice.xml
      folio = transac.invoice.folio
    end
    respond_to do |format|
      format.xml { send_data render_to_string(xml: xml_data) ,:filename => 'Factura'+folio.to_s+'.xml', :type=>"application/xml", :disposition => 'attachment' }
    end
  end
  def cancel_invoice
   invoice = Invoice.find(params[:invoice_id])
    doc = Doctor.find(session[:doctor_id])
    proveedor = Comprobante.new()
    pem = Dir.pwd + "/uploads/fiscal_information_" + doc.fiscal_information.id.to_s + "/certificate_key/key.pem"
    certificado =  %x'openssl x509 -inform DER -in #{doc.fiscal_information.certificate_stamp.current_path.to_s}'
    resultado =  proveedor.cancela invoice.UUID, doc.fiscal_information.rfc,pem, certificado.to_s
    if resultado[:message] == "1"
      invoice.update(xml: resultado[:data],cancel:1)
      if invoice.invoice_type != 3
        invoice.transactions.each { |x|
          transac = Transaction.find(x)
          transac.update(invoiced: 0)
        }
      end
      result_message = "Factura cancelada con exito"
      result_code = "1"
    else
      result_message = get_cancel_invoice_error resultado[:message]
      result_code = "0"
    end

    respond_to do |format|
      format.html { render json: {:message => result_message, :result => result_code} }
    end
  end
  def get_doctor_fi
    doctor = Doctor.find(params[:doctor_id])
    patients = []
    doctor.patients.each do |patient|
      patient.patient_fiscal_informations.each do |fi|
        patients.push(
            {
                id: fi.id,
                name: fi.business_name
            }
        )
      end
    end
    render :json => {:patients => patients}, layout: false
  end

  def get_doctor_open_names
    doctor = Doctor.find(params[:doctor_id])
    patients = []
    business_names = OpenInvoiceReceptor.joins(:invoice).where("invoices.doctor_id = #{doctor.id}").group(:business_name)
    business_names.each do |patient|
        patients.push(
            {
                id: patient.business_name,
                name: patient.business_name
            }
        )
    end
    render :json => {:patients => patients}, layout: false
  end
  def show
  end

  def new
    @invoice = Invoice.new
    @textd_button = "Guardar"
  end

  def edit
    @textd_button = "Guardar"
  end


  def create
    @textd_button = "Guardar"
    @invoice = Invoice.new(invoice_params)

    respond_to do |format|
      if @invoice.save
        format.html { redirect_to doctor_invoices_path, notice: 'La factura fue agregada con exito'}
        format.json { render show, status: :ok, location: @invoice}
      else
        format.html { render :new}
        format.json { render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def update
    @textd_button = "Guardar"
    respond_to do |format|
      if @invoice.update(invoice_params)
        format.html { redirect_to doctor_invoices_path, notice: 'factura actualizada'}
        format.json { render show, status: :ok,location: @invoice}
      else
        format.html { render :edit}
        format.json { render json: 'error factura', status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @invoice.destroy
    respond_to do |format|
      format.html { redirect_to doctor_invoices_path, notice: 'factura eliminada'}
      format.json { head :no_content}
    end
  end

  private
  def set_invoice
    @invoice = Invoice.find(params[:id])
  end

  def invoice_params
    params.require(:invoice).permit(:xml_base,:xml_signed,:original_string,:medical_consultation_id)
  end
end