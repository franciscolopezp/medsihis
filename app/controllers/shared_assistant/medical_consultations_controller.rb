class SharedAssistant::MedicalConsultationsController < ApplicationController
  let :SHARED_ASSISTANT, :all
  include Doctor::MedicalConsultationsHelper
  include Doctor::FilterRequestHelper
  before_action :filter_medical_consultations, only: [:generate_prescription]
  before_action :set_doctor_medical_consultation, only: [:show, :edit, :update, :destroy, :update_flag_print_diagnostic]


  def index
    if session[:doctor_id] != nil
      current_user = Doctor.find(session[:doctor_id]).user
    else
      current_user = nil
    end


    respond_to do |format|
      format.html
      format.json { render json: MedicalConsultationsDatatable.new(view_context,current_user,session[:assistant_id])}
    end
  end

  def show
  end



  def print_report

    @start_date = 'No proporcionado'
    @end_date = 'No proporcionado'
    @patient_name = 'Todos'
    @office_name = 'Todos'

    @doctor = Doctor.find(params[:doctor_id])

    @consultations = MedicalConsultation.joins(:office).joins(:medical_expedient => :doctor)
                     .joins(:medical_expedient => :patient)
                     .where(:doctors => {:id => @doctor.id})
                     .order("medical_consultations.date_consultation DESC")

    if params[:start_date] != nil && params[:start_date] != ''
      @start_date = params[:start_date]
      @consultations = @consultations.where('medical_consultations.date_consultation >= ?' , Date::strptime(params[:start_date], "%d/%m/%Y"))
    end

    if params[:end_date] != nil && params[:end_date] != ''
      @end_date = params[:end_date]
      @consultations = @consultations.where('medical_consultations.date_consultation < ?' , Date::strptime(params[:end_date], "%d/%m/%Y") + 1.day)
    end

    if params[:office_id] != nil && params[:office_id] != ''
      @office_name = Office.find(params[:office_id]).name
      @consultations = @consultations.where('medical_consultations.office_id = ?' , params[:office_id] )
    end

    if params[:patient] != nil && params[:patient] != ''
      @patient_name = Patient.find(params[:patient]).full_name
      @consultations = @consultations.where('medical_expedients.patient_id = ?' , params[:patient] )
    end

    respond_to do |format|
      format.pdf do
        render pdf: 'reporte_consultas_' + Date.today.strftime('%Y%m%d'),
               encoding: 'utf8',
               :footer => {   html: {   template:'shared_assistant/medical_consultations/print_report_footer.pdf.erb'  } }
      end
    end
  end


  private

end
