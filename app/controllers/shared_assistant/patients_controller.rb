class SharedAssistant::PatientsController < ApplicationController
  let :SHARED_ASSISTANT, :all

  include Doctor::PatientsHelper
  include Doctor::FilterRequestHelper
  before_action :filter_patient_assistant_shared, only: [:show, :edit]
  before_action :set_doctor_patient, only: [:show, :edit, :update]

  def index
    doctors_ids = get_doctors_active_assistant_shared.map{|doctor| doctor.id}

    respond_to do |format|
      format.html
      format.json { render json: PatientsDatatable.new(view_context,doctors_ids,true,current_user)}
    end
  end

  def new
    @doctor_patient = Patient.new
    @doctor_patient.build_city
    @text_button = "Agregar"
    @is_new = true
  end

  def create
    @doctor_patient = Patient.new(doctor_patient_params)
    set_city_patient params
    save_patient_folio @doctor_patient.doctor_id
    respond_to do |format|
      if @doctor_patient.save

        if !params[:picture64].nil? && params[:picture64]!=""
          save_picture_cam params[:picture64]
        end
        if !params[:crop_image_64].nil? && params[:crop_image_64]!=""
          save_picture_cam params[:crop_image_64]
        end
        format.html { redirect_to shared_assistant_patients_path}
        format.json { render :show, status: :created, location: @doctor_patient }
      else
        @doctor_patient.build_city
        format.html { render :new }
        format.json { render json: @doctor_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  def show

  end

  def edit
    @text_button = "Actualizar"
    @is_new = false
  end

  # PATCH/PUT /doctors/patients/1
  # PATCH/PUT /doctors/patients/1.json
  def update
    respond_to do |format|
      @city_name = params[:city_name]
      @text_button = "Actualizar"
      if @doctor_patient.update(doctor_patient_params)
        set_city_patient params
        @doctor_patient.update(city_id: @doctor_patient.city_id)

        if !params[:picture64].nil? && params[:picture64]!=""
          save_picture_cam params[:picture64]
        end

        if !params[:crop_image_64].nil? && params[:crop_image_64]!=""
          save_picture_cam params[:crop_image_64]
        end
        if params[:update_from_expediente]!=nil
          # se utiliza cuando se actualiza información del paciente desde el expediente
          # ahora el asistente compartido no puede ver el expediente
        #  format.html { redirect_to doctor_search_expedient_path(@doctor_patient.medical_expedient), notice: 'photo update.' }
          format.html { redirect_to shared_assistant_patients_path, notice: 'photo update'}
        else
          format.html { redirect_to shared_assistant_patients_path, notice: 'Patient was successfully updated.' }
          format.json { render :show, status: :ok, location: @doctor_patient }
        end
      else
        @doctor_patient.build_city
        format.html { render :edit }
        format.json { render json: @doctor_patient.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_doctor_patient
      @doctor_patient = Patient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def doctor_patient_params
      params.require(:patient).permit(:address, :birth_day, :blood_type_id, :cell, :email, :gender, :height, :name, :last_name, :phone, :weight, :city_id, :vaccinations, :birthplace, :doctor_id, :gender_id, :active,:allergy_ids => [])
    end

end
