class SharedAssistant::PersonalsupportsController < ApplicationController
  let :SHARED_ASSISTANT, :all
  include Doctor::FilterRequestHelper
  before_action :filter_personal_support, only: [:show, :edit]
  before_action :set_personal, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.json { render json: PersonalsupportsDatatable.new(view_context,session[:doctor_id],session[:assistant_id])}
    end
  end

  def show
  end

  def new
    @text_button = "Guardar"
    @personal = Personalsupport.new
    @personal.build_operation_role
  end

  def edit
    @text_button = "Guardar"
  end

  def create
    @text_button = "Guardar"
    puts personal_params
    @personal = Personalsupport.new(personal_params)
    respond_to do |format|
      if @personal.save
        format.html  { redirect_to shared_assistant_personalsupports_path}
        format.json { render :show, status: :created, location: @personal  }
      else
        format.html { render :new}
        format.json {render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def update
    @text_button = "Guardar"
    respond_to do |format|
      if @personal.update(personal_params)
        format.html { redirect_to shared_assistant_personalsupports_path}
        format.json { render show, status: :ok, location: @personal}
      else
        format.html { render :edit}
        format.json { render json: 'Personal error', status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @personal.destroy
    respond_to do |format|
      format.html { redirect_to shared_assistant_personalsupports_path}
      format.json { head :no_content}
    end
  end

  private
  def set_personal
    @personal = Personalsupport.find(params[:id])
  end

  def personal_params
    params.require(:personalsupport).permit(:name, :lastname, :email, :contact_number, :operation_role_id,:doctor_id)
  end

end