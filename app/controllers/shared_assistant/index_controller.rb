class SharedAssistant::IndexController < ApplicationController
  let :SHARED_ASSISTANT, :all
  let :ADMIN, :all
  include SessionsHelper
  include SharedAssistant::WorkingScheduleHelper
  include Doctor::ActivitiesHelper


  def index
    @assistant = current_user.assistant
    @payment_methods = PaymentMethod.all
    @daily_cash = DailyCash.current(session[:user_id]).first
    @patients = Patient.by_doctor session[:doctor_id]

    #get_permission_assistant
  end

  def activities
    is_agenda = params[:is_agenda] == 'true' ? true : false

    activities = get_activities

    result = []
    activities.each do |a|
      if is_agenda
        act = fullcalendar_activity a
      else # maybe is requested from waiting room page
        act = json_from_model a
      end
      result.push(act)
    end
    respond_to do |format|
      format.html { render json: result}
    end

  end

  def waiting_room
    activities = activities_waiting_room
    respond_to do |format|
      format.html { render json: activities}
    end
  end

  def pending_consult

    consults = pending_consults
    respond_to do |format|
      format.html { render json: consults}
    end
  end
  def pending_report

    consults = pending_reports
    respond_to do |format|
      format.html { render json: consults}
    end
  end

end