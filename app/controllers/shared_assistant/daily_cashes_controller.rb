class SharedAssistant::DailyCashesController < ApplicationController
  load_and_authorize_resource
  skip_authorize_resource :only => [:create,:update]
  include Doctor::DailyCashesHelper

  def index

    @assistant = current_user.assistant
    offices = @assistant.offices
    offices_ids = offices.map {|o| o.id }

    @accounts = Account.local_accounts offices_ids #local_accounts to open daily_cash
    @daily_cashes = DailyCash.open_by_offices offices_ids,current_user.id


    doctors = @assistant.doctors
    doctors_ids = doctors.map {|d| d.id }
    @all_accounts = Account.where('doctor_id IN ('+doctors_ids.join(',')+')')


    @transaction_types = TransactionType.all
    @movement_types = MovementType::group_by_category
    @payment_methods = PaymentMethod.all

    @daily_cash = []
  end
  def history
    respond_to do |format|
      format.html
      format.json { render json: DailyCashDatatable.new(view_context, current_user)}
    end
  end
  def show
    @daily_cash = DailyCash.find(params[:id])
    @prev = params[:prev]
  end
  def get_items transaction_type_id , transactions
    result = []
    transactions.each do |t|
      if t.transaction_type_id == transaction_type_id
        result.push t
      end
    end
    result
  end
  def print_daily_cash
    @daily_cash = DailyCash.find(params[:id])
    transactions = @daily_cash.movements.order('date DESC')
    @transaction_types = [
        [TransactionType::CONSULTATION,'Cobro de consultas','Consultas'],
        [TransactionType::REPORT,'Cobro de Estudios','Estudios'],
        [TransactionType::OTHER,'Otros Ingresos/Egresos','Otros'],
        [TransactionType::TRANSFER,'Transferencias','Transferencias']
    ]

    @data = Hash.new
    @transaction_types.each do |tt|
      @data[tt[0]] = get_items tt[0], transactions
    end

    respond_to do |format|
      format.pdf do
        render pdf: 'corte_de_caja_'+@daily_cash.user.user_name+'_' + @daily_cash.start_date.strftime('%Y%m%d'),
               encoding: 'utf8',
               margin: { top: 10, bottom: 10, left: 15, right: 15 },
               :footer =>  {   html: {   template:'shared_assistant/daily_cashes/print_daily_cash_footer.html.erb' }}
      end
    end
  end
  def create
    done = true
    error = ''
    message = ''
    daily_cash  = DailyCash.open_by_account(params[:account_id]).first
    if daily_cash == nil
      open_daily_cash params[:account_id]
    else
      done = false
      error = 'Existe un corte activo'
      message = 'No es posible tener dos cortes activos en la misma cuenta.'
    end

    respond_to do |format|
      format.html { render json: {:done => done, :message => message, :error => error}}
    end

  end

end