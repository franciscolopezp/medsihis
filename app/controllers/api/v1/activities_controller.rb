class Api::V1::ActivitiesController < ApplicationController
  include Api::V1::AppointmentsHelper
  include Api::V1::ActivitiesHelper
  include Doctor::ActivitiesHelper
  include Doctor::DashboardHelper
  include ApplicationHelper

  skip_before_action :verify_authenticity_token
  before_action :validate_token
  before_action :set_user,  except: [:show_activity,:destroy_activity]
  before_action :set_time_zone_api
  respond_to :json

  def schedule_filter
    array_json_activities = find_all_activity params
    send_response :ok, array_json_activities, "Lista de doctores"
  end

  def update_appointment
    @activity = Activity.find(params[:activity_id])

    flag_schedule = false
    date_before = nil
    flag_continue_notification = false
    if params[:activity_status_id].to_i == ActivityStatus::SCHEDULED && params[:new_schedule] == 'true'
      start_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:start_date])
      end_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:end_date])

      result = exist_rank_date start_date,end_date, @activity.doctor,@activity.id

      if result[:result]
        # error, ya existe una actividad en esa fecha
        send_response 'error', {}, "Error al actualizar la actividad", ['Existe una actividad ya agendada en la misma fecha']
      else
      #  if params[:activity_status_id].to_i == ActivityStatus::SCHEDULED && params[:new_schedule] == 'true'
        flag_continue_notification = true
          @activity.update({:activity_status_id => params[:activity_status_id] })
          flag_schedule = true
          date_before = @activity.start_date
          @activity.update({:start_date => params[:start_date] , :end_date => params[:end_date] })
      #  end
      end
    else
      flag_continue_notification = true
      @activity.update({:activity_status_id => params[:activity_status_id] })
    end

    # valida si puede continuar con las notificaciones
    if flag_continue_notification
      activity_json = format_json_activity @activity
      doctor_username = @activity.doctor.user.user_name

      act = json_from_model @activity

      @data = {
          :type => 'new_activity',
          :activity => act,
          :in_w_room => false,
          :is_new => false,
          :dragged => (params[:dragged].present? and  params[:dragged] == 'true') ? true : false
      }

      if @activity.activity_status_id == ActivityStatus::WAITING  && @activity.start_date.today?
        @data["in_w_room"] = true
        @data["w_room_data"] = format_activity_wroom(@activity)
      end

      # notificar al paciente si la cita se confirmo, cancelo o reagendo, en la app
      # notificar al doctor de la nueva cita en web y la app
      if @activity.user_patients.size > 0
        # se notifica al paciente de la app y al doctor
        notification_doctor_and_patient doctor_username, @data, @activity.user_patients.first.user, @activity, flag_schedule, date_before
      else
        # se notifica solo al doctor
        notification_private_web doctor_username, @data
      end

      send_response :ok, activity_json, "Se actualizo correctamente"
    end

  end

  def create_activity

    if params[:activityId].to_i == 0 # se crea actividad nueva
      @activity = Activity.new

      @activity.all_day = params[:allDay]
      @activity.name = params[:name]
      @activity.details = params[:details]

      start_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:startDate])
      end_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:endDate])

      @activity.start_date = start_date
      @activity.end_date = end_date

      doctor = @user.doctor
      res = exist_rank_date(@activity.start_date,@activity.end_date,doctor,0)

      if res[:result]

        render :json => {
                   status: 'error',
                   response:{},
                   message: "Error al crear la cita",
                   errors: ['Por favor verifique la disponibilidad nuevamente.']
               }, layout: false
      else
        @activity.owner_type = 'Doctor'
        @activity.owner_id = doctor.id
        @activity.doctor_id = doctor.id

        @activity.activity_type = ActivityType.find(params[:activityType])
        @activity.activity_status = ActivityStatus.find(params[:activityStatus])

        info_personal_support = false
        if @activity.activity_type.id == ActivityType::SURGERY
          if params[:personalSupportsIds].present?
            params[:personalSupportsIds].each do |value,idx|
              info_personal_support = true
              @activity.personalsupports << Personalsupport.find(value.to_i)
            end
          end
        end

        if @activity.save
          offices = []
          if params[:officeId].present? and params[:officeId] != 0
            offices << Office.find(params[:officeId])
          else
            offices = Office.active_offices.where(doctor_id: doctor.id)
          end

          patients = []
          if params[:patientId].present? and params[:patientId] != 0
            patients << Patient.find(params[:patientId].to_i)
          end

          @activity.update(:offices => offices, :patients => patients)
          abbreviation = abbreviation_dr_by_doctor(doctor)

          @activity.personalsupports.each{ |personal| InfoPersonalSupports.send_message(personal,abbreviation,doctor,@activity).deliver } if info_personal_support
          data = json_from_model @activity

          @data = { :type => 'new_activity', :activity => data, :in_w_room => false , :is_new => true}


          if @activity.activity_status.id == ActivityStatus::WAITING && @activity.start_date.today?
            @data["in_w_room"] = true;
            @data["w_room_data"] = format_activity_wroom(@activity);
          end

          key = @user.user_name
          notification_private_web key, @data

          status = :ok
          response = format_json_activity(@activity)
          message = "Actividad generada"
          errors = []
        else
          status = 'error'
          response = {}
          message = "Error al crear la actvidad"
          errors = @activity.errors.full_messages
        end

        render :json => {
                   status: status,
                   response: response,
                   message: message,
                   errors: errors
               }, layout: false
      end
    else
      # se actualiza actividad
      @activity = Activity.find(params[:activityId])

      start_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:startDate])
      end_date = ActiveSupport::TimeZone["Mexico City"].parse(params[:endDate])

      #result = exist_rank_date start_date,end_date, @activity.doctor,@activity.id

      if false#result[:result]
        # error, ya existe una actividad en esa fecha
=begin        render :json => {
                   status: 'error',
                   response: {},
                   message: "Error al actualizar la actividad",
                   errors: ['Existe una actividad ya agendada en la misma fecha']
               }, layout: false
=end
      else
        doctor = @user.doctor
        patients = []
        if params[:patientId].present? and params[:patientId] != 0
          @activity.patients.destroy_all
          patients << Patient.find(params[:patientId].to_i)
        end
        if @activity.update(all_day: params[:allDay],
                            name: params[:name],
                            details: params[:details],
                            start_date:start_date,
                            end_date:end_date,
                            patients:patients)

          #if params[:is_owner] == 'true'
          #  offices = get_offices params
          #  Activity.update(@activity.id, :offices => offices)
          #end
          offices = []
          if params[:officeId].present? and params[:officeId].to_i != 0
            offices << Office.find(params[:officeId])
          else
            offices = Office.active_offices.where(doctor_id: doctor.id)
          end
          @activity.offices.destroy_all
          @activity.update(offices: offices)

          if @activity.activity_type.id == ActivityType::SURGERY
            @activity.personalsupports.destroy_all

            if params[:personalSupportsIds].present?
              params[:personalSupportsIds].each do |value|
                @activity.personalsupports << Personalsupport.find(value.to_i)
              end
            end
          end


          act = json_from_model @activity

          @data = { :type => 'new_activity', :activity => act, :in_w_room => false, :is_new => false}

          if @activity.activity_status.id == ActivityStatus::WAITING  && @activity.start_date.today?
            @data["in_w_room"] = true;
            @data["w_room_data"] = format_activity_wroom(@activity);
          end

          key =  @user.user_name
          notification_private_web key, @data

          status = :ok
          response = format_json_activity(@activity)
          message = "Actividad generada"
          errors = []
        else
          status = 'error'
          response = {}
          message = "Error al crear la actvidad"
          errors = @activity.errors.full_messages

        end
        render :json => {
                   status: status,
                   response: response,
                   message: message,
                   errors: errors
               }, layout: false
      end

    end
  end

  def destroy_activity
    @activity = Activity.find(params[:activity_id])

    if @activity.destroy
      @data2 = nil

      @data = { :type => 'delete_activity', :id => params[:activity_id], :activity => @activity }

      channel = @activity.doctor.user.user_name

      notification_private_web channel, @data
      status = :ok
      message = "Actividad eliminada correctamente"
    else
      status = 'error'
      message = "Error al intenta eliminar, intente nuevamente"
    end

    render json: {status: status,
                  response:'Se ha eliminado correctamente',
                  message: message,
                  errors: []}
  end

  def show_activity
    @activity = Activity.find(params[:activity_id])

    activity_json = format_json_activity @activity

    send_response :ok, activity_json,"Detalle de actividad"
  end

  private

  def activity_params
    params.permit(:name, :details, :startDate, :endDate, :allDay)
  end

  def validate_token
    #  {:data => "medsi_movil"}      =>      "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoibWVkc2lfbW92aWwifQ."
    unless request.headers['authorization'].present?
      send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
    else
      begin
        token = request.headers['authorization']
        decoded_token = JWT.decode token, nil, false
        unless decoded_token[0]['data'].to_s === "medsi_movil"
          send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
        end
        rescue
        ## error
        send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
      end
    end
  end

  def set_user
    @user = User.find(params[:user_id])
  end

  def send_response(status,data = {},message="",errors = [])
    respond_with({
                     status: status,
                     response:data,
                     message: message,
                     errors: errors
                 })
  end

end
