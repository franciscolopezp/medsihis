class Api::V1::DoctorsController < ApplicationController
  include Api::V1::DoctorsDoc
  include Api::V1::DoctorsHelper
  include Admin::CitiesHelper
  include Patient::AppointmentsHelper
  include Api::V1::AppointmentsHelper
  include Api::V1::ActivitiesHelper

  skip_before_action :verify_authenticity_token
  before_action :validate_token, except: [:get_city]
  before_action :set_user, except: [:get_city, :office_availability, :offices, :check_availability]
  before_action :set_time_zone_api
  respond_to :json

  def doctors_filter
    array_json_doctors = find_all_doctors params
    send_response :ok, array_json_doctors, "Lista de doctores"
  end

  def offices
    doctor = Doctor.find(params[:doctor_id]) if params[:doctor_id].present?
    doctor = User.find(params[:user_id]).doctor if doctor.nil?

    offices_json = doctor.offices.includes(:city).map do |office|
      {
          officeId: office.id,
          name: office.name,
          address: office.address+', '+office.city.format_city_state_and_country,
          phone: office.telephone
      }
    end

    send_response :ok, offices_json, "Lista de consultorios"
  end

  def check_availability
    response = check_availability_office true
    send_response :ok, response, "Lista de fechas disponibles"
  end

  def office_availability

    data = availability_medical_consultations_mobile params
    send_response :ok, data, "Lista de disponibilidad en el consultorio"
  end

  def get_city
    cities = find_city params[:name] if params[:name].present?
    cities = [] if cities.nil?

    send_response :ok, cities, "Lista de ciudades"
  end

  def show_patients
    patients_json = patients_doctors(params[:patient_name], params[:page])
    send_response :ok, patients_json, "Lista de pacientes"
  end

  def index_favorite_doctors
    doctors_json =  @user.user_patient.doctors.includes(:specialties).map do |doctor|
        {
            doctorId: doctor.id,
            fullName: doctor.full_name_title,
            specialities: doctor.specialties.map{|s| s.name}.join(","),
            profilePictureUrl: url_for(controller: '/index', action: 'picture_doctor', user_id: doctor.user.id),
            isFavorite: '1'
        }
    end

    send_response :ok, doctors_json, "Lista de consultorios"
  end

  def add_favorite
    unless @user.user_patient.doctors.exists?(id: params[:doctor_id])
      @user.user_patient.doctors << Doctor.find(params[:doctor_id])
      render json: {status: :ok,
                    response: {key: "", value: "Exito"},
                    message: "",
                    errors: []}
    else
      render json: {status: 'error',
                    response: {},
                    message: "Error",
                    errors: ["El doctor ya se encuentra agregado"]}
    end
  end

  def remove_favorite
    errors = []
    if @user.user_patient.doctors.delete(Doctor.find(params[:doctor_id]))
      status = :ok
      response = {key: "", value: "Exito"}
    else
      status = 'error'
      errors << "Ocurrio un error, intente nuevamente"
    end

    render json: {status: status,
                  response:response,
                  message: "",
                  errors: errors}
  end


  def search_patient
    result = find_patient_by_name params[:name]
    send_response :ok, result, "Lista de paciente"
  end

  def search_personal_support
    result = find_personal_support_by_name params[:name]
    send_response :ok, result, "Lista de personal"
  end

  private

  def validate_token
    #  {:data => "medsi_movil"}      =>      "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoibWVkc2lfbW92aWwifQ."
    unless request.headers['authorization'].present?
      send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
    else
      begin
        token = request.headers['authorization']
        decoded_token = JWT.decode token, nil, false
        unless decoded_token[0]['data'].to_s === "medsi_movil"
          send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
        end
      rescue
        ## error
        send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
      end
    end
  end

  def set_user
    @user = User.find(params[:user_id])
  end

  def send_response(status,data = {},message="",errors = [])
    respond_with({
                     status: status,
                     response:data,
                     message: message,
                     errors: errors
                 })
  end
end
