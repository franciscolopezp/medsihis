require 'jwt'

class Api::V1::UsersController < ApplicationController
  include Api::V1::UsersDoc
  include Api::V1::UsersHelper
  include Doctor::ActivitiesHelper
  include Api::V1::ActivitiesHelper

  skip_before_action :verify_authenticity_token
  before_action :validate_token, except: [:login, :create_patient]
  before_action :set_user, except: [:login, :set_user, :create_patient]
  before_action :set_time_zone_api
  respond_to :json

  def login
    result = validate_access params[:user_name], params[:password]
    send_response(result[:status],result[:response],result[:message],result[:errors])
  end

  def create_patient
    patient = UserPatient.new
    params_patient = params
    patient.name = params_patient[:name]
    patient.last_name = params_patient[:lastName]
    patient.birth_date = params_patient[:birthDate]
    patient.phone = params_patient[:telephone]
    patient.city = City.find(params_patient[:cityId])
    patient.premium_account = false
    patient.accept_term_conditions = true
    patient.is_active = true
    patient.gender= Gender.find(params_patient[:genderId])

    user_patient = User.new
    user_patient.user_name = params_patient[:userName]
    user_patient.email = params_patient[:email]
    user_patient.password = params_patient[:password]
    user_patient.is_doctor = false
    patient.user = user_patient
    if patient.save
      render json: {
                 status: :ok,
                 response:{name: patient.name ,
                           lastName: patient.last_name,
                           birthDate: patient.birth_date,
                           isPremium: patient.premium_account,
                           email: patient.user.email,
                           userName: patient.user.user_name,
                           password: "",
                           telephone: patient.phone,
                           cityId: patient.city.id,
                           cityName: patient.city.name
                  },
                 message: "",
                 errors: []
             }, layout: false
    else
      render json: {
                 status: 'error',
                 response:{},
                 message: "Ocurrio un problema",
                 errors: patient.errors.full_messages
             }, layout: false
    end

  end

  def update_profile
   result = update_profile_patient_doctor params
   render json: result, layout: false
  end

  def create_token
    # notificaciones para google
    if !@user.user_tokens.exists?(token: params[:messages])
      user_token = UserToken.new(user: @user, token: params[:messages])
      if user_token.save
        render json: {
                   status: :ok,
                   response:{id:0, userId:@user.id, title:"", messages: "Token guardado", time:Time.now, status:0},
                   message: "",
                   errors: []
               }, layout: false
      else
        render json: {
                   status: :ok,
                   response:{id:0, userId:@user.id, title:"", messages: user_token.errors.full_messages.join(","), time:Time.now, status:0},
                   message: "",
                   errors: []
               }, layout: false
      end
    else
      render json: {
                 status: :ok,
                 response:{id:0, userId:@user.id, title:"", messages: "Token guardado", time:Time.now, status:0},
                 message: "",
                 errors: []
             }, layout: false
    end
  end

  private

    def validate_token
      #  {:data => "medsi_movil"}      =>      "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoibWVkc2lfbW92aWwifQ."
      unless request.headers['authorization'].present?
        send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
      else
        begin
          token = request.headers['authorization']
          decoded_token = JWT.decode token, nil, false
          unless decoded_token[0]['data'].to_s === "medsi_movil"
            send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
          end
        rescue
          ## error
          send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
        end
      end
    end

    def set_user
      @user = User.find(params[:user_id])
    end

    def send_response(status,data = {},message="",errors = [])
      respond_with({
                       status: status,
                       response:data,
                       message: message,
                       errors: errors
                   })
    end
end
