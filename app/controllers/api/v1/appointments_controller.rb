require 'jwt'

class Api::V1::AppointmentsController < ApplicationController
  include Api::V1::AppointmentsDoc
  include Api::V1::AppointmentsHelper
  include Api::V1::ActivitiesHelper
  include Doctor::ActivitiesHelper
  include NotificationHelper
  include Doctor::ActivitiesHelper

  skip_before_action :verify_authenticity_token
  before_action :validate_token
  before_action :set_user
  before_action :set_time_zone_api
  respond_to :json


  def index
   response = get_appointments_by_user @user, params
   send_response :ok, response
  end

  def create
    result = create_appointment params

    if !result.nil?
      render :json => {
                 status: 'error',
                 response:{},
                 message: "Error al crear la cita",
                 errors: result
             }, layout: false
    end

    office = Office.find(params[:office_id])
    doctor = office.doctor

    res = exist_rank_date(@activity.start_date,@activity.end_date,doctor,0)
    if res[:result]

      render :json => {
                 status: 'error',
                 response:{},
                 message: "Error al crear la cita",
                 errors: ['Por favor verifique la disponibilidad nuevamente.']
             }, layout: false
    else
      if @activity.save
        offices = get_offices params
        patient = @user.user_patient
        Activity.update(@activity.id, :offices => offices, :user_patients => [patient])

        activity_json = format_json_activity @activity

        # notificar al doctor de la nueva cita en web y la app
        data = json_from_model @activity
        message_doctor_web = { :type => 'new_activity', :activity => data, :in_w_room => false , :is_new => true}

        message_app = message_create_appointment_app @activity
        notification_all(doctor.user.user_name, message_doctor_web, @activity.doctor.user, message_app)
       # notification_private_web doctor.user.user_name, message_doctor_web

        render :json => {
                   status: :ok,
                   response:activity_json,
                   message: "Cita creada correctamente, en espera de confirmación",
                   errors: []
               }, layout: false
      else
        render :json => {
                   status: 'error',
                   response:{},
                   message: "Error al crear la cita",
                   errors: ['Ocurrio un problema al crear la cita.']
               }, layout: false
      end
    end
  end

  private

    def validate_token
      #  {:data => "medsi_movil"}      =>      "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoibWVkc2lfbW92aWwifQ."
      unless request.headers['authorization'].present?
        send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
      else
        begin
          token = request.headers['authorization']
          decoded_token = JWT.decode token, nil, false
          unless decoded_token[0]['data'].to_s === "medsi_movil"
            send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
          end
        rescue
          ## error
          send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
        end
      end
    end

    def set_user
      @user = User.find(params[:user_id])
    end

    def send_response(status,data = {},message="",errors = [])
      respond_with({
                       status: status,
                       response:data,
                       message: message,
                       errors: errors
                   })
    end
end
