class Api::V1::ExpedientsController < ApplicationController
  include Api::V1::ExpedientsHelper
  include Api::V1::ActivitiesHelper

  skip_before_action :verify_authenticity_token
  before_action :validate_token
  before_action :set_time_zone_api
  respond_to :json


  def expedient_info_patient
    expedient_json = expedient_info(params[:patient_id])
    send_response :ok, expedient_json, "Expediente - Datos generales"
  end

  def expedient_consultation_patient
    expedient_json = full_expedient_consultation(params[:consultation_id])
    send_response :ok, expedient_json, "Expediente - Consultas"
  end

  def consultation_data
    expedient_json = expedient_consultation(params[:patient_id])
    send_response :ok, expedient_json, "Expediente - Consultas"
  end

  private

  def validate_token
    #  {:data => "medsi_movil"}      =>      "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoibWVkc2lfbW92aWwifQ."
    unless request.headers['authorization'].present?
      send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
    else
      begin
        token = request.headers['authorization']
        decoded_token = JWT.decode token, nil, false
        unless decoded_token[0]['data'].to_s === "medsi_movil"
          send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
        end
      rescue
        ## error
        send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
      end
    end
  end

  def set_user
    @user = User.find(params[:user_id])
  end

  def send_response(status,data = {},message="",errors = [])
    respond_with({
                     status: status,
                     response:data,
                     message: message,
                     errors: errors
                 })
  end
end
