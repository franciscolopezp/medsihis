require 'jwt'

class Api::V1::HealthHistoriesController < ApplicationController
  include Api::V1::UsersDoc
  include Api::V1::HealthHistoriesHelper
  include Doctor::ActivitiesHelper
  include Api::V1::ActivitiesHelper

  skip_before_action :verify_authenticity_token
  before_action :validate_token
  before_action :set_user, except: [:health_histories_expedient, :health_histories_consultations, :medical_history_expedient, :observations_expedient, :laboratory_orders_expedient]
  before_action :set_time_zone_api
  respond_to :json

  def index
    hh_json = []
    if @user.user_patient.premium_account
      hh_json = @user.user_patient.health_histories.map{|hh| {id: 0, healthHistoryId: hh.id, name: hh.name, userId: @user.id}}
    else
      #first_hh = @user.user_patient.health_histories.order(:created_at).first  # version con cuenta premium
      #hh_json << {id: 0, healthHistoryId: first_hh.id, name: first_hh.name, userId: @user.id}
      first_hh = @user.user_patient.health_histories.order(:created_at)
      hh_json = first_hh.map{|hh| {id: 0, healthHistoryId: hh.id, name: hh.name, userId: @user.id}}
    end

   send_response :ok, hh_json, "Historial de salud"
  end

  def create
    result = create_health_histories
    render json: result
  end

  def update
    history = HealthHistory.find(params[:healthHistoryId])
    response = {}
    errors = []
    if history.update(name: params[:name])
      status = :ok
      response = {id: 0, healthHistoryId: history.id, name: history.name, userId: @user.id}
    else
      status = 'error'
      errors << "Ocurrio un error, intente nuevamente"
    end

    render json: {status: status,
                  response:response,
                  message: "",
                  errors: errors}
  end

  def destroy
    history = HealthHistory.find(params[:health_history_id])
    response = {}
    errors = []
    if history.destroy
      status = :ok
      response = {key: "", value: "Exito"}
    else
      status = 'error'
      errors << "Ocurrio un error, intente nuevamente"
    end
    render json: {status: status,
                  response:response,
                  message: "",
                  errors: errors}
  end

  def health_histories_expedient
    medical_expedients = expedients_by_health_history params[:health_history_id]
    send_response :ok, medical_expedients, "Lista de expedientes"
  end

   def medical_history_expedient
     #expedient =  MedicalExpedient.find(params[:expedient_id])
     expedient_json = format_medical_history params[:patient_id]

     send_response :ok, expedient_json, "Antecedentes médicos"
   end

  def observations_expedient
    patient = Patient.find(params[:patient_id])
    send_response :ok, patient.medical_expedient.general_observations, "Antecedentes médicos"
  end

  def laboratory_orders_expedient
    patient = Patient.find(params[:patient_id])

    puts "xxxxxxxxx"
    orders = patient.medical_expedient.clinical_analysis_orders.map{|order|{
        folio: order.folio,
        date: order.date_order,
        type: order.clinical_order_type.name,
        studies: order.clinical_studies.map{|s|s.name}.join('\n')
    }}
    puts orders.size
    send_response :ok, orders, "Antecedentes médicos"
  end

  def health_histories_consultations
    expedient = MedicalExpedient.find(params[:expedient_id])
    medical_consultations = expedient.medical_consultations.map{|consultation|
      {
          nameDoctor: expedient.doctor.full_name,
          dateConsultation: consultation.date_consultation,
          recommendations: consultation.prescription.recommendations,
          recipes: consultation.prescription.medical_indications.map{|medical_indication| {consultationId:consultation.id, id:medical_indication.medicament.id , name:medical_indication.medicament.comercial_name.to_s+" - "+medical_indication.medicament.presentation, dose: medical_indication.dose, expedientId:expedient.id}},
          expedientId: expedient.id,
          consultationId: consultation.id,
          userId: 0
      }
    }
    send_response :ok, medical_consultations, "Lista de consultas"
  end

  def valid_medical_expedient
    user_patient = @user.user_patient

    if MedicalExpedient.exists?(code: params[:code])
      expedient = MedicalExpedient.includes(:patient).find_by(code: params[:code])
      patient = expedient.patient
      if ExpedientRequest.by_patient(user_patient.id).exists?(medical_expedient_id: expedient.id)
        # ya se a vinculado
        # buscar el estatus del request
        expedient_request = ExpedientRequest.by_patient(user_patient.id).find_by(medical_expedient_id: expedient.id)
        errors = []
        status = :ok
        case expedient_request.status
          when ExpedientRequest::PENDING
            status = 'error'
            errors << "El expediente #{patient.full_name} está en espera de aprobación."
          when ExpedientRequest::APPROVED
            status = 'error'
            errors << "El expediente #{patient.full_name} ha sido vinculado previamente a su cuenta."
          when ExpedientRequest::REJECTED
            status = 'error'
            errors << "El expediente #{patient.full_name} no ha sido vinculado a su cuenta."
        end
        # mandar como error si el request ya existe y se encuentra en un estatus.
        send_response status, {}, "Expediente de #{patient.full_name}", errors
      else
        # validar si ya tiene maximo de 3 expedientes al usuario
        if user_patient.premium_account
          # no se a solicitado la vinculación
          send_response :ok, {key:"¿Esta seguro que desea agregar el expediente #{patient.full_name}?" , value: ""}, "Expediente de #{patient.full_name}"
        else
          if user_patient.medical_expedients.size > 3
            send_response 'error', {}, "MEDSI", ["Actualmente cuenta con una versión free de MEDSI, le recomendamos comprar la versión premium para vincular más expedientes clínicos"]
          else
            # no se a solicitado la vinculación
            send_response :ok, {key:"¿Esta seguro que desea agregar el expediente #{patient.full_name}?" , value: ""}, "Expediente de #{patient.full_name}"
          end
        end
      end
    else
      send_response 'error', {}, "Ocurrio un problema", ["El código #{params[:code]} no existe"]
    end
  end

  def link_medical_expedient
    expedient = MedicalExpedient.includes(:doctor, :patient).find_by(code: params[:code])
    doctor = expedient.doctor
    patient = expedient.patient

    expedient_request = ExpedientRequest.new(medical_expedient: expedient, health_history_id: params[:health_history_id], status: ExpedientRequest::PENDING)

    if expedient_request.save
      send_response :ok, {key:"Solicitud de vinculación enviada al #{doctor.full_name_title} " , value: ""}, "Expediente de #{patient.full_name}"
    else
      send_response 'error', {key:"" , value: ""}, "Ocurrio un problema", expedient_request.errors.full_messages
    end
    # {key:"Se a enviado correctamente el expediente" , value: ""}
  end

  def unlink_medical_expedient
    # expedientId, historyMedicalId
    request = ExpedientRequest.find_by(health_history_id: params[:health_history_id], medical_expedient_id: params[:medical_expedient_id])
    response = {}
    errors = []
    if request.destroy
      status = :ok
      response = {key: "", value: "Exito"}
    else
      status = 'error'
      errors << "Ocurrio un error, intente nuevamente"
    end
    render json: {status: status,
                  response:response,
                  message: "",
                  errors: errors}
  end

  private

    def validate_token
      #  {:data => "medsi_movil"}      =>      "eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJkYXRhIjoibWVkc2lfbW92aWwifQ."
      unless request.headers['authorization'].present?
        send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
      else
        begin
          token = request.headers['authorization']
          decoded_token = JWT.decode token, nil, false
          unless decoded_token[0]['data'].to_s === "medsi_movil"
            send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
          end
        rescue
          ## error
          send_response 'error', {}, "Ups tenemos problemas", ['Es imposible determinar su autenticidad.']
        end
      end
    end

    def set_user
      @user = User.find(params[:user_id])
    end

    def send_response(status,data = {},message="",errors = [])
      respond_with({
                       status: status,
                       response:data,
                       message: message,
                       errors: errors
                   })
    end
end
