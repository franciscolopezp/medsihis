json.array!(@office) do |office|
  json.extract! office, :id, :name, :address, :telephone
  json.url admin_medicament_url(office, format: :json)
end
