json.array!(@assistant) do |assistant|
  json.extract! assistant, :id
  json.url doctor_patient_url(assistant, format: :json)
end
