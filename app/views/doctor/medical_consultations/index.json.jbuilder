json.array!(@doctor_medical_consultations) do |doctor_medical_consultation|
  json.extract! doctor_medical_consultation, :id
  json.url doctor_medical_consultation_url(doctor_medical_consultation, format: :json)
end
