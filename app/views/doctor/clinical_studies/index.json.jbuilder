json.array!(@clinical_study) do |clinical_study|
  json.extract! clinical_study, :id, :name
  json.url admin_specialty_url(clinical_study, format: :json)
end
