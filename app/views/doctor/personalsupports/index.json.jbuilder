json.array!(@personal) do |personal|
  json.extract! personal, :id, :name, :lastname, :email, :contact_number
  json.url admin_medicament_url(personal, format: :json)
end
