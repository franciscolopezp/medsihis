json.array!(@attention_type) do |medicament|
  json.extract! attention_type, :id, :name
  json.url admin_medicament_url(attention_type, format: :json)
end
