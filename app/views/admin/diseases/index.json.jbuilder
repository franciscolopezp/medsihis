json.array!(@disease) do |disease|
  json.extract! disease, :id, :name, :code
  json.url admin_disease_url(disease, format: :json)
end
