json.array!(@rol) do |role|
  json.extract! role, :id, :name
  json.url admin_operation_role_path(rol, format: :json)
end
