json.array!(@laboratory) do |laboratory|
  json.extract! laboratory, :id
  json.url admin_laboratory_url(laboratory, format: :json)
end
