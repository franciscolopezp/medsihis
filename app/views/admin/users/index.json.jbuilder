json.array!(@user) do |user|
  json.extract! user, :id, :user_name, :password, :email
  json.url admin_user_url(user, format: :json)
end
