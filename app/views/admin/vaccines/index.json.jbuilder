json.array!(@doctor_vaccines) do |doctor_vaccine|
  json.extract! doctor_vaccine, :id, :name, :prevent_disease, :dose, :age_or_frecuency
  json.url admin_vaccine_url(doctor_vaccine, format: :json)
end
