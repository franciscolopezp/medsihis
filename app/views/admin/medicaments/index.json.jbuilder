json.array!(@medicament) do |medicament|
  json.extract! medicament, :id, :comercial_name, :presentation, :active_substance, :contraindication, :brand
  json.url admin_medicament_url(medicament, format: :json)
end
