json.array!(@specialty) do |specialty|
  json.extract! specialty, :id, :name
  json.url admin_specialty_url(specialty, format: :json)
end
