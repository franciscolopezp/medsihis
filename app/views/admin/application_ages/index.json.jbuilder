json.array!(@application_age) do |application_age|
  json.extract! application_age, :id, :age, :age_type, :dose, :comments
  json.url admin_application_age_url(application_age, format: :json)
end
