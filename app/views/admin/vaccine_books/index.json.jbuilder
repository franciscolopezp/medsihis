json.array!(@vaccine_book) do |vaccine_book|
  json.extract! vaccine_book, :id, :name, :gender, :start_age, :end_age
  json.url admin_vaccine_url(vaccine_book, format: :json)
end
