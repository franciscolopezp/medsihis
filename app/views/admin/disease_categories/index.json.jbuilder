json.array!(@disease_category) do |disease_category|
  json.extract! disease_category, :id, :name, :code
  json.url admin_disease_category_url(disease_category, format: :json)
end
