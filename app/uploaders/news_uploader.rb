# encoding: utf-8

class NewsUploader < CarrierWave::Uploader::Base

  storage :file

  def store_dir
    Rails.root.join('uploads')+"news/#{model.id}/#{mounted_as}/"
  end


end
