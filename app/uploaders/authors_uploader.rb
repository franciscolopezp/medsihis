# encoding: utf-8

class AuthorsUploader < CarrierWave::Uploader::Base

  storage :file

  def store_dir
    Rails.root.join('uploads')+"author#{model.id}/#{mounted_as}/"
  end


end
