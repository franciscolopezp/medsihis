# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'
# librerias js para el visualizador de documentos
Rails.application.config.assets.precompile += %w( viewFiles/mozillapdf/pdf.worker.js )
Rails.application.config.assets.precompile += %w( viewjs/viewer.css.erb )
Rails.application.config.assets.precompile += %w( viewFiles/viewpdf/locale/locale.properties )
# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
#include all images in vendor
Rails.application.config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif)
#include all fonts
Rails.application.config.assets.precompile += %w(*.svg *.eot *.woff *.ttf)

Rails.application.config.assets.precompile += %w( doctor/index.js doctor/index.css admin/index.css admin/index.js)
Rails.application.config.assets.precompile += %w( admin/*.js admin/*.css )
Rails.application.config.assets.precompile += %w( doctor/*.js doctor/*.css )
Rails.application.config.assets.precompile += %w( cli/*.js cli/*.css )
Rails.application.config.assets.precompile += %w( sessions.js sessions.css.erb )

# index
Rails.application.config.assets.precompile += %w(index.js index_gmap.js index.css news.js news.css index_main.js index_main.css new_chat_main.js new_chat_main.css)
# patient
Rails.application.config.assets.precompile += %w( patient/index.js )
Rails.application.config.assets.precompile += %w( patient/index.css )
Rails.application.config.assets.precompile += %w( patient/*.js )
Rails.application.config.assets.precompile += %w( patient/*.css )
Rails.application.config.assets.precompile += %w( socket_doctor.js )
Rails.application.config.assets.precompile += %w( socket_assistant.js )
Rails.application.config.assets.precompile += %w( appointments_actions.js )
Rails.application.config.assets.precompile += %w( date-helper.js )
Rails.application.config.assets.precompile += %w( doctor/calendar-doctor.js )

Rails.application.config.assets.precompile += %w( shared_assistant/index.js )
Rails.application.config.assets.precompile += %w( shared_assistant/index.css )
Rails.application.config.assets.precompile += %w( shared_assistant/*.js )
Rails.application.config.assets.precompile += %w( shared_assistant/*.css )


Rails.application.config.assets.precompile += ["pdf/prescription_a.css"]
Rails.application.config.assets.precompile += ["pdf/prescription_b.css"]
Rails.application.config.assets.precompile += ["pdf/analysis_order.css"]
Rails.application.config.assets.precompile += ["pdf/medical_expedient.css"]
Rails.application.config.assets.precompile += ["pdf/account_balance.css"]
Rails.application.config.assets.precompile += ["pdf/invoice.css"]
Rails.application.config.assets.precompile += ["pdf/vaccines.css"]

Rails.application.config.assets.precompile += ["pdf/certificate.css"]


