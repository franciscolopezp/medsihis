Apipie.configure do |config|
  config.app_name                = "Medsi"
  config.copyright = "&copy; 2016 Stj2"
  config.api_base_url            = "/api"
  config.doc_base_url            = "/docs"
  # where is your API defined?
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/**/*.rb"
  config.app_info["1.0"] = "
   Servicios web para Medsi.
  "
end
