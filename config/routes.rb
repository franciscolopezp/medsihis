Rails.application.routes.draw do

  root to: 'index#index'
  get 'index' => 'index#index'
  post 'submit_contact_form' => 'index#submit_contact'
  get 'directorio' => 'index#medical_directory'
  get 'nuevo_chat' => 'index#new_chat' #usuario
  get 'enviar_mensaje' => 'index#send_message', as: 'send_message' #usuario
  get 'caracteristicas' => 'index#load_features', as: 'load_feature'
  get 'search_results' => 'index#search_results'
  get 'get_doctor_info' => 'index#get_doctor_info'
  get 'medsi_pdf_info' => 'index#medsi_pdf'
  get 'aviso_de_privacidad' => 'index#aviso_de_privacidad'
  post 'register_user' => 'index#register_user'
  post 'register_patient' => 'index#register_user_patient'
  get 'check_user' => 'index#check_user'
  get 'reset_doctor_password' => 'sessions#reset_doctor_password', as: 'reset_doctor_password'
  get 'send_password_mail' => 'sessions#send_password_mail'
  get 'reset_password' => 'sessions#reset_password'
  get 'picture_doctor' => 'index#picture_doctor'
  get 'picture_patient' => 'index#picture_patient'

  get 'login' => 'sessions#new',as: 'login_user'
  post 'login' => 'sessions#create'
  get 'logout' => 'sessions#destroy'

  get 'e401' => 'application#unauthorized_view_render'

  resources :news, :path => 'noticias', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }

  namespace :cli do
    get 'index' => 'index#index'
    resources :tickets, :path => 'tickets', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'pago_admision/:id', to: 'tickets#admission_transaction', as: 'admission_transaction'
        get 'otros_pagos/:id', to: 'tickets#other_payments', as: 'other_payments'
      end
    end
    resources :emisor_fiscal_informations, :path => 'emisor_informacion_fiscal', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :suppliers, :path => 'proveedores', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :service_packages, :path => 'paquetes_servicios', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'manage_services/:id', to: 'service_packages#manage_services', as: 'manage_services'
        get 'add_service', to: 'service_packages#add_service', as: 'add_service'
        get 'delete_service', to: 'service_packages#delete_service', as: 'delete_service'

      end
    end
    resources :waste_types, :path => 'tipos_salida', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :insurances, :path => 'aseguradoras', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'prices', to: 'insurances#prices', as: 'prices'
        get 'get_services', to: 'insurances#get_services', as: 'get_services'
        get 'set_prices', to: 'insurances#set_prices', as: 'set_prices'
        get 'save_price', to: 'insurances#save_price', as: 'save_price'
        get 'active_insurence', to: 'insurances#active_insurence', as: 'active_insurence'
      end
    end
    resources :other_charges, :path => 'Generar_cobros', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'search_charges', to: 'other_charges#search_charges', as: 'search_charges'
        get 'charges_report', to: 'other_charges#charges_report', as: 'charges_report'
        get 'save_other_charge', to: 'other_charges#save_other_charge', as: 'save_other_charge'
        get 'get_charge_items', to: 'other_charges#get_charge_items', as: 'get_charge_items'
        get 'pay_other_charge', to: 'other_charges#pay_other_charge', as: 'pay_other_charge'
        get 'cancel_payment', to: 'other_charges#cancel_payment', as: 'cancel_payment'
        get 'close_other_charge', to: 'other_charges#close_other_charge', as: 'close_other_charge'
        get 'check_cancel_permission', to: 'other_charges#check_cancel_permission', as: 'check_cancel_permission'
        get 'check_close_permission', to: 'other_charges#check_close_permission', as: 'check_close_permission'
        get 'export_charges', to: 'other_charges#export_charges', as: 'export_charges'
        get 'print_charge_report', to: 'other_charges#print_charge_report', as: 'print_charge_report'
      end
    end
    resources :pharmacy_items, :path => 'articulos_farmacia', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'search_pharmacy_item', to: 'pharmacy_items#find_pharmacy_item', as: 'search_pharmacy_item'
      end
    end

    resources :medicament_purchases, :path => 'compra_medicamentos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'save_purchase', to: 'medicament_purchases#save_purchase', as: 'save_purchase'
        get 'get_lot', to: 'medicament_purchases#get_lot', as: 'get_lot'
        get 'get_lot_medicament', to: 'medicament_purchases#get_lot_medicament', as: 'get_lot_medicament'
        get 'get_last_folio', to: 'medicament_purchases#get_last_folio', as: 'get_last_folio'

      end
    end
    resources :medicament_movements, :path => 'movimientos_medicamentos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'pharmacy_movements', to: 'medicament_movements#pharmacy_movements', as: 'pharmacy_movements'
        post 'pharmacy_movement_save', to: 'medicament_movements#pharmacy_movement_save', as: 'pharmacy_movement_save'
      end
    end
    resources :medicament_wastes, :path => 'mermas_medicamentos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'check_quantity', to: 'medicament_wastes#check_quantity', as: 'check_quantity'
        get 'check_quantity_pharmacy', to: 'medicament_wastes#check_quantity_pharmacy', as: 'check_quantity_pharmacy'
        get 'pharmacy_wastes', to: 'medicament_wastes#pharmacy_wastes', as: 'pharmacy_wastes'
        post 'pharmacy_waste_save', to: 'medicament_wastes#pharmacy_waste_save', as: 'pharmacy_waste_save'
      end
    end
    resources :hospitalizations, :path => 'hospitalizaciones', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        post 'save_medical_valoration', to: 'hospitalizations#save_medical_valoration', as: 'save_medical_valoration'
        post 'save_medical_annotation', to: 'hospitalizations#save_medical_annotation', as: 'save_medical_annotation'
        get 'load_annotation_fields', to: 'hospitalizations#load_annotation_fields', as: 'load_annotation_fields'
        get 'imprimir/:id', to: 'hospitalizations#print', as: 'print'
      end
    end
    resources :medical_annotation_images, :path => 'imagenes_nota_medica', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'descarga/:id', to: 'medical_annotation_images#download', as: 'download'
      end
    end
    resources :medical_annotation_files, :path => 'archivos_nota_medica', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'descarga/:id', to: 'medical_annotation_files#download', as: 'download'
      end
    end
    resources :medical_annotations, :path => 'notas_medicas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'imprimir/:id', to: 'medical_annotations#print', as: 'print'
      end
    end
     resources :tooths, :path => 'odontología', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
       collection do
         get 'save_odontogram', to: 'tooths#save_odontogram', as: 'save_odontogram'
         get 'get_original_odontogram', to: 'tooths#get_original_odontogram', as: 'get_original_odontogram'
         get 'get_teeth_historial', to: 'tooths#get_teeth_historial', as: 'get_teeth_historial'
         get 'get_last_data', to: 'tooths#get_last_data', as: 'get_last_data'

       end
     end
    resources :admission_invoices, :path => 'facturas_admisiones', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'get_receptor_email', to: 'admission_invoices#get_receptor_email', as: 'get_receptor_email'
        get 'print_admission_invoice_pdf/:id', to: 'admission_invoices#print_admission_invoice_pdf', as: 'print_admission_invoice_pdf'
        get 'download_admission_invoice_xml/:id', to: 'admission_invoices#download_admission_invoice_xml', as: 'download_admission_invoice_xml'
        get 'cancel_admission_invoice', to: 'admission_invoices#cancel_admission_invoice', as: 'cancel_admission_invoice'
        get 'send_invoice_email', to: 'admission_invoices#send_invoice_email', as: 'send_invoice_email'

      end
    end
     resources :hospital_admissions, :path => 'admisiones_hospitalarias', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'print_pdf_admission/:id', to: 'hospital_admissions#print_pdf_admission', as: 'print_pdf_admission'
        get 'get_services_select', to: 'hospital_admissions#get_services_select', as: 'get_services_select'
        get 'stamp_admission_invoice', to: 'hospital_admissions#stamp_admission_invoice', as: 'stamp_admission_invoice'
        get 'get_payments', to: 'hospital_admissions#get_payments', as: 'get_payments'
        get 'get_emisor_info', to: 'hospital_admissions#get_emisor_info', as: 'get_emisor_info'
        get 'close_admission', to: 'hospital_admissions#close_admission', as: 'close_admission'
        get 'cancel_payment_admission', to: 'hospital_admissions#cancel_payment_admission', as: 'cancel_payment_admission'
        get 'closed_admission', to: 'hospital_admissions#closed_admission', as: 'closed_admission'
        get 'get_service_cost', to: 'hospital_admissions#get_service_cost', as: 'get_service_cost'
        get 'generate_cost', to: 'hospital_admissions#generate_cost', as: 'generate_cost'
        get 'get_residue', to: 'hospital_admissions#get_residue', as: 'get_residue'
        get 'generate_payment', to: 'hospital_admissions#generate_payment', as: 'generate_payment'
        get 'generate_discount', to: 'hospital_admissions#generate_discount', as: 'generate_discount'
        get 'stamp', to: 'hospital_admissions#stamp', as: 'stamp'
        get 'imprimir_factura_pdf/:id', to: 'hospital_admissions#print_pdf', as: 'print_pdf'
        get 'descarga_factura_xml/:id', to: 'hospital_admissions#download_xml', as: 'invoice_download_xml'
        get 'open_invoice_admission/:id',to: 'hospital_admissions#open_invoice_admission', as: 'open_invoice_admission'
        get 'cancel_invoice', to: 'hospital_admissions#cancel_invoice', as: 'cancel_invoice'
        get 'get_receptor_email', to: 'hospital_admissions#get_receptor_email', as: 'get_receptor_email'
        get 'send_invoice_email', to: 'hospital_admissions#send_invoice_email', as: 'send_invoice_email'
        get 'imprimir/:type/:item_id', to: 'hospital_admissions#print', as: 'print'
        get 'update_service_discount', to: 'hospital_admissions#update_service_discount', as: 'update_service_discount'
      end
     end
    resources :hospital_service_categories, :path => 'categorías_servicios_medicos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :hospital_services, :path => 'Servicios_hospitalarios', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'services_report', to: 'hospital_services#services_report', as: 'services_report'
        get 'search_services', to: 'hospital_services#search_services', as: 'search_services'
        get 'export_services', to: 'hospital_services#export_services', as: 'export_services'
        get 'print_services_report', to: 'hospital_services#print_services_report', as: 'print_services_report'
      end
    end
    resources :medicament_assortments, :path => 'Surtido_medicamentos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'save_assortment', to: 'medicament_assortments#save_assortment', as: 'save_assortment'
        get 'get_lot', to: 'medicament_assortments#get_lot', as: 'get_lot'
        get 'get_last_folio', to: 'medicament_assortments#get_last_folio', as: 'get_last_folio'
        get 'get_item_cost', to: 'medicament_assortments#get_item_cost', as: 'get_item_cost'

      end
    end
  end

  namespace :doctor do
    resources :exp_medical_histories, :path => 'historia_clinica', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :expedients, :path => 'exp', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'hospitalizaciones/:expedient_id', to:'expedients#hospital_admissions', as:'hospital_admissions'
        get 'antecedentes/:expedient_id', to:'expedients#medical_history', as:'medical_history'
        get 'aparatos_sistemas/:expedient_id', to:'expedients#systems', as:'systems'
        get 'cartillas_vacunacion/:expedient_id', to:'expedients#vaccine_books', as:'vaccine_books'
        get 'consultas/:expedient_id', to:'expedients#consultations', as:'consultations'
        get 'estudios_laboratorio/:expedient_id', to:'expedients#clinical_studies', as:'clinical_studies'
        get 'lista_consultas/:expedient_id', to:'expedients#consultations_list', as:'consultations_list'
        get 'formulario_nueva_consulta/:expedient_id', to:'expedients#consultations_new', as:'new_consultation'
        get 'nueva_valoracion', to:'expedients#consultations_valoration_fields', as:'new_valoration'
      end
    end
    resources :consultation_types do
      collection do
        get 'update_consultation_cost', to:'consultation_types#update_consultation_cost', as:'update_consultation_cost'
      end
    end
  resources :statistics, :path => 'estadisticas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
    collection do
      get 'doctor_charts', to: 'statistics#doctor_charts', as: 'doctor_charts'
      get 'comparative_chart', to: 'statistics#comparative_chart', as: 'comparative_chart'
      get 'comparative_chart_egress', to: 'statistics#comparative_chart_egress', as: 'comparative_chart_egress'
      get 'comparative_chart_ingress', to: 'statistics#comparative_chart_ingress', as: 'comparative_chart_ingress'
      get 'comparative_chart_consults', to: 'statistics#comparative_chart_consults', as: 'comparative_chart_consults'
      get 'comparative_charts', to: 'statistics#comparative_charts', as: 'comparative_charts'
      get 'get_genres', to: 'statistics#get_genres', as: 'get_genres'
      get 'get_cities', to: 'statistics#get_cities', as: 'get_cities'
      get 'consults_chart_data', to: 'statistics#consults_chart_data', as: 'consults_chart_data'
      get 'transacs_chart_data', to: 'statistics#transacs_chart_data', as: 'transacs_chart_data'
      get 'diseases', to: 'statistics#graph_diseases', as: 'diseases'
      get 'download_diseases', to: 'statistics#pdf_graph_diseases', as: 'download_diseases'
      get 'enfermedades', to: 'statistics#statistics_graph_diseases', as: 'graph_diseases'
    end
  end
  resources :invoices, :path => 'facturas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }  do
  collection do
    get 'cancelar_factura', to: 'invoices#cancel_invoice', as: 'cancel_invoice'
    get 'imprimir_factura_pdf/:id', to: 'invoices#print_pdf', as: 'print_pdf'
    get 'descarga_factura_xml/:id', to: 'invoices#download_xml', as: 'invoice_download_xml'
    get 'obtener_receptor_email', to: 'invoices#get_receptor_email', as: 'get_receptor_email'
    get 'facturas_abiertas', to: 'invoices#open_invoices', as: 'open_invoices'
    get 'enviar_factura_email', to: 'invoices#send_invoice_email', as: 'send_invoice_email'
    get 'imprimir_reporte_pdf', to: 'invoices#print_pdf_report', as: 'print_pdf_report'
  end
  end

    resources :ultrasounds, :path => 'ultrasonidos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }

    resources :address_books, :path => 'contactos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
    collection do
      get 'get_contact_data', to: 'address_books#get_contact_data', as: 'get_contact_data'
    end
  end
  resources :transactions, :path => 'transaccion', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }  do
    collection do
      get 'timbrar', to: 'transactions#timbrar', as: 'timbrar'
      get 'cancelar_factura', to: 'transactions#cancel_invoice', as: 'cancel_invoice'
      get 'timbrar_factura_abierta', to: 'transactions#stamp_open_invoice', as: 'stamp_open_invoice'
      get 'obtener_receptor_email', to: 'transactions#get_receptor_email', as: 'get_receptor_email'
      get 'enviar_factura_email', to: 'transactions#send_invoice_email', as: 'send_invoice_email'
      get 'abrir_factura', to: 'transactions#open_invoice', as: 'open_invoice'
      get 'obtener_informacion_fiscal', to: 'transactions#getFiscalInfos', as: 'get_fiscal_infos'
      get 'enviar_informacion_fiscal', to: 'transactions#setFiscalInfo', as: 'set_fiscal_infos'
      get 'obtener_informacion_fiscal_doc', to: 'transactions#getDocFiscalInfo', as: 'getDocFiscalInfo'
      get 'generar_factura', to: 'transactions#general_invoice', as: 'general_invoice'
      get 'buscar_facturas', to: 'transactions#search_invoices', as: 'search_invoices'
      get 'timbrar_factura_general', to: 'transactions#stamp_general_invoice', as: 'stamp_general_invoice'
      get 'imprimir_transaccion_pdf/:id/:folio', to: 'transactions#print_pdf', as: 'print_pdf'
      get 'descarga_transaccion_xml/:id/:folio', to: 'transactions#transaction_download_xml', as: 'transaction_download_xml'
      get 'get_doctor_patients', to: 'transactions#get_doctor_patients', as: 'get_doctor_patients'
      get 'get_patient_fiscal', to: 'transactions#get_patient_fiscal', as: 'get_patient_fiscal'
      get 'get_patient_fiscal_to_set', to: 'transactions#get_patient_fiscal_to_set', as: 'get_patient_fiscal_to_set'

    end
  end
   resources :patient_fiscal_informations, :path => 'informacion_fiscal_paciente', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    get 'configuracion', to: 'account_settings#settings', as: 'settings'
    get 'delete_prescription_field', to: 'account_settings#delete_prescription_field', as: 'delete_prescription_field'
    put 'add_prescription_field', to: 'account_settings#add_prescription_field', as: 'add_prescription_field'
    post 'add_file_prescription', to: 'account_settings#add_file_prescription', as: 'add_file_prescription'
    get 'download_image', to: 'account_settings#download_image', as: 'download_image'
    get 'delete_report_field', to: 'account_settings#delete_report_field', as: 'delete_report_field'
    put 'add_report_field', to: 'account_settings#add_report_field', as: 'add_report_field'
    post 'add_file_report', to: 'account_settings#add_file_report', as: 'add_file_report'
    get 'download_image_report', to: 'account_settings#download_image_report', as: 'download_image_report'
    get 'get_fields_toadd', to: 'account_settings#get_fields_toadd', as: 'get_fields_toadd'
    get 'add_currency_exchange', to: 'account_settings#add_currency_exchange', as: 'add_currency_exchange'
    get 'delete_currency_exchange', to: 'account_settings#delete_currency_exchange', as: 'delete_currency_exchange'
    get 'create_custom_prescription', to: 'account_settings#create_custom_prescription', as: 'create_custom_prescription'
    get 'create_custom_report', to: 'account_settings#create_custom_report', as: 'create_custom_report'
    post 'actualizar_configuracion', to: 'account_settings#update_settings', as: 'settings_update'
    get 'perfil', to: 'account_settings#doctor_profile', as: 'doctor_profile'
    get 'update_charge_report', to: 'account_settings#update_charge_report', as: 'update_charge_report'
    get 'update_iva_report', to: 'account_settings#update_iva_report', as: 'update_iva_report'
    get 'update_iva_report_included', to: 'account_settings#update_iva_report_included', as: 'update_iva_report_included'


    resources :analysis_order, only: [:show,:new], :path => 'orden_analisis', :path_names => { :show => 'ver', :new => 'nuevo' }  do
      collection do
        post 'crear/:expedient_id', to: 'analysis_order#create_analysis', as: 'create'
        get 'analisis_index/:expedient_id', to: 'analysis_order#index_analysis', as: 'index'
        get 'descarga_analisis/:id', to: 'analysis_order#print_analysis_order', as: 'download_analysis'
        get 'estudios_clinicos', to: 'analysis_order#find_clinical_studies', as: 'clinical_studies'
        post 'adjuntar_archivo', to: 'analysis_order#add_archive', as: 'attachment_file'
        get 'descargar_archivo/:id', to: 'analysis_order#download_file', as: 'download_file'
        delete 'eliminar_archivo', to: 'analysis_order#delete_archive', as: 'delete_file'
        get 'ver_pdf', to: 'analysis_order#viewer_pdf', as: 'viewer_pdf'
        get 'ver_imagen', to: 'analysis_order#viewer_img', as: 'viewer_img'
        get 'show_indications', to: 'analysis_order#find_all_indications', as: 'show_indications'
        get 'enviar_reporte_email', to: 'analysis_order#send_report_email', as: 'send_report_email'
      end
    end
    resources :cabinet_reports, only: [:show,:new,:edit], :path => 'orden_analisis_reporte', :path_names => { :show => 'ver', :new => 'nuevo', :edit => 'editar' } do
      collection do
        get 'descarga_analisis/:id', to: 'cabinet_reports#print_report_analysis_order', as: 'download_analysis'
        put 'save_cabinet_report', to: 'cabinet_reports#save_cabinet_report', as: 'save_cabinet_report'
        get 'update_price', to: 'cabinet_reports#update_price', as: 'update_price'
      end
    end


    resources :activities do
      collection do
        get 'get_day_activities', to: 'activities#get_day_activities', as: 'get_day_activities'
        get 'validate_date', to: 'activities#validate_rank_date', as: 'validate_date'
        get 'update_status', to: 'activities#update_status', as: 'update_activity_status'
        get 'update_dates', to: 'activities#update_dates', as: 'update_dates'
        get 'resend_surgery_email', to: 'activities#resend_surgery_email', as: 'resend_surgery_email'
      end
    end
    resources :patients, :path => 'pacientes', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        put 'fotografia', to: 'patients#picture', as: 'pictures'
        get 'buscar_paciente', to: 'patients#find_patients', as: 'find_patients'
        get 'pacientes_inactivos', to: 'patients#inactive_patients', as: 'inactive_patients'
        get 'tabla_pacientes_inactivos', to: 'patients#inactive_patients_table', as: 'inactive_patients_table'
        get 'paciente_inhabilitado', to: 'patients#disable_patient', as: 'disable_patient'
        get 'descargar_imagen/:id', to: 'patients#download_image', as: 'download_image'
        get 'calcular_edad', to: 'patients#calculate_age', as: 'calculate_age'
      end
    end

    resources :offices, :path => 'consultorios', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'office_manage', to: 'offices#office_manage', as: 'office_manage'
        get 'get_offices', to: 'offices#get_offices', as: 'get_offices'
        get 'save_offices', to: 'offices#save_offices', as: 'save_offices'
        get 'plantilla_index' => 'offices#index_template_prescription', as: 'templates_prescription'
        get 'descargar_imagen/:id', to: 'offices#download_image', as: 'download_image'
        post 'crear_plantilla' => 'offices#selected_template_prescription', as: 'selected_prescription'
        delete 'eliminar_plantilla/:id' => 'offices#deleted_template_office', as: 'delete_prescription'
        get 'seleccionar_consultorio_actual' => 'offices#set_current_office', as: 'set_current_office'
        get 'get_office_data' => 'offices#get_office_data', as: 'get_office_data'
      end
    end
    resources :general_fiscal_infos, :path => 'informacion_fiscal', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'get_fiscal_data', to: 'general_fiscal_infos#get_fiscal_data', as: 'get_fiscal_data'
      end
    end
    resources :allergies, :path => 'alergias', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :personalsupports, :path => 'participantes', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :assistants, :path => 'asistentes', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'get_assistant_data', to: 'assistants#get_assistant_data', as: 'get_assistant_data'
        get 'check_new_user', to: 'assistants#check_new_user', as: 'check_new_user'

      end
    end
    resources :pregnancies do
      collection do
        get 'finish', to: 'pregnancies#finish', as: 'finish_pregnancy'
        get 'reporte', to: 'pregnancies#pregnancy_report', as: 'pregnancy_report'
        get 'print_report', to: 'pregnancies#print_report', as: 'print_report'
        get 'get_birth_info', to: 'pregnancies#get_birth_info', as: 'get_birth_info'
        get 'get_neonate_info', to: 'pregnancies#get_neonate_info', as: 'get_neonate_info'
        get 'update_birth_info', to: 'pregnancies#update_birth_info', as: 'update_birth_info'
        get 'update_neonate_info', to: 'pregnancies#update_neonate_info', as: 'update_neonate_info'
        get 'get_abortion_info', to: 'pregnancies#get_abortion_info', as: 'get_abortion_info'
        get 'update_abortion_info', to: 'pregnancies#update_abortion_info', as: 'update_abortion_info'
        get 'load_ultrasounds', to: 'pregnancies#load_ultrasounds', as: 'load_ultrasounds'
      end
    end
    resources :medical_consultations, :path => 'consultas_medicas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'load_consultation', to: 'medical_consultations#load_consultation', as: 'load_consultation'
        get 'descargar_prescripcion/:id', to: 'medical_consultations#generate_prescription', as:'generate_prescription'
        get 'has_template_prescription', to: 'medical_consultations#has_medical_prescription_office', as:'has_template_prescription'
        get 'generate_imc/:id_patient', to: 'medical_consultations#imc', as: 'generate_imc'
        put 'actualizar_impresion', to: 'medical_consultations#update_flag_print_diagnostic', as: 'update_print'
        get 'imprimir_reporte', to: 'medical_consultations#print_report', as: 'print_report'
        get 'update_cost', to: 'medical_consultations#update_cost', as: 'update_cost'
        get 'send_prescription', to: 'medical_consultations#send_prescription', as: 'send_prescription'
      end
    end
    resources :perinatal_informations
    resources :child_non_pathological_infos
    resources :inherited_family_backgrounds
    resources :gynecologist_obstetrical_backgrounds
    resources :parent_infos do
      collection do
        delete 'delete', to: 'parent_infos#destroy', as: 'delete'
        put 'update', to: 'parent_infos#update', as: 'update'
      end
    end
    resources :accounts, :path => 'cuentas_bancarias', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'imprimir_cuenta_bancaria', to: 'accounts#account_balance', as: 'account_balance'
        get 'account_manage', to: 'accounts#account_manage', as: 'account_manage'
        get 'get_accounts', to: 'accounts#get_accounts', as: 'get_accounts'
        get 'save_accounts', to: 'accounts#save_accounts', as: 'save_accounts'
      end
    end
    get 'sala_espera', to: 'waiting_room#index', as: 'doctor_waiting_room'
    get 'vaccine/book', to: 'dashboard#vaccineBook', as: 'vaccine_book'
    get 'patient/vaccine_book/:id', to: 'dashboard#patient_vaccine_book', as: 'patient_vaccine_book'
    get 'dashboard/assistant', to: 'dashboard#assistant', as: 'dashboard_assistant'
    get 'dashboard/waiting_room', to: 'dashboard#assistant_waiting_room', as: 'assistant_waiting_room'
    get 'dashboard/pending_consult', to: 'dashboard#pending_consults', as: 'assistant_pending_consults'
    get 'dashboard/pending_report', to: 'dashboard#pending_reports', as: 'assistant_pending_reports'
    get 'index' => 'index#index'
    get 'terms_conditions', to: "index#accept_terms"
    put 'update_terms_conditions', to: "index#update_terms_conditions"
    get 'ver_expediente/:id', to: 'medical_expedients#show', as: 'search_expedient'
    get 'show_patient/:id', to: 'medical_expedients#search_patient', as: 'search_patient'
    get 'general_observations/:id', to: 'medical_expedients#general_observations', as: 'general_observations'
    get 'expediente_medico', to: 'medical_expedients#index', as: 'index_expedient'
    get 'medical_consultation/:id', to: 'medical_expedients#medical_consultation', as: 'index_consultation'
    get 'show_medical_consultation/:id', to: 'medical_expedients#show_medical_consultation', as: 'show_medical_consultation'
    get 'form_consultation/:id', to: 'medical_expedients#form_medical_consultation', as: 'new_consultation'
    get 'form_edit_medical_consultation/:id', to: 'medical_expedients#form_edit_medical_consultation', as: 'edit_consultation'
    post 'create_consultation/:expedient_id', to: 'medical_expedients#create_medical_consultation', as: 'create_consultation'
    get 'update_consultation/:expedient_id', to: 'medical_expedients#update_medical_consultation', as: 'update_medical_consultation'
    get 'view_expedient/:id', to: 'medical_expedients#view_expedient_patient', as: 'view_expedient'
    get 'view_expedient_graph_tall/', to: 'medical_expedients#view_expedient_patient_graph_tall', as: 'view_expedient_graph_tall'
    get 'view_expedient_graph_valoration/', to: 'medical_expedients#view_expedient_patient_graph_valoration', as: 'view_expedient_graph_valoration'

    resources :vital_signs
    resources :medical_histories do
      collection do
        get 'find_disease', to: 'medical_histories#find_disease', as: 'find_disease'
        get 'add_disease', to: 'medical_histories#add_disease', as: 'add_disease'
        get 'exist_disease', to: 'medical_histories#exist_disease', as: 'exist_disease'
        get 'search_medicament', to: 'medical_histories#find_medicament', as: 'search_medicament'
        get 'add_medicament', to: 'medical_histories#add_medicament', as: 'add_medicament'
        get 'exist_medicament', to: 'medical_histories#exist_medicament', as: 'exist_medicament'
        get 'load_vaccine_books', to: 'medical_histories#load_vaccine_books', as: 'load_vaccine_books'
        get 'add_vaccinebook_patient', to: 'medical_histories#add_vaccinebook_patient', as: 'add_vaccinebook_patient'
        get 'delete_vaccinebook_patient', to: 'medical_histories#delete_vaccinebook_patient', as: 'delete_vaccinebook_patient'

      end
    end
    get 'index' => 'index#index'
    get 'clinical_history/:id', to: 'medical_expedients#medical_history', as: 'clinical_history'
    get 'update_general_observations/:id', to: 'medical_expedients#update_general_observations', as: 'update_general_observations'
    get 'vital_sign_section/:id', to: 'medical_expedients#vital_sign_section', as: 'vital_sign_section'
    get 'vital_sign_table/:id', to: 'medical_expedients#vital_sign_table', as: 'vital_sign_table'
    get 'get_vital_sign', to: 'medical_expedients#get_vital_sign', as: 'get_vital_sign'
    get 'get_data_chart_vital_sign', to: 'medical_expedients#get_data_chart_vital_sign', as: 'get_data_chart_vital_sign'
    get 'medical_ped_histories/', to: 'medical_ped_histories#index', as: 'medical_ped_histories'
    get 'percentile_weight/', to: 'medical_ped_histories#graph_percentile_weight', as: 'percentile_weight'
    get 'percentile_head/', to: 'medical_ped_histories#graph_percentile_head_circumference', as: 'percentile_head'
    get 'percentile_imc/', to: 'medical_ped_histories#graph_percentile_imc', as: 'percentile_imc'
    get 'percentile_length/', to: 'medical_ped_histories#graph_percentile_size_length', as: 'percentile_length'

    resources :daily_cashes, :path => 'corte_caja', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'imprimir_corte/:id', to: 'daily_cashes#print_daily_cash', as: 'print_daily_cash'
        get 'agregar_transaccion', to: 'daily_cashes#add_transaction', as: 'add_transaction'
        get 'historial', to: 'daily_cashes#history', as: 'history'
        get 'imprimir/:id', to: 'daily_cashes#print_daily_cash', as: 'print_pdf'
        get 'generar_cobro', to: 'daily_cashes#create_charge', as: 'create_charge'
      end
    end

    resources :certificate, :path => 'certificados', only:[:index] do
        collection do
          get 'tipos_de_certificados', to: 'certificate#index_type_certificate', as:'index_type_certificate'
          get 'nuevo_tipo', to: 'certificate#new_type_certificate', as:'new_type_certificate'
          get 'ver_tipo/:id', to: 'certificate#show_type_certificate', as:'show_type_certificate'
          get 'editar_tipo_certificado/:id', to: 'certificate#edit_type_certificate', as:'edit_type_certificate'

          post 'create_type_certificate', to: 'certificate#create_type_certificate', as:'create_type_certificate'
          get 'create_type_certificate', to: 'certificate#new_type_certificate'
          put 'update_type_certificate/:id', to: 'certificate#update_type_certificate', as:'update_type_certificate'
          patch 'update_type_certificate/:id', to: 'certificate#update_type_certificate'
          get 'update_type_certificate/:id', to: 'certificate#edit_type_certificate'
          get 'type_certificate_container', to: 'certificate#type_certificate_container', as: 'type_certificate_html'
          delete 'destroy_type_certificate/:id', to: 'certificate#destroy_type_certificate', as:'destroy_type_certificate'

          get 'certificados', to: 'certificate#index_certificate', as:'index_certificate'
          get 'nuevo_certificado', to: 'certificate#new_certificate', as:'new_certificate'
          get 'ver_certificado/:id', to: 'certificate#show_certificate', as:'show_certificate'
          get 'editar_certificado/:id', to: 'certificate#edit_certificate', as:'edit_certificate'
          get 'imprimir_certificado/:id', to: 'certificate#print_certificate', as:'print_certificate'

          post 'create_certificate', to: 'certificate#create_certificate', as:'create_certificate'
          get 'create_certificate', to: 'certificate#new_certificate'
          put 'update_certificate/:id', to: 'certificate#update_certificate', as:'update_certificate'
          patch 'update_certificate/:id', to: 'certificate#update_certificate'
          get 'update_certificate/:id', to: 'certificate#edit_certificate'
          delete 'destroy_certificate/:id', to: 'certificate#destroy_certificate', as:'destroy_certificate'

          get 'certificate_patient/:patient_id', to: 'certificate#certificate_patient', as: 'certificate_patient'
        end
    end

    resources :study_packages, :path => 'paquetes', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'add_study', to: 'study_packages#add_study', as: 'add_study'
        get 'remove_study', to: 'study_packages#remove_study', as: 'remove_study'
        get 'by_laboratory', to: 'study_packages#by_laboratory', as: 'by_laboratory'
        get 'get_studies', to: 'study_packages#get_studies', as: 'get_studies'
      end
    end

    resources :laboratories, :path => 'laboratorios', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'download_image/:id', to: 'laboratories#download_image', as: 'download_image'
      end
    end
    resources :ca_groups, :path => 'grupos_indicaciones', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :clinical_studies, :path => 'estudios_clinicos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'plantilla', to: 'clinical_studies#get_template', as: 'get_template'
        get 'laboratorio', to: 'clinical_studies#get_by_laboratory', as: 'get_by_laboratory'
        get 'categoria', to: 'clinical_studies#get_by_category', as: 'get_by_category'
        get 'indicaciones', to: 'clinical_studies#get_indications', as: 'get_indications'
        get 'agregar_indicacion', to: 'clinical_studies#add_indication', as: 'add_indication'
        get 'eliminar_indicacion', to: 'clinical_studies#delete_indication', as: 'delete_indication'
      end
    end
    resources :indications, :path => 'indicaciones', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'grupo', to: 'indications#get_by_group', as: 'get_by_group'
      end
    end
    resources :clinical_study_types, :path => 'categorias_estudios', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'cargar_por_laboratorio', to: 'clinical_study_types#get_by_lab', as: 'get_by_lab'
      end
    end

    resources :cardiovascular_aps, :path => 'aparato_cardiovascular', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :respiratory_aps, :path => 'aparato_respiratorio', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :digestive_aps, :path => 'aparato_digestivo', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :urinary_aps, :path => 'aparato_urinario', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :psychosomatic_sis, :path => 'sistema_psicosomatico', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :nervous_sis, :path => 'sistema_nervioso', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :genital_aps, :path => 'aparato_genital', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
	  resources :otolaryngology_sis, :path => 'sistema otorrinolaringológico', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :metabolic_sis, :path => 'sistema_metabolico', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :hematological_sis, :path => 'sistema_hematologico', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :ophthalmological_sis, :path => 'sistema_oftalmológico', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :locomotor_sis, :path => 'sistema_locomotor', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :infection_ots, :path => 'infecciones', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :sense_organs, :path => 'organos_sentidos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }

    resources :urological_backgrounds, :path => 'antecedentes_urologicos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }


  end

  namespace :admin, path: 'administrador' do
    resources :invoices, :path => 'facturas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'getFiscalInfos', to: 'invoices#getFiscalInfos', as: 'getFiscalInfos'
      end
    end
    resources :study_packages, :path => 'paquetes_estudios', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :news_items, :path => 'noticias', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'get_banner/:id', to: 'news_items#get_banner', as: 'get_banner'
      end
    end
    resources :authors, :path => 'autores', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'get_picture/:id', to: 'authors#get_picture', as: 'get_picture'
      end
    end
    resources :general_accounts, :path => 'Cuentas_generales', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :general_consultation_types, :path => 'Tipos_consultas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'consultation_type_manage', to: 'general_consultation_types#consultation_type_manage', as: 'consultation_type_manage'
        get 'get_consultation_types', to: 'general_consultation_types#get_consultation_types', as: 'get_consultation_types'
        get 'save_consultation_types', to: 'general_consultation_types#save_consultation_types', as: 'save_consultation_types'
      end
    end
    resources :hospitals, :path => 'hospitales', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :finkok_configs, :path => 'configuracion_finkok', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :clinic_infos, :path => 'datos_clinica', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'get_emisor_info', to: 'clinic_infos#get_emisor_info', as: 'get_emisor_info'
        get 'delete_emisor_info', to: 'clinic_infos#delete_emisor_info', as: 'delete_emisor_info'
        get 'descargar_imagen/:id', to: 'clinic_infos#download_image', as: 'download_image'
        get 'add_currency_exchange', to: 'clinic_infos#add_currency_exchange', as: 'add_currency_exchange'
        get 'delete_currency_exchange', to: 'clinic_infos#delete_currency_exchange', as: 'delete_currency_exchange'
        get 'getClinicInfo', to: 'clinic_infos#getClinicInfo', as: 'getClinicInfo'
        get 'default_backgrounds', to: 'clinic_infos#default_backgrounds', as: 'default_backgrounds'
        post 'save_no_pathologicals', to: 'clinic_infos#save_no_pathologicals', as: 'save_no_pathologicals'
        post 'save_pathologicals', to: 'clinic_infos#save_pathologicals', as: 'save_pathologicals'
        post 'save_ginecological_background', to: 'clinic_infos#save_ginecological_background', as: 'save_ginecological_background'
        post 'save_urological_background', to: 'clinic_infos#save_urological_background', as: 'save_urological_background'
        post 'save_perinatal_background', to: 'clinic_infos#save_perinatal_background', as: 'save_perinatal_background'
      end
    end
    get 'index' => 'index#index'
    resources :index_comments, :path => 'comentarios_index', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'profile_image/:id', to: 'index_comments#profile_image', as: 'profile_image'
      end
    end
    resources :medical_annotation_types, :path => 'tipos_anotaciones_medicas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        delete 'eliminar_campo/:id', to: 'medical_annotation_types#delete_field', as: 'delete_field'
      end
    end
    resources :vaccines, :path => 'vacunas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :banks, :path => 'bancos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :attention_types, :path => 'tipos_atencion', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :triages, :path => 'triages', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :fixed_asset_categories, :path => 'categorias_activos_fijos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :medicament_laboratories, :path => 'laboratorios_medicamentos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :fixed_asset_activities, :path => 'actividades_activos_fijos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do

        get 'load_asset_activities', to: "fixed_asset_activities#load_asset_activities", as: 'load_asset_activities'
      end
    end
    resources :fixed_asset_items, :path => 'elementos_activos_fijos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :asset_activities, :path => 'actividades_activos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }do
      collection do
        get 'check_color', to: "fixed_asset_activities#check_color", as: 'check_color'
      end
    end
    resources :fixed_assets, :path => 'activos_fijos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'manage_items/:id', to: 'fixed_assets#manage_items', as: 'manage_items'
        get 'dashboard', to: 'fixed_assets#dashboard', as: 'dashboard'
        get 'load_dashboard_items', to: 'fixed_assets#load_dashboard_items', as: 'load_dashboard_items'
        get 'agenda/:item_id', to: 'fixed_assets#schedule', as: 'schedule'
        get 'get_fixed_asset_activities', to: 'fixed_assets#get_fixed_asset_activities', as: 'get_fixed_asset_activities'
      end
    end
    resources :lots, :path => 'Lotes', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'edit_expiration_date', to: 'lots#edit_expiration_date', as: 'edit_expiration_date'
      end
    end
    resources :warehouses, :path => 'Almacenes', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'manage_warehouse', to: 'warehouses#manage_warehouse', as: 'manage_warehouse'
        get 'get_warehouses', to: 'warehouses#get_warehouses', as: 'get_warehouses'
        get 'save_warehouses', to: 'warehouses#save_warehouses', as: 'save_warehouses'
      end
    end
    resources :vaccinations do
      collection do
        get 'add_vaccinate_date', to: 'vaccinations#add_vaccinate_date', as: 'add_vaccinate_date'
      end
    end
    resources :disease_categories, :path => 'categoria_enfermedades', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'diseases', to: 'disease_categories#diseaseDelete', as: 'disease'
      end
    end
    get 'cities/find_cities', to: 'cities#find_cities', as: 'search_city'
    get 'doctors/find_doctors', to: 'doctors#find_doctors', as: 'search_doctor'
    resources :diseases, :path => 'enfermedades', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'disease_json', to: 'diseases#disease_json', as: 'disease_json'
      end
    end
    resources :doctors, :path => 'medicos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'get_doctor_offices', to: 'doctors#get_doctor_offices', as: 'get_doctor_offices'
        get 'new/:user_id', to: "doctors#new", as: 'new'
        get 'search_city', to: "doctors#find_city", as: 'search_city'
        get 'download/:id', to: "doctors#download_certificate", as: 'download'
        get 'download_key/:id', to: "doctors#download_key", as: 'download_key'
        get 'download_image/:id', to: "doctors#download_image", as: 'download_image'
        get 'descargar_imagen_perfil/:id', to: "doctors#download_profile_image", as: 'download_profile_image'
        post 'send_email_password/:id', to: "doctors#send_email_reset_password", as: 'send_email_password'
        get 'check_new_user', to: "doctors#check_new_user", as: 'check_new_user'
        get 'print_certificate/:license_id', to: "doctors#print_certificate", as: 'print_certificate'
      end
    end
    resources :assistants, :path => 'superasistentes', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'add_doctor_assistant', to: 'assistants#add_doctor_assistant', as: 'add_doctor_assistant'
        get 'delete_doctor_assistant', to: 'assistants#delete_doctor_assistant', as: 'delete_doctor_assistant'
        get 'get_assistant_data', to: 'assistants#get_assistant_data', as: 'get_assistant_data'
        get 'get_doctor_offices', to: 'assistants#get_doctor_offices', as: 'get_doctor_offices'
      end
    end
    resources :users
    resources :persons, :path => 'usuarios', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'find_person', to: 'persons#find_person', as: 'find_person'
        get 'save_doctors_agendas', to: 'persons#save_doctors_agendas', as: 'save_doctors_agendas'
        get 'get_doctors_agendas', to: 'persons#get_doctors_agendas', as: 'get_doctors_agendas'
      end
    end
    resources :specialties, :path => 'especialidades', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :active_substances, :path => 'sustancias_activas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :medicaments, :path => 'medicamentos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'medicament_json', to: 'medicaments#medicament_json', as: 'medicament_json'
        get 'find_medicament', to: 'medicaments#find_medicament', as: 'find_medicament'
        get 'sustancia_activa/:id', to: 'medicaments#active_substance', as: 'active_substance'
        get 'add_substance', to: 'medicaments#add_substance', as: 'add_substance'
        get 'delete_substance', to: 'medicaments#delete_substance', as: 'delete_substance'
      end
    end
    resources :operation_roles, :path => 'roles_operacion', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :identity_cards
    resources :licenses, :path => 'licencias', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :importations, :path => 'Importaciones', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        put 'import_patients', to: 'importations#import_patients', as: 'import_patients'
        post 'import_medical_history', to: 'importations#import_medical_history', as: 'import_medical_history'
        post 'import_medical_consultation', to: 'importations#import_medical_consultation', as: 'import_medical_consultation'
        post 'import_pregnancy', to: 'importations#import_pregnancy', as: 'import_pregnancy'
        post 'import_user', to: 'importations#import_user', as: 'import_user'
        get 'cli_importations', to: 'importations#cli_importations', as: 'cli_importations'
      end
    end
    resources :email_settings
    resources :ca_groups, :path => 'grupos_indicaciones', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :clinical_studies, :path => 'estudios_clinicos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'categoria', to: 'clinical_studies#get_by_category', as: 'get_by_category'
        get 'indicaciones', to: 'clinical_studies#get_indications', as: 'get_indications'
        get 'agregar_indicacion', to: 'clinical_studies#add_indication', as: 'add_indication'
        get 'eliminar_indicacion', to: 'clinical_studies#delete_indication', as: 'delete_indication'
      end
    end
    resources :indications, :path => 'indicaciones', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'grupo', to: 'indications#get_by_group', as: 'get_by_group'
      end
    end
    resources :clinical_study_types, :path => 'categorias_estudios', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :laboratories, :path => 'laboratorios', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'download_image/:id', to: 'laboratories#download_image', as: 'download_image'
      end
    end
    resources :application_ages, :path => 'datos_vacunacion', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :vaccine_books, :path => 'cartilla_vacunas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'add_book_patient', to: 'vaccine_books#add_book_patient', as: 'add_book_patient'
      end
    end

    resources :chats, :path => 'soporte', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'chat/:channel', to: 'chats#chat', as: 'chat'
      end
    end

    resources :medicament_categories, :path => 'categorias_medicamentos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :inventories, :path => 'inventarios', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'busqueda_medicamentos', to: 'inventories#search_medicament_inventories', as: 'search_medicament_inventories'
        get 'reporte_inventario/:id', to: 'inventories#print_pdf_inventory_report', as: 'print_pdf_inventory_report'
        get 'actualizar_existecias', to: 'inventories#update_medicament_inventories', as: 'update_medicament_inventories'
      end
    end

    resources :medicament_inventories, :path => 'existencia_medicamentos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }


  end

  namespace :patient, path: 'paciente' do

    resources :index, only: [:index] do
      collection do
        get 'terms_conditions', to: "index#accept_terms"
        put 'update_terms_conditions', to: "index#update_terms_conditions"
      end
    end

    resources :appointments, :path => 'citas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }do
      collection do
        get 'check_availability', to: "appointments#check_availability"
        get 'load_doctor_schedule', to: "appointments#load_doctor_schedule"
        get 'doctors', to: "appointments#doctors"
      end
    end

    resources :expedients, :path => 'expedientes', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'health_history', to: "expedients#health_history"
        get 'default_patient_data', to: "expedients#default_patient_data"
      end
    end

  end

  namespace :shared_assistant, path: 'asistente' do

    resources :patients, :path=> 'paciente', :path_names => { :edit => 'editar', :show => 'ver', :new => 'nuevo' }

    resources :index, only: [:index] do
      collection do
        get 'activities', to: "index#activities"
        get 'waiting_room', to: "index#waiting_room"
        get 'pending_medical_consult', to: "index#pending_consult"
        get 'pending_report', to: "index#pending_report"
      end
    end
    resources :personalsupports, :path => 'participantes', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' }
    resources :general_fiscal_infos, :path => 'informacion_fiscal', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'get_fiscal_data', to: 'general_fiscal_infos#get_fiscal_data', as: 'get_fiscal_data'
      end
    end
    resources :transactions, :path => 'transaccion', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'general_invoice', to: 'transactions#general_invoice', as: 'general_invoice'
        get 'getDocFiscalInfo', to: 'transactions#getDocFiscalInfo', as: 'getDocFiscalInfo'
        get 'search_invoices', to: 'transactions#search_invoices', as: 'search_invoices'
        get 'open_invoice', to: 'transactions#open_invoice', as: 'open_invoice'
      end
    end
    resources :address_books, :path => 'contactos', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'get_contact_data', to: 'address_books#get_contact_data', as: 'get_contact_data'
      end
    end
    resources :invoices, :path => 'facturas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'get_doctor_fi', to: 'invoices#get_doctor_fi', as: 'get_doctor_fi'
        get 'facturas_abiertas', to: 'invoices#open_invoices', as: 'open_invoices'
        get 'get_doctor_open_names', to: 'invoices#get_doctor_open_names', as: 'get_doctor_open_names'
      end
    end
    resources :medical_consultations, :path => 'consultas_medicas', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'print_report', to: 'medical_consultations#print_report', as: 'print_report'
      end
    end


    resources :daily_cashes, :path => 'cortes_de_caja', :path_names => { :edit => 'editar', :delete => 'eliminar', :show => 'ver', :new => 'nuevo' } do
      collection do
        get 'historial', to: 'daily_cashes#history', as: 'history'
        get 'imprimir/:id', to: 'daily_cashes#print_daily_cash', as: 'print_pdf'
      end
    end

  end


  # Api definition
  # Keeping our API under its own subdomain allows load balancing traffic at the DNS level
  # Para acceder al subdominio en localhost modificar el archivo /etc/hosts
  # namespace :api, path: '/', constraints: {subdomain: 'api'},
  namespace :api, path: '/',
            defaults: {format: :json} do
    scope module: :v1 do
      # We are going to list our resources here
      get "users/login" => "users#login"
      post "users/token/:user_id" => "users#create_token"
      put "users/update_profile/:user_id" => "users#update_profile"
      post "users/create" => "users#create_patient"

      get "appointments/index/:user_id" => "appointments#index"
      post "appointments/create/:user_id" => "appointments#create"  # se usa por el paciente de la app, genera citas

      get "doctors/filter/:user_id" => "doctors#doctors_filter"
      get "doctors/city" => "doctors#get_city"
      get "doctors/offices" => "doctors#offices"
      get "doctors/search_patients/:user_id" => "doctors#search_patient"
      get "doctors/search_personal_support/:user_id" => "doctors#search_personal_support"

      get "doctors/offices_availability/:office_id" => "doctors#office_availability"
      get "doctors/check_availability/:office_id" => "doctors#check_availability"

      get "activities/schedule_filter/:user_id" => "activities#schedule_filter"
      get "activities/update_appointment/:user_id/:activity_id" => "activities#update_appointment"
      get "activities/show_activity/:activity_id" => "activities#show_activity"
      delete "activities/destroy/:activity_id" => "activities#destroy_activity"
      post "activities/create/:user_id" => "activities#create_activity"  # se usa por el doctor de la app, genera citas, cirujias, vacaciones, otros.

      get "doctors/patients/:user_id" => "doctors#show_patients"
      get "expedients/expedient_info/:patient_id" => "expedients#expedient_info_patient"
      get "expedients/expedient_consultation/:consultation_id" => "expedients#expedient_consultation_patient"
      get "expedients/expedient_consultation_data/:patient_id" => "expedients#consultation_data"

      # doctores favoritos
      get "doctors/favorite_doctors/:user_id" => "doctors#index_favorite_doctors"
      post "doctors/add_favorite/:user_id" => "doctors#add_favorite"
      delete "doctors/delete_favorite/:user_id" => "doctors#remove_favorite"

      # historial de salud
      get "health_histories/index/:user_id" => "health_histories#index"
      post "health_histories/create/:user_id" => "health_histories#create"
      put "health_histories/update/:user_id" => "health_histories#update"
      # parameter health_history_id
      delete "health_histories/delete/:user_id" => "health_histories#destroy"
      # parameter health_history_id, medical_expedient_id
      put "health_histories/unlink_expedient/:user_id" => "health_histories#unlink_medical_expedient"

      get "health_histories/filter/:health_history_id" => "health_histories#health_histories_expedient"
      get "health_histories/consultations/:expedient_id" => "health_histories#health_histories_consultations"

      get "health_histories/check_expedient/:user_id" => "health_histories#valid_medical_expedient"
      get "health_histories/add_expedient/:user_id" => "health_histories#link_medical_expedient"
      get "health_histories/medical_history/:patient_id" => "health_histories#medical_history_expedient"
      get "health_histories/observations/:patient_id" => "health_histories#observations_expedient"
      get "health_histories/laboratory_orders/:patient_id" => "health_histories#laboratory_orders_expedient"


    end
  end
end
