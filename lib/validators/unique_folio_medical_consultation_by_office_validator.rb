class UniqueFolioMedicalConsultationByOfficeValidator < ActiveModel::EachValidator

  def validate_each(object, attribute, value)
    if value.present?
      unless object.id.present?
        # solo cuando se crea por primera vez
        if MedicalConsultation.all.where.not(id: object.id).includes(:office).where(offices: {id:object.office.id}).exists?(folio: value.to_i)
          object.errors[attribute] << (options[:message] || "ya está en uso, actualizar el siguiente folio del consultorio #{object.office.name}")
        end
      end
    end
  end
end