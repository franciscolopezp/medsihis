class UniqueFolioOfficeValidator < ActiveModel::EachValidator

  def validate_each(object, attribute, value)
    if value.present?
      #ultimo folio del consultorio
      last_folio = MedicalConsultation.includes(:office).where(offices: {id:object.id}).maximum(:folio)
      unless last_folio.nil?
        # valida si existe el mismo folio ya guardado en alguna consulta
        if MedicalConsultation.includes(:office).where(offices: {id:object.id}).exists?(folio: value.to_i)
          object.errors[attribute] << (options[:message] || 'ya está en uso')
        else
          if value <= last_folio
            object.errors[attribute] << (options[:message] || 'No puede ser menor al ultimo folio de las consultas')
          end
        end
      end
    else
      object.errors[attribute] << (options[:message] || 'compo requerido')
    end
  end
end